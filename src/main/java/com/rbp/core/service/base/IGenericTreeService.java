package com.rbp.core.service.base;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import java.util.List;

public interface IGenericTreeService {
    List<BaseEntity> getChildrenByParentId(Long parentId);
    List<BaseEntity> getParentsByChildId(Long childId);
    void deleteNode(Long nodeId,Boolean deleteAllData);
}
