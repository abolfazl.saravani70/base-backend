package com.rbp.core.service.base.impl;
/**
 * @author Alireza Souhani 1398.02.01
 */

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.gridStructure.request.EnterpriseGetRowsRequest;
import com.rbp.core.model.domainmodel.gridStructure.response.EnterpriseGetRowsResponse;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.model.dto.base.FilterJsonInput;
import com.rbp.core.service.base.IGenericService;
import com.rbp.core.utility.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Service
public abstract class GenericService<T extends BaseEntity>
        implements IGenericService<T> {

    @Autowired
    protected abstract IGenericRepository<T> getGenericRepository();
/*    IGenericRepository<T, PK> iGenericRepository;


    protected  IGenericRepository<T> getGenericRepository(){
     return  iGenericRepository;
    }*/

    @Transactional
    public T save(T entity) {

        if (entity.getId() != null) {
            if (entity.getId() != 0) {
                T existingEntity = loadById(entity.getId());
                if (existingEntity == null) {
                    // TODO: 7/16/2019 throw exception
                    throw new ApplicationException(0, "داده مورد نظر برای بروز رسانی وجود ندارد",
                            entity.getClass().getName());
                } else if (existingEntity.getVersion() > entity.getVersion()) {
                    // TODO: 7/16/2019 throw exception
                    throw new ApplicationException(0, "نسخه پیشنهادی از نسخه موجود کمتر است",
                            entity.getClass().getName());
                }
            }
            return getGenericRepository().save(entity);
        } else
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0,
                    " مورد نظر را برای بروزرسانی یا صفر را برای ثبت داده مورد نظر وارد کنید id لطفاً",
                    entity.getClass().getName());
    }

    @Transactional
    public AdvancedSearchResult<T> getList(FilterJsonInput filter) throws ApplicationException {
        FilterJsonInput filterFactory = filterFactory();
        filterFactory.pageSize(filter.getPageSize());
        filterFactory.pageNumber(filter.getPageNumber());
        filterFactory.orderBy(filter.getOrderBy());
        if (filterFactory.getPageNumber() <= 0 || filterFactory.getPageSize() < 0) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0,
                    "The pageNumber must be greater than or equal to zero and " +
                            "the pageSize must be greater than zero!");
        }
        return getGenericRepository().getList(filterFactory);
    }

    @Transactional
    public T loadById(Long id) {
        FilterJsonInput filter = filterFactory();
        filter.where("id=" + id);
        filter.pageSize(1);
        return getGenericRepository().loadById(filter);
    }

    @Transactional
    public void deleteById(Long id) {
        T entity = loadById(id);
        getGenericRepository().delete(entity);
    }

    //@TODO: FIX User for spring security!!!!
    @Transactional
    public Long softDeleteById(Long entityId) {
        // User user = (User) SecurityContextHolder.getContext().getAuthentication();
        T entity = loadById(entityId);
        if (entity == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "داده مورد نظر برای حذف یافت نشد");
        } else {
            entity.setDeletedDate(LocalDateTime.now());
            // entity.setDeletedById(user.getId());
            entity.setIsDeleted(true);
            //entity.setVersion(new Date().getTime());
            // entity.setZoneCode(user.getZoneCode()); //<-----Uncomment if required!
            //entity.setIp(user.getIp());
            // getGenericRepository().save(entity);
            return (Long) entity.getId();
        }
    }

    @Transactional
    public Long tempDeleteById(Long entityId) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication();
        T entity = loadById(entityId);
        entity.setIsTempDeleted(true);
        entity.setUpdatedById(user.getId());
        entity.setUpdatedDate(LocalDateTime.now());
        //entity.setVersion(new Date().getTime());
        entity.setZoneCode(user.getZoneCode()); //<-----Uncomment if required!
        entity.setIp(user.getIp());
        getGenericRepository().save(entity);
        return (Long) entity.getId();
    }

    @Transactional
    public void update(T entry) {
        getGenericRepository().update(entry);
    }

    @PostConstruct
    void initClasses() {
        /*iGenericRepository = */
        ;
    }

    @Transactional
    public T getReference(Long id) {
        return getGenericRepository().getReference(id);
    }

    @Override
    @Transactional
    public AdvancedSearchResult<T> searchEntity(FilterJsonInput filter, Boolean isTempDeleted) {
        if (filter.getPageNumber() <= 0 || filter.getPageSize() < 0) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "The pageNumber must be greater than or equal to zero and the pageSize must be greater than zero!");
        }
        return getGenericRepository().searchEntity(filter, isTempDeleted);
    }

    @Override
    @Transactional
    public AdvancedSearchResult searchView(Class viewClass,FilterJsonInput filter, Boolean isTempDeleted){
        if (filter.getPageNumber() <= 0 || filter.getPageSize() < 0) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "The pageNumber must be greater than or equal to zero and the pageSize must be greater than zero!");
        }
        return getGenericRepository().searchView(viewClass, filter, isTempDeleted);

    }

    @Override
    @Transactional
    public AdvancedSearchResult<T> searchCoding(FilterJsonInput filter) {
        FilterJsonInput filterFactory = filterFactory();
        filterFactory.where("coding=" + filter.getWhere());
        filterFactory.pageNumber(filter.getPageNumber());
        filterFactory.pageSize(filter.getPageSize());
        filterFactory.orderBy(filter.getOrderBy());
        return searchEntity(filterFactory, false);
    }

    @Override
    @Transactional
    public String getCurrentDateTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now); //2016/11/16 12:08:43

    }

    @Override
    @Transactional
    public EnterpriseGetRowsResponse getDataGrid(EnterpriseGetRowsRequest request) {
        return getGenericRepository().getData(request);
    }

    protected FilterJsonInput filterFactory() {
        FilterJsonInput filter = new FilterJsonInput();
        filter.where("");
        filter.pageNumber(1);
        filter.pageSize(0);
        filter.orderBy("");
        filter.groupBy("");
        filter.groupByFunction("");
        filter.having("");
        filter.deleteAllData(false);
        return filter;
    }
}
