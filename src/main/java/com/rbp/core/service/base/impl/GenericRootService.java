package com.rbp.core.service.base.impl;

import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.core.model.dto.base.FilterJsonInput;
import com.rbp.core.service.base.IGenericRootService;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.common.enums.EN_PlanningLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

@Service
public abstract class GenericRootService<T extends RootBaseEntity>
        extends GenericTreeService<T> implements IGenericRootService<T> {
    @Autowired
    protected abstract IGenericRootRepository<T> getGenericRootRepository();

    @Autowired
    ApplicationContext appContext;

    @Override
    @Transactional
    public void deleteNode(Long nodeId, Boolean deleteAllData, Long JId, Integer levelId) {
        // Get children of node in same level
        List<T> children = getServiceByLevel(getClass().getSimpleName(), levelId).getChildrenByParentIdJunction(nodeId);
        if (children.size() == 0) {
            // Get children of node in next level
            List<T> treeRecord = getChildrenOfNextLevelByRootId(nodeId,
                    getServiceByLevel(getClass().getSimpleName(), levelId + 1));
            if (treeRecord.size() != 0) {
                if (deleteAllData==true) {
                    deleteListOfNode(treeRecord, deleteAllData, levelId + 1);
                    deleteSelfNode(levelId, nodeId);
                } else {
                    throw new ApplicationException(0, "گره مورد نظر دارای زیر مجموعه است، ابتدا زیرمجموعه حذف گردد!");
                }
            } else if (JId != 0) {
                getServiceByLevel(getClass().getSimpleName(), levelId).delete(JId);
            } else {
                deleteSelfNode(levelId, nodeId);
            }
        } else if (children.size() != 0 && deleteAllData==true) {
            deleteListOfNode(children, deleteAllData, levelId);
            deleteSelfNode(levelId, nodeId);
        } else {
            throw new ApplicationException(0, "گره مورد نظر دارای زیر مجموعه است، ابتدا زیرمجموعه حذف گردد!");
        }

    }

    @Override
    @Transactional
    public void getNodeChildren(Long nodeId, Long JId, Integer levelId, Stack<RootBaseEntity> listOfNodes) {
        // Get children of node in same level
        List<T> children = getServiceByLevel(getClass().getSimpleName(), levelId).getChildrenByParentIdJunction(nodeId);
        if (children.size() == 0) {
            // Get children of node in next level
            List<T> treeRecord = getChildrenOfNextLevelByRootId(nodeId,
                    getServiceByLevel(getClass().getSimpleName(), levelId + 1));
            if (treeRecord.size() != 0) {
                getChildrenOfChildren(treeRecord, levelId + 1,listOfNodes);
                pushSelfNodeInList(levelId,nodeId,listOfNodes);
            } else if (JId != 0) {
                listOfNodes.push(getServiceByLevel(getClass().getSimpleName(), levelId).getByIdInJunction(JId));
            } else {
                pushSelfNodeInList(levelId,nodeId,listOfNodes);
            }
        } else {
            getChildrenOfChildren(children, levelId,listOfNodes);
            pushSelfNodeInList(levelId,nodeId,listOfNodes);
        }
    }

    private void getChildrenOfChildren(List<T> children, Integer levelId,Stack<RootBaseEntity> listOfNodes) {
        for (T record : children)
            getNodeChildren(record.getChild().getId(), record.getId(), levelId, listOfNodes);
    }

    private void deleteListOfNode(List<T> children, Boolean deleteAllData, Integer levelId) {
        for (T record : children)
            deleteNode(record.getChild().getId(), deleteAllData, record.getId(), levelId);
    }

    private T getSelfNode(Integer levelId, Long nodeId){
        List<T> recordJ = getServiceByLevel(getClass().getSimpleName(), levelId).getJunctionByChildId(nodeId);
        if(recordJ != null && recordJ.size() != 0){
            return recordJ.get(0);
        }else{
            return null;
        }
    }

    private void pushSelfNodeInList(Integer levelId, Long nodeId,Stack<RootBaseEntity> listOfNodes){
        listOfNodes.push(getSelfNode(levelId,nodeId));
    }

    private void deleteSelfNode(Integer levelId, Long nodeId) {
//        List<T> jRecord = getServiceByLevel(getClass().getSimpleName(), levelId).getJunctionByChildId(nodeId);
        getServiceByLevel(getClass().getSimpleName(), levelId).delete(getSelfNode(levelId,nodeId).getId());
    }

    private List<T> getChildrenOfNextLevelByRootId(Long rootId, IGenericRootService nextLevelService) {
        List<T> list = nextLevelService.getChildrenByRootId(rootId);
        return list;
    }

    private IGenericRootService getServiceByLevel(String simpleName, int i) {
        EN_PlanningLevel currentLevel = EN_PlanningLevel.getEnumLevel(simpleName);
        EN_PlanningLevel anotherLevel = EN_PlanningLevel.getByKey(currentLevel.getKey() + i);
        String next = anotherLevel.toString();
        next = next.toLowerCase() + "Tree" + EN_PlanningLevel.SERVICE;
        IGenericRootService result = (IGenericRootService) appContext.getBean(next);
        return result;
    }


    @Override
    @Transactional
    public List<BaseEntity> getChildrenByParentId(Long parentId) {
        List<BaseEntity> children = getTreeChildren(parentId);
        if (children.size() == 0) {
            List<T> treeRecord = getChildrenOfNextLevelByRootId(parentId,
                    getServiceByLevel(getClass().getSimpleName(), 1));

            List<BaseEntity> roots = treeRecord
                    .stream()
                    .map(T::getChild)
                    .collect(Collectors.toList());
            return roots;
        } else
            return children;

    }

    @Override
    @Transactional
    public List<BaseEntity> getParentsByChildId(Long childId) {
        List<BaseEntity> parents = getTreeParent(childId);
        if (parents.size() != 0 && parents.get(0) == null) {
            List<T> treeRecord = getGenericTreeRepository().getChildrenByParentId(null);
            List<BaseEntity> roots = treeRecord
                    .stream()
                    .filter(root -> root.getRoot() != null)
                    .filter(root -> root.getChild().getId() == childId)
                    .map(T::getRoot)
                    .collect(Collectors.toList());
            return roots;
        } else return parents;
    }

    @Override
    @Transactional
    public List<T> getChildrenByRootId(Long rootId) {
        FilterJsonInput filter=filterFactory();
        filter.where("root.id=" + rootId);
        List<T> list = getGenericRootRepository().getChildrenByRootId(filter);
        return list;
    }

    @Override
    @Transactional
    public List<T> getChildrenByParentIdJunction(Long parentId) {
        FilterJsonInput filter=filterFactory();
        filter.where("parent.id="+parentId);
        List<T> treeRecord = getGenericRootRepository().getChildrenByParentId(filter);
        return treeRecord;
    }

    @Override
    @Transactional
    public List<T> getJunctionByChildId(Long childId) {
        FilterJsonInput filter=filterFactory();
        filter.where("child.id="+childId);
        return getGenericRootRepository().getParentsByChildId(filter);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        deleteById(id);
    }

    @Override
    @Transactional
    public RootBaseEntity getByIdInJunction(Long id) {
        return loadById(id);
    }
}
