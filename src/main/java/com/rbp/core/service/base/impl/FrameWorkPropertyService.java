package com.rbp.core.service.base.impl;
/**
 * @author Alireza Souhani 1398.02.01
 */
import com.rbp.core.model.dao.base.IFrameWorkPropertyDao;
import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.base.FrameworkProperty;
import com.rbp.core.service.base.IFrameWorkPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FrameWorkPropertyService extends GenericService<FrameworkProperty> implements IFrameWorkPropertyService {

	@Autowired
	IFrameWorkPropertyDao iFrameWorkPropertyDao;

	@Override
	protected IGenericRepository<FrameworkProperty> getGenericRepository() {
		// TODO Auto-generated method stub
		return this.iFrameWorkPropertyDao;
	}

}
