package com.rbp.core.service.base;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;

import java.util.List;
import java.util.Stack;

public interface IGenericRootService<T> extends IGenericTreeService {

    List<BaseEntity> getChildrenByParentId(Long parentId);
    List<BaseEntity> getParentsByChildId(Long childId);
    List<T> getChildrenByRootId(Long rootId);
    List<T> getChildrenByParentIdJunction(Long parentId);
    List<T> getJunctionByChildId(Long childId);
    void deleteNode(Long nodeId, Boolean deleteAllData, Long JId,Integer levelId);
    void getNodeChildren(Long nodeId, Long JId, Integer levelId, Stack<RootBaseEntity> listOfNodes);
    void delete(Long id);
    RootBaseEntity getByIdInJunction(Long id);
}
