package com.rbp.core.service.base.impl;

import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.core.model.dto.base.FilterJsonInput;
import com.rbp.core.service.base.IGenericService;
import com.rbp.core.service.base.IGenericTreeService;
import com.rbp.core.utility.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public abstract class GenericTreeService<T extends TreeBaseEntity>
        extends GenericService<T> implements IGenericTreeService {
    @Autowired
    protected abstract IGenericTreeRepository<T> getGenericTreeRepository();

    @Autowired
    protected IGenericService<T> genericService;

    @Override
    @Transactional
    public void deleteNode(Long id,Boolean deleteAllData) {

        List<BaseEntity> treeRecord =  getChildrenByParentId(id);

            if (treeRecord.size() == 0 || (treeRecord.size() != 0 && deleteAllData==true)) {
                deleteById(id);
            } else {
                throw new ApplicationException(0, "گره مورد نظر دارای زیر مجموعه است، ابتدا زیرمجموعه حذف گردد!");
            }
        }


    @Override
    @Transactional
    public List<BaseEntity> getChildrenByParentId(Long parentId) {
        return getTreeChildren(parentId);
    }


    List<BaseEntity> getTreeChildren(Long parentId) {
        FilterJsonInput filter=filterFactory();
        filter.where("parent.id="+parentId);
        List<T> treeRecord = getGenericTreeRepository().getChildrenByParentId(filter);
        return getSpecificFieldValue(treeRecord, T::getChild);
    }

    @Override
    @Transactional
    public List<BaseEntity> getParentsByChildId(Long childId) {
        return getTreeParent(childId);
    }

    List<BaseEntity> getTreeParent(Long childId) {
        FilterJsonInput filter=filterFactory();
        filter.where("child.id="+childId);
        List<T> treeRecord = getGenericTreeRepository().getParentsByChildId(filter);
        return getSpecificFieldValue(treeRecord, T::getParent);
    }

    private List<BaseEntity> getSpecificFieldValue(List<T> treeRecord, Function<T, BaseEntity> mapper) {
        List<BaseEntity> records = new ArrayList<>();
        if (treeRecord != null && treeRecord.size() != 0) {
            records = treeRecord
                    .stream()
                    .map(mapper)
                    .collect(Collectors.toList());
        }
        return records;
    }
}

