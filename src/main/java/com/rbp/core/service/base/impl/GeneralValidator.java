package com.rbp.core.service.base.impl;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

//@TODO: ALl of this error codes and messages need to be implemented inside REST API Advisor.

/**
 * Validation Errors are in range of 4000 -> for example 4001
 * IF anything invalid is encountered, throw new Application Exception with error code and message.
 * This class Does not comply with GenericService/Validator. Consider these a few Utilities.
 */
@Service
public class GeneralValidator {

    private static boolean Validate(String s, String PATTERN) {
        if ((s instanceof String)) {
            String value = ((String) s).trim();
            if (!value.matches(PATTERN) && !value.isEmpty()) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if String is null or blank
     *
     * @param s
     * @throws com.rbp.core.utility.ApplicationException
     */
    public static void validateString(String s) throws com.rbp.core.utility.ApplicationException {
        if (s.trim() == "" || s == null) throw new com.rbp.core.utility.ApplicationException(4001, "String null/blank");
    }

    /**
     * Validate if String is Null/Blank/Obeys the given @NotNull Pattern
     *
     * @param s
     * @param regex
     * @throws com.rbp.core.utility.ApplicationException
     */
    public static void validateStringWithRegex(String s, String regex) throws com.rbp.core.utility.ApplicationException {
        if (s.trim() == "" || s == null)
            throw new com.rbp.core.utility.ApplicationException(4001, "String null/blank");
        if (!(Pattern.matches(regex, s)))
            throw new com.rbp.core.utility.ApplicationException(4002, "String invalid with this given regex");
    }

    /**
     * Gets a string and a list of regex. Checks null/empty/ list of regex
     *
     * @param s
     * @param regex
     * @throws com.rbp.core.utility.ApplicationException
     */
    public static void validateStringWithRegex(String s, List<String> regex) throws com.rbp.core.utility.ApplicationException {
        if (s.trim() == "" || s == null)
            throw new com.rbp.core.utility.ApplicationException(4001, "String null/blank");
        for (String pattern : regex) {
            if (!Validate(s, pattern))
                throw new com.rbp.core.utility.ApplicationException(4002, "String invalid with this given regex");
        }
    }

    //@TODO: what makes a date valid?

    /**
     * This method has no implementation yet!
     *
     * @param d
     */
    public static void validateDate(Date d) {

    }

    /**
     * validate a number to be between the bound specified.
     *
     * @param number
     * @param lowbound
     * @param highbound
     * @throws com.rbp.core.utility.ApplicationException
     */
    public static void validateNumberRange(int number, int lowbound, int highbound) throws com.rbp.core.utility.ApplicationException {
        if (!(lowbound < number && number < highbound))
            throw new com.rbp.core.utility.ApplicationException(4003, "Number not in range");
    }

    /**
     * validate a number to be between the bound specified.
     *
     * @param number
     * @param lowbound
     * @param highbound
     * @throws com.rbp.core.utility.ApplicationException
     */
    public static void validateNumberRange(float number, float lowbound, float highbound) {
        if (!(lowbound < number && number < highbound))
            throw new com.rbp.core.utility.ApplicationException(4004, "Number not in range");
    }

    /**
     * Validates a string national code. Checks:
     * Empty, null, correct format and validity (based on a math formula. formula is available on the web)
     *
     * @param input National code in String format
     * @throws com.rbp.core.utility.ApplicationException
     */
    public static void validateNationalcode(String input) throws com.rbp.core.utility.ApplicationException {
        if (input.trim() == "" || input == null)
            throw new com.rbp.core.utility.ApplicationException(4005, "Null/Blank National Code");
        if (!input.matches("^\\d{10}$"))
            throw new com.rbp.core.utility.ApplicationException(4006, "Invalid National Code format");

        int check = Integer.parseInt(input.substring(9, 10));

        int sum = IntStream.range(0, 9)
                .map(x -> Integer.parseInt(input.substring(x, x + 1)) * (10 - x))
                .sum() % 11;

        if (!(sum < 2 && check == sum) || (sum >= 2 && check + sum == 11))
            throw new com.rbp.core.utility.ApplicationException(4007, "Invalid National Code");

    }

    /**
     * Validate the Cellphone number based on regex.
     * It will trim the number before validation
     *
     * @param cell Cell number in String format
     * @throws com.rbp.core.utility.ApplicationException
     */
    public static void validateCellphone(String cell) throws com.rbp.core.utility.ApplicationException {
        String cellregex = "^(0|\\+98)?([ ]|,|-|[()]){0,2}9[0|1|2|3|4]([ ]|,|-|[()]){0,3}(?:[0-9]([ ]|,|-|[()]){0,2}){8}$";
        Pattern p = Pattern.compile(cellregex);
        if (!(Pattern.matches(cellregex, cell.trim())))
            throw new com.rbp.core.utility.ApplicationException(4008, "Invalid Cellphone format");
    }

    /**
     * String is all numbers
     *
     * @param s
     */
    public static void numbersOnly(String s) throws com.rbp.core.utility.ApplicationException {
        final String PATTERN = "^[0-9\\u06F0-\\u06F9\\u0660-\\u0669]+$";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4009, "String is not all numbers.");
    }

    /**
     * String is All english and it CAN NOT have symbols.
     *
     * @param s
     */
    public static void englishOnly(String s) throws com.rbp.core.utility.ApplicationException {
        final String PATTERN = "^[a-zA-Z0-9 ]+$";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4010, "String is not all English(no symbol allowed)");
    }

    /**
     * String is all English, it can have symbols.
     *
     * @param s
     * @return
     */
    public static void englishOnly2(String s) throws com.rbp.core.utility.ApplicationException {
        final String PATTERN = "^[a-zA-Z0-9~@#\\^\\$&\\*\\(\\)-_\\+=\\[\\]\\{\\}\\|\\\\,\\.\\?\\s]+$";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4011, "String is not all English (symbol allowed)");
    }

    /**
     * String is all Persian, Can not have symbols just . or ? is allowed!
     *
     * @param s
     * @return
     * @TODO:Add other symbols if needed.
     */
    public static void persianOnly(String s) throws com.rbp.core.utility.ApplicationException {
        final String PATTERN = "^[\\u0600-\\u06FF ]+$";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4012, "String is not all Persian");
    }

    /**
     * String is all Persian and it accepts blank, Can not have symbols just . and ? is allowed.
     *
     * @param s
     * @return
     */
    public static void persianOnlyAndAcceptEmpty(String s) throws com.rbp.core.utility.ApplicationException {
        final String PATTERN = "^$|^[\\u0600-\\u06FF ]+$";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4013, "String is not all Persian/Blank");
    }

    /**
     * String is float number
     *
     * @param s
     */
    public static void floatNumberOnly(String s) {
        final String PATTERN = "^[0-9]{1,2}(?:\\.[0-9]{1,2})?$";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4014, "Invalid all float");
    }

    /**
     * String is email only
     *
     * @param s
     * @return
     */
    public static void emailOnly(String s) throws com.rbp.core.utility.ApplicationException {
        final String PATTERN = ".+@.+\\.[a-z]+";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4015, "String is not an email");
    }
//------------------->DO NOT DELETE THIS YET! Let me (A. Saravani) do it if needed.<----------------------
//    /**
//     *
//     * @param s
//     * @return
//     */
//    public static boolean englishAndpersianOnly(String s) {
//        final String PATTERN = "^[a-zA-Z0-9~@#\\^\\$&\\*\\(\\)-_\\+=\\[\\]\\{\\}\\|\\\\,\\.\\?\\s\\u0600-\\u06FF ]+$";
//        return Validate(s, PATTERN);
//
//    }
//--------------------------------------------------------------------------------------------------------
    /**
     * String is a date
     *
     * @param s
     * @return
     * @TODO: maybe we need a more comprehensive REGEX here!
     */
    public static void dateOnly(String s) {
        final String PATTERN = "[1][3-4][0-9][0-9][0-1][0-9][0-3][0-9]";
        if (!Validate(s, PATTERN))
            throw new com.rbp.core.utility.ApplicationException(4016, "String is not a date");
    }
}
