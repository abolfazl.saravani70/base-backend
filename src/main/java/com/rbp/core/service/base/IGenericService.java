package com.rbp.core.service.base;
/**
 * @author Alireza Souhani 1398.02.01
 */

import com.rbp.core.model.domainmodel.gridStructure.request.EnterpriseGetRowsRequest;
import com.rbp.core.model.domainmodel.gridStructure.response.EnterpriseGetRowsResponse;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.dto.base.FilterJsonInput;

public interface IGenericService<T> {

    T save(T entity);

    AdvancedSearchResult<T> getList(FilterJsonInput filter);

    T loadById(Long id);

    void deleteById(Long id);

    void update(T entry);

    Long softDeleteById(Long entityId);

    Long tempDeleteById(Long entityId);

    T getReference(Long id);

    AdvancedSearchResult<T> searchEntity(FilterJsonInput filter, Boolean isTempDeleted);
    public AdvancedSearchResult searchView(Class viewClass,FilterJsonInput filter, Boolean isTempDeleted);

    AdvancedSearchResult<T> searchCoding(FilterJsonInput filter);

    String getCurrentDateTime();

    EnterpriseGetRowsResponse getDataGrid(EnterpriseGetRowsRequest request);
}
