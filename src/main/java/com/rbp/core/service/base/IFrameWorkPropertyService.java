package com.rbp.core.service.base;
/**
 * @author Alireza Souhani 1398.02.01
 */
import com.rbp.core.model.domainmodel.base.FrameworkProperty;

public interface IFrameWorkPropertyService extends IGenericService<FrameworkProperty> {

}

