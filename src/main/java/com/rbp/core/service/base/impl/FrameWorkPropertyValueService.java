package com.rbp.core.service.base.impl;
/**
 * @author Alireza Souhani 1398.02.01
 */

import com.rbp.core.model.dao.base.IFrameWorkPropertyValueDao;
import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
import com.rbp.core.model.dto.base.FrameworkPropertyValueDTO;
import com.rbp.core.service.base.IFrameWorkPropertyValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FrameWorkPropertyValueService extends GenericService<FrameworkPropertyValue> implements IFrameWorkPropertyValueService {

    @Autowired
    IFrameWorkPropertyValueDao iFrameWorkPropertyValueDao;

    @Override
    protected IGenericRepository<FrameworkPropertyValue> getGenericRepository() {
        // TODO Auto-generated method stub
        return this.iFrameWorkPropertyValueDao;
    }

    @Transactional
    @Override
    public List<FrameworkPropertyValue> getByPropertyId(Long PropId) {
        return iFrameWorkPropertyValueDao.getByPropertyId(PropId);
    }

    @Override
    public FrameworkPropertyValueDTO getByPropertyIdAndKeyCode(Long PropId, Long KeyCode) {
        return iFrameWorkPropertyValueDao.getByPropertyIdAndKeyCode(PropId, KeyCode);
    }

    @Override
    public List<FrameworkPropertyValueDTO> getByPropertyKeyCode(Long KeyCode) {
        return iFrameWorkPropertyValueDao.getByPropertyKeyCode(KeyCode);
    }


}
