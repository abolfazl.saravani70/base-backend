package com.rbp.core.service.base;


/**
 * This interface is used to create Validators that produce EXCEPTIONS!
 * They are not advised to be used coupled with UI. Exceptions are better off with REST apis
 * Add Any Exception you create to the REST Advisor class.
 * T the arbitrary type to be validated and E is the exception which is inherited from the General exception class.
 */

public interface IValidator<T, ApplicationException> {
    public void validate(Object o);

    public boolean supports(Class<T> clazz);

}
