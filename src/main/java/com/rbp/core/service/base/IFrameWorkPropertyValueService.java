package com.rbp.core.service.base;
/**
 * @author Alireza Souhani 1398.02.01
 */


import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
import com.rbp.core.model.dto.base.FrameworkPropertyValueDTO;

import java.util.List;

public interface IFrameWorkPropertyValueService extends IGenericService<FrameworkPropertyValue> {
	public List<FrameworkPropertyValue> getByPropertyId(Long PropId);
	public FrameworkPropertyValueDTO getByPropertyIdAndKeyCode(Long PropId, Long KeyCode);
	public List<FrameworkPropertyValueDTO> getByPropertyKeyCode(Long KeyCode);
}
