package com.rbp.core.service.security;

import com.bahman.securitycommon.model.security.UsersModel;

public interface SecurityService {

    String getCurrentUsername();

    UsersModel getCurrentUser();

    UsersModel getUserByUserId(String userId);

}
