/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.service.security.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.security.IUserOldPasswordDao;
import com.rbp.core.model.domainmodel.security.UserOldPassword;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.security.IUserOldPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserOldPasswordService extends GenericService<UserOldPassword> implements IUserOldPasswordService {
    @Autowired
    IUserOldPasswordDao iUserOldPasswordDao;
    @Override
    protected IGenericRepository<UserOldPassword> getGenericRepository() {
        return iUserOldPasswordDao;
    }
}
