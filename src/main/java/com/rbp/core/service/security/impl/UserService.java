package com.rbp.core.service.security.impl;
/**
 * @author Alireza Souhani 1398.02.01
 */

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.security.IUserDao;
import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.security.IGroupService;
import com.rbp.core.service.security.IUserService;
import com.rbp.sayban.service.organization.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//*******************************************************
//*******************************************************
//I have Commented ZoneCode! Add it back if REQUIRED!!!!
//*******************************************************
//*******************************************************


@Service
public class UserService extends GenericService<User> implements IUserService {

    @Autowired
    private IUserDao iUserDao;
    @Autowired
    private IPersonService iPersonService;
    @Autowired
    private IStakeholderService iStakeholderService;
    @Autowired
    private IOrganizationUnitService iOrganizationUnitService;
    @Autowired
    private IOrganizationChartService iOrganizationChartService;
    @Autowired
    private IOrganizationService iOrganizationService;
    @Autowired
    private IOrgTreeService iOrgTreeService;
    @Autowired
    private IGroupService iGroupService;

    @Override
    protected IGenericRepository<User> getGenericRepository() {

        return iUserDao;
    }

//    @Transactional
//    @Override
//    public Organization getOrganizationByUser(Long userId) {
//        OrganizationUnit organizationUnit = getOrganizationUnitByUser(userId);
//        Organization organization = organizationUnit.getOrganization();
//        return organization;
//    }
//
//    private OrganizationChart getOrganizationChartByUser(Long userId) {
//        OrganizationUnit organizationUnit = getOrganizationUnitByUser(userId);
//        OrganizationChart organizationChart = organizationUnit.getOrganizationChart();
//        return organizationChart;
//    }
//
//    private OrganizationUnit getOrganizationUnitByUser(Long userId) {
//        User u = iUserDao.loadById(userId);
//        if (u == null)
//            throw new ApplicationException(0, "کاربری با چنین مشخصاتی وجود ندارد.");
//        Hibernate.initialize(Stakeholder.class);
//        Stakeholder st = u.getStakeholder();
//        Person p = iPersonService.searchEntity("stakeholder.id=" + st.getId(), "", 1, 1).get(0);
//        OrganizationUnit organizationUnit = p.getOrganizationUnit();
//        return organizationUnit;
//    }
//
//    @Transactional
//    @Override
//    public List<User> getUsersSameOrganizationChartAndOrganization(Long userId) {
//        Organization organization = getOrganizationByUser(userId);
//        OrganizationChart organizationChart = getOrganizationChartByUser(userId);
//        List<OrganizationUnit> organizationUnits=getOrganizationUnitsByOrganizationChartAndOrganization
//                (organizationChart,organization);
//        List<User> users = getUsersByOrganizationUnits(organizationUnits);
//        users.remove(loadById(userId));
//        return users;
//    }
//
//    @Transactional
//    @Override
//    public List<User> getUsersSameOrganization(Long userId) {
//        Organization organization = getOrganizationByUser(userId);
//        List<OrganizationUnit> organizationUnits = getOrganizationUnitsByOrganization(organization);
//        List<User> users = getUsersByOrganizationUnits(organizationUnits);
//        users.remove(loadById(userId));
//        return users;
//    }
//
//    @Transactional
//    @Override
//    public List<User> getUsersLowerDownOrganizations(Long userId) {
//        Organization organization = getOrganizationByUser(userId);
//        List<BaseEntity> organizationChildren = iOrgTreeService.getChildrenByParentId(organization.getId());
//        if (organizationChildren.size() == 0)
//            throw new ApplicationException(0, "کاربر مورد نظر در رده ای قرار دارد که فاقد زیر رده است در نتیجه فاقد کاربران زیر رده است");
//        else {
//            List<User> users = new ArrayList<>();
//            for (BaseEntity orgChild : organizationChildren) {
//                List<OrganizationUnit> organizationUnits=getOrganizationUnitsByOrganization((Organization) orgChild);
//                List<User> usersByOrganization = getUsersByOrganizationUnits(organizationUnits);
//                users.addAll(usersByOrganization);
//            }
//            return users;
//        }
//    }
//
//    @Override
//    public List<User> getUsersHigherOrganization(Long userId) {
//        return null;
//    }
//
////    @Transactional
////    @Override
////    public List<User> getUsersLowerDownLevelINOrganizationChart(Long userId) {
////        Organization organization = getOrganizationByUser(userId);
////        OrganizationChart organizationChart=getOrganizationChartByUser(userId);
////        List<OrganizationChart> orgChartChildren = iOrganizationChartService;
////        if (organizationChildren.size() == 0)
////            throw new ApplicationException(0, "کاربر مورد نظر در رده ای قرار دارد که فاقد زیر رده است در نتیجه فاقد کاربران زیر رده است");
////        else {
////            List<User> users = new ArrayList<>();
////            for (Organization orgChild : organizationChildren) {
////                List<OrganizationUnit> organizationUnits=getOrganizationUnitsByOrganization(orgChild);
////                List<User> usersByOrganization = getUsersByOrganizationUnits(organizationUnits);
////                users.addAll(usersByOrganization);
////            }
////            return users;
////        }
////    }
//
////    @Transactional
////    @Override
////    public List<User> getUsersHigherOrganization(Long userId) {
////        Organization organization = getOrganizationByUser(userId);
////        List<Organization> orgParent = iOrgTreeService.getParentsByChildId(organization.getId());
////        if (orgParent == null)
////            throw new ApplicationException(0, "کاربر مورد نظر در بالاترین رده قرار دارد در نتیجه فاقد کاربرانی در رده بالاتر است");
////        else {
////            List<OrganizationUnit> organizationUnits=getOrganizationUnitsByOrganization(orgParent.get(0));
////            List<User> users = getUsersByOrganizationUnits(organizationUnits);
////            return users;
////        }
////    }
//
//    private List<OrganizationUnit> getOrganizationUnitsByOrganization(Organization organization) {
//        List<OrganizationUnit> organizationUnits = iOrganizationUnitService
//                .searchEntity("organization.id=" + organization.getId()
//                        , "", 1, 0);
//        return organizationUnits;
//    }
//
//    private List<OrganizationUnit> getOrganizationUnitsByOrganizationChartAndOrganization
//            (OrganizationChart organizationChart, Organization organization) {
//        List<OrganizationUnit> organizationUnits = new ArrayList<>();
//        OrganizationChart parent = organizationChart.getParent();
//        List<OrganizationChart> children = (List<OrganizationChart>) parent.getChildren();
//        for (OrganizationChart child : children) {
//            organizationUnits = iOrganizationUnitService
//                    .searchEntity("organization.id=" + organization.getId()
//                                    + "$and$organizationChart.id=" + child.getId()
//                            , "", 1, 0);
//        }
//        return organizationUnits;
//    }
//
//    private List<User> getUsersByOrganizationUnits(List<OrganizationUnit> organizationUnits) {
//        List<User> users = new ArrayList<>();
//        for (OrganizationUnit organizationUnit : organizationUnits) {
//            List<Person> persons = iPersonService
//                    .searchEntity("organizationUnit.id=" + organizationUnit.getId()
//                            , "", 1, 1);
//            if (persons.size() != 0) {
//                Stakeholder stakeholder = persons.get(0).getStakeholder();
//                User user = stakeholder.getUser();
//                users.add(user);
//            }
//        }
//        return users;
//    }
//
//    @Transactional
//    @Override
//    public UserManagementGridViewModel getUserManagementGridData(int pageNumber, int pageSize) {
//        List<User> users = iUserDao.getList(pageNumber, pageSize);
//        List<Long> orgIds = new ArrayList<>();
//        List<Long> orgUnitIds = new ArrayList<>();
//        List<String> orgUnitNames = new ArrayList<>();
//        List<String> organizationNames = users.stream().map(x -> {
//            Stakeholder st = x.getStakeholder();
//            Person p = iPersonService.searchEntity("stakeholder.id=" + st.getId(), "", 1, 1).get(0);
//            OrganizationUnit organizationUnit = p.getOrganizationUnit();
//            orgUnitIds.add(organizationUnit.getId());
//            orgUnitNames.add(organizationUnit.getTitle());
//            Organization organization = organizationUnit.getOrganization();
//            orgIds.add(organization.getId());
//            return organization.getName();
//        }).collect(Collectors.toList());
//
//        UserManagementGridViewModel userManagementGridViewModel = new UserManagementGridViewModel();
//        for (int i = 0; i < users.size(); i++) {
//            Long userId = users.get(i).getId();
//            String username = users.get(i).getUsername();
//            String firstname = users.get(i).getFirstName();
//            String lastname = users.get(i).getLastName();
//            Boolean isActive = users.get(i).getIsActive();
//            userManagementGridViewModel.getList()
//                    .add(new UserOrganizationViewModel(organizationNames.get(i), orgIds.get(i), orgUnitIds.get(i), orgUnitNames.get(i), userId, username, firstname, lastname, isActive));
//        }
//        return userManagementGridViewModel;
//
//    }
//
//    @Override
//    @Transactional
//    public User loadByUsername(String username) {
//        return iUserDao.getUserByUsername(username);
//    }
//
//    @Override
//    public void deleteById(Long id) {
//        if (!iUserDao.loadById(id).getIsOnline())
//            iUserDao.deleteById(id);
//        else
//            throw new ApplicationException(2004, "کاربر آنلاین است. امکان پاک کردن کاربر وجود ندارد.");
//    }
//
//    /**
//     * Register a user, all the required information is inside UserMamangementRegisterViewModel
//     *
//     * @param UMR
//     * @return
//     */
//    @Transactional
//    public User registerUser(UserManagementRegisterViewModel UMR) {
//        String username = UMR.getUserName();
//        if (iUserDao.getUserByUsername(username) == null) {
//            //Create Stake Holder
//            Stakeholder st = new Stakeholder();
//            st.setId(0L);
//            st.setVersion(1L);
//            st.setIsPerson(true);
//            st = iStakeholderService.save(st);
//
//            //Create Person
//            Person p = new Person();
//            p.setId(0L);
//            p.setVersion(1L);
//            p.setNationalId(UMR.getNationalId());
//            OrganizationUnit organizationUnit = iOrganizationUnitService.loadById(UMR.getOrgId());
//            p.setOrganizationUnit(organizationUnit);
//            p.setStakeholder(st);
//            p.setBirthDate(UMR.getBirthDay());
//            p = iPersonService.save(p);
//            //Update OrganizationUni and set Assigned to True
//            organizationUnit.setisAssigned(true);
//            iOrganizationUnitService.update(organizationUnit);
//            //Create Blob
//            //	Blob b=iBlobService.save(UMR.getBlob());
//            //Create User
//            User u = new User();
//            HashSet<Group> t = new HashSet<>();
//            u.setGroups(t);
//            u.setId(0L);
//            u.setVersion(1L);
//            u.setStakeholder(st);
//            u.setUsername(UMR.getUserName());
//            u.setFirstName(UMR.getFirstName());
//            u.setLastName(UMR.getLastName());
//            u.setPassword(UMR.getPassword());
//            u.setEmail(UMR.getEmail());
//            u.setExpireDate(UMR.getExpireDate());
//            u.setIsActive(UMR.getisActive());
//            u.setDescription(UMR.getDescription());
//            Set<Long> groups = UMR.getGroupList();
//            User finalU = u;
//            //The finalU is required since the variable inside lambda should be final/effectively final
//            groups.parallelStream().forEach(x -> {
//                finalU.getGroups().add(iGroupService.getReference(x));
//            });
//            u = iUserDao.save(finalU);
//
//            return u;
//        } else throw new ApplicationException(1, "کاربری با این نام کاربری موجود است.");
//    }
//
//    @Transactional
//    public UserManagementRegisterViewModel showUserOrganizationInfo(Long userId) {
//        User u = iUserDao.loadById(userId);
//        Set<Group> groups = u.getGroups();
//        Person p = iPersonService.searchEntity("stakeholder.id=" + u.getStakeholder().getId()
//                , "", 1, 1).get(0);
//        OrganizationUnit unit = p.getOrganizationUnit();
//        Organization org = unit.getOrganization();
//        UserManagementRegisterViewModel view = new UserManagementRegisterViewModel();
//
//        //User
//        view.setUserId(u.getId());
//        view.setUserName(u.getUsername());
//        view.setFirstName(u.getFirstName());
//        view.setLastName(u.getLastName());
//        view.setEmail(u.getEmail());
//        view.setExpireDate(u.getExpireDate());
//        view.setTell(u.getTell());
//        view.setMobile(u.getMobile());
//        view.setKowsarNumber(u.getKowsarNumber());
//        view.setisActive(u.getIsActive());
//        view.setDescription(u.getDescription());
//        //Person
//        view.setNationalId(p.getNationalId());
//        view.setBirthDay(p.getBirthDate());
//        //Group
//        groups.parallelStream().forEach(x -> {
//                    view.getGroupList().add(x.getId());
//                    view.getGroupNames().add(x.getPersianName());
//                }
//        );
//        //Organization
//        view.setOrgId(org.getId());
//        view.setOrgName(org.getName());
//
//        //OrganizationUnit
//        view.setOrgUnitId(unit.getId());
//        view.setOrgUnitTitle(unit.getTitle());
//        //Blob
//
//        return view;
//    }
//
//    @Transactional
//    public Long setLastLoginDateandIP(User entity) {
//        User user = iUserDao.loadById(entity.getId());
//        user.setLastLogin(entity.getLastLogin());
//        user.setLastIp(entity.getLastIp());
//
//        return super.save(user).getId();
//    }
//
//    @Transactional
//    public Boolean generalChangePassword(Long id, String newPassword) {
//        iUserDao.updatePassowrd(id, newPassword);
//        return true;
//    }
//
//    @Transactional
//    public Boolean resetPassword(User entity) {
//        generalChangePassword(entity.getId(), "123");//TODO: replace this with a password generator or sth
//        return true;
//    }
//
//    @Override
//    @Transactional
//    public User loadAuthenticated() {
//        User authenticatedUser = SecurityUtility.getAuthenticatedUser();
//        return iUserDao.loadById(authenticatedUser.getId());
//    }
//
//    @Override
//    @Transactional
//    //TODO: All the validation must be done as a whole then returned accordingly.
//    public ActionResult update(UserManagementRegisterViewModel view) {
//        User u = iUserDao.loadById(view.getUserId());
//
//        //User checks
//        User temp = iUserDao.getUserByUsername(view.getUserName());
//        if (temp != null) {
//            if (!temp.getId().equals(u.getId()))
//                throw new ApplicationException(1, "این نام کاربری قبلا انتخاب شده است.");
//        }
//
//        //User Updates
//        u.setUsername(view.getUserName());
//        u.setFirstName(view.getFirstName());
//        u.setLastName(view.getLastName());
//        u.setEmail(view.getEmail());
//        u.setExpireDate(view.getExpireDate());
//        u.setTell(view.getTell());
//        u.setMobile(view.getMobile());
//        u.setKowsarNumber(view.getKowsarNumber());
//        u.setIsActive(view.getisActive());
//
//
//        //Group
//        if (view.getGroupList().size() == 0) throw new ApplicationException(1, "حداقل یک گروه باید انتخاب شود.");
//        Set<Group> grouplst = view.getGroupList().stream().map(x -> iGroupService.loadById(x)).collect(Collectors.toSet());
//        u.setGroups(grouplst);
//
//
//        Person p = iPersonService.searchEntity("stakeholder.id=" + u.getStakeholder().getId(),
//                "", 1, 1).get(0);
//        p.setNationalId(view.getNationalId());
//        p.setBirthDate(view.getBirthDay());
//        p.setOrganizationUnit(iOrganizationUnitService.loadById(view.getOrgUnitId()));
//
//        iUserDao.save(u);
//        iPersonService.save(p);
//        return new ActionResult(null, 0, 0, 0, 0, "بروز رسانی با موفقیت انجام شد.", "");
//    }
//
//    @Override
//    @Transactional
//    public void setUserOnline(Long userId) {
//        changeOnlineStatus(userId, true);
//
//    }
//
//    @Override
//    @Transactional
//    public void setUserOffline(Long userId) {
//        changeOnlineStatus(userId, false);
//
//    }
//
//    @Transactional
//    public void changeOnlineStatus(Long userId, Boolean isOnline) {
//        User user = iUserDao.loadById(userId);
//        user.setIsOnline(isOnline);
//        iUserDao.save(user);
//    }
//
//    @Override
//    public Integer getOnlineUsersCount() {
//        return SecurityUtility.getOnlineUserCount();
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public Long saveGroup(User entity) {
//        return iUserDao.saveGroup(entity);
//    }
//
//    @Override
//    public User getCurrentUser() {
//        return SecurityUtility.getAuthenticatedUser();
//    }
//
//    @Override
//    public List<Group> getUserGroups(Long userId) {
//        return iUserDao.getUserGroups(userId);
//    }
//
//    @Override
//    public List<UserGroupPermissionDTO> getUserPermission(Long userId) {
//        return iUserDao.getUserPermission(userId);
//    }
//
//    @Override
//    @Transactional
//    public List<User> getUsers() {
//        return this.iUserDao.getUsers();
//    }
//
//    @Override
//    @Transactional
//    public void setStatus(Long userId, Boolean status) {
//        User user = iUserDao.loadById(userId);
//        user.setIsActive(status);
//        iUserDao.update(user);
//
//    }
//
//    @Override
//    @Transactional
//    public List<User> getUserById(Long userId) {
//        return this.iUserDao.getUserById(userId);
//    }
//
//    @Override
//    @Transactional
//    public List<User> getUserPost(Long userId) {
//        return this.iUserDao.getUserPost(userId);
//    }
//
//    @Override
//    public List<OrganizationPositionViewModel> getUserPostList(Long userId) {
//        return null;
//    }
//
//
//    @Override
//    public User lightLoadById(Long id) {
//        return iUserDao.lightLoadById(id);
//    }
//
//    @Override
//    public List<BigDecimal> getUserCount() {
//        return this.iUserDao.getUserCount();
//    }
//
//    @Override
//    public Integer getOnlineUserCount() {
//        return this.iUserDao.getOnlineUserCount();
//    }
//
//    @Override
//    public Integer getStateUserCount(String stateCode) {
//        return this.iUserDao.getStateUserCount(stateCode);
//    }
//
//    @Override
//    public Integer getStateOnlineUserCount(String stateCode) {
//        return this.iUserDao.getStateOnlineUserCount(stateCode);
//    }
//
//    @Override
//    public Integer getCityUserCount(String cityCode) {
//        return this.iUserDao.getCityUserCount(cityCode);
//    }
//
//    @Override
//    public Integer getCityOnlineUserCount(String cityCode) {
//        return this.iUserDao.getCityOnlineUserCount(cityCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getUserPerStateStatusCount() {
//        return this.iUserDao.getUserPerStateStatusCount();
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getStateUserPerCityCount(String stateCode) {
//        return this.iUserDao.getStateUserPerCityCount(stateCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getStateOnlineUserPerCityCount(String stateCode) {
//        return this.iUserDao.getStateOnlineUserPerCityCount(stateCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getOnlineUserPerStateCount() {
//        return this.iUserDao.getOnlineUserPerstateCount();
//    }
//
//    @Override
//    public Integer getVillageUserCount(String villageCode) {
//        return this.iUserDao.getVillageUserCount(villageCode);
//    }
//
//    @Override
//    public Integer getVillageOnlineUserCount(String villageCode) {
//        return this.iUserDao.getVillageOnlineUserCount(villageCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getCityPerVillageUserCount(String cityCode) {
//        return this.iUserDao.getCityPerVillageUserCount(cityCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getCityPerVillageOnlineUserCount(String cityCode) {
//        return this.iUserDao.getCityPerVillageOnlineUserCount(cityCode);
//    }
//
//    @Override
//    public Integer getZoneUserCount(String zoneCode) {
//        return this.iUserDao.getZoneUserCount(zoneCode);
//    }
//
//    @Override
//    public Integer getZoneOnlineUserCount(String zoneCode) {
//        return this.iUserDao.getZoneOnlineUserCount(zoneCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskStateUserCount(String stateCode) {
//        return this.iUserDao.getServiceDeskStateUserCount(stateCode);
//    }
//
//    @Override
//    public Integer getServiceDeskStateOnlineUserCount(String stateCode) {
//        return this.iUserDao.getServiceDeskStateOnlineUserCount(stateCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskCityUserCount(String stateCode) {
//        return this.iUserDao.getServiceDeskCityUserCount(stateCode);
//    }
//
//    @Override
//    public Integer getServiceDeskCityOnlineUserCount(String cityCode) {
//        return this.iUserDao.getServiceDeskCityOnlineUserCount(cityCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskVillageUserCount(String cityCode) {
//        return this.iUserDao.getServiceDeskVillageUserCount(cityCode);
//    }
//
//    @Override
//    public Integer getServiceDeskVillageZoneOnlineUserCount(String villageCode) {
//        return this.iUserDao.getServiceDeskVillageZoneOnlineUserCount(villageCode);
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskZoneUserCount(String stateCode) {
//        return this.iUserDao.getServiceDeskZoneUserCount(stateCode);
//    }
//
//    @Override
//    public Integer getServiceDeskZoneOnlineUserCount(String zoneCode) {
//        return this.iUserDao.getServiceDeskZoneOnlineUserCount(zoneCode);
//    }


}
