//package com.rbp.core.service.security;
//
///**
// * @author Alireza Souhani 1398.02.01
// */
//import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
//import com.rbp.core.model.domainmodel.security.OrganizationPosition;
//import com.rbp.core.service.base.IGenericService;
//
//import java.util.List;
//
//public interface IOrganizationPositionService extends IGenericService<OrganizationPosition> {
//
//	public List<FrameworkPropertyValue> getOrganizationUnit();
//
//	public List<OrganizationPosition> getOrganizationPost();
//
//	public List<OrganizationPosition> getOrganizationGroup(Long postId);
//
//}
