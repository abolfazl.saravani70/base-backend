//package com.rbp.core.service.security.impl;
//
//import com.rbp.core.model.dao.base.IGenericRepository;
//import com.rbp.core.model.dao.security.IOrganizationPositionDao;
//import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
//import com.rbp.core.model.domainmodel.security.OrganizationPosition;
//import com.rbp.core.service.base.impl.GenericService;
//import com.rbp.core.service.security.IOrganizationPositionService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
///**
// * @author Alireza Souhani 1398.02.01
// */
//import javax.transaction.Transactional;
//import java.util.List;
//
//@Service
//public class OrganizationPositionService extends GenericService<OrganizationPosition> implements IOrganizationPositionService {
//
//	@Autowired
//	private IOrganizationPositionDao iOrganizationPositionDao;
//	@Override
//	public List<FrameworkPropertyValue> getOrganizationUnit() {
//		// TODO Auto-generated method stub
//		return this.iOrganizationPositionDao.getOrganizationUnit();
//	}
//
//	@Override
//	@Transactional
//	protected IGenericRepository<OrganizationPosition> getGenericRepository() {
//		return this.iOrganizationPositionDao;
//	}
//
//	@Override
//	@Transactional
//	public List<OrganizationPosition> getOrganizationPost() {
//		return this.iOrganizationPositionDao.getOrganizationPosition();
//	}
//
//	@Override
//	@Transactional
//	public List<OrganizationPosition> getOrganizationGroup(Long postId) {
//		return this.iOrganizationPositionDao.getOrganizationGroup(postId);
//	}
//
//
//
//}
