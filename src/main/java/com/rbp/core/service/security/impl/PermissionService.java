package com.rbp.core.service.security.impl;
/**
 * @author Alireza Souhani 1398.02.01
 */
import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.security.IPermisseionDao;
import com.rbp.core.model.domainmodel.security.Permission;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.security.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionService extends GenericService<Permission> implements IPermissionService {

	@Autowired
	private IPermisseionDao iPermisseionDao;

	@Override
	protected IGenericRepository<Permission> getGenericRepository() {
		return this.iPermisseionDao;
	}

//	@Override
//	@Transactional
//	public List<Permission> getPermissions() {
//		return this.iPermisseionDao.getPermissions();
//	}


}
