package com.rbp.core.service.security;

/**
 * @author Alireza Souhani 1398.02.01
 */

import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.service.base.IGenericService;

public interface IUserService extends IGenericService<User> {

//    User registerUser(UserManagementRegisterViewModel userManagementRegisterViewModel);
//
//    UserManagementRegisterViewModel showUserOrganizationInfo(Long userId);
//
//    User loadByUsername(String username);
//
//    UserManagementGridViewModel getUserManagementGridData(int pageNumber, int pageSize);
//
//    Organization getOrganizationByUser(Long userId);
//
//    User loadAuthenticated();
//
//    ActionResult update(UserManagementRegisterViewModel userManagementRegisterViewModel);
//
//    Long saveGroup(User entity);
//
//    Boolean generalChangePassword(Long id, String newPassword);
//
//    Boolean resetPassword(User entity);
//
//    void setUserOnline(Long userId);
//
//    void setUserOffline(Long userId);
//
//    Integer getOnlineUsersCount();
//
//    Long setLastLoginDateandIP(User entity);
//
//    User getCurrentUser();
//
//    List<Group> getUserGroups(Long userId);
//
//    List<User> getUsersSameOrganization(Long userId);
//
//    List<User> getUsersSameOrganizationChartAndOrganization(Long userId);
//
//    List<User> getUsersLowerDownOrganizations(Long userId);
//
//    List<User> getUsersHigherOrganization(Long userId);
//
//    List<UserGroupPermissionDTO> getUserPermission(Long userId);
//
//    //List<OrganizationPosition> getUserOrganizationPosition(User user);
//
//    List<User> getUsers();
//
//    void setStatus(Long userId, Boolean status);
//
//    List<User> getUserById(Long userId);
//
//    List<User> getUserPost(Long userId);
//
//    List<OrganizationPositionViewModel> getUserPostList(Long userId);
//
//    User lightLoadById(Long id);
//
//    List<BigDecimal> getUserCount();
//
//    Integer getOnlineUserCount();
//
//    Integer getStateUserCount(String stateCode);
//
//    Integer getStateOnlineUserCount(String stateCode);
//
//    Integer getCityUserCount(String cityCode);
//
//    Integer getCityOnlineUserCount(String cityCode);
//
//    List<MonitoringUserDTO> getUserPerStateStatusCount();
//
//    List<MonitoringUserDTO> getStateUserPerCityCount(String stateCode);
//
//    List<MonitoringUserDTO> getStateOnlineUserPerCityCount(String stateCode);
//
//    List<MonitoringUserDTO> getOnlineUserPerStateCount();
//
//    Integer getVillageUserCount(String villageCode);
//
//    Integer getVillageOnlineUserCount(String villageCode);
//
//    List<MonitoringUserDTO> getCityPerVillageUserCount(String cityCode);
//
//    List<MonitoringUserDTO> getCityPerVillageOnlineUserCount(String cityCode);
//
//    Integer getZoneUserCount(String zoneCode);
//
//    Integer getZoneOnlineUserCount(String zoneCode);
//
//    List<MonitoringUserDTO> getServiceDeskStateUserCount(String stateCode);
//
//    Integer getServiceDeskStateOnlineUserCount(String stateCode);
//
//    List<MonitoringUserDTO> getServiceDeskCityUserCount(String stateCode);
//
//    Integer getServiceDeskCityOnlineUserCount(String cityCode);
//
//    List<MonitoringUserDTO> getServiceDeskVillageUserCount(String cityCode);
//
//    Integer getServiceDeskVillageZoneOnlineUserCount(String villageCode);
//
//    List<MonitoringUserDTO> getServiceDeskZoneUserCount(String stateCode);
//
//    Integer getServiceDeskZoneOnlineUserCount(String zoneCode);


}
