package com.rbp.core.service.security.impl;
/**
 * @author Alireza Souhani 1398.02.01
 */

import com.rbp.core.service.security.IUserService;
import com.rbp.core.utility.PersianCalendar;
import org.springframework.beans.factory.annotation.Autowired;

public class FrameworkUserDetailsService {//implements UserDetailsService {

    @Autowired
    private IUserService userService;

    PersianCalendar pc = new PersianCalendar();

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.organization.core.userdetails.UserDetailsService#
     * loadUserByUsername(java.lang.String)
     */

//    @Override
//    @Transactional
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        User userEntity = userService.loadByUsername(username);
//
//        if (userEntity == null)
//            throw new UsernameNotFoundException("USER not found");
//        Set<GrantedAuthority> authorities = new HashSet<>();
//
//        Set<Group> userGroups=userEntity.getGroups();
//        List<String> permissions=new ArrayList();
//
//        userGroups.stream().forEach(x->{
//            x.getPermissions().stream().forEach(y->permissions.add(y.getPermissionName()));
//        });
//
//        permissions.stream().forEach(x->authorities.add(new SimpleGrantedAuthority(x)));
//
//        userEntity.setAuthorities(authorities);
//        userService.setUserOnline(userEntity.getId());
//
//        return userEntity;
//    }
}
