package com.rbp.core.service.security;
/**
 * @author Alireza Souhani 1398.02.01
 */
import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.core.service.base.IGenericService;

public interface IGroupService extends IGenericService<Group> {
	
//	List<Group> getGroupsWithoutPermission();
//
//	List<Group> getChildrenGroups(Long groupId);
//
//	Set<Permission> getPermissions(Long id);
//
//	GroupTreeModel getGroupTree(Long userId);
//
//	GroupTreeModel setChildren(GroupTreeModel groupTreeModel,List<Long> userGroups);
//
//	Group getGroupByIdEager(Long groupId);

}
