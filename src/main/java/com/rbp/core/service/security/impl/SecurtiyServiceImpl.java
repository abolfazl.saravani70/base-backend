package com.rbp.core.service.security.impl;

import com.bahman.securitycommon.model.security.UsersModel;
import com.rbp.core.service.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Service
public class SecurtiyServiceImpl implements SecurityService {

    @Autowired
    OAuth2RestOperations restTemplate;

    @Autowired
    Environment environment;

    @Override
    @Transactional(readOnly = true)
    public String getCurrentUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName().toLowerCase();
    }

    @Override
    @Transactional(readOnly = true)
    public UsersModel getCurrentUser() {
        try {
            String uri = environment.getProperty("authenticationUrl") + "/api/getUserByUserName";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("username", getCurrentUsername());
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            ResponseEntity<UsersModel> responseEntity = restTemplate.postForEntity(uri, request, UsersModel.class);
            if (responseEntity != null)
                return responseEntity.getBody();
            else
                return null;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public UsersModel getUserByUserId(String userId) {
        try {
            String uri = environment.getProperty("authenticationUrl") + "/api/getUserByUserId";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("userId", userId);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            ResponseEntity<UsersModel> responseEntity = restTemplate.postForEntity(uri, request, UsersModel.class);
            if (responseEntity != null)
                return responseEntity.getBody();
            else
                return null;
        } catch (Exception ex) {
            return null;
        }
    }
}
