package com.rbp.core.service.security.impl;
/**
 * @author Alireza Souhani 1398.02.01
 */
import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.security.IGroupDao;
import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.security.IGroupService;
import com.rbp.core.service.security.IUserService;
import com.rbp.sayban.model.mapper.GenericMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupService extends GenericService<Group> implements IGroupService {

	@Autowired
	private IGroupDao iGroupDao;
	@Autowired
	GenericMapperService genericMapperService;
	@Autowired
	IUserService iUserService;
	@Override
	protected IGenericRepository<Group> getGenericRepository() {

		return iGroupDao;
	}

//	@Override
//	@Transactional
//	public List<Group> getGroupsWithoutPermission() {
//		return this.iGroupDao.getGroupsWithoutPermission();
//	}
//
//	/**
//	 * return the result with a bignumber so you can return everything.
//	 * @param groupId
//	 * @return
//	 */
//	@Override
//	public List<Group> getChildrenGroups(Long groupId) {
//		//return this.iGroupDao.getChildrenGroups(groupId);
//		return searchEntity("parentGroup.id="+groupId,"",1,1000);
//	}
//
//	/**
//	 * recursive function to return all of the permission for a node
//	 * @param id
//	 * @return
//	 */
//	@Override
//	@Transactional
//	public Set<Permission> getPermissions(Long id){
//		Set<Permission> result=new HashSet<>();
//		result.addAll(loadById(id).getPermissions());
//		List<Group> children=getChildrenGroups(id);
//		if(children==null) return result;
//
//		children.forEach(x->{
//			result.addAll(x.getPermissions());
//			result.addAll(getPermissions(x.getId()));
//		});
//		return result;
//	}
//
//	/**
//	 * Returns all of the group starting from the Root Group all the way down. DepthFirst Travers of the tree!
//	 * @param userId
//	 * @return
//	 */
//	@Override
//	@Transactional
//	public GroupTreeModel getGroupTree(Long userId){
//		List<Group> userGroups= iUserService.getUserGroups(userId);
//		List<Long> userGroupIds=userGroups.stream().map(x->x.getId()).collect(Collectors.toList());
//		//root node has 1 as its id. this is a standard through this project.
//		GroupDTO root=genericMapperService.getMapper(GroupDTO.class,Group.class).toView(iGroupDao.loadById(1L));
//		GroupTreeModel result=new GroupTreeModel();
//		result.setGroup(root);
//		result.setParent(null);
//		if(userGroupIds.contains(result.getGroup().getId())) result.setMember(true);
//		result=setChildren(result,userGroupIds);
//		return result;
//	}
//
//	/**
//	 * a recursive function to get children of a group and fill it till we reach the leaf.
//	 * @param groupTreeModel The group tree model to be filled
//	 * @param userGroups the List of userGroups to determine if the user isMember for a group is true or false.
//	 * @return
//	 */
//	@Override
//	public GroupTreeModel setChildren(GroupTreeModel groupTreeModel,List<Long> userGroups){
//		List<GroupDTO> children=genericMapperService.getMapper(GroupDTO.class,Group.class).
//				toView(getChildrenGroups(groupTreeModel.getGroup().getId()));
//		if(children==null || children.size()==0) return groupTreeModel;//if we reach a leaf, leave the list with size 0
//		children.stream().forEach(x->{
//			GroupTreeModel temp=new GroupTreeModel();
//			temp.setGroup(x);
//			temp.setParent(groupTreeModel.getGroup().getId());
//			if(groupTreeModel.getMember()) temp.setMember(true);//if parent is a member then this group is too.
//			else {//if parent is not, check if the any of the children are.
//				if(userGroups.contains(temp.getGroup().getId())) temp.setMember(true);
//			}
//			temp=setChildren(temp,userGroups);
//			groupTreeModel.getChildren().add(temp);
//		});
//		return groupTreeModel;
//	}
//
//	/**
//	 * given a group id returns a group with it's parent initialized eagerly!
//	 * Normal loadById will fetch the entity lazy!
//	 * @param groupId
//	 * @return
//	 */
//	@Override
//	public Group getGroupByIdEager(Long groupId) {
//		Group gp=getGenericRepository().loadById(groupId);
//		gp.getParentGroup();//so it gets initialized
//		return gp;
//	}


}