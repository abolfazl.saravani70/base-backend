package com.rbp.core.service.security;
/**
 * @author Alireza Souhani 1398.02.01
 */
import com.rbp.core.model.domainmodel.security.Permission;
import com.rbp.core.service.base.IGenericService;

public interface IPermissionService extends IGenericService<Permission> {
	
	//public List<Permission> getPermissions();
	
}
