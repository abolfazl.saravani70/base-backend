package com.rbp.core.service.jasper;

import com.rbp.core.utility.ApplicationException;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.*;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class ExporterService {

    public static final String MEDIA_TYPE_EXCEL = "application/vnd.ms-excel";
    public static final String MEDIA_TYPE_PDF = "application/pdf";
    public static final String EXTENSION_TYPE_EXCEL = "xls";
    public static final String EXTENSION_TYPE_PDF = "pdf";
    public static final String EXTENSION_TYPE_IMG = "img";

    public HttpServletResponse export(String realfileName, String type, JasperPrint jp, HttpServletResponse response, ByteArrayOutputStream baos) {

        if (type.equalsIgnoreCase(EXTENSION_TYPE_EXCEL)) {
            // Export to output stream
            exportXls(jp, baos);

            // Set our response properties
            // Here you can declare a custom filename
            String fileName = realfileName+".xls";
            response.setHeader("Content-Disposition", "inline; filename=" + fileName);

            // Set content type
            response.setContentType(MEDIA_TYPE_EXCEL);
            response.setContentLength(baos.size());

            return response;
        }

        if (type.equalsIgnoreCase(EXTENSION_TYPE_PDF)) {
            // Export to output stream
            exportPdf(jp, baos);

            // Set our response properties
            // Here you can declare a custom filename
            String fileName = realfileName+".pdf";

            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("X-Frame-Options", "SAMEORIGIN");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-Disposition", "inline; filename="+ fileName);
            response.setContentType("application/pdf");

            // Set content type
            //response.setContentType(MEDIA_TYPE_PDF);
            response.setContentLength(baos.size());
            try {
                response.getOutputStream().write(baos.toByteArray());
                response.getOutputStream().flush();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return response;

        }

        if (type.equalsIgnoreCase(EXTENSION_TYPE_IMG)) {
            exportImg(jp, baos,1);
            String fileName = realfileName+".img";

            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("X-Frame-Options", "SAMEORIGIN");
            response.setHeader("Pragma", "public");
            response.setHeader("Content-Disposition", "inline; filename="+ fileName);
            response.setContentType("image/png");
            return response;

        }
        else
            throw new ApplicationException(1,"نوع خروجی مشخص نشده است.");
    }

    public void exportXls(JasperPrint jp, ByteArrayOutputStream baos) {
        // Create a JRXlsExporter instance
        JRXlsExporter exporter = new JRXlsExporter();

        // Here we assign the parameters jp and baos to the exporter
        exporter.setExporterInput(new SimpleExporterInput(jp));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
        // Excel specific parameters
        SimpleXlsxReportConfiguration reportConfig
                = new SimpleXlsxReportConfiguration();
        reportConfig.setOnePagePerSheet(false);
        reportConfig.setRemoveEmptySpaceBetweenRows(true);
        reportConfig.setWhitePageBackground(false);
        exporter.setConfiguration(reportConfig);
        try {
            exporter.exportReport();

        } catch (JRException e) {
            throw new RuntimeException(e);
        }
    }

    public void exportPdf(JasperPrint jp, ByteArrayOutputStream baos) {
        // Create a JRXlsExporter instance
        JRPdfExporter exporter = new JRPdfExporter();
        // Here we assign the parameters jp and baos to the exporter
        exporter.setExporterInput(new SimpleExporterInput(jp));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(baos));

        SimplePdfReportConfiguration reportConfig
                = new SimplePdfReportConfiguration();
        reportConfig.setSizePageToContent(true);
        reportConfig.setForceLineBreakPolicy(false);
        SimplePdfExporterConfiguration exportConfig
                = new SimplePdfExporterConfiguration();
        exportConfig.setMetadataAuthor("Sayban");
        exportConfig.setEncrypted(true);
        exportConfig.setAllowedPermissionsHint("PRINTING");

        exporter.setConfiguration(reportConfig);
        exporter.setConfiguration(exportConfig);
        try {
            exporter.exportReport();

        } catch (JRException e) {
            throw new RuntimeException(e);
        }
    }

    public void exportImg(JasperPrint jp, ByteArrayOutputStream baos,float zoom){
        JasperPrintManager printManager = JasperPrintManager.getInstance(DefaultJasperReportsContext.getInstance());
        BufferedImage rendered_image = null;
        try {
            rendered_image = (BufferedImage)printManager.printPageToImage(jp, 0,zoom);
            ImageIO.write(rendered_image, "png", baos);
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
