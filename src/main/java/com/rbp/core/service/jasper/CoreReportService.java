package com.rbp.core.service.jasper;

import com.rbp.core.utility.ApplicationException;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Service
public class CoreReportService {

    public static final String TEMPLATE = "/reports";

    @Autowired
    private ExporterService exporter;

	/*@Autowired
	private TokenService tokenService;*/

    @Transactional
    public void download(String reportName,String fileName,String type,List<Object> beanList, HttpServletResponse response) {

        try {
            // 1. Add report parameters
            HashMap<String, Object> params = new HashMap<String, Object>();

            //JRDataSource dataSource = new JREmptyDataSource();

            // 2.  Retrieve template
            InputStream reportStream = this.getClass().getResourceAsStream(TEMPLATE+"/"+reportName);

            // 3. Convert template to JasperDesign
            JasperDesign jd = JRXmlLoader.load(reportStream);

            // 4. Compile design to JasperReport
            JasperReport jr = JasperCompileManager.compileReport(jd);

            // 5. Create the JasperPrint object
            // Make sure to pass the JasperReport, report parameters, and data source
            JasperPrint jp = JasperFillManager.fillReport(jr, params);

            // 6. Create an output byte stream where data will be written
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            // 7. Export report
            exporter.export(fileName,type, jp, response, baos);

            // 8. Write to reponse stream
            write(response, baos);

        } catch (JRException jre) {
            jre.printStackTrace();
            //logger.error("Unable to process download");
            throw new ApplicationException(1,"خطا در دریافت گزارش");
        }
    }


    private static JRDataSource getDataSource(List<Object> beanList) {
     //   Collection<Object> coll = new ArrayList<>();

//        for(Object bean:beanList)
//            coll.add(bean);
        //TODO: check this function to return a valid bean list!
        return new JRBeanCollectionDataSource(beanList);
    }
    private void write(HttpServletResponse response, ByteArrayOutputStream baos) {

        try {
            //logger.debug(baos.size());

            // Retrieve output stream
            ServletOutputStream outputStream = response.getOutputStream();
            // Write to output stream
            baos.writeTo(outputStream);
            // Flush the stream
            outputStream.flush();

            // Remove download token
            //tokenService.remove(token);

        } catch (Exception e) {
            //logger.error("Unable to write report to the output stream");
            throw new ApplicationException(1,"خطا در نوشتن گزارش روی پیام");
        }
    }

}

