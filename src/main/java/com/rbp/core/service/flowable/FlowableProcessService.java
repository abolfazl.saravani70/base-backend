package com.rbp.core.service.flowable;

import com.rbp.core.model.domainmodel.flowable.ProcessDefinitionDTO;
import com.rbp.core.model.domainmodel.flowable.ProcessInstanceDTO;
import com.rbp.core.model.dto.base.ActionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

@Service
public class FlowableProcessService {

    @Autowired
    OAuth2RestOperations restTemplate;


    @Value("${flowableAddress}")
    String flowable;

    /**
     * returns all the available process definitions. No restrictions
     * @return
     */
    public ActionResult<ProcessDefinitionDTO> getAllDefinitions(){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/getDefinitions");
            HttpEntity<ActionResult<ProcessDefinitionDTO>> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Fill the ProcessInstanseDTO with process Definition key and get a process instance started.
     * YOU HAVE TO FILL "initator" inside the the processInstanceDTO as the initiator!
     * if you want a global due date set it as: YYYY-MM-DD no other format is accepted!
     * @param process
     * @return
     */
    public ActionResult<ProcessInstanceDTO> startProcessInstanceByProcessDefinitionKey(ProcessInstanceDTO process){
        ActionResult result=new ActionResult();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/startByProcessDefinitionKey");
            HttpEntity<ProcessInstanceDTO> entity = new HttpEntity<>(process,headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+result.getExceptionMessage(),"");
        }
        return result;
    }

    /**
     * return a list of process definitions that can be started by a particular group.
     * @param groupId
     * @return
     */
    public ActionResult<ProcessDefinitionDTO> getCandidateStartersForProcessDefinition(String groupId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/getProcessDefinitionByCandidateGroup")
                    .queryParam("groupId",groupId);
            HttpEntity<ProcessInstanceDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * get all the variables for a process inside a Map<String,Object>
     * @param processInstanceId
     * @return
     */
    public ActionResult<Map<String,Object>> getVariables(String processInstanceId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/getVariables")
                    .queryParam("processInstanceId",processInstanceId);
            HttpEntity<ProcessInstanceDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * return all the process definitions specified by the category
     * @param category
     * @return
     */
    public ActionResult<ProcessDefinitionDTO> getProcessDefinitionsByCategory(String category){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/getProcessDefinitionByCategory")
                    .queryParam("category",category);
            HttpEntity<ProcessDefinitionDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Given a task, return the execution id in which this task is active. if there are no parallel executions, this will be the process ID
     * @param taskId
     * @return
     */
    public ActionResult<ProcessInstanceDTO> getProcessByTaskId( String taskId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/getProcessInstanceByTaskId")
                    .queryParam("taskId",taskId);
            HttpEntity<ProcessInstanceDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Delete a process Instance by process instanceId, you can also send a reason to why this process has been terminated.
     * @param processId
     * @param reason
     * @return
     */
    public ActionResult<ProcessInstanceDTO> cancelProcessInstance( String processId,String reason){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/cancelProcess")
                    .queryParam("processId",processId)
                    .queryParam("reason",reason);
            HttpEntity<ProcessInstanceDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * get a full process instance DTO with the given process business key
     * @param businessKey
     * @return
     */
    public ActionResult<ProcessInstanceDTO> getInfo(String businessKey){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/getInfo")
                    .queryParam("businessKey",businessKey);
            HttpEntity<ProcessInstanceDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * given a process instance, there might be alot of other executions inside of it. return back those executions.
     * Note: a process instance with 1 running execution has the same ids for process instance and execution id. hence this will return the process instance id.
     * @param processInstnaceId
     * @return
     */
    public ActionResult<String> getExecutionIds(String processInstnaceId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/process/getExecutionIds")
                    .queryParam("processInstanceId",processInstnaceId);
            HttpEntity<ProcessInstanceDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }
}
