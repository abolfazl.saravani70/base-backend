package com.rbp.core.service.flowable;


import com.rbp.core.model.domainmodel.flowable.TaskDTO;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.service.security.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class FlowableTaskService {

    @Autowired
    OAuth2RestOperations restTemplate;
    @Autowired
    IUserService iUserService;
    @Value("${flowableAddress}")
    String flowable;

    //*************** Primitive Operations with simple Logic. These have equal method names inside flowable*************
    /**
     * this endpoint returns a list of tasks for a user. these tasks are:
     * tasks that this user is an assignee
     * @param userId
     * @return
     */
    //Read Comments on the impementation
    //All of them are similar mostly
    public ActionResult<TaskDTO> getAssignedTaskByUserId(String userId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders(); //Set header
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);//Set what it accepts

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/assignedUserId")
                    .queryParam("userId", userId);//Building the URI and Settign @RequestParam Variables!

            HttpEntity<ActionResult<TaskDTO>> entity = new HttpEntity<>(headers);//creating the http entity which is
            //the whole entity. as you can see we can pass the headers to its constructor. we can also define
            //an Object and pass it as the Object body! hence we can send messages taht require data as in form of DTOs
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);//sending the reuqest and
            //saving the result. Obviously the result will be in the form of ActionResult! specify URI,the http entity and
            //the type of Object we expect the request to return which as i said is Action Result.
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * provided a user Id as a string, this returns all the task this user Can be assigned to
     * Tasks for groups this user is a member of and tasks that this user directly is a candidate for
     * @param userId
     * @return
     */
    public ActionResult<TaskDTO> getCandidateTasksByUserId(String userId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/candidateUserId")
                    .queryParam("userId", userId);
            HttpEntity<ActionResult<TaskDTO>> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;

    }
    /**
     * Given a groupId returns all the tasks that members of this group can start.
     * @param groupId
     * @return
     */
    public ActionResult<TaskDTO> getTaskByGroupId(String groupId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/candidateGroupId")
                    .queryParam("groupId", groupId);
            HttpEntity<ActionResult<TaskDTO>> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Claim a task with TaskId and UserId, this method will check, if by the time this user requests to claim this task, no one else has been assigned to it.
     * if they do, it will return an exception! if you want to set assignee no matter what, use force claim.
     * @param userId
     * @param task
     * @return
     */
    public ActionResult<TaskDTO> claimTaskByTaskId(String userId,String task){

        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/claim")
                    .queryParam("userId", userId)
                    .queryParam("task",task);
            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Force claim a task by Task Id and User Id. This will not CHECK if the task has already been assigned. meaning that the assignee will be change no matter what.
     * Use this to forcibly claim a task no matter the circumstances.
     * @param userId
     * @param task
     * @return
     */
    public ActionResult<TaskDTO> forceClaimTaskByTaskId(String userId,String task){

        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/forceClaim")
                    .queryParam("userId", userId)
                    .queryParam("task",task);

            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Using a userID and a taskDTO complete a task.
     * Complete a task by taskId, also you have to send data inside a TaskDTO, fill Data and Task ID inside the DTO
     * @param userId
     * @param task
     * @return
     */
    public ActionResult<TaskDTO> completeTask(String userId,String task){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/complete")
                    .queryParam("userId", userId)
                    .queryParam("task",task);

            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers); //Body is set for the httpEntity inside constructor
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * get data for a task inside a map<string,Object> with taskId
     * @param taskId
     * @return
     */
    public ActionResult<Map<String,Object>> getVariables(String taskId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/getVariables")
                    .queryParam("taskId", taskId);
            HttpEntity<ActionResult<Map<String,Object>>> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * returns the list of all the un-assigned tasks
     * @return
     */
    public ActionResult<TaskDTO> getAllUnAssignedTasks(){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/getAllUnassigned");
            HttpEntity<ActionResult<TaskDTO>> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * send a user list and add them as candidate users that can get that task. (be assigned to)
     * it DOES NOT remove the previous identity links!
     * @param userList
     * @param taskId
     * @return
     */
    public ActionResult<TaskDTO> setUserListAsCandidate(List<String> userList,String taskId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/setUserListAsCandidate")
                    .queryParam("taskId", taskId)
                    .queryParam("userList",userList);
            result=new ActionResult<TaskDTO>();
            HttpEntity<ActionResult<TaskDTO>> entity = new HttpEntity<>(result,headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Set a group as candidate for a task
     * it DOES NOT remove the previous identity links!
     * @param groupList
     * @param taskId
     * @return
     */
    public ActionResult<TaskDTO> setGroupListAsCandidate(List<String> groupList,String taskId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/setGroupListAsCandidate")
                    .queryParam("taskId", taskId)
                    .queryParam("groupList",groupList);
            result=new ActionResult<TaskDTO>();
            HttpEntity<ActionResult<TaskDTO>> entity = new HttpEntity<>(result,headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /***
     * setting local variables for a task. NOTE: if a variable doesn't already exist in the task, it will be created!
     * @param dto
     * @return
     */
    public ActionResult<TaskDTO> setTaskVariables(TaskDTO dto){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/setTaskVariables");
            result=new ActionResult<TaskDTO>();
            HttpEntity<TaskDTO> entity = new HttpEntity<>(dto,headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }


    /**
     * given a process instance, return the active tasks for that process (Current tasks for that process).
     * @param processInstanceId
     * @return
     */
    public ActionResult<TaskDTO> getCurrentTasks(String processInstanceId) {
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/setTaskVariables")
                    .queryParam("processInstanceId",processInstanceId);
            result=new ActionResult<TaskDTO>();
            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * returns the list of tasks defined inside a process definition
     * @param processDefinitionId
     * @return
     */
    public ActionResult<TaskDTO> getAllTasks(String processDefinitionId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/getAllTaskByProcessDefinitionId")
                    .queryParam("processDefinitionId",processDefinitionId);
            result=new ActionResult<TaskDTO>();
            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * Get the current task of this execution id
     * @param executionId
     * @return
     */
    public ActionResult<TaskDTO> getAllTasksByExecutionId(String executionId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/getAllTaskByExecutionId")
                    .queryParam("executionId",executionId);

            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);
            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * this will remove all the identity links from the task
     * @param taskId
     * @return
     */
    public ActionResult<TaskDTO> removeIdentityLinks(String taskId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/removeIdentityLinks")
                    .queryParam("taskId",taskId);
            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);

            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }
    //*************** END OF Primitive Operations with simple Logic. These have equal method names inside flowable*************
    /**
     * complete a task and set the user for the list of the next upcoming tasks!
     * be warned as this will set this user assignee for all of the next tasks!
     * In case of parallel gateway, all of the next tasks will take this user as assignee!
     * @param userId
     * @param task
     * @param nextUserId
     * @return
     */
    public ActionResult<TaskDTO> completeAndSetNextAssignee(String userId,TaskDTO task,String nextUserId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/completeAndSetNextAssignee")
                    .queryParam("userId",userId)
                    .queryParam("nextUserId",nextUserId);
            HttpEntity<TaskDTO> entity = new HttpEntity<>(task,headers);

            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }


    /**
     * this method completes a task and then sets a zone code for the next level tasks!
     * this doest not consult the process variable! and it will override it if there is any.
     * @param task
     * @param zone
     * @return
     */
    public ActionResult<TaskDTO> completeAndSetNextGroupAssignmentByZone(String userId,TaskDTO task,
                                                                           String zone){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/completeAndSetNextGroupAssignmentByZone")
                    .queryParam("userId",userId)
                    .queryParam("zone",zone);
            HttpEntity<TaskDTO> entity = new HttpEntity<>(task,headers);

            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;

    }

    /**
     * unclaim a task.
     * @param taskId
     * @return
     */
    public ActionResult<TaskDTO> unclaim( String taskId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/unclaim")
                    .queryParam("taskId",taskId);
            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);

            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

    /**
     * get task information based on task id.
     * @param taskId
     * @return
     */
    public ActionResult<TaskDTO> getInfo( String taskId){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/getInfo")
                    .queryParam("taskId",taskId);
            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);

            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }


    /**
     * According to flowable documentation:
     * There is an activity extension that allows you to specify an expression in your task-definition to set the initial due date of a task when it is created.
     * The expression should always resolve to a java.util.Date, java.util.String (ISO8601 formatted), ISO8601 time-duration (for example, PT50M) or null.
     * For example, you could use a date that was entered in a previous form in the process or calculated in a previous Service Task.
     * If a time-duration is used, the due-date is calculated based on the current time and incremented by the given period.
     * For example, when "PT30M" is used as dueDate, the task is due in thirty minutes from now.
     *
     * NOTE: you have to add the java delegate defined in flowable for this to work!
     * @param executionId
     * @param dueDate
     * @return
     */
    @PostMapping("/setDueDateOfNextTasks")
    @ApiOperation("set a due date for an execution or process instance. it will return a list of the tasks that was affected!")
    public ActionResult<TaskDTO> setDueDateOfNextTasks(String executionId,Date dueDate){
        ActionResult result;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(flowable + "/task/setDueDateOfNextTasks")
                    .queryParam("executionId",executionId)
                    .queryParam("dueDate",dueDate);
            HttpEntity<TaskDTO> entity = new HttpEntity<>(headers);

            result = restTemplate.postForObject(builder.build().toUriString(), entity, ActionResult.class);
        }catch (Exception e){
            result=new ActionResult(null,0,0,0L,1,"Flowable ERROR:"+e.getMessage(),"");
        }
        return result;
    }

}
