/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.bankingInfo;

import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Account;
import com.rbp.core.service.base.IGenericService;

import java.util.Optional;

public interface IAccountService extends IGenericService<Account> {

    Account findByNumber(String number);
}
