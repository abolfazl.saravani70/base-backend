/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.ICityDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.City;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.geographical.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CityService extends GenericService<City> implements ICityService {
    @Autowired
    ICityDao iCityDao;
    @Override
    protected IGenericRepository<City> getGenericRepository() {
        return iCityDao;
    }
    @Transactional
    @Override
    public List<City> getByStateId(Long stateId) {
        return iCityDao.getByStateId(stateId);
    }
}
