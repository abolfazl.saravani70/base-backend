/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.bankingInfo.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.bankingInfo.IAccountDao;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Account;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.bankingInfo.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService extends GenericService<Account> implements IAccountService {

    @Autowired
    IAccountDao iAccountDao;



    @Override
    protected IGenericRepository<Account> getGenericRepository() {
        return iAccountDao;
    }

    @Override
    public Account findByNumber(String number) {

        Account acc = iAccountDao.findByNumber(number);
        return acc;
    }
}
