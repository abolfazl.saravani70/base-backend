/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.IVillageDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Village;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.geographical.IVillageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VillageService extends GenericService<Village> implements IVillageService {
    @Autowired
    IVillageDao iVillageDao;
    @Override
    protected IGenericRepository<Village> getGenericRepository() {
        return iVillageDao;
    }
    @Transactional
    @Override
    public List<Village> getByCityId(Long cityId) {
        return iVillageDao.getByCityId(cityId);
    }
}
