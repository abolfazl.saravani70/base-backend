package com.rbp.core.service.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.ILocationDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Location;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.geographical.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationService extends GenericService<Location> implements ILocationService {
    @Autowired
    ILocationDao iLocationDao;
    @Override
    protected IGenericRepository<Location> getGenericRepository() {
        return iLocationDao;
    }
}
