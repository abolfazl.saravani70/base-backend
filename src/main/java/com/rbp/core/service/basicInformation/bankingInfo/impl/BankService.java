/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.bankingInfo.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.bankingInfo.IBankDao;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Bank;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.bankingInfo.IBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankService extends GenericService<Bank> implements IBankService {
    @Autowired
    IBankDao iBankDao;
    @Override
    protected IGenericRepository<Bank> getGenericRepository() {
        return iBankDao;
    }
}
