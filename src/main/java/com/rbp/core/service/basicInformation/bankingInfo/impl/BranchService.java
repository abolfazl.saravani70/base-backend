/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.bankingInfo.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.bankingInfo.IBranchDao;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Branch;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.bankingInfo.IBranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchService extends GenericService<Branch> implements IBranchService {
    @Autowired
    IBranchDao iBranchDao;
    @Override
    protected IGenericRepository<Branch> getGenericRepository() {
        return iBranchDao;
    }

    @Override
    public List<Branch> getBranchesByBankId(Long bankId) {
        return iBranchDao.getBranchesByBankId(bankId);
    }
}
