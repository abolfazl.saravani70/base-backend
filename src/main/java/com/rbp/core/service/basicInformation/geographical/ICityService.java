/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.geographical;

import com.rbp.core.model.domainmodel.basicInformation.geographical.City;
import com.rbp.core.service.base.IGenericService;

import java.util.List;

public interface ICityService extends IGenericService<City> {
    public List<City> getByStateId(Long stateId);
}
