package com.rbp.core.service.basicInformation.geographical;

import com.rbp.core.model.domainmodel.basicInformation.geographical.Location;
import com.rbp.core.service.base.IGenericService;

public interface ILocationService extends IGenericService<Location> {
}
