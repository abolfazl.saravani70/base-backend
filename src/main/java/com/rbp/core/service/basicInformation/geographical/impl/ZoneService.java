/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.IZoneDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Zone;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.geographical.IZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ZoneService extends GenericService<Zone> implements IZoneService {
    @Autowired
    IZoneDao iZoneDao;
    @Override
    protected IGenericRepository<Zone> getGenericRepository() {
        return iZoneDao;
    }
    @Transactional
    @Override
    public List<Zone> getByVillageId(Long villageId) {
        return iZoneDao.getByVillageId(villageId);
    }
}
