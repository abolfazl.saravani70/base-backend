/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.service.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.IStateDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.State;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.geographical.IStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StateService extends GenericService<State> implements IStateService {
    @Autowired
    IStateDao iStateDao;

    @Override
    protected IGenericRepository<State> getGenericRepository() {
        return iStateDao;
    }
}
