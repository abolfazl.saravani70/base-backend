package com.rbp.core.common.config.spring;

import com.rbp.core.controller.base.restConstant;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@EnableWebMvc
@Configuration
public class springConfig {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping( restConstant.BASE_REST +"/**").allowedOrigins("*")
                .allowedHeaders("Access-Control-Allow-Origin", "http://192.168.1.158:8080")
                .allowedHeaders("Access-Control-Allow-Methods", "POST, GET")
                .allowedHeaders("Access-Control-Max-Age", "3600")
                        .allowedHeaders("Access-Control-Allow-Headers", "Content-Type", "Access-Control-Allow-Headers", "Authorization", "X-Requested-With");
            }
        };
    }
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource
                = new ReloadableResourceBundleMessageSource();

        messageSource.setBasename("/classes/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

//    @Bean
//    public RestTemplate restTemplate(RestTemplateBuilder builder) {
//        return builder.build();
//    }

}
