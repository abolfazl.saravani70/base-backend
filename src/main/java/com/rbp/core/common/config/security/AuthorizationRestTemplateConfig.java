package com.rbp.core.common.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.web.client.RestTemplateExchangeTags;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Behnam Mohammadi
 */
@Configuration
@EnableOAuth2Client
public class AuthorizationRestTemplateConfig {

    @Autowired
    private Environment env;

//    @Value("${server.ssl.key-store}")
//    private Resource keyStoreFile;

    @Bean
    protected OAuth2ProtectedResourceDetails resourceDetails() {
        ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();
        List<String> scopes = new ArrayList(1);
        scopes.add(env.getProperty("clientScope"));
        resource.setAccessTokenUri(env.getProperty("getTokenUrl"));
        resource.setClientId(env.getProperty("clientId"));
        resource.setClientSecret(env.getProperty("clientSecret"));
        resource.setGrantType(env.getProperty("grantType"));
        resource.setScope(scopes);
        resource.setUsername(env.getProperty("oauth2-userName"));
        resource.setPassword(env.getProperty("oauth2-password"));
        return resource;
    }

    @Bean
    public OAuth2RestOperations restTemplate() {
//        CloseableHttpClient httpClient
//                = HttpClients.custom()
//                .setSSLHostnameVerifier(new NoopHostnameVerifier())
//                .build();
//        HttpComponentsClientHttpRequestFactory requestFactory
//                = new HttpComponentsClientHttpRequestFactory();
//        requestFactory.setHttpClient(httpClient);
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails(), new DefaultOAuth2ClientContext());
//        restTemplate.setRequestFactory(requestFactory);
        return restTemplate;
    }
    @Bean("default")
    public RestTemplate getTemplateAE(){
        return new RestTemplate();
    }

}
