package com.rbp.core.common.config.swagger;

import com.rbp.core.controller.base.restConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * @author Alireza Souhani 1398.02.01
 */

/**
 * COMMENTED @CONFIGURATION TO REMOVE ITS BEAN! UNCOMMENT AND REUSE THIS
 * BUT DELETE SWAGGERCONFIG CLASS (ADDED BY BEHNAM MOHAMMADI)!
 */
//@Configuration
//@EnableSwagger2
public class SwaggerConfigOld extends WebMvcConfigurationSupport {

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.rbp.*"))
                .paths(regex("/**" + restConstant.BASE_REST + "**/**"))
                .build()
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Application REST API")
                .description("\"REST API for this App\"")
                .version("1.0.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0\"")
                .contact(new Contact("Rayan Bahman Pardaz", "https://www.rbp.ir", "info@rbp.ir"))
                .build();
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/dozerMappingFiles/**")
                .addResourceLocations("classpath:/dozerMappingFiles/");
    }
}