package com.rbp.core.controller.exception;

import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.utility.ApplicationException;
import com.rbp.core.utility.ApplicationLogger;
import org.springframework.aop.AopInvocationException;
import org.springframework.aop.aspectj.annotation.NotAnAtAspectException;
import org.springframework.aop.framework.AopConfigException;
import org.springframework.beans.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.BeanDefinitionParsingException;
import org.springframework.beans.factory.support.BeanDefinitionValidationException;
import org.springframework.beans.factory.xml.XmlBeanDefinitionStoreException;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.convert.ConversionException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.ConverterNotFoundException;
import org.springframework.core.serializer.support.SerializationFailedException;
import org.springframework.dao.*;
import org.springframework.ejb.access.EjbAccessException;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.jca.cci.CannotCreateRecordException;
import org.springframework.jca.cci.CannotGetCciConnectionException;
import org.springframework.jca.cci.CciOperationNotSupportedException;
import org.springframework.jca.cci.RecordTypeNotSupportedException;
import org.springframework.jdbc.*;
import org.springframework.jdbc.datasource.lookup.DataSourceLookupFailureException;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.jdbc.support.xml.SqlXmlFeatureNotImplementedException;
import org.springframework.jmx.JmxException;
import org.springframework.jmx.MBeanServerNotFoundException;
import org.springframework.jmx.access.InvocationFailureException;
import org.springframework.jmx.access.MBeanConnectFailureException;
import org.springframework.jmx.access.MBeanInfoRetrievalException;
import org.springframework.jmx.export.MBeanExportException;
import org.springframework.jmx.export.UnableToRegisterMBeanException;
import org.springframework.jmx.export.metadata.InvalidMetadataException;
import org.springframework.jmx.export.notification.UnableToSendNotificationException;
import org.springframework.jndi.JndiLookupFailureException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.remoting.*;
import org.springframework.remoting.jaxws.JaxWsSoapFaultException;
import org.springframework.remoting.soap.SoapFaultException;
import org.springframework.scheduling.SchedulingException;
import org.springframework.scripting.ScriptCompilationException;
import org.springframework.scripting.bsh.BshScriptUtils.BshExecutionException;
import org.springframework.transaction.*;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.IOException;
import java.util.Locale;


/**
 * 
 * @author Alireza Souhani 1398.02.26
 *
 */
//@EnableWebMvc
//@RestControllerAdvice
public class ApplicationExceptionController{
	
	@Autowired
	ApplicationLogger applicationLogger;
	
	ReloadableResourceBundleMessageSource exceptionMessageSource;
	public ApplicationExceptionController(){
		exceptionMessageSource= new ReloadableResourceBundleMessageSource();
		exceptionMessageSource.setBasename("messages/messages");
	}

	/**
	 * Spring Exception Handler Block
	 * @param exp
	 * @return Permission Result
	 */
	@ExceptionHandler(NullPointerException.class)
	 public ActionResult getNullPointerException(NullPointerException exp){
		exp.printStackTrace();
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "2", exceptionMessageSource.getMessage("2", null,Locale.US), "NullPointerException");
		return new ActionResult(null, 0, 0, 0L, 2, exceptionMessageSource.getMessage("2", null, Locale.US), null);
	}
	
	@ExceptionHandler(AopConfigException.class)
	 public ActionResult getAopConfigException(AopConfigException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1000", exceptionMessageSource.getMessage("1000", null, Locale.US), "AopConfigException");
		return new ActionResult(null, 0, 0, 0L, 1000, exceptionMessageSource.getMessage("1000", null, Locale.US), null);
	}
	
	@ExceptionHandler(AopInvocationException.class)
	 public ActionResult getAopInvocationException(AopInvocationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1021", exceptionMessageSource.getMessage("1021", null, Locale.US), "AopInvocationException");
		return new ActionResult(null, 0, 0, 0L, 1001, exceptionMessageSource.getMessage("1001", null, Locale.US), null);
	}
	
	@ExceptionHandler(ApplicationContextException.class)
	 public ActionResult getApplicationContextException(ApplicationContextException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1001", exceptionMessageSource.getMessage("1001", null, Locale.US), "ApplicationContextException");
		return new ActionResult(null, 0, 0, 0L, 1002, exceptionMessageSource.getMessage("1002", null, Locale.US), null);
	}
	
	@ExceptionHandler(BadSqlGrammarException.class)
	 public ActionResult getBadSqlGrammarException(BadSqlGrammarException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1003", exceptionMessageSource.getMessage("1003", null, Locale.US), "BadSqlGrammarException");
		return new ActionResult(null, 0, 0, 0L, 1003, exceptionMessageSource.getMessage("1003", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanCreationException.class)
	 public ActionResult getBeanCreationException(BeanCreationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1004", exceptionMessageSource.getMessage("1004", null, Locale.US), "BeanCreationException");
		return new ActionResult(null, 0, 0, 0L, 1004, exceptionMessageSource.getMessage("1004", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanCreationNotAllowedException.class)
	 public ActionResult getBeanCreationNotAllowedException(BeanCreationNotAllowedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1005", exceptionMessageSource.getMessage("1005", null, Locale.US), "BeanCreationNotAllowedException");
		return new ActionResult(null, 0, 0, 0L, 1005, exceptionMessageSource.getMessage("1005", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanCurrentlyInCreationException.class)
	 public ActionResult getBeanCurrentlyInCreationException(BeanCurrentlyInCreationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1006", exceptionMessageSource.getMessage("1006", null, Locale.US), "BeanCurrentlyInCreationException");
		return new ActionResult(null, 0, 0, 0L, 1006, exceptionMessageSource.getMessage("1006", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanDefinitionParsingException.class)
	 public ActionResult getBeanDefinitionParsingException(BeanDefinitionParsingException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1007", exceptionMessageSource.getMessage("1007", null, Locale.US), "BeanDefinitionParsingException");
		return new ActionResult(null, 0, 0, 0L, 1007, exceptionMessageSource.getMessage("1007", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanDefinitionStoreException.class)
	 public ActionResult getBeanDefinitionStoreException(BeanDefinitionStoreException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1008", exceptionMessageSource.getMessage("1008", null, Locale.US), "BeanDefinitionStoreException");
		return new ActionResult(null, 0, 0, 0L, 1008, exceptionMessageSource.getMessage("1008", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanDefinitionValidationException.class)
	 public ActionResult getBeanDefinitionValidationException(BeanDefinitionValidationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1009", exceptionMessageSource.getMessage("1009", null, Locale.US), "BeanDefinitionValidationException");
		return new ActionResult(null, 0, 0, 0L, 1009, exceptionMessageSource.getMessage("1009", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanExpressionException.class)
	 public ActionResult getBeanExpressionException(BeanExpressionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1010", exceptionMessageSource.getMessage("1010", null, Locale.US), "BeanExpressionException");
		return new ActionResult(null, 0, 0, 0L, 1010, exceptionMessageSource.getMessage("1010", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanInitializationException.class)
	 public ActionResult getBeanInitializationException(BeanInitializationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1011", exceptionMessageSource.getMessage("1011", null, Locale.US), "BeanInitializationException");
		return new ActionResult(null, 0, 0, 0L, 1011, exceptionMessageSource.getMessage("1011", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanInstantiationException.class)
	 public ActionResult getBeanInstantiationException(BeanInstantiationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1012", exceptionMessageSource.getMessage("1012", null, Locale.US), "BeanInstantiationException");
		return new ActionResult(null, 0, 0, 0L, 1012, exceptionMessageSource.getMessage("1012", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanIsAbstractException.class)
	 public ActionResult getBeanIsAbstractException(BeanIsAbstractException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1013", exceptionMessageSource.getMessage("1013", null, Locale.US), "BeanIsAbstractException");
		return new ActionResult(null, 0, 0, 0L, 1013, exceptionMessageSource.getMessage("1013", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanIsNotAFactoryException.class)
	 public ActionResult getBeanIsNotAFactoryException(BeanIsNotAFactoryException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1014", exceptionMessageSource.getMessage("1014", null, Locale.US), "BeanIsNotAFactoryException");
		return new ActionResult(null, 0, 0, 0L, 1014, exceptionMessageSource.getMessage("1014", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeanNotOfRequiredTypeException.class)
	 public ActionResult getBeanNotOfRequiredTypeException(BeanNotOfRequiredTypeException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1015", exceptionMessageSource.getMessage("1015", null, Locale.US), "BeanNotOfRequiredTypeException");
		return new ActionResult(null, 0, 0, 0L, 1015, exceptionMessageSource.getMessage("1015", null, Locale.US), null);
	}
	
	@ExceptionHandler(BeansException.class)
	 public ActionResult getBeansException(BeansException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1016", exceptionMessageSource.getMessage("1016", null, Locale.US), "BeansException");
		return new ActionResult(null, 0, 0, 0L, 1016, exceptionMessageSource.getMessage("1016", null, Locale.US), null);
	}
	
//	@ExceptionHandler(BootstrapException.class)
//	 public ActionResult getBootstrapException(BootstrapException exp){
//		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1017", exceptionMessageSource.getMessage("1017", null, Locale.US), "BootstrapException");
//		return new ActionResult(null, 0, 0, 0, 1017, exceptionMessageSource.getMessage("1017", null, Locale.US), null);
//	}
	
	@ExceptionHandler(BshExecutionException.class)
	 public ActionResult getBshExecutionException(BshExecutionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1018", exceptionMessageSource.getMessage("1018", null, Locale.US), "BshExecutionException");
		return new ActionResult(null, 0, 0, 0L, 1018, exceptionMessageSource.getMessage("1018", null, Locale.US), null);
	}
	
	@ExceptionHandler(CannotAcquireLockException.class)
	 public ActionResult getCannotAcquireLockException(CannotAcquireLockException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1019", exceptionMessageSource.getMessage("1019", null, Locale.US), "CannotAcquireLockException");
		return new ActionResult(null, 0, 0, 0L, 1019,exceptionMessageSource.getMessage("1019", null, Locale.US), null);
	}
	
	@ExceptionHandler(CannotCreateRecordException.class)
	 public ActionResult getCannotCreateRecordException(CannotCreateRecordException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1020", exceptionMessageSource.getMessage("1020", null, Locale.US), "CannotCreateRecordException");
		return new ActionResult(null, 0, 0, 0L, 1020, exceptionMessageSource.getMessage("1020", null, Locale.US), null);
	}
	
	@ExceptionHandler(CannotCreateTransactionException.class)
	 public ActionResult getCannotCreateTransactionException(CannotCreateTransactionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1021", exceptionMessageSource.getMessage("1021", null, Locale.US), "CannotCreateTransactionException");
		return new ActionResult(null, 0, 0, 0L, 1021, exceptionMessageSource.getMessage("1021", null, Locale.US), null);
	}
	
	@ExceptionHandler(CannotGetCciConnectionException.class)
	 public ActionResult getCannotGetCciConnectionException(CannotGetCciConnectionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1022", exceptionMessageSource.getMessage("1022", null, Locale.US), "CannotGetCciConnectionException");
		return new ActionResult(null, 0, 0, 0L, 1022, exceptionMessageSource.getMessage("1022", null, Locale.US), null);
	}
	
	@ExceptionHandler(CannotGetJdbcConnectionException.class)
	 public ActionResult getCannotGetJdbcConnectionException(CannotGetJdbcConnectionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1023", exceptionMessageSource.getMessage("1023", null, Locale.US), "CannotGetJdbcConnectionException");
		return new ActionResult(null, 0, 0, 0L, 1023, exceptionMessageSource.getMessage("1023", null, Locale.US), null);
	}
	
	@ExceptionHandler(CannotLoadBeanClassException.class)
	 public ActionResult getCannotLoadBeanClassException(CannotLoadBeanClassException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1024", exceptionMessageSource.getMessage("1024", null, Locale.US), "CannotLoadBeanClassException");
		return new ActionResult(null, 0, 0, 0L, 1024, exceptionMessageSource.getMessage("1024", null, Locale.US), null);
	}
	
	@ExceptionHandler(CannotSerializeTransactionException.class)
	 public ActionResult getCannotSerializeTransactionException(CannotSerializeTransactionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1025", exceptionMessageSource.getMessage("1025", null, Locale.US), "CannotSerializeTransactionException");
		return new ActionResult(null, 0, 0, 0L, 1025, exceptionMessageSource.getMessage("1025", null, Locale.US), null);
	}
	
	@ExceptionHandler(CciOperationNotSupportedException.class)
	 public ActionResult getCciOperationNotSupportedException(CciOperationNotSupportedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1026", exceptionMessageSource.getMessage("1026", null, Locale.US), "CciOperationNotSupportedException");
		return new ActionResult(null, 0, 0, 0L, 1026, exceptionMessageSource.getMessage("1026", null, Locale.US), null);
	}
	
	@ExceptionHandler(CleanupFailureDataAccessException.class)
	 public ActionResult getCleanupFailureDataAccessException(CleanupFailureDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1027", exceptionMessageSource.getMessage("1027", null, Locale.US), "CleanupFailureDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1027, exceptionMessageSource.getMessage("1027", null, Locale.US), null);
	}
	
	@ExceptionHandler(ConcurrencyFailureException.class)
	 public ActionResult getConcurrencyFailureException(ConcurrencyFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1028", exceptionMessageSource.getMessage("1028", null, Locale.US), "ConcurrencyFailureException");
		return new ActionResult(null, 0, 0, 0L, 1028, exceptionMessageSource.getMessage("1028", null, Locale.US), null);
	}
	
	@ExceptionHandler(ConversionException.class)
	 public ActionResult getConversionException(ConversionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1029", exceptionMessageSource.getMessage("1029", null, Locale.US), "ConversionException");
		return new ActionResult(null, 0, 0, 0L, 1029, exceptionMessageSource.getMessage("1029", null, Locale.US), null);
	}
	
	@ExceptionHandler(ConversionFailedException.class)
	 public ActionResult getConversionFailedException(ConversionFailedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1030", exceptionMessageSource.getMessage("1030", null, Locale.US), "ConversionFailedException");
		return new ActionResult(null, 0, 0, 0L, 1030, exceptionMessageSource.getMessage("1030", null, Locale.US), null);
	}
	
	@ExceptionHandler(ConversionNotSupportedException.class)
	 public ActionResult getConversionNotSupportedException(ConversionNotSupportedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1031", exceptionMessageSource.getMessage("1031", null, Locale.US), "ConversionNotSupportedException");
		return new ActionResult(null, 0, 0, 0L, 1031, exceptionMessageSource.getMessage("1031", null, Locale.US), null);
	}
	
	@ExceptionHandler(ConverterNotFoundException.class)
	 public ActionResult getConverterNotFoundException(ConverterNotFoundException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1032", exceptionMessageSource.getMessage("1032", null, Locale.US), "ConverterNotFoundException");
		return new ActionResult(null, 0, 0, 0L, 1032,exceptionMessageSource.getMessage("1032", null, Locale.US), null);
	}
	
	@ExceptionHandler(DataAccessException.class)
	 public ActionResult getDataAccessException(DataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1033", exceptionMessageSource.getMessage("1033", null, Locale.US), "DataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1033,exceptionMessageSource.getMessage("1033", null, Locale.US), null);
	}
	
	@ExceptionHandler(DataAccessResourceFailureException.class)
	 public ActionResult getDataAccessResourceFailureException(DataAccessResourceFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1034", exceptionMessageSource.getMessage("1034", null, Locale.US), "DataAccessResourceFailureException");
		return new ActionResult(null, 0, 0, 0L, 1034,exceptionMessageSource.getMessage("1034", null, Locale.US), null);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	 public ActionResult getDataIntegrityViolationException(DataIntegrityViolationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1035", exceptionMessageSource.getMessage("1035", null, Locale.US), "DataIntegrityViolationException");
		return new ActionResult(null, 0, 0, 0L, 1035,exceptionMessageSource.getMessage("1035", null, Locale.US), null);
	}
	
	@ExceptionHandler(DataRetrievalFailureException.class)
	 public ActionResult getDataRetrievalFailureException(DataRetrievalFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1036", exceptionMessageSource.getMessage("1036", null, Locale.US), "DataRetrievalFailureException");
		return new ActionResult(null, 0, 0, 0L, 1036,exceptionMessageSource.getMessage("1036", null, Locale.US), null);
	}
	
	@ExceptionHandler(DataSourceLookupFailureException.class)
	 public ActionResult getDataSourceLookupFailureException(DataSourceLookupFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1037", exceptionMessageSource.getMessage("1037", null, Locale.US), "DataSourceLookupFailureException");
		return new ActionResult(null, 0, 0, 0L, 1037,exceptionMessageSource.getMessage("1037", null, Locale.US), null);
	}
	
	@ExceptionHandler(DeadlockLoserDataAccessException.class)
	 public ActionResult getDeadlockLoserDataAccessException(DeadlockLoserDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1038", exceptionMessageSource.getMessage("1038", null, Locale.US), "DeadlockLoserDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1038,exceptionMessageSource.getMessage("1038", null, Locale.US), null);
	}
	
	@ExceptionHandler(DuplicateKeyException.class)
	 public ActionResult getDuplicateKeyException(DuplicateKeyException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1039", exceptionMessageSource.getMessage("1039", null, Locale.US), "DuplicateKeyException");
		return new ActionResult(null, 0, 0, 0L, 1039,exceptionMessageSource.getMessage("1039", null, Locale.US), null);
	}
	
	@ExceptionHandler(EjbAccessException.class)
	 public ActionResult getEjbAccessException(EjbAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1040", exceptionMessageSource.getMessage("1040", null, Locale.US), "EjbAccessException");
		return new ActionResult(null, 0, 0, 0L, 1040,exceptionMessageSource.getMessage("1040", null, Locale.US), null);
	}
	
	@ExceptionHandler(EmptyResultDataAccessException.class)
	 public ActionResult getEmptyResultDataAccessException(EmptyResultDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1041", exceptionMessageSource.getMessage("1041", null, Locale.US), "EmptyResultDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1041,exceptionMessageSource.getMessage("1041", null, Locale.US), null);
	}
	
	@ExceptionHandler(FactoryBeanNotInitializedException.class)
	 public ActionResult getFactoryBeanNotInitializedException(FactoryBeanNotInitializedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1042", exceptionMessageSource.getMessage("1042", null, Locale.US), "FactoryBeanNotInitializedException");
		return new ActionResult(null, 0, 0, 0L, 1042,exceptionMessageSource.getMessage("1042", null, Locale.US), null);
	}
	
	@ExceptionHandler(FatalBeanException.class)
	 public ActionResult getFatalBeanException(FatalBeanException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1043", exceptionMessageSource.getMessage("1043", null, Locale.US), "FatalBeanException");
		return new ActionResult(null, 0, 0, 0L, 1043,exceptionMessageSource.getMessage("1043", null, Locale.US), null);
	}
	
//	@ExceptionHandler(HandlerMethodInvocationException.class)
//	 public ActionResult getHandlerMethodInvocationException(HandlerMethodInvocationException exp){
//		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1044", exceptionMessageSource.getMessage("1044", null, Locale.US), "HandlerMethodInvocationException");
//		return new ActionResult(null, 0, 0, 0, 1044,exceptionMessageSource.getMessage("1044", null, Locale.US), null);
//	}
	
	@ExceptionHandler(HeuristicCompletionException.class)
	 public ActionResult getHeuristicCompletionException(HeuristicCompletionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1045", exceptionMessageSource.getMessage("1045", null, Locale.US), "HeuristicCompletionException");
		return new ActionResult(null, 0, 0, 0L, 1045,exceptionMessageSource.getMessage("1045", null, Locale.US), null);
	}
	
	@ExceptionHandler(HibernateOptimisticLockingFailureException.class)
	 public ActionResult getHibernateOptimisticLockingFailureException(HibernateOptimisticLockingFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1046", exceptionMessageSource.getMessage("1046", null, Locale.US), "HibernateOptimisticLockingFailureException");
		return new ActionResult(null, 0, 0, 0L, 1046,exceptionMessageSource.getMessage("1046", null, Locale.US), null);
	}
	
	@ExceptionHandler(HttpClientErrorException.class)
	 public ActionResult getHttpClientErrorException(HttpClientErrorException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1047", exceptionMessageSource.getMessage("1047", null, Locale.US), "HttpClientErrorException");
		return new ActionResult(null, 0, 0, 0L, 1047,exceptionMessageSource.getMessage("1047", null, Locale.US), null);
	}
	
	@ExceptionHandler(HttpMessageConversionException.class)
	 public ActionResult getHttpMessageConversionException(HttpMessageConversionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1048", exceptionMessageSource.getMessage("1048", null, Locale.US), "HttpMessageConversionException");

		return new ActionResult(null, 0, 0, 0L, 1048,exceptionMessageSource.getMessage("1048", null, Locale.US), null);
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	 public ActionResult getHttpMessageNotReadableException(HttpMessageNotReadableException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1049", exceptionMessageSource.getMessage("1049", null, Locale.US), "HttpMessageNotReadableException");
		return new ActionResult(null, 0, 0, 0L, 1049,exceptionMessageSource.getMessage("1049", null, Locale.US), null);
	}
	
//	@ExceptionHandler(HttpMessageNotWritableException.class)
//	 public ActionResult getHttpMessageNotWritableException(HttpMessageNotWritableException exp){
//		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1050", exceptionMessageSource.getMessage("1050", null, Locale.US), "HttpMessageNotWritableException");
//		return new ActionResult(null, 0, 0, 0, 1050,exceptionMessageSource.getMessage("1050", null, Locale.US), null);
//	}
	
	@ExceptionHandler(HttpServerErrorException.class)
	 public ActionResult getHttpServerErrorException(HttpServerErrorException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1051", exceptionMessageSource.getMessage("1051", null, Locale.US), "HttpServerErrorException");
		return new ActionResult(null, 0, 0, 0L, 1051,exceptionMessageSource.getMessage("1051", null, Locale.US), null);
	}
	
	@ExceptionHandler(HttpStatusCodeException.class)
	 public ActionResult getHttpStatusCodeException(HttpStatusCodeException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1052", exceptionMessageSource.getMessage("1052", null, Locale.US), "HttpStatusCodeException");
		return new ActionResult(null, 0, 0, 0L, 1052,exceptionMessageSource.getMessage("1052", null, Locale.US), null);
	}
	
	@ExceptionHandler(IllegalTransactionStateException.class)
	 public ActionResult getIllegalTransactionStateException(IllegalTransactionStateException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1053", exceptionMessageSource.getMessage("1053", null, Locale.US), "IllegalTransactionStateException");
		return new ActionResult(null, 0, 0, 0L, 1053,exceptionMessageSource.getMessage("1053", null, Locale.US), null);
	}
	
	@ExceptionHandler(IncorrectResultSetColumnCountException.class)
	 public ActionResult getIncorrectResultSetColumnCountException(IncorrectResultSetColumnCountException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1054", exceptionMessageSource.getMessage("1054", null, Locale.US), "IncorrectResultSetColumnCountException");
		return new ActionResult(null, 0, 0, 0L, 1054,exceptionMessageSource.getMessage("1054", null, Locale.US), null);
	}
	
	@ExceptionHandler(IncorrectResultSizeDataAccessException.class)
	 public ActionResult getIncorrectResultSizeDataAccessException(IncorrectResultSizeDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1055", exceptionMessageSource.getMessage("1055", null, Locale.US), "IncorrectResultSizeDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1055,exceptionMessageSource.getMessage("1055", null, Locale.US), null);
	}
	
	@ExceptionHandler(IncorrectUpdateSemanticsDataAccessException.class)
	 public ActionResult getIncorrectUpdateSemanticsDataAccessException(IncorrectUpdateSemanticsDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1056", exceptionMessageSource.getMessage("1056", null, Locale.US), "IncorrectUpdateSemanticsDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1056,exceptionMessageSource.getMessage("1056", null, Locale.US), null);
	}
	
	@ExceptionHandler(InvalidIsolationLevelException.class)
	 public ActionResult getInvalidIsolationLevelException(InvalidIsolationLevelException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1057", exceptionMessageSource.getMessage("1057", null, Locale.US), "InvalidIsolationLevelException");
		return new ActionResult(null, 0, 0, 0L, 1057,exceptionMessageSource.getMessage("1057", null, Locale.US), null);
	}
	
	@ExceptionHandler(InvalidDataAccessResourceUsageException.class)
	 public ActionResult getInvalidDataAccessResourceUsageException(InvalidDataAccessResourceUsageException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1058", exceptionMessageSource.getMessage("1058", null, Locale.US), "InvalidDataAccessResourceUsageException");
		return new ActionResult(null, 0, 0, 0L, 1058,"تابع انتخابی با ورودی همخوانی ندارد.", null);
	}

	@ExceptionHandler(InvalidMetadataException.class)
	 public ActionResult getInvalidMetadataException(InvalidMetadataException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1059", exceptionMessageSource.getMessage("1059", null, Locale.US), "InvalidMetadataException");
		return new ActionResult(null, 0, 0, 0L, 1059,exceptionMessageSource.getMessage("1059", null, Locale.US), null);
	}
	
	@ExceptionHandler(InvalidPropertyException.class)
	 public ActionResult getInvalidPropertyException(InvalidPropertyException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1060", exceptionMessageSource.getMessage("1060", null, Locale.US), "InvalidPropertyException");
		return new ActionResult(null, 0, 0, 0L, 1060,exceptionMessageSource.getMessage("1060", null, Locale.US), null);
	}
	
	@ExceptionHandler(InvalidResultSetAccessException.class)
	 public ActionResult getInvalidResultSetAccessException(InvalidResultSetAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1061", exceptionMessageSource.getMessage("1061", null, Locale.US), "InvalidResultSetAccessException");
		return new ActionResult(null, 0, 0, 0L, 1061,exceptionMessageSource.getMessage("1061", null, Locale.US), null);
	}
	
	@ExceptionHandler(InvalidTimeoutException.class)
	 public ActionResult getInvalidTimeoutException(InvalidTimeoutException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1062", exceptionMessageSource.getMessage("1062", null, Locale.US), "InvalidTimeoutException");
		return new ActionResult(null, 0, 0, 0L, 1062,exceptionMessageSource.getMessage("1062", null, Locale.US), null);
	}
	
	@ExceptionHandler(InvocationFailureException.class)
	 public ActionResult getInvocationFailureException(InvocationFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1063", exceptionMessageSource.getMessage("1063", null, Locale.US), "InvocationFailureException");
		return new ActionResult(null, 0, 0, 0L, 1063,exceptionMessageSource.getMessage("1063", null, Locale.US), null);
	}
	
	@ExceptionHandler(JaxWsSoapFaultException.class)
	 public ActionResult getJaxRpcSoapFaultException(JaxWsSoapFaultException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1064", exceptionMessageSource.getMessage("1064", null, Locale.US), "JaxWsSoapFaultException");
		return new ActionResult(null, 0, 0, 0L, 1064,exceptionMessageSource.getMessage("1064", null, Locale.US), null);
	}
	
	@ExceptionHandler(JdbcUpdateAffectedIncorrectNumberOfRowsException.class)
	 public ActionResult getJdbcUpdateAffectedIncorrectNumberOfRowsException(JdbcUpdateAffectedIncorrectNumberOfRowsException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1065", exceptionMessageSource.getMessage("1065", null, Locale.US), "JdbcUpdateAffectedIncorrectNumberOfRowsException");
		return new ActionResult(null, 0, 0, 0L, 1065,exceptionMessageSource.getMessage("1065", null, Locale.US), null);
	}
	
//	@ExceptionHandler(JdoOptimisticLockingFailureException.class)
//	 public ActionResult getJdoOptimisticLockingFailureException(JdoOptimisticLockingFailureException exp){
//		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1066", exceptionMessageSource.getMessage("1066", null, Locale.US), "JdoOptimisticLockingFailureException");
//		return new ActionResult(null, 0, 0, 0,1066,exceptionMessageSource.getMessage("1066", null, Locale.US), null);
//	}
	
	@ExceptionHandler(JmxException.class)
	 public ActionResult getJmxException(JmxException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1067", exceptionMessageSource.getMessage("1067", null, Locale.US), "JmxException");
		return new ActionResult(null, 0, 0, 0L, 1067,exceptionMessageSource.getMessage("1067", null, Locale.US), null);
	}
	
	@ExceptionHandler(JndiLookupFailureException.class)
	 public ActionResult getJndiLookupFailureException(JndiLookupFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1068", exceptionMessageSource.getMessage("1068", null, Locale.US), "JndiLookupFailureException");
		return new ActionResult(null, 0, 0, 0L, 1068,exceptionMessageSource.getMessage("1068", null, Locale.US), null);
	}
	
	@ExceptionHandler(JpaOptimisticLockingFailureException.class)
	 public ActionResult getJpaOptimisticLockingFailureException(JpaOptimisticLockingFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1069", exceptionMessageSource.getMessage("1069", null, Locale.US), "JpaOptimisticLockingFailureException");
		return new ActionResult(null, 0, 0, 0L, 1069,exceptionMessageSource.getMessage("1069", null, Locale.US), null);
	}
	
//	@ExceptionHandler(JRubyScriptUtils.JRubyExecutionException.class)
//	 public ActionResult getJRubyScriptUtilsJRubyExecutionException(JRubyScriptUtils.JRubyExecutionException exp){
//		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1070", exceptionMessageSource.getMessage("1070", null, Locale.US), "JRubyScriptUtils.JRubyExecutionException");
//		return new ActionResult(null, 0, 0, 0, 1070, exceptionMessageSource.getMessage("1070", null, Locale.US), null);
//	}
//
	@ExceptionHandler(LobRetrievalFailureException.class)
	 public ActionResult getLobRetrievalFailureException(LobRetrievalFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1071", exceptionMessageSource.getMessage("1071", null, Locale.US), "LobRetrievalFailureException");
		return new ActionResult(null, 0, 0, 0L, 1071,exceptionMessageSource.getMessage("1071", null, Locale.US), null);
	}
/*	
	@ExceptionHandler(MailAuthenticationException.class)
	 public ActionResult getMailAuthenticationException(MailAuthenticationException exp){
		return new ActionResult(null, 0, 0, 0, exp.hashCode(), exp.getMessage(), null);
	}
	
	@ExceptionHandler(MailException.class)
	 public ActionResult getMailException(MailException exp){
		return new ActionResult(null, 0, 0, 0, exp.hashCode(), exp.getMessage(), null);
	}
	
	@ExceptionHandler(MailParseException.class)
	 public ActionResult getMailParseException(MailParseException exp){
		return new ActionResult(null, 0, 0, 0, exp.hashCode(), exp.getMessage(), null);
	}
	
	@ExceptionHandler(MailPreparationException.class)
	 public ActionResult getMailPreparationException(MailPreparationException exp){
		return new ActionResult(null, 0, 0, 0, exp.hashCode(), exp.getMessage(), null);
	}
	
	@ExceptionHandler(MailSendException.class)
	 public ActionResult getMailSendException(MailSendException exp){
		return new ActionResult(null, 0, 0, 0, exp.hashCode(), exp.getMessage(), null);
	}*/
	
	@ExceptionHandler(MaxUploadSizeExceededException.class)
	 public ActionResult getMaxUploadSizeExceededException(MaxUploadSizeExceededException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1072", exceptionMessageSource.getMessage("1072", null, Locale.US), "MaxUploadSizeExceededException");
		return new ActionResult(null, 0, 0, 0L, 1072,exceptionMessageSource.getMessage("1072", null, Locale.US), null);
	}
	
	@ExceptionHandler(MBeanConnectFailureException.class)
	 public ActionResult getMBeanConnectFailureException(MBeanConnectFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1073", exceptionMessageSource.getMessage("1073", null, Locale.US), "MBeanConnectFailureException");
		return new ActionResult(null, 0, 0, 0L, 1073,exceptionMessageSource.getMessage("1073", null, Locale.US), null);
	}
	
	@ExceptionHandler(MBeanExportException.class)
	 public ActionResult getMBeanExportException(MBeanExportException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1074", exceptionMessageSource.getMessage("1074", null, Locale.US), "MBeanExportException");
		return new ActionResult(null, 0, 0, 0L, 1074,exceptionMessageSource.getMessage("1074", null, Locale.US), null);
	}
	
	@ExceptionHandler(MBeanInfoRetrievalException.class)
	 public ActionResult getMBeanInfoRetrievalException(MBeanInfoRetrievalException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1075", exceptionMessageSource.getMessage("1075", null, Locale.US), "MBeanInfoRetrievalException");
		return new ActionResult(null, 0, 0, 0L, 1075,exceptionMessageSource.getMessage("1075", null, Locale.US), null);
	}
	
	@ExceptionHandler(MBeanServerNotFoundException.class)
	 public ActionResult getMBeanServerNotFoundException(MBeanServerNotFoundException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1076", exceptionMessageSource.getMessage("1076", null, Locale.US), "MBeanServerNotFoundException");
		return new ActionResult(null, 0, 0, 0L, 1076,exceptionMessageSource.getMessage("1076", null, Locale.US), null);
	}
	
	/*@ExceptionHandler(MessageConversionException.class)
	 public ActionResult getMessageConversionException(MessageConversionException exp){
		return new ActionResult(null, 0, 0, 0, exp.hashCode(), exp.getMessage(), null);
	}*/
	
	@ExceptionHandler(MetaDataAccessException.class)
	 public ActionResult getMetaDataAccessException(MetaDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1077", exceptionMessageSource.getMessage("1077", null, Locale.US), "MetaDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1077,exceptionMessageSource.getMessage("1077", null, Locale.US), null);
	}
	
	@ExceptionHandler(MethodInvocationException.class)
	 public ActionResult getMethodInvocationException(MethodInvocationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1078", exceptionMessageSource.getMessage("1078", null, Locale.US), "MethodInvocationException");
		return new ActionResult(null, 0, 0, 0L, 1078,exceptionMessageSource.getMessage("1078", null, Locale.US), null);
	}
	
	@ExceptionHandler(MultipartException.class)
	 public ActionResult getMultipartException(MultipartException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1079", exceptionMessageSource.getMessage("1079", null, Locale.US), "MultipartException");
		return new ActionResult(null, 0, 0, 0L, 1079,exceptionMessageSource.getMessage("1079", null, Locale.US), null);
	}
	
	@ExceptionHandler(NestedTransactionNotSupportedException.class)
	 public ActionResult getNestedTransactionNotSupportedException(NestedTransactionNotSupportedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1080", exceptionMessageSource.getMessage("1080", null, Locale.US), "NestedTransactionNotSupportedException");
		return new ActionResult(null, 0, 0, 0L, 1080,exceptionMessageSource.getMessage("1080", null, Locale.US), null);
	}
	
	@ExceptionHandler(NonTransientDataAccessException.class)
	 public ActionResult getNonTransientDataAccessException(NonTransientDataAccessException exp){
		System.out.println("Message is: "+exp.getMessage());
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1081", exceptionMessageSource.getMessage("1081", null, Locale.US), "NonTransientDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1081,exceptionMessageSource.getMessage("1081", null, Locale.US), null);
	}
	
	@ExceptionHandler(NonTransientDataAccessResourceException.class)
	 public ActionResult getNonTransientDataAccessResourceException(NonTransientDataAccessResourceException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1082", exceptionMessageSource.getMessage("1082", null, Locale.US), "NonTransientDataAccessResourceException");
		return new ActionResult(null, 0, 0, 0L, 1082,exceptionMessageSource.getMessage("1082", null, Locale.US), null);
	}
	
	@ExceptionHandler(NoSuchBeanDefinitionException.class)
	 public ActionResult getNoSuchBeanDefinitionException(NoSuchBeanDefinitionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1083", exceptionMessageSource.getMessage("1083", null, Locale.US), "NoSuchBeanDefinitionException");
		return new ActionResult(null, 0, 0, 0L, 1083,exceptionMessageSource.getMessage("1083", null, Locale.US), null);
	}
	
	@ExceptionHandler(NotAnAtAspectException.class)
	 public ActionResult getNotAnAtAspectException(NotAnAtAspectException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1084", exceptionMessageSource.getMessage("1084", null, Locale.US), "NotAnAtAspectException");
		return new ActionResult(null, 0, 0, 0L, 1084,exceptionMessageSource.getMessage("1084", null, Locale.US), null);
	}
	
	@ExceptionHandler(NoTransactionException.class)
	 public ActionResult getNoTransactionException(NoTransactionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1085", exceptionMessageSource.getMessage("1085", null, Locale.US), "NoTransactionException");
		return new ActionResult(null, 0, 0, 0L, 1085,exceptionMessageSource.getMessage("1085", null, Locale.US), null);
	}
	
	@ExceptionHandler(NotReadablePropertyException.class)
	 public ActionResult getNotReadablePropertyException(NotReadablePropertyException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1086", exceptionMessageSource.getMessage("1086", null, Locale.US), "NotReadablePropertyException");
		return new ActionResult(null, 0, 0, 0L, 1086,exceptionMessageSource.getMessage("1086", null, Locale.US), null);
	}
	
	@ExceptionHandler(NotWritablePropertyException.class)
	 public ActionResult getNotWritablePropertyException(NotWritablePropertyException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1087", exceptionMessageSource.getMessage("1087", null, Locale.US), "NotWritablePropertyException");
		return new ActionResult(null, 0, 0, 0L, 1087,exceptionMessageSource.getMessage("1087", null, Locale.US), null);
	}
	
	@ExceptionHandler(NullValueInNestedPathException.class)
	 public ActionResult getNullValueInNestedPathException(NullValueInNestedPathException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1088", exceptionMessageSource.getMessage("1088", null, Locale.US), "NullValueInNestedPathException");
		return new ActionResult(null, 0, 0, 0L, 1088,exceptionMessageSource.getMessage("1088", null, Locale.US), null);
	}
	
	@ExceptionHandler(ObjectOptimisticLockingFailureException.class)
	 public ActionResult getObjectOptimisticLockingFailureException(ObjectOptimisticLockingFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1089", exceptionMessageSource.getMessage("1089", null, Locale.US), "ObjectOptimisticLockingFailureException");
		return new ActionResult(null, 0, 0, 0L, 1089,exceptionMessageSource.getMessage("1089", null, Locale.US), null);
	}
	
	@ExceptionHandler(OptimisticLockingFailureException.class)
	 public ActionResult getOptimisticLockingFailureException(OptimisticLockingFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1090", exceptionMessageSource.getMessage("1090", null, Locale.US), "OptimisticLockingFailureException");
		return new ActionResult(null, 0, 0, 0L, 1090,exceptionMessageSource.getMessage("1090", null, Locale.US), null);
	}
	
	@ExceptionHandler(PermissionDeniedDataAccessException.class)
	 public ActionResult getPermissionDeniedDataAccessException(PermissionDeniedDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1091", exceptionMessageSource.getMessage("1091", null, Locale.US), "PermissionDeniedDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1091,exceptionMessageSource.getMessage("1091", null, Locale.US), null);
	}
	
	@ExceptionHandler(PessimisticLockingFailureException.class)
	 public ActionResult getPessimisticLockingFailureException(PessimisticLockingFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1092", exceptionMessageSource.getMessage("1092", null, Locale.US), "PessimisticLockingFailureException");
		return new ActionResult(null, 0, 0, 0L, 1092,exceptionMessageSource.getMessage("1092", null, Locale.US), null);
	}
	
	@ExceptionHandler(PropertyAccessException.class)
	 public ActionResult getPropertyAccessException(PropertyAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1093", exceptionMessageSource.getMessage("1093", null, Locale.US), "PropertyAccessException");
		return new ActionResult(null, 0, 0, 0L, 1093,exceptionMessageSource.getMessage("1093", null, Locale.US), null);
	}
	
	@ExceptionHandler(RecordTypeNotSupportedException.class)
	 public ActionResult getRecordTypeNotSupportedException(RecordTypeNotSupportedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1094", exceptionMessageSource.getMessage("1094", null, Locale.US), "RecordTypeNotSupportedException");
		return new ActionResult(null, 0, 0, 0L, 1094,exceptionMessageSource.getMessage("1094", null, Locale.US), null);
	}
	
	@ExceptionHandler(RecoverableDataAccessException.class)
	 public ActionResult getRecoverableDataAccessException(RecoverableDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1095", exceptionMessageSource.getMessage("1095", null, Locale.US), "RecoverableDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1095,exceptionMessageSource.getMessage("1095", null, Locale.US), null);
	}
	
	@ExceptionHandler(RemoteAccessException.class)
	 public ActionResult getRemoteAccessException(RemoteAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1021", exceptionMessageSource.getMessage("1096", null, Locale.US), "RemoteAccessException");
		return new ActionResult(null, 0, 0, 0L, 1096,exceptionMessageSource.getMessage("1096", null, Locale.US), null);
	}
	
	@ExceptionHandler(RemoteConnectFailureException.class)
	 public ActionResult getRemoteConnectFailureException(RemoteConnectFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1097", exceptionMessageSource.getMessage("1097", null, Locale.US), "RemoteConnectFailureException");
		return new ActionResult(null, 0, 0, 0L, 1097,exceptionMessageSource.getMessage("1097", null, Locale.US), null);
	}
	
	@ExceptionHandler(RemoteInvocationFailureException.class)
	 public ActionResult getRemoteInvocationFailureException(RemoteInvocationFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1098", exceptionMessageSource.getMessage("1098", null, Locale.US), "RemoteInvocationFailureException");
		return new ActionResult(null, 0, 0, 0L, 1098,exceptionMessageSource.getMessage("1098", null, Locale.US), null);
	}
	
	@ExceptionHandler(RemoteLookupFailureException.class)
	 public ActionResult getRemoteLookupFailureException(RemoteLookupFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1099", exceptionMessageSource.getMessage("1099", null, Locale.US), "RemoteLookupFailureException");
		return new ActionResult(null, 0, 0, 0L, 1099,exceptionMessageSource.getMessage("1099", null, Locale.US), null);
	}
	
	@ExceptionHandler(RemoteProxyFailureException.class)
	 public ActionResult getRemoteProxyFailureException(RemoteProxyFailureException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1100", exceptionMessageSource.getMessage("1100", null, Locale.US), "RemoteProxyFailureException");
		return new ActionResult(null, 0, 0, 0L, 1100,exceptionMessageSource.getMessage("1100", null, Locale.US), null);
	}
	
	@ExceptionHandler(ResourceAccessException.class)
	 public ActionResult getResourceAccessException(ResourceAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1101", exceptionMessageSource.getMessage("1101", null, Locale.US), "ResourceAccessException");
		return new ActionResult(null, 0, 0, 0L, 1101,exceptionMessageSource.getMessage("1101", null, Locale.US), null);
	}
	
	@ExceptionHandler(RestClientException.class)
	 public ActionResult getRestClientException(RestClientException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1102", exceptionMessageSource.getMessage("1102", null, Locale.US), "RestClientException");
		return new ActionResult(null, 0, 0, 0L, 1102,exceptionMessageSource.getMessage("1102", null, Locale.US), null);
	}
	
	@ExceptionHandler(SchedulingException.class)
	 public ActionResult getSchedulingException(SchedulingException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1103", exceptionMessageSource.getMessage("1103", null, Locale.US), "SchedulingException");
		return new ActionResult(null, 0, 0, 0L, 1103,exceptionMessageSource.getMessage("1103", null, Locale.US), null);
	}
	
	@ExceptionHandler(ScriptCompilationException.class)
	 public ActionResult getScriptCompilationException(ScriptCompilationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1104", exceptionMessageSource.getMessage("1104", null, Locale.US), "ScriptCompilationException");
		return new ActionResult(null, 0, 0, 0L, 1104,exceptionMessageSource.getMessage("1104", null, Locale.US), null);
	}
	
	@ExceptionHandler(SerializationFailedException.class)
	 public ActionResult getSerializationFailedException(SerializationFailedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1105", exceptionMessageSource.getMessage("1105", null, Locale.US), "SerializationFailedException");
		return new ActionResult(null, 0, 0, 0L, 1105,exceptionMessageSource.getMessage("1105", null, Locale.US), null);
	}
	
	@ExceptionHandler(SoapFaultException.class)
	 public ActionResult getSoapFaultException(SoapFaultException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1106", exceptionMessageSource.getMessage("1106", null, Locale.US), "SoapFaultException");
		return new ActionResult(null, 0, 0, 0L, 1106,exceptionMessageSource.getMessage("1106", null, Locale.US), null);
	}
	
	@ExceptionHandler(SQLWarningException.class)
	 public ActionResult getSQLWarningException(SQLWarningException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1107", exceptionMessageSource.getMessage("1107", null, Locale.US), "SQLWarningException");
		return new ActionResult(null, 0, 0, 0L, 1107,exceptionMessageSource.getMessage("1107", null, Locale.US), null);
	}
	
	@ExceptionHandler(SqlXmlFeatureNotImplementedException.class)
	 public ActionResult getSqlXmlFeatureNotImplementedException(SqlXmlFeatureNotImplementedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1108", exceptionMessageSource.getMessage("1108", null, Locale.US), "SqlXmlFeatureNotImplementedException");
		return new ActionResult(null, 0, 0, 0L, 1108,exceptionMessageSource.getMessage("1108", null, Locale.US), null);
	}
	
	@ExceptionHandler(TransactionException.class)
	 public ActionResult getTransactionException(TransactionException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1109", exceptionMessageSource.getMessage("1109", null, Locale.US), "TransactionException");
		return new ActionResult(null, 0, 0, 0L, 1109,exceptionMessageSource.getMessage("1109", null, Locale.US), null);
	}
	
	@ExceptionHandler(TransactionSuspensionNotSupportedException.class)
	 public ActionResult getTransactionSuspensionNotSupportedException(TransactionSuspensionNotSupportedException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1110", exceptionMessageSource.getMessage("1110", null, Locale.US), "TransactionSuspensionNotSupportedException");
		return new ActionResult(null, 0, 0, 0L, 1110,exceptionMessageSource.getMessage("1110", null, Locale.US), null);
	}
	
	@ExceptionHandler(TransactionSystemException.class)
	 public ActionResult getTransactionSystemException(TransactionSystemException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1111", exceptionMessageSource.getMessage("1111", null, Locale.US), "TransactionSystemException");
		return new ActionResult(null, 0, 0, 0L, 1111,exceptionMessageSource.getMessage("1111", null, Locale.US), null);
	}
	
	@ExceptionHandler(TransactionTimedOutException.class)
	 public ActionResult getTransactionTimedOutException(TransactionTimedOutException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1112", exceptionMessageSource.getMessage("1112", null, Locale.US), "TransactionTimedOutException");
		return new ActionResult(null, 0, 0, 0L, 1112,exceptionMessageSource.getMessage("1112", null, Locale.US), null);
	}
	
	@ExceptionHandler(TransactionUsageException.class)
	 public ActionResult getTransactionUsageException(TransactionUsageException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1113", exceptionMessageSource.getMessage("1113", null, Locale.US), "TransactionUsageException");
		return new ActionResult(null, 0, 0, 0L, 1113,exceptionMessageSource.getMessage("1113", null, Locale.US), null);
	}
	
	@ExceptionHandler(TransientDataAccessException.class)
	 public ActionResult getTransientDataAccessException(TransientDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1114", exceptionMessageSource.getMessage("1114", null, Locale.US), "TransientDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1114,exceptionMessageSource.getMessage("1114", null, Locale.US), null);
	}
	
	@ExceptionHandler(TransientDataAccessResourceException.class)
	 public ActionResult getTransientDataAccessResourceException(TransientDataAccessResourceException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1115", exceptionMessageSource.getMessage("1115", null, Locale.US), "TransientDataAccessResourceException");
		return new ActionResult(null, 0, 0, 0L, 1115,exceptionMessageSource.getMessage("1115", null, Locale.US), null);
	}
	
	@ExceptionHandler(TypeMismatchDataAccessException.class)
	 public ActionResult getTypeMismatchDataAccessException(TypeMismatchDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1116", exceptionMessageSource.getMessage("1116", null, Locale.US), "TypeMismatchDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1116,exceptionMessageSource.getMessage("1116", null, Locale.US), null);
	}
	
	@ExceptionHandler(TypeMismatchException.class)
	 public ActionResult getTypeMismatchException(TypeMismatchException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1117", exceptionMessageSource.getMessage("1117", null, Locale.US), "TypeMismatchException");
		return new ActionResult(null, 0, 0, 0L, 1117,exceptionMessageSource.getMessage("1117", null, Locale.US), null);
	}
	
	@ExceptionHandler(UnableToRegisterMBeanException.class)
	 public ActionResult getUnableToRegisterMBeanException(UnableToRegisterMBeanException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1118", exceptionMessageSource.getMessage("1118", null, Locale.US), "UnableToRegisterMBeanException");
		return new ActionResult(null, 0, 0, 0L, 1118,exceptionMessageSource.getMessage("1118", null, Locale.US), null);
	}
	
	@ExceptionHandler(UnableToSendNotificationException.class)
	 public ActionResult getUnableToSendNotificationException(UnableToSendNotificationException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1119", exceptionMessageSource.getMessage("1119", null, Locale.US), "UnableToSendNotificationException");
		return new ActionResult(null, 0, 0, 0L, 1119,exceptionMessageSource.getMessage("1119", null, Locale.US), null);
	}
	
	@ExceptionHandler(UncategorizedDataAccessException.class)
	 public ActionResult getUncategorizedDataAccessException(UncategorizedDataAccessException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1120", exceptionMessageSource.getMessage("1120", null, Locale.US), "UncategorizedDataAccessException");
		return new ActionResult(null, 0, 0, 0L, 1120,exceptionMessageSource.getMessage("1120", null, Locale.US), null);
	}
	
	@ExceptionHandler(UncategorizedSQLException.class)
	 public ActionResult getUncategorizedSQLException(UncategorizedSQLException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1121", exceptionMessageSource.getMessage("1121", null, Locale.US), "UncategorizedSQLException");
		return new ActionResult(null, 0, 0, 0L, 1121,exceptionMessageSource.getMessage("1121", null, Locale.US), null);
	}
	
	@ExceptionHandler(UnexpectedRollbackException.class)
	 public ActionResult getUnexpectedRollbackException(UnexpectedRollbackException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1122", exceptionMessageSource.getMessage("1122", null, Locale.US), "UnexpectedRollbackException");
		return new ActionResult(null, 0, 0, 0L, 1122,exceptionMessageSource.getMessage("1122", null, Locale.US), null);
	}
	
	@ExceptionHandler(UnsatisfiedDependencyException.class)
	 public ActionResult getUnsatisfiedDependencyException(UnsatisfiedDependencyException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1123", exceptionMessageSource.getMessage("1123", null, Locale.US), "UnsatisfiedDependencyException");
		return new ActionResult(null, 0, 0, 0L, 1123,exceptionMessageSource.getMessage("1123", null, Locale.US), null);
	}
	
	@ExceptionHandler(XmlBeanDefinitionStoreException.class)
	 public ActionResult getXmlBeanDefinitionStoreException(XmlBeanDefinitionStoreException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1124", exceptionMessageSource.getMessage("1124", null, Locale.US), "XmlBeanDefinitionStoreException");
		return new ActionResult(null, 0, 0, 0L, 1124,exceptionMessageSource.getMessage("1124", null, Locale.US), null);
	}
	
	/**
	 * Hibernate Exception Handler Block
	 * @param exp
	 * @return Permission Result
	 */
	/*@ExceptionHandler(ConstraintViolationException.class)
	 public ActionResult getConstraintViolationException(ConstraintViolationException exp){
		switch (exp.getErrorCode()){
				*//**
				 * Find Child Record Integrity Exception Handler
				 *//*
				case 2292:
					applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1125", exceptionMessageSource.getMessage("1125", null, Locale.US), "Find Child Record");
					return new ActionResult(null, 0, 0, 0, 1125,exceptionMessageSource.getMessage("1125", null, Locale.US), null);
				*//**
				 * Duplicate Key Violation  Exception Handler
				 *//*
				case 1:
					applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1126", exceptionMessageSource.getMessage("1126", null, Locale.US), "Duplicate Key Insert");
					return new ActionResult(null, 0, 0, 0, 1126,exceptionMessageSource.getMessage("1126", null, Locale.US), null);
		
		}
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1125", exceptionMessageSource.getMessage("1125", null, Locale.US), "XmlBeanDefinitionStoreException");
		return new ActionResult(null, 0, 0, 0, 1125,exceptionMessageSource.getMessage("1125", null, Locale.US), null);
	}*/
	//@TODO: all types of ApplicationException should be set here!
	@ExceptionHandler(ApplicationException.class)
	 public ActionResult getApplicationException(ApplicationException exp){
		
		if(exp.getExpCode()==3000){
		  if(exp.getExpMessage()==null){
			applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, exp.getExpCode()+"", exp.getExpMessage(), exp.getEntityName());
			return new ActionResult(null, 0, 0, 0L, exp.getExpCode(),exceptionMessageSource.getMessage("3000", null, Locale.US), null);
		  } else if(exp.getExpMessage()!=null){
			  applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, exp.getExpCode()+"",exceptionMessageSource.getMessage("3001", null, Locale.US)+"--"+ exp.getExpMessage(), exp.getEntityName());
			  return new ActionResult(null, 0, 0, 0L, exp.getExpCode(),exceptionMessageSource.getMessage("3001", null, Locale.US)+"--"+exp.getExpMessage(), null);
		  }
		}
		
		 if(exp.getExpCode()==2010){
		    applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, exp.getExpCode()+"", exceptionMessageSource.getMessage(exp.getExpCode()+"", null, Locale.US), exp.getEntityName());
		    return new ActionResult(null, 0, 0, 0L, exp.getExpCode(),exceptionMessageSource.getMessage(exp.getExpCode()+"", null, Locale.US)+"( "+exceptionMessageSource.getMessage("1", null, Locale.US)+exp.getExpMessage()+" )", null);
		 }


		 
		    applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, exp.getExpCode()+"", exceptionMessageSource.getMessage(exp.getExpCode()+"", null, Locale.US), exp.getEntityName());
		    return new ActionResult(null, 0, 0, 0L, exp.getExpCode(),exp.getExpMessage(), null);
	}
	
	/*@ExceptionHandler(IllegalStateException.class)
	 public ActionResult getIllegalStateException(IllegalStateException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1127", exceptionMessageSource.getMessage("1127", null, Locale.US), "IllegalStateException");
		return new ActionResult(null, 0, 0, 0, 1127,exceptionMessageSource.getMessage("1127", null, Locale.US), null);
	}*/
	
	@ExceptionHandler(IOException.class)
	 public ActionResult getIOException(IOException exp){
		applicationLogger.printLog("error", ApplicationExceptionController.class,null, null, null, "1128", exceptionMessageSource.getMessage("1128", null, Locale.US), "IOException");
		return new ActionResult(null, 0, 0, 0L, 1128,exceptionMessageSource.getMessage("1128", null, Locale.US), null);
	}
//    @ExceptionHandler(InvalidDataAccessResourceUsageException.class)
//    public ActionResult handleInvalidDataAccessResourceUsageException(InvalidDataAccessResourceUsageException e){
//        applicationLogger.printLog("error", InvalidDataAccessResourceUsageException.class,null, null, null, "1128","تابع انتخابی با نوع داده همخوانی ندارد.", "InvalidDataAccessResourceUsageException");
//        return new ActionResult(null, 0, 0, 0L, 1128,"تابع انتخابی با نوع داده همخوانی ندارد.", null);
//
//    }
}
