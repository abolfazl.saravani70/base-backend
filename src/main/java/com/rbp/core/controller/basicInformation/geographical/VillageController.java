/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.controller.basicInformation.geographical;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Village;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.model.dto.basicInformation.geographical.VillageDTO;
import com.rbp.core.service.basicInformation.geographical.IVillageService;
import com.rbp.core.service.basicInformation.geographical.impl.VillageService;
import com.rbp.core.utility.ApplicationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.VILLAGES)
@Api(description = "Operations pertaining to village in Village Management System")
public class VillageController extends FrameworkAbstractController<Village, VillageDTO, VillageService> {
    @Autowired
    IVillageService iVillageService;

    @PostMapping(value = restConstant.CITY_ID)
    @ApiOperation(value = "get list of village by city id")
    public ActionResult getCityType(@RequestBody JsonInput<VillageDTO> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را وارد کنید");
        }
        getApplicationLogger().printLog("info", VillageController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByCityId()", null, null, "Method is starting....");
        List<Village> list = iVillageService.getByCityId(jsonInput.getFieldId());
        List<VillageDTO> objectList = getMapper().getMapper(VillageDTO.class , Village.class).toView(list);
        getApplicationLogger().printLog("info", VillageController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByCityId()", null, null, "Method finish successfull!!!!");
        return new ActionResult(Collections.singletonList(objectList), 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }
}
