/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.controller.basicInformation.bankingInfo;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Bank;
import com.rbp.core.model.dto.basicInformation.bankinginfo.BankDTO;
import com.rbp.core.service.basicInformation.bankingInfo.impl.BankService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.BANKS)
@Api(description = "Operations pertaining to bank in Bank Management System")
public class BankController extends FrameworkAbstractController<Bank, BankDTO, BankService> {
}
