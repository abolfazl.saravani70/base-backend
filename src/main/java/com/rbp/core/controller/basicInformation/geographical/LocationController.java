package com.rbp.core.controller.basicInformation.geographical;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Location;
import com.rbp.core.model.dto.basicInformation.geographical.LocationDTO;
import com.rbp.core.service.basicInformation.geographical.impl.LocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.LOCATIONS)
@Api(description = "Operations pertaining to location in GEO Management System")
public class LocationController extends FrameworkAbstractController<Location, LocationDTO, LocationService> {
}
