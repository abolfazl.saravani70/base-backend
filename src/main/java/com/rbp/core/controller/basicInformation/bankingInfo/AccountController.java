/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.controller.basicInformation.bankingInfo;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Account;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.model.viewModel.basicinformation.bankingInfo.AccountViewModel;
import com.rbp.core.service.basicInformation.bankingInfo.impl.AccountService;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.controller.organization.OrganizationController;
import com.rbp.sayban.service.organization.IOrganizationAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACCOUNTS)
@Api(description = "Operations pertaining to account in Account Management System")
public class AccountController extends FrameworkAbstractController<Account, AccountViewModel, AccountService> {

    @Autowired
    IOrganizationAccountService iOrganizationAccountService;


    @Override
    protected ActionResult saveAndUpdate(JsonInput<AccountViewModel> jsonInput) {
        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), AccountViewModel.class.getSimpleName(), "getService().insertAccount()", null, null, "Method is starting....");
        AccountViewModel list = iOrganizationAccountService.insertNewAccount(jsonInput.getData());
        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), AccountViewModel.class.getSimpleName(), "getService().insertAccount()", null, null, "Method finish successfull!!!!");
        return new ActionResult(Collections.singletonList(list), 0, 0, 0L, 0, "", "");
    }


    @PostMapping(restConstant.ORG_ID)
    @ApiOperation("get bank accounts by organization id")
    protected ActionResult getAccountsByOrgId(@RequestBody JsonInput<AccountViewModel> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را وارد کنید");
        }

        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), AccountViewModel.class.getSimpleName(), "getService().getAccountDetails()", null, null, "Method is starting....");
        List<AccountViewModel> list = iOrganizationAccountService.getAccountsDetail(jsonInput.getFieldId());
        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), AccountViewModel.class.getSimpleName(), "getService().getAccountDetails()", null, null, "Method finish successfull!!!!");
        return new ActionResult(Collections.singletonList(list), 0, 0, 0L, 0, "", "داده های مورئ نظر یافت نشدند");
    }

  }
