/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.controller.basicInformation.geographical;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.geographical.City;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.model.dto.basicInformation.geographical.CityDTO;
import com.rbp.core.service.basicInformation.geographical.ICityService;
import com.rbp.core.service.basicInformation.geographical.impl.CityService;
import com.rbp.core.utility.ApplicationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.CITIES)
@Api(description = "Operations pertaining to city in City Management System")
public class CityController extends FrameworkAbstractController<City, CityDTO, CityService> {
    @Autowired
    ICityService iCityService;

    @PostMapping(value = restConstant.STATE_ID)
    @ApiOperation(value = "get list of city by state id")
    public ActionResult getStateType(@RequestBody JsonInput<CityDTO> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را وارد کنید");
        }
        getApplicationLogger().printLog("info", CityController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByStateId()", null, null, "Method is starting....");
        List<City> list = iCityService.getByStateId(jsonInput.getFieldId());
        List<CityDTO> objectList = getMapper().getMapper(CityDTO.class , City.class).toView(list);
        getApplicationLogger().printLog("info", CityController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByStateId()", null, null, "Method finish successfull!!!!");
        return new ActionResult(Collections.singletonList(objectList), 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }
}
