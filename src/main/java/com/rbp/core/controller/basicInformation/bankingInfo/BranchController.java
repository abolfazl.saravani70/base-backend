/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.controller.basicInformation.bankingInfo;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Branch;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.model.dto.basicInformation.bankinginfo.BranchDTO;
import com.rbp.core.service.basicInformation.bankingInfo.IBranchService;
import com.rbp.core.service.basicInformation.bankingInfo.impl.BranchService;
import com.rbp.core.utility.ApplicationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.BRANCHES)
@Api(description = "Operations pertaining to branch in Branch Management System")
public class BranchController extends FrameworkAbstractController<Branch, BranchDTO, BranchService> {

    @Autowired
    IBranchService iBranchService;

    @PostMapping(value = restConstant.BANK_ID)
    @ApiOperation(value = "get list of Branches by Bank id")
    public ActionResult getBranchesByBankId(@RequestBody JsonInput<Branch> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را وارد کنید");
        }
        getApplicationLogger().printLog("info", BranchController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getBranchesByBankId()", null, null, "Method is starting....");
        List<Branch> list = iBranchService.getBranchesByBankId(jsonInput.getFieldId());
        List<BranchDTO> objectList = getMapper().getMapper(BranchDTO.class , Branch.class).toView(list);
        getApplicationLogger().printLog("info", BranchController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getBranchesByBankId()", null, null, "Method finish successfull!!!!");
        return new ActionResult(Collections.singletonList(objectList), 0, 0, 0L, 0, "", "داده های مورد نظر یافت شدند");
    }
}
