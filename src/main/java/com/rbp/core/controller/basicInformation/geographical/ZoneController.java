/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.controller.basicInformation.geographical;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Zone;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.model.dto.basicInformation.geographical.ZoneDTO;
import com.rbp.core.service.basicInformation.geographical.IZoneService;
import com.rbp.core.service.basicInformation.geographical.impl.ZoneService;
import com.rbp.core.utility.ApplicationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.ZONES)
@Api(description = "Operations pertaining to zone in Zone Management System")
public class ZoneController extends FrameworkAbstractController<Zone, ZoneDTO, ZoneService> {
    @Autowired
    IZoneService iZoneService;

    @PostMapping(value = restConstant.VILLAGE_ID)
    @ApiOperation(value = "get list of zone by village id")
    public ActionResult getVillageType(@RequestBody JsonInput<ZoneDTO> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را وارد کنید");
        }
        getApplicationLogger().printLog("info", ZoneController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByVillageId()", null, null, "Method is starting....");
        List<Zone> list = iZoneService.getByVillageId(jsonInput.getFieldId());
        List<ZoneDTO> objectList = getMapper().getMapper(ZoneDTO.class , Zone.class).toView(list);
        getApplicationLogger().printLog("info", ZoneController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByVillageId()", null, null, "Method finish successfull!!!!");
        return new ActionResult(Collections.singletonList(objectList), 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }

    //@TODO: check this! token is passed as null.
//    @Override
//    protected ActionResult saveAndUpdate(ZoneDTO zoneDTO) {
//        if (zoneDTO.isCityZone()) {
//            if (zoneDTO.getCityId() == null)
//                return new ActionResult(null, 0, 0, 0, 0, "City id has not been set!", null);
//        } else {
//            if (zoneDTO.getVillageId() == null)
//                return new ActionResult(null, 0, 0, 0, 0, "Village id has not been set!", null);
//        }
//        return super.saveAndUpdate(zoneDTO);
//    }

}
