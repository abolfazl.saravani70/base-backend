package com.rbp.core.controller.flowable;


import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.flowable.ProcessDefinitionDTO;
import com.rbp.core.model.domainmodel.flowable.ProcessInstanceDTO;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.service.flowable.FlowableProcessService;
import com.rbp.sayban.model.domainmodel.system.ProcessInstance;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = restConstant.BASE_REST+restConstant.FLOWABLE_PROCESS)
public class FlowableProcessController {
    @Autowired
    FlowableProcessService service;

    @Value("${flowableAddress}")
    String flowable;

    /**
     * returns all the available process definitions. No restrictions
     * @return
     */
    @PostMapping("/getDefinitions")
    @ApiOperation("returns all the process definition names and keys")
    public ActionResult<ProcessDefinitionDTO> getAllDefinitions(){
        return service.getAllDefinitions();
    }

    /**
     * Fill the ProcessInstanseDTO with process Definition key and get a process instance started.
     * @param process
     * @return
     */
    @PostMapping("/startByProcessDefinitionKey")
    @ApiOperation("start a process instance by it's definition key. the initiator user and all other process variables must be sent.")
    public ActionResult<ProcessInstanceDTO> startProcessInstanceByProcessDefinitionKey(@RequestBody ProcessInstanceDTO process){
        return service.startProcessInstanceByProcessDefinitionKey(process);
    }

    /**
     * return a list of process definitions that can be started by a particular group.
     * @param groupId
     * @return
     */
    @PostMapping("/getProcessDefinitionByCandidateGroup")
    @ApiOperation("given a groupId, return all the process members of that group can start.")
    public ActionResult<ProcessDefinitionDTO> getCandidateStartersForProcessDefinition(@RequestParam String groupId){
        return service.getCandidateStartersForProcessDefinition(groupId);
    }

    /**
     * get all the variables for a process inside a Map<String,Object>
     * @param processInstanceId
     * @return
     */
    @PostMapping("/getVariables")
    @ApiOperation("get all the process variables by ProcessInstance Id")
    public ActionResult<Map<String,Object>> getVariables(@RequestBody ProcessInstanceDTO processInstanceId){
        return service.getVariables(processInstanceId.getProcessInstanceId());
   }

    /**
     * return all the process definitions specified by the category
     * @param category
     * @return
     */
    @PostMapping("/getProcessDefinitionByCategory")
    @ApiOperation("return all process definitions by the specified category")
    public ActionResult<ProcessDefinitionDTO> getProcessDefinitionsByCategory(@RequestParam String category){
        return service.getProcessDefinitionsByCategory(category);
    }

    /**
     * Given a task, return the execution id in which this task is active. if there are no parallel executions, this will be the process ID
     * @param taskId
     * @return
     */
    @PostMapping("/getProcessInstanceByTaskId")
    @ApiOperation("get a process DTO using task id")
    public ActionResult<ProcessInstanceDTO> getProcessByTaskId(@RequestParam String taskId){
        return service.getProcessByTaskId(taskId);
   }

    /**
     * Delete a process Instance by process instanceId, you can also send a reason to why this process has been terminated.
     * @param processId
     * @param reason
     * @return
     */
    @PostMapping("/cancelProcess")
    @ApiOperation("This will cancel a process given a process instance id")
    public ActionResult<ProcessInstanceDTO> cancelProcessInstance(@RequestParam String processId,@RequestParam String reason){
        return service.cancelProcessInstance(processId, reason);
    }

    /**
     * get a full process instance DTO with the given process business key
     * @param businessKey
     * @return
     */
    @PostMapping("/getInfo")
    @ApiOperation("get a process instance info by process business key, only possible if you set a process business key when starting a process insntace")
    public ActionResult<ProcessInstanceDTO> getInfo(@RequestParam String businessKey){
        return service.getInfo(businessKey);
    }

    /**
     * given a process instance, there might be alot of other executions inside of it. return back those executions.
     * Note: a process instance with 1 running execution has the same ids for process instance and execution id. hence this will return the process instance id.
     * @param processInstnaceId
     * @return
     */
    @PostMapping("/getExecutionIds")
    @ApiOperation("get a list of the execution ids associated with this process instance")
    public ActionResult<String> getExecutionIds(@RequestParam  String processInstnaceId){
        return service.getExecutionIds(processInstnaceId);
    }
}
