package com.rbp.core.controller.flowable;


import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.flowable.TaskDTO;
import com.rbp.core.model.domainmodel.flowable.TaskDueDateEnum;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.service.flowable.FlowableTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * DO NOT CONFUSE THE TASK_DTO used by flowabe by the TASK_DTO defined in Sayban!
 */
//TODO: added primitive operations on tasks still require to handle more complex scenarios.
@RestController
@RequestMapping(value = restConstant.BASE_REST+restConstant.FLOWABLE_TASK)
@Api("Controller responsible for connecting to flowable apis ")
public class FlowableTaskController {

    @Autowired
    FlowableTaskService service;

    /**
     * this endpoint returns a list of tasks for a user. these tasks are:
     * tasks that this user is an assignee
     * @param userId
     * @return
     */
    //Read Comments on the impementation
    //All of them are similar mostly
    @PostMapping("/assignedUserId")
    @ApiOperation("returns Tasks that this user is a assignee")
    public ActionResult<TaskDTO> getAssignedTaskByUserId(@RequestParam String userId){
        return service.getAssignedTaskByUserId(userId);
    }

    /**
     * provided a user Id as a string, this returns all the task this user Can be assigned to
     * Tasks for groups this user is a member of and tasks that this user directly is a candidate for
     * @param userId
     * @return
     */
    @PostMapping("/candidateUserId")
    @ApiOperation("Returns tasks that this user is a candidate of")
    public ActionResult<TaskDTO> getCandidateTasksByUserId(@RequestParam String userId){
       return service.getCandidateTasksByUserId(userId);
    }
    /**
     * Given a groupId returns all the tasks that members of this group can start.
     * @param groupId
     * @return
     */
    @PostMapping("/candidateGroupId")
    @ApiOperation("returns Tasks that this GroupId is a candidate for")
    public ActionResult<TaskDTO> getTaskByGroupId(@RequestParam String groupId){
       return   service.getTaskByGroupId(groupId);
    }

    /**
     * Claim a task with TaskId and UserId, this method will check, if by the time this user requests to claim this task, no one else has been assigned to it.
     * if they do, it will return an exception! if you want to set assignee no matter what, use force claim.
     * @param userId
     * @param task
     * @return
     */
    @PostMapping("/claim")
    @ApiOperation("given a task ID, Claim the task ->this means the assignee with be the user sent. it will check if the task has not been claimed by any else!")
    public ActionResult<TaskDTO> claimTaskByTaskId(@RequestParam String userId,@RequestBody String task){
        return service.claimTaskByTaskId(userId,task);
    }

    /**
     * Force claim a task by Task Id and User Id. This will not CHECK if the task has already been assigned. meaning that the assignee will be change no matter what.
     * Use this to forcibly claim a task no matter the circumstances.
     * @param userId
     * @param task
     * @return
     */
    @PostMapping("/forceClaim")
    @ApiOperation("Force to claim the task by taskId, this will not check if there is another user assigned to this taks")
    public ActionResult<TaskDTO> forceClaimTaskByTaskId(@RequestParam String userId,@RequestParam String task){
        return service.forceClaimTaskByTaskId(userId,task);
    }

    /**
     * Using a userID and a taskDTO complete a task.
     * Complete a task by taskId, also you have to send data inside a TaskDTO, fill Data and Task ID inside the DTO
     * @param userId
     * @param task
     * @return
     */
    @PostMapping("/complete")
    @ApiOperation("Complete a task by taskId, also you have to send data inside a TaskDTO, fill Data and Task ID inside the DTO")
    public ActionResult<TaskDTO> completeTask(@RequestParam String userId,@RequestParam String task){
        return service.completeTask(userId,task);
    }

    /**
     * get data for a task inside a map<string,Object> with taskId
     * @param taskId
     * @return
     */
    @PostMapping("/getVariables")
    @ApiOperation("Get variables of a task with task Id, it will return a map with <String,Object>")
    public ActionResult<Map<String,Object>> getVariables(@RequestParam String taskId){
       return service.getVariables(taskId);
    }

    /**
     * returns the list of all the un-assigned tasks
     * @return
     */
    @PostMapping("/getAllUnassigned")
    @ApiOperation("Return a list of all the unassigned tasks")
    public ActionResult<TaskDTO> getAllUnAssignedTasks(){
        return  service.getAllUnAssignedTasks();
    }

    /**
     * send a user list and add them as candidate users that can get that task. (be assigned to)
     * it DOES NOT remove the previous identity links!
     * @param userList
     * @param taskId
     * @return
     */
    @PostMapping("/setUserListAsCandidate")
    @ApiOperation("Set list of users as candidate for a task (it will eliminate all other identityLInks including groups!)")
    public ActionResult<TaskDTO> setUserListAsCandidate(@RequestParam List<String> userList,@RequestParam String taskId){
        return service.setUserListAsCandidate(userList,taskId);
    }

    /**
     * Set a group as candidate for a task
     * it DOES NOT remove the previous identity links!
     * @param groupList
     * @param taskId
     * @return
     */
    @PostMapping("/setGroupListAsCandidate")
    @ApiOperation("send a candidate and set it for a task with task ID and candidate group ID")
    public ActionResult<TaskDTO> setGroupListAsCandidate(@RequestParam List<String> groupList, @RequestParam String taskId){
       return service.setGroupListAsCandidate(groupList, taskId);
    }

    /***
     * setting local variables for a task. NOTE: if a variable doesn't already exist in the task, it will be created!
     * @param dto
     * @return
     */
    @PostMapping("/setTaskVariables")
    @ApiOperation("sending variables to be set for LOCAL task variables. this works for process INSTANCES not process definition tasks ")
    public ActionResult<TaskDTO> setTaskVariables(@RequestBody TaskDTO dto) {
        return service.setTaskVariables(dto);
    }


    /**
     * given a process instance, return the active tasks for that process (Current tasks for that process).
     * @param processInstanceId
     * @return
     */
    @PostMapping("/getCurrentTaskByProcessId")
    @ApiOperation("get current task for a process instance (Or active tasks for that process.) it will include TASK identityLinks as well.")
    public ActionResult<TaskDTO> getCurrentTasks(@RequestParam String processInstanceId) {
        return service.getCurrentTasks(processInstanceId);
    }

    /**
     * returns the list of tasks defined inside a process definition
     * @param processDefinitionId
     * @return
     */
    @PostMapping("/getAllTaskByProcessDefinitionId")
    @ApiOperation("return all the user tasks available inside a process definition")
    public ActionResult<TaskDTO> getAllTasks(@RequestParam String processDefinitionId){
        return service.getAllTasks(processDefinitionId);
    }

    /**
     * Get the current task of this execution id
     * @param executionId
     * @return
     */
    @PostMapping("/getAllTaskByExecutionId")
    @ApiOperation("Given an execution id, return all of its current tasks. ProcessInstance can be executionId if there is only one execution inside a process")
    public ActionResult<TaskDTO> getAllTasksByExecutionId(@RequestParam String executionId){
        return service.getAllTasksByExecutionId(executionId);
    }

    /**
     * this will remove all the identity links from the task
     * @param taskId
     * @return
     */
    @PostMapping("/removeIdentityLinks")
    @ApiOperation("sometimes you might need to delete the default identityLink provided inside bpmn")
    public ActionResult<TaskDTO> removeIdentityLinks(@RequestParam String taskId){
        return service.removeIdentityLinks(taskId);
    }
    //**************Indicator Controllers************************
    //controllers to set color thresholds for UI (RED GREEN YELLOW on TASKS)
    @PostMapping("/setRedIndicator")
    @ApiOperation("set a value for red indicator enum")
    public ActionResult<TaskDTO> setRedIndicator(@RequestParam int redIndicator){
        TaskDueDateEnum.setRedIndicator(redIndicator);
        return new ActionResult<>(null,0,0,0L,0,"عملیات با موفقیت انجام شد.","");
    }
    @PostMapping("/setYellowIndicator")
    @ApiOperation("set a value for red indicator enum")
    public ActionResult<TaskDTO> setYellowIndicator(@RequestParam int yellowIndicator){
        TaskDueDateEnum.setYellowIndicator(yellowIndicator);
        return new ActionResult<>(null,0,0,0L,0,"عملیات با موفقیت انجام شد.","");
    }
    @PostMapping("/setGreenIndicator")
    @ApiOperation("set a value for red indicator enum")
    public ActionResult<TaskDTO> setGreenIndicator(@RequestParam int greenIndicator){
        TaskDueDateEnum.setGreenIndicator(greenIndicator);
        return new ActionResult<>(null,0,0,0L,0,"عملیات با موفقیت انجام شد.","");
    }
    //**************End of Indicator Post**********************

    /**
     * complete a task and set the user for the list of the next upcoming tasks!
     * be warned as this will set this user assignee for all of the next tasks!
     * In case of parallel gateway, all of the next tasks will take this user as assignee!
     * @param userId
     * @param task
     * @param nextUserId
     * @return
     */
    @PostMapping("/completeAndSetNextAssignee")
    @ApiOperation("comepete a task and set the user as the next assignee. you will recieve a TaskDTO of that next task")
    public ActionResult<TaskDTO> completeAndSetNextAssignee(@RequestParam String userId,@RequestBody TaskDTO task,@RequestParam String nextUserId){
        return service.completeAndSetNextAssignee(userId, task, nextUserId);
    }


    /**
     * this method completes a task and then sets a zone code for the next level tasks!
     * this doest not consult the process variable! and it will override it if there is any.
     * @param task
     * @param zone
     * @return
     */
    @PostMapping("/completeAndSetNextGroupAssignmentByZone") //this will set Zone for next level TASKS!
    @ApiOperation("this will set the zone code provided by the user. this will override the existing group. " +
            "the existing group candidate can be the default group specified by the BPMN or over-written by Process Zone.")
    public ActionResult<TaskDTO> completeAndSetNextGroupAssignmentByZone(@RequestParam String userId,@RequestBody TaskDTO task,
                                                                         @RequestParam String zone){
        return service.completeAndSetNextGroupAssignmentByZone(userId, task, zone);
    }

    @PostMapping("/unclaim")
    @ApiOperation("clears the assignee for a user.")
    public ActionResult<TaskDTO> unclaim(@RequestParam String taskId){
        return service.unclaim(taskId);
    }
    @PostMapping("/getInfo")
    @ApiOperation("Returns a DTO with all information about a task using TASK id")
    public ActionResult<TaskDTO> getInfo(@RequestParam String taskId){
        return service.getInfo(taskId);
     }

    @PostMapping("/setDueDateOfNextTasks")
    @ApiOperation("set a due date for an execution or process instance. it will return a list of the tasks that was affected!")
    public ActionResult<TaskDTO> setDueDateOfNextTasks(@RequestParam String executionId,@RequestParam Date dueDate){
        return service.setDueDateOfNextTasks(executionId,dueDate);
    }
}
