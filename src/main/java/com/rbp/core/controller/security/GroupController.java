package com.rbp.core.controller.security;
/**
 * @author Alireza Souhani 1398.02.01
 */

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.core.model.dto.security.GroupDTO;
import com.rbp.core.service.security.IGroupService;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value = restConstant.BASE_REST + restConstant.GROUPS)
@Api(description = "Operations pertaining to group in Group Management System")
public class GroupController extends FrameworkAbstractController<Group, GroupDTO, IGroupService> {

//    @SuppressWarnings("unchecked")
//    @PostMapping(value = restConstant.WITHOUT_PERMISSIONS)
//    @ApiOperation(value = "View of available groups without permission")
//    public ActionResult getGroupsWithoutPermission() {
//        IGroupService iGroupService = (IGroupService) getService();
//        getApplicationLogger().printLog("info", GroupController.class, Group.class.getSimpleName(), GroupViewModel.class.getSimpleName(), "getService().getGroupsWithoutPermission()", null, null, "Method is starting....");
//        //List<GroupViewModel> objectList= ModelMapper.mapList(iGroupService.getGroupsWithoutPermission(), GroupViewModel.class) ;
//        List<Group> listGroup = iGroupService.getGroupsWithoutPermission();
//        List<GroupViewModel> objectList = getMapper().getMapper(GroupViewModel.class, Group.class).toView(listGroup);
//        getApplicationLogger().printLog("info", GroupController.class, Group.class.getSimpleName(), GroupViewModel.class.getSimpleName(), "getService().getGroupsWithoutPermission()", null, null, "Method is starting....");
//        return new ActionResult((List<Object>) (Object) objectList, 0, 0, 0, 0, "", "");
//    }
//
//    @SuppressWarnings("unchecked")
////    @PostMapping(value = restConstant.ID+restConstant.PERMISSIONS)
////    @ApiOperation(value = "get a list of permissions for  group id ")
////    public ActionResult getGroupPermission(JsonInput<Group> groupJsonInput) {
////        IGroupService iGroupService = (IGroupService) getService();
////        Long id=groupJsonInput.getFieldId();
////        getApplicationLogger().printLog("info", GroupController.class, Group.class.getSimpleName(), GroupViewModel.class.getSimpleName(), "getService().getChildrenGroups()", null, null, "Method is starting....");
////        // List<PermissionViewModel> objectList = ModelMapper.mapList(iGroupService.getChildrenGroups(groupId), PermissionViewModel.class);
////        List<Group> listGroup = iGroupService.getChildrenGroups(id);
////        List<GroupViewModel> objectList = getMapper().getMapper(GroupViewModel.class, Group.class).toView(listGroup);
////        getApplicationLogger().printLog("info", GroupController.class, Group.class.getSimpleName(), GroupViewModel.class.getSimpleName(), "getService().getChildrenGroups()", null, null, "Method is starting....");
////        return new ActionResult((List<Object>) (Object) objectList, 0, 0, 0, 0, "", "");
////    }
//
//    @PostMapping(value =restConstant.GROUP_ID+restConstant.CHILDREN)
//    @ApiOperation("Get a list of children for a group id, Fill in Field id for this.")
//    public ActionResult getChildrenGroup(@RequestBody JsonInput<UserManagementRegisterViewModel>  jsonInput){
//        Long id=jsonInput.getFieldId();
//        IGroupService ig=(IGroupService) getService();
//        List<GroupDTO> groupDTOS=getMapper().getMapper(getViewModel(),getModel()).toView(ig.getChildrenGroups(id));
//        return new ActionResult((List<Object>) (Object)groupDTOS,0,0,0,0,"","");
//    }
//    @PostMapping(value = restConstant.GROUP_ID+restConstant.PERMISSIONS)
//    @ApiOperation("Get permissions for a group. returns a group permission plus all of it's children permissions down to leafs.")
//    public ActionResult getPermissions(@RequestBody JsonInput<Group>groupJsonInput ){
//        Long id=groupJsonInput.getFieldId();
//        IGroupService ig=(IGroupService) getService();
//        Set<Permission> result=ig.getPermissions(id);
//        Set<PermissionDTO> resultDTO=PermissionMapper.INSTANCE.setPermissionToSetPermissionDTO(result);
//        List<PermissionDTO> lst=new ArrayList<>();
//        lst.addAll(resultDTO);
//        return new ActionResult((List<Object>) (Object)lst,0,0,0,0,"","");
//    }
//    @PostMapping(value ="/TreeModel")
//    @ApiOperation("Get a tree model based on NgZorro Tree data structure. Provide a user id and get ALL GROUPS." +
//            "Groups that this user is a member of, will have the isMember boolean set to true.")
//    public ActionResult getAllGroupsSettingIsMemberByUserId(@RequestBody JsonInput<Group> groupJsonInput){
//        Long id=groupJsonInput.getFieldId();
//        IGroupService service=(IGroupService) getService();
//        GroupTreeModel groupTreeModel= service.getGroupTree(id);
//        return new ActionResult(Collections.singletonList(groupTreeModel),0,0,0,0,"","");
//    }

}
