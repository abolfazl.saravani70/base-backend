package com.rbp.core.controller.security;
/**
 *
 * @author Alireza Souhani 1398.02.01
 *
 */

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.security.Permission;
import com.rbp.core.model.dto.security.PermissionDTO;
import com.rbp.core.service.security.IPermissionService;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value= restConstant.BASE_REST +restConstant.PERMISSIONS)
@Api(description = "Operations pertaining to permission in Permission Management System")
public class PermissionController extends FrameworkAbstractController<Permission, PermissionDTO, IPermissionService> {


//	@Autowired
//	ApplicationLogger applicationLogger;
//
//	@Autowired
//	IPermissionService iPermissionService;

//	@Override
//	protected Class<PermissionViewModel> getViewModel() {
//		return PermissionViewModel.class;
//	}
//
//	@Override
//	protected Class<Permission> getModel() {
//		return Permission.class;
//	}
//
//	@Override
//	protected IGenericService<Permission> getService() {
//		return this.iPermissionService;
//	}
	
//	@SuppressWarnings("unchecked")
//	@RequestMapping(value="/getPermissions",method= RequestMethod.POST)
//	public ActionResult getPermissions(){
//		applicationLogger.printLog("info", PermissionController.class, Permission.class.getSimpleName(), PermissionViewModel.class.getSimpleName(), "getDuty().getPermissions()", null, null, "Method is starting....");
//		List<PermissionViewModel> objectList= ModelMapper.mapList(iPermissionService.getPermissions(), PermissionViewModel.class) ;
//		applicationLogger.printLog("info", PermissionController.class, Permission.class.getSimpleName(), PermissionViewModel.class.getSimpleName(), "getDuty().getPermissions()", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0, 0, 0, 0, "", "");
//	}

}
