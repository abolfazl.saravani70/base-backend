//package com.rbp.core.controller.security;
///**
// * @author Alireza Souhani 1398.02.01
// */
//
//import com.rbp.core.controller.base.FrameworkAbstractController;
//import com.rbp.core.controller.base.restConstant;
//import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
//import com.rbp.core.model.dto.base.ActionResult;
//import com.rbp.core.model.domainmodel.security.OrganizationPosition;
//import com.rbp.core.model.dto.base.FrameworkPropertyValueDTO;
//import com.rbp.core.model.viewModel.security.OrganizationPositionViewModel;
//import com.rbp.core.service.security.IOrganizationPositionService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import java.util.List;
//
//
//@Controller
//@RequestMapping(value = restConstant.BASE_REST + restConstant.ORGANIZATION_POSITIONS)
//@Api(description = "Operations pertaining to organization position in Organization Position Management System")
//public class OrganizationPositionController extends FrameworkAbstractController<OrganizationPosition, OrganizationPositionViewModel, IOrganizationPositionService> {
//
//    @SuppressWarnings("unchecked")
//    @PostMapping(value = restConstant.UNITS)
//    @ApiOperation(value = "get a list of units")
//    public ActionResult getUnit() {
//        IOrganizationPositionService iOrganizationPositionService = (IOrganizationPositionService) getService();
//        getApplicationLogger().printLog("info", OrganizationPositionController.class, OrganizationPosition.class.getSimpleName(), OrganizationPositionViewModel.class.getSimpleName(), "getService().getOrganizationUnit()", null, null, "Method is starting....");
//        //List<FrameworkPropertyValueDTO> objectList= ModelMapper.mapList(iOrganizationPositionService.getOrganizationUnit(), FrameworkPropertyValueDTO.class) ;
//        List<FrameworkPropertyValue> listOrganizationUnit = iOrganizationPositionService.getOrganizationUnit();
//        List<FrameworkPropertyValueDTO> objectList = getMapper().getMapper(FrameworkPropertyValueDTO.class, FrameworkPropertyValue.class).toView(listOrganizationUnit);
//        getApplicationLogger().printLog("info", OrganizationPositionController.class, OrganizationPosition.class.getSimpleName(), OrganizationPositionViewModel.class.getSimpleName(), "getService().getOrganizationUnit()", null, null, "Method is starting....");
//        return new ActionResult((List<Object>) (Object) objectList, 0, 0, 0, 0, "", "");
//    }
//
//    @SuppressWarnings("unchecked")
//    @PostMapping(value = restConstant.POSITIONS)
//    @ApiOperation(value = "get a list of positions")
//    public ActionResult getPosition() {
//        IOrganizationPositionService iOrganizationPositionService = (IOrganizationPositionService) getService();
//        getApplicationLogger().printLog("info", OrganizationPositionController.class, OrganizationPosition.class.getSimpleName(), OrganizationPositionViewModel.class.getSimpleName(), "getService().getOrganizationPositions()", null, null, "Method is starting....");
//        //List<OrganizationPositionViewModel> objectList=ModelMapper.mapList(iOrganizationPositionService.getOrganizationPositions(), OrganizationPositionViewModel.class) ;
//        List<OrganizationPosition> listOrganizationPost = iOrganizationPositionService.getOrganizationPost();
//        List<OrganizationPositionViewModel> objectList = getMapper().getMapper(OrganizationPositionViewModel.class, OrganizationPosition.class).toView(listOrganizationPost);
//        getApplicationLogger().printLog("info", OrganizationPositionController.class, OrganizationPosition.class.getSimpleName(), OrganizationPositionViewModel.class.getSimpleName(), "getService().getOrganizationPositions()", null, null, "Method is starting....");
//        return new ActionResult((List<Object>) (Object) objectList, 0, 0, 0, 0, "", "");
//    }
//
//    @SuppressWarnings("unchecked")
//    @PostMapping(value = restConstant.ID + restConstant.POST_GROUPS)
//    @ApiOperation(value = "get a list of post groups for post id")
//    public ActionResult getPostGroup(@ApiParam(value = "Organization Position id from which Organization Position object will retrieve", required = true)@PathVariable(value = "postId") Long id) {
//        IOrganizationPositionService iOrganizationPositionService = (IOrganizationPositionService) getService();
//        getApplicationLogger().printLog("info", OrganizationPositionController.class, OrganizationPosition.class.getSimpleName(), OrganizationPositionViewModel.class.getSimpleName(), "getService().getOrganizationGroup()", null, null, "Method is starting....");
//        //List<OrganizationPositionViewModel> objectList=ModelMapper.mapList(iOrganizationPositionService.getOrganizationGroup(postId), OrganizationPositionViewModel.class) ;
//        List<OrganizationPosition> listOrganizationGroup = iOrganizationPositionService.getOrganizationGroup(id);
//        List<OrganizationPositionViewModel> objectList = getMapper().getMapper(OrganizationPositionViewModel.class, OrganizationPosition.class).toView(listOrganizationGroup);
//        getApplicationLogger().printLog("info", OrganizationPositionController.class, OrganizationPosition.class.getSimpleName(), OrganizationPositionViewModel.class.getSimpleName(), "getService().getOrganizationGroup()", null, null, "Method is starting....");
//        return new ActionResult((List<Object>) (Object) objectList, 0, 0, 0, 0, "", "");
//    }
//
//}
