package com.rbp.core.controller.security;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.model.dto.security.UserDTO;
import com.rbp.core.service.security.IUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Alireza Souhani 1398.02.01
 */

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.USERS)
@Api(description = "Operations pertaining to user in User Management System")
public class UserController extends FrameworkAbstractController<User, UserDTO, IUserService> {

    //TODO: use getService method from FAC instead of this autowire.
    @Autowired//This Autowire is not required. Since i don't have time. i will change it later.
    private IUserService iUserService;

//    @PostMapping(value = restConstant.ORGANIZATIONS)
//    @ApiOperation("get all of the users and corresponding organization units." +
//            "Fill JsonInput for page size, page number and search params. (Page size and number are MANDATORY) ")
//    public ActionResult getUsersOrganizationUnits(@RequestBody JsonInput<UserDTO> input) {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersOrganizationUnits", null, null, "Method is starting....");
//
//        UserManagementGridViewModel userManagementGridViewModel =
//                iUserService.getUserManagementGridData(input.getPageNumber(), input.getPageSize());
//
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersOrganizationUnits", null, null, "Method is ending....");
//        return new ActionResult(Collections.singletonList(userManagementGridViewModel), input.getPageNumber(), input.getPageSize(), 0, 0, "", "");
//    }

//    @PostMapping(value = restConstant.AUTHENTICATED_USER)
//    @ApiOperation("get the current logged in user, the password will not be sent!")
//    public ActionResult getCurrentUser() {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().getUserById", null, null, "Method is starting....");
//        User objectUser = SecurityUtility.getAuthenticatedUser();
//        UserDTO userDTO = getMapper().getMapper(getViewModel(), getModel()).toView(objectUser);
//        userDTO.setPassword("");//password omitted before sending user info to ui!
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().getUserById", null, null, "Method is starting....");
//        return new ActionResult(Collections.singletonList(userDTO), 0, 0, 0, 0, "", "");
//    }

//    @PostMapping(value = restConstant.USER_ID + restConstant.SAME_ORGANIZATION)
//    @ApiOperation("get all users of the same organization by giving them the user ID")
//    public ActionResult getUsersSameOrganization(@RequestBody JsonInput<UserDTO> input) {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersSameOrganization", null, null, "Method is starting....");
//        List<User> users = iUserService.getUsersSameOrganization(input.getFieldId());
//        List<UserDTO> objectList = getMapper().getMapper(getViewModel(), getModel()).toView(users);
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersSameOrganization", null, null, "Method is ending....");
//        if (objectList.size() == 0)
//            return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "کاربر مورد نظر فاقد کاربران هم رده است", "");
//        else
//            return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "لیست کاربران هم رده با کاربر مورد نظر یافت شد", "");
//    }

//    @PostMapping(value = restConstant.USER_ID + restConstant.SAME_ORGANIZATION_CHART_LEVEL)
//    @ApiOperation("get all users of the same organization and organizationChart and level by giving them the user ID")
//    public ActionResult getUsersSameOrganizationChartAndOrganization(@RequestBody JsonInput<UserDTO> input) {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersSameOrganizationChartAndOrganization", null, null, "Method is starting....");
//        List<User> users = iUserService.getUsersSameOrganizationChartAndOrganization(input.getFieldId());
//        List<UserDTO> objectList = getMapper().getMapper(getViewModel(), getModel()).toView(users);
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersSameOrganizationChartAndOrganization", null, null, "Method is ending....");
//        if (objectList.size() == 0)
//            return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "کاربر مورد نظر فاقد کاربران هم چارت است", "");
//        else
//            return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "لیست کاربران هم چارت با کاربر مورد نظر یافت شد", "");
//    }

//    @PostMapping(value = restConstant.USER_ID + restConstant.LOWER_DOWN_ORGANIZATIONS)
//    @ApiOperation("get all users of the lower down organizations by giving them the user ID")
//    public ActionResult getUsersLowerDownOrganizations(@RequestBody JsonInput<UserDTO> input) {
//
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersLowerDownOrganizations", null, null, "Method is starting....");
//        List<User> users = iUserService.getUsersLowerDownOrganizations(input.getFieldId());
//        List<UserDTO> objectList = getMapper().getMapper(getViewModel(), getModel()).toView(users);
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersLowerDownOrganizations", null, null, "Method is ending....");
//        if (objectList.size() == 0)
//            return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "کاربر مورد نظر فاقد کاربران زیر رده است", "");
//        else
//            return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "لیست کاربران موجود در رده های پایین تر از رده کاربر مورد نظر یافت شد", "");
//    }

//    @PostMapping(value = restConstant.USER_ID + restConstant.HIGHER_ORGANIZATIONS)
//    @ApiOperation("get all users of the higher organization by giving them the user ID")
//    public ActionResult getUsersHigherOrganization(@RequestBody JsonInput<UserDTO> input) {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersHigherOrganization", null, null, "Method is starting....");
//        List<User> users = iUserService.getUsersHigherOrganization(input.getFieldId());
//        List<UserDTO> objectList = getMapper().getMapper(getViewModel(), getModel()).toView(users);
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getUsersHigherOrganization", null, null, "Method is ending....");
//        if (objectList.size()==0)
//            return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "کاربر مورد نظر فاقد کاربرانی در رده بالاتر است", "");
//        else
//        return new ActionResult(objectList, input.getPageNumber(), input.getPageSize(), 0, 0, "لیست کاربران موجود در رده بالاتر از رده کاربر مورد نظر یافت شد", "");
//    }


    //Add What type of input? What is status?
//	@PostMapping(value="/setStatus")
//	public ActionResult setStatus(@PathVariable Long userId, @PathVariable Boolean status){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().setStatus()", null, null, "Method is starting....");
//		iUserService.setStatus(userId, status);
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().setStatus()", null, null, "Method is starting....");
//		return new ActionResult(null, 0, 0, 0, 0, "", "");
//	}


//    @PostMapping(value = restConstant.USER_ID + restConstant.RESET_PASSWORD)
//    @ApiOperation("send a user id and reset password to 123, only if you are the currently logged in user!")
//    public ActionResult resetPassword(@RequestBody JsonInput<UserDTO> input) {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "ResetPassword", null, null, "Method is starting....");
//        if (!SecurityUtility.getAuthenticatedUserId().equals(input.getFieldId()))
//            throw new ApplicationException(1, "Currently logged user is someone else.");
//        iUserService.resetPassword(getService().loadById(input.getFieldId()));
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "ResetPassword", null, null, "Method is starting....");
//        return new ActionResult(null, 0, 0, 0, 0, "", "");
//    }

//    @PostMapping(value = restConstant.USER_ID + restConstant.CHANGE_PASSWORD)
//    @ApiOperation("Send User DTO with the password and ID")
//    public ActionResult changePassword(@RequestBody JsonInput<UserDTO> input) {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "ChangePassword", null, null, "Method is starting....");
//        UserDTO userDTO = input.getData();
//        if (!SecurityUtility.getAuthenticatedUser().getId().equals(userDTO.getId()))
//            throw new ApplicationException(1, "Currently logged user is someone else.");
//        iUserService.generalChangePassword(userDTO.getId(), userDTO.getPassword());
//        SecurityUtility.getAuthenticatedUser().setPassword(userDTO.getPassword());
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "ChangePassword", null, null, "Method is ending....");
//        return new ActionResult(null, 0, 0, 0, 0, "", "");
//    }

//    @PostMapping(value = restConstant.ONLINE_COUNT)
//    @ApiOperation("returns an integer equals to the number of online users")
//    public ActionResult getOnlineUsers() {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().getUserPost", null, null, "Method is starting....");
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().getUserPost", null, null, "Method is starting....");
//        return new ActionResult(Collections.singletonList(SecurityUtility.getOnlineUserCount()), 0, 0, 0, 0, "", "");
//    }


    //	@PostMapping(value="/getStateUserCount/{stateCode}")
//	public ActionResult getStateUserCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().getStateUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getStateUserCount(stateCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "getDuty().getStateUserCount", null, null, "Method is starting....");
//		return new ActionResult(objectList, 0,0, 0, 0, "", "");
//	}
//    @PostMapping(value = restConstant.GENERAL_UPDATE)
//    @ApiOperation("General Update for the user. you can update everything except password")
//    public ActionResult updateGeneralUserInfo(@RequestBody JsonInput<UserManagementRegisterViewModel> input) {
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserManagementRegisterViewModel.class.getSimpleName(), "UpdateGeneralUserInfo", null, null, "Method is starting....");
//
//        applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserManagementRegisterViewModel.class.getSimpleName(), "UpdateGeneralUserInfo", null, null, "Method is ending....");
//        return iUserService.update(input.getData());
//    }


//TODO: this was a redirect for Login, not required for now ->change back if needed.
// the view model creation needs to be fixed to get organization unit name and id

//	@GetMapping(value = restConstant.SIGN_IN_REDIRECT)
//	@ApiOperation("After login, this rest will be called to send an according ActionResult with Token." +
//			"Token is set to True after successful login")
//	public ActionResult signinRedirectUser(){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "signinRedirectUser", null, null, "Method is starting....");
//
//		User u=SecurityUtility.getAuthenticatedUser();
//		Long userId=u.getId();
//        System.out.println(userId);
//		String orgunit=iUserService.getOrganizationByUser(u.getId());
//		String username=u.getUsername();
//		String firstname=u.getFirstName();
//		String lastname=u.getLastName();
//		Boolean isActtive=u.getIsActive();
//		UserOrganizationViewModel userOrganizationViewModel =new UserOrganizationViewModel(orgunit,0L,userId,username,firstname,lastname,isActtive);
//
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserDTO.class.getSimpleName(), "signinRedirectUser", null, null, "Method is ending....");
//		return new ActionResult(Collections.singletonList(userOrganizationViewModel),0,0,0,0,"","True");
//	}



//    @PostMapping(value = restConstant.USER_ID + restConstant.INFO)
//    @ApiOperation("Get the required information for a user. (User management page)")
//    public ActionResult getUserInfo(@RequestBody JsonInput<UserDTO> jsonInput) {
//        Long userId = jsonInput.getFieldId();
//        UserManagementRegisterViewModel UMRV = iUserService.showUserOrganizationInfo(userId);
//        return new ActionResult(Collections.singletonList(UMRV), 0, 0, 0, 0, "", "");
//    }

//    @PostMapping(value = restConstant.LOGIN)
//    @ApiOperation("Send Username and password to login")
//    public ActionResult login(@RequestParam String username, @RequestParam String password, HttpServletResponse response) throws IOException {
//        if (SecurityContextHolder.getContext().getAuthentication() instanceof User) {
//            String currentusername = ((User) SecurityContextHolder.getContext().getAuthentication()).getUsername();
//            if (username.equalsIgnoreCase(currentusername)) throw new ApplicationException(1, "شما قبلا وارد شده اید!");
//        }
//
//        Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);
//        authentication = userAuthenticationProvider.authenticate(authentication);
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        Object o = SecurityContextHolder.getContext().getAuthentication().getDetails();
//        iUserService.setLastLoginDateandIP((User) authentication.getPrincipal());
//        iUserService.setUserOnline(((User) authentication.getPrincipal()).getId());
//        SecurityUtility.addOnlineUser();
//        response.sendRedirect(restConstant.BASE_REST + restConstant.USERS + restConstant.SIGN_IN_REDIRECT);
//        if (authentication.isAuthenticated()) return new ActionResult(null, 0, 0, 0, 0, "", "True");
//        return new ActionResult(null, 0, 0, 0, 1, "ورود به سیستم موفقیت آمیز نبود.", "");
//    }

//    @PostMapping(value = restConstant.LOGOUT)
//    @ApiOperation("Send a request to logout")
//    public ActionResult logout(HttpServletRequest request) {
//        User user = SecurityUtility.getAuthenticatedUser();
//        iUserService.setLastLoginDateandIP(user);
//        user.setLastIp(SecurityUtility.getRequestedIp());
//        user.setLastLogin(DateUtility.todayfa() + " - " + DateUtility.getTime());
//        iUserService.setLastLoginDateandIP(user);
//        iUserService.setUserOffline(user.getId());
//        request.getSession().removeAttribute("login");
//        request.getSession().removeAttribute("USER");
//        SecurityUtility.removeOnlineUser();
//        SecurityContextHolder.clearContext();
//        return new ActionResult(null, 0, 0, 0, 0, "", "False");
//    }


//	@RequestMapping(value="/getStateOnlineUserCount/{stateCode}",method= RequestMethod.POST)
//	public ActionResult getStateOnlineUserCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getStateOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getStateOnlineUserCount(stateCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getStateOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}

//	@RequestMapping(value="/getCityUserCount/{cityCode}",method= RequestMethod.POST)
//	public ActionResult getCityUserCount(@PathVariable String cityCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getCityUserCount(cityCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getCityOnlineUserCount/{cityCode}",method= RequestMethod.POST)
//	public ActionResult getCityOnlineUserCount(@PathVariable String cityCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getCityOnlineUserCount(cityCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getUserPerStateStatusCount",method= RequestMethod.POST)
//	public ActionResult getUserPerStateStatusCount(){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getUserPerStateStatusCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getUserPerStateStatusCount(), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getUserPerStateStatusCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0, 0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getStateUserPerCityCount/{stateCode}",method= RequestMethod.POST)
//	public ActionResult getStateUserPerCityCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getStateUserPerCityCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getStateUserPerCityCount(stateCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getStateUserPerCityCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0, 0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getStateOnlineUserPerCityCount/{stateCode}",method= RequestMethod.POST)
//	public ActionResult getStateOnlineUserPerCityCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getStateOnlineUserPerCityCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getStateOnlineUserPerCityCount(stateCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getStateOnlineUserPerCityCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getOnlineUserPerstateCount",method= RequestMethod.POST)
//	public ActionResult getOnlineUserPerstateCount(){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getOnlineUserPerstateCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getOnlineUserPerstateCount(), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getOnlineUserPerstateCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getVillageUserCount/{villageCode}",method= RequestMethod.POST)
//	public ActionResult getVillageUserCount(@PathVariable String villageCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getVillageUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getVillageUserCount(villageCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getVillageUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getVillageOnlineUserCount/{villageCode}",method= RequestMethod.POST)
//	public ActionResult getVillageOnlineUserCount(@PathVariable String villageCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getVillageOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getVillageOnlineUserCount(villageCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getVillageOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//
//	@RequestMapping(value="/getCityPerVillageUserCount/{cityCode}",method= RequestMethod.POST)
//	public ActionResult getCityPerVillageUserCount(@PathVariable String cityCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityPerVillageUserCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getCityPerVillageUserCount(cityCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityPerVillageUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getCityPerVillageOnlineUserCount/{cityCode}",method= RequestMethod.POST)
//	public ActionResult getCityPerVillageOnlineUserCount(@PathVariable String cityCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityPerVillageOnlineUserCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getCityPerVillageOnlineUserCount(cityCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getCityPerVillageOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getZoneUserCount/{zoneCode}",method= RequestMethod.POST)
//	public ActionResult getZoneUserCount(@PathVariable String zoneCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getZoneUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getZoneUserCount(zoneCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getZoneUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getZoneOnlineUserCount/{zoneCode}",method= RequestMethod.POST)
//	public ActionResult getZoneOnlineUserCount(@PathVariable String zoneCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getZoneOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getZoneOnlineUserCount(zoneCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getZoneOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskStateUserCount/{stateCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskStateUserCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskStateUserCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getServiceDeskStateUserCount(stateCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskStateUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskStateOnlineUserCount/{stateCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskStateOnlineUserCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskStateOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getServiceDeskStateOnlineUserCount(stateCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskStateOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskCityUserCount/{stateCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskCityUserCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskCityUserCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getServiceDeskCityUserCount(stateCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskCityUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskCityOnlineUserCount/{cityCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskCityOnlineUserCount(@PathVariable String cityCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskCityOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getServiceDeskCityOnlineUserCount(cityCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskCityOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskVillageUserCount/{cityCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskVillageUserCount(@PathVariable String cityCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskVillageUserCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getServiceDeskVillageUserCount(cityCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskVillageUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskVillageZoneOnlineUserCount/{villageCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskVillageZoneOnlineUserCount(@PathVariable String villageCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskVillageZoneOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getServiceDeskVillageZoneOnlineUserCount(villageCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskVillageZoneOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskZoneUserCount/{stateCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskZoneUserCount(@PathVariable String stateCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskZoneUserCount", null, null, "Method is starting....");
//		List<MonitoringUserDTO> objectList=ModelMapper.mapList(iUserService.getServiceDeskZoneUserCount(stateCode), MonitoringUserDTO.class) ;
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskZoneUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//
//	@RequestMapping(value="/getServiceDeskZoneOnlineUserCount/{zoneCode}",method= RequestMethod.POST)
//	public ActionResult getServiceDeskZoneOnlineUserCount(@PathVariable String zoneCode){
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskZoneOnlineUserCount", null, null, "Method is starting....");
//		List<Object> objectList=new ArrayList<Object>();
//		objectList.add(iUserService.getServiceDeskZoneOnlineUserCount(zoneCode));
//		applicationLogger.printLog("info", UserController.class, User.class.getSimpleName(), UserViewModel.class.getSimpleName(), "getDuty().getServiceDeskZoneOnlineUserCount", null, null, "Method is starting....");
//		return new ActionResult((List<Object>)(Object)objectList, 0,0, 0, 0, "", "");
//	}
//

}
