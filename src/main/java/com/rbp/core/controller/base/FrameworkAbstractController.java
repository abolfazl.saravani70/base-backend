package com.rbp.core.controller.base;

import com.bahman.securitycommon.model.security.UsersModel;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.gridStructure.request.EnterpriseGetRowsRequest;
import com.rbp.core.model.domainmodel.gridStructure.response.EnterpriseGetRowsResponse;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.service.base.IGenericService;
import com.rbp.core.service.security.SecurityService;
import com.rbp.core.utility.ApplicationException;
import com.rbp.core.utility.ApplicationLogger;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.mapper.GenericMapperService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.rbp.core.model.domainmodel.search.SearchOperation.BLANK;

/**
 * @author Alireza Souhani 1398.02.01
 * model
 * viewModel=Viewmodel
 */
@RestController
public abstract class FrameworkAbstractController<model extends BaseEntity, viewModel, service> {

    @Autowired
    SecurityService securityService;

    @Autowired
    ApplicationLogger applicationLogger;

    @Autowired
    ApplicationContext applicationContext;

    Class<model> modelClass;
    Class<viewModel> viewModelClass;
    Class<service> serviceClass;
    IGenericService<model> controllerService;
    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected GenericMapperService mapper;

    protected Class<viewModel> getViewModel() {
        return viewModelClass;
    }

    protected Class<model> getModel() {
        return modelClass;
    }

    protected IGenericService<model> getService() {
        return controllerService;
    }

    protected GenericMapperService getMapper() {
        return mapper;
    }

    public ApplicationLogger getApplicationLogger() {
        return applicationLogger;
    }


    @PostConstruct
    void initClasses() {
        modelClass = (Class<model>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        viewModelClass = (Class<viewModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];

        serviceClass = (Class<service>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[2];
        try {
            controllerService = (IGenericService<model>) applicationContext.getBean(serviceClass);

        } catch (Exception e) {
            System.out.println("Not Created Service Class is:" + serviceClass.getSimpleName());
        }
    }


    @PostMapping
    @ApiOperation(value = "crud by Json input {data: Dto or view modle , model: model name , type:  1= add an entity ,2= get list of enteties,3= get an entity by Id,4= delete an entity by Id, 5= advanced search operations, token:client token , page: pages number , pageSize: size of page }")
    public ActionResult crud(@RequestBody JsonInput<viewModel> jsonInput) {
        UsersModel user1 = securityService.getCurrentUser();
        switch (jsonInput.getType()) {
            case 1:
                return saveAndUpdate(jsonInput);
            case 2:
                return getObjectList(jsonInput);
            case 3:
                return loadById(jsonInput);
            case 4:
                return delete(jsonInput);
            case 5:
                return advancedSearch(jsonInput);
            default:
                return new ActionResult(null, 0, 0, 0L, 0, "Typing is invalid...", "");
        }
    }

    @PostMapping(value = restConstant.CURRENT_DATE_TIME)
    @ApiOperation(value = "get current date time")
    public ActionResult getCurrentDateTime() {
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getCurrentDateTime()", null, null, "Method is starting....");
        List<String> objectList = Collections.singletonList(getService().getCurrentDateTime());
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getCurrentDateTime()", null, null, "Method finish successfull!!!!");
        return new ActionResult(objectList, 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }

    @PostMapping(value = restConstant.GET_ROWS)
    @ApiOperation(value = "get ag-grid records")
    public ActionResult getRows(@RequestBody EnterpriseGetRowsRequest request) {
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().EnterpriseGetRowsResponse()", null, null, "Method is starting....");
        EnterpriseGetRowsResponse objectList = getService().getDataGrid(request);
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().EnterpriseGetRowsResponse()", null, null, "Method finish successfull!!!!");
        return new ActionResult((List<Object>) objectList, 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }

    protected ActionResult searchCoding(JsonInput<viewModel> jsonInput) {
        if (jsonInput.getFilter() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً مورد جستجو را پرکنید.");
        }
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchCoding()", null, null, "Method is starting....");
        AdvancedSearchResult searchResult = getService().searchCoding(jsonInput.getFilter());
        List<viewModel> objectList = mapper.getMapper(getViewModel(), getModel()).toView(searchResult.getRecords());
        if (objectList.size() == 0) {
            applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchCoding()", null, null, "Method is starting....");
            return new ActionResult(objectList, 0, 0, searchResult.getTotalCount(), 0, "هیچ موردی برای جستجو یافت نشد.", "");
        }
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchCoding()", null, null, "Method finish successfull!!!!");
        return new ActionResult(objectList, 0, 0, searchResult.getTotalCount(), 0, "جستجو با موفقیت انجام شد", "");

    }

    /**
     * @return Permission Result
     */
    protected ActionResult getObjectList(JsonInput<viewModel> jsonInput) {

        Authentication userId = SecurityContextHolder.getContext().getAuthentication();
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getList()", null, null, "Method is starting....");
        AdvancedSearchResult searchResult = getService().getList(jsonInput.getFilter());
        List<model> t = searchResult.getRecords();
        List<viewModel> objectList = mapper.getMapper(getViewModel(), getModel()).toView(t);
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getList()", null, null, "Method finish successfull!!!!");
        return new ActionResult((List<Object>) objectList, 0, 0, searchResult.getTotalCount(), 0, "داده های مورد نظر یافت شدند", "");

    }

    protected ActionResult saveAndUpdate(JsonInput<viewModel> jsonInput) {

        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().save", null, null, "Method is starting....");
        //model model=ModelMapper.map(viewModel, this.getModel());
        model model = mapper.getMapper(getViewModel(), getModel()).toDomainModel(jsonInput.getData());
        boolean isForSave = model.getId() != 0 ? false : true;

        model savedEntity = getService().save(model);
        viewModel result = mapper.getMapper(getViewModel(), getModel()).toView(savedEntity);
        List<Object> objectList = Arrays.asList(result);

        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().save", null, null, "Method finish successfull!!!!");

        if (isForSave)
            return new ActionResult(objectList, 0, 0, 0L, 0, "ثبت با موفقیت انجام شد", "");
        else
            return new ActionResult(objectList, 0, 0, 0L, 0, "ویرایش یا موفقیت انجام شد", "");
    }

    protected ActionResult delete(JsonInput<viewModel> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را برای حذف ارائه دهید");
        }
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().deleteById", null, null, "Method is starting....");
        getService().deleteById(jsonInput.getFieldId());
        //getService().softDeleteById(entityId);
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().deleteById", null, null, "Method finish successfull!!!!");

        return new ActionResult(null, 0, 0, 0L, 0, "حذف با موفقیت انجام شد", "");
    }

    protected ActionResult loadById(JsonInput<viewModel> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "داده مورد نظر را وارد کنید.");
        }
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().loadById", null, null, "Method is starting....");
        //List<viewModel> objectList=(List<viewModel>) ModelMapper.mapList(getDuty().loadById(id),this.getViewModel());
        // List<viewModel> objectList = mapper.getMapper(getViewModel(), getModel()).toView(getService().loadById(entityId));
        viewModel object = mapper.getMapper(getViewModel(), getModel()).toView(getService().loadById(jsonInput.getFieldId()));
        List<Object> objectList = new ArrayList<>();
        objectList.add(object);
        if (object == null) {
            applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().loadById", null, null, "Such an entity does not exist with this ID");
            return new ActionResult(objectList, 0, 0, 0L, 0, "چنین داده ای وجود ندارد", "");
        }
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().loadById", null, null, "Method finish successfull!!!!");
        return new ActionResult(objectList, 0, 0, 0L, 0, "داده مورد نظر یافت شد", "");
    }

    protected ActionResult advancedSearch(JsonInput<viewModel> jsonInput) {
        if (jsonInput.getFilter() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً مورد جستجو را پرکنید.");
        }
        applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchEntity()", null, null, "Method is starting....");
        AdvancedSearchResult searchResult = getService().searchEntity(jsonInput.getFilter(), false);
        if (jsonInput.getFilter().getGroupBy() != null && !jsonInput.getFilter().getGroupBy().equals(BLANK)) {
            if (searchResult.getRecords().size() == 0) {
                applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchEntity()", null, null, "Method is starting....");
                return new ActionResult(searchResult.getRecords(), 0, 0, searchResult.getTotalCount(), 0, "هیچ موردی برای جستجو یافت نشد.", "");
            } else {
                applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchEntity()", null, null, "Method finish successfull!!!!");
                return new ActionResult(searchResult.getRecords(), 0, 0, searchResult.getTotalCount(), 0, "جستجو با موفقیت انجام شد", "");
            }
        } else {
            List<viewModel> objectList = mapper.getMapper(getViewModel(), getModel()).toView(searchResult.getRecords());
            if (objectList.size() == 0) {
                applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchEntity()", null, null, "Method is starting....");
                return new ActionResult(objectList, 0, 0, searchResult.getTotalCount(), 0, "هیچ موردی برای جستجو یافت نشد.", "");
            }
            applicationLogger.printLog("info", FrameworkAbstractController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().searchEntity()", null, null, "Method finish successfull!!!!");
            return new ActionResult(objectList, 0, 0, searchResult.getTotalCount(), 0, "جستجو با موفقیت انجام شد", "");
        }


    }

    protected List getMappers(List<BaseEntity> list) {

        String className;
        GenericMapper currentMapper;
        if (list.size() != 0) {
            className = list.get(0).getClass().getSuperclass().getSimpleName();
            currentMapper = mapper.getMapperByEntityClass(className);
            return currentMapper.toView(list);
        }
        return list;
    }
}
