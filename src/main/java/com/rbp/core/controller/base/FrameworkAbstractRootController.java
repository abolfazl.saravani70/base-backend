package com.rbp.core.controller.base;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.service.base.IGenericRootService;
import com.rbp.core.service.base.IGenericService;
import com.rbp.core.utility.ApplicationException;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.List;
@RestController
public abstract class FrameworkAbstractRootController
        <rootModel extends RootBaseEntity, rootViewModel, rootService>
        extends FrameWorkAbstractTreeController<rootModel, rootViewModel, rootService> {

    private Class<rootModel> rootModelClass;
    private Class<rootViewModel> rootViewModelClass;
    private Class<rootService> rootServiceClass;
    private IGenericRootService controllerRootService;

    private IGenericRootService getRootService() {
        return controllerRootService;
    }

    private Class<rootViewModel> getRootViewModel() {
        return rootViewModelClass;
    }

    private Class<rootModel> getRootModel() {
        return rootModelClass;
    }

    @PostConstruct
    void initClasses() {
        modelClass = (Class<rootModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        viewModelClass = (Class<rootViewModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];

        serviceClass = (Class<rootService>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[2];

        rootModelClass = (Class<rootModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        rootViewModelClass = (Class<rootViewModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];

        rootServiceClass = (Class<rootService>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[2];
        try {
            controllerRootService = (IGenericRootService) applicationContext.getBean(rootServiceClass);
            controllerService = (IGenericService<rootModel>) applicationContext.getBean(serviceClass);

        } catch (Exception e) {
            System.out.println("Not Created Tree Service Class is:" + treeServiceClass.getSimpleName());
        }
    }

    @Override
    protected ActionResult getTreeByChildId(Long fieldId) {
        applicationLogger.printLog("info", FrameworkAbstractRootController.class, getRootModel().getSimpleName(), getRootViewModel().getSimpleName(), "getRootService().getParentsByChildId", null, null, "Method is starting....");
        List<BaseEntity> t = getRootService().getParentsByChildId(fieldId);
        List objectList = getMappers(t);
        applicationLogger.printLog("info", FrameworkAbstractRootController.class, getRootModel().getSimpleName(), getRootViewModel().getSimpleName(), "getRootService().getParentsByChildId", null, null, "Method finish successfull!!!!");
        if (objectList.size() == 0)
            return new ActionResult(objectList, 0, 0, 0L, 0, "داده مورده نظر ریشه است و فاقد پدر است", "");
        else
            return new ActionResult(objectList, 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }

    @Override
    protected ActionResult getTreeByParents(Long fieldId) {
        applicationLogger.printLog("info", FrameworkAbstractRootController.class, getRootModel().getSimpleName(), getRootViewModel().getSimpleName(), "getRootService().getChildrenByParentId", null, null, "Method is starting....");
        List t = getRootService().getChildrenByParentId(fieldId);
        List objectList = getMappers(t);
        applicationLogger.printLog("info", FrameworkAbstractRootController.class, getRootModel().getSimpleName(), getRootViewModel().getSimpleName(), "getRootService().getChildrenByParentId", null, null, "Method finish successfull!!!!");
        if (objectList.size() == 0)
            return new ActionResult(objectList, 0, 0, 0L, 0, "پدر مورده نظر فاقد فرزند است", "");
        else
            return new ActionResult(objectList, 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }

    @Override
    protected ActionResult delete(JsonInput<rootViewModel> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را برای حذف ارائه دهید");
        }
        applicationLogger.printLog("info", FrameworkAbstractRootController.class, getRootModel().getSimpleName(), getRootViewModel().getSimpleName(), "getRootService().deleteNode()", null, null, "Method is starting....");
        getRootService().deleteNode(jsonInput.getFieldId(),jsonInput.getFilter().getDeleteAllData(),0L, 0);
        //getService().softDeleteById(entityId);
        applicationLogger.printLog("info", FrameworkAbstractRootController.class, getRootModel().getSimpleName(), getRootViewModel().getSimpleName(), "getRootService().deleteNode()", null, null, "Method finish successfull!!!!");

        return new ActionResult(null, 0, 0, 0L, 0, "حذف با موفقیت انجام شد", "");
    }




}
