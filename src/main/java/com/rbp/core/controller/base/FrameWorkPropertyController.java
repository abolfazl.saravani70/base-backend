package com.rbp.core.controller.base;

import com.rbp.core.model.domainmodel.base.FrameworkProperty;
import com.rbp.core.model.dto.base.FrameworkPropertyDTO;
import com.rbp.core.service.base.impl.FrameWorkPropertyService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Alireza Souhani 1398.02.01
 */

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.PROPERTIES)
@Api(description = "Operations pertaining to property in Frame Work property Management System")
public class FrameWorkPropertyController extends FrameworkAbstractController<FrameworkProperty, FrameworkPropertyDTO, FrameWorkPropertyService> {

}
