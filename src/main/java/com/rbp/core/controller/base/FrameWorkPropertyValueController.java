package com.rbp.core.controller.base;
/**
 *
 * @author Alireza Souhani 1398.02.01
 *
 */

import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.FrameworkPropertyValueDTO;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.service.base.IFrameWorkPropertyValueService;
import com.rbp.core.service.base.impl.FrameWorkPropertyValueService;
import com.rbp.core.utility.ApplicationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping(value=restConstant.BASE_REST +restConstant.PROPERTY_VALUES)
@Api(description = "Operations pertaining to property value in Frame Work Property Value Management System")
public class FrameWorkPropertyValueController extends FrameworkAbstractController<FrameworkPropertyValue, FrameworkPropertyValueDTO, FrameWorkPropertyValueService>{

    @Autowired
    IFrameWorkPropertyValueService iFrameWorkPropertyValueService;

    @PostMapping(value = restConstant.PROPERTY_ID)
    @ApiOperation(value = "get list of property values by property id")
    public ActionResult getPvTypes(@RequestBody JsonInput<FrameworkPropertyValueDTO> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را برای جستجو وارد کنید");
        }
        getApplicationLogger().printLog("info", FrameWorkPropertyValueController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByPropertyId()", null, null, "Method is starting....");
        List<FrameworkPropertyValue> list = iFrameWorkPropertyValueService.getByPropertyId(jsonInput.getFieldId());
        List<FrameworkPropertyValueDTO> objectList = getMapper().getMapper(FrameworkPropertyValueDTO.class , FrameworkPropertyValue.class).toView(list);
        getApplicationLogger().printLog("info", FrameWorkPropertyValueController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().getByPropertyId()", null, null, "Method finish successfull!!!!");
        return new ActionResult(Collections.singletonList(objectList), 0, 0, 0L, 0, "", "داده های مورد نظر یافت شدند");
    }

}
