package com.rbp.core.controller.base;

import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.service.base.IGenericService;
import com.rbp.core.service.base.IGenericTreeService;
import com.rbp.core.utility.ApplicationException;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@RestController
public abstract class FrameWorkAbstractTreeController<treeModel extends TreeBaseEntity, treeViewModel, treeService>
        extends FrameworkAbstractController<treeModel, treeViewModel, treeService> {
    private Class<treeModel> treeModelClass;
    private Class<treeViewModel> treeViewModelClass;
    Class<treeService> treeServiceClass;
    private IGenericTreeService controllerTreeService;

    private IGenericTreeService getTreeService() {
        return controllerTreeService;
    }

    private Class<treeViewModel> getTreeViewModel() {
        return treeViewModelClass;
    }

    private Class<treeModel> getTreeModel() {
        return treeModelClass;
    }

    @PostConstruct
    void initClasses() {
        modelClass = (Class<treeModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        viewModelClass = (Class<treeViewModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];

        serviceClass = (Class<treeService>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[2];

        treeModelClass = (Class<treeModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        treeViewModelClass = (Class<treeViewModel>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];

        treeServiceClass = (Class<treeService>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[2];
        try {
            controllerTreeService = (IGenericTreeService) applicationContext.getBean(treeServiceClass);
            controllerService = (IGenericService<treeModel>) applicationContext.getBean(serviceClass);

        } catch (Exception e) {
            System.out.println("Not Created Tree Service Class is:" + treeServiceClass.getSimpleName());
        }
    }

    @PostMapping(value = restConstant.CHILDREN)
    @ApiOperation(value = "get all children of an parent in tree")
    public ActionResult getChildrenByParentId(@RequestBody JsonInput<treeViewModel> jsonInput) {
        return getTreeByParents(jsonInput.getFieldId());
    }

    protected ActionResult getTreeByParents(Long fieldId) {
        applicationLogger.printLog("info", FrameWorkAbstractTreeController.class, getTreeModel().getSimpleName(), getTreeViewModel().getSimpleName(), "getTreeService().getChildrenByParentId", null, null, "Method is starting....");
        List t = getTreeService().getChildrenByParentId(fieldId);
        List objectList = getMappers(t);;
        applicationLogger.printLog("info", FrameWorkAbstractTreeController.class, getTreeModel().getSimpleName(), getTreeViewModel().getSimpleName(), "getTreeService().getChildrenByParentId", null, null, "Method finish successfull!!!!");
        if (objectList.size() == 0)
            return new ActionResult(objectList, 0, 0, 0L, 0, "پدر مورده نظر فاقد فرزند است", "");
        else
            return new ActionResult(objectList, 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }

    @PostMapping(value = restConstant.PARENTS)
    @ApiOperation(value = "get all parents of an child in tree")
    public ActionResult getParentsByChildId(@RequestBody JsonInput<treeViewModel> jsonInput) {
        return getTreeByChildId(jsonInput.getFieldId());
    }

    protected ActionResult getTreeByChildId(Long fieldId) {
        applicationLogger.printLog("info", FrameWorkAbstractTreeController.class, getTreeModel().getSimpleName(), getTreeViewModel().getSimpleName(), "getTreeService().getParentsByChildId", null, null, "Method is starting....");
        List t = getTreeService().getParentsByChildId(fieldId);
        List objectList = getMappers(t);;
        applicationLogger.printLog("info", FrameWorkAbstractTreeController.class, getTreeModel().getSimpleName(), getTreeViewModel().getSimpleName(), "getTreeService().getParentsByChildId", null, null, "Method finish successfull!!!!");
        if (objectList.size() == 0)
            return new ActionResult(objectList, 0, 0, 0L, 0, "داده مورده نظر ریشه است و فاقد پدر است", "");
        else
            return new ActionResult(objectList, 0, 0, 0L, 0, "داده های مورد نظر یافت شدند", "");
    }

    @Override
    protected ActionResult delete(JsonInput<treeViewModel> jsonInput) {
        if (jsonInput.getFieldId() == null) {
            // TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "لطفاً داده مورد نظر را برای حذف ارائه دهید");
        }
        applicationLogger.printLog("info", FrameWorkAbstractTreeController.class, getTreeModel().getSimpleName(), getTreeViewModel().getSimpleName(), "getTreeService().deleteNode()", null, null, "Method is starting....");
        getTreeService().deleteNode(jsonInput.getFieldId(),jsonInput.getFilter().getDeleteAllData());
        //getService().softDeleteById(entityId);
        applicationLogger.printLog("info", FrameWorkAbstractTreeController.class, getTreeModel().getSimpleName(), getTreeViewModel().getSimpleName(), "getTreeService().deleteNode()", null, null, "Method finish successfull!!!!");

        return new ActionResult(null, 0, 0, 0L, 0, "حذف با موفقیت انجام شد", "");
    }
}
