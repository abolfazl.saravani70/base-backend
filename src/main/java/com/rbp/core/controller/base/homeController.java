package com.rbp.core.controller.base;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class homeController {

    @RequestMapping("/")
    public String home(){
        return "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>#Sayban#</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Sayban Application</h1>\n" +
                "<a href=\"/swagger-ui.html\">Application REST API List</a>\n" +
                "</body>\n" +
                "</html>";
    }
    @RequestMapping(restConstant.BASE_REST)
    public RedirectView localRedirect() {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/swagger-ui.html");
        return redirectView;
    }
    @RequestMapping(restConstant.BASE_API)
    public RedirectView localRedirect2() {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/swagger-ui.html");
        return redirectView;
    }
}