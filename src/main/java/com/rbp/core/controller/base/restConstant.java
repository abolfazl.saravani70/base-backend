package com.rbp.core.controller.base;

public interface restConstant {
    String BASE_REST = "/rest";
    String BASE_API = "/api";
    String ID = "/{id}";

    //****************************
    //ag-grid
    String GET_ROWS = "/getRows";
    //****************************
    //core security
    //****************
    //group
    String GROUPS = "/groups";
    String WITHOUT_PERMISSIONS = "/without-permissions";
    String PERMISSIONS = "/permissions";
    String POST_GROUPS = "/post-groups";
    //******************************
    //organizationPosition
    String ORGANIZATION_POSITIONS = "/organization-positions";
    String POSITIONS = "/Positions";
    //************************************
    String USERS = "/users";
    //************************
    //core base
    String PROPERTIES = "/properties";
    String PROPERTY_VALUES = "/property-values";
    String PROPERTY_ID   ="/property-id";
    //************************
    //core basicInformation bankInfo
    String ACCOUNTS = "/accounts";
    String ORG_ID = "/org-id";
    String INSERT_ACCOUNT = "/insert-account";
    String DELETE_ACCOUNT = "/delete-account";
    String UPDATE_ACCOUNT = "/update-account";
    String BANKS = "/banks";
    String BRANCHES = "/branches";
    String BANK_ID = "/bank-id";
    //*****************************
    //core basicInformation geographical
    String CITIES = "/cities";
    String STATE_ID = "/state-id";
    String COUNTRIES = "/countries";
    String STATES = "/states";
    String VILLAGES = "/villages";
    String CITY_ID = "/city-id";
    String ZONES = "/zones";
    String VILLAGE_ID = "/village-id";
    String LOCATIONS = "/locations";
    //***************************
    //activity package
    String ACTIVITIES = "/activities";
    String ACTIVITY_TREES ="/activity-trees" ;
    String ACTIVITY_COSTS = "/activity-costs";
    String ACTIVITY_NORMS = "/activity-norms";
    String ACTIVITY_NORM_COSTS = "/activity-norm-costs";
    String ACTIVITY_PERFORMANCE_LOCKINGS = "/activity-performance-lockings";
    String ACTIVITY_QUALITIES = "/activity-qualities";
    String ACTIVITY_TEMPLATES = "/activity-templates";
    String ACTIVITY_TEMPLATE_TYPES = "/activity-template-types";
    String ACTIVITY_TYPE = "/activity-types";
    String ACTIVITY_KPIES = "/activity-kpies";
    String ACTIVITY_ORGANIZATION_UNITS = "/activity-organization-units";
    String ACTIVITY_RISKS = "/activity-risks";
    String ACTIVITY_SUBJECTIVES = "/activity-subjectives";
    String NORM_COSTS = "/norm-costs";
    String NORM_PARAMETERS = "/norm-parameters";
    String NORM_TYPES = "/norm-types";
    String PAID_CREDITS = "/paid-credits";
    String PAYMENTS = "/payments";
    String PERFORMANCES = "/performances";
    String PERFORMANCE_INDICATORS = "/performance-indicators";
    String QUALITATIVE_INDICATORS = "/qualitative-indicators";
    String SCALAR_INDICATORS = "/scalar-indicators";
    String ACT_SHARING_TYPES = "/act-sharing-types";
    String ACT_SHARING_DETAILS = "/act-sharing-details";
    String ACT_SHARINGS = "/act-sharings";
    String ACTIVITY_BUDGETS = "/activity-budgets";
    String ACTIVITY_HRS = "/activity-hrs";
    String ACTIVITY_LOCATIONS = "/activity-locations";
    //********************
    //application package
    String APP_ENTITIES = "/app-entities";
    String APP_ENUMS = "/app-enums";
    String APPLICATIONS = "/applications";
    String APPLICATION_KPIES = "/application-kpies";
    String COMMENTS = "/comments";
    String ENUM_VALUES = "/enum-values";
    String FIELDS = "/fields";
    String FORMS = "/forms";
    String ISSUES = "/issues";
    String MENUS = "/menus";

    String APP_MESSAGES = "/app-messages";
    String APP_MESSAGE_TYPES = "/app-message-types";
    String APP_SCHEDULINGS = "/app-schedulings";
    String NOTIFICATIONS = "/notifications";
    String PAGES = "/pages";
    String SCOPES = "/scopes";
    String SIGN_BOARDS = "/sign-boards";
    String STORE_PROCEDURES = "/store-procedures";
    String TESTS = "/tests";
    String TOPICS = "/topics";
    String VIEWS = "/views";
    //**********************
    //budgeting
    String ACCEPTED_CREDITS = "/accepted-credits";
    String ALLOCATIONS = "/allocations";
    String ALLOCATION_TYPES = "/allocation-types";
    String BUDGETS = "/budgets";
    String BUDGET_TREES = "/budget-trees";
    String BUDGET_TYPES = "/budget-types";
    String CREDITS = "/credits";
    String EXPENDITURES = "/expenditures";
    String EXPENSES = "/expenses";
    String FISCAL_YEARS = "/fiscal-years";
    String FUNDS = "/funds";
    String FUND_DETAILS = "/fund-details";
    String FUND_LIMITS = "/fund-limits";
    String POLICIES = "/policies";
    String SHERINGS = "/sharings";
    //****************************
    //goal
    String GOALS = "/goals";
    String GOAL_KPIES = "/goal-kpies";
    String GOAL_NORM_COSTS = "/goal-norm-costs";
    String GOAL_ORGANIZATION_UNITS = "goal-organization-units";
    String GOAL_RISKS = "/goal-risks";
    String GOAL_SUBJECTIVES = "/goal-subjectives";
    String GOAL_TREES ="/goal-trees";
    String GOAL_BUDGETS="/goal-budgets";
    String GOAL_HRS="/goal-hrs";
    String GOAL_LOCATIONS="/goal-locations";
    //****************************
    //kpi
    String KPIES = "/kpies";
    String KPI_Trees = "/kpi-trees";
    String KPI_TYPES = "/kpi-types";
    String UNITS = "/units";
    String UNIT_COSTS = "/unit-costs";
    //**************************
    //objectives
    String OBJECTIVES = "/objectives";
    String OBJECTIVE_TYPES = "/objective-types";
    String OBJECTIVE_TREES = "/objective-trees";
    String OBJECTIVE_SUBJECTIVES = "/objective-subjectives" ;
    String OBJECTIVE_ORGANIZATION_UNITS = "/objective-organization-units";
    String OBJECTIVE_KPIES ="/objective-kpies" ;
    String OBJECTIVE_RISKS = "/objective-risks" ;
    String OBJECTIVE_NORM_COSTS = "/objective-norm-costs" ;
    String OBJECTIVE_BUDGETS = "/objective-budgets";
    String OBJECTIVE_HRS = "/objective-hrs";
    String OBJECTIVE_LOCATIONS = "/objective-locations";
    //*************************************
    //organization
    String PARENT_ID ="parent-id";
    String PARENTS="/parents";
    String ADDRESSES = "/addresses";
    String CONFIRMATIONS = "/confirmations";
    String DASHBOARDS = "/dashboards";
    String EMAILS = "/emails";
    String GROUP_SCHEDULINGS = "/group-schedulings";
    String MESSAGE_BOXES = "/message-boxes";
    String MESSAGES = "/messages";
    String NOTES = "/notes";
    String ORGANIZATION_CATEGORIES = "/organization-categories";
    String ORGANIZATION_CHART_TEMPLATES = "/organization-chart-templates";
    String ORGANIZATIONS = "/organizations";
    String CHILDREN = "/children";
    String ORGANIZATION_REQUIREMENT_TYPES = "/organization-requirement-types";
    String ORGANIZATION_CHARTS = "/organization-charts";
    String ORGANIZATION_UNITS = "/organization-units";
    String ORGANIZATION_UNITS_GENERATOR = "/generator";
    String PERSONS = "/persons";
    String PHONES = "/phones";
    String PROFILES = "/profiles";
    String REQUIREMENTS = "/requirements";
    String RULES = "/rules";
    String SIGNS = "/signs";
    String SOCIAL_ACCOUNTS = "/socialAccounts";
    String STAKEHOLDERS = "/stakeholders";
    String USER_CONFIGS = "/user-configs";
    String USER_SCHEDULING_LISTS = "/user-scheduling-lists";
    String USER_TYPES = "/ user-types";
    String WIDGETS = "/widgets";
    String HUMAN_RESOURCES = "/human-resources";
    //****************************
    //plan
    String PLANS = "/plans";
    String PLAN_TYPES = "/plan-types";
    String PLAN_TREES ="/plan-trees";
    String PLAN_KPIES = "/plan-kpies";
    String PLAN_NORM_COSTS ="/'plan-norm-costs" ;
    String PLAN_ORGANIZATION_UNITS = "plan-organization-units";
    String PLAN_RISKS = "/plan-risks";
    String PLAN_SUBJECTIVES = "/plan-subjectives";
    String PLAN_BUDGETS="/plan-budgets";
    String PLAN_HRS="/plan-hrs";
    String PLAN_LOCATIONS="/plan-locations";
    //*******************************
    //project
    String PROJECT_RESOURCES = "/project-resources";
    String CONTRACTS = "/contracts";
    String CONTRACT_SECTIONS = "/contract-sections";
    String PROJECTS = "/projects";
    String PROJECT_TREES = "/project-trees";
    String PROJECT_RESOURCE_TYPES = "/project-resource-types";
    String PROJECT_KPIES = "/project-kpies" ;
    String PROJECT_NORM_COSTS = "/project-norm-costs";
    String PROJECT_ORGANIZATION_UNITS = "project-organization-units";
    String PROJECT_RISKS = "/project-risks";
    String PROJECT_SUBJECTIVES = "/project-subjectives";
    String PROJECT_BUDGETS = "/project-budgets";
    String PROJECT_HRS = "/project-hrs";
    String PROJECT_LOCATIONS = "/project-locations";
    //***********************************
    //report
    String FILTER_OPERATORS = "/filter-operators";
    String OPERAND_TYPES = "/operand-types";
    String REPORTS = "/reports";
    String REPORT_FILTERS = "/report-filters";
    String REPORT_TYPES = "/report-types";
    String SYSTEM_REPORT_FILTERS = "/system-report-filters";
    //*********************************************
    //risk
    String ALTERNATIVES = "/alternatives";
    String RISKS = "/risks";
    String RISK_RISK = "/risk-risks";
    String SOLUTIONS = "/solutions";
    //********************************
    //rule
    String PLANNING_TYPE_RULE = "/plan-type-rule";
    //********************************
    //strategies
    String STRATEGIES = "/strategies";
    String STRATEGY_TREES = "/strategy-Trees";
    String STRATEGIES_OF_OBJECTIVE = "/objective";
    String STRATEGY_EXECUTION_TYPES = "/strategy-execution-types";
    String STRATEGY_TYPES = "/strategy-types";
    String STRAREGY_KPIES = "/strategy-kpies";
    String STRATEGY_NORM_COSTS = "/strategy-norm-costs" ;
    String STRATEGY_ORGANIZATION_UNITS = "/strategy-organization-units";
    String STRATEGY_RISKS = "/strategy-risks";
    String STRATEGY_SUBJECTIVES = "/strategy-subjectives";
    String STRATEGY_BUDGETS = "/strategy-budgets";
    String STRATEGY_HRS = "/strategy-hrs";
    String STRATEGY_LOCATIONS = "/strategy-locations";
    //**********************************
    //subjective
    String SUBJECTIVES = "/subjectives";
    //********************************
    //system
    String BLOBS = "/blobs";
    String DOCUMENTS = "/documents";
    String DOCUMENT_DETAILS = "/document-details";
    String DOCUMENT_TYPES = "/document-types";
    String DUTIES = "/duties";
    String EVENTS = "/events";
    String EVENT_TYPES = "/event-types";
    String FORMULA_PARAMETERS = "/formula-parameters";
    String LETTERS = "/letters";
    String LICENSES = "/licenses";
    String MONITOR_CONFIGS = "/monitor-configs";
    String NEWS = "/news";
    String PROCESS_INSTANCES = "/process-instances";
    String PROCESS_STEPS = "/process-Steps";
    String PROCESS_TEMPLATES = "/process-templates";
    String REQUESTS = "/requests";
    String REQUESTERS = "/requesters";
    String SECURITY_SETTINGS = "/security-settings";
    String STEREO_TYPES = "/stereo-types";
    String SYS_POLICIES = "/sys-policies";
    String SYS_RULES = "/sys-rules";
    String SYS_RULE_FORMULAS = "/sys-rule-formulas";
    String SYS_SCEDULINGS = "/sys-schedulings";
    String SYS_SCOPES = "/sys-scopes";
    String SYSTEM_CONFIGS = "/system-configs";
    String SYSTEMS = "/systems";
    String SYSTEM_KPIES = "/system-kpies";
    String SYSTEM_SETTINGS = "/system-Settings";
    String TAGS = "/tags";
    //*******************************************
    //task
    String TASKS = "/tasks";
    String TASK_TREES ="/task-trees" ;
    String TASK_KPIES ="/task-kpies" ;
    String TASK_NORM_COSTS = "/task-norm-costs";
    String TASK_ORGANIZATION_UNITS = "/task-organization-units";
    String TASK_RISKS = "/task-risks";
    String TASK_SUBJECTIVES = "/task-subjectives";
    String TASK_BUDGETS = "/task-budgets";
    String TASK_HRS = "/task-hrs";
    String TASK_LOCATIONS = "/task-locations";
    //************************************
    //todoPackage
    String TODOS = "/todos";
    String TODO_TREES ="/todo-trees" ;
    String TODO_KPIES = "/todo-kpies";
    String TODO_NORM_COSTS = "/todo-norm-costs";
    String TODO_ORGANIZATION_UNITS = "/todo-organization-units";
    String TODO_RISKS = "/todo-risks";
    String TODO_SUBJECTIVES = "/todo-subjectives";
    String TODO_BUDGETS = "/todo-budgets";
    String TODO_HRS = "/todo-hrs";
    String TODO_LOCATIONS = "/todo-locations";
    //************************************
    //security
    String AUTHENTICATED_USER = "/authenticated-user" ;
    String USER_ID = "/user-id" ;
    String RESET_PASSWORD = "/reset-password";
    String CHANGE_PASSWORD = "/change-password";
    String ONLINE_COUNT = "/online-count";
    String GENERAL_UPDATE =  "/general-update";
    String SIGN_IN_REDIRECT = "/sign-in-redirect";
    String REGISTER ="/register" ;
    String INFO =  "/info";
    String LOGIN = "/login";
    String LOGOUT ="/logout" ;
    String GROUP_ID =  "/group-id";
    String ORGANIZATION_ACCOUNTS = "/organization-accounts" ;
    String SAME_ORGANIZATION = "/same-organization";
    String LOWER_DOWN_ORGANIZATIONS ="/lower-down-organizations" ;
    String HIGHER_ORGANIZATIONS ="/higher-organization" ;
    String SAME_ORGANIZATION_CHART_LEVEL = "/same-organization-chart-level";
    //***************************************
    //Flowable
    String FLOWABLE_TASK="/flowable-task";
    String FLOWABLE_PROCESS="/flowable-process";
    String CURRENT_DATE_TIME ="/current-date-time" ;
    String Subjective_Trees = "/subjective-trees";
}
