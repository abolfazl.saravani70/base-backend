package com.rbp.core.utility.security;
/**
 * @author Alireza Souhani 1398.02.01
 */

import com.bahman.securitycommon.model.security.UsersModel;
import com.rbp.core.service.security.SecurityService;
import com.rbp.core.utility.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;

//*************************************
//*************************************
//Commented UmgsUser(1l)
//The class has no arg constructor!
//*************************************
//*************************************
@Service
public class SecurityUtility {
	@Autowired
	SecurityService ss;
	 static SecurityService securityService;
	@PostConstruct
	public void init(){
		this.securityService=ss;
	}
	static int onlineUserCount=0;
	public static  Long getAuthenticatedUserId() {
		return getAuthenticatedUser().getId();
	}

	public static  UsersModel getAuthenticatedUser() {
		try {
			//This will return a user, what class represents a user is upto how SecurityContext has been filled when
			//Providing Authentication.
			return  securityService.getCurrentUser();
		} catch (Exception ex) {
			throw new ApplicationException(2,"کاربر جاری وجود ندارد.");
		}
	}

	//TODO: check to see if this actually returns the requested ip of the user!
	public static  String getRequestedIp() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteHost();
	}
	public static  Integer getUserLevelFlag(){
		UsersModel user=getAuthenticatedUser();
		return 0;
	}

	public static  String getUserZoneCode(){
		UsersModel user=getAuthenticatedUser();
		return "h";
	}

    public static Integer getUserPermissionFlag(){
        UsersModel user=getAuthenticatedUser();
        return 0;
    }
}
