package com.rbp.core.utility;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.util.Date;
/**
 * @author Alireza Souhani 1398.02.01
 */
@Component
public class JsonDateSerializer extends JsonSerializer<Date> {

   @Override
   public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException,
           JsonProcessingException {
      String formattedDate = DateConvertor.miladi2date(date);
      gen.writeString(formattedDate);

   }
}