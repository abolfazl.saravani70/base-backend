package com.rbp.core.utility;
/**
 * @author Alireza Souhani 1398.02.01
 */
public class SearchOption {
	
	private String where;
	private String order;
	private int pageNumber;
	private int pageSize;
	
	public SearchOption() {
		super();
	}
	public SearchOption(String where, String order, int pageNumber, int pageSize) {
		super();
		this.where = where;
		this.order = order;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}
	
	public String getWhere() {
		return where;
	}
	public void setWhere(String where) {
		this.where = where;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
