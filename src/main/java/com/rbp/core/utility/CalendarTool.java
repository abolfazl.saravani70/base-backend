package com.rbp.core.utility;
/**
 * @author Alireza Souhani 1398.02.01
 */
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;

@Component
public class CalendarTool {
   private int                 JDN;
   private int                 gDay;
   private int                 gMonth;
   private int                 gYear;
   private int                 irDay;
   private int                 irMonth;
   private int                 irYear;
   private int                 juDay;
   private int                 juMonth;
   private int                 juYear;
   private int                 arDay;
   private int                 arMonth;
   private int                 arYear;
   private int                 leap;
   private int                 march;

   private static final double ISLAMIC_EPOCH = 1948439.5;


   public static LocalDateTime convertToLocalDateTimeViaSqlTimestamp(Date dateToConvert) {
      if(dateToConvert==null) return null;
      return new java.sql.Timestamp(
              dateToConvert.getTime()).toLocalDateTime();
   }

   public static  Date convertToDateViaInstant(LocalDateTime dateToConvert) {
      if(dateToConvert==null) return null;
      return java.util.Date
              .from(dateToConvert.atZone(ZoneId.systemDefault())
                      .toInstant());
   }

   public CalendarTool() {
      GregorianCalendar localGregorianCalendar = new GregorianCalendar();
      setGregorianDate(localGregorianCalendar.get(1), 1 + localGregorianCalendar.get(2), localGregorianCalendar.get(5));
   }

   public CalendarTool(int paramInt1, int paramInt2, int paramInt3) {
      setGregorianDate(paramInt1, paramInt2, paramInt3);
   }

   private void IranianCalendar() {
      int[] arrayOfInt = { -61, 9, 38, 199, 426, 686, 756, 818, 1111, 1181, 1210, 1635, 2060, 2097, 2192, 2262, 2324,
            2394, 2456, 3178 };
      this.gYear = (621 + this.irYear);
      int i = -14;
      int j = arrayOfInt[0];
      int k = 1;
      int m;
      int n;
      do {
         m = arrayOfInt[k];
         n = m - j;
         if (this.irYear >= m) {
            i += 8 * (n / 33) + n % 33 / 4;
            j = m;
         }
         k++;
      } while ((k < 20) && (this.irYear >= m));
      int i1 = this.irYear - j;
      int i2 = i + (8 * (i1 / 33) + (3 + i1 % 33) / 4);
      if ((n % 33 == 4) && (n - i1 == 4)) {
         i2++;
      }
      int i3 = -150 + (this.gYear / 4 - 3 * (1 + this.gYear / 100) / 4);
      this.march = (i2 + 20 - i3);
      if (n - i1 < 6) {
         i1 = i1 - n + 33 * ((n + 4) / 33);
      }
      this.leap = ((-1 + (i1 + 1) % 33) % 4);
      if (this.leap == -1) {
         this.leap = 4;
      }
   }

   private int IranianDateToJDN() {
      IranianCalendar();
      return -1
            + (gregorianDateToJDN(this.gYear, 3, this.march) + 31 * (-1 + this.irMonth) - this.irMonth / 7
                  * (-7 + this.irMonth) + this.irDay);
   }

   private void JDNToGregorian() {
      int i = 139361631 + 4 * this.JDN + (-3908 + 4 * (3 * ((183187720 + 4 * this.JDN) / 146097) / 4));
      int j = 308 + 5 * (i % 1461 / 4);
      this.gDay = (1 + j % 153 / 5);
      this.gMonth = (1 + j / 153 % 12);
      this.gYear = (i / 1461 - 100100 + (8 - this.gMonth) / 6);
   }

   private int ArabicDateToJDN(int paramInt1, int paramInt2, int paramInt3) {
      return (int) Math.floor((paramInt3 + Math.ceil(29.5 * (paramInt2 - 1)) + (paramInt1 - 1) * 354
            + Math.floor((3 + (11 * paramInt1)) / 30) + ISLAMIC_EPOCH) - 1);
   }

   private void JDNToArabic() {
      double jd = this.JDN + 0.5;
      this.arYear = (int) Math.floor(((30 * (jd - ISLAMIC_EPOCH)) + 10646) / 10631);
      this.arMonth = (int) Math.min(12, Math.ceil((jd - (29 + ArabicDateToJDN(this.arYear, 1, 1))) / 29.5) + 1);
      this.arDay = (int) ((jd - ArabicDateToJDN(this.arYear, this.arMonth, 1)) + 1);
   }

   private void JDNToIranian() {
      JDNToGregorian();
      this.irYear = (-621 + this.gYear);
      IranianCalendar();
      int i = gregorianDateToJDN(this.gYear, 3, this.march);
      int j = this.JDN - i;
      if (j >= 0) {
         if (j <= 185) {
            this.irMonth = (1 + j / 31);
            this.irDay = (1 + j % 31);
            return;
         }
         j = j - 186;
      }
      this.irMonth = (7 + j / 30);
      this.irDay = (1 + j % 30);
      if (j < 0) {
         j = j + 179;
         if (this.leap == 1) {
            j++;
         }
         this.irYear = (-1 + this.irYear);
         this.irMonth = (7 + j / 30);
         this.irDay = (1 + j % 30);
      }
      return;
   }

   private void JDNToJulian() {
      int i = 139361631 + 4 * this.JDN;
      int j = 308 + 5 * (i % 1461 / 4);
      this.juDay = (1 + j % 153 / 5);
      this.juMonth = (1 + j / 153 % 12);
      this.juYear = (i / 1461 - 100100 + (8 - this.juMonth) / 6);
   }

   private int gregorianDateToJDN(int paramInt1, int paramInt2, int paramInt3) {
      return 752 + (paramInt3
            + (1461 * (100100 + (paramInt1 + (paramInt2 - 8) / 6)) / 4 + (2 + 153 * ((paramInt2 + 9) % 12)) / 5)
            - 34840408 - 3 * ((paramInt1 + 100100 + (paramInt2 - 8) / 6) / 100) / 4);
   }

   private int julianDateToJDN(int paramInt1, int paramInt2, int paramInt3) {
      return paramInt3
            + (1461 * (100100 + (paramInt1 + (paramInt2 - 8) / 6)) / 4 + (2 + 153 * ((paramInt2 + 9) % 12)) / 5)
            - 34840408;
   }

   public boolean IsLeap(int paramInt) {
      int[] arrayOfInt = { -61, 9, 38, 199, 426, 686, 756, 818, 1111, 1181, 1210, 1635, 2060, 2097, 2192, 2262, 2324,
            2394, 2456, 3178 };
      this.gYear = (paramInt + 621);
      int i = -14;
      int j = arrayOfInt[0];
      int k = 1;
      int m;
      int n;
      do {
         m = arrayOfInt[k];
         n = m - j;
         if (paramInt >= m) {
            i += 8 * (n / 33) + n % 33 / 4;
            j = m;
         }
         k++;
      } while ((k < 20) && (paramInt >= m));
      int i1 = paramInt - j;
      int i2 = i + (8 * (i1 / 33) + (3 + i1 % 33) / 4);
      if ((n % 33 == 4) && (n - i1 == 4)) {
         i2++;
      }
      int i3 = -150 + (this.gYear / 4 - 3 * (1 + this.gYear / 100) / 4);
      this.march = (i2 + 20 - i3);
      if (n - i1 < 6) {
         i1 = i1 - n + 33 * ((n + 4) / 33);
      }
      this.leap = ((-1 + (i1 + 1) % 33) % 4);
      if (this.leap == -1) {
         this.leap = 4;
      }
      boolean bool;
      if (this.leap != 4) {
         int i4 = this.leap;
         bool = false;
         if (i4 != 0) {
            ;
         }
      } else {
         bool = true;
      }
      return bool;
   }

   public int getJulianYear() {
      return this.juYear;
   }

   public String getWeekDayStr() {
      return new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }[getDayOfWeek()];
   }

   public void nextDay() {
      this.JDN = (1 + this.JDN);
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public void nextDay(int paramInt) {
      this.JDN = (paramInt + this.JDN);
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public void previousDay() {
      this.JDN = (-1 + this.JDN);
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public void previousDay(int paramInt) {
      this.JDN -= paramInt;
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public void setGregorianDate(int paramInt1, int paramInt2, int paramInt3) {
      this.gYear = paramInt1;
      this.gMonth = paramInt2;
      this.gDay = paramInt3;
      this.JDN = gregorianDateToJDN(paramInt1, paramInt2, paramInt3);
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public void setIranianDate(int paramInt1, int paramInt2, int paramInt3) {
      this.irYear = paramInt1;
      this.irMonth = paramInt2;
      this.irDay = paramInt3;
      this.JDN = IranianDateToJDN();
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public void setArabicDate(int paramInt1, int paramInt2, int paramInt3) {
      this.arYear = paramInt1;
      this.arMonth = paramInt2;
      this.arDay = paramInt3;
      this.JDN = ArabicDateToJDN(paramInt1, paramInt2, paramInt3);
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public void setJulianDate(int paramInt1, int paramInt2, int paramInt3) {
      this.juYear = paramInt1;
      this.juMonth = paramInt2;
      this.juDay = paramInt3;
      this.JDN = julianDateToJDN(paramInt1, paramInt2, paramInt3);
      JDNToIranian();
      JDNToJulian();
      JDNToGregorian();
      JDNToArabic();
   }

   public String toString() {
      return getWeekDayStr() + ", Gregorian:[" + getGregorianDate() + "], Julian:[" + getJulianDate() + "], Iranian:["
            + getIranianDate() + "]";
   }

   public int getDayOfWeek() {
      return this.JDN % 7;
   }

   public String getGregorianDate() {
      return this.gYear + "/" + this.gMonth + "/" + this.gDay;
   }

   public int getGregorianDay() {
      return this.gDay;
   }

   public int getGregorianMonth() {
      return this.gMonth;
   }

   public int getGregorianYear() {
      return this.gYear;
   }

   public String getArabicDate() {
      return this.arYear + "/" + this.arMonth + "/" + this.arDay;
   }

   public int getArabicDay() {
      return this.arDay;
   }

   public int getArabicMonth() {
      return this.arMonth;
   }

   public int getArabicYear() {
      return this.arYear;
   }

   public String getIranianDate() {
      return this.irYear + "/" + this.irMonth + "/" + this.irDay;
   }

   public int getIranianDay() {
      return this.irDay;
   }

   public int getIranianMonth() {
      return this.irMonth;
   }

   public int getIranianYear() {
      return this.irYear;
   }

   public String getJulianDate() {
      return this.juYear + "/" + this.juMonth + "/" + this.juDay;
   }

   public int getJulianDay() {
      return this.juDay;
   }

   public int getJulianMonth() {
      return this.juMonth;
   }

}