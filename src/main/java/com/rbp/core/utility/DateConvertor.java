package com.rbp.core.utility;
/**
 * @author Alireza Souhani 1398.02.01
 */
import org.springframework.util.StringUtils;

import java.util.Calendar;
import java.util.Date;

public abstract class DateConvertor {

   public static String convertDateTypes(String date, DateType src, DateType dest) {
      CalendarTool tool = new CalendarTool();
      int[] dateVals = DateUtils.stringDate2intDate(date);
      switch (src) {
      case ghamari:
         tool.setArabicDate(dateVals[0], dateVals[1], dateVals[2]);
         break;
      case shamsi:
         tool.setIranianDate(dateVals[0], dateVals[1], dateVals[2]);
         break;
      case miladi:
         tool.setGregorianDate(dateVals[0], dateVals[1], dateVals[2]);
         break;
      default:
         break;
      }
      switch (dest) {
      case ghamari:
         return tool.getArabicDate();
      case shamsi:
         return tool.getIranianDate();
      case miladi:
         return tool.getGregorianDate();
      default:
         break;
      }
      return null;
   }

   public static Date date2miladi(String date, String Time) {
      if (StringUtils.isEmpty(date) || StringUtils.isEmpty(Time))
         return null;
      DateType type = DateType.getFromSystem();
      Calendar cal = Calendar.getInstance();
      switch (type) {
      case shamsi:
         cal.setTime(DateUtility.solarToDate(date));
         break;
      case miladi:
         cal.setTime(DateUtility.gerToDate(date));
         break;
      case ghamari:
         String ger = ghamari2miladi(date);
         cal.setTime(DateUtility.gerToDate(ger));
         break;
      default:
         break;
      }
      int[] vals = new int[3];
      String[] svals = Time.split(":");
      for (int i = 0; i < svals.length; i++) {
         vals[i] = Integer.valueOf(svals[i]);
      }
      cal.set(Calendar.HOUR_OF_DAY, vals[0]);
      cal.set(Calendar.MINUTE, vals[1]);
      cal.set(Calendar.SECOND, vals[2]);
      return cal.getTime();
   }

   public static Date date2miladi(String date) {
      String time = getTimeFromMiladiDate(Calendar.getInstance().getTime());
      return date2miladi(date, time);
   }

   public static String miladi2date(Date miladi) {
      if (miladi == null) {
         return null;
      }
      DateType type = DateType.getFromSystem();
      String res;
      Calendar cal = Calendar.getInstance();
      cal.setTime(miladi);
      StringBuilder sb = new StringBuilder(10);
      sb.append(cal.get(Calendar.YEAR));
      sb.append("/");
      if (cal.get(Calendar.MONTH) + 1 < 10) {
         sb.append("0");
      }
      sb.append(cal.get(Calendar.MONTH) + 1);
      sb.append("/");
      if (cal.get(Calendar.DAY_OF_MONTH) < 10) {
         sb.append("0");
      }
      sb.append(cal.get(Calendar.DAY_OF_MONTH));
      sb.append("/");
      switch (type) {
      case shamsi:
         res = miladi2shamsi(sb.toString());
         break;
      case miladi:
         res = sb.toString();
         break;
      case ghamari:
         res = miladi2ghamari(sb.toString());
         res = "";
         break;
      default:
         res = "";
         break;
      }
      return res;
   }

   public static String getTimeFromMiladiDate(Date miladi) {
      if (miladi == null)
         return null;
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(miladi);
      return String.format("%d:%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
            calendar.get(Calendar.SECOND));
   }

   public static String shamsi2miladi(String shamsi) {
      CalendarTool tool = new CalendarTool();
      int[] dateVals = DateUtils.stringDate2intDate(shamsi);
      tool.setIranianDate(dateVals[0], dateVals[1], dateVals[2]);
      return tool.getGregorianDate();
   }

   @Deprecated
   public static String miladi2shamsi(String miladi) {
      CalendarTool tool = new CalendarTool();
      int[] dateVals = DateUtils.stringDate2intDate(miladi);
      tool.setGregorianDate(dateVals[0], dateVals[1], dateVals[2]);
      return tool.getIranianDate();
   }

   public static String shamsi2ghamari(String shamsi) {
      CalendarTool tool = new CalendarTool();
      int[] dateVals = DateUtils.stringDate2intDate(shamsi);
      tool.setIranianDate(dateVals[0], dateVals[1], dateVals[2]);
      return tool.getArabicDate();
   }

   public static String ghamari2shamsi(String ghamari) {
      CalendarTool tool = new CalendarTool();
      int[] dateVals = DateUtils.stringDate2intDate(ghamari);
      tool.setArabicDate(dateVals[0], dateVals[1], dateVals[2]);
      return tool.getIranianDate();
   }

   public static String miladi2ghamari(String miladi) {
      CalendarTool tool = new CalendarTool();
      int[] dateVals = DateUtils.stringDate2intDate(miladi);
      tool.setGregorianDate(dateVals[0], dateVals[1], dateVals[2]);
      return tool.getArabicDate();
   }

   public static String ghamari2miladi(String ghamari) {
      CalendarTool tool = new CalendarTool();
      int[] dateVals = DateUtils.stringDate2intDate(ghamari);
      tool.setArabicDate(dateVals[0], dateVals[1], dateVals[2]);
      return tool.getGregorianDate();
   }
}
