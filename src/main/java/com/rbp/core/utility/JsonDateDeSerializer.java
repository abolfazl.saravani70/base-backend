package com.rbp.core.utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
/**
 * @author Alireza Souhani 1398.02.01
 */
@Component
public class JsonDateDeSerializer extends JsonDeserializer<Date> {

   @Override
   public Date deserialize(JsonParser arg0, DeserializationContext arg1) throws IOException, JsonProcessingException {
      if (arg0.getText() == null || arg0.getText().equals("") || arg0.getText().length() == 1)
         return null;
      return DateConvertor.date2miladi(arg0.getText());
   }

}