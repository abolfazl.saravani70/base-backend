package com.rbp.core.utility;
/**
 * @author Alireza Souhani 1398.02.01
 */
public class ApplicationException extends RuntimeException{
	
	private Integer expCode;
	
	private String expMessage;
	
	private String entityName;
	
	private Exception exp;
	
	public ApplicationException(Integer expCode,String expMessage){
		this.expCode=expCode;
		this.expMessage=expMessage;
	}
	public ApplicationException(String expMessage, String entityName) {
		this.expMessage = expMessage;
		this.entityName = entityName;
	}
	
	public ApplicationException(Integer expCode,String expMessage,String entityName){
		this.expCode=expCode;
		this.expMessage=expMessage;
		this.entityName=entityName;
	}

	public Integer getExpCode() {
		return expCode;
	}

	public void setExpCode(Integer expCode) {
		this.expCode = expCode;
	}

	public String getExpMessage() {
		return expMessage;
	}

	public void setExpMessage(String expMessage) {
		this.expMessage = expMessage;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
}
