package com.rbp.core.utility;
/**
 * @author Alireza Souhani 1398.02.01
 */
public enum DateType {
   shamsi(1), miladi(2), ghamari(3);

   private int value;

   private DateType(int value) {
      this.value = value;
   }

   public static DateType get(int value) {
      for (DateType dateType : DateType.values()) {
         if (dateType.value == value) {
            return dateType;
         }
      }
      return null;
   }

   public static DateType getFromSystem() {
      /*
       * if (SecurityContextHolder.getContext().getAuthentication() != null) {
       * User USER = (User)
       * SecurityContextHolder.getContext().getAuthentication
       * ().getPrincipal(); UacCulture Culture = USER.getCulture(); if
       * (Culture != null) { switch (Culture.getCalendarType().getType()) {
       * case 1: return shamsi; case 2: return miladi; case 3: return ghamari;
       * } } } return shamsi;
       */
      return shamsi;
   }
}
