package com.rbp.core.utility;
/**
 * @author Alireza Souhani 1398.02.01
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class ApplicationLogger {
    //tst
    @Autowired
    PersianCalendar persianCalendar;

    public void printLog(String logLevel, Class clazz, String model, String viewModel, String methodName, String exceptionCode, String exceptionMessage, String customMessage) {
        Logger logger = LoggerFactory.getLogger(clazz);

        switch (logLevel.trim().toUpperCase()) {
            case "INFO":
                logger.info("Application Log INFO-----> " + persianCalendar.GregorianToSolar(new Timestamp(System.currentTimeMillis()) + "") + "---" + methodName + "---" + model + "---" + viewModel + "---" + customMessage);
                break;
            case "WARN":
                logger.warn("Application Log WARN-----> " + persianCalendar.GregorianToSolar(new Timestamp(System.currentTimeMillis()) + "") + "---" + methodName + "---" + model + "---" + viewModel + "---" + customMessage);
                break;
            case "DEBUG":
                logger.debug("Application Log DEBUG-----> " + persianCalendar.GregorianToSolar(new Timestamp(System.currentTimeMillis()) + "") + "---" + methodName + "---" + model + "---" + viewModel + "---" + exceptionCode + "---" + exceptionMessage + "---" + customMessage);
                break;
            case "ERROR":
                logger.error("Application Log ERROR-----> " + persianCalendar.GregorianToSolar(new Timestamp(System.currentTimeMillis()) + "") + "---" + methodName + "---" + model + "---" + viewModel + "---" + exceptionCode + "---" + exceptionMessage + "---" + customMessage);
                break;
        }

    }
}
