package com.rbp.core.utility;

import java.util.GregorianCalendar;
/**
 * @author Alireza Souhani 1398.02.01
 */
public class DateUtils {
   public static String formatUnformattedDate(String date) {
      if (!date.contains("/")) {
         return (date.substring(0, 4) + "/" + date.substring(5, 7) + "/" + date.substring(8, 10));
      } else {
         return date;
      }
   }

   public static int[] stringDate2intDate(String date) {
      int[] ints = new int[3];
      date = formatUnformattedDate(date);
      String[] vals = date.split("/");
      for (int i = 0; i < vals.length; i++) {
         ints[i] = Integer.valueOf(vals[i]);
      }
      return ints;
   }

   public static String calendar2DateString(GregorianCalendar cal) {
      return cal.get(GregorianCalendar.YEAR) + "/" + cal.get(GregorianCalendar.MONTH) + "/"
            + cal.get(GregorianCalendar.DAY_OF_MONTH);
   }

   public static int getDayOfWeek(GregorianCalendar cal) {// change day start
                                                          // from sunday to
                                                          // saturday
      int day = cal.get(GregorianCalendar.DAY_OF_WEEK);
      return (day % 7) + 1;
   }

   public static long StringDateToLong(String date) {
      int[] vals = stringDate2intDate(date);
      long res = vals[0] * 365;
      if ((vals[1] - 1) <= 6) {
         res += (vals[1] - 1) * 31;
      } else {
         res += ((vals[1] - 7) * 30) + 186;
      }
      res += (vals[2] - 1);
      return res;
   }

   public static String longToStringDate(long date) {
      int[] res = new int[3];
      res[0] = (int) (date / 365);
      date = (int) (date % 365);
      if (date < 186) {
         res[1] = (int) (date / 31) + 1;
         res[2] = (int) (date % 31) + 1;
      } else if (date < 336) {
         res[1] = (int) ((date - 186) / 30) + 7;
         res[2] = (int) ((date - 186) % 30) + 1;
      } else {
         res[1] = 12;
         res[2] = (int) ((date - 336) + 1);
      }
      return String.format("%d/%02d/%02d", res[0], res[1], res[2]);
   }

}
