package com.rbp.core.utility.caching;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Use this class to create as much cache managers as you want.
 *
 *
 */

@Component
//@TODO: ASK mr Sohani if this should be as SingleClass, I myself think it should be single!
public class CacheProviderFactory {
    List<com.rbp.core.utility.caching.CacheProvider> listCacheProvider;


    //initializing the list to store all the caches.
    public CacheProviderFactory() {
        listCacheProvider = new ArrayList<>();
    }

    /**
     * This method gets u a provider.
     *
     * @return A Cache Provider
     */
    public com.rbp.core.utility.caching.CacheProvider getCachProvider() {
        com.rbp.core.utility.caching.CacheProvider cp = new com.rbp.core.utility.caching.CacheProvider();
        cp.init();
        listCacheProvider.add(cp);
        return cp;
    }

    /**
     * Get all of the caches you have created using this provider.
     *
     * @return
     */
    public List<com.rbp.core.utility.caching.CacheProvider> getListCacheProvider() {
        return listCacheProvider;
    }
}
