package com.rbp.core.utility.caching;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;


/**
 * This class is used as bean to provide CacheManager and CacheManagerFactory.
 * This cache works with Annotations. You have to:
 * 1-Enable caching with @EnableCaching
 * 2-Specify Cache structures inside ehcache.xml (otherwise it won't work!)
 */

@Configuration
@EnableCaching
public class CacheConfiguration {
    // If no config location is specified, a CacheManager will be configured from "ehcache.xml" in
    // the root of the class path (that is, default EhCache initialization - as defined in the EhCache docs - will apply).
    //Neither Spring, nore Ehchace3 creates any cache! you have to put them in the "ehcache.xml" file and
    //Add this to the application.properties file: spring.cache.jcache.config=classpath:ehcache.xml

}