package com.rbp.core.utility.caching;

import org.ehcache.Cache;
import org.ehcache.StateTransitionException;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * ^^^if you want your cache to be threadsafe, Always get the value, delete it then reinsert it^^^
 * What do we need to configure for caching? (you can do it both in XML or programmatically.)
 * 1-We need some sort of name or ID in order to refer to the cache component
 * 2-How many elements do we want to store in the memory?
 * 3-Do we want to store those elements in the cache forever?
 * 4-If the answer for the preceding query is No, how long do we want elements
 *  to live?
 * 5-Do we want to store to disk as well?
 * 6-If the answer for the preceding query is Yes, how many elements do we want to store
 * on disk?
 * How to use this?
 * Create one Cache manager for each pair of unique Key and Value you have. Meaning:
 * you can have multiple caches with one cache manager with Long,String for example.
 * if you want Long,Person , create another cache manager to handle that for you.
 *
 */
//@TODO: add every exception here to REST advisor if any of these is suppose to be thrown using REST service
public class CacheProvider{

    protected org.ehcache.CacheManager cm=null;
    private List<Cache> cachlist;


    //build a cache manager. Config each Cache accordingly.
    public CacheProvider(){
        //This line helps to get the Class of a variable. Use it if you need it!
        //Since we are working with generics, how would we know which type to use?
//        keyclass=(Class<K>) ((ParameterizedType) getClass()
//                .getGenericSuperclass()).getActualTypeArguments()[0];
//        valueclass=(Class<V>) ((ParameterizedType) getClass()
//                .getGenericSuperclass()).getActualTypeArguments()[0];


        //There is no way to get all the caches from EhCache CacheManager!!!!
        //So store every cache ever created to have them at your disposal if needed.
        cachlist=new ArrayList<>();
    }

    /**
     * Initialize the CacheManager with Config files
     * A cache Manager Without any cache inside.
     */
    public  void init(){
        cm=CacheManagerBuilder.newCacheManagerBuilder().build(true);
    }


    /**
     * Add a cache to the list of caches. returns an exception if the cache with that name already exists.
     * @param name name of the cache
     * @param timetoidle idle time after which element is expired (set 0 for infinity)
     * @param timetolive life time after which element is expired (set 0 for infinity)
     * @param keyclass .Class of the key
     * @param valueclass .Class of the value
     */

    public <K,V> void addCache(String name, long timetolive,long timetoidle,Class<K> keyclass,
                          Class<V> valueclass,long heapsize){
        //Add a cache to the Cache list.
        //Throw the proper Exception when Manager already has that Cache.
        //@TODO: if we need OFFHEAP or DISK caches, we already need to implement Serializable objects!
        //@TODO: Do i need to add an exception handler for this as well?
        //---------------------CACHE CONFIG
        CacheConfiguration<K,V>  cacheConfiguration=null;
        cacheConfiguration= CacheConfigurationBuilder.newCacheConfigurationBuilder(
                        keyclass,valueclass, ResourcePoolsBuilder.heap(heapsize))
                        .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(timetolive)))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofSeconds(timetoidle)))
                        .build();
        //----------------------DONE

        try{
            //Create and add the cache to allCache list!
            cachlist.add(cm.createCache(name,cacheConfiguration));
        } catch (IllegalArgumentException e) {
            System.out.println("There is already a cache with that name!");
            e.printStackTrace();
        } catch (IllegalStateException e) {
            System.out.println("Cache Creation failed!");
            e.printStackTrace();
        } finally {
            //@TODO: do something if cache creation was successful. log it?

        }
    }

    /**
     * get the cache with the name, key.class and value.class you specified.
     * returns null if the cache doesn't exist.
     * @param name
     * @return Cache , null if there is no cache with this name.
     * Throws Exception with a sout message if the key.class or value.class are not right.
     */
    public <K, V> Cache getCache(String name, Class<K> keyclass, Class<V> valueclass) {
        //return the Cache with the Name
        Cache c=null;
        try {
            c = cm.getCache(name, keyclass, valueclass);
        } catch (IllegalArgumentException e) {
            System.out.println("Key or Value Type are not a match for this cache!");
            e.printStackTrace();
        }
        return c;
    }

    /**
     * @return
     * All Caches available inside the cache manager.
     * @TODO: this must be tested as it has been ommited from Ehcache3 despite the fact that it is availabe in JSR107
     */
    public List<Cache> getAll(){
        return cachlist;
    }


    /**delete a cache with the name you specified. Does nothing if there is no cache with the name.
     *Delete = Closing the cache and releasing all of its associated resources.
     * @param name
     */
    public void deleteCache(String name){
        //Delete a cache
        cm.removeCache(name);
    }

    /**
     * A cache manager has to be closed inside a finally block cleanly or close with application shutdown!
     * @todo Add @Bean(shutdown) if we want to use this bean or implement a clean close function.
     * if we need cache persistence in disk, we need to close this cleanly and not with spring.
     */
    public void shutDown(){
        try {
            cm.close();
        } catch (IllegalStateException e) {
            System.out.println("Cache Manager is not available!");
            e.printStackTrace();
        } catch (StateTransitionException e) {
            System.out.println("The CacheManager could not reach UNINITIALIZED cleanly");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Something went wrong with Shutdown process!");
            e.printStackTrace();
        }
    }
//    public static void main(String[] args){ //later for testing
//        CacheProvider cp=new CacheProvider();
//        cp.addCache();
//    }
}
