package com.rbp.core.utility;
/**
 * @author Alireza Souhani 1398.02.01
 */
import java.util.ArrayList;
import java.util.List;

/**
 * @param <T>
 */

public class QueryResult<T> {
	
	private int pageNumber;
	private int totalRecords;
	private int pageSize;
	private List<T> entityList = new ArrayList<T>();

	public QueryResult() {

	}

	public QueryResult(int pageNumber, int totalRecords, int pageSize,List<T> entityList) {
		super();
		this.pageNumber = pageNumber;
		this.totalRecords = totalRecords;
		this.pageSize = pageSize;
		this.entityList = entityList;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<T> getEntityList() {
		return entityList;
	}

	public void setEntityList(List<T> entityList) {
		this.entityList = entityList;
	}
}
