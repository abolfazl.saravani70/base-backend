package com.rbp.core.model.dto.base;

public class FilterJsonInput {
    private Integer pageNumber;
    private Integer pageSize;
    private String where;
    private String orderBy;
    private String groupBy;
    private String groupByFunction;
    private String having;
    private Boolean deleteAllData;

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void pageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void pageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getWhere() {
        return where;
    }

    public void where(String where) {
        this.where = where;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void orderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public void groupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public String getGroupByFunction() {
        return groupByFunction;
    }

    public void groupByFunction(String groupByFunction) {
        this.groupByFunction = groupByFunction;
    }

    public String getHaving() {
        return having;
    }

    public void having(String having) {
        this.having = having;
    }

    public Boolean getDeleteAllData() {
        return deleteAllData;
    }

    public void deleteAllData(Boolean deleteAllData) {
        this.deleteAllData = deleteAllData;
    }
}
