/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.basicInformation.bankinginfo;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import io.swagger.annotations.ApiModel;

@ApiModel(value = "Bank Entity")
public class BankDTO extends BaseEntityDTO {
    private static final long serialVersionUID = 3381929907178220226L;
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private String webService;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebService() {
        return webService;
    }

    public void setWebService(String webService) {
        this.webService = webService;
    }
}
