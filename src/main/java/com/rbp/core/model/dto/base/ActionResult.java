package com.rbp.core.model.dto.base;

import java.util.List;

public class ActionResult<T extends Object> {

	private List<T> data;
	private Integer pageNumber;
	private Integer pageSize;
	private Long recordsTotal;
	private Integer exceptionCode;
	private String exceptionMessage;
	private String token;
	private String draw;
	private Integer recordsFiltered;

	public ActionResult() {
	}

	public ActionResult(List<T> data, Integer pageNumber, Integer pageSize, Long totalCount,
						Integer exceptionCode, String exceptionMessage, String token) {
		super();
		this.data = data;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.recordsTotal = totalCount;
		this.exceptionCode = exceptionCode;
		this.exceptionMessage = exceptionMessage;
		this.token = token;
	}
	
	public ActionResult(List<T> data, Integer pageNumber, Integer pageSize, Long totalCount,
			Integer exceptionCode, String exceptionMessage, String token,String draw,Integer recordsFiltered) {
		super();
		this.data = data;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.recordsTotal = totalCount;
		this.exceptionCode = exceptionCode;
		this.exceptionMessage = exceptionMessage;
		this.token = token;
		this.draw=draw;
		this.recordsFiltered=recordsFiltered;
	}
	
	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(Integer exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(Long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public String getDraw() {
		return draw;
	}

	public void setDraw(String draw) {
		this.draw = draw;
	}

	public Integer getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(Integer recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	
	
}
