/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.dto.base.abstractClass;

public abstract class TreeBaseEntityDTO<selfType extends BaseEntityDTO> extends BaseEntityDTO {

//    private selfType child;
//    private selfType parent;

    public abstract selfType getChild();

    public abstract void setChild(selfType child);

    public abstract selfType getParent();

    public abstract void setParent(selfType parent);
}
