/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */
//
//package com.rbp.core.model.dto.security;
//
//import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
//
//import java.util.Set;
//
//public class OrganizationPositionDTO extends BaseEntityDTO {
//
//    private static final long serialVersionUID = 5291550367769394040L;
//    private Long parentPostitionId;
//    private String name;
//    private Integer code;
//    private Long pvOrganizationUnitId;
//    private Set<Long> usersId;
//    private Set<Long> GroupsId;
//
//    public Long getParentPostitionId() {
//        return parentPostitionId;
//    }
//
//    public void setParentPostitionId(Long parentPostitionId) {
//        this.parentPostitionId = parentPostitionId;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Integer getCode() {
//        return code;
//    }
//
//    public void setCode(Integer code) {
//        this.code = code;
//    }
//
//    public Long getPvOrganizationUnitId() {
//        return pvOrganizationUnitId;
//    }
//
//    public void setPvOrganizationUnitId(Long pvOrganizationUnitId) {
//        this.pvOrganizationUnitId = pvOrganizationUnitId;
//    }
//
//    public Set<Long> getUsersId() {
//        return usersId;
//    }
//
//    public void setUsersId(Set<Long> usersId) {
//        this.usersId = usersId;
//    }
//
//    public Set<Long> getGroupsId() {
//        return GroupsId;
//    }
//
//    public void setGroupsId(Set<Long> groupsId) {
//        GroupsId = groupsId;
//    }
//}
