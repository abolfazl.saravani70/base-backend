/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.base.abstractClass;


import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import io.swagger.models.auth.In;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

//@JsonInclude(value = JsonInclude.Include.NON_NULL) TODO: Test if UI gets bugs if we don NOT send Null values into JSON
public abstract class BaseEntityDTO implements Serializable {

    private static final long serialVersionUID = -9069377608901194635L;

    private Long id;
    private String ip;
    private Long version;
    private LocalDateTime createdDate;
    private Long createdById;
    private LocalDateTime updatedDate;
    private Long updatedById;
    private LocalDateTime deletedDate;
    private Long deletedById;
    private Boolean isDeleted;
    private Boolean isTempDeleted;
    private Boolean isStar;
    private Boolean isFinal;
    private Boolean isLocked;
    private Boolean isConfirmed;
    private String coding;
    private Long rowLevelId;
    private Long sysScope;
    private String userTag;
    private String groupTag;
    private String systemTag;
    private Integer permissionFlag;
    private Boolean isSecure;
    private Integer levelFlag;
    private Long requestId;
    private String description;
    private String zoneCode;
    private Long pvStereoTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getUpdatedById() {
        return updatedById;
    }

    public void setUpdatedById(Long updatedById) {
        this.updatedById = updatedById;
    }

    public LocalDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Long getDeletedById() {
        return deletedById;
    }

    public void setDeletedById(Long deletedById) {
        this.deletedById = deletedById;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Boolean getIsTempDeleted() {
        return isTempDeleted;
    }

    public void setIsTempDeleted(Boolean tempDeleted) {
        isTempDeleted = tempDeleted;
    }

    public Boolean getIsStar() {
        return isStar;
    }

    public void setIsStar(Boolean star) {
        isStar = star;
    }

    public Boolean getIsFinal() {
        return isFinal;
    }

    public void setIsFinal(Boolean aFinal) {
        isFinal = aFinal;
    }

    public Boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Boolean locked) {
        isLocked = locked;
    }

    public Boolean getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(Boolean confirmed) {
        isConfirmed = confirmed;
    }

    public String getCoding() {
        return coding;
    }

    public void setCoding(String coding) {
        this.coding = coding;
    }

    public Long getRowLevelId() {
        return rowLevelId;
    }

    public void setRowLevelId(Long rowLevelId) {
        this.rowLevelId = rowLevelId;
    }

    public Long getSysScope() {
        return sysScope;
    }

    public void setSysScope(Long sysScope) {
        this.sysScope = sysScope;
    }

    public String getUserTag() {
        return userTag;
    }

    public void setUserTag(String userTag) {
        this.userTag = userTag;
    }

    public String getGroupTag() {
        return groupTag;
    }

    public void setGroupTag(String groupTag) {
        this.groupTag = groupTag;
    }

    public String getSystemTag() {
        return systemTag;
    }

    public void setSystemTag(String systemTag) {
        this.systemTag = systemTag;
    }

    public Integer getPermissionFlag() {
        return permissionFlag;
    }

    public void setPermissionFlag(Integer permissionFlag) {
        this.permissionFlag = permissionFlag;
    }

    public Boolean getSecure() {
        return isSecure;
    }

    public void setSecure(Boolean secure) {
        isSecure = secure;
    }

    public Integer getLevelFlag() {
        return levelFlag;
    }

    public void setLevelFlag(Integer levelFlag) {
        this.levelFlag = levelFlag;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public Long getPvStereoTypeId() {
        return pvStereoTypeId;
    }

    public void setPvStereoTypeId(Long pvStereoTypeId) {
        this.pvStereoTypeId = pvStereoTypeId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Boolean getTempDeleted() {
        return isTempDeleted;
    }

    public void setTempDeleted(Boolean tempDeleted) {
        isTempDeleted = tempDeleted;
    }

    public Boolean getStar() {
        return isStar;
    }

    public void setStar(Boolean star) {
        isStar = star;
    }

    public Boolean getFinal() {
        return isFinal;
    }

    public void setFinal(Boolean aFinal) {
        isFinal = aFinal;
    }

    public Boolean getLocked() {
        return isLocked;
    }

    public void setLocked(Boolean locked) {
        isLocked = locked;
    }

    public Boolean getConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        isConfirmed = confirmed;
    }

    @Override
    public boolean equals(Object o) {
        if(o==null) return false;
        if (this == o) return true;
        if (!(o instanceof BaseEntityDTO)) return false;
        BaseEntityDTO that = (BaseEntityDTO) o;
        boolean x=getId()!=null&& getId().equals(that.getId());
        return x && getId()!=null && getId()!=0;
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(getId());
//    }
}
