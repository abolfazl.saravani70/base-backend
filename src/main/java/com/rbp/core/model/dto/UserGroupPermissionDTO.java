package com.rbp.core.model.dto;

import java.io.Serializable;

public class UserGroupPermissionDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long USER_ID;
	
	private String USER_FIRSTNAME;
	
	private String USER_LASTNAME;
	
	private Long GROUP_ID;
	
	private String GROUP_NAME;
	
	private String GROUP_PERSIOAN_NAME;
	
	private Long PERMISSION_ID;
	
	private String PERMISSION_TITLE;
	
	private String PERMISSION_NAME;
	
	private Long PERMISSION_PARENT_ID;
	
	private String PERMISSION_ENGLISH_NAME;
	
	private Long GROUP_PERMISSION_GROUP_ID;
	
	private Long GROUP_PERMISSION_PERM_ID;
	
	private Long ORGAN_POS_GROUP_ID;
	
	private Long ORGAN_POST_POSITION_ID;
	
	private String ORGAN_POST_POSITION_NAME;
	
	private Long ORGAN_POST_PARENT_ID;
	
	private String ORGANIZATION_POSITION_CODE;
	
	private String UMGS_PROV_ORGANIZATIONUNIT;

	public Long getUSER_ID() {
		return USER_ID;
	}

	public void setUSER_ID(Long uSER_ID) {
		USER_ID = uSER_ID;
	}

	public String getUSER_FIRSTNAME() {
		return USER_FIRSTNAME;
	}

	public void setUSER_FIRSTNAME(String uSER_FIRSTNAME) {
		USER_FIRSTNAME = uSER_FIRSTNAME;
	}

	public String getUSER_LASTNAME() {
		return USER_LASTNAME;
	}

	public void setUSER_LASTNAME(String uSER_LASTNAME) {
		USER_LASTNAME = uSER_LASTNAME;
	}

	public Long getGROUP_ID() {
		return GROUP_ID;
	}

	public void setGROUP_ID(Long gROUP_ID) {
		GROUP_ID = gROUP_ID;
	}

	public String getGROUP_NAME() {
		return GROUP_NAME;
	}

	public void setGROUP_NAME(String gROUP_NAME) {
		GROUP_NAME = gROUP_NAME;
	}

	public String getGROUP_PERSIOAN_NAME() {
		return GROUP_PERSIOAN_NAME;
	}

	public void setGROUP_PERSIOAN_NAME(String gROUP_PERSIOAN_NAME) {
		GROUP_PERSIOAN_NAME = gROUP_PERSIOAN_NAME;
	}

	public Long getPERMISSION_ID() {
		return PERMISSION_ID;
	}

	public void setPERMISSION_ID(Long pERMISSION_ID) {
		PERMISSION_ID = pERMISSION_ID;
	}

	public String getPERMISSION_TITLE() {
		return PERMISSION_TITLE;
	}

	public void setPERMISSION_TITLE(String pERMISSION_TITLE) {
		PERMISSION_TITLE = pERMISSION_TITLE;
	}

	public String getPERMISSION_NAME() {
		return PERMISSION_NAME;
	}

	public void setPERMISSION_NAME(String pERMISSION_NAME) {
		PERMISSION_NAME = pERMISSION_NAME;
	}

	public Long getPERMISSION_PARENT_ID() {
		return PERMISSION_PARENT_ID;
	}

	public void setPERMISSION_PARENT_ID(Long pERMISSION_PARENT_ID) {
		PERMISSION_PARENT_ID = pERMISSION_PARENT_ID;
	}

	public String getPERMISSION_ENGLISH_NAME() {
		return PERMISSION_ENGLISH_NAME;
	}

	public void setPERMISSION_ENGLISH_NAME(String pERMISSION_ENGLISH_NAME) {
		PERMISSION_ENGLISH_NAME = pERMISSION_ENGLISH_NAME;
	}

	public Long getGROUP_PERMISSION_GROUP_ID() {
		return GROUP_PERMISSION_GROUP_ID;
	}

	public void setGROUP_PERMISSION_GROUP_ID(Long gROUP_PERMISSION_GROUP_ID) {
		GROUP_PERMISSION_GROUP_ID = gROUP_PERMISSION_GROUP_ID;
	}

	public Long getGROUP_PERMISSION_PERM_ID() {
		return GROUP_PERMISSION_PERM_ID;
	}

	public void setGROUP_PERMISSION_PERM_ID(Long gROUP_PERMISSION_PERM_ID) {
		GROUP_PERMISSION_PERM_ID = gROUP_PERMISSION_PERM_ID;
	}

	public Long getORGAN_POS_GROUP_ID() {
		return ORGAN_POS_GROUP_ID;
	}

	public void setORGAN_POS_GROUP_ID(Long oRGAN_POS_GROUP_ID) {
		ORGAN_POS_GROUP_ID = oRGAN_POS_GROUP_ID;
	}

	public Long getORGAN_POST_POSITION_ID() {
		return ORGAN_POST_POSITION_ID;
	}

	public void setORGAN_POST_POSITION_ID(Long oRGAN_POST_POSITION_ID) {
		ORGAN_POST_POSITION_ID = oRGAN_POST_POSITION_ID;
	}

	public String getORGAN_POST_POSITION_NAME() {
		return ORGAN_POST_POSITION_NAME;
	}

	public void setORGAN_POST_POSITION_NAME(String oRGAN_POST_POSITION_NAME) {
		ORGAN_POST_POSITION_NAME = oRGAN_POST_POSITION_NAME;
	}

	public Long getORGAN_POST_PARENT_ID() {
		return ORGAN_POST_PARENT_ID;
	}

	public void setORGAN_POST_PARENT_ID(Long oRGAN_POST_PARENT_ID) {
		ORGAN_POST_PARENT_ID = oRGAN_POST_PARENT_ID;
	}

	public String getORGANIZATION_POSITION_CODE() {
		return ORGANIZATION_POSITION_CODE;
	}

	public void setORGANIZATION_POSITION_CODE(String oRGANIZATION_POSITION_CODE) {
		ORGANIZATION_POSITION_CODE = oRGANIZATION_POSITION_CODE;
	}

	public String getUMGS_PROV_ORGANIZATIONUNIT() {
		return UMGS_PROV_ORGANIZATIONUNIT;
	}

	public void setUMGS_PROV_ORGANIZATIONUNIT(String uMGS_PROV_ORGANIZATIONUNIT) {
		UMGS_PROV_ORGANIZATIONUNIT = uMGS_PROV_ORGANIZATIONUNIT;
	}
}
