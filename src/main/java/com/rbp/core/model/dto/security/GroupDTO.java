/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.security;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.util.Set;


public class GroupDTO extends BaseEntityDTO {

    private static final long serialVersionUID = 7825359694953531247L;
    private String name;
    private String persianName;
    private Set<Long> permissionId;
    private Long parentGroupId;
    private String parentGroupName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersianName() {
        return persianName;
    }

    public void setPersianName(String persianName) {
        this.persianName = persianName;
    }

    public Set<Long> getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Set<Long> permissionId) {
        this.permissionId = permissionId;
    }

    public Long getParentGroupId() {
        return parentGroupId;
    }

    public void setParentGroupId(Long parentGroupId) {
        this.parentGroupId = parentGroupId;
    }

    public String getParentGroupName() {
        return parentGroupName;
    }

    public void setParentGroupName(String parentGroupName) {
        this.parentGroupName = parentGroupName;
    }
}
