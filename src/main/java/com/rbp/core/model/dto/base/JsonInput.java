/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.base;

public class JsonInput<T> {
    private Integer type;
//    private Integer pageNumber;
//    private Integer pageSize;
    private Long fieldId;
    private String model;
    private String token;
//    private String filter;
//    private String sortColumns;
    private T data;
    private FilterJsonInput filter;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public FilterJsonInput getFilter() {
        return filter;
    }

    public void setFilter(FilterJsonInput filter) {
        this.filter = filter;
    }
}
