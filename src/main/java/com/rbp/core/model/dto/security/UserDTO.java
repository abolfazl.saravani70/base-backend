/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.security;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.sayban.model.dto.system.BlobDTO;

import java.util.Set;


public class UserDTO extends BaseEntityDTO {

    private static final long serialVersionUID = 8776648371346316401L;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Boolean isObsolete;
    private String lastLogin;
    private Boolean isOnline;
    private String expireDate;
    private String lastIp;
    private Boolean isActive;
    private String tell;
    private String mobile;
    private String picture;
    private String activeTheme;
    private Boolean isForceChangePassword;
    private String token;
    private Set<Long> groupId;
    private Long stakeholderId;

    public Long getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(Long stakeholderId) {
        this.stakeholderId = stakeholderId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIsObsolete() {
        return isObsolete;
    }

    public void setIsObsolete(Boolean obsolete) {
        this.isObsolete = obsolete;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public String getTell() {
        return tell;
    }

    public void setTell(String tell) {
        this.tell = tell;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getActiveTheme() {
        return activeTheme;
    }

    public void setActiveTheme(String activeTheme) {
        this.activeTheme = activeTheme;
    }

    public Boolean getIsForceChangePassword() {
        return isForceChangePassword;
    }

    public void setIsForceChangePassword(Boolean forceChangePassword) {
        this.isForceChangePassword = forceChangePassword;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Set<Long> getGroupId() {
        return groupId;
    }

    public void setGroupId(Set<Long> groupId) {
        this.groupId = groupId;
    }
}
