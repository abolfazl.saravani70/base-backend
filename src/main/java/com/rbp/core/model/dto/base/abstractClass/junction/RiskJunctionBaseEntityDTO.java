/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.base.abstractClass.junction;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;
import com.rbp.sayban.model.dto.risk.AlternativeDTO;
import com.rbp.sayban.model.dto.risk.RiskDTO;

public abstract class RiskJunctionBaseEntityDTO extends EvalStateBaseEntityDTO {

    private static final long serialVersionUID = 159997840990933054L;

    private Boolean isActive;
    private Boolean isActived;
    private RiskDTO risk;
    private AlternativeDTO alternative;


    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getIsActived() {
        return isActived;
    }

    public void setIsActived(Boolean actived) {
        this.isActived = actived;
    }

    public Boolean getActived() {
        return isActived;
    }

    public void setActived(Boolean actived) {
        isActived = actived;
    }

    public RiskDTO getRisk() {
        return risk;
    }

    public void setRisk(RiskDTO risk) {
        this.risk = risk;
    }

    public AlternativeDTO getAlternative() {
        return alternative;
    }

    public void setAlternative(AlternativeDTO alternative) {
        this.alternative = alternative;
    }
}
