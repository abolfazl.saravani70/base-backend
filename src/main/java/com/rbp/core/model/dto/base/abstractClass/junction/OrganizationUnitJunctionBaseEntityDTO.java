/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.base.abstractClass.junction;

import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;
import com.rbp.sayban.model.dto.organization.OrganizationUnitDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

public abstract class OrganizationUnitJunctionBaseEntityDTO extends EvalStateBaseEntityDTO {

    private static final long serialVersionUID = -5072797458331045051L;

    private LocalDate assignDate;
    private LocalDate dueDate;
    private Long pvStateId;
    private String pvStateTitle;
    private Long pvTypeId;
    private String pvTypeTitle;
    private OrganizationUnitDTO organizationUnit;

//    private Long ouStackholderId;
//    private Long ouAssignerId;
//    private Long ouExecutorId;
//    private Long ouSupervisorId;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public LocalDate getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(LocalDate assignDate) {
        this.assignDate = assignDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getPvStateId() {
        return pvStateId;
    }

    public void setPvStateId(Long pvStateId) {
        this.pvStateId = pvStateId;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvStateTitle() {
        return pvStateTitle;
    }

    public void setPvStateTitle(String pvStateTitle) {
        this.pvStateTitle = pvStateTitle;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public OrganizationUnitDTO getOrganizationUnit() {
        return organizationUnit;
    }

    public void setOrganizationUnit(OrganizationUnitDTO organizationUnit) {
        this.organizationUnit = organizationUnit;
    }

    //    public Long getOuStackholderId() {
//        return ouStackholderId;
//    }
//
//    public void setOuStackholderId(Long ouStackholderId) {
//        this.ouStackholderId = ouStackholderId;
//    }
//
//    public Long getOuAssignerId() {
//        return ouAssignerId;
//    }
//
//    public void setOuAssignerId(Long ouAssignerId) {
//        this.ouAssignerId = ouAssignerId;
//    }
//
//    public Long getOuExecutorId() {
//        return ouExecutorId;
//    }
//
//    public void setOuExecutorId(Long ouExecutorId) {
//        this.ouExecutorId = ouExecutorId;
//    }
//
//    public Long getOuSupervisorId() {
//        return ouSupervisorId;
//    }
//
//    public void setOuSupervisorId(Long ouSupervisorId) {
//        this.ouSupervisorId = ouSupervisorId;
//    }
}
