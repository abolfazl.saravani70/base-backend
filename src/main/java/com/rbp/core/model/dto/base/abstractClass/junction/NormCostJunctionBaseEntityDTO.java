/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.base.abstractClass.junction;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.NormCostDTO;

public abstract class NormCostJunctionBaseEntityDTO extends EvalStateBaseEntityDTO {

    private static final long serialVersionUID = 3127670536907500903L;

    private Long count;
    private Long minimum;
    private Long maximum;
    private Long average;
    private Long amount;
    private Double rate;
    private Long predicate;
    private NormCostDTO normCost;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getMinimum() {
        return minimum;
    }

    public void setMinimum(Long minimum) {
        this.minimum = minimum;
    }

    public Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        this.maximum = maximum;
    }

    public Long getAverage() {
        return average;
    }

    public void setAverage(Long average) {
        this.average = average;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Long getPredicate() {
        return predicate;
    }

    public void setPredicate(Long predicate) {
        this.predicate = predicate;
    }

    public NormCostDTO getNormCost() {
        return normCost;
    }

    public void setNormCost(NormCostDTO normCost) {
        this.normCost = normCost;
    }
}
