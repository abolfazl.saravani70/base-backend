/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.basicInformation.geographical;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import io.swagger.annotations.ApiModel;

@ApiModel(value = "Zone Entity")
public class ZoneDTO extends BaseEntityDTO {

    private static final long serialVersionUID = 5479549539048851385L;
    private String name;
    private String number;
    private String title;
   // private Boolean isCityZone;
    //private Long cityId;
    private Long villageId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public Boolean isCityZone() {
//        return cityZone;
//    }
//
//    public void setCityZone(Boolean cityZone) {
//        this.cityZone = cityZone;
//    }

//    public Long getCityId() {
//        return cityId;
//    }
//
//    public void setCityId(Long cityId) {
//        this.cityId = cityId;
//    }

    public Long getVillageId() {
        return villageId;
    }

    public void setVillageId(Long villageId) {
        this.villageId = villageId;
    }
}
