/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.dto.base.abstractClass.junction;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;

public abstract class SubjectiveJunctionBaseEntityDTO extends EvalStateBaseEntityDTO {

    private static final long serialVersionUID = 5782978858532181670L;


    private SubjectiveDTO subjective;

    public SubjectiveDTO getSubjective() {
        return subjective;
    }

    public void setSubjective(SubjectiveDTO subjective) {
        this.subjective = subjective;
    }
}
