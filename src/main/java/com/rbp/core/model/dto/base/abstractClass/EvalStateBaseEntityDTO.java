/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.base.abstractClass;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class EvalStateBaseEntityDTO extends BaseEntityDTO {

    private static final long serialVersionUID = 1164466995711803633L;

    private String name;
    private String title;
    private Long level;
    private Boolean isRoot;
    private Double initialPercentage;
    private Double percentage;
    private Long weight;
    private Long volume;
    private Integer priority;
    private Double importance;
    private LocalDate actualStartDate;
    private LocalDate actualEndDate;
    private LocalDate expirationDate;
    private LocalDate startDate;
    private LocalDate endDate;
    private String fiscalYear;
//    private Long organizationLocation;
//    private DocumentDTO document;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Boolean getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(Boolean root) {
        isRoot = root;
    }

    public Double getInitialPercentage() {
        return initialPercentage;
    }

    public void setInitialPercentage(Double initialPercentage) {
        this.initialPercentage = initialPercentage;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Double getImportance() {
        return importance;
    }

    public void setImportance(Double importance) {
        this.importance = importance;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getRoot() {
        return isRoot;
    }

    public void setRoot(Boolean root) {
        isRoot = root;
    }

    public LocalDate getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(LocalDate actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public LocalDate getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(LocalDate actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

//    public Long getOrganizationLocation() {
//        return organizationLocation;
//    }
//
//    public void setOrganizationLocation(Long organizationLocation) {
//        this.organizationLocation = organizationLocation;
//    }
//
//    public Long getDocument() {
//        return document;
//    }
//
//    public void setDocument(Long document) {
//        this.document = document;
//    }
}
