package com.rbp.core.model.dto.base.abstractClass.junction;

import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;
import com.rbp.sayban.model.dto.organization.HumanResourceDTO;

public abstract class HRJunctionBaseEntityDTO extends EvalStateBaseEntityDTO {


    private static final long serialVersionUID = -8900251868649824762L;

    private Long low;
    private Long high;
    private Long normal;
    private Double actualCost;
    private Long actualValue;
    private Double auditCost;
    private Long auditValue;
    private HumanResourceDTO humanResource;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public HumanResourceDTO getHumanResource() {
        return humanResource;
    }

    public void setHumanResource(HumanResourceDTO humanResource) {
        this.humanResource = humanResource;
    }
}
