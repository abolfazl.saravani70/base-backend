package com.rbp.core.model.dto;

public class MonitoringUserDTO {
	
	private String count;
		
	private String code;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
