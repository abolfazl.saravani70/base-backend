/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.basicInformation.geographical;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import io.swagger.annotations.ApiModel;

@ApiModel(value = "Country Entity")
public class CountryDTO extends BaseEntityDTO {
    private static final long serialVersionUID = -3639981506837280060L;
    private String name;
    private String number;
    private String title;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
