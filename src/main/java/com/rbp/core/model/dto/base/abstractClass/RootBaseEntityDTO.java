package com.rbp.core.model.dto.base.abstractClass;

public abstract class RootBaseEntityDTO<T extends BaseEntityDTO,K extends BaseEntityDTO> extends TreeBaseEntityDTO<T> {

//    private K root;

    public abstract K getRoot();

    public abstract void setRoot(K root);
}
