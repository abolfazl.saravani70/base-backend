/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.security;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;


public class UserOldPasswordDTO extends BaseEntityDTO {

    private static final long serialVersionUID = -5515010257430102123L;
    private String oldPassword;
    private Long userId;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
