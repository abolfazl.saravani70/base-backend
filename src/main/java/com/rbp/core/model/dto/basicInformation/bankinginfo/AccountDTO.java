/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dto.basicInformation.bankinginfo;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import io.swagger.annotations.ApiModel;

@ApiModel(value = "Account Entity")
public class AccountDTO extends BaseEntityDTO {
    private static final long serialVersionUID = 2078555659124005529L;

    private String number;
    private Long branchId;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }
}
