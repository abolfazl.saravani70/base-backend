package com.rbp.core.model.dto.base.abstractClass.junction;

import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;
import com.rbp.sayban.model.dto.budgeting.BudgetDTO;

public abstract class BudgetJunctionBaseEntityDTO extends EvalStateBaseEntityDTO {

    private static final long serialVersionUID = 4688381537071456113L;

    private Long estimateBudget;
    private Long actualBudget;
    private BudgetDTO budget;

    public Long getEstimateBudget() {
        return estimateBudget;
    }

    public void setEstimateBudget(Long estimateBudget) {
        this.estimateBudget = estimateBudget;
    }

    public Long getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Long actualBudget) {
        this.actualBudget = actualBudget;
    }

    public BudgetDTO getBudget() {
        return budget;
    }

    public void setBudget(BudgetDTO budget) {
        this.budget = budget;
    }
}
