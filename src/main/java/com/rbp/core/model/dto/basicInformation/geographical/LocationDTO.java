package com.rbp.core.model.dto.basicInformation.geographical;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import javax.persistence.Column;

public class LocationDTO extends BaseEntityDTO {

    private static final long serialVersionUID = 6960380600853642334L;
    private String name;
    private String number;
    private String title;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
