/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.search;

import javax.persistence.criteria.Expression;

public class SearchCriteria<T> {

    private Expression<T> key;
    private SearchOperation operation;
    private Object value;
    private boolean orPredicate;

    SearchCriteria(Expression<T> key, String operation, String value) {
        super();

        this.key = key;
        this.operation = SearchOperation.getSimpleOperation(operation);
        this.value = value;
    }

    public Expression<T> getKey() {
        return key;
    }

    public void setKey(final Expression<T> key) {
        this.key = key;
    }

    SearchOperation getOperation() {
        return operation;
    }

    public void setOperation(final SearchOperation operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(final Object value) {
        this.value = value;
    }

    public boolean isOrPredicate() {
        return orPredicate;
    }

    public void setOrPredicate(boolean orPredicate) {
        this.orPredicate = orPredicate;
    }

}