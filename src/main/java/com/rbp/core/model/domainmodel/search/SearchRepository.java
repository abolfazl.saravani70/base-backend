package com.rbp.core.model.domainmodel.search;

import com.rbp.core.model.dto.base.FilterJsonInput;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.*;

import static com.rbp.core.model.domainmodel.search.SearchOperation.*;
import static java.lang.String.format;

public class SearchRepository<T> {


    private final EntityManager entityManager;

    public SearchRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public AdvancedSearchResult<T> search(Class cls, FilterJsonInput filter, Boolean isTempDeleted) {
        AdvancedSearchResult result = new AdvancedSearchResult();
        SearchParameter sp = null;
        // TODO: 11/13/2019
        //  filter.where(secureConditions(filter.getWhere(),isTempDeleted));
        if (filter.getGroupBy() == null || filter.getGroupBy().equals(BLANK))
            sp = getSearchParameter(cls, cls);
        else
            sp = getSearchParameter(cls, Object[].class);

        if (filter.getWhere() != null && !filter.getWhere().equals(BLANK)) {
            sp.setPredicate(sp.getParsedPredicate(filter.getWhere()));
        }
        if (filter.getGroupBy() != null && !filter.getGroupBy().equals(BLANK)) {
            if (filter.getHaving() != null && !filter.getHaving().equals(BLANK)) {
                sp.setHavingPredicate(sp.getParsedPredicate(filter.getHaving()));
                groupBy(sp, filter.getGroupBy(), filter.getGroupByFunction(), true);
            } else {
                groupBy(sp, filter.getGroupBy(), filter.getGroupByFunction(), false);
            }
        }

        if (filter.getOrderBy() != null && !filter.getOrderBy().equals(BLANK)) {
            if (filter.getGroupBy() != null && !filter.getGroupBy().equals(BLANK)) {
                orderParseForGroupBy(sp, filter.getOrderBy());
            } else {
                orderByList(sp.getQuery().where(sp.getPredicate()), sp.getParsedOrderBys(filter.getOrderBy(), filter.getGroupBy()));
            }
        } else {
            sp.getQuery().where(sp.getPredicate());
        }
        if (filter.getPageSize() != 0) {
            result.setRecords(entityManager.createQuery(sp.getQuery())
                    .setFirstResult((filter.getPageNumber() - 1) * filter.getPageSize())
                    .setMaxResults(filter.getPageSize())
                    .getResultList());
        } else {
            result.setRecords(entityManager.createQuery(sp.getQuery()).getResultList());
        }
        result.setTotalCount(countOfSearch(cls, filter.getWhere()));
        return result;
    }

    private CriteriaQuery<T> orderByList(CriteriaQuery<T> query, List<Order> orders) {
        return query.having().orderBy(orders);
    }

    private void orderParseForGroupBy(SearchParameter sp, String orderBy) {
        CriteriaBuilder criteriaBuilder = sp.getBuilder();
        CriteriaQuery<Object[]> query = sp.getQuery();
        String[] orders = orderBy.trim().split(SPLIT_FOR_COLUMN);
        List<Order> expOrderBys = new ArrayList<>();
        for (String order : orders) {
            int i = 0;
            if (order.endsWith(SPLIT_FOR_DESCENDING)) {
                String orderSub = order.substring(0, order.length() - 1);
                i = sp.getOrderList().indexOf(orderSub);
                expOrderBys.add(criteriaBuilder.desc((Expression<?>) sp.getLst().get(i)));
            } else {
                i = sp.getOrderList().indexOf(order);
                expOrderBys.add(criteriaBuilder.asc((Expression<?>) sp.getLst().get(i)));
            }
        }
        query.orderBy(expOrderBys);
    }

    private String secureConditions(String queryStr, Boolean tempDeleted) {
        String securityFilter;
        // TODO: 11/5/2019 Remove const values 
        Integer userLevelFlag = 3; //SecurityUtility.getUserLevelFlag(); 
        Integer userPermissionFlag = 3;//SecurityUtility.getUserPermissionFlag();
        String userZoneCode = "001.002.003";// SecurityUtility.getUserZoneCode();
        String subZoneCode = userZoneCode.substring(0, userZoneCode.lastIndexOf("."));
        if (queryStr.equals("")) {
            securityFilter = format("$($$($isDeleted=false$and$isTempDeleted=%1$b" +
                            "$and$isSecure=false$)$$or$$($isDeleted=false$and$isTempDeleted=%1$b" +
                            "$and$isSecure=true$and$zoneCode=%2$s*" +
                            "$and$levelFlag<=%3$d$and$permissionFlag<=%4$d$)$$)$",
                    tempDeleted, subZoneCode, userLevelFlag, userPermissionFlag);


        } else {
            securityFilter = format("$($%1$s$)$$and$$($$($isDeleted=false$and$isTempDeleted=%2$b" +
                            "$and$isSecure=false$)$$or$$($isDeleted=false$and$isTempDeleted=%2$b" +
                            "$and$isSecure=true$and$zoneCode=%3$s*" +
                            "$and$levelFlag<=%4$d$and$permissionFlag<=%5$d$)$$)$",
                    queryStr, tempDeleted, subZoneCode, userLevelFlag, userPermissionFlag);


        }
        return securityFilter;
    }

    private Long countOfSearch(Class cls, String queryStr) {
        SearchParameter sp = getSearchParameter(cls, cls);
        Predicate predicate = sp.getParsedPredicate(queryStr);
        if (predicate != null)
            sp.setPredicate(predicate);
        sp.getQuery().select(sp.getBuilder().count(sp.getRoot()));
        sp.getQuery().where(sp.getPredicate());
        Long count = (Long) entityManager.createQuery(sp.getQuery())
                .getSingleResult();
        return count;
    }

    private SearchParameter getSearchParameter(Class root, Class result) {
        SearchParameter searchParameter = new SearchParameter();
        searchParameter.setBuilder(entityManager.getCriteriaBuilder());
        searchParameter.setQuery(searchParameter.getBuilder().createQuery(result));
        searchParameter.setRoot(searchParameter.getQuery().from(root));
        searchParameter.setPredicate(searchParameter.getBuilder().conjunction());
        searchParameter.setSearchConsumer(new SearchQueryCriteria(searchParameter.getPredicate(),
                searchParameter.getBuilder()));
        searchParameter.setCriteriaParser(new CriteriaParser());
        return searchParameter;
    }

    private void groupBy(SearchParameter sp, String groupByColumns, String functionGroupBy, Boolean isHaving) {
        CriteriaBuilder criteriaBuilder = sp.getBuilder();
        CriteriaQuery<Object[]> query = sp.getQuery();
        List<String[]> funcTittles = sp.getCriteriaParser().groupByParser(sp, groupByColumns,
                functionGroupBy);
        for (String[] funcTittle : funcTittles) {
            switch (funcTittle[0]) {
                case COUNT:
                    sp.getLst().add(criteriaBuilder.count(sp.getRoot().get("id")));
                    break;
                case SUM:
                    sp.getLst().add(criteriaBuilder.sum(sp.getRoot().get(funcTittle[1])));
                    break;
                case AVG:
                    sp.getLst().add(criteriaBuilder.avg(sp.getRoot().get(funcTittle[1])));
                    break;
                case MIN:
                    sp.getLst().add(criteriaBuilder.min(sp.getRoot().get(funcTittle[1])));
                    break;
                case MAX:
                    sp.getLst().add(criteriaBuilder.max(sp.getRoot().get(funcTittle[1])));
                    break;
            }
        }
        if (isHaving) {
            query.multiselect(sp.getLst()).groupBy(sp.getExpressionList());
            query.having(sp.getHavingPredicate());

        } else {
            query.multiselect(sp.getLst()).groupBy(sp.getExpressionList());
        }
    }
}
