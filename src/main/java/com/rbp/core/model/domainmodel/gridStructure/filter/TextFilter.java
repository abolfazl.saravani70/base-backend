/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.gridStructure.filter;

public class TextFilter {

    private String type;
    private String filter;
    private String filterType;

    public String getType() {
        return type;
    }

    public String getFilter() {
        return filter;
    }

    public String getFilterType() {
        return filterType;
    }
}
