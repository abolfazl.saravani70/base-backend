/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.base.abstractClass.junction;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.sayban.model.domainmodel.subjective.Subjective;

import javax.persistence.*;

@MappedSuperclass
public abstract class SubjectiveJunctionBaseEntity extends EvalStateBaseEntity {

    private static final long serialVersionUID = -2298876893264758263L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SUBJECTIVE_ID")
    private Subjective subjective;

    public Subjective getSubjective() {
        return subjective;
    }

    public void setSubjective(Subjective subjective) {
        this.subjective = subjective;
    }
}
