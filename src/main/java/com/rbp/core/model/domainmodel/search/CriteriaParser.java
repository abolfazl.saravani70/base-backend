/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.search;


import com.rbp.core.utility.ApplicationException;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.util.*;
import java.util.regex.Matcher;

import static com.rbp.core.model.domainmodel.search.SearchOperation.*;

class CriteriaParser {

    private static Map<String, Operator> ops;

    private enum Operator {
        OR(1), AND(2);
        final int precedence;

        Operator(int p) {
            precedence = p;
        }
    }

    static {
        Map<String, Operator> tempMap = new HashMap<>();
        tempMap.put(AND_OPERATOR, Operator.AND);
        tempMap.put(OR_OPERATOR, Operator.OR);
        tempMap.put(LOWER_CASE_OR_OPERATOR, Operator.OR);
        tempMap.put(LOWER_CASE_AND_OPERATOR, Operator.AND);
        ops = Collections.unmodifiableMap(tempMap);
    }

    private static boolean isHigerPrecedenceOperator(String currOp, String prevOp) {
        return (ops.containsKey(prevOp) && ops.get(prevOp).precedence >= ops.get(currOp).precedence);
    }

    Deque<?> parse(SearchParameter sp, String searchParam) {
        Deque<Object> output = new LinkedList<>();
        Deque<String> stack = new LinkedList<>();
        String[] tt = searchParam.split(SPLIT_FOR_OPERATOR);
        Arrays.stream(searchParam.split(SPLIT_FOR_OPERATOR)).forEach(token -> {
            if (!token.equals("")) {
                if (ops.containsKey(token)) {
                    while (!stack.isEmpty() && isHigerPrecedenceOperator(token, stack.peek()))
                        output.push(stack.pop()
                                .equalsIgnoreCase(OR_OPERATOR) ? OR_OPERATOR : AND_OPERATOR);
                    stack.push(token.equalsIgnoreCase(OR_OPERATOR) ? OR_OPERATOR : AND_OPERATOR);
                } else if (token.equals(LEFT_PARENTHESES)) {
                    stack.push(LEFT_PARENTHESES);
                } else if (token.equals(RIGHT_PARENTHESES)) {
                    while (!stack.peek()
                            .equals(LEFT_PARENTHESES))
                        output.push(stack.pop());
                    stack.pop();
                } else {

                    Matcher matcher = SPEC_CRITERIA_REGEX.matcher(token);

                    while (matcher.find()) {
                        if (!matcher.group(1).contains(SPLIT_FOR_GROUP_BY_FUNCTION)) {
                            Expression<String> expr = getPath(sp, matcher.group(1));
                            output.push(new SearchCriteria(expr, matcher.group(2), matcher.group(3)));
                        } else {
                            Expression<String> expr = getPath(sp, matcher.group(1));
                            output.push(new SearchCriteria(expr, matcher.group(2), matcher.group(3)));
                        }
                    }
                }
            }
        });
        while (!stack.isEmpty())
            output.addFirst(stack.pop());
        return output;
    }

    List<String[]> groupByParser(SearchParameter sp, String groupByColumns, String functionGroupBy) {
        String[] columns = groupByColumns.split(SPLIT_FOR_COLUMN);
        String[] functions = functionGroupBy.split(SPLIT_FOR_COLUMN);
        sp.setOrderList(Arrays.asList((String[]) ArrayUtils.addAll(columns, functions)));
        List<String[]> funcTittles = new ArrayList<>();
        sp.setExpressionList(new ArrayList<>(5));
        sp.setLst(new ArrayList<>());
        for (String column : columns) {
            sp.getExpressionList().add(sp.getRoot().get(column));
            sp.getLst().add(sp.getRoot().get(column));
        }
        for (String function : functions) {
            Matcher matcher = SPEC_GROUP_REGEX.matcher(function.trim());
            while (matcher.find()) {
                String[] funcSplit = new String[2];
                funcSplit[0] = matcher.group(1);
                funcSplit[1] = matcher.group(2);
                funcTittles.add(funcSplit);
            }
        }
        return funcTittles;
    }

    /*
    sortColumns : "column name1,column name2,....,column name n"
    if field is LocalDateTime then column name=column name#YYYY-MM-DD-hh-mm-ss
     YYYY=year , MM=month , DD=day , hh=hour , mm=minute , ss=second
    if order by is descending then column name=column name^
    if field is LocalDateTime and order by is descending then for example column name= column name#YYYY^

    /**
     *     sortColumns : "column name1,column name2,....,column name n"
     *     if field is LocalDateTime then column name=column name#YYYY-MM-DD-hh-mm-ss
     *      YYYY=year , MM=month , DD=day , hh=hour , mm=minute , ss=second
     *     if order by is descending then column name=column name^
     *     if field is LocalDateTime and order by is descending then for example column name= column name#YYYY^
     * @param root
     * @param orderBy
     * @param searchQueryCriteria
     * @return this will return an ordered list specified by the orderBy
     */
    List<Order> orderParse(SearchParameter sp, String orderBy, String groupByColumn) {
        if (StringUtils.isEmpty(orderBy)) {
            return Collections.emptyList();
        }
        String[] groups = orderBy.trim().split(SPLIT_FOR_COLUMN);
        List<Order> orders = new ArrayList<>(groups.length);
        for (String field : groups) {
            boolean ascending = true;
            if (!field.contains(SPLIT_FOR_DATE) && field.endsWith(SPLIT_FOR_DESCENDING)) {
                ascending = false;
                field = field.substring(0, field.length() - 1);
            }
            if (field.contains(SPLIT_FOR_DATE)) {
                orders.addAll(orderDate(sp, field));
            } else {
                orders.add(new OrderImpl(getPath(sp, field), ascending));
            }
        }

        return orders;
    }

    private static <X> Expression getPath(SearchParameter sp, String name) {
        String[] array = null;
        Expression expr = null;

        if (!name.contains(SPLIT_FOR_GROUP_BY_FUNCTION)) {
            // where condition
            array = name.split(SPLIT_FOR_PATH);
            expr = sp.getRoot().get(array[0]);
            for (int i = 1; i < array.length; i++) {
                expr = ((Path<X>) expr).get(array[i]);
            }
            return expr;
        } else {
            // having condition
            array = name.split(SPLIT_FOR_GROUP_BY_FUNCTION);
            expr = sp.getRoot().get(array[1]);
            switch (array[0]) {
                case COUNT:
                    return sp.getBuilder().count(expr);
                case SUM:
                    return sp.getBuilder().sum(expr);

                case AVG:
                    return sp.getBuilder().avg(expr);

                case MIN:
                    return sp.getBuilder().min(expr);

                case MAX:
                    return sp.getBuilder().max(expr);
            }
            return null;

        }
    }

    private List<Order> orderDate(SearchParameter sp, String field) {
        String[] array = field.split(SPLIT_FOR_DATE);
        List<Order> orders = new ArrayList<>();
        Expression exp = getPath(sp, array[0]);
        SearchCriteria searchCriteria = new SearchCriteria(exp, BLANK, BLANK);
        Expression t;
        if (array.length > 1) {
            String[] pats = array[1].split(SPLIT_FOR_FORMAT_DATE);
            for (String pat : pats) {
                boolean ascending = true;
                if (pat.endsWith(SPLIT_FOR_DESCENDING)) {
                    ascending = false;
                    pat = pat.substring(0, pat.length() - 1);
                }

                switch (pat) {
                    case FORMAT_OF_YEAR:
                        t = sp.getSearchConsumer().getComponentsOfDate(searchCriteria, YEAR);
                        orders.add(new OrderImpl(t, ascending));
                        break;
                    case FORMAT_OF_MONTH:
                        t = sp.getSearchConsumer().getComponentsOfDate(searchCriteria, MONTH);
                        orders.add(new OrderImpl(t, ascending));
                        break;
                    case FORMAT_OF_DAY:
                        t = sp.getSearchConsumer().getComponentsOfDate(searchCriteria, DAY);
                        orders.add(new OrderImpl(t, ascending));
                        break;
                    case FORMAT_OF_HOUR:
                        t = sp.getSearchConsumer().getComponentsOfDate(searchCriteria, HOUR);
                        orders.add(new OrderImpl(t, ascending));
                        break;
                    case FORMAT_OF_MINUTE:
                        t = sp.getSearchConsumer().getComponentsOfDate(searchCriteria, MINUTE);
                        orders.add(new OrderImpl(t, ascending));
                        break;
                    case FORMAT_OF_SECOND:
                        t = sp.getSearchConsumer().getComponentsOfDate(searchCriteria, SECOND);
                        orders.add(new OrderImpl(t, ascending));
                        break;

                }
            }
        } else
            //TODO: 7/16/2019 throw exception
            throw new ApplicationException(0, "الگوی تاریخ را صحیح وارد کنید");
        return orders;
    }
}
