package com.rbp.core.model.domainmodel.flowable;

import com.rbp.core.utility.ApplicationException;

public enum TaskDueDateEnum {
    RED (1),
    YELLOW(2),
    GREEN(3);
    private int level_code;
    //indicators show how much difference in date must be for that color to be active!
    private static int redIndicator=0;
    private static int yellowIndicator=3;
    private static int greenIndicator=4;

    TaskDueDateEnum(int level_code) {
        this.level_code=level_code;
    }

    public int getLevel_code() {
        return level_code;
    }

    public static int getRedIndicator() {
        return redIndicator;
    }

    public static void setRedIndicator(int redIndicatorr) {
        if(redIndicator>yellowIndicator || redIndicator>greenIndicator)
            throw new ApplicationException(1,"نشانگر قرمز باید از زرد و سبز کوچکتر باشد!");
        redIndicator = redIndicatorr;
    }

    public static int getYellowIndicator() {
        return yellowIndicator;
    }

    public static void setYellowIndicator(int yellowIndicatorr) {
        if(yellowIndicator<redIndicator || yellowIndicator>greenIndicator)
            throw new ApplicationException(1,"نشانگر زرد باید از سبز کوچکتر و از قرمز بزرگتر باشد.");
        yellowIndicator = yellowIndicatorr;
    }

    public static int getGreenIndicator() {
        return greenIndicator;
    }

    public static void setGreenIndicator(int greenIndicatorr) {
        if(greenIndicator<yellowIndicator||greenIndicator<redIndicator)
            throw new ApplicationException(1,"نشانگر سبز باید از قرمز و زرد بزرگتر باشد!");
        greenIndicator = greenIndicatorr;
    }
}
