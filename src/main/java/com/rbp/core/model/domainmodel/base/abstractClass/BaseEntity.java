/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.base.abstractClass;

import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.utility.security.SecurityUtility;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
//TODO: set zone code and Ip address inside BE for update/delete/insert
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = -4960957756039804651L;

	@Id
	@org.springframework.data.annotation.AccessType(value= org.springframework.data.annotation.AccessType.Type.PROPERTY)
	@Column(name = "ID",updatable = false,nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "SEQ_GENERATOR")
	private Long id;

	@Column(name = "IP")
	private String ip;

	@Version
	@Column(name = "VERSION")
	private Long version;

	@ApiModelProperty(access ="hidden")
	@Column(name = "CREATED_DATE",updatable = false)
	private LocalDateTime createdDate;

	@Column(name = "CREATED_BY")
	private Long createdById;

	@ApiModelProperty(access ="hidden")
	@Column(name = "UPDATED_DATE")
	private LocalDateTime updatedDate;

	@Column(name = "UPDATED_BY")
	private Long updatedById;

	@ApiModelProperty(access ="hidden")
	@Column(name = "DELETED_DATE")
	private LocalDateTime deletedDate;

	@Column(name = "DELETED_BY")
	private Long deletedById;

	@Column(name = "IS_DELETED")
	private Boolean isDeleted;

	@Column(name = "IS_TEMP_DELETED")
	private Boolean isTempDeleted;

	@Column(name = "STAR_FLAG")
	private Boolean isStar;

	@Column(name = "FINAL_FLAG")
	private Boolean isFinal;

	@Column(name = "CODING")
	private String coding;

	@Column(name = "ZONE_CODE")
	private String zoneCode;

	@Column(name = "IS_LOCKED")
	private Boolean isLocked;

	@Column(name = "ROW_LEVEL_ID")
	private Long rowLevelId;

	@Column(name = "USER_TAG")
	private String userTag;

	@Column(name = "GROUP_TAG")
	private String groupTag;

	@Column(name = "SYSTEM_TAG")
	private String systemTag;

	@Column(name = "PERMISSION_FLAG")
	private Integer permissionFlag;

	@Column(name = "IS_SECURE")
	private Boolean isSecure;

	@Column(name = "LEVEL_FLAG")
	private Integer levelFlag;

	@Column(name = "REQUEST_ID")
	private Long requestId;

	@Column(name = "IS_CONFIRMED")
	private Boolean isConfirmed;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "FK_PV_STEREO_TYPE_ID")
	private Long pvStereoTypeId;

	@Column(name = "FK_SYS_SCOPE_ID")
	private Long sysScope;


	@PrePersist
	public void setCreated(){
		this.setCreatedById(SecurityUtility.getAuthenticatedUser().getUserId());
		this.setCreatedDate(LocalDateTime.now());
		this.setIp("0.0.0.0");
		this.setZoneCode(SecurityUtility.getUserZoneCode()); //<-----Uncomment if required!
		this.setLevelFlag(SecurityUtility.getUserLevelFlag());
		this.setPermissionFlag(SecurityUtility.getUserPermissionFlag());
		this.setIsSecure(true);
		this.setIsDeleted(false);
		this.setIsTempDeleted(false);
		this.setVersion(1L);
	}

	@PreUpdate
	public void setUpdate(){
		this.setUpdatedById(SecurityUtility.getAuthenticatedUser().getUserId());
		this.setUpdatedDate(LocalDateTime.now());
		this.setZoneCode(SecurityUtility.getUserZoneCode()); //<-----Uncomment if required!
		this.setLevelFlag(SecurityUtility.getUserLevelFlag());
		this.setPermissionFlag(SecurityUtility.getUserPermissionFlag());
		this.setIp("0.0.0.0");
	}
	@PreRemove
	public void setDeleted(){
		this.setDeletedById(SecurityUtility.getAuthenticatedUser().getUserId());
		this.setDeletedDate(LocalDateTime.now());
	}

	User getDummyUser(){
		User u=new User();
		u.setId(1L);
		return u;
	}

	public Long getId() {
        return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
        return ip;
    }

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedById() {
		return createdById;
	}

	public void setCreatedById(Long createdById) {
		this.createdById = createdById;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}

	public Long getUpdatedById() {
		return updatedById;
	}

	public void setUpdatedById(Long updatedById) {
		this.updatedById = updatedById;
	}

	public Long getDeletedById() {
		return deletedById;
	}

	public void setDeletedById(Long deletedById) {
		this.deletedById = deletedById;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean deleted) {
		this.isDeleted = deleted;
	}

	public Boolean getIsTempDeleted() {
		return isTempDeleted;
	}

	public void setIsTempDeleted(Boolean tempDeleteFlag) {
		this.isTempDeleted = tempDeleteFlag;
	}

	public Boolean getIsStar() {
		return isStar;
	}

	public void setIsStar(Boolean starFlag) {
		this.isStar = starFlag;
	}

	public Boolean getIsFinal() {
		return isFinal;
	}

	public void setIsFinal(Boolean finalFlag) {
		this.isFinal = finalFlag;
	}

	public String getCoding() {
		return coding;
	}

	public void setCoding(String coding) {
		this.coding = coding;
	}

	public String getZoneCode() {
		return zoneCode;
	}

	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}

	public Boolean getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Boolean locked) {
		this.isLocked = locked;
	}

	public Long getRowLevelId() {
		return rowLevelId;
	}

	public void setRowLevelId(Long rowLevelId) {
		this.rowLevelId = rowLevelId;
	}

	public Long getSysScope() {
		return sysScope;
	}

	public void setSysScope(Long sysScope) {
		this.sysScope = sysScope;
	}

	public String getUserTag() {
		return userTag;
	}

	public void setUserTag(String userTag) {
		this.userTag = userTag;
	}

	public String getGroupTag() {
		return groupTag;
	}

	public void setGroupTag(String groupTag) {
		this.groupTag = groupTag;
	}

	public String getSystemTag() {
		return systemTag;
	}

	public void setSystemTag(String systemTag) {
		this.systemTag = systemTag;
	}

	public Integer getPermissionFlag() {
		return permissionFlag;
	}

	public void setPermissionFlag(Integer permissionFlag) {
		this.permissionFlag = permissionFlag;
	}

	public Boolean getIsSecure() {
		return isSecure;
	}

	public void setIsSecure(Boolean isSecure) {
		this.isSecure = isSecure;
	}

	public Integer getLevelFlag() {
		return levelFlag;
	}

	public void setLevelFlag(Integer levelFlag) {
		this.levelFlag = levelFlag;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public Boolean getIsConfirmed() {
		return isConfirmed;
	}

	public void setIsConfirmed(Boolean confirmed) {
		this.isConfirmed = confirmed;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public Long getPvStereoTypeId() {
        return pvStereoTypeId;
    }

    public void setPvStereoTypeId(Long pvStereoTypeId) {
        this.pvStereoTypeId = pvStereoTypeId;
    }

	//EQUALS and HASHCODE
	@Override
	public boolean equals(Object o) {
		if(o==null) return false;
		if (this == o) return true;
		if (!(o instanceof BaseEntity)) return false;
		BaseEntity that = (BaseEntity) o;
		boolean x=getId()!=null&& getId().equals(that.getId());
		return x && getId()!=null && getId()!=0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}
}
