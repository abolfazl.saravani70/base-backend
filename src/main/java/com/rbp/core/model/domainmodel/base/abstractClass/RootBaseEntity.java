package com.rbp.core.model.domainmodel.base.abstractClass;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class RootBaseEntity <selfType extends BaseEntity, parentType extends BaseEntity> extends TreeBaseEntity<selfType>{

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ROOT_ID")
//    private parentType root;

    public abstract parentType getRoot();

    public abstract void setRoot(parentType root);

 }
