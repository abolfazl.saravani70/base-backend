package com.rbp.core.model.domainmodel.basicInformation.geographical;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "GEO$Location")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Location",allocationSize = 1)
public class Location extends BaseEntity {

    private static final long serialVersionUID = 2344765800317649710L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "Type")
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
