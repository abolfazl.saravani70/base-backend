/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.gridStructure.filter;

public class TextColumnFilter extends ColumnFilter {

    private String operator;
    private String type;
    private String filter;
    private TextFilter condition1;
    private TextFilter condition2;

    public String getOperator() {
        return operator;
    }

    public String getType() {
        return type;
    }

    public String getFilter() {
        return filter;
    }

    public TextFilter getCondition1() {
        return condition1;
    }

    public TextFilter getCondition2() {
        return condition2;
    }
}
