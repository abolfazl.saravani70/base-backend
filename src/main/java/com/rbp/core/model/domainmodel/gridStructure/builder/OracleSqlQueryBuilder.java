/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.gridStructure.builder;

import com.google.common.collect.Sets;
import com.rbp.core.model.domainmodel.gridStructure.filter.ColumnFilter;
import com.rbp.core.model.domainmodel.gridStructure.filter.NumberColumnFilter;
import com.rbp.core.model.domainmodel.gridStructure.filter.SetColumnFilter;
import com.rbp.core.model.domainmodel.gridStructure.filter.TextColumnFilter;
import com.rbp.core.model.domainmodel.gridStructure.request.ColumnVO;
import com.rbp.core.model.domainmodel.gridStructure.request.EnterpriseGetRowsRequest;
import com.rbp.core.model.domainmodel.gridStructure.request.SortModel;
import org.apache.commons.lang3.tuple.Pair;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.google.common.collect.Streams.zip;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.stream.Collectors.*;
import static java.util.stream.Stream.concat;

/**
 * Builds Oracle SQL queries from an EnterpriseGetRowsRequest.
 */
public class OracleSqlQueryBuilder {

    private List<String> columns;
    private String innerJoins;
    private List<String> groupKeys;
    private List<String> rowGroups;
    private List<String> rowGroupsToInclude;
    private boolean isGrouping;
    private List<ColumnVO> valueColumns;
    private List<ColumnVO> pivotColumns;
    private Map<String, ColumnFilter> filterModel;
    private List<SortModel> sortModel;
    private int startRow, endRow;
    private List<ColumnVO> rowGroupCols;
    private Map<String, List<String>> pivotValues;
    private boolean isPivotMode;
    private Map<String, String> mapColumns; // todo: remove this line
    private Map<String, String> mapColumnsViewToTable;
    private Map<String, String> mapColumnsTableToView;
    private Class clazz;

    public String createSql(EnterpriseGetRowsRequest request, String tableName, Class<?> cls, Map<String, List<String>> pivotValues) {
        this.clazz = cls;
        getMapColumnsBiDir(this.clazz);
        this.valueColumns = getColumnVOList(request.getValueCols());
        this.pivotColumns = getColumnVOList(request.getPivotCols());
        this.groupKeys = request.getGroupKeys(); //getStringColumnNameList(request.getGroupKeys());
        this.rowGroupCols = getColumnVOList(request.getRowGroupCols());
        this.pivotValues = pivotValues;
        this.isPivotMode = request.isPivotMode();
        this.rowGroups = getRowGroups();
        this.rowGroupsToInclude = getRowGroupsToInclude();
        this.isGrouping = rowGroups.size() > groupKeys.size();
        this.filterModel = request.getFilterModel();
        this.sortModel = getMappedSortColumns(request.getSortModel());
        this.startRow = request.getStartRow();
        this.endRow = request.getEndRow();
        this.innerJoins = "";
        this.columns = request.getColumns();
        this.mapColumns =  new HashMap<>();



        String result = selectSql(tableName) + fromSql(tableName)+whereSql()
                + groupBySql() + orderBySql() + limitSql();
        return result;
    }

    private String selectSql(String mainTableName) {
        List<String> selectCols;
        if (isPivotMode && !pivotColumns.isEmpty()) {
            selectCols = concat(rowGroupsToInclude.stream(), extractPivotStatements()).collect(toList());
        } else {
            Stream<String> valueCols = valueColumns.stream()
                    .map(valueCol -> valueCol.getAggFunc() + '(' + valueCol.getField() + ") as " + valueCol.getField());

            selectCols = concat(rowGroupsToInclude.stream(), valueCols).collect(toList());
        }

        String result = isGrouping ? "SELECT " + join(", ", selectCols) : "SELECT * ";// + parseColumns(mainTableName);
        return result;
    }

    private String fromSql(String... tableName) {
        return format(" FROM %s", tableName) + this.innerJoins;

    }

    private String whereSql() {
        String whereFilters =
                concat(getGroupColumns(), getFilters())
                        .collect(joining(" AND "));

        String query = whereFilters.isEmpty() ? "" : format(" WHERE " +
                //"(" +
                "%s", whereFilters);
        // TODO: 11/16/2019  uncomment above and below for secure fetch data
//        if(query.contains("WHERE")){
//            query += ") AND " + secureConditions(false);
//        } else {
//            query += " WHERE " + secureConditions(false);
//        }
        return query;
    }

    private String secureConditions(Boolean tempDeleted) {
        String securityFilter;
        int tempDeletedDigit=tempDeleted? 1:0;
        // TODO: 11/5/2019 Remove const values
        Integer userLevelFlag = 3; //SecurityUtility.getUserLevelFlag();
        Integer userPermissionFlag = 3;//SecurityUtility.getUserPermissionFlag();
        String userZoneCode = "001.002.003";// SecurityUtility.getUserZoneCode();
        String subZoneCode = userZoneCode.substring(0, userZoneCode.lastIndexOf("."));
            securityFilter = format("(( IS_DELETED = 0 AND IS_TEMP_DELETED = %1$d" +
                            " AND IS_SECURE = 0 ) OR ( IS_DELETED = 0" +
                            " AND IS_TEMP_DELETED = %1$d" +
                            " AND IS_SECURE = 1 AND ZONE_CODE like '%2$s%%'" +
                            " AND LEVEL_FLAG <= %3$d AND PERMISSION_FLAG <= %4$d))",
                    tempDeletedDigit, subZoneCode, userLevelFlag, userPermissionFlag);
        return securityFilter;
    }

    private String groupBySql() {
        return isGrouping ? " GROUP BY " + join(", ", rowGroupsToInclude) : "";
    }

    private String orderBySql() {
        Function<SortModel, String> orderByMapper = model -> "\"" + model.getColId() + "\"" + " " + model.getSort();

        boolean isDoingGrouping = rowGroups.size() > groupKeys.size();
        int num = isDoingGrouping ? groupKeys.size() + 1 : MAX_VALUE;

        List<String> orderByCols = sortModel.stream()
                .filter(model -> !isDoingGrouping || rowGroups.contains(model.getColId()))
                .map(orderByMapper)
                .limit(num)
                .collect(toList());

        return orderByCols.isEmpty() ? "" : " ORDER BY " + join(",", orderByCols);
    }

    private String limitSql() {
        return " OFFSET " + startRow + " ROWS FETCH NEXT " + (endRow - startRow + 1) + " ROWS ONLY";
    }

    private Stream<String> getFilters() {
        Function<Map.Entry<String, ColumnFilter>, String> applyFilters = entry -> {
            String columnName = this.mapColumnsViewToTable.get(entry.getKey());

            ColumnFilter filter = entry.getValue();

            if (filter instanceof SetColumnFilter) {
                return setFilter().apply(columnName, (SetColumnFilter) filter);
            }

            if (filter instanceof NumberColumnFilter) {
                return numberFilter().apply(columnName, (NumberColumnFilter) filter);
            }

            if (filter instanceof TextColumnFilter) {
                return textFilter().apply(columnName, (TextColumnFilter) filter);
            }

            return "";
        };

        return filterModel.entrySet().stream().map(applyFilters);
    }

    private BiFunction<String, SetColumnFilter, String> setFilter() {
        return (String columnName, SetColumnFilter filter) ->
                columnName + (filter.getValues().isEmpty() ? " IN ('') " : " IN " + asString(filter.getValues()));
    }

    private BiFunction<String, NumberColumnFilter, String> numberFilter() {
        return (String columnName, NumberColumnFilter filter) -> {
            String query = "";
            Integer filterValue = 0;
            String filterType = "";
            String operator = "";

            if (filter.getCondition1() == null) {
                filterValue = filter.getFilter();
                filterType = filter.getType();
                operator = operatorMap.get(filterType);

                query = columnName + (filterType.equals("inRange") ?
                        " BETWEEN " + filterValue + " AND " + filter.getFilterTo() : " " + operator + " " + filterValue);
            } else {
                // First condition
                filterValue = filter.getCondition1().getFilter();
                filterType = filter.getCondition1().getType();
                operator = operatorMap.get(filterType);

                query = columnName + (filterType.equals("inRange") ?
                        " BETWEEN " + filterValue + " AND " + filter.getCondition1().getFilterTo() : " " + operator + " " + filterValue);

                // Second condition
                filterValue = filter.getCondition2().getFilter();
                filterType = filter.getCondition2().getType();
                operator = operatorMap.get(filterType);

                query += " " + filter.getOperator() + " ";
                query += columnName + (filterType.equals("inRange") ?
                        " BETWEEN " + filterValue + " AND " + filter.getCondition2().getFilterTo() : " " + operator + " " + filterValue);
            }
            return query;

        };
    }

    private BiFunction<String, TextColumnFilter, String> textFilter() {
        return (String columnName, TextColumnFilter filter) -> {
            String query = "";

            if (filter.getCondition1() == null) {
                query = columnName + getTextFilter(filter.getType(), filter.getFilter());
            } else {
                query = columnName + getTextFilter(filter.getCondition1().getType(), filter.getCondition1().getFilter());
                query += " " + filter.getOperator() + " ";
                query += columnName + getTextFilter(filter.getCondition2().getType(), filter.getCondition2().getFilter());
                return query;
            }
            return query;
        };
    }

    private Stream<String> extractPivotStatements() {

        // create pairs of pivot col and pivot value i.e. (DEALTYPE,Financial), (BIDTYPE,Sell)...
        List<Set<Pair<String, String>>> pivotPairs = pivotValues.entrySet().stream()
                .map(e -> e.getValue().stream()
                        .map(pivotValue -> Pair.of(e.getKey(), pivotValue))
                        .collect(toCollection(LinkedHashSet::new)))
                .collect(toList());

        // create a cartesian product of decode statements for all pivot and value columns combinations
        // i.e. sum(DECODE(DEALTYPE, 'Financial', DECODE(BIDTYPE, 'Sell', CURRENTVALUE)))
        return Sets.cartesianProduct(pivotPairs)
                .stream()
                .flatMap(pairs -> {
                    String pivotColStr = pairs.stream()
                            .map(Pair::getRight)
                            .collect(joining("_"));

                    String decodeStr = pairs.stream()
                            .map(pair -> "DECODE(" + pair.getLeft() + ", '" + pair.getRight() + "'")
                            .collect(joining(", "));

                    String closingBrackets = IntStream
                            .range(0, pairs.size() + 1)
                            .mapToObj(i -> ")")
                            .collect(joining(""));

                    return valueColumns.stream()
                            .map(valueCol -> valueCol.getAggFunc() + "(" + decodeStr + ", " + valueCol.getField() +
                                    closingBrackets + " \"" + pivotColStr + "_" + valueCol.getField() + "\"");
                });
    }

    private List<String> getRowGroupsToInclude() {
        return rowGroups.stream()
                .limit(groupKeys.size() + 1)
                .collect(toList());
    }

    private Stream<String> getGroupColumns() {
        return zip(groupKeys.stream(), rowGroups.stream(), (key, group) -> group + " = '" + key + "'");
    }

    private List<String> getRowGroups() {
        return rowGroupCols.stream()
                .map(ColumnVO::getField)
                .collect(toList());
    }

    private String asString(List<String> l) {
        return "(" + l.stream().map(s -> "\'" + s + "\'").collect(joining(", ")) + ")";
    }

    private Map<String, String> operatorMap = new HashMap<String, String>() {{
        put("equals", "=");
        put("notEqual", "<>");
        put("lessThan", "<");
        put("lessThanOrEqual", "<=");
        put("greaterThan", ">");
        put("greaterThanOrEqual", ">=");
        put("is", "is");
    }};

    private String getTextFilter(String filterType, String filterValue) {
        if (filterType == null || filterType.equals("")) {
            filterType = "contains";
        }

        switch (filterType) {
            case "equals":
                return " = '" + filterValue + "'";
            case "notEqual":
                return " != '" + filterValue + "'";
            case "contains":
                return " like '%" + filterValue + "%'";
            case "notContains":
                return " not like '%" + filterValue + "%'";
            case "startsWith":
                return " like '" + filterValue + "%'";
            case "endsWith":
                return " like '%" + filterValue + "'";
            case "is":
                return  " is NUll";
            default:
                return "";
        }
    }

    private List<ColumnVO> getColumnVOList(List<ColumnVO> columnList) {
        List<ColumnVO> r = columnList.stream().map(
                col -> {
                    col.setField(this.mapColumnsViewToTable.get(col.getField()));
                    return col;
                }
        ).collect(Collectors.toList());
        return r;
    }

    private List<String> getStringColumnNameList(List<String> columnList) {
        List<String> r = columnList.stream().map(
                col -> {
                    String a = this.mapColumnsViewToTable.get(col);
                    return a;
                }
        ).collect(Collectors.toList());
        return r;
    }

    private List<SortModel> getMappedSortColumns(List<SortModel> columnList) {
        List<SortModel> r = columnList.stream().map(
                col -> {
                    col.setColId(this.mapColumnsViewToTable.get(col.getColId()));
                    return col;
                }
        ).collect(Collectors.toList());
        return r;
    }


    public List<Map<String, Object>> mapColumnToFieldRecords(List<Map<String, Object>> rows) {
        //Map<String, String> fields = getMapColumnToFieldsOfClass(this.clazz,new TableToView());

        List<Map<String, Object>> newRows=new ArrayList<>();
        for(Map<String,Object> row : rows){
            Map<String,Object> newRow=new HashMap<>();
            for(Map.Entry<String,Object> property: row.entrySet()){
                String key = property.getKey();
                String newKey = this.mapColumnsTableToView.get(key);
                if( newKey != null){
                    newRow.put(this.mapColumnsTableToView.get(key),row.get(key));
                }
            }
            newRows.add(newRow);
        }
        return newRows;
    }

    private void getMapColumnsBiDir(Class<?> cls) {

        this.mapColumnsTableToView = new HashMap<>();
        this.mapColumnsViewToTable = new HashMap<>();

        do {
            Field[] fields = cls.getDeclaredFields();
            for(Field field: fields){
                if(field.getAnnotation(Column.class) != null){
                    this.mapColumnsViewToTable.put(field.getName(), field.getAnnotation(Column.class).name());
                    this.mapColumnsTableToView.put(field.getAnnotation(Column.class).name(), field.getName());
                }
            }
            cls = cls.getSuperclass();
        } while (cls != Object.class);
    }

    private Map<String, String> getMapColumnToFieldsOfClass(Class<?> cls,TableViewOperator op) {
        Map<String, String> fields = new HashMap<>();
        do {
            fields.putAll(Arrays.stream(cls.getDeclaredFields()).
                    filter(x -> x.getAnnotation(Column.class) != null).collect(
                    Collectors.toMap(op::operation1,op::operation2)));
            cls = cls.getSuperclass();
        } while (cls != Object.class);

        return fields;
    }



    interface TableViewOperator{
        String operation1(Field f);
        String operation2(Field f);
    }
    class TableToView implements TableViewOperator{

        @Override
        public String operation1(Field f) {
            return f.getAnnotation(Column.class).name() ;
        }

        @Override
        public String operation2(Field f) {
            return f.getName();
        }
    }

    class ViewToTable implements TableViewOperator{

        @Override
        public String operation1(Field f) {
            return f.getName();
        }

        @Override
        public String operation2(Field f) {
            return f.getAnnotation(Column.class).name();
        }
    }

    private String parseColumns(String mainTableName) {
        String result = mainTableName + ".ID AS \"id\"";
        Field[] fields = null;
        Field field = null;
        Class<?> innerClass = null;
        String colName = "";
        String fullColName = "";
        String innerTableName = "";
        String innerFieldName = "";


        for (String col : columns) {
            fullColName = "";
            try {
                if (col.contains("->")) {
                    String[] fieldsStr = col.split("->");
                    field = getFieldInClass(this.clazz, fieldsStr[0], false);
                    if (field != null) {
                        colName = field.getAnnotation(JoinColumn.class).name();
                        innerClass = field.getType();
                        field = innerClass.getDeclaredField(fieldsStr[1]);
                        innerFieldName = field.getAnnotation(Column.class).name();
                        innerTableName = innerClass.getAnnotation(Table.class).name();
                        this.innerJoins += " LEFT OUTER JOIN " + innerTableName + " ON " + mainTableName + "." + colName + "=" + innerTableName + ".ID";
                        fullColName = innerTableName + "." + innerFieldName + " AS \"" + col + "\"";
                        this.mapColumns.put(col, innerTableName + "." + innerFieldName);
                    } else
                        fullColName = "";
                } else {
                    field = getFieldInClass(this.clazz, col, false);
                    if (field != null) {
                        colName = field.getAnnotation(Column.class).name();
                        fullColName = mainTableName + "." + colName + " AS \"" + col + "\"";
                        this.mapColumns.put(col, mainTableName + "." + colName);
                    } else
                        fullColName = "";

                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

            if (!fullColName.equals(""))
                result += ", " + fullColName;
        }

        //result += ")";
//        result += "AND ()";
        return result;
    }

    private Field getFieldInClass(Class<?> cls, String colName, Boolean getFieldOfParentClass) {

        Field[] fields = null;
        if (getFieldOfParentClass == false) {
            fields = cls.getDeclaredFields();
        } else {
            cls = cls.getSuperclass();
            if (cls != Object.class)
                fields = cls.getDeclaredFields();
            else
                return null;
        }

        fields = Arrays.stream(fields)
                .filter(x -> x.getName().equals(colName))
                .toArray(Field[]::new);

        if (fields.length > 0) {
            return fields[0];
        } else {
            return getFieldInClass(cls, colName, true);
        }
    }
}