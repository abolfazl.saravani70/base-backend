/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.base.abstractClass.junction;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.sayban.model.domainmodel.base.MonthsPercentage;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class OrganizationUnitJunctionBaseEntity extends EvalStateBaseEntity {

    private static final long serialVersionUID = 8681778170096976585L;

    @Column(name = "ASSIGN_DATE")
    private LocalDate assignDate;

    @Column(name = "DUE_DATE")
    private LocalDate dueDate;

    @Column(name = "FK_PV_STATE_ID")
    private Long pvStateId;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long orgUnitId;

    @Embedded
    private MonthsPercentage monthsPercentage;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_ID")
//    @OnDelete(action= OnDeleteAction.CASCADE)
//    private OrganizationUnit organizationUnit;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_STACKHOLDER_ID")
//    private OrganizationUnit stackholder;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_ASSIGNER_ID")
//    private OrganizationUnit assigner;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_EXECUTOR_ID")
//    private OrganizationUnit executor;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_SUPERVISOR_ID")
//    private OrganizationUnit supervisor;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public LocalDate getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(LocalDate assignDate) {
        this.assignDate = assignDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getPvStateId() {
        return pvStateId;
    }

    public void setPvStateId(Long pvStateId) {
        this.pvStateId = pvStateId;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

//    public OrganizationUnit getOrganizationUnit() {
//        return organizationUnit;
//    }
//
//    public void setOrganizationUnit(OrganizationUnit organizationUnit) {
//        this.organizationUnit = organizationUnit;
//    }

    public MonthsPercentage getMonthsPercentage() {
        return monthsPercentage;
    }

    public void setMonthsPercentage(MonthsPercentage monthsPercentage) {
        this.monthsPercentage = monthsPercentage;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }
}
