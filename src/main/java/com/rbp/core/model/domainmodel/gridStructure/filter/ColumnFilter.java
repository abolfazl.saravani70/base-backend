/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.gridStructure.filter;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "filterType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = NumberColumnFilter.class, name = "number"),
        @JsonSubTypes.Type(value = SetColumnFilter.class, name = "set"),
        @JsonSubTypes.Type(value = TextColumnFilter.class, name = "text")})
public abstract class ColumnFilter {
    String filterType;
}