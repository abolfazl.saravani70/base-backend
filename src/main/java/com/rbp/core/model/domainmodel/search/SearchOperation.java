/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.search;

import java.util.regex.Pattern;

public enum SearchOperation {
    EQUAL, NOT_EQUAL, GREATER_THAN, GREATER_THAN_OR_EQUAL, LESS_THAN, LESS_THAN_OR_EQUAL, IN, NOT_IN;

    public static final String OR_OPERATOR = "OR";
    public static final String AND_OPERATOR = "AND";
    public static final String LOWER_CASE_OR_OPERATOR = "or";
    public static final String LOWER_CASE_AND_OPERATOR = "and";
    public static final String LEFT_PARENTHESES = "(";
    public static final String RIGHT_PARENTHESES = ")";
    public static final String TRUE = "TRUE";
    public static final String TRUE_LOWER_CASE = "true";
    public static final String FALSE = "FALSE";
    public static final String FALSE_LOWER_CASE = "false";
    public static final String PERCENTAGE = "%";
    public static final String STAR = "*";
    public static final String BLANK = "";
    public final static Pattern SPEC_CRITERIA_REGEX = Pattern.compile("^(.+?)(=out=|=in=|=|<=|>=|<|>|!=)(.+?)$"
            , Pattern.UNICODE_CHARACTER_CLASS);
    public final static Pattern SPEC_GROUP_REGEX = Pattern.compile("^(s|a|c|m|x)\\((.+?)\\)$"
            , Pattern.UNICODE_CHARACTER_CLASS);
    public final static String SPLIT_FOR_GROUP_BY_FUNCTION="@";
    public final static String SPLIT_FOR_OPERATOR = "\\$";
    public final static String SPLIT_FOR_COLUMN = ",";
    public final static String SPLIT_FOR_DATE = "#";
    public final static String SPLIT_FOR_DESCENDING = "^";
    public final static String SPLIT_FOR_PATH = "[.]";
    public final static String SPLIT_FOR_FORMAT_DATE = "-";
    public final static String FORMAT_OF_YEAR = "YYYY";
    public final static String FORMAT_OF_MONTH = "MM";
    public final static String FORMAT_OF_DAY = "DD";
    public final static String FORMAT_OF_HOUR = "hh";
    public final static String FORMAT_OF_MINUTE = "mm";
    public final static String FORMAT_OF_SECOND = "ss";
    public final static String YEAR = "YEAR";
    public final static String MONTH = "MONTH";
    public final static String DAY = "DAY";
    public final static String HOUR = "HOUR";
    public final static String MINUTE = "MINUTE";
    public final static String SECOND = "SECOND";
    public final static String FORMAT_OF_DATE_CLASS="yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_YEAR_MONTH = "YYYY-MM";
    public final static String FORMAT_YEAR_MONTH_DAY = "YYYY-MM-DD";
    public final static String FORMAT_YEAR_MONTH_DAY_HOUR = "YYYY-MM-DD-hh";
    public final static String FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE = "YYYY-MM-DD-hh-mm";
    public final static String COUNT="c";
    public final static String SUM="s";
    public final static String AVG="a";
    public final static String MIN="m";
    public final static String MAX="x";

    public static SearchOperation getSimpleOperation(String input) {
        switch (input) {
            case "=":
                return EQUAL;
            case "!=":
                return NOT_EQUAL;
            case ">":
                return GREATER_THAN;
            case ">=":
                return GREATER_THAN_OR_EQUAL;
            case "<":
                return LESS_THAN;
            case "<=":
                return LESS_THAN_OR_EQUAL;
            case "=in=":
                return IN;
            case "=out=":
                return NOT_IN;
            default:
                return null;
        }
    }
}
