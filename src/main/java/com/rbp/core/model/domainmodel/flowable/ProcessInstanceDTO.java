package com.rbp.core.model.domainmodel.flowable;

import com.rbp.core.utility.CalendarTool;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

/**
 * A process DTO, it also has all the variables shared between all the process instances eg. initiator
 */
public class ProcessInstanceDTO implements Serializable {
    private static final long serialVersionUID = -1695510788097217462L;
    @Autowired
    CalendarTool calendarUtility;

    LocalDateTime startTime;
    String processDefinitionId;
    String processDefinitionKey;
    String processDefinitionName;
    String deploymentId;
    String processInstanceId;
    String businessKey;
    String id;
    boolean isSuspended;
    Map<String,Object> map;


    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = calendarUtility.convertToLocalDateTimeViaSqlTimestamp(startTime);
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSuspended() {
        return isSuspended;
    }

    public void setSuspended(boolean suspended) {
        isSuspended = suspended;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map=map;
    }
}
