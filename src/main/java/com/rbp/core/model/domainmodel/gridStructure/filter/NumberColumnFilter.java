/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.gridStructure.filter;

public class NumberColumnFilter extends ColumnFilter {

    private String operator;
    private String type;
    private Integer filter;
    private Integer filterTo;
    private NumberFilter condition1;
    private NumberFilter condition2;

    public NumberColumnFilter() {
    }

    public NumberColumnFilter(String type, Integer filter, Integer filterTo) {
        this.type = type;
        this.filter = filter;
        this.filterTo = filterTo;
    }

    public String getOperator() {
        return operator;
    }

    public String getFilterType() {
        return filterType;
    }

    public String getType() {
        return type;
    }

    public Integer getFilter() {
        return filter;
    }

    public Integer getFilterTo() {
        return filterTo;
    }

    public NumberFilter getCondition1() {
        return condition1;
    }

    public NumberFilter getCondition2() {
        return condition2;
    }
}
