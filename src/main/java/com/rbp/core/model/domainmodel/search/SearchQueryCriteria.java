/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.search;

import com.rbp.core.utility.ApplicationException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

import static com.rbp.core.model.domainmodel.search.SearchOperation.*;

public class SearchQueryCriteria {
    private Predicate predicate;
    private CriteriaBuilder builder;

    SearchQueryCriteria(Predicate predicate, CriteriaBuilder builder) {
        this.predicate = predicate;
        this.builder = builder;
    }

    Predicate build(Deque<?> postFixedExprStack) {
        Deque<Predicate> specStack = new LinkedList<>();
        while (!postFixedExprStack.isEmpty()) {
            Object mayBeOperand = postFixedExprStack.pollLast();
            if (!(mayBeOperand instanceof String)) {
                specStack.push(acceptSearch((SearchCriteria) mayBeOperand));
            } else {
                Predicate operand1 = specStack.pop();
                Predicate operand2 = specStack.pop();
                if (mayBeOperand.equals(AND_OPERATOR)) {
                    specStack.push(builder.and(operand2, operand1));
                } else if (mayBeOperand.equals(OR_OPERATOR)) {
                    specStack.push(builder.or(operand2, operand1));
                }
            }
        }
        return specStack.pop();
    }

    /*
    EQUAL: "field name=value"
    NOT_EQUAL: "field name!=value"
    GREATER_THAN: "field name>value"
    GREATER_THAN_OR_EQUAL: "field name>=value"
    LESS_THAN: "field name<value"
    LESS_THAN_OR_EQUAL: "field name<=value"
    IN: "field name=in=(value1,...,value n)"
    NOT_IN: "field name=out=(value1,...,value n)"
    if filed is LocalDateTime than value="2015-03-08T09:40:11.977#YYYY-MM-DD-hh-mm-ss"
    and if filed is Timestamp than value="2015-03-08 9:40:11.977#YYYY-MM-DD-hh-mm-ss"
    YYYY=year , MM=month , DD=day , hh=hour , mm=minute , ss=second
    if value in filed name has and /or then value=($value1$and$value2$)$or$value3

     */
    private Predicate acceptSearch(SearchCriteria param) {
        List<Object> args = castArguments(param);
        Object argument = args.get(0);
        final String formatString = argument != null ? argument.toString().replace(STAR, PERCENTAGE).toUpperCase() : "";
        switch (param.getOperation()) {
            case EQUAL: {
                if (argument instanceof String) {
                    return builder.like(builder.upper(param.getKey())
                            , formatString);
                } else if (argument == null) {
                    return builder.isNull(param.getKey());
                } else if (argument instanceof LocalDateTime) {
                    return getDatePredicate(param, (LocalDateTime) argument);
                } else if (argument instanceof LocalDate) {
                    LocalDateTime converter = localDateToLocalDateTime((LocalDate) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Date) {
                    LocalDateTime converter = dateToLocalDateTime((Date) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof LocalTime) {
                    LocalDateTime converter = localTimeToLocalDateTime((LocalTime) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Timestamp) {
                    LocalDateTime converter = timeStampToLocalDateTime((Timestamp) argument);
                    return getDatePredicate(param, converter);
                } else {
                    return builder.equal(param.getKey(), argument);
                }
            }

            case NOT_EQUAL: {
                if (argument instanceof String) {
                    return builder.notLike(builder.upper(param.getKey()), formatString);
                } else if (argument == null) {
                    return builder.isNotNull(param.getKey());
                } else if (argument instanceof LocalDateTime) {
                    return getDatePredicate(param, (LocalDateTime) argument);
                } else if (argument instanceof LocalDate) {
                    LocalDateTime converter = localDateToLocalDateTime((LocalDate) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Date) {
                    LocalDateTime converter = dateToLocalDateTime((Date) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof LocalTime) {
                    LocalDateTime converter = localTimeToLocalDateTime((LocalTime) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Timestamp) {
                    LocalDateTime converter = timeStampToLocalDateTime((Timestamp) argument);
                    return getDatePredicate(param, converter);
                }  else {
                    return builder.notEqual(param.getKey(), argument);
                }
            }

            case GREATER_THAN: {
                if (argument instanceof LocalDateTime) {
                    return getDatePredicate(param, (LocalDateTime) argument);
                } else if (argument instanceof LocalDate) {
                    LocalDateTime converter = localDateToLocalDateTime((LocalDate) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Date) {
                    LocalDateTime converter = dateToLocalDateTime((Date) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof LocalTime) {
                    LocalDateTime converter = localTimeToLocalDateTime((LocalTime) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Timestamp) {
                    LocalDateTime converter = timeStampToLocalDateTime((Timestamp) argument);
                    return getDatePredicate(param, converter);
                }  else
                    return builder.greaterThan(param.getKey(), argument.toString());
            }

            case GREATER_THAN_OR_EQUAL:
                if (argument instanceof LocalDateTime) {
                    return getDatePredicate(param, (LocalDateTime) argument);
                } else if (argument instanceof LocalDate) {
                    LocalDateTime converter = localDateToLocalDateTime((LocalDate) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Date) {
                    LocalDateTime converter = dateToLocalDateTime((Date) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof LocalTime) {
                    LocalDateTime converter = localTimeToLocalDateTime((LocalTime) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Timestamp) {
                    LocalDateTime converter = timeStampToLocalDateTime((Timestamp) argument);
                    return getDatePredicate(param, converter);
                }  else
                    return builder.greaterThanOrEqualTo(param.getKey(), argument.toString());

            case LESS_THAN: {
                if (argument instanceof LocalDateTime) {
                    return getDatePredicate(param, (LocalDateTime) argument);
                } else if (argument instanceof LocalDate) {
                    LocalDateTime converter = localDateToLocalDateTime((LocalDate) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Date) {
                    LocalDateTime converter = dateToLocalDateTime((Date) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof LocalTime) {
                    LocalDateTime converter = localTimeToLocalDateTime((LocalTime) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Timestamp) {
                    LocalDateTime converter = timeStampToLocalDateTime((Timestamp) argument);
                    return getDatePredicate(param, converter);
                }  else
                    return builder.lessThan(param.getKey(), argument.toString());
            }

            case LESS_THAN_OR_EQUAL:
                if (argument instanceof LocalDateTime) {
                    return getDatePredicate(param, (LocalDateTime) argument);
                } else if (argument instanceof LocalDate) {
                    LocalDateTime converter = localDateToLocalDateTime((LocalDate) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Date) {
                    LocalDateTime converter = dateToLocalDateTime((Date) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof LocalTime) {
                    LocalDateTime converter = localTimeToLocalDateTime((LocalTime) argument);
                    return getDatePredicate(param, converter);
                }else if (argument instanceof Timestamp) {
                    LocalDateTime converter = timeStampToLocalDateTime((Timestamp) argument);
                    return getDatePredicate(param, converter);
                }  else
                    return builder.lessThanOrEqualTo(param.getKey(), argument.toString());
            case IN: {
                return param.getKey().in(args);
            }
            case NOT_IN: {
                return builder.not(param.getKey().in(args));
            }

        }
        return null;
    }

    Expression getComponentsOfDate(SearchCriteria param, String name) {
        return builder.function(name, Integer.class, param.getKey());
    }

    private LocalDateTime localDateToLocalDateTime(LocalDate localDate) {
        return localDate.atStartOfDay();
    }
    private LocalDateTime dateToLocalDateTime(Date date) {

        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
    private LocalDateTime timeStampToLocalDateTime(Timestamp timestamp) {

        return timestamp.toLocalDateTime();
    }

    private LocalDateTime localTimeToLocalDateTime(LocalTime localTime) {
        return localTime.atDate(LocalDate.now());
    }
    private Predicate comparisonOfDate(SearchCriteria param
            , LocalDateTime localDateTime
            , Integer date, String pattern, String name) {
        localDateTime.with(TemporalAdjusters.lastDayOfMonth());
        Boolean bol = pattern.equals(FORMAT_OF_YEAR)
                || pattern.equals(FORMAT_OF_MONTH)
                || pattern.equals(FORMAT_OF_DAY)
                || pattern.equals(FORMAT_OF_HOUR)
                || pattern.equals(FORMAT_OF_MINUTE)
                || pattern.equals(FORMAT_OF_SECOND);
        switch (param.getOperation()) {
            case EQUAL:
            case NOT_EQUAL:
                return builder.equal(getComponentsOfDate(param, name), date);
            case GREATER_THAN: {
                if (bol)
                    return builder.greaterThan(getComponentsOfDate(param, name), date);
                else {
                    switch (pattern) {
                        case FORMAT_YEAR_MONTH:
                            return builder.greaterThan(param.<LocalDateTime>getKey(), firstDayOfNextMonth(localDateTime));

                        case FORMAT_YEAR_MONTH_DAY:
                            return builder.greaterThan(param.<LocalDateTime>getKey(), nextDay(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR:
                            return builder.greaterThan(param.<LocalDateTime>getKey(), nextHour(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE:
                            return builder.greaterThan(param.<LocalDateTime>getKey(), nextMinute(localDateTime));
                        default:
                            return builder.greaterThan(param.<LocalDateTime>getKey(), localDateTime);
                    }
                }
            }

            case GREATER_THAN_OR_EQUAL: {
                if (bol)
                    return builder.greaterThanOrEqualTo(getComponentsOfDate(param, name), date);
                else {
                    switch (pattern) {
                        case FORMAT_YEAR_MONTH:
                            return builder.greaterThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , firstDayOfMonth(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY:
                            return builder.greaterThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , firstDay(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR:
                            return builder.greaterThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , firstHour(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE:
                            return builder.greaterThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , firstMinute(localDateTime));
                        default:
                            return builder.greaterThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , localDateTime);
                    }
                }
            }

            case LESS_THAN: {
                if (bol)
                    return builder.lessThan(getComponentsOfDate(param, name), date);
                else {
                    switch (pattern) {
                        case FORMAT_YEAR_MONTH:
                            return builder.lessThan(param.<LocalDateTime>getKey()
                                    , lastDayOfBeforeMonth(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY:
                            return builder.lessThan(param.<LocalDateTime>getKey()
                                    , beforeDay(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR:
                            return builder.lessThan(param.<LocalDateTime>getKey()
                                    , beforeHour(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE:
                            return builder.lessThan(param.<LocalDateTime>getKey()
                                    , beforeMinute(localDateTime));
                        default:
                            return builder.lessThan(param.<LocalDateTime>getKey()
                                    , localDateTime);
                    }
                }
            }

            case LESS_THAN_OR_EQUAL: {
                if (bol)
                    return builder.lessThanOrEqualTo(getComponentsOfDate(param, name), date);
                else {
                    switch (pattern) {
                        case FORMAT_YEAR_MONTH:
                            return builder.lessThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , lastDayOfMonth(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY:
                            return builder.lessThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , lastDay(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR:
                            return builder.lessThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , lastHour(localDateTime));
                        case FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE:
                            return builder.lessThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , lastMinute(localDateTime));
                        default:
                            return builder.lessThanOrEqualTo(param.<LocalDateTime>getKey()
                                    , localDateTime);
                    }
                }
            }
        }
        return null;
    }

    private Predicate getDatePredicate(SearchCriteria param, LocalDateTime date) {

        String[] patt = ((String) param.getValue()).split(SPLIT_FOR_DATE);

        String[] patterns = patt[1].split(SPLIT_FOR_FORMAT_DATE);
        Deque<Predicate> timePredicates = new LinkedList<>();
        for (String pat : patterns) {
            switch (pat) {
                case FORMAT_OF_YEAR: {
                    if (param.getOperation().equals(NOT_EQUAL)
                            || param.getOperation().equals(EQUAL)
                            || patterns.length == 1) {
                        timePredicates.push(comparisonOfDate(param, date
                                , date.getYear(), patt[1], YEAR));
                    }
                }
                break;
                case FORMAT_OF_MONTH:
                    if (param.getOperation().equals(NOT_EQUAL)
                            || param.getOperation().equals(EQUAL)
                            || patterns.length == 1) {
                        timePredicates.push(comparisonOfDate(param, date
                                , date.getMonthValue(), patt[1], MONTH));
                    }
                    break;
                case FORMAT_OF_DAY:
                    if (param.getOperation().equals(NOT_EQUAL)
                            || param.getOperation().equals(EQUAL)
                            || patterns.length == 1) {
                        timePredicates.push(comparisonOfDate(param, date
                                , date.getDayOfMonth(), patt[1], DAY));
                    }
                    break;
                case FORMAT_OF_HOUR:
                    if (param.getOperation().equals(NOT_EQUAL)
                            || param.getOperation().equals(EQUAL)
                            || patterns.length == 1) {
                        timePredicates.push(comparisonOfDate(param, date
                                , date.getHour(), patt[1], HOUR));
                    }
                    break;
                case FORMAT_OF_MINUTE:
                    if (param.getOperation().equals(NOT_EQUAL)
                            || param.getOperation().equals(EQUAL)
                            || patterns.length == 1) {
                        timePredicates.push(comparisonOfDate(param, date
                                , date.getMinute(), patt[1], MONTH));
                    }
                    break;
                case FORMAT_OF_SECOND:
                    if (param.getOperation().equals(NOT_EQUAL)
                            || param.getOperation().equals(EQUAL)
                            || patterns.length == 1) {
                        timePredicates.push(comparisonOfDate(param, date
                                , date.getSecond(), patt[1], SECOND));
                    }
            }
        }

        while (timePredicates.size() > 1) {
            Predicate operand1 = timePredicates.pop();
            Predicate operand2 = timePredicates.pop();
            timePredicates.push(builder.and(operand1, operand2));
        }
        if (param.getOperation().equals(NOT_EQUAL)) {
            timePredicates.push(builder.not(timePredicates.pop()));
        } else if (!param.getOperation().equals(EQUAL) && patterns.length > 1) {
            timePredicates.push(comparisonOfDate(param, date, 0, patt[1], BLANK));
        }
        return timePredicates.pop();
    }

    private LocalDateTime lastDayOfBeforeMonth(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.minusMonths(1);
        LocalDateTime lastDay = temp.with(TemporalAdjusters.lastDayOfMonth());
        return LocalDateTime.of(lastDay.getYear(), lastDay.getMonth()
                , lastDay.getDayOfMonth(), 23, 59, 59);
    }

    private LocalDateTime lastDayOfMonth(LocalDateTime localDateTime) {
        LocalDateTime lastDay = localDateTime.with(TemporalAdjusters.lastDayOfMonth());
        return LocalDateTime.of(lastDay.getYear(), lastDay.getMonth()
                , lastDay.getDayOfMonth(), 23, 59, 59);
    }


    private LocalDateTime beforeDay(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.minusDays(1);
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), 23, 59, 59);
    }

    private LocalDateTime beforeHour(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.minusHours(1);
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), temp.getHour(), 59, 59);
    }

    private LocalDateTime beforeMinute(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.minusMonths(1);
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), temp.getHour(), temp.getMinute(), 59);
    }

    private LocalDateTime firstDayOfNextMonth(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.with(TemporalAdjusters.firstDayOfNextMonth());
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), 0, 0, 0);
    }

    private LocalDateTime lastDay(LocalDateTime localDateTime) {

        return LocalDateTime.of(localDateTime.getYear()
                , localDateTime.getMonth(), localDateTime.getDayOfMonth()
                , 23, 59, 59);
    }

    private LocalDateTime lastHour(LocalDateTime localDateTime) {

        return LocalDateTime.of(localDateTime.getYear()
                , localDateTime.getMonth(), localDateTime.getDayOfMonth()
                , localDateTime.getHour(), 59, 59);
    }

    private LocalDateTime lastMinute(LocalDateTime localDateTime) {

        return LocalDateTime.of(localDateTime.getYear()
                , localDateTime.getMonth(), localDateTime.getDayOfMonth()
                , localDateTime.getHour(), localDateTime.getMinute(), 59);
    }

    private LocalDateTime firstDayOfMonth(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.with(TemporalAdjusters.firstDayOfMonth());
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), 0, 0, 0);
    }

    private LocalDateTime firstDay(LocalDateTime localDateTime) {
        LocalDateTime temp = LocalDateTime.of(localDateTime.getYear()
                , localDateTime.getMonth(), localDateTime.getDayOfMonth()
                , 0, 0, 0);
        return temp;
    }

    private LocalDateTime firstHour(LocalDateTime localDateTime) {
        LocalDateTime temp = LocalDateTime.of(localDateTime.getYear()
                , localDateTime.getMonth(), localDateTime.getDayOfMonth()
                , localDateTime.getHour(), 0, 0);
        return temp;
    }

    private LocalDateTime firstMinute(LocalDateTime localDateTime) {
        LocalDateTime temp = LocalDateTime.of(localDateTime.getYear()
                , localDateTime.getMonth(), localDateTime.getDayOfMonth()
                , localDateTime.getHour(), localDateTime.getMinute(), 0);
        return temp;
    }

    private LocalDateTime nextDay(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.plusDays(1);
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), 0, 0);
    }

    private LocalDateTime nextHour(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.plusHours(1);
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), temp.getHour(), 0);
    }

    private LocalDateTime nextMinute(LocalDateTime localDateTime) {
        LocalDateTime temp = localDateTime.plusMinutes(1);
        return LocalDateTime.of(temp.getYear(), temp.getMonth()
                , temp.getDayOfMonth(), temp.getHour(), temp.getMinute(), 0);
    }


    public Predicate getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate predicate) {
        this.predicate = predicate;
    }

    public CriteriaBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(CriteriaBuilder builder) {
        this.builder = builder;
    }

    private List<Object> castArguments(SearchCriteria param) {
        String value = ((String) param.getValue());
        List<String> arguments = Arrays.asList(value
                .replace(LEFT_PARENTHESES, BLANK)
                .replace(RIGHT_PARENTHESES, BLANK)
                .split(SPLIT_FOR_COLUMN));
        Class<? extends Object> type = param.getKey().getJavaType();
        List<Object> args = arguments.stream().map(arg -> {
            if (!arg.equals("null")) {
                if (type.equals(Integer.class)) {
                    return Integer.parseInt(arg);
                } else if (type.equals(Long.class)) {
                    return Long.parseLong(arg);
                } else if (type.equals(Double.class)) {
                    return Double.parseDouble(arg);
                } else if (type.equals(Float.class)) {
                    return Float.parseFloat(arg);
                } else if (type.equals(Byte.class)) {
                    return Byte.parseByte(arg);
                } else if (type.equals(Short.class)) {
                    return Short.parseShort(arg);
                } else if (type.equals(Boolean.class)) {
                    if (param.getValue().equals(TRUE_LOWER_CASE)
                            || param.getValue().equals(TRUE)
                            || param.getValue().equals(FALSE_LOWER_CASE)
                            || param.getValue().equals(FALSE))
                        return Boolean.parseBoolean(arg);
                    else
                        // TODO: 7/16/2019 throw exception
                        throw new ApplicationException
                                (0, "داده از جنس Boolean است لطفاً مقادیر true و false وارد کنید");
                } else if (type.equals(BigDecimal.class)) {
                    return new BigDecimal(arg);
                } else if (type.equals(BigInteger.class)) {
                    return new BigInteger(arg);
                } else if (type.equals(LocalDateTime.class)) {
                    String[] patt = arg.split(SPLIT_FOR_DATE);
                    if (patt.length == 2) {
                        return LocalDateTime.parse(patt[0]);
                    } else
                        //TODO: 7/16/2019 throw exception
                        throw new ApplicationException(0, "الگوی صحیح تاریخ را وارد کنید");
                } else if (type.equals(LocalDate.class)) {
                    String[] patt = arg.split(SPLIT_FOR_DATE);
                    if (patt.length == 2) {
                        return LocalDate.parse(patt[0]);
                    } else
                        //TODO: 7/16/2019 throw exception
                        throw new ApplicationException(0, "الگوی صحیح تاریخ را وارد کنید");
                }else if (type.equals(Date.class)) {
                    String[] patt = arg.split(SPLIT_FOR_DATE);
                    if (patt.length == 2) {
                        try {
                            return new SimpleDateFormat(FORMAT_OF_DATE_CLASS).parse(patt[0]);
                        } catch (ParseException e) {
                            //e.printStackTrace();
                            //TODO: 7/16/2019 throw exception
                            throw new ApplicationException(0, "الگوی مورد نظر قابل تبدیل نیست.");
                        }

                    } else
                        //TODO: 7/16/2019 throw exception
                        throw new ApplicationException(0, "الگوی صحیح تاریخ را وارد کنید");
                }else if (type.equals(LocalTime.class)) {
                    String[] patt = arg.split(SPLIT_FOR_DATE);
                    if (patt.length == 2) {
                        return LocalTime.parse(patt[0]);
                    } else
                        //TODO: 7/16/2019 throw exception
                        throw new ApplicationException(0, "الگوی صحیح تاریخ را وارد کنید");
                }else if (type.equals(Timestamp.class)) {
                    String[] patt = arg.split(SPLIT_FOR_DATE);
                    if (patt.length == 2) {
                        return Timestamp.valueOf(patt[0]);
                    } else
                        //TODO: 7/16/2019 throw exception
                        throw new ApplicationException(0, "الگوی صحیح تاریخ را وارد کنید");
                }
                return arg;
            } else {
                return null;
            }
        }).collect(Collectors.toList());
        return args;
    }
}
