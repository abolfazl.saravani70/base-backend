package com.rbp.core.model.domainmodel.security;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "SEC$Permission")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Permission",allocationSize = 1)
public class Permission extends BaseEntity {

	private static final long serialVersionUID = -676914803401520725L;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "SRC")
	private String src;

	@Column(name = "ENGLISH_NAME")
	private String englishName;

	@Column(name = "PERMISSION_NAME")
	private String permissionName;

	@ManyToMany(mappedBy = "permissions",fetch = FetchType.LAZY,cascade = CascadeType.MERGE)
	//@OnDelete(action = OnDeleteAction.NO_ACTION)
	private Set<Group> groups;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}
}
