package com.rbp.core.model.domainmodel.flowable;


import com.rbp.core.utility.CalendarTool;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

/**
 * Data Transfer Object for Task
 * IdentityLinkInfos will not be sent to the UI as they contain information on who can do the task etc. due to security reasons.
 * Flowable trusts that Sayban provides consistent and correct map for tasks.
 */
public class TaskDTO implements Serializable {

    private static final long serialVersionUID = -7440068586304052792L;


    String id;
    String owner;
    String assignee;
    String name;
    String description;//@TODO: is this required?
    LocalDateTime createTime;
    LocalDateTime dueDate;
    String category;
    String executionId;
    String processInstanceId;
    String processDefinitionId;
    String taskDefinitionKey;
    Map<String,Object> map;
    TaskDueDateEnum dueDateColor=TaskDueDateEnum.GREEN; //After setting the due date, this value will be filled.
    //default means there is no due date so the enum will always be green (no dead line)



    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = CalendarTool.convertToLocalDateTimeViaSqlTimestamp(createTime);
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = CalendarTool.convertToLocalDateTimeViaSqlTimestamp(dueDate);

        LocalDate currentDate=LocalDateTime.now().toLocalDate();//Current Date!
        LocalDate JustDateOfDueDate=this.dueDate.toLocalDate();//Current Due date minus the time!

        if(JustDateOfDueDate.compareTo(currentDate)==0) //equal dates ->red color
            dueDateColor=TaskDueDateEnum.RED;
        else if(JustDateOfDueDate.minusDays((long)TaskDueDateEnum.getYellowIndicator()).compareTo(currentDate)>=0)
            dueDateColor=TaskDueDateEnum.YELLOW;//the due date is YellowIndicator days more than current date
        //if non of the above is currect or there is no due date, the color will be green.
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

}