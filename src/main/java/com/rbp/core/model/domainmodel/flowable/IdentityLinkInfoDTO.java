package com.rbp.core.model.domainmodel.flowable;

import java.io.Serializable;

/**
 * a DTO for identityLinks. this inlcude userId or GroupId associated with a taskId. The type can be one of the following(you can get them from IdentityLinkType):
 * CANDIDATE, ASSIGNEE, OWNER etc.
 */
public class IdentityLinkInfoDTO implements   Serializable {
    private static final long serialVersionUID = -523350439475370377L;
    String type; //IndentityLinkType interface has all the values required for this.

//    public static final String ASSIGNEE = "assignee";
//    public static final String CANDIDATE = "candidate";
//    public static final String OWNER = "owner";
//    public static final String STARTER = "starter";
//    public static final String PARTICIPANT = "participant";

    String userId;
    String groupId;
    String taskId;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
