/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.basicInformation.geographical;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "GEO$Zone")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Zone",allocationSize = 1)
public class Zone extends BaseEntity {

    private static final long serialVersionUID = 2327400028343826633L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "TITLE")
    private String title;

//    @Column(name = "IS_CITY_ZONE")
//    private Boolean cityZone;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_CITY_ID")
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    private City city;
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "FK_VILLAGE_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Village village;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public Boolean isCityZone() {
//        return cityZone;
//    }
//
//    public void setCityZone(Boolean cityZone) {
//        this.cityZone = cityZone;
//    }
//
//    public City getCity() {
//        return city;
//    }
//
//    public void setCity(City city) {
//        this.city = city;
//    }

    public Village getVillage() {
        return village;
    }

    public void setVillage(Village village) {
        this.village = village;
    }
}
