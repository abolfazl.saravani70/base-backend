package com.rbp.core.model.domainmodel.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.Stakeholder;
import com.rbp.sayban.model.domainmodel.system.Blob;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "SEC$User")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_User",allocationSize = 1)
@DynamicUpdate
public class User extends BaseEntity implements UserDetails {

	private static final long serialVersionUID = 5465629337450087688L;

	@Column(name = "NAME", unique = true)
	private String username;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "PASSWORD",updatable = false)
	private String password;

	@Column(name = "OBSOLETE")
	private Boolean isObsolete;

	@Column(name = "LAST_LOGIN")
	private String lastLogin;
	
	@Transient
    @JsonIgnore
	private Set<GrantedAuthority> authorities;

	@Column(name = "IS_ONLINE")
	private Boolean isOnline;

	@Column(name = "EXPIRE_DATE")
	private String expireDate;

	@Column(name = "LAST_IP")
	private String lastIp;

	@Column(name = "IS_ACTIVE")
	private Boolean isActive;
    //---------------------------
    //TODO: FIX TYPES OF PHONE NUMBERS
	@Column(name = "TELL")
	private String tell;

	@Column(name = "MOBILE")
	private String mobile;

	@Column
    private String kowsarNumber;
	//----------------------------

	@Column(name = "PICTURE")
	private String picture;

	@Column(name = "ACTIVE_THEME")
	private String activeTheme;

	@Column(name = "FORCE_CHANGE_PASSWORD")
	private Boolean isForceChangePassword;

	@Column(name = "TOKEN")
	private String token;

	@Transient
	boolean locked;

   // @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  //  @JoinColumn(name = "FK_BLOB_ID")


    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STAKEHOLDER_ID")
    private Stakeholder stakeholder;


    @ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    @JoinTable(name = "SEC$UserGroup", joinColumns = @JoinColumn(name = "FK_USER_ID", referencedColumnName = "ID", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "FK_GROUP_ID", referencedColumnName = "ID", nullable = false))
    private Set<Group> groups;



	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return false;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIsObsolete() {
        return isObsolete;
    }

    public void setIsObsolete(Boolean obsolete) {
        this.isObsolete = obsolete;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public void setAuthorities(Set<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean online) {
        isOnline = online;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public String getTell() {
        return tell;
    }

    public void setTell(String tell) {
        this.tell = tell;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getActiveTheme() {
        return activeTheme;
    }

    public void setActiveTheme(String activeTheme) {
        this.activeTheme = activeTheme;
    }

    public Boolean getIsForceChangePassword() {
        return isForceChangePassword;
    }

    public void setIsForceChangePassword(Boolean forceChangePassword) {
        this.isForceChangePassword = forceChangePassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Stakeholder getStakeholder() {
        return stakeholder;
    }

    public void setStakeholder(Stakeholder stakeholder) {
        this.stakeholder = stakeholder;
    }

    //EQUALS AND HASHCODE

    @Override
    public int hashCode() {
        return 11;
    }

    public Boolean getObsolete() {
        return isObsolete;
    }

    public void setObsolete(Boolean obsolete) {
        isObsolete = obsolete;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getKowsarNumber() {
        return kowsarNumber;
    }

    public void setKowsarNumber(String kowsarNumber) {
        this.kowsarNumber = kowsarNumber;
    }

}
