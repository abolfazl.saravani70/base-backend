/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.gridStructure.filter;

import java.util.List;

public class SetColumnFilter extends ColumnFilter {
    private List<String> values;

    public SetColumnFilter() {
    }

    public SetColumnFilter(List<String> values) {
        this.values = values;
    }

    public List<String> getValues() {
        return values;
    }
}
