/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.basicInformation.bankingInfo;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.OrganizationAccount;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "BNK$Account")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Account", allocationSize = 1)
public class Account extends BaseEntity {

    private static final long serialVersionUID = -6218315704260465355L;
   // public static final String[] RELATIONS={"OrganizationAccount"};

    @Column(name = "NUMBER$",unique = true)
    private String number;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "FK_BRANCH_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Branch branch;

    //@TODO: check why this works without this: mappedBy = "account",
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<OrganizationAccount> organizationAccount;

//    public static String[] getRELATIONS() {
//        return RELATIONS;
//    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Set<OrganizationAccount> getOrganizationAccount() {
        return organizationAccount;
    }

    public void setOrganizationAccount(Set<OrganizationAccount> organizationAccount) {
        this.organizationAccount = organizationAccount;
    }
}
