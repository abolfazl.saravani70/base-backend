package com.rbp.core.model.domainmodel.base.abstractClass.junction;


import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.Budget;

import javax.persistence.*;

@MappedSuperclass
public class BudgetJunctionBaseEntity extends EvalStateBaseEntity {

    private static final long serialVersionUID = -6602540687050092559L;

    @Column(name = "ESTIMATE_BUDGET")
    private Long estimateBudget;

    @Column(name = "ACTUAL_BUDGET")
    private Long actualBudget;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_BUDGET_ID")
    private Budget budget;

    public Long getEstimateBudget() {
        return estimateBudget;
    }

    public void setEstimateBudget(Long estimateBudget) {
        this.estimateBudget = estimateBudget;
    }

    public Long getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Long actualBudget) {
        this.actualBudget = actualBudget;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }
}
