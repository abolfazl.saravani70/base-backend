package com.rbp.core.model.domainmodel.search;

import java.util.List;

public class AdvancedSearchResult<T> {
   private List<T> records;
   private Long totalCount;

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }
}
