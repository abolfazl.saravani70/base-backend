package com.rbp.core.model.domainmodel.base.abstractClass.junction;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Location;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class LocationJunctionBaseEntity extends EvalStateBaseEntity {

    private static final long serialVersionUID = -1433780330453115107L;

    @Column(name = "CAPACITY")
    private Long capacity;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "USAGE_TYPE")
    private String usageType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_LOCATION_ID")
    private Location location;

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
