
package com.rbp.core.model.domainmodel.security;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "SEC$Group")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Group", allocationSize = 1)
public class Group extends BaseEntity {
    private static final long serialVersionUID = -1314520518355363493L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PERSIAN_NAME")
    private String persianName;

    @ManyToMany(mappedBy = "groups",fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
   // @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Set<User> users;

    @ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    @JoinTable(name = "SEC$PermissionGroup",
            joinColumns = @JoinColumn(name = "FK_GROUP_ID", referencedColumnName = "ID", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "FK_PERMISSION_ID", referencedColumnName = "ID", nullable = false))
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<Permission> permissions;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_GRP_ID")
    private Group parentGroup;

    public Group getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(Group parentGroup) {
        this.parentGroup = parentGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersianName() {
        return persianName;
    }

    public void setPersianName(String persianName) {
        this.persianName = persianName;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Group group = (Group) o;
        boolean result=getId()!=null && getId().equals(group.getId());
        return result;
    }

    @Override
    public int hashCode() {
        return 21;
    }
}
