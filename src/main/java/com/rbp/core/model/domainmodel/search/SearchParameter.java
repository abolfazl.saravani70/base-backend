package com.rbp.core.model.domainmodel.search;


import javax.persistence.criteria.*;
import java.util.List;

import static com.rbp.core.model.domainmodel.search.SearchOperation.BLANK;

public class SearchParameter {
    private CriteriaBuilder builder;
    private CriteriaQuery query;
    private Predicate predicate;
    private Predicate havingPredicate;
    private Root root;
    private SearchQueryCriteria searchConsumer;
    private CriteriaParser criteriaParser;
    private List<Expression<?>> expressionList;
    private List<Selection<?>> lst;
    private List<String> orderList;

    public CriteriaBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(CriteriaBuilder builder) {
        this.builder = builder;
    }

    public CriteriaQuery getQuery() {
        return query;
    }

    public void setQuery(CriteriaQuery query) {
        this.query = query;
    }

    public Predicate getPredicate() {
        return predicate;
    }

    public void setPredicate(Predicate predicate) {
        this.predicate = predicate;
    }

    public Root getRoot() {
        return root;
    }

    public void setRoot(Root root) {
        this.root = root;
    }

    public SearchQueryCriteria getSearchConsumer() {
        return searchConsumer;
    }

    public void setSearchConsumer(SearchQueryCriteria searchConsumer) {
        this.searchConsumer = searchConsumer;
    }

    public CriteriaParser getCriteriaParser() {
        return criteriaParser;
    }

    public void setCriteriaParser(CriteriaParser criteriaParser) {
        this.criteriaParser = criteriaParser;
    }

    public List<Expression<?>> getExpressionList() {
        return expressionList;
    }

    public void setExpressionList(List<Expression<?>> expressionList) {
        this.expressionList = expressionList;
    }

    public List<Selection<?>> getLst() {
        return lst;
    }

    public void setLst(List<Selection<?>> lst) {
        this.lst = lst;
    }

    public List<String> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<String> orderList) {
        this.orderList = orderList;
    }

    public Predicate getHavingPredicate() {
        return havingPredicate;
    }

    public void setHavingPredicate(Predicate havingPredicate) {
        this.havingPredicate = havingPredicate;
    }

    public Predicate getParsedPredicate(String queryStr) {
        if (queryStr != null && !queryStr.equals(BLANK)) {
            return this.searchConsumer.build(this.criteriaParser.parse(this, queryStr));
        } else {
            return null;
        }
    }

    public List<Order> getParsedOrderBys(String orderByColumn,String groupByCoulmn) {
        return this.criteriaParser.orderParse(this, orderByColumn,groupByCoulmn);
    }
}
