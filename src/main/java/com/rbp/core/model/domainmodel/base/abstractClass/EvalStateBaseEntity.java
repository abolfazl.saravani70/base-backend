package com.rbp.core.model.domainmodel.base.abstractClass;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class EvalStateBaseEntity extends BaseEntity {

    private static final long serialVersionUID = 7009376696529342493L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "IS_ROOT")
    private Boolean isRoot;

    @Column(name = "INITIAL_PERCENTAGE")
    private Double initialPercentage;

    @Column(name = "PERCENTAGE")
    private Double percentage;

    @Column(name = "WEIGHT")
    private Long weight;

    @Column(name = "VOLUME")
    private Long volume;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Column(name = "IMPORTANCE")
    private Double importance;

    @Column(name = "ACTUAL_START_DATE")
    private LocalDate actualStartDate;

    @Column(name = "ACTUAL_END_DATE")
    private LocalDate actualEndDate;

    @Column(name = "EXPIRATION_DATE")
    private LocalDate expirationDate;

    @Column(name = "START_DATE")
    private LocalDate startDate;

    @Column(name = "END_DATE")
    private LocalDate endDate;

    @Column(name = "FISCAL_YEAR")
    private String fiscalYear;

    @Column(name = "LEVEL$")
    private Long level;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_LOCATION_ID")
//    private OrganizationLocation organizationLocation;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_DOCUMENT_ID")
//    private Document document;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsRoot() {
        return isRoot;
    }

    public void setIsRoot(Boolean root) {
        isRoot = root;
    }

    public Double getInitialPercentage() {
        return initialPercentage;
    }

    public void setInitialPercentage(Double initialPercentage) {
        this.initialPercentage = initialPercentage;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Double getImportance() {
        return importance;
    }

    public void setImportance(Double importance) {
        this.importance = importance;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getRoot() {
        return isRoot;
    }

    public void setRoot(Boolean root) {
        isRoot = root;
    }

    public LocalDate getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(LocalDate actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public LocalDate getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(LocalDate actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

//    public OrganizationLocation getOrganizationLocation() {
//        return organizationLocation;
//    }
//
//    public void setOrganizationLocation(OrganizationLocation organizationLocation) {
//        this.organizationLocation = organizationLocation;
//    }
//
//    public Document getDocument() {
//        return document;
//    }
//
//    public void setDocument(Document document) {
//        this.document = document;
//    }
}
