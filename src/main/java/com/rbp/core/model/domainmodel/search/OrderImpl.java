/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.search;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;

public class OrderImpl implements Order {
    private final Expression<?> expression;
    private boolean ascending;

    public OrderImpl(Expression<?> expression) {
        this(expression, true);
    }

    OrderImpl(Expression<?> expression, boolean ascending) {
        this.expression = expression;
        this.ascending = ascending;
    }

    @Override
    public Order reverse() {

        ascending = !ascending;
        return this;
    }

    @Override
    public boolean isAscending() {
        return ascending;
    }

    @Override
    public Expression<?> getExpression() {
        return expression;
    }
}
