/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.domainmodel.base;


import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "BAS$FrameworkProperty")
public class FrameworkProperty extends BaseEntity {

	private static final long serialVersionUID = -2926513384904216846L;

//	@Id
//	@Column(name="ID")
//	@GenericGenerator(name = "FRMW_PROPERTY_SEQ",
//	strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
//	parameters = {
//	        @org.hibernate.annotations.Parameter(
//	                name = "optimizer", value = "pooled-lo"),
//	        @org.hibernate.annotations.Parameter(
//	                name = "initial_value", value = "1000"),
//	        @org.hibernate.annotations.Parameter(
//	                name = "increment_size", value = "1"),
//	        @org.hibernate.annotations.Parameter(
//	                name = SequenceStyleGenerator.SEQUENCE_PARAM, value = "FRMW_PROPERTY_SEQ"),
//	    }
//	)
//	@GeneratedValue(generator="FRMW_PROPERTY_SEQ",strategy=GenerationType.TABLE )
//	private Long Id;

	@Column(name = "KEY",unique = true)
	private String key;

	@Column(name = "CODE", unique = true)
	private Integer code;

	@Column(name = "VALUE")
	private String value;

	@Column(name = "NAME")
	private String name;

	@Column(name = "ENGLISH_NAME")
	private String englishName;

	@Column(name = "IS_FOR_SYS")
	private Boolean isForSys;

	@Column(name = "PRIORITY")
	private Integer priority;

	@Column(name = "EXPIRATION_DATE")
	private LocalDate expirationDate;


	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public Boolean getIsForSys() {
		return isForSys;
	}

	public void setIsForSys(Boolean forSys) {
		isForSys = forSys;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public LocalDate getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDate expirationDate) {
		this.expirationDate = expirationDate;
	}
}
