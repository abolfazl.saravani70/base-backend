//package com.rbp.core.model.domainmodel.security;
//
//import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
//import org.hibernate.annotations.OnDelete;
//import org.hibernate.annotations.OnDeleteAction;
//
//import javax.persistence.*;
//import java.util.Set;
//
//@Entity
//@Table(name = "SEC$OrgPosition")
//@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_OrgPosition",allocationSize = 1)
//public class OrganizationPosition extends BaseEntity {
//
//	private static final long serialVersionUID = 3877354980394951003L;
//
//	@Column(name = "NAME")
//	private String name;
//
//	@Column(name = "CODE")
//	private Integer code;
//
//	@ManyToOne(fetch = FetchType.EAGER,cascade={CascadeType.ALL})
//	@JoinColumn(name = "FK_ORG_POSTITION_ID")
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	private OrganizationPosition parentPosition;
//
//	@Column(name = "FK_PV_ORG_UNIT_ID")
//	private Long pvOrganizationUnitId;
//
//	@ManyToMany(fetch = FetchType.EAGER)
//	@JoinTable(name = "SEC$OrgPositionGroup", joinColumns = @JoinColumn(name = "FK_ORG_POSITION_ID", referencedColumnName = "ID", nullable = false), inverseJoinColumns = @JoinColumn(name = "FK_GROUP_ID", referencedColumnName = "ID", nullable = false))
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	private Set<Group> groups;
//
//	@ManyToMany(mappedBy = "organizationPositions",fetch = FetchType.LAZY)
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	private Set<User> users;
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public Integer getCode() {
//		return code;
//	}
//
//	public void setCode(Integer code) {
//		this.code = code;
//	}
//
//	public OrganizationPosition getParentPosition() {
//		return parentPosition;
//	}
//
//	public void setParentPosition(OrganizationPosition parentPosition) {
//		this.parentPosition = parentPosition;
//	}
//
//	public Long getPvOrganizationUnitId() {
//		return pvOrganizationUnitId;
//	}
//
//	public void setPvOrganizationUnitId(Long pvOrganizationUnitId) {
//		this.pvOrganizationUnitId = pvOrganizationUnitId;
//	}
//
//	public Set<Group> getGroups() {
//		return groups;
//	}
//
//	public void setGroups(Set<Group> groups) {
//		this.groups = groups;
//	}
//
//	public Set<User> getUsers() {
//		return users;
//	}
//
//	public void setUsers(Set<User> users) {
//		this.users = users;
//	}
//}
