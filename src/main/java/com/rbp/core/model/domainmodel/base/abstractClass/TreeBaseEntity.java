/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.core.model.domainmodel.base.abstractClass;

import javax.persistence.*;

@MappedSuperclass
public abstract class TreeBaseEntity<selfType extends BaseEntity> extends BaseEntity{

//    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
//    @JoinColumn(name = "FK_CHILD_ID")
//    private selfType child;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_PARENT_ID")
//    private selfType parent;

    public abstract selfType getChild();

    public abstract void setChild(selfType child);

    public abstract selfType getParent();

    public abstract void setParent(selfType parent);
}
