package com.rbp.core.model.domainmodel.softDelete;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.model.dto.base.FilterJsonInput;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Class.forName;

public abstract class SoftDelete<T extends BaseEntity> {
    protected abstract Class<T> getDomainClass();

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    protected abstract IGenericRepository<T> getGenericRepository();

    public EntityManager getEntityManager() {
        return entityManager;
    }

    protected Class<T> clazz = getDomainClass();

    private List<Annotation> getAnnotationFields(Class cls) {
//        Class<? extends BaseEntity> cls = entity.getClass();
        List annotationList = new ArrayList();
        try {
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                annotationList.addAll(Arrays.asList(field.getDeclaredAnnotations()));
            }
            return annotationList;
        } catch (Exception var5) {
            return null;

        }
    }

    public void softEntity(T entity) {
        Class<? extends BaseEntity> cls = entity.getClass();
        entityManager.createQuery("update " + cls.getName() +
                " set  isDeleted= true" +
                ", ip='" + getDummyUser().getIp() +
                "', deletedById=" + getDummyUser().getId() +
                ", version= version+1" +
                ",zoneCode='" + getDummyUser().getZoneCode() +
                "' , deletedDate='" + LocalDateTime.now() +
                "' where  id= " + entity.getId())
                .executeUpdate();
        soft(cls, entity.getId());
    }

    private int soft(Class cls, Long id) {
        List<Type> types = getTypes(cls);
        if (types.size() == 0)
            return 0;
        else {
            for (Type type : types) {
                String[] fildClassName = getClassName(type);
                Class clsChilde = null;
                Class clsParrent = null;
                try {
                    clsChilde = Class.forName(fildClassName[1]);
                    clsParrent = forName(cls.getTypeName());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                List<String> fieldNameOfChild = getFieldOfChild(clsChilde, clsParrent);
                FilterJsonInput filter=new FilterJsonInput();
                for (String field : fieldNameOfChild) {
                    entityManager.createQuery("update " + fildClassName[0] +
                            " set  isDeleted= true" +
                            ", ip='" + getDummyUser().getIp() +
                            "', deletedById=" + getDummyUser().getId() +
                            ", version= version+1" +
                            ",zoneCode='" + getDummyUser().getZoneCode() +
                            "' , deletedDate='" + LocalDateTime.now() +
                            "' where  " + field + ".id=" + id)
                            .executeUpdate();

                    clazz = clsChilde;
                    filter.where(field + ".id=" + id);
                    AdvancedSearchResult searchResult = getGenericRepository().searchEntity(filter, false);
                    List<T> searchIds = searchResult.getRecords();
                    for (T searchId : searchIds)
                        soft(clsChilde, searchId.getId());
                }


            }

            return 1;
        }


    }

    private List<Type> getTypes(Class cls) {

        List<Annotation> annotationList = getAnnotationFields(cls);
        List<Type> types = new ArrayList<>();
        for (Annotation annotation : annotationList) {
            String[] strings = annotation.annotationType().getName().split("[.]");
            if (strings.length - 1 > 0 && strings[strings.length - 1].equals("OneToMany")) {

                types.add(TypeOfFieldOfAnnotation(cls, annotation));
            }
        }
        return types;
    }

    private Type TypeOfFieldOfAnnotation(Class cls, Annotation annotation) {
//        Class<? extends BaseEntity> cls = entity.getClass();
        Field[] fields = cls.getDeclaredFields();

        for (Field field : fields) {


            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation1 : annotations) {
                if (annotation1.equals(annotation)) {
                    Type f = field.getGenericType();
                    return field.getGenericType();
                }
            }
        }
        return null;
    }


    private String[] getClassName(Type type) {
        String typeName = type.getTypeName();
        String[] result = new String[2];
        if (typeName.contains("<")) {
            String[] name = typeName.split("<");
            name = name[1].split(">");
            result[1] = name[0];
            name = name[0].split("[.]");
            if (name.length == 0)
                return null;
            else {
                result[0] = name[name.length - 1];
            }
        } else
            return null;

        return result;
    }

    private List<String> getFieldOfChild(Class fieldName, Class parentType) {
        try {
            Field[] fields = fieldName.getDeclaredFields();

            List filedSameWithTypeOfParent = Arrays.stream(fields)
                    .filter(x -> x.getType().equals(parentType))
                    .map(Field::getName)
                    .collect(Collectors.toList());

            return filedSameWithTypeOfParent;
        } catch (Exception e) {
            return null;
        }

    }

    User getDummyUser() {
        User u = new User();
        u.setId(1L);
        u.setIp("0.0.0");
        u.setZoneCode("000");
        return u;
    }
}
