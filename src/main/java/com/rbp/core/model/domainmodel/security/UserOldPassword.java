package com.rbp.core.model.domainmodel.security;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "SEC$UserOldPassword")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_UserOldPassword",allocationSize = 1)
public class UserOldPassword extends BaseEntity {

	private static final long serialVersionUID = 3731656165696349274L;

	@Column(name = "OLD_PASSWORD")
	private String oldPassword;

	@ManyToOne(fetch = FetchType.LAZY,optional = false)
	@JoinColumn(name = "FK_USER_ID")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
