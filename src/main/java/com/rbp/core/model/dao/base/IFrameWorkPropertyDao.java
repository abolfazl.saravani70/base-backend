package com.rbp.core.model.dao.base;

import com.rbp.core.model.domainmodel.base.FrameworkProperty;

public interface IFrameWorkPropertyDao extends IGenericRepository<FrameworkProperty>{

}
