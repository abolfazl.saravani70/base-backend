/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.bankingInfo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.bankingInfo.IBankDao;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Bank;
import org.springframework.stereotype.Repository;

@Repository
public class BankDaoImpl extends GenericRepository<Bank> implements IBankDao {

    @Override
    protected Class<Bank> getDomainClass() {
        // TODO Auto-generated method stub
        return Bank.class;
    }
}
