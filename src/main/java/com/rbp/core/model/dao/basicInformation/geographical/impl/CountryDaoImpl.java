/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.ICountryDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Country;
import org.springframework.stereotype.Repository;

@Repository
public class CountryDaoImpl extends GenericRepository<Country> implements ICountryDao {

    @Override
    protected Class<Country> getDomainClass() {
        // TODO Auto-generated method stub
        return Country.class;
    }
}
