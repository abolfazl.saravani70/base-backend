package com.rbp.core.model.dao.basicInformation.geographical;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Location;

public interface ILocationDao extends IGenericRepository<Location> {
}
