package com.rbp.core.model.dao.base;

import com.rbp.core.model.dto.base.FilterJsonInput;

import java.util.List;

public interface IGenericRootRepository<T> extends IGenericTreeRepository<T> {

    List<T> getChildrenByRootId(FilterJsonInput filter);
}
