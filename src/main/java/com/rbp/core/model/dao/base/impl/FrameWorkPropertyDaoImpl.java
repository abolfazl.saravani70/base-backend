package com.rbp.core.model.dao.base.impl;

import com.rbp.core.model.dao.base.IFrameWorkPropertyDao;
import com.rbp.core.model.domainmodel.base.FrameworkProperty;
import org.springframework.stereotype.Repository;

@Repository
public class FrameWorkPropertyDaoImpl extends GenericRepository<FrameworkProperty> implements IFrameWorkPropertyDao {

	@Override
	protected Class<FrameworkProperty> getDomainClass() {
		// TODO Auto-generated method stub
		return FrameworkProperty.class;
	}



}
