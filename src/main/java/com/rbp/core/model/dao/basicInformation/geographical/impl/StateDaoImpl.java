/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.IStateDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.State;
import org.springframework.stereotype.Repository;

@Repository
public class StateDaoImpl extends GenericRepository<State> implements IStateDao {

    @Override
    protected Class<State> getDomainClass() {
        // TODO Auto-generated method stub
        return State.class;
    }
}
