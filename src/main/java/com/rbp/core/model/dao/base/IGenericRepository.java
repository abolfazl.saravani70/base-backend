package com.rbp.core.model.dao.base;
/**
 * @author mirghafari
 */

import com.rbp.core.model.domainmodel.gridStructure.request.EnterpriseGetRowsRequest;
import com.rbp.core.model.domainmodel.gridStructure.response.EnterpriseGetRowsResponse;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.dto.base.FilterJsonInput;

public interface IGenericRepository<T> {

    T save(T entity);

    AdvancedSearchResult<T> getList(FilterJsonInput filter);

    T loadById(FilterJsonInput filter);

    void delete(T entity);

    void update(T entity);

    T getReference(Long id);

    AdvancedSearchResult<T> searchEntity(FilterJsonInput filter, Boolean isTempDeleted);
     AdvancedSearchResult searchView(Class viewClass, FilterJsonInput filter, Boolean isTempDeleted);

    EnterpriseGetRowsResponse getData(EnterpriseGetRowsRequest request);
}
