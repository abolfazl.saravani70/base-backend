/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.dao.security.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.security.IUserOldPasswordDao;
import com.rbp.core.model.domainmodel.security.UserOldPassword;
import org.springframework.stereotype.Repository;

@Repository
public class UserOldPasswordDaoImpl extends GenericRepository<UserOldPassword> implements IUserOldPasswordDao {
    @Override
    protected Class<UserOldPassword> getDomainClass() {
        return UserOldPassword.class;
    }
}
