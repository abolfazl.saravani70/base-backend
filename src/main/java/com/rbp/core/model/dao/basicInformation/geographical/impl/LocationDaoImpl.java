package com.rbp.core.model.dao.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.ILocationDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Location;
import org.springframework.stereotype.Repository;

@Repository
public class LocationDaoImpl  extends GenericRepository<Location> implements ILocationDao {

    @Override
    protected Class<Location> getDomainClass() {
        // TODO Auto-generated method stub
        return Location.class;
    }
}
