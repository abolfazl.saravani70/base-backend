package com.rbp.core.model.dao.base.impl;

import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.dto.base.FilterJsonInput;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public abstract class GenericTreeRepository<T extends TreeBaseEntity>
        extends GenericRepository<T> implements IGenericTreeRepository<T> {

    protected abstract Class<T> getDomainClass();

    @Override
    public List<T> getChildrenByParentId(FilterJsonInput filter) {
        AdvancedSearchResult<T> searchResult=searchEntity(filter,false);
        List<T> children = searchResult.getRecords();
        return children;
    }

    @Override
    public List<T> getParentsByChildId(FilterJsonInput filter) {
        AdvancedSearchResult<T> searchResult=searchEntity(filter,false);
        List<T> parents=searchResult.getRecords();
        return parents;
    }

}
