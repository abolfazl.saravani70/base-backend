package com.rbp.core.model.dao.security.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.security.IGroupDao;
import com.rbp.core.model.domainmodel.security.Group;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GroupDaoImpl extends GenericRepository<Group> implements IGroupDao {

	@Override
	protected Class<Group> getDomainClass() {
		return Group.class;
	}

	@Override
	public List<Group> getGroupsWithoutPermission() {
		return null;
	}

}
