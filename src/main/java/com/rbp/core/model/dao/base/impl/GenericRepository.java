package com.rbp.core.model.dao.base.impl;

/**
 * @author shobeir shafiee
 */

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.gridStructure.builder.OracleSqlQueryBuilder;
import com.rbp.core.model.domainmodel.gridStructure.request.ColumnVO;
import com.rbp.core.model.domainmodel.gridStructure.request.EnterpriseGetRowsRequest;
import com.rbp.core.model.domainmodel.gridStructure.response.EnterpriseGetRowsResponse;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.domainmodel.search.SearchRepository;
import com.rbp.core.model.dto.base.FilterJsonInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.metamodel.EntityType;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.rbp.core.model.domainmodel.gridStructure.builder.EnterpriseResponseBuilder.createResponse;
import static java.lang.String.format;
import static java.util.stream.Collectors.toMap;


@Repository
public abstract class GenericRepository<T extends BaseEntity> implements IGenericRepository<T> {
    protected Class<T> clazz = getDomainClass();
    private String viewName;

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private JdbcTemplate template;
    private OracleSqlQueryBuilder queryBuilder;

    public GenericRepository() {
        this.queryBuilder = new OracleSqlQueryBuilder();
    }

    protected abstract Class<T> getDomainClass();

    public EntityManager getEntityManager() {
        return entityManager;
    }


    public static Class<?> getEntityClass(EntityManager entityManager, String entityName) {
        for (EntityType<?> entity : entityManager.getMetamodel().getEntities()) {
            if (entityName.equals(entity.getName())) {
                return entity.getJavaType();
            }
        }

        return null;
    }


    public T save(T entity) {
        Long id = entity.getId();
        if (id == 0) {
            entity.setId(null);
            entityManager.persist(entity);
            //TODO: handle exceptions that persist may throw
        } else {
            entityManager.merge(entity);
        }
        return entity;
    }

    @Override
    public AdvancedSearchResult<T> getList(FilterJsonInput filter) {
        return searchEntity(filter, false);
    }

    @Override
    public T loadById(FilterJsonInput filter) {
        AdvancedSearchResult<T> list = searchEntity(filter, false);
        if (list.getRecords().size() != 0) {
            T t = list.getRecords().get(0);
            return t;
        } else
            return null;
    }

    @Override
    public void delete(T entity) {
        // getSoftDelete().softEntity(entity);
        entityManager.remove(entity);
    }

    @Override
    public void update(T entity) {
        entityManager.merge(entity);
    }

    @Override
    public T getReference(Long id) {
        return entityManager.getReference(clazz, id);
    }

    @Override
    public AdvancedSearchResult<T> searchEntity(FilterJsonInput filter, Boolean isTempDeleted) {
        SearchRepository<T> searchRepository = new SearchRepository(entityManager);
        return searchRepository.search(clazz, filter, isTempDeleted);
    }
    @Override
    public AdvancedSearchResult searchView(Class viewClass, FilterJsonInput filter, Boolean isTempDeleted) {
        SearchRepository<T> searchRepository = new SearchRepository(entityManager);
        return searchRepository.search(viewClass,  filter, isTempDeleted);
    }


    @Override
    public EnterpriseGetRowsResponse getData(EnterpriseGetRowsRequest request) {
        Class<?> cls = getViewNameAndClass(request.getViewName());

        EnterpriseGetRowsResponse response = new EnterpriseGetRowsResponse();

        if (!this.viewName.equals("") && cls != null) {
            // first obtain the pivot values from the DB for the requested pivot columns
            Map<String, List<String>> pivotValues = getPivotValues(request.getPivotCols());

            // generate sql
            String sql = queryBuilder.createSql(request, this.viewName, cls, pivotValues);

            // query db for rows
            List<Map<String, Object>> rows = template.queryForList(sql);

            rows = queryBuilder.mapColumnToFieldRecords(rows);

            // create response with our results
            response = createResponse(request, rows, pivotValues);
        }
        return response;
    }

    private Class<?> getViewNameAndClass(String className) {
        List<EntityType<?>> entity = getEntityManager().getMetamodel().getEntities()
                .stream()
                .filter(x-> x.getName().equals(className)).collect(Collectors.toList());
        Class<?> cls = null;
        String viewName = "";
        if( entity.size() > 0){
            cls = entity.get(0).getJavaType();
            Table table = cls.getAnnotation(Table.class); // could be supplied in request as a lookup key?
            this.viewName = table.name();
        }

        return cls;
    }

    private Map<String, List<String>> getPivotValues(List<ColumnVO> pivotCols) {
        return pivotCols.stream()
                .map(ColumnVO::getField)
                .collect(toMap(pivotCol -> pivotCol, this::getPivotValues, (a, b) -> a, LinkedHashMap::new));
    }

    private List<String> getPivotValues(String pivotColumn) {
        String sql = "SELECT DISTINCT %1$s FROM %2$s";
        return template.queryForList(format(sql, pivotColumn,this.viewName), String.class);
    }

}


