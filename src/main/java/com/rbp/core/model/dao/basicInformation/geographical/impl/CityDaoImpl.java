/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.ICityDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.City;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityDaoImpl extends GenericRepository<City> implements ICityDao {

    @Override
    protected Class<City> getDomainClass() {
        // TODO Auto-generated method stub
        return City.class;
    }
    @Override
    public List<City> getByStateId(Long stateId) {
        List<City> result = getEntityManager().createQuery("from " + getDomainClass().getName() + " where state = " + stateId)
                .getResultList();

        return result;
    }
}
