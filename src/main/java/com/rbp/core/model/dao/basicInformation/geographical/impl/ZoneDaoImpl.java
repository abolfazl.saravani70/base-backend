/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.IZoneDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Zone;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ZoneDaoImpl extends GenericRepository<Zone> implements IZoneDao {

    @Override
    protected Class<Zone> getDomainClass() {
        // TODO Auto-generated method stub
        return Zone.class;
    }
    @Override
    public List<Zone> getByVillageId(Long villageId) {
        List<Zone> result = getEntityManager().createQuery("from " + getDomainClass().getName() + " where village = " + villageId)
                .getResultList();

        return result;
    }
}
