package com.rbp.core.model.dao.base.impl;

import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.core.model.dto.base.FilterJsonInput;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public abstract class GenericRootRepository<T extends RootBaseEntity> extends GenericTreeRepository<T>
        implements IGenericRootRepository<T> {

    protected abstract Class<T> getDomainClass();

    @Override
    public List<T> getChildrenByRootId(FilterJsonInput filter) {
        AdvancedSearchResult<T> searchResult = searchEntity(filter, false);
        List<T> children = searchResult.getRecords();
        return children;
    }
}
