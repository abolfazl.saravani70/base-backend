package com.rbp.core.model.dao.base;

import com.rbp.core.model.dto.base.FilterJsonInput;

import java.util.List;

public interface IGenericTreeRepository<T> extends IGenericRepository<T> {

    List<T> getChildrenByParentId(FilterJsonInput filter);
    List<T> getParentsByChildId(FilterJsonInput filter);
}
