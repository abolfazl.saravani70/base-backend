package com.rbp.core.model.dao.base.impl;

import com.rbp.core.model.dao.base.IFrameWorkPropertyValueDao;
import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
import com.rbp.core.model.dto.base.FrameworkPropertyValueDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FrameWorkPropertyValueDaoImpl extends GenericRepository<FrameworkPropertyValue> implements IFrameWorkPropertyValueDao {

	@Override
	protected Class<FrameworkPropertyValue> getDomainClass() {
		// TODO Auto-generated method stub
		return FrameworkPropertyValue.class;
	}

	@Override
	public List<FrameworkPropertyValue> getByPropertyId(Long PropId) {
		List<FrameworkPropertyValue> result = getEntityManager().createQuery("from " + getDomainClass().getName() + " where frameworkProperty = " + PropId)
				.getResultList();

		return result;
	}

	@Override
	public FrameworkPropertyValueDTO getByPropertyIdAndKeyCode(Long PropId, Long KeyCode) {
		return null;
	}

	@Override
	public List<FrameworkPropertyValueDTO> getByPropertyKeyCode(Long KeyCode) {
		return null;
	}
}
