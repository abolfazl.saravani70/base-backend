/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.geographical.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.geographical.IVillageDao;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Village;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VillageDaoImpl extends GenericRepository<Village> implements IVillageDao {

    @Override
    protected Class<Village> getDomainClass() {
        // TODO Auto-generated method stub
        return Village.class;
    }
    @Override
    public List<Village> getByCityId(Long cityId) {
        List<Village> result = getEntityManager().createQuery("from " + getDomainClass().getName() + " where city = " + cityId)
                .getResultList();

        return result;
    }
}
