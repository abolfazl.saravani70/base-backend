package com.rbp.core.model.dao.base;

import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
import com.rbp.core.model.dto.base.FrameworkPropertyValueDTO;

import java.util.List;

public interface IFrameWorkPropertyValueDao extends IGenericRepository<FrameworkPropertyValue>{
	 List<FrameworkPropertyValue> getByPropertyId(Long PropId);
	 FrameworkPropertyValueDTO getByPropertyIdAndKeyCode(Long PropId, Long KeyCode);
	 List<FrameworkPropertyValueDTO> getByPropertyKeyCode(Long KeyCode);
}
