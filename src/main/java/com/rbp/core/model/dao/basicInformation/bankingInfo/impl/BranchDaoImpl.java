/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.bankingInfo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.bankingInfo.IBranchDao;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Branch;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BranchDaoImpl extends GenericRepository<Branch> implements IBranchDao {

    @Override
    protected Class<Branch> getDomainClass() {
        // TODO Auto-generated method stub
        return Branch.class;
    }

    @Override
    public List<Branch> getBranchesByBankId(Long bankId) {

        List<Branch> branches = getEntityManager()
                .createQuery("FROM Branch where bank = " + bankId)
                .getResultList();

        return branches;
    }
}
