package com.rbp.core.model.dao.security.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.security.IUserDao;
import com.rbp.core.model.domainmodel.security.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends GenericRepository<User> implements IUserDao {

    @Override
    protected Class<User> getDomainClass() {
        return User.class;
    }

//    /**
//     * get user with username instead of an id
//     *
//     * @param username
//     * @return
//     */
//    @Override
//    public User getUserByUsername(String username) {
//        User user = null;
//        List<Object> o = getEntityManager().createQuery("from User where name ='" + username + "'").getResultList();
//        if (o.size() != 0) {
//            user = (User) o.get(0);
//        } else return null;
//        user.getGroups().size();//just fill in the Group (don't load users!)
//        user.getGroups().forEach(x -> Hibernate.initialize(x.getPermissions()));
//        return user;
//    }
//
//    //TODO: Add password encryption if necessary
//
//    /**
//     * updates the password
//     *
//     * @param id
//     * @param password
//     */
//    @Override
//    public void updatePassowrd(Long id, String password) {
//        if (password == null || password.isEmpty()) throw new ApplicationException(1, "رمز عبور نباید خالی باشد.");
//        User user = loadById(id);
//        if (user.getPassword().equals(password)) throw new ApplicationException(1, "رمز عبور صحیح نیست.");
//        if (user.getPassword() == password)
//            throw new ApplicationException(1, "رمز عبور جدید با رمز عبور قبلی یکسان است.");
//        if (!user.getPassword().equals(password)) throw new ApplicationException(1, "رمز عبور نادرست است.");
//        if (user.getPassword().equals(password))
//            throw new ApplicationException(1, "رمز عبور جدید با رمز عبور قبلی یکسان است.");
//        Query q = getEntityManager().createQuery("update User set password='" + password + "'" +
//                "where id=" + id);
//        q.executeUpdate();
//    }
//
//
//    @Override
//    public Long saveGroup(User entity) {
//        return null;
//    }
//
//    @Override
//    public List<Group> getUserGroups(Long userId) {
//        User u = loadById(userId);
//        u.getGroups().size();
//        return u.getGroups().stream().collect(Collectors.toList());
//    }
//
//    @Override
//    public List<UserGroupPermissionDTO> getUserPermission(Long userId) {
//        return null;
//    }
//
//    @Override
//    public List<User> getUsers() {
//        return null;
//    }
//
////	@Override
////	public List<OrganizationPosition> getUserOrganizationPosition(User user) {
////		return null;
////	}
//
//
//    @Override
//    public List<User> getUserById(Long userId) {
//        return null;
//    }
//
//    @Override
//    public List<User> getUserPost(Long userId) {
//        return null;
//    }
//
//    @Override
//    public List<OrganizationPositionViewModel> getUserPostList(Long userId) {
//        return null;
//    }
//
//    /**
//     * just loading user, no relationship is loaded.
//     *
//     * @param id
//     * @return
//     */
//    @Override
//    public User lightLoadById(Long id) {
//        return loadById(id);
//    }
//
//    @Override
//    public List<BigDecimal> getUserCount() {
//        return null;
//    }
//
//    @Override
//    public Integer getOnlineUserCount() {
//        return null;
//    }
//
//    @Override
//    public Integer getStateUserCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getStateOnlineUserCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getCityUserCount(String cityCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getCityOnlineUserCount(String cityCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getUserPerStateStatusCount() {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getStateUserPerCityCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getStateOnlineUserPerCityCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getOnlineUserPerstateCount() {
//        return null;
//    }
//
//    @Override
//    public Integer getVillageUserCount(String villageCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getVillageOnlineUserCount(String villageCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getCityPerVillageUserCount(String cityCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getCityPerVillageOnlineUserCount(String cityCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getZoneUserCount(String zoneCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getZoneOnlineUserCount(String zoneCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskStateUserCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getServiceDeskStateOnlineUserCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskCityUserCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getServiceDeskCityOnlineUserCount(String cityCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskVillageUserCount(String cityCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getServiceDeskVillageZoneOnlineUserCount(String villageCode) {
//        return null;
//    }
//
//    @Override
//    public List<MonitoringUserDTO> getServiceDeskZoneUserCount(String stateCode) {
//        return null;
//    }
//
//    @Override
//    public Integer getServiceDeskZoneOnlineUserCount(String zoneCode) {
//        return null;
//    }
}
