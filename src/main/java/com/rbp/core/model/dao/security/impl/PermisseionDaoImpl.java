package com.rbp.core.model.dao.security.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.security.IPermisseionDao;
import com.rbp.core.model.domainmodel.security.Permission;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PermisseionDaoImpl extends GenericRepository<Permission> implements IPermisseionDao {
    @Override
    protected Class<Permission> getDomainClass() {
        // TODO Auto-generated method stub
        return Permission.class;
    }

//    @Override
//    public List<Permission> getPermissions() {
//        return null;
//    }
}
