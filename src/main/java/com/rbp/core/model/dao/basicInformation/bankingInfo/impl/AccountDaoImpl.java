/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.core.model.dao.basicInformation.bankingInfo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.basicInformation.bankingInfo.IAccountDao;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Account;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AccountDaoImpl extends GenericRepository<Account> implements IAccountDao {

    @Override
    protected Class<Account> getDomainClass() {
        // TODO Auto-generated method stub
        return Account.class;
    }


    @Override
    public Account findByNumber(String number) {

        List<Account> acc = getEntityManager().createQuery("FROM " + getDomainClass().getSimpleName() +
                " WHERE  number = '" + number + "'", getDomainClass())
                .getResultList();

        if(acc.size() > 0){
            return acc.get(0);
        }else {
            return null;
        }
    }
}
