package com.rbp.core.model.dao.security;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.security.Group;

import java.util.List;

public interface IGroupDao extends IGenericRepository<Group> {
	
	public List<Group> getGroupsWithoutPermission();

}