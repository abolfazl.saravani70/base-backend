/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

//package com.rbp.core.model.mapper.security;
//
//import com.rbp.core.model.domainmodel.security.OrganizationPosition;
//import com.rbp.core.model.viewModel.security.OrganizationPositionViewModel;
//import com.rbp.sayban.model.mapper.BaseEntityMapper;
//import com.rbp.sayban.model.mapper.BaseMapperMethods;
//import com.rbp.sayban.model.mapper.GenericMapper;
//import org.mapstruct.AfterMapping;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.factory.Mappers;
//
//@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
//public interface OrganizationPositionMapper extends GenericMapper<OrganizationPositionViewModel, OrganizationPosition> {
//        OrganizationPositionMapper INSTANCE = Mappers.getMapper(OrganizationPositionMapper.class);
//
//        @Mapping(source = "parentPosition.name",target = "parentPositionName")
//        @Mapping(source = "parentPosition.id",target = "parentPositionId")
//        @Mapping(source = "parentPosition.code",target = "parentPositionCode")
//        OrganizationPositionViewModel toView(OrganizationPosition organizationPosition);
//
//        @AfterMapping
//        default void update(OrganizationPositionViewModel organizationPositionViewModel,OrganizationPosition organizationPosition){
//               OrganizationPosition p= organizationPosition.getParentPosition();
//               p.setCode(organizationPositionViewModel.getCode());
//               p.setName(organizationPositionViewModel.getName());
//        }
//}
