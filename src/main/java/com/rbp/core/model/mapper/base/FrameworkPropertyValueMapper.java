/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.base;

import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
import com.rbp.core.model.dto.base.FrameworkPropertyValueDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface FrameworkPropertyValueMapper extends GenericMapper<FrameworkPropertyValueDTO, FrameworkPropertyValue> {
    FrameworkPropertyValueMapper INSTANCE = Mappers.getMapper(FrameworkPropertyValueMapper.class);

    @Mapping(source = "frameworkProperty",target = "propertyId")
    FrameworkPropertyValueDTO toView(FrameworkPropertyValue frameWorkPropertyValue);

    @Mapping(source = "propertyId",target = "frameworkProperty")
    FrameworkPropertyValue toDomainModel(FrameworkPropertyValueDTO frameWorkPropertyValueDTO);
}
