/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.security;

import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.model.dto.security.UserDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Set;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface UserMapper extends GenericMapper<UserDTO, User> {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(source = "groups", target = "groupId")
    @Mapping(source = "stakeholder", target = "stakeholderId")
    UserDTO toView(User user);

    @Mapping(source = "groupId", target = "groups")
    @Mapping(source = "stakeholderId", target = "stakeholder")
    User toDomainModel(UserDTO userDTO);

    Set<Group> SetGroupToLong(Set<Long>  longs);
    Set<Long> LongToSetGroup(Set<Group> groups);
}
