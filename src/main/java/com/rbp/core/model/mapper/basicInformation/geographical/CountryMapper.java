/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.basicInformation.geographical;

import com.rbp.core.model.domainmodel.basicInformation.geographical.Country;
import com.rbp.core.model.dto.basicInformation.geographical.CountryDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface CountryMapper extends GenericMapper<CountryDTO, Country> {
    CountryMapper INSTANCE= Mappers.getMapper(CountryMapper.class);


}
