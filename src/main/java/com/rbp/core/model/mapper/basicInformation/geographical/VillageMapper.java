/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.basicInformation.geographical;

import com.rbp.core.model.domainmodel.basicInformation.geographical.Village;
import com.rbp.core.model.dto.basicInformation.geographical.VillageDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface VillageMapper extends GenericMapper<VillageDTO, Village> {
    VillageMapper INSTANCE= Mappers.getMapper(VillageMapper.class);

    @Mapping(source = "city",target = "cityId")
    VillageDTO toView(Village village);

    @Mapping(source = "cityId",target = "city")
    Village toDomainModel(VillageDTO villageDTO);


}
