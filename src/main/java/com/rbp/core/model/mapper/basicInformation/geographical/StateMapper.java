/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.basicInformation.geographical;

import com.rbp.core.model.domainmodel.basicInformation.geographical.State;
import com.rbp.core.model.dto.basicInformation.geographical.StateDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StateMapper extends GenericMapper<StateDTO, State> {
    StateMapper INSTANCE= Mappers.getMapper(StateMapper.class);

    @Mapping(source = "country",target = "countryId")
    StateDTO toView(State state);
    @Mapping(source = "countryId",target ="country")
    State toDomainModel(StateDTO stateDTO);
}
