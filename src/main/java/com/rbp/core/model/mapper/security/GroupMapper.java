/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.security;

import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.core.model.dto.security.GroupDTO;
import com.rbp.core.service.security.IGroupService;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class GroupMapper implements GenericMapper<GroupDTO,Group> {

    //Good to know that creating a self reference in an abstract class will cause mapstruct to
    //get inside a loop for the reference creating endless references and finally overflowing!
   // GroupMapper INSTANCE = Mappers.getMapper(GroupMapper.class);

    @Mapping(source = "permissions",target = "permissionId")
    @Mapping(source = "parentGroup",target = "parentGroupId")
    public abstract GroupDTO toView(Group group);

    @Mapping(source = "permissionId",target = "permissions")
    @Mapping(source = "parentGroupId",target = "parentGroup")
    public abstract Group toDomainModel(GroupDTO groupDTO);

    abstract Set<Group> setLongToSetGroup(Set<Long> longs);
    abstract Set<Long> setGroupToSetLong(Set<Group> groups);

    @AfterMapping
    void setGroupParentName(@MappingTarget GroupDTO groupDTO,Group group){
        Group parent=group.getParentGroup();
        groupDTO.setParentGroupName(parent.getPersianName());
    }

}
