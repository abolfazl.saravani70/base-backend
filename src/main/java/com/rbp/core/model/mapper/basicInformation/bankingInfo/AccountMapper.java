/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.basicInformation.bankingInfo;

import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Account;
import com.rbp.core.model.viewModel.basicinformation.bankingInfo.AccountViewModel;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface AccountMapper extends GenericMapper<AccountViewModel, Account> {
    AccountMapper INSTANCE= Mappers.getMapper(AccountMapper.class);

    @Mapping(source = "branch",target = "branchId")
    @Mapping(source = "branch.name", target = "branchName")
    @Mapping(source = "branch.bank.id", target = "bankId")
    @Mapping(source = "branch.bank.name", target = "bankName")
    AccountViewModel toView(Account account);

    //@Mapping(source = "branchId",target = "branch")


}
