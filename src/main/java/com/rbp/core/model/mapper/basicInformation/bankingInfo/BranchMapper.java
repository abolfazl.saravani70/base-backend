/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.basicInformation.bankingInfo;

import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Branch;
import com.rbp.core.model.dto.basicInformation.bankinginfo.BranchDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface BranchMapper extends GenericMapper<BranchDTO, Branch> {

    BranchMapper INSTANCE= Mappers.getMapper(BranchMapper.class);
    @Mapping(source = "bank",target = "bankId")
    BranchDTO toView(Branch branch);

    @Mapping(source = "bankId",target = "bank")
    Branch toDomainModel(BranchDTO branchDTO);
}
