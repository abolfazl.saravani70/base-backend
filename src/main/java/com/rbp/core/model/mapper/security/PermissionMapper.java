/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.core.model.mapper.security;

import com.rbp.core.model.domainmodel.security.Permission;
import com.rbp.core.model.dto.security.PermissionDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Set;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PermissionMapper extends GenericMapper<PermissionDTO, Permission> {
    PermissionMapper INSTANCE = Mappers.getMapper(PermissionMapper.class);

    Set<Permission> setLongToSetPermission(Set<Long> longs);
    Set<Long> setPermissionToSetLong(Set<Permission> permissions);

    Set<Permission> setPermissionDTOToSetPermission(Set<PermissionDTO> permissionDTOS);
    Set<PermissionDTO> setPermissionToSetPermissionDTO(Set<Permission> permissions);

}
