package com.rbp.core.model.viewModel.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

/**
 * this is the ViewModel that goes into UserManagementGridViewModel
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserOrganizationViewModel extends BaseEntityDTO {

    private static final long serialVersionUID = 2766691048194481006L;
    private String orgName;

    private String orgUnitName;

    private Long orgUnitId;

    private Long orgId;

    private Long userId;

    private String username;

    private String firstName;

    private String lastName;

    private Boolean isActive;

    public UserOrganizationViewModel(String org,Long orgId,Long orgUnitId,String orgUnitName ,Long userId,String username, String firstName, String lastName, Boolean isActive) {
        this.orgId=orgId;
        this.orgName = org;
        this.userId=userId;
        this.orgUnitName=orgUnitName;
        this.orgUnitId=orgUnitId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isActive = isActive;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgUnitName() {
        return orgUnitName;
    }

    public void setOrgUnitName(String orgUnitName) {
        this.orgUnitName = orgUnitName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getisActive() {
        return isActive;
    }

    public void setisActive(Boolean active) {
        isActive = active;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }
}
