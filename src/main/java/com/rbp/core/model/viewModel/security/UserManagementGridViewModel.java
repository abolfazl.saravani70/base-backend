package com.rbp.core.model.viewModel.security;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.util.ArrayList;
import java.util.List;


@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserManagementGridViewModel extends BaseEntityDTO {
	private static final long serialVersionUID = 496867091327883360L;
	List<UserOrganizationViewModel> list=new ArrayList<>();

	public List<UserOrganizationViewModel> getList() {
		return list;
	}

	public void setList(List<UserOrganizationViewModel> list) {
		this.list = list;
	}
}
