package com.rbp.core.model.viewModel.security;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.util.Set;
@Deprecated
public class PermissionViewModel extends BaseEntityDTO {

	private static final long serialVersionUID = 4716996524436292260L;

	private String title;
	
	private String src;
		
	private Set<Long> groups;

    private Long parentActionId;
    
    private String parentActionTitle;
    
    private String parentActionSrc;
    
    private String parentActionEnglishName;
    
    private String parentActionName;
    
    private String englishName;
    
    private String permissionName;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public Set<Long> getGroups() {
		return groups;
	}

	public void setGroups(Set<Long> groups) {
		this.groups = groups;
	}

	public Long getParentActionId() {
		return parentActionId;
	}

	public void setParentActionId(Long parentActionId) {
		this.parentActionId = parentActionId;
	}

	public String getParentActionTitle() {
		return parentActionTitle;
	}

	public void setParentActionTitle(String parentActionTitle) {
		this.parentActionTitle = parentActionTitle;
	}

	public String getParentActionSrc() {
		return parentActionSrc;
	}

	public void setParentActionSrc(String parentActionSrc) {
		this.parentActionSrc = parentActionSrc;
	}

	public String getParentActionEnglishName() {
		return parentActionEnglishName;
	}

	public void setParentActionEnglishName(String parentActionEnglishName) {
		this.parentActionEnglishName = parentActionEnglishName;
	}

	public String getParentActionName() {
		return parentActionName;
	}

	public void setParentActionName(String parentActionName) {
		this.parentActionName = parentActionName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
}
