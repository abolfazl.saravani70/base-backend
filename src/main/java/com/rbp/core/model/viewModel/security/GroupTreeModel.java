package com.rbp.core.model.viewModel.security;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.security.GroupDTO;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(value = JsonInclude.Include.NON_ABSENT)
public class GroupTreeModel extends BaseEntityDTO {

    private static final long serialVersionUID = 8888023959011754548L;
    GroupDTO group;
    List<GroupTreeModel> children=new ArrayList<>();
    Boolean isMember=false;//always false unless specified true
    Long parent;

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public GroupDTO getGroup() {
        return group;
    }

    public void setGroup(GroupDTO group) {
        this.group = group;
    }

    public List<GroupTreeModel> getChildren() {
        return children;
    }

    public void setChildren(List<GroupTreeModel> children) {
        this.children = children;
    }

    public Boolean getMember() {
        return isMember;
    }

    public void setMember(Boolean member) {
        isMember = member;
    }
}
