package com.rbp.core.model.viewModel.security;


import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.util.Set;
@Deprecated
public class GroupViewModel extends BaseEntityDTO {
	private static final long serialVersionUID = -8291715576898967109L;

	private String name;
	
	private String persianName;
	
	private Set<Long> permissions;
	
	private Set<Long> organizationPositions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPersianName() {
		return persianName;
	}

	public void setPersianName(String persianName) {
		this.persianName = persianName;
	}

	public Set<Long> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Long> permissions) {
		this.permissions = permissions;
	}

	public Set<Long> getOrganizationPositions() {
		return organizationPositions;
	}

	public void setOrganizationPositions(Set<Long> organizationPositions) {
		this.organizationPositions = organizationPositions;
	}
}
