package com.rbp.core.model.viewModel.security;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class UserManagementRegisterViewModel extends BaseEntityDTO {
    private static final long serialVersionUID = 2479529828686908657L;
    //user
    private long userId;
    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String expireDate;
    private String tell;
    private String mobile;
    private String kowsarNumber;
    private Boolean isActive;

    //person
    private Long nationalId;
    private LocalDateTime birthDay;

    //group
    private Set<Long> groupList=new HashSet<>();
    private Set<String> groupNames=new HashSet<>();

    //Organization
    private Long orgId;
    private String orgName;

    //OrganizationUnit
    private Long orgUnitId;
    private String orgUnitTitle;


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getTell() {
        return tell;
    }

    public void setTell(String tell) {
        this.tell = tell;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getKowsarNumber() {
        return kowsarNumber;
    }

    public void setKowsarNumber(String kowsarNumber) {
        this.kowsarNumber = kowsarNumber;
    }

    public Boolean getisActive() {
        return isActive;
    }

    public void setisActive(Boolean active) {
        isActive = active;
    }

    public Long getNationalId() {
        return nationalId;
    }

    public void setNationalId(Long nationalId) {
        this.nationalId = nationalId;
    }

    public LocalDateTime getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDateTime birthDay) {
        this.birthDay = birthDay;
    }

    public Set<Long> getGroupList() {
        return groupList;
    }

    public void setGroupList(Set<Long> groupList) {
        this.groupList = groupList;
    }

    public Set<String> getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(Set<String> groupNames) {
        this.groupNames = groupNames;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public String getOrgUnitTitle() {
        return orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle) {
        this.orgUnitTitle = orgUnitTitle;
    }

}
