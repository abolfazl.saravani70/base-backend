package com.rbp.core.model.viewModel.security;



import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.util.Set;

@Deprecated
public class OrganizationPositionViewModel extends BaseEntityDTO {
	private static final long serialVersionUID = -3917686531197733865L;

	private Long parentPositionId;
	
	private String parentPositionName;
	
	private Integer parentPositionCode;

	private String name;

	private Integer code;

	private Long pvOrganizationUnitId;
	
	private String organizationName;
	
	private Integer organizationUnitCode;

	private Set<Long> users;

	private Set<Long> groups;

	public Long getParentPositionId() {
		return parentPositionId;
	}

	public void setParentPositionId(Long parentPositionId) {
		this.parentPositionId = parentPositionId;
	}

	public String getParentPositionName() {
		return parentPositionName;
	}

	public void setParentPositionName(String parentPositionName) {
		this.parentPositionName = parentPositionName;
	}

	public Integer getParentPositionCode() {
		return parentPositionCode;
	}

	public void setParentPositionCode(Integer parentPositionCode) {
		this.parentPositionCode = parentPositionCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Long getPvOrganizationUnitId() {
		return pvOrganizationUnitId;
	}

	public void setPvOrganizationUnitId(Long pvOrganizationUnitId) {
		this.pvOrganizationUnitId = pvOrganizationUnitId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Integer getOrganizationUnitCode() {
		return organizationUnitCode;
	}

	public void setOrganizationUnitCode(Integer organizationUnitCode) {
		this.organizationUnitCode = organizationUnitCode;
	}

	public Set<Long> getUsers() {
		return users;
	}

	public void setUsers(Set<Long> users) {
		this.users = users;
	}

	public Set<Long> getGroups() {
		return groups;
	}

	public void setGroups(Set<Long> groups) {
		this.groups = groups;
	}
}
