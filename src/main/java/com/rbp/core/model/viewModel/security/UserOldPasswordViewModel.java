package com.rbp.core.model.viewModel.security;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
@Deprecated
public class UserOldPasswordViewModel extends BaseEntityDTO {
	private static final long serialVersionUID = -4210569536025684729L;

	private String oldPassword;
	
	private Long userId;
	
	private String userName;
	
	private String firstName;
	
	private String lastName;
	
	private String fullName;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return this.getFirstName()+"  "+this.getLastName() ;
	}	
}

