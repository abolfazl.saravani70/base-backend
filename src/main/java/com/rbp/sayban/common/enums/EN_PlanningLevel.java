package com.rbp.sayban.common.enums;

public enum EN_PlanningLevel {
    UNKNOWN(0),
    OBJECTIVE(1),
    STRATEGY(2),
    GOAL(3),
    PLAN(4),
    PROJECT(5),
    ACTIVITY(6),
    TASK(7),
    TODO(8);

    public static final String SERVICE = "Service";
    private final Integer key;

    EN_PlanningLevel(Integer key) {
        this.key = key;
    }

    public Integer getKey() {
        return this.key;
    }

    public static EN_PlanningLevel getByKey(int key) {
        for(EN_PlanningLevel e : values()) {
            if(e.key.equals(key)) return e;
        }
        return UNKNOWN;
    }

    public Integer getMaxLevel() {
        return TODO.getKey();
    }

    public Integer getMinLevel() {
        return OBJECTIVE.getKey();
    }

    public static EN_PlanningLevel getEnumLevel(String className) {
        switch (className) {
            case "ObjectiveTreeService":
                return OBJECTIVE;
            case "StrategyTreeService":
                return STRATEGY;
            case "GoalTreeService":
                return GOAL;
            case "PlanTreeService":
                return PLAN;
            case "ProjectTreeService":
                return PROJECT;
            case "TaskTreeService":
                return TASK;
            case "TodoTreeService":
                return TODO;
            default:
                return UNKNOWN;
        }
    }
}
