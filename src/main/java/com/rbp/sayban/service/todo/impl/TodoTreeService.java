/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.todo.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoTreeDao;
import com.rbp.sayban.model.domainmodel.todo.TodoTree;
import com.rbp.sayban.service.todo.ITodoTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoTreeService extends GenericRootService<TodoTree>
        implements ITodoTreeService {

    @Autowired
    ITodoTreeDao iTodoTreeDao;

    @Override
    protected IGenericRootRepository<TodoTree> getGenericRootRepository() {
        return iTodoTreeDao;
    }

    @Override
    protected IGenericTreeRepository<TodoTree> getGenericTreeRepository() {
        return iTodoTreeDao;
    }

    @Override
    protected IGenericRepository<TodoTree> getGenericRepository() {
        return iTodoTreeDao;
    }
}
