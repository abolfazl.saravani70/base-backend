package com.rbp.sayban.service.todo;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.todo.TodoSubjective;

public interface ITodoSubjectiveService extends IGenericService<TodoSubjective> {
}
