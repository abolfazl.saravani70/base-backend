/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.todo.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoHRDao;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoKpiDao;
import com.rbp.sayban.model.domainmodel.todo.TodoHR;
import com.rbp.sayban.model.domainmodel.todo.TodoKpi;
import com.rbp.sayban.service.todo.ITodoHRService;
import com.rbp.sayban.service.todo.ITodoKpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoHRService extends GenericService<TodoHR> implements ITodoHRService {
    @Autowired
    ITodoHRDao iTodoHRDao;

    @Override
    protected IGenericRepository<TodoHR> getGenericRepository() {
        return iTodoHRDao;
    }
}
