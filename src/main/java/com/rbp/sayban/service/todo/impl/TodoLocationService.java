/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.todo.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoBudgetDao;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoLocationDao;
import com.rbp.sayban.model.domainmodel.todo.TodoBudget;
import com.rbp.sayban.model.domainmodel.todo.TodoLocation;
import com.rbp.sayban.service.todo.ITodoBudgetService;
import com.rbp.sayban.service.todo.ITodoLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoLocationService extends GenericService<TodoLocation> implements ITodoLocationService {

    @Autowired
    ITodoLocationDao iTodoLocationDao;

    @Override
    protected IGenericRepository<TodoLocation> getGenericRepository() {
        return iTodoLocationDao;
    }
}
