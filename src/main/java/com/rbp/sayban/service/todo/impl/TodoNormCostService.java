/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.todo.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoNormCostDao;
import com.rbp.sayban.model.domainmodel.todo.TodoNormCost;
import com.rbp.sayban.service.todo.ITodoNormCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoNormCostService extends GenericService<TodoNormCost> implements ITodoNormCostService {
    @Autowired
    ITodoNormCostDao iTodoNormCostDao;

    @Override
    protected IGenericRepository<TodoNormCost> getGenericRepository() {
        return iTodoNormCostDao;
    }
}
