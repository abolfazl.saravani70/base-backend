package com.rbp.sayban.service.todo.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoDao;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoSubjectiveDao;
import com.rbp.sayban.model.domainmodel.todo.Todo;
import com.rbp.sayban.model.domainmodel.todo.TodoSubjective;
import com.rbp.sayban.service.todo.ITodoService;
import com.rbp.sayban.service.todo.ITodoSubjectiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoSubjectiveService extends GenericService<TodoSubjective> implements ITodoSubjectiveService {
    @Autowired
    ITodoSubjectiveDao iTodoSubjectiveDao;

    @Override
    protected IGenericRepository<TodoSubjective> getGenericRepository() {
        return iTodoSubjectiveDao;
    }
}
