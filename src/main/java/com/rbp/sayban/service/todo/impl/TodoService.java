/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.todo.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoDao;
import com.rbp.sayban.model.domainmodel.todo.Todo;
import com.rbp.sayban.service.todo.ITodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoService extends GenericService<Todo> implements ITodoService {
    @Autowired
    ITodoDao iTodoDao;

    @Override
    protected IGenericRepository<Todo> getGenericRepository() {
        return iTodoDao;
    }
}
