/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.task.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.task.ITaskHRDao;
import com.rbp.sayban.model.domainmodel.task.TaskHR;
import com.rbp.sayban.service.task.ITaskHRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskHRService extends GenericService<TaskHR> implements ITaskHRService {
    @Autowired
    ITaskHRDao iTaskHRDao;

    @Override
    protected IGenericRepository<TaskHR> getGenericRepository() {
        return iTaskHRDao;
    }

}
