package com.rbp.sayban.service.task;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.task.TaskSubjective;

public interface ITaskSubjectiveService extends IGenericService<TaskSubjective> {
}
