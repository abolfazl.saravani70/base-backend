/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.task.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.task.ITaskTreeDao;
import com.rbp.sayban.model.domainmodel.task.TaskTree;
import com.rbp.sayban.service.task.ITaskTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskTreeService extends GenericRootService<TaskTree>
        implements ITaskTreeService {

    @Autowired
    ITaskTreeDao iTaskTreeDao;

    @Override
    protected IGenericRootRepository<TaskTree> getGenericRootRepository() {
        return iTaskTreeDao;
    }

    @Override
    protected IGenericTreeRepository<TaskTree> getGenericTreeRepository() {
        return iTaskTreeDao;
    }

    @Override
    protected IGenericRepository<TaskTree> getGenericRepository() {
        return iTaskTreeDao;
    }
}
