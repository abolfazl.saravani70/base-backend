package com.rbp.sayban.service.task.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.task.ITaskDao;
import com.rbp.sayban.model.dao.domainModel.task.ITaskSubjectiveDao;
import com.rbp.sayban.model.domainmodel.task.Task;
import com.rbp.sayban.model.domainmodel.task.TaskSubjective;
import com.rbp.sayban.service.task.ITaskService;
import com.rbp.sayban.service.task.ITaskSubjectiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskSubjectiveService extends GenericService<TaskSubjective> implements ITaskSubjectiveService {
    @Autowired
    ITaskSubjectiveDao iTaskSubjectiveDao;

    @Override
    protected IGenericRepository<TaskSubjective> getGenericRepository() {
        return iTaskSubjectiveDao;
    }
}
