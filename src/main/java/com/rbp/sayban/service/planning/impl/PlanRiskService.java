/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.planning.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanRiskDao;
import com.rbp.sayban.model.domainmodel.planning.PlanRisk;
import com.rbp.sayban.service.planning.IPlanRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanRiskService extends GenericService<PlanRisk> implements IPlanRiskService {


    @Autowired
    IPlanRiskDao iPlanRiskDao;

    @Override
    protected IGenericRepository<PlanRisk> getGenericRepository() {
        return iPlanRiskDao;
    }


}
