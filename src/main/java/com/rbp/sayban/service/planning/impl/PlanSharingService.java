/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.planning.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanSharingDao;
import com.rbp.sayban.model.domainmodel.planning.PlanSharing;
import com.rbp.sayban.service.planning.IPlanSharingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanSharingService extends GenericService<PlanSharing> implements IPlanSharingService {


    @Autowired
    IPlanSharingDao iPlanSharingDao;

    @Override
    protected IGenericRepository<PlanSharing> getGenericRepository() {
        return iPlanSharingDao;
    }

}
