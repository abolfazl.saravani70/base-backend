/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.planning;


import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.planning.PlanNormCost;

public interface IPlanNormCostService extends IGenericService<PlanNormCost> {


}
