/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.planning.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanBudgetDao;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanHRDao;
import com.rbp.sayban.model.domainmodel.planning.PlanBudget;
import com.rbp.sayban.model.domainmodel.planning.PlanHR;
import com.rbp.sayban.service.planning.IPlanBudgetService;
import com.rbp.sayban.service.planning.IPlanHRService;
import com.rbp.sayban.service.planning.IPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanBudgetService extends GenericService<PlanBudget> implements IPlanBudgetService {


    @Autowired
    IPlanBudgetDao iPlanBudgetDao;

    @Override
    protected IGenericRepository<PlanBudget> getGenericRepository() {
        return iPlanBudgetDao;
    }


}
