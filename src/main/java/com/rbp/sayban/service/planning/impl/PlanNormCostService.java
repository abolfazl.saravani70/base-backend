/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.planning.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanNormCostDao;
import com.rbp.sayban.model.domainmodel.planning.PlanNormCost;
import com.rbp.sayban.service.planning.IPlanNormCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanNormCostService extends GenericService<PlanNormCost> implements IPlanNormCostService {


    @Autowired
    IPlanNormCostDao iPlanNormCostDao;

    @Override
    protected IGenericRepository<PlanNormCost> getGenericRepository() {
        return iPlanNormCostDao;
    }


}
