/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.planning.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanBudgetDao;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanLocationDao;
import com.rbp.sayban.model.domainmodel.planning.PlanBudget;
import com.rbp.sayban.model.domainmodel.planning.PlanLocation;
import com.rbp.sayban.service.planning.IPlanHRService;
import com.rbp.sayban.service.planning.IPlanLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanLocationService extends GenericService<PlanLocation> implements IPlanLocationService {


    @Autowired
    IPlanLocationDao iPlanLocationDao;

    @Override
    protected IGenericRepository<PlanLocation> getGenericRepository() {
        return iPlanLocationDao;
    }


}
