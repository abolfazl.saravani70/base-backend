/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.planning.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanHRDao;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanKpiDao;
import com.rbp.sayban.model.domainmodel.planning.PlanHR;
import com.rbp.sayban.model.domainmodel.planning.PlanKpi;
import com.rbp.sayban.service.planning.IPlanHRService;
import com.rbp.sayban.service.planning.IPlanKpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanHRService extends GenericService<PlanHR> implements IPlanHRService {


    @Autowired
    IPlanHRDao iPlanHRDao;

    @Override
    protected IGenericRepository<PlanHR> getGenericRepository() {
        return iPlanHRDao;
    }


}
