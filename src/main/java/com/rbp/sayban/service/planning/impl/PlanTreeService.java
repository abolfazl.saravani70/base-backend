/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.planning.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanTreeDao;
import com.rbp.sayban.model.domainmodel.planning.PlanTree;
import com.rbp.sayban.service.planning.IPlanTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanTreeService extends GenericRootService<PlanTree>
        implements IPlanTreeService {

    @Autowired
    IPlanTreeDao iPlanTreeDao;

    @Override
    protected IGenericRootRepository<PlanTree> getGenericRootRepository() {
        return iPlanTreeDao;
    }

    @Override
    protected IGenericTreeRepository<PlanTree> getGenericTreeRepository() {
        return iPlanTreeDao;
    }

    @Override
    protected IGenericRepository<PlanTree> getGenericRepository() {
        return iPlanTreeDao;
    }

}
