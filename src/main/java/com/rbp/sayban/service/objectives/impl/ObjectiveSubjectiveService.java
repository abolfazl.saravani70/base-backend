/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveSubjectiveDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveSubjective;
import com.rbp.sayban.service.objectives.IObjectiveSubjectiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveSubjectiveService extends GenericService<ObjectiveSubjective> implements IObjectiveSubjectiveService {
    @Autowired
    IObjectiveSubjectiveDao iObjectiveSubjectiveDao;

    @Override
    protected IGenericRepository<ObjectiveSubjective> getGenericRepository() {
        return iObjectiveSubjectiveDao;
    }

}
