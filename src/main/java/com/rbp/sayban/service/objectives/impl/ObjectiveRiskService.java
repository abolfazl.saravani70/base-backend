/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveRiskDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveRisk;
import com.rbp.sayban.service.objectives.IObjectiveRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveRiskService extends GenericService<ObjectiveRisk> implements IObjectiveRiskService {


    @Autowired
    IObjectiveRiskDao iObjectiveRiskDao;

    @Override
    protected IGenericRepository<ObjectiveRisk> getGenericRepository() {
        return iObjectiveRiskDao;
    }
}
