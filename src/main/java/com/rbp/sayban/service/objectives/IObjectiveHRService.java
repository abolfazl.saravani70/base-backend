package com.rbp.sayban.service.objectives;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveHR;

public interface IObjectiveHRService extends IGenericService<ObjectiveHR> {
}
