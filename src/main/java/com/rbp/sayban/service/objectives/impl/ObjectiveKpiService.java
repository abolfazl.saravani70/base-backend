/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveKpiDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveKpi;
import com.rbp.sayban.service.objectives.IObjectiveKpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveKpiService extends GenericService<ObjectiveKpi> implements IObjectiveKpiService {


    @Autowired
    IObjectiveKpiDao iObjectiveKpiDao;

    @Override
    protected IGenericRepository<ObjectiveKpi> getGenericRepository() {
        return iObjectiveKpiDao;
    }
}
