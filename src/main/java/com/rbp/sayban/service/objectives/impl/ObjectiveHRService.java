package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveHRDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveHR;
import com.rbp.sayban.service.objectives.IObjectiveHRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveHRService extends GenericService<ObjectiveHR> implements IObjectiveHRService {


    @Autowired
    IObjectiveHRDao iObjectiveHRDao;

    @Override
    protected IGenericRepository<ObjectiveHR> getGenericRepository() {
        return iObjectiveHRDao;
    }
}
