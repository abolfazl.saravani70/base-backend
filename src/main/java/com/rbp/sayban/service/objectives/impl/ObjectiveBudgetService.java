package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveBudgetDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveBudget;
import com.rbp.sayban.service.objectives.IObjectiveBudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveBudgetService extends GenericService<ObjectiveBudget> implements IObjectiveBudgetService {


    @Autowired
    IObjectiveBudgetDao iObjectiveBudgetDao;

    @Override
    protected IGenericRepository<ObjectiveBudget> getGenericRepository() {
        return iObjectiveBudgetDao;
    }
}
