package com.rbp.sayban.service.objectives;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveLocation;

public interface IObjectiveLocationService extends IGenericService<ObjectiveLocation> {
}
