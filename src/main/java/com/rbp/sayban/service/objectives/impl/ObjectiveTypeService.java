/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveTypeDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveType;
import com.rbp.sayban.service.objectives.IObjectiveTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveTypeService extends GenericService<ObjectiveType> implements IObjectiveTypeService {
    @Autowired
    IObjectiveTypeDao iObjectiveTypeDao;

    @Override
    protected IGenericRepository<ObjectiveType> getGenericRepository() {
        return iObjectiveTypeDao;
    }
}
