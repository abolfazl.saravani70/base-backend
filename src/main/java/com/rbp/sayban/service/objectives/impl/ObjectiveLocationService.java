package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveKpiDao;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveLocationDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveKpi;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveLocation;
import com.rbp.sayban.service.objectives.IObjectiveKpiService;
import com.rbp.sayban.service.objectives.IObjectiveLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveLocationService extends GenericService<ObjectiveLocation> implements IObjectiveLocationService {


    @Autowired
    IObjectiveLocationDao iObjectiveLocationDao;

    @Override
    protected IGenericRepository<ObjectiveLocation> getGenericRepository() {
        return iObjectiveLocationDao;
    }
}
