/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.objectives.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveTreeDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveTree;
import com.rbp.sayban.service.objectives.IObjectiveTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObjectiveTreeService extends GenericRootService<ObjectiveTree>
        implements IObjectiveTreeService {

    @Autowired
    IObjectiveTreeDao iObjectiveTreeDao;

    @Override
    protected IGenericRepository<ObjectiveTree> getGenericRepository() {
        return iObjectiveTreeDao;
    }

    @Override
    protected IGenericRootRepository<ObjectiveTree> getGenericRootRepository() {
        return iObjectiveTreeDao;
    }

    @Override
    protected IGenericTreeRepository<ObjectiveTree> getGenericTreeRepository() {
        return iObjectiveTreeDao;
    }
}
