package com.rbp.sayban.service.objectives;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveBudget;

public interface IObjectiveBudgetService extends IGenericService<ObjectiveBudget> {
}
