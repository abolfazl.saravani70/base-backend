/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.kpi.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.kpi.IUnitDao;
import com.rbp.sayban.model.domainmodel.kpi.Unit;
import com.rbp.sayban.service.kpi.IUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnitService extends GenericService<Unit> implements IUnitService {
    @Autowired
    IUnitDao iUnitDao;

    @Override
    protected IGenericRepository<Unit> getGenericRepository() {
        return iUnitDao;
    }
}
