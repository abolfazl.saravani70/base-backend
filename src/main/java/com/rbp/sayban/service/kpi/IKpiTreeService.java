package com.rbp.sayban.service.kpi;


import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.IGenericService;
import com.rbp.core.service.base.IGenericTreeService;
import com.rbp.sayban.model.domainmodel.kpi.KpiTree;

public interface IKpiTreeService extends IGenericTreeService {
}