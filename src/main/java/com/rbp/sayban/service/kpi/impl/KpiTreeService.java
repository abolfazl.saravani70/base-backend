package com.rbp.sayban.service.kpi.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.base.impl.GenericTreeService;
import com.rbp.sayban.model.dao.domainModel.kpi.IKpiTreeDao;
import com.rbp.sayban.model.domainmodel.kpi.KpiTree;
import com.rbp.sayban.service.kpi.IKpiTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KpiTreeService extends GenericTreeService<KpiTree> implements IKpiTreeService {

    @Autowired
    IKpiTreeDao iKpiTreeDao;

    @Override
    protected IGenericRepository<KpiTree> getGenericRepository() {
        return iKpiTreeDao;
    }

    @Override
    protected IGenericTreeRepository<KpiTree> getGenericTreeRepository() {
        return iKpiTreeDao;
    }
}