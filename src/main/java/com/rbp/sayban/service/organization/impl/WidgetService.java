/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.organization.IWidgetDao;
import com.rbp.sayban.model.domainmodel.organization.Widget;
import com.rbp.sayban.service.organization.IWidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WidgetService extends GenericService<Widget> implements IWidgetService {
    @Autowired
    IWidgetDao iWidgetDao;

    @Override
    protected IGenericRepository<Widget> getGenericRepository() {
        return iWidgetDao;
    }
}

