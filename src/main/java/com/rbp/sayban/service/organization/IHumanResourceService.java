package com.rbp.sayban.service.organization;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;

public interface IHumanResourceService extends IGenericService<HumanResource> {

}
