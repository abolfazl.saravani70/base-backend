/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.consts.organization.C_OrgChart;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.organization.Organization;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import com.rbp.sayban.service.organization.IOrganizationChartService;
import com.rbp.sayban.service.organization.IOrganizationService;
import com.rbp.sayban.service.organization.IOrganizationUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrganizationUnitService extends GenericService<OrganizationUnit> implements IOrganizationUnitService {
    @Autowired
    IOrganizationUnitDao iOrganizationUnitDao;

    @Autowired
    IOrganizationService iOrganizationService;

    @Autowired
    IOrganizationChartService iOrganizationChartService;

    @Override
    protected IGenericRepository<OrganizationUnit> getGenericRepository() {
        return iOrganizationUnitDao;
    }

    @Override
    @Transactional
    public List<OrganizationUnit> generateOuPerOrg(Long orgId, Long chartId) {

        Organization org = iOrganizationService.loadById(orgId);
        OrganizationChart orgChartMain = iOrganizationChartService.loadById(chartId);
        List<OrganizationUnit> ouForPersist = generateOu(orgChartMain, org);
        return ouForPersist;
    }

//    public List<OrganizationUnit> findByOrgId(Long orgId, int pageNumber, int pageSize) {
//        return iOrganizationUnitDao.findByOrgId(orgId, pageNumber, pageSize);
//    }

    private List<OrganizationUnit> generateOu(OrganizationChart orgChartMain, Organization org) {

        List<OrganizationUnit> ouForPersist = new ArrayList<>();

        // TODO: 8/7/2019 use Enum
        for (OrganizationChart orgChart : orgChartMain.getChildren()) {
            if(orgChart.getPvTypeId().longValue() == C_OrgChart.CHART){
                if(orgChart.getPvOrgLevelId().equals(orgChart.getPvOrgDegreeId()))
                    ouForPersist.addAll(generateOu(orgChart, org));

            }else if(orgChart.getPvTypeId().longValue() == C_OrgChart.CHART_POSITION){
                ouForPersist.add(save(createInstance(org, orgChart, false)));

            }else if(orgChart.getPvTypeId().longValue() == C_OrgChart.CHART_AND_CHART_POSITION){
                ouForPersist.add(save(createInstance(org, orgChart, false)));

                    if(orgChart.getPvOrgLevelId().equals(orgChart.getPvOrgDegreeId()))
                        ouForPersist.addAll(generateOu(orgChart, org));
                    break;
            }
        }

        return ouForPersist;
    }

    private OrganizationUnit createInstance(Organization org, OrganizationChart orgChart, Boolean isEnable) {
        OrganizationUnit ou = new OrganizationUnit();
        ou.setId(0L);
        ou.setIsEnabled(isEnable);
        ou.setOrganization(org);
        ou.setOrganizationChart(orgChart);
        return ou;
    }


}
