/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */
package com.rbp.sayban.service.organization;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;

import java.util.List;

public interface IOrganizationChartService extends IGenericService<OrganizationChart> {

    List<OrganizationChart> findAllParent(int pageNumber,int pageSize,Long parentId);
    List<OrganizationChart> findAllChildren(int pageNumber,int pageSize,Long parentId);
    void deleteOrgChart(Long id, String filter);
}
