/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.organization.IPhoneDao;
import com.rbp.sayban.model.domainmodel.organization.Phone;
import com.rbp.sayban.service.organization.IPhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneService extends GenericService<Phone> implements IPhoneService {
    @Autowired
    IPhoneDao iPhoneDao;

    @Override
    protected IGenericRepository<Phone> getGenericRepository() {
        return iPhoneDao;
    }
}
