/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization;


import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import com.rbp.sayban.model.domainmodel.organization.Person;

public interface IPersonService extends IGenericService<Person> {

}
