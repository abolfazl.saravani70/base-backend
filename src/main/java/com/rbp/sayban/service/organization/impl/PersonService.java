/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationUnitDao;
import com.rbp.sayban.model.dao.domainModel.organization.IPersonDao;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import com.rbp.sayban.model.domainmodel.organization.Person;
import com.rbp.sayban.service.organization.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService extends GenericService<Person> implements IPersonService {
    @Autowired
    IPersonDao iPersonDao;
    @Autowired
    IOrganizationUnitDao iOrganizationUnitDao;

    @Override
    protected IGenericRepository<Person> getGenericRepository() {
        return iPersonDao;
    }

}
