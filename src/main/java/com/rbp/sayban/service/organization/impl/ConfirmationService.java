/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.organization.IConfirmationDao;
import com.rbp.sayban.model.domainmodel.organization.Confirmation;
import com.rbp.sayban.service.organization.IConfirmationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmationService extends GenericService<Confirmation> implements IConfirmationService {
    @Autowired
    IConfirmationDao iConfirmationDao;

    @Override
    protected IGenericRepository<Confirmation> getGenericRepository() {
        return iConfirmationDao;
    }
}
