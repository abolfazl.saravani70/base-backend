/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */
package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.organization.IPersonDao;
import com.rbp.sayban.model.dao.domainModel.organization.IStakeholderDao;
import com.rbp.sayban.model.domainmodel.organization.Stakeholder;
import com.rbp.sayban.service.organization.IPersonService;
import com.rbp.sayban.service.organization.IStakeholderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StakeholderService extends GenericService<Stakeholder> implements IStakeholderService {
    @Autowired
    IStakeholderDao iStakeholderDao;


    @Override
    protected IGenericRepository<Stakeholder> getGenericRepository() {
        return iStakeholderDao;
    }


}