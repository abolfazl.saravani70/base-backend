package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericTreeService;
import com.rbp.sayban.model.dao.domainModel.organization.IOrgTreeDao;
import com.rbp.sayban.model.domainmodel.organization.OrgTree;
import com.rbp.sayban.service.organization.IOrgTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrgTreeService extends GenericTreeService<OrgTree> implements IOrgTreeService {

    @Autowired
    IOrgTreeDao iOrgTreeDao;

    @Override
    protected IGenericTreeRepository<OrgTree> getGenericTreeRepository() {
        return iOrgTreeDao;
    }

    @Override
    protected IGenericRepository<OrgTree> getGenericRepository() {
        return iOrgTreeDao;
    }
}
