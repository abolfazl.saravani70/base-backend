/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.geographical.IZoneService;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationDao;
import com.rbp.sayban.model.domainmodel.organization.Organization;
import com.rbp.sayban.service.organization.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class OrganizationService extends GenericService<Organization> implements IOrganizationService {

    @Autowired
    IOrganizationDao iOrganizationDao;

    @Autowired
    IStakeholderService iStakeholderService;

    @Autowired
    IZoneService iZoneService;

    @Autowired
    IAddressService iAddressService;

    @Autowired
    IEmailService iEmailService;

    @Autowired
    IPhoneService iPhoneService;

    @Autowired
    IOrganizationUnitService iOrganizationUnitService;

    @Override
    protected IGenericRepository<Organization> getGenericRepository() {
        return iOrganizationDao;
    }

    @Override
    @Transactional
    public void deleteOrg(Long id, String filter) {

        Organization org = loadById(id);

        if (org != null) {
            if(org.getOrganizations().size() == 0 || (org.getOrganizations().size() != 0 && filter.equals("true"))){
                deleteById(id);
            }else{
                throw new ApplicationException(0, "رده مورد نظر دارای زیر مجموعه است، ابتدا زیرمجموعه حذف گردد!");
            }
        } else {
            throw new ApplicationException(0, "رده مورد نظر موجود نیست.");
        }
    }
    @Override
    @Transactional
    public List<Organization> getOrgChildByOrgParentId(Long parentId){
        Organization orgParent=loadById(parentId);
        if (orgParent==null)
            throw new ApplicationException(0, "رده مورد نظر موجود نیست.");
        else {
        Set<Organization> organizations=orgParent.getOrganizations();
        List<Organization> organizationList=new ArrayList<>();
        organizationList.addAll(organizations);
        return organizationList;
        }
    }

}
