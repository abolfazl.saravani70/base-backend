package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.organization.IHumanResourceDao;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;
import com.rbp.sayban.service.organization.IHumanResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HumanResourceService extends GenericService<HumanResource> implements IHumanResourceService {
    @Autowired
    IHumanResourceDao iHumanResourceDao;

    @Override
    protected IGenericRepository<HumanResource> getGenericRepository() {
        return iHumanResourceDao;
    }
}
