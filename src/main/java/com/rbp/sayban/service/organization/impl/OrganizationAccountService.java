/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Account;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Branch;
import com.rbp.core.model.viewModel.basicinformation.bankingInfo.AccountViewModel;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.basicInformation.bankingInfo.IAccountService;
import com.rbp.core.service.basicInformation.bankingInfo.IBranchService;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationAccountDao;
import com.rbp.sayban.model.domainmodel.organization.Organization;
import com.rbp.sayban.model.domainmodel.organization.OrganizationAccount;
import com.rbp.sayban.service.organization.IOrganizationAccountService;
import com.rbp.sayban.service.organization.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrganizationAccountService extends GenericService<OrganizationAccount> implements IOrganizationAccountService {

    @Autowired
    IOrganizationAccountDao iOrganizationAccountDao;

    @Autowired
    IAccountService iAccountService;

    @Autowired
    IBranchService iBranchService;

    @Autowired
    IOrganizationService iOrganizationService;

    @Override
    protected IGenericRepository<OrganizationAccount> getGenericRepository() {
        return iOrganizationAccountDao;
    }

    @Override
    public AccountViewModel insertNewAccount(AccountViewModel entity) {
        return null;
    }

    @Override
    public List<AccountViewModel> getAccountsDetail(Long orgId) {
        return null;
    }

//    @Transactional
//    @Override
//    public AccountViewModel insertNewAccount(AccountViewModel entity) {
//
//        if (entity.getId() == 0) { // TODO: 8/6/2019 Remove this from here and use crud save
//            Account exisitAccount = iAccountService.findByNumber(entity.getNumber());
//
//            if (exisitAccount == null) {
//
//                Branch branch = iBranchService.loadById(entity.getBranchId());
//
//                Account newAcc = new Account();
//                newAcc.setId(0L);
//                newAcc.setBranch(branch);
//                newAcc.setNumber(entity.getNumber());
//
//                Account savedAcc = iAccountService.save(newAcc);
//                Organization org = iOrganizationService.getReference(entity.getOrgId());
//
//                OrganizationAccount orgAcc = new OrganizationAccount();
//                orgAcc.setId(0L);
//                orgAcc.setAccount(savedAcc);
//                orgAcc.setOrganization(org);
//                if (entity.getIsMain()){
//                    List<OrganizationAccount> organizationAccounts=org.getOrgAccount();
//                    for (OrganizationAccount organizationAccount:organizationAccounts){
//                        organizationAccount.setIsMain(false);
//                    }
//
//                }
//                orgAcc.setIsMain(entity.getIsMain());
//
//                save(orgAcc);
//
//                entity.setId(savedAcc.getId());
//                return entity;
//            } else {
//                throw new ApplicationException(0, "شماره حساب مورد نظر قبلاً ثبت شده است.");
//            }
//        } else {
//            Account account = iAccountService.loadById(entity.getId());
//            Branch branch = iBranchService.loadById(entity.getBranchId());
//            account.setNumber(entity.getNumber());
//            account.setBranch(branch);
//            iAccountService.save(account);
//            List<OrganizationAccount> organizationAccounts = iOrganizationAccountDao.findByOrgId(entity.getOrgId());
//            OrganizationAccount acc = organizationAccounts.stream()
//                    .filter(x -> x.getAccount().getId() == entity.getId())
//                    .findFirst()
//                    .get();
//            if (entity.getIsMain()){
//
//                for (OrganizationAccount organizationAccount:organizationAccounts){
//                    organizationAccount.setIsMain(false);
//                }
//
//            }
//            acc.setIsMain(entity.getIsMain());
//            iOrganizationAccountDao.save(acc);
//            return entity;
//        }
//
//    }
//
//    @Transactional
//    @Override
//    public List<AccountViewModel> getAccountsDetail(Long orgId) {
//
//        List<Object[]> records = iOrganizationAccountDao.getAccountsDetail(orgId);
//
//        List<AccountViewModel> result = new ArrayList<AccountViewModel>();
//
//        for (Object[] record : records) {
//            AccountViewModel model = new AccountViewModel();
//
//            model.setId((Long) record[0]);
//            model.setIsMain((Boolean) record[1]);
//            model.setNumber((String) record[2]);
//            model.setBranchId((Long) record[3]);
//            model.setBranchName((String) record[4]);
//            model.setBankId((Long) record[5]);
//            model.setBankName((String) record[6]);
//            model.setOrgId((Long) record[7]);
//
//            result.add(model);
//        }
//
//        return result;
//    }

}