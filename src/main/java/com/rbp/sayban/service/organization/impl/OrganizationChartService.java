/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.organization.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationChartDao;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;
import com.rbp.sayban.service.organization.IOrganizationChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrganizationChartService extends GenericService<OrganizationChart> implements IOrganizationChartService {
    @Autowired
    IOrganizationChartDao iOrganizationChartDao;

    @Override
    protected IGenericRepository<OrganizationChart> getGenericRepository() {
        return iOrganizationChartDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganizationChart> findAllParent(int pageNumber, int pageSize, Long parentId) {
        return iOrganizationChartDao.findAllParent(pageNumber, pageSize, parentId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganizationChart> findAllChildren(int pageNumber, int pageSize, Long parentId) {
        return iOrganizationChartDao.findAllChildren(pageNumber, pageSize, parentId);
    }

    @Override
    @Transactional
    public void deleteOrgChart(Long id, String filter) {

        OrganizationChart orgChart = loadById(id);

        if (orgChart != null) {
            if (orgChart.getChildren().size() == 0 || (orgChart.getChildren().size() != 0 && filter.equals("true"))) {
                deleteById(id);
            } else {
                throw new ApplicationException(0, "چارت مورد نظر دارای سمت است، ابتدا باید سمت ها حذف شود!");
            }
        } else {
            throw new ApplicationException(0, "چارت مورد نظر موجود نیست.");
        }
    }

}

