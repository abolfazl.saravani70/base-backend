/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.budgeting.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.budgeting.IFundDao;
import com.rbp.sayban.model.domainmodel.budgeting.Fund;
import com.rbp.sayban.service.budgeting.IFundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FundService extends GenericService<Fund> implements IFundService {

    @Autowired
    IFundDao iFundDao;


    @Override
    protected IGenericRepository<Fund> getGenericRepository() {
        return iFundDao;
    }
}