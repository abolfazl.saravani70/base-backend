/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.budgeting.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.budgeting.IFundSharingDao;
import com.rbp.sayban.model.domainmodel.budgeting.FundSharing;
import com.rbp.sayban.service.budgeting.IFundSharingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FundSharingService extends GenericService<FundSharing> implements IFundSharingService {

    @Autowired
    IFundSharingDao iFundSharingDao;

    @Override
    protected IGenericRepository<FundSharing> getGenericRepository() {
        return iFundSharingDao;
    }
}
