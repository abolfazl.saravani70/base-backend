package com.rbp.sayban.service.budgeting;


import com.rbp.core.service.base.IGenericService;
import com.rbp.core.service.base.IGenericTreeService;
import com.rbp.sayban.model.domainmodel.budgeting.BudgetTree;

public interface IBudgetTreeService extends IGenericTreeService {
}