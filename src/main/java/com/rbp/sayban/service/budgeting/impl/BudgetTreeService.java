package com.rbp.sayban.service.budgeting.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.base.impl.GenericTreeService;
import com.rbp.sayban.model.dao.domainModel.budgeting.IBudgetTreeDao;
import com.rbp.sayban.model.domainmodel.budgeting.BudgetTree;
import com.rbp.sayban.service.budgeting.IBudgetTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BudgetTreeService extends GenericTreeService<BudgetTree> implements IBudgetTreeService {

    @Autowired
    IBudgetTreeDao iBudgetTreeDao;

    @Override
    protected IGenericRepository<BudgetTree> getGenericRepository() {
        return iBudgetTreeDao;
    }

    @Override
    protected IGenericTreeRepository<BudgetTree> getGenericTreeRepository() {
        return iBudgetTreeDao;
    }
}