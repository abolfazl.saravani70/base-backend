/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.goal.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalDao;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.service.goal.IGoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoalService extends GenericService<Goal> implements IGoalService {


    @Autowired
    IGoalDao iGoalDao;

    @Override
    protected IGenericRepository<Goal> getGenericRepository() {
        return iGoalDao;
    }
}
