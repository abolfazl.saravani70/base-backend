package com.rbp.sayban.service.goal.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalBudgetDao;
import com.rbp.sayban.model.domainmodel.goal.GoalBudget;
import com.rbp.sayban.service.goal.IGoalBudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoalBudgetService extends GenericService<GoalBudget> implements IGoalBudgetService {

    @Autowired
    IGoalBudgetDao iGoalBudgetDao;

    @Override
    protected IGenericRepository<GoalBudget> getGenericRepository() {
        return iGoalBudgetDao;
    }
}
