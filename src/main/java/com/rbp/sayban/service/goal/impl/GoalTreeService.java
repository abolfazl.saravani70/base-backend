/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.goal.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalTreeDao;
import com.rbp.sayban.model.domainmodel.goal.GoalTree;
import com.rbp.sayban.service.goal.IGoalTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoalTreeService extends GenericRootService<GoalTree>
        implements IGoalTreeService {

    @Autowired
    IGoalTreeDao iGoalTreeDao;

    @Override
    protected IGenericRepository<GoalTree> getGenericRepository() {
        return iGoalTreeDao;
    }


    @Override
    protected IGenericRootRepository<GoalTree> getGenericRootRepository() {
        return iGoalTreeDao;
    }

    @Override
    protected IGenericTreeRepository<GoalTree> getGenericTreeRepository() {
        return iGoalTreeDao;
    }
}
