package com.rbp.sayban.service.goal.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalKpiDao;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalLocationDao;
import com.rbp.sayban.model.domainmodel.goal.GoalKpi;
import com.rbp.sayban.model.domainmodel.goal.GoalLocation;
import com.rbp.sayban.service.goal.IGoalKpiService;
import com.rbp.sayban.service.goal.IGoalLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoalLocationService  extends GenericService<GoalLocation> implements IGoalLocationService {

    @Autowired
    IGoalLocationDao iGoalLocationDao;

    @Override
    protected IGenericRepository<GoalLocation> getGenericRepository() {
        return iGoalLocationDao;
    }
}
