package com.rbp.sayban.service.goal.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalHRDao;
import com.rbp.sayban.model.domainmodel.goal.GoalHR;
import com.rbp.sayban.service.goal.IGoalHRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoalHRService extends GenericService<GoalHR> implements IGoalHRService {

    @Autowired
    IGoalHRDao iGoalHRDao;

    @Override
    protected IGenericRepository<GoalHR> getGenericRepository() {
        return iGoalHRDao;
    }
}
