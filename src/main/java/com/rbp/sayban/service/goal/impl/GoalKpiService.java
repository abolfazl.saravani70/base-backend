/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.goal.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalKpiDao;
import com.rbp.sayban.model.domainmodel.goal.GoalKpi;
import com.rbp.sayban.service.goal.IGoalKpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class GoalKpiService  extends GenericService<GoalKpi> implements IGoalKpiService {
    @Autowired
    IGoalKpiDao iGoalKpiDao;

    @Override
    protected IGenericRepository<GoalKpi> getGenericRepository() {
        return iGoalKpiDao;
    }
}
