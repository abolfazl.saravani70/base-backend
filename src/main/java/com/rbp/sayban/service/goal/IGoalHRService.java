package com.rbp.sayban.service.goal;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.goal.GoalHR;

public interface IGoalHRService extends IGenericService<GoalHR> {
}
