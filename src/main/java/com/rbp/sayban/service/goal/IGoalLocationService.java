package com.rbp.sayban.service.goal;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.goal.GoalLocation;

public interface IGoalLocationService extends IGenericService<GoalLocation> {
}
