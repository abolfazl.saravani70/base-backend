/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.goal;

import com.rbp.core.service.base.IGenericRootService;
import com.rbp.sayban.model.domainmodel.goal.GoalTree;

public interface IGoalTreeService extends IGenericRootService<GoalTree> {
}
