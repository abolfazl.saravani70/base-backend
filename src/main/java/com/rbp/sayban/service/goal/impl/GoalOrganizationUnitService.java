/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.goal.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.goal.GoalOrganizationUnit;
import com.rbp.sayban.service.goal.IGoalOrganizationUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoalOrganizationUnitService  extends GenericService<GoalOrganizationUnit> implements IGoalOrganizationUnitService {
    @Autowired
    IGoalOrganizationUnitDao iGoalOrganizationUnitDao;

    @Override
    protected IGenericRepository<GoalOrganizationUnit> getGenericRepository() {
        return iGoalOrganizationUnitDao;
    }
}
