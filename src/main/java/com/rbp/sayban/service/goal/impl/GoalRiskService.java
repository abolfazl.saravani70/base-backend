/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.goal.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalRiskDao;
import com.rbp.sayban.model.domainmodel.goal.GoalRisk;
import com.rbp.sayban.service.goal.IGoalRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class GoalRiskService  extends GenericService<GoalRisk> implements IGoalRiskService {
    @Autowired
    IGoalRiskDao iGoalRiskDao;

    @Override
    protected IGenericRepository<GoalRisk> getGenericRepository() {
        return iGoalRiskDao;
    }
}
