/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.risk.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.risk.ISolutionDao;
import com.rbp.sayban.model.domainmodel.risk.Solution;
import com.rbp.sayban.service.risk.ISolutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SolutionService extends GenericService<Solution> implements ISolutionService {
    @Autowired
    ISolutionDao iSolutionDao;

    @Override
    protected IGenericRepository<Solution> getGenericRepository() {
        return iSolutionDao;
    }
}
