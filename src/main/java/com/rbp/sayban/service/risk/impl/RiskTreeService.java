package com.rbp.sayban.service.risk.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.core.service.base.impl.GenericTreeService;
import com.rbp.sayban.model.dao.domainModel.risk.IRiskTreeDao;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;
import com.rbp.sayban.service.risk.IRiskTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiskTreeService extends GenericTreeService<RiskTree> implements IRiskTreeService {

    @Autowired
    IRiskTreeDao iRiskTreeDao;

    @Override
    protected IGenericRepository<RiskTree> getGenericRepository() {
        return iRiskTreeDao;
    }

    @Override
    protected IGenericTreeRepository<RiskTree> getGenericTreeRepository() {
        return iRiskTreeDao;
    }
}