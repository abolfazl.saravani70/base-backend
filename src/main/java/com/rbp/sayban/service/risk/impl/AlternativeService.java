/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.risk.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.risk.IAlternativeDao;
import com.rbp.sayban.model.domainmodel.risk.Alternative;
import com.rbp.sayban.service.risk.IAlternativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlternativeService extends GenericService<Alternative> implements IAlternativeService {

    @Autowired
    IAlternativeDao iAlternativeDao;

    @Override
    protected IGenericRepository<Alternative> getGenericRepository() {
        return iAlternativeDao;
    }

}
