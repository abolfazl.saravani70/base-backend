/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.risk.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.risk.IRiskDao;
import com.rbp.sayban.model.domainmodel.risk.Risk;
import com.rbp.sayban.service.risk.IRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiskService extends GenericService<Risk> implements IRiskService {
    @Autowired
    IRiskDao iRiskDao;

    @Override
    protected IGenericRepository<Risk> getGenericRepository() {
        return iRiskDao;
    }
}
