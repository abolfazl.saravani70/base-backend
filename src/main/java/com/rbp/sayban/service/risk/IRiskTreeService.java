package com.rbp.sayban.service.risk;


import com.rbp.core.service.base.IGenericService;
import com.rbp.core.service.base.IGenericTreeService;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;

public interface IRiskTreeService extends IGenericTreeService {
}