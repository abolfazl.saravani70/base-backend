package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.project.IProjectHRDao;
import com.rbp.sayban.model.domainmodel.project.ProjectHR;
import com.rbp.sayban.service.project.IProjectHRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectHRService extends GenericService<ProjectHR> implements IProjectHRService {


    @Autowired
    IProjectHRDao iProjectHRDao;

    @Override
    protected IGenericRepository<ProjectHR> getGenericRepository() {
        return iProjectHRDao;
    }
}
