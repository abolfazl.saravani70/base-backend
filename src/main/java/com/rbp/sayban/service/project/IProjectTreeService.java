/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.project;

import com.rbp.core.service.base.IGenericRootService;
import com.rbp.sayban.model.domainmodel.project.ProjectTree;

public interface IProjectTreeService extends IGenericRootService<ProjectTree> {
}
