package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.project.IProjectLocationDao;
import com.rbp.sayban.model.domainmodel.project.ProjectLocation;
import com.rbp.sayban.service.project.IProjectLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectLocationService extends GenericService<ProjectLocation> implements IProjectLocationService {


    @Autowired
    IProjectLocationDao iProjectLocationDao;

    @Override
    protected IGenericRepository<ProjectLocation> getGenericRepository() {
        return iProjectLocationDao;
    }
}
