/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.project.IProjectNormCostDao;
import com.rbp.sayban.model.domainmodel.project.ProjectNormCost;
import com.rbp.sayban.service.project.IProjectNormCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectNormCostService extends GenericService<ProjectNormCost> implements IProjectNormCostService {


    @Autowired
    IProjectNormCostDao iProjectNormCostDao;

    @Override
    protected IGenericRepository<ProjectNormCost> getGenericRepository() {
        return iProjectNormCostDao;
    }


}
