/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.project.IProjectProjectResourceDao;
import com.rbp.sayban.model.domainmodel.project.ProjectProjectResource;
import com.rbp.sayban.service.project.IProjectProjectResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectProjectResourceService extends GenericService<ProjectProjectResource> implements IProjectProjectResourceService {


    @Autowired
    IProjectProjectResourceDao iProjectProjectResourceDao;
    @Override
    protected IGenericRepository<ProjectProjectResource> getGenericRepository() {
        return iProjectProjectResourceDao;
    }


}
