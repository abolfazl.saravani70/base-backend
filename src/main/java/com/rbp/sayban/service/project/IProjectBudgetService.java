package com.rbp.sayban.service.project;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.project.ProjectBudget;

public interface IProjectBudgetService extends IGenericService<ProjectBudget> {
}
