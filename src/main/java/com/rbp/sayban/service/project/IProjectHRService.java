package com.rbp.sayban.service.project;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.project.ProjectHR;

public interface IProjectHRService extends IGenericService<ProjectHR> {
}
