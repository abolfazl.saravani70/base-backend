package com.rbp.sayban.service.project;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.project.ProjectSubjective;

public interface IProjectSubjectiveService extends IGenericService<ProjectSubjective> {
}
