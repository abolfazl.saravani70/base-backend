package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.project.IProjectSubjectiveDao;
import com.rbp.sayban.model.domainmodel.project.ProjectSubjective;
import com.rbp.sayban.service.project.IProjectSubjectiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectSubjectiveService extends GenericService<ProjectSubjective> implements IProjectSubjectiveService {


    @Autowired
    IProjectSubjectiveDao iProjectSubjectiveDao;

    @Override
    protected IGenericRepository<ProjectSubjective> getGenericRepository() {
        return iProjectSubjectiveDao;
    }
}
