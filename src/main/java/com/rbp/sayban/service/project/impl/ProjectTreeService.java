/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.project.IProjectTreeDao;
import com.rbp.sayban.model.domainmodel.project.ProjectTree;
import com.rbp.sayban.service.project.IProjectTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectTreeService extends GenericRootService<ProjectTree>
        implements IProjectTreeService {

    @Autowired
    IProjectTreeDao iProjectTreeDao;

    @Override
    protected IGenericRootRepository<ProjectTree> getGenericRootRepository() {
        return iProjectTreeDao;
    }

    @Override
    protected IGenericTreeRepository<ProjectTree> getGenericTreeRepository() {
        return iProjectTreeDao;
    }

    @Override
    protected IGenericRepository<ProjectTree> getGenericRepository() {
        return iProjectTreeDao;
    }



}
