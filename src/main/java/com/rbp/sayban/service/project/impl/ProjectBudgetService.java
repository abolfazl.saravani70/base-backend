package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.project.IProjectBudgetDao;
import com.rbp.sayban.model.dao.domainModel.project.IProjectKpiDao;
import com.rbp.sayban.model.domainmodel.project.ProjectBudget;
import com.rbp.sayban.model.domainmodel.project.ProjectKpi;
import com.rbp.sayban.service.project.IProjectBudgetService;
import com.rbp.sayban.service.project.IProjectKpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectBudgetService extends GenericService<ProjectBudget> implements IProjectBudgetService {


    @Autowired
    IProjectBudgetDao iProjectBudgetDao;

    @Override
    protected IGenericRepository<ProjectBudget> getGenericRepository() {
        return iProjectBudgetDao;
    }
}
