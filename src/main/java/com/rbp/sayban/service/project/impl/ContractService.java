/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.project.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.project.IContractDao;
import com.rbp.sayban.model.domainmodel.project.Contract;
import com.rbp.sayban.service.project.IContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractService extends GenericService<Contract> implements IContractService {


    @Autowired
    IContractDao iContractDao;

    @Override
    protected IGenericRepository<Contract> getGenericRepository() {
        return iContractDao;
    }


}
