/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.rule.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.rule.IPlanningTypeRuleDao;
import com.rbp.sayban.model.domainmodel.rule.PlanningTypeRule;
import com.rbp.sayban.service.rule.IPlanningTypeRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanningTypeRuleService extends GenericService<PlanningTypeRule> implements IPlanningTypeRuleService {
    @Autowired
    IPlanningTypeRuleDao iPlanningTypeRuleDao;

    @Override
    protected IGenericRepository<PlanningTypeRule> getGenericRepository() {
        return iPlanningTypeRuleDao;
    }
}
