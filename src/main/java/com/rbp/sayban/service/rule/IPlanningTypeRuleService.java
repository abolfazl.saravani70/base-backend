/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.rule;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.rule.PlanningTypeRule;

public interface IPlanningTypeRuleService extends IGenericService<PlanningTypeRule> {
}
