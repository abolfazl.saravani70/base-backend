package com.rbp.sayban.service.subjective.Impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericTreeService;
import com.rbp.sayban.model.dao.domainModel.risk.IRiskTreeDao;
import com.rbp.sayban.model.dao.domainModel.subjective.ISubjectiveTreeDao;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;
import com.rbp.sayban.model.domainmodel.subjective.SubjectiveTree;
import com.rbp.sayban.service.risk.IRiskTreeService;
import com.rbp.sayban.service.subjective.ISubjectiveTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubjectiveTreeService extends GenericTreeService<SubjectiveTree> implements ISubjectiveTreeService {

    @Autowired
    ISubjectiveTreeDao iSubjectiveTreeDao;

    @Override
    protected IGenericRepository<SubjectiveTree> getGenericRepository() {
        return iSubjectiveTreeDao;
    }

    @Override
    protected IGenericTreeRepository<SubjectiveTree> getGenericTreeRepository() {
        return iSubjectiveTreeDao;
    }
}