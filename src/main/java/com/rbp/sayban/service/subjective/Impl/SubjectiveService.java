/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.subjective.Impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;

import com.rbp.sayban.model.dao.domainModel.subjective.ISubjectiveDao;
import com.rbp.sayban.model.domainmodel.subjective.Subjective;
import com.rbp.sayban.service.subjective.ISubjectiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubjectiveService extends GenericService<Subjective> implements ISubjectiveService {
    @Autowired
    ISubjectiveDao iSubjectiveDao;


    @Override
    protected IGenericRepository<Subjective> getGenericRepository() {
        return iSubjectiveDao;
    }
}
