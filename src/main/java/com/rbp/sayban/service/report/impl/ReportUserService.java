/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.report.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.report.IReportUserDao;
import com.rbp.sayban.model.domainmodel.report.ReportUser;
import com.rbp.sayban.service.report.IReportUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportUserService extends GenericService<ReportUser> implements IReportUserService {
    @Autowired
    IReportUserDao iReportUserDao;

    @Override
    protected IGenericRepository<ReportUser> getGenericRepository() {
        return iReportUserDao;
    }
}
