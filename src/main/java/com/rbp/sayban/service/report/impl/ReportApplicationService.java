/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.report.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.report.IReportApplicationDao;
import com.rbp.sayban.model.domainmodel.report.ReportApplication;
import com.rbp.sayban.service.report.IReportApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportApplicationService extends GenericService<ReportApplication> implements IReportApplicationService {
    @Autowired
    IReportApplicationDao iReportApplicationDao;

    @Override
    protected IGenericRepository<ReportApplication> getGenericRepository() {
        return iReportApplicationDao;
    }
}
