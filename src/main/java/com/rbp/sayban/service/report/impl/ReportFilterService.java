/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.report.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.report.IReportFilterDao;
import com.rbp.sayban.model.domainmodel.report.ReportFilter;
import com.rbp.sayban.service.report.IReportFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportFilterService extends GenericService<ReportFilter> implements IReportFilterService {
    @Autowired
    IReportFilterDao iReportFilterDao;

    @Override
    protected IGenericRepository<ReportFilter> getGenericRepository() {
        return iReportFilterDao;
    }
}