/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.report.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.report.IFilterOperatorDao;
import com.rbp.sayban.model.domainmodel.report.FilterOperator;
import com.rbp.sayban.service.report.IFilterOperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FilterOperatorService extends GenericService<FilterOperator> implements IFilterOperatorService {
    @Autowired
    IFilterOperatorDao iFilterOperatorDao;

    @Override
    protected IGenericRepository<FilterOperator> getGenericRepository() {
        return iFilterOperatorDao;
    }
}
