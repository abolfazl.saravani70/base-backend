/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.report.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.report.ISystemReportFilterDao;
import com.rbp.sayban.model.domainmodel.report.SystemReportFilter;
import com.rbp.sayban.service.report.ISystemReportFilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemReportFilterService extends GenericService<SystemReportFilter> implements ISystemReportFilterService {
    @Autowired
    ISystemReportFilterDao iSystemReportFilterDao;

    @Override
    protected IGenericRepository<SystemReportFilter> getGenericRepository() {
        return iSystemReportFilterDao;
    }
}
