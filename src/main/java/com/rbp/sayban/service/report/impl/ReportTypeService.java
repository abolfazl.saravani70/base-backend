/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.report.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.report.IReportTypeDao;
import com.rbp.sayban.model.domainmodel.report.ReportType;
import com.rbp.sayban.service.report.IReportTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportTypeService extends GenericService<ReportType> implements IReportTypeService {
    @Autowired
    IReportTypeDao iReportTypeDao;

    @Override
    protected IGenericRepository<ReportType> getGenericRepository() {
        return iReportTypeDao;
    }
}
