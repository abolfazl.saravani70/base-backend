/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.report.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.report.IReportEntityDao;
import com.rbp.sayban.model.domainmodel.report.ReportEntity;
import com.rbp.sayban.service.report.IReportEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportEntityService extends GenericService<ReportEntity> implements IReportEntityService {
    @Autowired
    IReportEntityDao iReportEntityDao;

    @Override
    protected IGenericRepository<ReportEntity> getGenericRepository() {
        return iReportEntityDao;
    }
}