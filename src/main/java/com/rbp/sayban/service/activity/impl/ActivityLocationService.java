package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityLocationDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityLocation;
import com.rbp.sayban.service.activity.IActivityLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityLocationService extends GenericService<ActivityLocation> implements IActivityLocationService {

    @Autowired
    IActivityLocationDao iActivityLocation;

    @Override
    protected IGenericRepository<ActivityLocation> getGenericRepository() {
        return iActivityLocation;
    }

}
