package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IPerformanceDao;
import com.rbp.sayban.model.domainmodel.dep.Performance;
import com.rbp.sayban.service.activity.IPerformanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerformanceService extends GenericService<Performance>implements IPerformanceService {
    @Autowired
    IPerformanceDao iPerformanceDao;
    @Override
    protected IGenericRepository<Performance> getGenericRepository() {
        return iPerformanceDao;
    }
}
