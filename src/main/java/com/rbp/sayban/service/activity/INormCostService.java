package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.norm.NormCost;

public interface INormCostService extends IGenericService<NormCost> {
}
