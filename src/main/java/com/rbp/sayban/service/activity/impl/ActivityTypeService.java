package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTypeDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityType;
import com.rbp.sayban.service.activity.IActivityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityTypeService extends GenericService<ActivityType>implements IActivityTypeService {
    @Autowired
    IActivityTypeDao iActivityTypeDao;
    @Override
    protected IGenericRepository<ActivityType> getGenericRepository() {
        return iActivityTypeDao;
    }
}
