package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActSharingDao;
import com.rbp.sayban.model.domainmodel.dep.ActSharing;
import com.rbp.sayban.service.activity.IActSharingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActSharingService extends GenericService<ActSharing> implements IActSharingService {
    @Autowired
    IActSharingDao iActSharingDao;
    @Override
    protected IGenericRepository<ActSharing> getGenericRepository() {
        return iActSharingDao;
    }
}
