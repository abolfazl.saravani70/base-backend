package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTemplateDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplate;
import com.rbp.sayban.service.activity.IActivityTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityTemplateService extends GenericService<ActivityTemplate> implements IActivityTemplateService {

    @Autowired
    IActivityTemplateDao iActivityTemplateDao;

    @Override
    protected IGenericRepository<ActivityTemplate> getGenericRepository() {
        return iActivityTemplateDao;
    }
}
