package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.dep.ActivityExpenditure;

public interface IActivityExpenditureService extends IGenericService<ActivityExpenditure> {
}
