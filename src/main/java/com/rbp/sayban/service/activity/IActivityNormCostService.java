package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.activity.ActivityNormCost;

public interface IActivityNormCostService extends IGenericService<ActivityNormCost> {
}
