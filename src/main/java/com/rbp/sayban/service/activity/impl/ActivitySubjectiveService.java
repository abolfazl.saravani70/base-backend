package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivitySubjectiveDao;
import com.rbp.sayban.model.domainmodel.activity.ActivitySubjective;
import com.rbp.sayban.service.activity.IActivitySubjectiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivitySubjectiveService extends GenericService<ActivitySubjective> implements IActivitySubjectiveService {

    @Autowired
    IActivitySubjectiveDao iActivitySubjectiveDao;

    @Override
    protected IGenericRepository<ActivitySubjective> getGenericRepository() {
        return iActivitySubjectiveDao;
    }
}
