/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTreeDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityTree;
import com.rbp.sayban.service.activity.IActivityTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityTreeService extends GenericRootService<ActivityTree>
        implements IActivityTreeService {

    @Autowired
    IActivityTreeDao iActivityTreeDao;


    @Override
    protected IGenericRepository<ActivityTree> getGenericRepository() {
        return iActivityTreeDao;
    }

    @Override
    protected IGenericRootRepository<ActivityTree> getGenericRootRepository() {
        return iActivityTreeDao;
    }

    @Override
    protected IGenericTreeRepository<ActivityTree> getGenericTreeRepository() {
        return iActivityTreeDao;
    }
}
