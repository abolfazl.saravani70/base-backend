package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.INormCostOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.dep.NormCostOrganizationUnit;
import com.rbp.sayban.service.activity.INormCostOrganizationUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NormCostOrganizationUnitService extends GenericService<NormCostOrganizationUnit> implements INormCostOrganizationUnitService {
    @Autowired
    INormCostOrganizationUnitDao inormCostOrganizationUnitDao;
    @Override
    protected IGenericRepository<NormCostOrganizationUnit> getGenericRepository() {
        return inormCostOrganizationUnitDao;
    }


}
