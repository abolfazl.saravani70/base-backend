package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IPaymentDao;
import com.rbp.sayban.model.domainmodel.dep.Payment;
import com.rbp.sayban.service.activity.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService extends GenericService<Payment>implements IPaymentService {
    @Autowired
    IPaymentDao iPaymentDao;
    @Override
    protected IGenericRepository<Payment> getGenericRepository() {
        return iPaymentDao;
    }
}
