package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityRiskDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityRisk;
import com.rbp.sayban.service.activity.IActivityRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityRiskService extends GenericService<ActivityRisk> implements IActivityRiskService {
    @Autowired
    IActivityRiskDao iActivityRiskDao;
    @Override
    protected IGenericRepository<ActivityRisk> getGenericRepository() {
        return iActivityRiskDao;
    }
}
