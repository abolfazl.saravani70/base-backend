package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IPaidCreditDao;
import com.rbp.sayban.model.domainmodel.dep.PaidCredit;
import com.rbp.sayban.service.activity.IPaidCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaidCreditService extends GenericService<PaidCredit>implements IPaidCreditService {
    @Autowired
    IPaidCreditDao iPaidCreditDao;
    @Override
    protected IGenericRepository<PaidCredit> getGenericRepository() {
        return iPaidCreditDao;
    }
}
