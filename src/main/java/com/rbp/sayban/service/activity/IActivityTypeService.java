package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.activity.ActivityType;

public interface IActivityTypeService extends IGenericService<ActivityType> {
}
