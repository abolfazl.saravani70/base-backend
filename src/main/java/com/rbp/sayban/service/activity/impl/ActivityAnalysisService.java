package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityAnalysisDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityAnalysis;
import com.rbp.sayban.service.activity.IActivityAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityAnalysisService  extends GenericService<ActivityAnalysis> implements IActivityAnalysisService {
    @Autowired
    IActivityAnalysisDao iActivityAnalysisDao;
    @Override
    protected IGenericRepository<ActivityAnalysis> getGenericRepository() {
        return iActivityAnalysisDao;
    }
}
