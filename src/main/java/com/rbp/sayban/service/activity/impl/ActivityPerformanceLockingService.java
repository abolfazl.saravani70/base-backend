package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityPerformanceLockingDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityPerformanceLocking;
import com.rbp.sayban.service.activity.IActivityPerformanceLockingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityPerformanceLockingService extends GenericService<ActivityPerformanceLocking>implements IActivityPerformanceLockingService {
    @Autowired
    IActivityPerformanceLockingDao iActivityPerformanceLockingDao;
    @Override
    protected IGenericRepository<ActivityPerformanceLocking> getGenericRepository() {
        return iActivityPerformanceLockingDao;
    }
}
