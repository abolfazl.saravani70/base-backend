package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.norm.NormParameter;

public interface INormParameterService extends IGenericService<NormParameter> {
}
