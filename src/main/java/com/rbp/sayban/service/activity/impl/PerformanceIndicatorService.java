package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IPerformanceIndicatorDao;
import com.rbp.sayban.model.domainmodel.dep.PerformanceIndicator;
import com.rbp.sayban.service.activity.IPerformanceIndicatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerformanceIndicatorService extends GenericService<PerformanceIndicator>implements IPerformanceIndicatorService {
    @Autowired
    IPerformanceIndicatorDao iPerformanceIndicatorDao;
    @Override
    protected IGenericRepository<PerformanceIndicator> getGenericRepository() {
        return iPerformanceIndicatorDao;
    }
}
