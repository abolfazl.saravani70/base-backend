package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.INormCostDao;
import com.rbp.sayban.model.domainmodel.norm.NormCost;
import com.rbp.sayban.service.activity.INormCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NormCostService extends GenericService<NormCost>implements INormCostService {
    @Autowired
    INormCostDao iNormCostDao;
    @Override
    protected IGenericRepository<NormCost> getGenericRepository() {
        return iNormCostDao;
    }
}
