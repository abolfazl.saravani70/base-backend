package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IScalarIndicatorDao;
import com.rbp.sayban.model.domainmodel.dep.ScalarIndicator;
import com.rbp.sayban.service.activity.IScalarIndicatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScalarIndicatorService extends GenericService<ScalarIndicator>implements IScalarIndicatorService {
    @Autowired
    IScalarIndicatorDao iScalarIndicatorDao;
    @Override
    protected IGenericRepository<ScalarIndicator> getGenericRepository() {
        return iScalarIndicatorDao;
    }
}
