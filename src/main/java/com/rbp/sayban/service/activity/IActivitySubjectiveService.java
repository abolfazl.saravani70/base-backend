package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.activity.ActivitySubjective;

public interface IActivitySubjectiveService extends IGenericService<ActivitySubjective> {
}
