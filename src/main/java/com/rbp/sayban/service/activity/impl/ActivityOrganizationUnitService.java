package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.service.activity.IActivityOrganizationUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityOrganizationUnitService extends GenericService<ActivityOrganizationUnit>implements IActivityOrganizationUnitService {
    @Autowired
    IActivityOrganizationUnitDao activityOrganizationUnitDao;
    @Override
    protected IGenericRepository<ActivityOrganizationUnit> getGenericRepository() {
        return activityOrganizationUnitDao;
    }
}
