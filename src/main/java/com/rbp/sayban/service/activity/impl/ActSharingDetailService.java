package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActSharingDetailDao;
import com.rbp.sayban.model.domainmodel.dep.ActSharingDetail;
import com.rbp.sayban.service.activity.IActSharingDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActSharingDetailService extends GenericService<ActSharingDetail> implements IActSharingDetailsService {
    @Autowired
    IActSharingDetailDao iActSharingDetailDao;
    @Override
    protected IGenericRepository<ActSharingDetail> getGenericRepository() {
        return iActSharingDetailDao;
    }
}
