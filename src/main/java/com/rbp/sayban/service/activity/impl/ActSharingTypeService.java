package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActSharingTypeDao;
import com.rbp.sayban.model.domainmodel.dep.ActSharingType;
import com.rbp.sayban.service.activity.IActSharingTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActSharingTypeService extends GenericService<ActSharingType>implements IActSharingTypeService {
    @Autowired
    IActSharingTypeDao iActSharingTypeDao;
    @Override
    protected IGenericRepository<ActSharingType> getGenericRepository() {
        return iActSharingTypeDao;
    }
}
