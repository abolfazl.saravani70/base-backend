package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityQualityDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityQuality;
import com.rbp.sayban.service.activity.IActivityQualityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityQualityService extends GenericService<ActivityQuality>implements IActivityQualityService {
    @Autowired
    IActivityQualityDao iActivityQualityDao;
    @Override
    protected IGenericRepository<ActivityQuality> getGenericRepository() {
        return iActivityQualityDao;
    }
}
