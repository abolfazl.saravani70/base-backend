package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityNormCostDao;
import com.rbp.sayban.model.dao.domainModel.activity.impl.ActivityNormCostDaoImpl;
import com.rbp.sayban.model.domainmodel.activity.ActivityNormCost;
import com.rbp.sayban.service.activity.IActivityNormCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityNormCostService extends GenericService<ActivityNormCost>implements IActivityNormCostService {
    @Autowired
    IActivityNormCostDao iActivityNormCostDao;

    @Override
    protected IGenericRepository<ActivityNormCost> getGenericRepository() {
        return iActivityNormCostDao;
    }
}
