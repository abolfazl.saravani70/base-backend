package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IQulitiveIndicatorDao;
import com.rbp.sayban.model.domainmodel.dep.QualitativeIndicator;
import com.rbp.sayban.service.activity.IQualitativeIndicatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QualitativeIndicatorService extends GenericService<QualitativeIndicator>implements IQualitativeIndicatorService {
@Autowired
    IQulitiveIndicatorDao iQulitiveIndicatorDao;
    @Override

    protected IGenericRepository<QualitativeIndicator> getGenericRepository() {
        return iQulitiveIndicatorDao;
    }
}
