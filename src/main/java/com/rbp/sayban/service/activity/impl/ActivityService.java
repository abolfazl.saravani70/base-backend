package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityDao;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import com.rbp.sayban.service.activity.IActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService extends GenericService<Activity> implements IActivityService {

    @Autowired
    IActivityDao iActivityDao;

    @Override
    protected IGenericRepository<Activity> getGenericRepository() {
        return iActivityDao;
    }
}