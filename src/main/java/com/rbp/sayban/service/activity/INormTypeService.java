package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.norm.NormType;

public interface INormTypeService extends IGenericService<NormType> {
}
