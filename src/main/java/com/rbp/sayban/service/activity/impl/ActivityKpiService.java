package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityKpiDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityKpi;
import com.rbp.sayban.service.activity.IActivityKpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityKpiService extends GenericService<ActivityKpi>implements IActivityKpiService {

    @Autowired
    IActivityKpiDao iactivityKpiDao;
    @Override
    protected IGenericRepository<ActivityKpi> getGenericRepository() {
        return iactivityKpiDao;
    }
}
