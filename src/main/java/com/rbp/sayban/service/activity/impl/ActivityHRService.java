package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityHRDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityHR;
import com.rbp.sayban.service.activity.IActivityHRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityHRService extends GenericService<ActivityHR> implements IActivityHRService {

    @Autowired
    IActivityHRDao iActivityHRDao;

    @Override
    protected IGenericRepository<ActivityHR> getGenericRepository() {
        return iActivityHRDao;
    }


}
