package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityPerformanceDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityPerformance;
import com.rbp.sayban.service.activity.IActivityPerformanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityPerformanceService extends GenericService<ActivityPerformance>implements IActivityPerformanceService {
    @Autowired
    IActivityPerformanceDao iActivityPerformanceDao;
    @Override
    protected IGenericRepository<ActivityPerformance> getGenericRepository() {
        return iActivityPerformanceDao;
    }
}
