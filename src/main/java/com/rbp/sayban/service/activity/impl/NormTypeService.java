package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.INormTypeDao;
import com.rbp.sayban.model.domainmodel.norm.NormType;
import com.rbp.sayban.service.activity.INormTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NormTypeService extends GenericService<NormType>implements INormTypeService {
    @Autowired
    INormTypeDao iNormTypeDao;
    @Override
    protected IGenericRepository<NormType> getGenericRepository() {
        return iNormTypeDao;
    }
}
