package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplate;

public interface IActivityTemplateService extends IGenericService<ActivityTemplate> {
}
