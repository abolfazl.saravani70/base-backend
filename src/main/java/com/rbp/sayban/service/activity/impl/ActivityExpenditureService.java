package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityExpenditureDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityExpenditure;
import com.rbp.sayban.service.activity.IActivityExpenditureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityExpenditureService extends GenericService<ActivityExpenditure> implements IActivityExpenditureService {
    @Autowired
    IActivityExpenditureDao iActivityExpenditureDao;
    @Override
    protected IGenericRepository<ActivityExpenditure> getGenericRepository() {
        return iActivityExpenditureDao;
    }
}
