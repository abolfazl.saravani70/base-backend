package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTemplateNormCostDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateNormCost;
import com.rbp.sayban.service.activity.IActivityTemplateNormCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityTemplateNormCostService extends GenericService<ActivityTemplateNormCost>implements IActivityTemplateNormCostService {
    @Autowired
    IActivityTemplateNormCostDao activityTemplateNormCostDao;
    @Override
    protected IGenericRepository<ActivityTemplateNormCost> getGenericRepository() {
        return activityTemplateNormCostDao;
    }
}
