package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTemplateTypeDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateType;
import com.rbp.sayban.service.activity.IActivityTemplateTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityTemplateTypeService extends GenericService<ActivityTemplateType>implements IActivityTemplateTypeService {
    @Autowired
    IActivityTemplateTypeDao iActivityTemplateTypeDao;
    @Override
    protected IGenericRepository<ActivityTemplateType> getGenericRepository() {
        return iActivityTemplateTypeDao;
    }
}
