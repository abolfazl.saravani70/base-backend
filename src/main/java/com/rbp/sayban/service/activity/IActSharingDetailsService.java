package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.dep.ActSharingDetail;

public interface IActSharingDetailsService extends IGenericService<ActSharingDetail> {
}
