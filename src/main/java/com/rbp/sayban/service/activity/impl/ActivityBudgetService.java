package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityBudgetDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityBudget;
import com.rbp.sayban.service.activity.IActivityBudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityBudgetService  extends GenericService<ActivityBudget> implements IActivityBudgetService {

    @Autowired
    IActivityBudgetDao iActivityBudgetDao;

    @Override
    protected IGenericRepository<ActivityBudget> getGenericRepository() {
        return iActivityBudgetDao;
    }
}
