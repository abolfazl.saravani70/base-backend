package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityCostDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityCost;
import com.rbp.sayban.service.activity.IActivityCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityCostService extends GenericService<ActivityCost>implements IActivityCostService {

    @Autowired
    IActivityCostDao iActivityCostDao;

    @Override
    protected IGenericRepository<ActivityCost> getGenericRepository() {
        return iActivityCostDao;
    }
}
