package com.rbp.sayban.service.activity;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.dep.ActSharingType;

public interface IActSharingTypeService extends IGenericService<ActSharingType> {
}
