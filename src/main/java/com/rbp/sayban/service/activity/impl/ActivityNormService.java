package com.rbp.sayban.service.activity.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityNormDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityNorm;
import com.rbp.sayban.service.activity.IActivityNormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityNormService extends GenericService<ActivityNorm>implements IActivityNormService {
    @Autowired
    IActivityNormDao iActivityNormDao;
    @Override
    protected IGenericRepository<ActivityNorm> getGenericRepository() {
        return iActivityNormDao;
    }
}
