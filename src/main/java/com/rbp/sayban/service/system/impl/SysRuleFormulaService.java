/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.system.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.system.ISysRuleFormulaDao;
import com.rbp.sayban.model.domainmodel.system.SysRuleFormula;
import com.rbp.sayban.service.system.ISysRuleFormulaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysRuleFormulaService extends GenericService<SysRuleFormula> implements ISysRuleFormulaService {
    @Autowired
    ISysRuleFormulaDao iSysRuleFormulaDao;

    @Override
    protected IGenericRepository<SysRuleFormula> getGenericRepository() {
        return iSysRuleFormulaDao;
    }
}
