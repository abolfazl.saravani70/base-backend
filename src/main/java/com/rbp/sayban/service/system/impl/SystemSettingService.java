/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.system.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.system.ISystemSettingDao;
import com.rbp.sayban.model.dao.domainModel.system.impl.SystemSettingDaoImpl;
import com.rbp.sayban.model.domainmodel.system.SystemSetting;
import com.rbp.sayban.service.system.ISystemSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemSettingService extends GenericService<SystemSetting> implements ISystemSettingService {
    @Autowired
    ISystemSettingDao iSystemSettingDao;

    @Override
    protected IGenericRepository<SystemSetting> getGenericRepository() {
        return iSystemSettingDao;
    }
}