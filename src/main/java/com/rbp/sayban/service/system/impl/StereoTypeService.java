/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.system.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.system.IStereoTypeDao;
import com.rbp.sayban.model.dao.domainModel.system.impl.StereoTypeDaoImpl;
import com.rbp.sayban.model.domainmodel.system.StereoType;
import com.rbp.sayban.service.system.IStereoTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StereoTypeService extends GenericService<StereoType> implements IStereoTypeService {
    @Autowired
    IStereoTypeDao iStereoTypeDao;

    @Override
    protected IGenericRepository<StereoType> getGenericRepository() {
        return iStereoTypeDao;
    }
}