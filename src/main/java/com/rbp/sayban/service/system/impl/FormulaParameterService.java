/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.system.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.system.IFormulaParameterDao;
import com.rbp.sayban.model.dao.domainModel.system.impl.FormulaParameterDaoImpl;
import com.rbp.sayban.model.domainmodel.system.FormulaParameter;
import com.rbp.sayban.service.system.IFormulaParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormulaParameterService extends GenericService<FormulaParameter> implements IFormulaParameterService {
    @Autowired
    IFormulaParameterDao iFormulaParameterDao;

    @Override
    protected IGenericRepository<FormulaParameter> getGenericRepository() {
        return iFormulaParameterDao;
    }
}