/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.system;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.system.Request;

public interface IRequestService extends IGenericService<Request> {
}
