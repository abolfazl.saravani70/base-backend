/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.system.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.system.ISystemDao;
import com.rbp.sayban.model.dao.domainModel.system.impl.SystemDaoImpl;
import com.rbp.sayban.model.domainmodel.system.System;
import com.rbp.sayban.service.system.ISystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemService extends GenericService<System> implements ISystemService {
    @Autowired
    ISystemDao iSystemDao;

    @Override
    protected IGenericRepository<System> getGenericRepository() {
        return iSystemDao;
    }
}