package com.rbp.sayban.service.system.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.system.ISystemKPIDao;
import com.rbp.sayban.model.domainmodel.system.SystemKpi;
import com.rbp.sayban.service.system.ISystemKpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemKpiService extends GenericService<SystemKpi> implements ISystemKpiService {
    @Autowired
    ISystemKPIDao iSystemKPIDao;

    @Override
    protected IGenericRepository<SystemKpi> getGenericRepository() {
        return iSystemKPIDao;
    }
}