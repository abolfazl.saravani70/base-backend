/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.system.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.system.IDocumentDao;
import com.rbp.sayban.model.dao.domainModel.system.impl.DocumentDaoImpl;
import com.rbp.sayban.model.domainmodel.system.Document;
import com.rbp.sayban.service.system.IDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentService extends GenericService<Document> implements IDocumentService {
    @Autowired
    IDocumentDao iDocumentDao;

    @Override
    protected IGenericRepository<Document> getGenericRepository() {
        return iDocumentDao;
    }
}