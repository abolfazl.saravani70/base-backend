/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.strategies.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyTypeDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyType;
import com.rbp.sayban.service.strategies.IStrategyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyTypeService extends GenericService<StrategyType> implements IStrategyTypeService {
    @Autowired
    IStrategyTypeDao iStrategyTypeDao;

    @Override
    protected IGenericRepository<StrategyType> getGenericRepository() {
        return iStrategyTypeDao;
    }
}
