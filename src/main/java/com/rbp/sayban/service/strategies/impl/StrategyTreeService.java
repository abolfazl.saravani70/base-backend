/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.service.strategies.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.core.service.base.impl.GenericRootService;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyTreeDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyTree;
import com.rbp.sayban.service.strategies.IStrategyTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyTreeService extends GenericRootService<StrategyTree>
        implements IStrategyTreeService {

    @Autowired
    IStrategyTreeDao iStrategyTreeDao;

    @Override
    protected IGenericRepository<StrategyTree> getGenericRepository() {
        return iStrategyTreeDao;
    }

    @Override
    protected IGenericRootRepository<StrategyTree> getGenericRootRepository() {
        return iStrategyTreeDao;
    }

    @Override
    protected IGenericTreeRepository<StrategyTree> getGenericTreeRepository() {
        return iStrategyTreeDao;
    }

}
