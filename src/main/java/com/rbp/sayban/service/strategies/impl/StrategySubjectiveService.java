/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.strategies.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategySubjectiveDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategySubjective;
import com.rbp.sayban.service.strategies.IStrategySubjectiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class StrategySubjectiveService extends GenericService<StrategySubjective> implements IStrategySubjectiveService {

    @Autowired
    IStrategySubjectiveDao iStrategySubjectiveDao;

    @Override
    protected IGenericRepository<StrategySubjective> getGenericRepository() {
        return iStrategySubjectiveDao;
    }



}
