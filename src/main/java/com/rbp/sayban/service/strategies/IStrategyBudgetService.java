package com.rbp.sayban.service.strategies;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.strategies.StrategyBudget;

public interface IStrategyBudgetService extends IGenericService<StrategyBudget> {
}
