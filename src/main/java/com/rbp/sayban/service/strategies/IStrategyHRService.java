package com.rbp.sayban.service.strategies;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.strategies.StrategyHR;

public interface IStrategyHRService extends IGenericService<StrategyHR> {
}
