/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.strategies.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyOrganizationUnit;
import com.rbp.sayban.service.strategies.IStrategyOrganizationUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyOrganizationUnitService extends GenericService<StrategyOrganizationUnit> implements IStrategyOrganizationUnitService {


    @Autowired
    IStrategyOrganizationUnitDao iStrategyOrganizationUnitDao;

    @Override
    protected IGenericRepository<StrategyOrganizationUnit> getGenericRepository() {
        return iStrategyOrganizationUnitDao;
    }
}
