/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.strategies.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyExecutionTypeDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyExecutionType;
import com.rbp.sayban.service.strategies.IStrategyExecutionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyExecutionTypeService extends GenericService<StrategyExecutionType> implements IStrategyExecutionTypeService {
    @Autowired
    IStrategyExecutionTypeDao iStrategyExecutionTypeDao;

    @Override
    protected IGenericRepository<StrategyExecutionType> getGenericRepository() {
        return iStrategyExecutionTypeDao;
    }
}