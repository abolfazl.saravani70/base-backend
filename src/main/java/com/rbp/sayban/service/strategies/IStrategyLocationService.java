package com.rbp.sayban.service.strategies;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.strategies.StrategyLocation;

public interface IStrategyLocationService extends IGenericService<StrategyLocation> {
}
