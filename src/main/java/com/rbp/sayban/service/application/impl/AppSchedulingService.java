/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.application.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.application.IAppSchedulingDao;
import com.rbp.sayban.model.domainmodel.application.AppScheduling;
import com.rbp.sayban.service.application.IAppSchedulingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppSchedulingService extends GenericService<AppScheduling> implements IAppSchedulingService {
    @Autowired
    IAppSchedulingDao iAppSchedulingDao;

    @Override
    protected IGenericRepository<AppScheduling> getGenericRepository() {
        return iAppSchedulingDao;
    }
}

