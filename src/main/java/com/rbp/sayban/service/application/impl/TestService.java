/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.application.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.application.ITestDao;
import com.rbp.sayban.model.domainmodel.application.Test;
import com.rbp.sayban.service.application.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService extends GenericService<Test> implements ITestService {

    @Autowired
    ITestDao iTestDao;

    @Override
    protected IGenericRepository<Test> getGenericRepository() {
        return iTestDao;
    }
}
