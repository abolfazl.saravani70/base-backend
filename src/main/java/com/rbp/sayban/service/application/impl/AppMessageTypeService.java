/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.application.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.application.IAppMessageTypeDao;
import com.rbp.sayban.model.domainmodel.application.AppMessageType;
import com.rbp.sayban.service.application.IAppMessageTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppMessageTypeService extends GenericService<AppMessageType> implements IAppMessageTypeService {

    @Autowired
    IAppMessageTypeDao iAppMessageTypeDao;

    @Override
    protected IGenericRepository<AppMessageType> getGenericRepository() {
        return iAppMessageTypeDao;
    }
}
