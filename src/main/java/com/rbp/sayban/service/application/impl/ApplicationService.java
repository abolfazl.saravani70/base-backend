/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.application.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.application.IApplicationDao;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.service.application.IApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationService extends GenericService<Application> implements IApplicationService {

    @Autowired
    IApplicationDao iApplicationDao;

    @Override
    protected IGenericRepository<Application> getGenericRepository() {
        return iApplicationDao;
    }
}
