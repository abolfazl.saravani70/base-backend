/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.service.application.impl;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.application.IStoreProcedureDao;
import com.rbp.sayban.model.domainmodel.application.StoreProcedure;
import com.rbp.sayban.service.application.IStoreProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreProcedureService extends GenericService<StoreProcedure> implements IStoreProcedureService {

    @Autowired
    IStoreProcedureDao iStoreProcedureDao;

    @Override
    protected IGenericRepository<StoreProcedure> getGenericRepository() {
        return iStoreProcedureDao;
    }
}
