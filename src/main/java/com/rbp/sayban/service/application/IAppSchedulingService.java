/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.service.application;

import com.rbp.core.service.base.IGenericService;
import com.rbp.sayban.model.domainmodel.application.AppScheduling;

public interface IAppSchedulingService extends IGenericService<AppScheduling> {
}