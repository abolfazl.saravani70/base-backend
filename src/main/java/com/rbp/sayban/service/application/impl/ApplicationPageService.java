/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.service.application.impl;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.service.base.impl.GenericService;
import com.rbp.sayban.model.dao.domainModel.application.IApplicationPageDao;
import com.rbp.sayban.model.domainmodel.application.ApplicationPage;
import com.rbp.sayban.service.application.IApplicationPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationPageService extends GenericService<ApplicationPage> implements IApplicationPageService {

    @Autowired
    IApplicationPageDao iApplicationPageDao;

    @Override
    protected IGenericRepository<ApplicationPage> getGenericRepository() {
        return iApplicationPageDao;
    }
}
