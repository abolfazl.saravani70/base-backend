package com.rbp.sayban;

import com.rbp.core.common.config.spring.springConfig;
import com.rbp.core.common.config.swagger.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.DispatcherServlet;

@EntityScan( basePackages = {"com.rbp"} )
@Import({SwaggerConfig.class, springConfig.class})
@ComponentScan(basePackages = { "com.rbp.core","com.rbp.sayban"} )
@EnableCaching //Required annotation for caching.
@SpringBootApplication
public class SaybanApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {

		SpringApplication app = new SpringApplication(SaybanApplication.class);
		//app.setBannerMode(Banner.Mode.OFF);
		ApplicationContext ctx =app.run(args);
		DispatcherServlet dispatcherServlet = (DispatcherServlet)ctx.getBean("dispatcherServlet");
		dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
	}

}

//@SpringBootApplication
//@EntityScan( basePackages = {"com.rbp"} )
//@Import({SwaggerConfigOld.class, springConfig.class, springSecurity.class})
//@ComponentScan(basePackages = { "com.rbp.core","com.rbp.sayban"} )
//@EnableCaching //Required annotation for caching.
//public class SaybanApplication extends SpringBootServletInitializer {
//
//	@Bean
//	public ExitCodeGenerator exitCodeGenerator() {
//		return () -> 3000;
//	}
//	public static void main(String[] args) {
//
//		SpringApplication app = new SpringApplication(SaybanApplication.class);
//		app.setBannerMode(Banner.Mode.OFF);
//		ApplicationContext ctx =app.run(args);
//		DispatcherServlet dispatcherServlet = (DispatcherServlet)ctx.getBean("dispatcherServlet");
//		dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
//	}

//	@Bean
//	public EngineConfigurationConfigurer<SpringProcessEngineConfiguration> customProcessEngineConfigurer() {
//		return engineConfiguration -> {
//			engineConfiguration.setValidateFlowable5EntitiesEnabled(false);
//		};
//	}
//}
