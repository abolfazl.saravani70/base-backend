/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.consts.organization;

public interface C_OrgChart {
    long CHART = 22330047;
    long CHART_POSITION = 22330048;
    long CHART_AND_CHART_POSITION = 22330049;
}
