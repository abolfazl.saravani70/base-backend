/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.norm;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;

import javax.persistence.*;

@Entity
@Table(name = "PLN$Cost")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Cost", allocationSize = 1)
public class Cost extends BaseEntity {

    private static final long serialVersionUID = -5261522441004654227L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FK_PV_CENTRALIZE_TYPE_ID")
    private Long pvCentralizeTypeId; // متمرکز، غیر متمرکز

    @Column(name = "FK_PV_CASH_TYPE_ID")
    private Long pvCashTypeId; // نقدی، غیر نقدی

    @Column(name = "FK_PV_ORG_LEVEL_ID")
    private Long pvOrgLevelId;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_ID")
//    private OrganizationUnit organizationUnit; // معاونتی که نقش پشتیبانی دارد

    @Column(name = "FK_ORG_UNIT_ID")
    private Long organizationUnitId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPvCentralizeTypeId() {
        return pvCentralizeTypeId;
    }

    public void setPvCentralizeTypeId(Long pvCentralizeTypeId) {
        this.pvCentralizeTypeId = pvCentralizeTypeId;
    }

    public Long getPvCashTypeId() {
        return pvCashTypeId;
    }

    public void setPvCashTypeId(Long pvCashTypeId) {
        this.pvCashTypeId = pvCashTypeId;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public Long getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Long organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }
}
