/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
@Table(name = "APP$ApplicationKpi")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ApplicationKpi", allocationSize = 1)
public class ApplicationKpi extends BaseEntity {
    private static final long serialVersionUID = -2978343039745714263L;

    @Column(name = "APP_KPI_NAME")
    private String appKpiName;

    @Column(name = "APP_KPI_KEY")
    private Integer appKpiKey;

    @Column(name = "APP_KPI_CODE")
    private String appKpiCode;

    @Column(name = "APP_KPI_VALUE")
    private String appKpiValue;

    @Column(name = "APP_KPI_BEST_VALUE")
    private String appKpiBestValue;

    @Column(name = "APP_KPI_PRIORITY")
    private Integer appKpiPriority;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APPLICATION_ID")
    private Application application;

    public String getAppKpiName() {
        return appKpiName;
    }

    public void setAppKpiName(String appKpiName) {
        this.appKpiName = appKpiName;
    }

    public Integer getAppKpiKey() {
        return appKpiKey;
    }

    public void setAppKpiKey(Integer appKpiKey) {
        this.appKpiKey = appKpiKey;
    }

    public String getAppKpiCode() {
        return appKpiCode;
    }

    public void setAppKpiCode(String appKpiCode) {
        this.appKpiCode = appKpiCode;
    }

    public String getAppKpiValue() {
        return appKpiValue;
    }

    public void setAppKpiValue(String appKpiValue) {
        this.appKpiValue = appKpiValue;
    }

    public String getAppKpiBestValue() {
        return appKpiBestValue;
    }

    public void setAppKpiBestValue(String appKpiBestValue) {
        this.appKpiBestValue = appKpiBestValue;
    }

    public Integer getAppKpiPriority() {
        return appKpiPriority;
    }

    public void setAppKpiPriority(Integer appKpiPriority) {
        this.appKpiPriority = appKpiPriority;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
