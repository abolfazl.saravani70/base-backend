/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.KpiJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "FIN$ObjectiveKpi")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ObjectiveKpi", allocationSize = 1)
public class ObjectiveKpi extends KpiJunctionBaseEntity {

    private static final long serialVersionUID = -2041579857834477084L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_OBJECTIVE_ID")
    private Objective objective;

    public Objective getObjective() {
        return objective;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }
}
