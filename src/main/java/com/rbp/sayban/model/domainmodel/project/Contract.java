/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.project;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.Document;

import javax.persistence.*;

@Entity
@Table(name = "PRJ$Contract")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Contract", allocationSize = 1)
public class Contract extends BaseEntity {

    private static final long serialVersionUID = -6752521027867158998L;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "CONTRACTOR")
    private String contractor;

    @Column(name = "CONTRACTEE")
    private String contractee;

    @Column(name = "NUMBER_OF_SECTION")
    private Long numberOfSection;

    @Column(name = "HAS_ATTACHMENT")
    private Boolean isAttachment;

    @Column(name = "IS_COMPLEMENT")
    private Boolean isComplement;

    @Column(name = "DOCUMENT")
    private Document document;

    @Column(name = "NUMBER_COPY")
    private Long numberCopy;

    @Column(name = "VERSION_OF_CONTRACT")
    private Long versionOfContract;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_BASE_CONTRACT_ID")
    private Contract baseContract;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PROJECT_ID")
    private Project project;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getContractee() {
        return contractee;
    }

    public void setContractee(String contractee) {
        this.contractee = contractee;
    }

    public Long getNumberOfSection() {
        return numberOfSection;
    }

    public void setNumberOfSection(Long numberOfSection) {
        this.numberOfSection = numberOfSection;
    }

    public Boolean getIsAttachment() {
        return isAttachment;
    }

    public void setIsAttachment(Boolean attachment) {
        this.isAttachment = attachment;
    }

    public Boolean getIsComplement() {
        return isComplement;
    }

    public void setIsComplement(Boolean complement) {
        this.isComplement = complement;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Long getNumberCopy() {
        return numberCopy;
    }

    public void setNumberCopy(Long numberCopy) {
        this.numberCopy = numberCopy;
    }

    public Long getVersionOfContract() {
        return versionOfContract;
    }

    public void setVersionOfContract(Long versionOfContract) {
        this.versionOfContract = versionOfContract;
    }

    public Contract getBaseContract() {
        return baseContract;
    }

    public void setBaseContract(Contract baseContract) {
        this.baseContract = baseContract;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
