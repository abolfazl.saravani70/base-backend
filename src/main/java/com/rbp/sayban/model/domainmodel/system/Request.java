/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;

import javax.persistence.*;

@Entity
@Table(name = "SYS$Request")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Request", allocationSize = 1)
public class Request extends BaseEntity {

    private static final long serialVersionUID = -8345387093976691366L;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "FK_REQUEST_TYPE")
    private Long pvRequestType;

    @Column(name = "NUMBER$")
    private String number;

//    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST} )
//    @JoinColumn(name = "FK_OU_FROM_ID")
//    private OrganizationUnit from;
//
//    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST})
//    @JoinColumn(name = "FK_OU_TO_ID")
//    private OrganizationUnit to;

    @Column(name = "FK_OU_FROM_ID")
    private Long orgUnitIdFrom;

    @Column(name = "FK_OU_TO_ID")
    private Long orgUnitIdTo;

    @Column(name = "FK_PV_REASON_ID")
    private Long pvReasonId;

    @Column(name = "FK_PV_REJECT_REASON_ID")
    private Long pvRejectReasonId;

    @Column(name = "FK_PV_STATE_ID")
    private Long pvStateId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_REQUESTER_ID")
    private Requester requester;

    @Column(name = "BUSINESS_KEY")
    private String businessKey;

    @Column(name = "PROCESS_INSTANCE_ID")
    private String  processInstanceId;




    }