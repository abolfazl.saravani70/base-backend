/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "FIN$Credit")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Credit", allocationSize = 1)
public class Credit extends BaseEntity {

    private static final long serialVersionUID = 3682560226813468873L;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_FUND_SHARING_ID")
    private FundSharing fundSharing;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ALLOCATION_ID")
    private Allocation allocation;

    public FundSharing getFundSharing() {
        return fundSharing;
    }

    public void setFundSharing(FundSharing fundSharing) {
        this.fundSharing = fundSharing;
    }

    public Allocation getAllocation() {
        return allocation;
    }

    public void setAllocation(Allocation allocation) {
        this.allocation = allocation;
    }
}
