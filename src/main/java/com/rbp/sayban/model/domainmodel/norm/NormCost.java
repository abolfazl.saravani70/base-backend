/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.norm;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$NormCost")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_NormCost", allocationSize = 1)
public class NormCost extends BaseEntity {

    private static final long serialVersionUID = -7834080877228583293L;

    @Column(name ="NAME" )
    private String name;

    @Column (name="TITLE")
    private String title;
    
    @Column(name="COST")
    private Long cost;

    @Column(name="RATE")
    private Long rate;

    @Column (name="PRIORITORY")
    private Integer prioritory;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public Integer getPrioritory() {
        return prioritory;
    }

    public void setPrioritory(Integer prioritory) {
        this.prioritory = prioritory;
    }

}
