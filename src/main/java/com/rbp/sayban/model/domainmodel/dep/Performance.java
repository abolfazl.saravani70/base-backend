/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$Performance")
public class Performance extends BaseEntity {
    private static final long serialVersionUID = 5377045404510913702L;

    @Column(name="NAME")
    private String name;

    @Column(name="TITLE")
    private String title;

    @Column(name = "KEYWORD")
    private String keyword;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "FK_PV_PERFORMANCE_TYPE_ID")
    private Long pvPerformanceTypeId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getPvPerformanceTypeId() {
        return pvPerformanceTypeId;
    }

    public void setPvPerformanceTypeId(Long pvPerformanceTypeId) {
        this.pvPerformanceTypeId = pvPerformanceTypeId;
    }
}
