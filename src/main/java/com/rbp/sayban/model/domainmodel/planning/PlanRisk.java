/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.RiskJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$PlanRisk")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_PlanRisk", allocationSize = 1)
public class PlanRisk extends RiskJunctionBaseEntity {

    private static final long serialVersionUID = -89532576840426188L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_PLAN_ID")
    private Plan plan;

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }
}
