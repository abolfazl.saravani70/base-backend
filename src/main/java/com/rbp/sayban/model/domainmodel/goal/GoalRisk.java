/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.RiskJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "GOL$GoalRisk")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_GoalRisk", allocationSize = 1)
public class GoalRisk extends RiskJunctionBaseEntity {

    private static final long serialVersionUID = -8610269853860740252L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_GOAL_ID")
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }
}
