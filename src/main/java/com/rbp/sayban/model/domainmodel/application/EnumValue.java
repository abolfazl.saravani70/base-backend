/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.application;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "APP$EnumValue")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_EnumValue", allocationSize = 1)
public class EnumValue extends StereotypeBaseEntity {
    private static final long serialVersionUID = -5150440447490101584L;

    @Column(name = "IS_OBSOLETE")
    private Boolean isObsolete;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APP_ENUM_ID")
    private AppEnum appEnum;

    public Boolean getIsObsolete() {
        return isObsolete;
    }

    public void setIsObsolete(Boolean obsolete) {
        this.isObsolete = obsolete;
    }

    public AppEnum getAppEnum() {
        return appEnum;
    }

    public void setAppEnum(AppEnum appEnum) {
        this.appEnum = appEnum;
    }
}
