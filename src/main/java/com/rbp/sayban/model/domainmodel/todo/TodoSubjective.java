/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.SubjectiveJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TDO$TodoSubjective")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TodoSubjective", allocationSize = 1)
public class TodoSubjective extends SubjectiveJunctionBaseEntity {

    private static final long serialVersionUID = 1685270499116360088L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_TODO_ID")
    private Todo todo;

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }
}
