/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.dep.ActivityPerformance;
import com.rbp.sayban.model.domainmodel.task.Task;
import com.rbp.sayban.model.domainmodel.todo.Todo;

import javax.persistence.*;

@Entity
@Table(name = "FIN$Expenditure")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Expenditure", allocationSize = 1)
public class Expenditure extends BaseEntity {

    private static final long serialVersionUID = -7812583267425970611L;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ACTIVITY_PERFORMANCE_ID")
//    private ActivityPerformance activityPerformance;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_TASK_ID")
    private Task task;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_TODO_ID")
    private Todo todo;

//    public ActivityPerformance getActivityPerformance() {
//        return activityPerformance;
//    }
//
//    public void setActivityPerformance(ActivityPerformance activityPerformance) {
//        this.activityPerformance = activityPerformance;
//    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }
}
