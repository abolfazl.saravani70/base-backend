package com.rbp.sayban.model.domainmodel.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TDO$Todo_HR")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TodoHR", allocationSize = 1)
public class TodoHR extends HRJunctionBaseEntity {

    private static final long serialVersionUID = -7540581418451276910L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_TODO_ID")
    private Todo todo;

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }
}
