/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.Organization;
import com.rbp.sayban.model.domainmodel.organization.Signing;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "SYS$License")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_License", allocationSize = 1)
public class License extends BaseEntity {

    private static final long serialVersionUID = -511516273238326176L;

    @Column(name = "EXPIRED_DATE")
    private LocalDate expiredDate;

    @Column(name = "LICENSE_DATE")
    private LocalDate date;

    @Column(name = "FK_SUBJECT_ID")
    private String subject;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "FK_PV_LICENSE_TYPE_ID")
    private Long pvLicenseTypeId;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_ID")
//    private Organization organization;

    @Column(name = "FK_ORG_ID")
    private Long organizationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SIGNING_ID")
    private Signing signing;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_DOCUMENT_ID")
    private Document document;

    public LocalDate getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(LocalDate expiredDate) {
        this.expiredDate = expiredDate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getPvLicenseTypeId() {
        return pvLicenseTypeId;
    }

    public void setPvLicenseTypeId(Long pvLicenseTypeId) {
        this.pvLicenseTypeId = pvLicenseTypeId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Signing getSigning() {
        return signing;
    }

    public void setSigning(Signing signing) {
        this.signing = signing;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
