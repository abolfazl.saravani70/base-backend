/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.RiskJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TDO$TodoRisk")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TodoRisk", allocationSize = 1)
public class TodoRisk extends RiskJunctionBaseEntity {

    private static final long serialVersionUID = 1209289990910064214L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_TODO_ID")
    private Todo todo;

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }
}
