/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "FIN$Fund")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Fund", allocationSize = 1)
public class Fund extends BaseEntity {

    private static final long serialVersionUID = 9221183602242607286L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_BUDGET_RESOURCES_ID")
    private BudgetResource budgetResource;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_FUNDING_ID")
    private Fund fund;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_POLICY_ID")
    private Policy policy;

    public BudgetResource getBudgetResource() {
        return budgetResource;
    }

    public void setBudgetResource(BudgetResource budgetResource) {
        this.budgetResource = budgetResource;
    }

    public Fund getFund() {
        return fund;
    }

    public void setFund(Fund fund) {
        this.fund = fund;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }
}
