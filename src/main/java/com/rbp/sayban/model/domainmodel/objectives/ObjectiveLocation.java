package com.rbp.sayban.model.domainmodel.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "FIN$ObjectiveLocation")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ObjectiveLocation", allocationSize = 1)
public class ObjectiveLocation extends LocationJunctionBaseEntity {

    private static final long serialVersionUID = 4831099815635924068L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_OBJECTIVE_ID")
    private Objective objective;

    public Objective getObjective() {
        return objective;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }
}
