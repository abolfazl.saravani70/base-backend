/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "SYS$Letter")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Letter", allocationSize = 1)
public class Letter extends BaseEntity {

    private static final long serialVersionUID = 7759111330649502712L;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "LETTER_FROM")
    private String letterFrom;

    @Column(name = "LETTER_TO")
    private String letterTo;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "TEMPLATE")
    private String template;

    @Column(name = "KEY_WORD")
    private String keyWord;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_DOCUMENT_ID")
    private Document document;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFrom() {
        return letterFrom;
    }

    public void setFrom(String from) {
        this.letterFrom = from;
    }

    public String getTo() {
        return letterTo;
    }

    public void setTo(String to) {
        this.letterTo = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
