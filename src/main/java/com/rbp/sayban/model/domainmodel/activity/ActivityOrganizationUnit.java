/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.OrganizationUnitJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ActivityOrgUnit")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ActivityOrgUnit", allocationSize = 1)
public class ActivityOrganizationUnit extends OrganizationUnitJunctionBaseEntity {

    private static final long serialVersionUID = -206895265298191774L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
