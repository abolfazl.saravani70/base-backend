/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.KpiJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "GOL$GoalKpi")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_GoalKpi", allocationSize = 1)
public class GoalKpi extends KpiJunctionBaseEntity {

    private static final long serialVersionUID = 1984660281436555696L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_GOAL_ID")
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }
}
