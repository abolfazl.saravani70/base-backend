/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;


import javax.persistence.*;

@Entity
@Table(name = "SYS$SysRuleFormula")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_SysRuleFormula", allocationSize = 1)
public class SysRuleFormula extends BaseEntity {

    private static final long serialVersionUID = -3897282531993973670L;

    @Column(name = "KEY")
    private String key;

    @Column(name = "VALUE")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SYS_RULE_ID")
    private SysRule sysRule;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SysRule getSysRule() {
        return sysRule;
    }

    public void setSysRule(SysRule sysRule) {
        this.sysRule = sysRule;
    }
}
