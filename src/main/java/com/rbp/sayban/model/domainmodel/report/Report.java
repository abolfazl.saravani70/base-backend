/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.AppEntity;
import com.rbp.sayban.model.domainmodel.application.StoreProcedure;
import com.rbp.sayban.model.domainmodel.application.View;

import javax.persistence.*;

@Entity
@Table(name = "RPT$Report")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Report", allocationSize = 1)
public class Report extends BaseEntity {

    private static final long serialVersionUID = 3028508422687670506L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "QUERY")
    private String query;

    @Column(name = "REPORT_FILE")
    private String reportFile;

    @Column(name = "REPORT_PATH")
    private String reportPath;

    @Column(name = "IS_VIEW")
    private Boolean isViewFlag;

    @Column(name = "IS_TABLE")
    private Boolean isTable;

    @Column(name = "IS_ONLINE")
    private Boolean isOnline;

    @Column(name = "IS_STORE_PROCEDURE")
    private Boolean isStoreProcedure;

    @Column(name = "IS_DYNAMIC")
    private Boolean isDynamic;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_VIEW_ID")
    private View view;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_STORE_PROCEDURE_ID")
    private StoreProcedure storeProcedure;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APP_ENTITY_ID")
    private AppEntity appEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_REPORT_FILTER_ID")
    private ReportFilter reportFilter;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public AppEntity getAppEntity() {
        return appEntity;
    }

    public void setAppEntity(AppEntity appEntity) {
        this.appEntity = appEntity;
    }

    public ReportFilter getReportFilter() {
        return reportFilter;
    }

    public void setReportFilter(ReportFilter reportFilter) {
        this.reportFilter = reportFilter;
    }

    public Boolean getIsViewFlag() {
        return isViewFlag;
    }

    public void setIsViewFlag(Boolean viewFlag) {
        this.isViewFlag = viewFlag;
    }

    public Boolean getIsTable() {
        return isTable;
    }

    public void setIsTable(Boolean table) {
        this.isTable = table;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean online) {
        this.isOnline = online;
    }

    public Boolean getIsDynamic() {
        return isDynamic;
    }

    public void setIsDynamic(Boolean dynamic) {
        this.isDynamic = dynamic;
    }

    public Boolean getIsStoreProcedure() {
        return isStoreProcedure;
    }

    public void setIsStoreProcedure(Boolean storeProcedure) {
        this.isStoreProcedure = storeProcedure;
    }

    public StoreProcedure getStoreProcedure() {
        return storeProcedure;
    }

    public void setStoreProcedure(StoreProcedure storeProcedure) {
        this.storeProcedure = storeProcedure;
    }
}
