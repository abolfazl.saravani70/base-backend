/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Zone;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "ORG$Address")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Address",allocationSize = 1)
public class Address extends BaseEntity {

    private static final long serialVersionUID = -291694102518506201L;

    @Column(name = "ADDRESS_TEXT")
    private String addressText;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "STREET")
    private String street;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "FK_STAKEHOLDERS_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Stakeholder stakeholder;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "FK_ZONE_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Zone zone;

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getAddressText() {
        return addressText;
    }

    public void setAddressText(String addressText) {
        this.addressText = addressText;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Stakeholder getStakeholder() {
        return stakeholder;
    }

    public void setStakeholder(Stakeholder stakeholder) {
        this.stakeholder = stakeholder;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }
}
