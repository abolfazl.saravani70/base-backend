/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.strategies;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.SubjectiveJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$StrategySubjective")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_StrategySubjective", allocationSize = 1)
public class StrategySubjective extends SubjectiveJunctionBaseEntity {

    private static final long serialVersionUID = -7021588548215362714L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STRATEGY_ID")
    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
