/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "ORG$Phone")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Phone",allocationSize = 1)
public class Phone extends BaseEntity {

    private static final long serialVersionUID = 3842663895173086136L;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "CITY_PRECODE_NUMBER")
    private String cityPrecodeNumber;

    @Column(name = "IS_MAIN")
    private Boolean isMain;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "FK_STAKEHOLDERS_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Stakeholder stakeholder;

    public Stakeholder getStakeholder() {
        return stakeholder;
    }

    public void setStakeholder(Stakeholder stakeholder) {
        this.stakeholder = stakeholder;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCityPrecodeNumber() {
        return cityPrecodeNumber;
    }

    public void setCityPrecodeNumber(String cityPrecodeNumber) {
        this.cityPrecodeNumber = cityPrecodeNumber;
    }

    public Boolean getIsMain() {
        return isMain;
    }

    public void setIsMain(Boolean main) {
        this.isMain = main;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }
}
