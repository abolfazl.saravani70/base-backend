/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.norm.NormCost;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;

import javax.persistence.*;

@Entity
@Table(name = "PLN$NormCostOrgUnit")
public class NormCostOrganizationUnit extends BaseEntity {
    private static final long serialVersionUID = -8999383755172004749L;

    @Column(name = "EFFECTING_FACTOR")
    private Double effectingFactor;

    @Column(name = "DEFAULT_VALUE")
    private Long defaultValue;

    @Column(name = "TELORANCE")
    private Double telorance;

    @Column(name = "FISCAL_YEAR")
    private Long fiscalYear;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_NORMCOST_ID")
    private NormCost normCost;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_ID")
//    private OrganizationUnit organizationUnit;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long organizationUnitId;

    public Double getEffectingFactor() {
        return effectingFactor;
    }

    public void setEffectingFactor(Double effectingFactor) {
        this.effectingFactor = effectingFactor;
    }

    public Long getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Long defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Double getTelorance() {
        return telorance;
    }

    public void setTelorance(Double telorance) {
        this.telorance = telorance;
    }

    public Long getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(Long fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public NormCost getNormCost() {
        return normCost;
    }

    public void setNormCost(NormCost normCost) {
        this.normCost = normCost;
    }

    public Long getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Long organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }
}
