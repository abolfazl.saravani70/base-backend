/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.EnumValue;

import javax.persistence.*;

@Entity
@Table(name = "ORG$UserConfig")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_UserConfig",allocationSize = 1)
public class UserConfig extends BaseEntity {

    private static final long serialVersionUID = -2771055659061667807L;

    @Column(name = "KEY")
    private String key;

    @Column(name = "VALUE")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ENUM_VALUE_ID")
    private EnumValue enumValue;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public EnumValue getEnumValue() {
        return enumValue;
    }

    public void setEnumValue(EnumValue enumValue) {
        this.enumValue = enumValue;
    }
}
