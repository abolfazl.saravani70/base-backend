package com.rbp.sayban.model.domainmodel.task;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TSK$TaskLocation")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TaskLocation", allocationSize = 1)
public class TaskLocation extends LocationJunctionBaseEntity {

    private static final long serialVersionUID = 6937994681774469885L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_TASK_ID")
    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
