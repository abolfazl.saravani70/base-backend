/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.NormCostJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ActTemplateNormCost")
public class ActivityTemplateNormCost extends NormCostJunctionBaseEntity {

    private static final long serialVersionUID = 7285016447097111580L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_TEMPLATE_ID")
    private ActivityTemplate activityTemplate;

    public ActivityTemplate getActivityTemplate() {
        return activityTemplate;
    }

    public void setActivityTemplate(ActivityTemplate activityTemplate) {
        this.activityTemplate = activityTemplate;
    }
}
