/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.kpi;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "FIN$kpi")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_kpi", allocationSize = 1)
public class Kpi extends EvalStateBaseEntity {

    private static final long serialVersionUID = -5879665650627392392L;

    @Column(name = "KEY")
    private Integer key;

    @Column(name = "CODE")
    private String code;

    @Column(name = "VALUE")
    private Long value;

    @Column(name = "BEST_VALUE")
    private Long bestValue;

    @Column(name = "KPI_PRIORITY")
    private Integer kpiPriority;

    @Column(name = "COST")
    private Integer cost;

    @Column(name = "MINIMUM")
    private Long minimum;

    @Column(name = "MAXIMUM")
    private Long maximum;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "FK_PV_KPI_TYPE_ID")
    private Long pvKpiTypeId;

    @Column(name = "FK_PV_MESURE_UNIT_ID")
    private Long pvMesureUnitId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_UNIT_ID")
    private Unit unit;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getBestValue() {
        return bestValue;
    }

    public void setBestValue(Long bestValue) {
        this.bestValue = bestValue;
    }

    public Integer getKpiPriority() {
        return kpiPriority;
    }

    public void setKpiPriority(Integer kpiPriority) {
        this.kpiPriority = kpiPriority;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Long getMinimum() {
        return minimum;
    }

    public void setMinimum(Long minimum) {
        this.minimum = minimum;
    }

    public Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        this.maximum = maximum;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Long getPvKpiTypeId() {
        return pvKpiTypeId;
    }

    public void setPvKpiTypeId(Long pvKpiTypeId) {
        this.pvKpiTypeId = pvKpiTypeId;
    }

    public Long getPvMesureUnitId() {
        return pvMesureUnitId;
    }

    public void setPvMesureUnitId(Long pvMesureUnitId) {
        this.pvMesureUnitId = pvMesureUnitId;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        if (!super.equals(o)) return false;
//        Kpi kpi = (Kpi) o;
//        return getKey().equals(kpi.getKey());
//    }

    @Override
    public int hashCode() {
        return Objects.hash(1000000021);
    }
}
