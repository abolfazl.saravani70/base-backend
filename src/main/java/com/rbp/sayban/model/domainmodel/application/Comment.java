/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name ="APP$Comment")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Comment ", allocationSize = 1)
public class Comment extends BaseEntity {
    private static final long serialVersionUID = -8125223489131463790L;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "COMMENT_FROM")
    private String commentFrom;

    @Column(name = "COMMENT_TO")
    private String commentTo;

    @Column(name = "TYPE")
    private String type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APPLICATION_ID")
    private Application application;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFrom() {
        return commentFrom;
    }

    public void setFrom(String commentFrom) {
        this.commentFrom = commentFrom;
    }

    public String getTo() {
        return commentTo;
    }

    public void setTo(String commentTo) {
        this.commentTo = commentTo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
