/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "ORG$OrgUnitTemplate")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_OrgUnitTemplate", allocationSize = 1)
public class OrganizationUnitTemplate extends BaseEntity {

    private static final long serialVersionUID = 2724075472628814940L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "LEVEL$")
    private Integer level;

    @Column(name = "IS_POSITION")
    private Boolean isPosition;

    @Column(name = "FK_PV_DEGREE_ID")
    private Long pvDegreeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ORG_CHART_TEMPLATE_ID")
    private OrganizationChartTemplate organizationChartTemplate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_OU_TEMPLATE_ID")
    private OrganizationUnitTemplate organizationUnitTemplate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getIsPosition() {
        return isPosition;
    }

    public void setIsPosition(Boolean position) {
        this.isPosition = position;
    }

    public Long getPvDegreeId() {
        return pvDegreeId;
    }

    public void setPvDegreeId(Long pvDegreeId) {
        this.pvDegreeId = pvDegreeId;
    }

    public OrganizationChartTemplate getOrganizationChartTemplate() {
        return organizationChartTemplate;
    }

    public void setOrganizationChartTemplate(OrganizationChartTemplate organizationChartTemplate) {
        this.organizationChartTemplate = organizationChartTemplate;
    }

    public OrganizationUnitTemplate getOrganizationUnitTemplate() {
        return organizationUnitTemplate;
    }

    public void setOrganizationUnitTemplate(OrganizationUnitTemplate organizationUnitTemplate) {
        this.organizationUnitTemplate = organizationUnitTemplate;
    }
}
