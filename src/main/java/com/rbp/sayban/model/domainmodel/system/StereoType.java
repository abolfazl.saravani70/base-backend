/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "SYS$StereoType")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_StereoType", allocationSize = 1)
public class StereoType extends StereotypeBaseEntity {

    private static final long serialVersionUID = -138987097671632296L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SYSTEM_ID")
    private System system;

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
}
