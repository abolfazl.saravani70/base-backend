/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.kpi;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.sayban.model.domainmodel.risk.Risk;

import javax.persistence.*;

@Entity
@Table(name = "FIN$KpiRisk")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_KpiRisk", allocationSize = 1)
public class KpiRisk extends EvalStateBaseEntity {

    private static final long serialVersionUID = -1877448524836028222L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_KPI_ID")
    private Kpi kpi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_RISK_ID")
    private Risk risk;

    public Kpi getKpi() {
        return kpi;
    }

    public void setKpi(Kpi kpi) {
        this.kpi = kpi;
    }

    public Risk getRisk() {
        return risk;
    }

    public void setRisk(Risk risk) {
        this.risk = risk;
    }
}
