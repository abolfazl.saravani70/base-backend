package com.rbp.sayban.model.domainmodel.strategies;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$StrategyLocation")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_StrategyLocation", allocationSize = 1)
public class StrategyLocation extends LocationJunctionBaseEntity {

    private static final long serialVersionUID = 1171170156469077257L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STRATEGY_ID")
    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
