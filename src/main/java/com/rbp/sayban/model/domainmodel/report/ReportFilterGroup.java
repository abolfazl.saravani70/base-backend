/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.security.Group;

import javax.persistence.*;

@Entity
@Table(name = "RPT$ReportFilterGroup")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ReportFilterGroup", allocationSize = 1)
public class ReportFilterGroup extends BaseEntity {

    private static final long serialVersionUID = 3777642451661232729L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_REPORT_FILTER_ID")
    private ReportFilter reportFilter;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_GROUP_ID")
    private Group group;

    public ReportFilter getReportFilter() {
        return reportFilter;
    }

    public void setReportFilter(ReportFilter reportFilter) {
        this.reportFilter = reportFilter;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
