/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "ORG$OrgUnitRule")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_OrgUnitRule", allocationSize = 1)
public class OrganizationUnitRule extends BaseEntity {

    private static final long serialVersionUID = -2687412205443198604L;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_ID")
//    private OrganizationUnit organizationUnit;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long organizationUnitId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_RULE_ID")
    private Rule rule;

    public Long getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Long organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }
}
