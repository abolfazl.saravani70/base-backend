/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.kpi;

import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.domainmodel.strategies.Strategy;

import javax.persistence.*;

@Entity
@Table(name = "FIN$kpiTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_kpiTree", allocationSize = 1)
public class KpiTree extends TreeBaseEntity<Kpi> {

    private static final long serialVersionUID = 5514472456985355158L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Kpi child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Kpi parent;

    @Override
    public Kpi getChild() {
        return child;
    }

    @Override
    public void setChild(Kpi child) {
        this.child = child;
    }

    @Override
    public Kpi getParent() {
        return parent;
    }

    @Override
    public void setParent(Kpi parent) {
        this.parent = parent;
    }
}
