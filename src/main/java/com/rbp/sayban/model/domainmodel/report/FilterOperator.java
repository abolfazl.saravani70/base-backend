/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "RPT$FilterOperator")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_FilterOperator", allocationSize = 1)
public class FilterOperator extends BaseEntity {

    private static final long serialVersionUID = 1769177092465287921L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "ABBR")
    private String abbr;

    @Column(name = "SIGN")
    private String sign;

    @Column(name = "PRIORITY")
    private Integer priority;


    //------------------ Getter & Setter ------------------//
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
