/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "FIN$ObjectiveTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ObjectiveTree", allocationSize = 1)
public class ObjectiveTree extends RootBaseEntity<Objective,Objective> {

    private static final long serialVersionUID = -4093823150785808691L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Objective root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Objective child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Objective parent;

    @Override
    public Objective getRoot() {
        return root;
    }

    @Override
    public void setRoot(Objective root) {
        this.root = root;
    }

    @Override
    public Objective getChild() {
        return child;
    }

    @Override
    public void setChild(Objective child) {
        this.child = child;
    }

    @Override
    public Objective getParent() {
        return parent;
    }

    @Override
    public void setParent(Objective parent) {
        this.parent = parent;
    }
}
