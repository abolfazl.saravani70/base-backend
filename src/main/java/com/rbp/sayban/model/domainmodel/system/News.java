/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "SYS$News")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_News", allocationSize = 1)
public class News extends BaseEntity {

    private static final long serialVersionUID = 3301723527902738821L;

    @Column(name = "NEWS_DATE")
    private LocalDate date;

    @Column(name = "START_DATE")
    private LocalDate startDate;

    @Column(name = "END_DATE")
    private LocalDate endDate;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CONTEXT")
    private String context;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_OU_SENDER_ID")
//    private OrganizationUnit sender;

    @Column(name = "FK_OU_SENDER_ID")
    private Long ouIdSender;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APP_RECEIVER_ID")
    private Application reciever;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SYSTEM_ID")
    private System system;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Long getOuIdSender() {
        return ouIdSender;
    }

    public void setOuIdSender(Long ouIdSender) {
        this.ouIdSender = ouIdSender;
    }

    public Application getReciever() {
        return reciever;
    }

    public void setReciever(Application reciever) {
        this.reciever = reciever;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
}
