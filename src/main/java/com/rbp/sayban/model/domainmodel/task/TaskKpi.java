/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.task;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.KpiJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TSK$TaskKpi")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TaskKpi", allocationSize = 1)
public class TaskKpi extends KpiJunctionBaseEntity {

    private static final long serialVersionUID = -5054532844705447827L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_TASK_ID")
    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
