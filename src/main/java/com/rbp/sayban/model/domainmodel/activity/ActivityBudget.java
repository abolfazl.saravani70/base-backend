package com.rbp.sayban.model.domainmodel.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ActivityBudget")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ActivityBudget", allocationSize = 1)
public class ActivityBudget extends BudgetJunctionBaseEntity {

    private static final long serialVersionUID = 8209264315224735984L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
