/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.domainmodel.task.Task;

import javax.persistence.*;

@Entity
@Table(name = "TDO$TodoTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TodoTree", allocationSize = 1)
public class TodoTree extends RootBaseEntity<Todo,Task> {

    private static final long serialVersionUID = 5101421346793343445L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Task root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Todo child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Todo parent;

    @Override
    public Task getRoot() {
        return root;
    }

    @Override
    public void setRoot(Task root) {
        this.root = root;
    }

    @Override
    public Todo getChild() {
        return child;
    }

    @Override
    public void setChild(Todo child) {
        this.child = child;
    }

    @Override
    public Todo getParent() {
        return parent;
    }

    @Override
    public void setParent(Todo parent) {
        this.parent = parent;
    }
}
