/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.application;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "APP$AppMessage")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_AppMessage", allocationSize = 1)
public class AppMessage extends StereotypeBaseEntity {

    private static final long serialVersionUID = 2123507444478970290L;

    @Column(name = "IS_EXCEPTION")
    private Boolean isException;

    @Column(name = "PRIORITY")
    private Integer priority;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APPLICATION_ID")
    private Application application;


    public Boolean getIsException() {
        return isException;
    }

    public void setIsException(Boolean exception) {
        this.isException = exception;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
