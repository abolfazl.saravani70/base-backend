/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.AppEntity;
import com.rbp.sayban.model.domainmodel.application.Application;

import javax.persistence.*;

@Entity
@Table(name = "ORG$Signing")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Signing",allocationSize = 1)
public class Signing extends BaseEntity {
    private static final long serialVersionUID = -3385800046965056884L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SIGN_ID")
    private Sign sign;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APP_ENTITY_ID")
    private AppEntity appEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APPLICATION_ID")
    private Application application;

    public Sign getSign() {
        return sign;
    }

    public void setSign(Sign sign) {
        this.sign = sign;
    }

    public AppEntity getAppEntity() {
        return appEntity;
    }

    public void setAppEntity(AppEntity appEntity) {
        this.appEntity = appEntity;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
