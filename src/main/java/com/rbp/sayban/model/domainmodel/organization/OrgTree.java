/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;

import javax.persistence.*;

@Entity
@Table(name = "ORG$OrgTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_OrgTree", allocationSize = 1)
public class OrgTree extends TreeBaseEntity<Organization> {

    private static final long serialVersionUID = -5197892635401674179L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Organization child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Organization parent;

    @Override
    public Organization getChild() {
        return child;
    }

    @Override
    public void setChild(Organization child) {
        this.child = child;
    }

    @Override
    public Organization getParent() {
        return parent;
    }

    @Override
    public void setParent(Organization parent) {
        this.parent = parent;
    }
}
