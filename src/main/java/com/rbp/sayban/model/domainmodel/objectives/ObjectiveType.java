/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FIN$ObjectiveType")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ObjectiveType", allocationSize = 1)
public class ObjectiveType extends StereotypeBaseEntity {
    private static final long serialVersionUID = -8898222990597615928L;

}
