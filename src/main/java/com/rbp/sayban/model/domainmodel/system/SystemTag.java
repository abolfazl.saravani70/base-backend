/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "SYS$SystemTag")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_SystemTag", allocationSize = 1)
public class SystemTag extends BaseEntity {

    private static final long serialVersionUID = -2504577978866672877L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SYSTEM_ID")
    private System system;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_TAG_ID")
    private Tag tag;

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
