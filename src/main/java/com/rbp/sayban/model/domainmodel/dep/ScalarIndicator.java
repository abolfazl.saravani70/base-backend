
/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ScalarIndicator")
public class ScalarIndicator extends BaseEntity {
    private static final long serialVersionUID = 6619638691638102513L;

    @Column(name = "INDICATOR_NAME")
    private String indicatorName;

    @Column(name = "INDEX_NUMBER")
    private Integer indexNumber;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Column(name = "BEST_VALUE")
    private String bestValue;

    @Column(name = "INDICATOR_TYPE")
    private String indicatorType;

    @Column(name = "KEYWORD")
    private String keyword;

    @Column(name = "VALUE")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public Integer getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(Integer indexNumber) {
        this.indexNumber = indexNumber;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getBestValue() {
        return bestValue;
    }

    public void setBestValue(String bestValue) {
        this.bestValue = bestValue;
    }

    public String getIndicatorType() {
        return indicatorType;
    }

    public void setIndicatorType(String indicatorType) {
        this.indicatorType = indicatorType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}

