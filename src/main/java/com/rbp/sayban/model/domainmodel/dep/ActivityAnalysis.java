/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.activity.ActivityKpi;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ActivityAnalysis")
public class ActivityAnalysis extends BaseEntity {

    private static final long serialVersionUID = -482074717042788509L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_KPI_ID")
    private ActivityKpi activityKpi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PERFORMANCE_ID")
    private Performance performance;

    public ActivityKpi getActivityKpi() {
        return activityKpi;
    }

    public void setActivityKpi(ActivityKpi activityKpi) {
        this.activityKpi = activityKpi;
    }

    public Performance getPerformance() {
        return performance;
    }

    public void setPerformance(Performance performance) {
        this.performance = performance;
    }
}
