/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.task;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;

import javax.persistence.*;

@Entity
@Table(name = "TSK$TaskTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TaskTree", allocationSize = 1)
public class TaskTree extends RootBaseEntity<Task, Activity> {

    private static final long serialVersionUID = -8482726082857092586L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Activity root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Task child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Task parent;

    @Override
    public Activity getRoot() {
        return root;
    }

    @Override
    public void setRoot(Activity root) {
        this.root = root;
    }

    @Override
    public Task getChild() {
        return child;
    }

    @Override
    public void setChild(Task child) {
        this.child = child;
    }

    @Override
    public Task getParent() {
        return parent;
    }

    @Override
    public void setParent(Task parent) {
        this.parent = parent;
    }
}
