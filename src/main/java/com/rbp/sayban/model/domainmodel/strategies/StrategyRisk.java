/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.strategies;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.RiskJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$StrategyRisk")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_StrategyRisk", allocationSize = 1)
public class StrategyRisk extends RiskJunctionBaseEntity {

    private static final long serialVersionUID = 1833227898887727396L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STRATEGY_ID")
    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
