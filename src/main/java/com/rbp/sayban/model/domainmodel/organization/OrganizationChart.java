/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "ORG$OrgChart")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_OrgChart", allocationSize = 1)
public class OrganizationChart extends BaseEntity {

    private static final long serialVersionUID = -1997564998829758890L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    @Column(name = "IS_VIRTUAL")
    private Boolean isVirtual;

    @Column(name = "PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "FK_PV_ORG_LEVEL_ID")
    private Long pvOrgLevelId;

    @Column(name = "FK_PV_ORG_DEGREE_ID")
    private Long pvOrgDegreeId;

    @Column(name = "FK_PV_ORGANIZE_TYPE_ID")
    private Long pvOrganizeTypeId;

    @Column(name = "FK_PV_POSITION_NUMBER_ID")
    private Long pvPositionNumberId;

    @Column(name = "FK_PV_POSITION_TITLE_ID")
    private Long pvPositionTitleId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private OrganizationChart parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private Set<OrganizationChart> children;

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public Set<OrganizationChart> getChildren() {
        return children;
    }

    public void setChildren(Set<OrganizationChart> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Boolean virtual) {
        isVirtual = virtual;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }

    public Long getPvOrganizeTypeId() {
        return pvOrganizeTypeId;
    }

    public void setPvOrganizeTypeId(Long pvOrganizeTypeId) {
        this.pvOrganizeTypeId = pvOrganizeTypeId;
    }

    public Long getPvPositionNumberId() {
        return pvPositionNumberId;
    }

    public void setPvPositionNumberId(Long pvPositionNumberId) {
        this.pvPositionNumberId = pvPositionNumberId;
    }

    public Long getPvPositionTitleId() {
        return pvPositionTitleId;
    }

    public void setPvPositionTitleId(Long pvPositionTitleId) {
        this.pvPositionTitleId = pvPositionTitleId;
    }

    public OrganizationChart getParent() {
        return parent;
    }

    public void setParent(OrganizationChart parent) {
        this.parent = parent;
    }

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORGANIZATIO_ID")
//    private Organization organization;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_CHART_TEMPLATE")
//    private OrganizationChartTemplate organizationChartTemplate;

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Integer getLevel() {
//        return level;
//    }
//
//    public void setLevel(Integer level) {
//        this.level = level;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }

//    public Organization getOrganizationUnit() {
//        return organization;
//    }
//
//    public void setOrganizationUnit(Organization organization) {
//        this.organization = organization;
//    }
//
//    public OrganizationChartTemplate getOrganizationChartTemplate() {
//        return organizationChartTemplate;
//    }
//
//    public void setOrganizationChartTemplate(OrganizationChartTemplate organizationChartTemplate) {
//        this.organizationChartTemplate = organizationChartTemplate;
//    }
}
