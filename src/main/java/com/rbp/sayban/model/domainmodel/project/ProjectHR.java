package com.rbp.sayban.model.domainmodel.project;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PRJ$Project_HR")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ProjectHR", allocationSize = 1)
public class ProjectHR extends HRJunctionBaseEntity {

    private static final long serialVersionUID = 1370662987126908584L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "PROJECT_ID")
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
