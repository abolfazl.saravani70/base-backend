/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "FIN$Budget")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Budget", allocationSize = 1)
public class Budget extends EvalStateBaseEntity {

    private static final long serialVersionUID = 5646815087197789085L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_BUDGET_TYPE_ID")
    private BudgetType budgetType;

    public BudgetType getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(BudgetType budgetType) {
        this.budgetType = budgetType;
    }
}
