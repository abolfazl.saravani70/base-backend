/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.domainmodel.project.Project;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ActivityTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ActivityTree", allocationSize = 1)
public class ActivityTree extends RootBaseEntity<Activity,Project> {

    private static final long serialVersionUID = 636279882061860826L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Project root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Activity child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Activity parent;

    @Override
    public Project getRoot() {
        return root;
    }

    @Override
    public void setRoot(Project root) {
        this.root = root;
    }

    @Override
    public Activity getChild() {
        return child;
    }

    @Override
    public void setChild(Activity child) {
        this.child = child;
    }

    @Override
    public Activity getParent() {
        return parent;
    }

    @Override
    public void setParent(Activity parent) {
        this.parent = parent;
    }
}
