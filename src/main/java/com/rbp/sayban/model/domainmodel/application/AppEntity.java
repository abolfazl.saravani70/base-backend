/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "APP$AppEntity")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_AppEntity", allocationSize = 1)
public class AppEntity extends BaseEntity {
    private static final long serialVersionUID = 1921366777333907299L;

    @Column(name = "ENTITY_NAME")
    private String entityName;

    @Column(name = "ENTITY_KEYWORD")
    private String entityKeyword;

    @Column(name = "ENTITY_TYPE")
    private String entityType;

    @Column(name = "ENTITY_CODE")
    private String entityCode;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column(name = "REGULAR_EXP")
    private String regularExp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APPLICATION_ID")
    private Application application;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityKeyword() {
        return entityKeyword;
    }

    public void setEntityKeyword(String entityKeyword) {
        this.entityKeyword = entityKeyword;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public Boolean getIsDeprecated() {
        return isDeprecated;
    }

    public void setIsDeprecated(Boolean deprecated) {
        this.isDeprecated = deprecated;
    }

    public String getRegularExp() {
        return regularExp;
    }

    public void setRegularExp(String regularExp) {
        this.regularExp = regularExp;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
