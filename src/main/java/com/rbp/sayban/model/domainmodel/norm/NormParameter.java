/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.norm;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$NormParameter")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_NormParameter", allocationSize = 1)
public class NormParameter extends BaseEntity {
    private static final long serialVersionUID = -8633423939509205778L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_NORM_COST_ID")
    private NormCost normCost;

    @Column(name = "FK_PV_MESURE_UNIT_ID")
    private Long pvMesureUnitId;

    public NormCost getNormCost() {
        return normCost;
    }

    public void setNormCost(NormCost normCost) {
        this.normCost = normCost;
    }

    public Long getPvMesureUnitId() {
        return pvMesureUnitId;
    }

    public void setPvMesureUnitId(Long pvMesureUnitId) {
        this.pvMesureUnitId = pvMesureUnitId;
    }
}
