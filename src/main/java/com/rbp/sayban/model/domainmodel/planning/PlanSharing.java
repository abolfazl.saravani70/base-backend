/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.Sharing;

import javax.persistence.*;
@Entity
@Table(name = "PLN$PlanSharing")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_PlanSharing", allocationSize = 1)
public class PlanSharing extends BaseEntity {

    private static final long serialVersionUID = 3333405056205027631L;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "PERCENT")
    private Double percent;

    @Column(name = "PRIDIِCATE")
    private Long pridicate;

    @Column(name = "ACCEPTEDAMOUNT")
    private Long acceptedAmount;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_PLAN_ID")
    private Plan plan;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_SHARING_ID")
    private Sharing sharing;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Long getPridicate() {
        return pridicate;
    }

    public void setPridicate(Long pridicate) {
        this.pridicate = pridicate;
    }

    public Long getAcceptedAmount() {
        return acceptedAmount;
    }

    public void setAcceptedAmount(Long acceptedAmount) {
        this.acceptedAmount = acceptedAmount;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public Sharing getSharing() {
        return sharing;
    }

    public void setSharing(Sharing sharing) {
        this.sharing = sharing;
    }
}
