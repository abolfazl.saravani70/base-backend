/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.security.User;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ORG$Stakeholder")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Stakeholder", allocationSize = 1)
public class Stakeholder extends BaseEntity {

    private static final long serialVersionUID = -6252245536239855500L;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "IS_PERSON")
    private Boolean isPerson;

//    @OneToOne(mappedBy = "stakeholder",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
//    private Organization organization;

    @OneToOne(mappedBy = "stakeholder",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private User user;

    @OneToMany(mappedBy = "stakeholder",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Email> emails;

    @OneToMany(mappedBy = "stakeholder",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Address> addresses;

    @OneToMany(mappedBy = "stakeholder",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Phone> phones;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsPerson() {
        return isPerson;
    }

    public void setIsPerson(Boolean person) {
        this.isPerson = person;
    }

//    public Organization getOrganizationUnit() {
//        return organization;
//    }
//
//    public void setOrganizationUnit(Organization organization) {
//        this.organization = organization;
//    }
//
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Email> getEmails() {
        return emails;
    }

    public void setEmails(Set<Email> emails) {
        this.emails = emails;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }
}
