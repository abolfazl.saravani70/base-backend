/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "ORG$Organization")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Organization", allocationSize = 1)
public class Organization extends BaseEntity {

    private static final long serialVersionUID = -1583396924954718272L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "ORG_CODE")
    private String orgCode;

    @Column(name = "ZAMZAM_CODE")
    private String zamzamCode;

    @Column(name = "STRATEGIC_CODE")
    private String strategicCode;

    @Column(name = "IS_ORGANIZED")
    private Boolean isOrganized;

    @Column(name = "FK_PV_ORG_LEVEL_ID")
    private Long pvOrgLevelId;

    @Column(name = "FK_PV_ORG_DEGREE_ID")
    private Long pvOrgDegreeId;

    @Column(name = "FK_PV_ORG_TYPE_ID")
    private Long pvOrgTypeId;

    @OneToMany(mappedBy = "organization", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Organization> organizations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Organization organization;

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STAKEHOLDER_ID")
    private Stakeholder stakeholder;

//    @OneToMany(mappedBy = "organization", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
//    private List<OrganizationAccount> orgAccount;

//    public List<OrganizationAccount> getOrgAccount() {
//        return orgAccount;
//    }
//
//    public void setOrgAccount(List<OrganizationAccount> orgAccount) {
//        this.orgAccount = orgAccount;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsOrganized() {
        return isOrganized;
    }

    public void setIsOrganized(Boolean organized) {
        this.isOrganized = organized;
    }

    public Long getPvOrgTypeId() {
        return pvOrgTypeId;
    }

    public void setPvOrgTypeId(Long pvOrgTypeId) {
        this.pvOrgTypeId = pvOrgTypeId;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Set<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(Set<Organization> organizations) {
        this.organizations = organizations;
    }

    public Stakeholder getStakeholder() {
        return stakeholder;
    }

    public void setStakeholder(Stakeholder stakeholder) {
        this.stakeholder = stakeholder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getZamzamCode() {
        return zamzamCode;
    }

    public void setZamzamCode(String zamzamCode) {
        this.zamzamCode = zamzamCode;
    }

    public String getStrategicCode() {
        return strategicCode;
    }

    public void setStrategicCode(String strategicCode) {
        this.strategicCode = strategicCode;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }
}
