/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import com.rbp.sayban.model.domainmodel.project.Project;
import com.rbp.sayban.model.domainmodel.strategies.Strategy;

import javax.persistence.*;

@Entity
@Table(name = "GOL$GoalTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_GoalTree", allocationSize = 1)
public class GoalTree extends RootBaseEntity<Goal, Strategy> {

    private static final long serialVersionUID = -2816475693143275574L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Strategy root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Goal child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Goal parent;

    @Override
    public Strategy getRoot() {
        return root;
    }

    @Override
    public void setRoot(Strategy root) {
        this.root = root;
    }

    @Override
    public Goal getChild() {
        return child;
    }

    @Override
    public void setChild(Goal child) {
        this.child = child;
    }

    @Override
    public Goal getParent() {
        return parent;
    }

    @Override
    public void setParent(Goal parent) {
        this.parent = parent;
    }
}
