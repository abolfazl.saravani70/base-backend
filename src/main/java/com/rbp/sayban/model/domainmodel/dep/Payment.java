/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$Payment")
public class Payment extends BaseEntity {

    private static final long serialVersionUID = -8583876130942576266L;

    @Column(name="PAYMENT_TEMPLATE_ID")
    private Integer paymentTemplateId;

    @Column(name="PAID_AMOUNT")
    private Integer paidAmount;

    @Column(name="TOTALA_MOUNT")
    private Integer totalAmount;

    @Column(name="INSTALLMENT_NUMBER")
    private Integer installmentNumber;

    @Column(name="INSTALLMENT_LIST")
    private Integer installmentList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public Integer getPaymentTemplateId() {
        return paymentTemplateId;
    }

    public void setPaymentTemplateId(Integer paymentTemplateId) {
        this.paymentTemplateId = paymentTemplateId;
    }

    public Integer getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Integer paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public Integer getInstallmentList() {
        return installmentList;
    }

    public void setInstallmentList(Integer installmentList) {
        this.installmentList = installmentList;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
