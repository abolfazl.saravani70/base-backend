/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "ORG$OrgLocation")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_OrgLocation",allocationSize = 1)
public class OrganizationLocation extends BaseEntity {

    private static final long serialVersionUID = 1861266198218315802L;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_ID")
//    private Organization organization;

    @Column(name = "FK_ORG_ID")
    private Long organizationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ADDRESSES_ID")
    private Address address;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
