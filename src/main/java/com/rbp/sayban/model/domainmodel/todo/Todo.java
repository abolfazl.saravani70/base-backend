/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TDO$Todo")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Todo", allocationSize = 1)
public class Todo extends EvalStateBaseEntity {

    private static final long serialVersionUID = -2191579990298950673L;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }
}
