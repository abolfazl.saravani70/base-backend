/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "SYS$ProcessStepSysRule")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ProcessStepSysRule", allocationSize = 1)
public class ProcessStepSysRule extends BaseEntity{

    private static final long serialVersionUID = -5132080193682810066L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PROCESS_STEP_ID")
    private ProcessStep processStep;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SYS_RULE_ID")
    private SysRule sysRule;

    public ProcessStep getProcessStep() {
        return processStep;
    }

    public void setProcessStep(ProcessStep processStep) {
        this.processStep = processStep;
    }

    public SysRule getSysRule() {
        return sysRule;
    }

    public void setSysRule(SysRule sysRule) {
        this.sysRule = sysRule;
    }
}
