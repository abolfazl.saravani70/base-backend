package com.rbp.sayban.model.domainmodel.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "FIN$ObjectiveBudget")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ObjectiveBudget", allocationSize = 1)
public class ObjectiveBudget extends BudgetJunctionBaseEntity {

    private static final long serialVersionUID = 5574275717545708448L;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_OBJECTIVE_ID")
    private Objective objective;

    public Objective getObjective() {
        return objective;
    }

    public void setObjective(Objective objective) {
        this.objective = objective;
    }
}
