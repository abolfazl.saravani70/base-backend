/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "ORG$OrganizationUnit")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_OrgUnit", allocationSize = 1)
public class OrganizationUnit extends BaseEntity {

    private static final long serialVersionUID = 2724075472628814940L;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "IS_ENABLE")
    private Boolean isEnabled;

    @Column(name = "IS_ASSIGNED")
    Boolean isAssigned;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ORG_CHART_ID")
    private OrganizationChart organizationChart;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "FK_ORG_ID")
    @OnDelete(action= OnDeleteAction.CASCADE)
    private Organization organization;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean enable) {
        isEnabled = enable;
    }

    public OrganizationChart getOrganizationChart() {
        return organizationChart;
    }

    public void setOrganizationChart(OrganizationChart organizationChart) {
        this.organizationChart = organizationChart;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Boolean getisEnabled() {
        return isEnabled;
    }

    public void setisEnabled(Boolean enable) {
        isEnabled = enable;
    }

    public Boolean getisAssigned() {
        return isAssigned;
    }

    public void setisAssigned(Boolean assigned) {
        isAssigned = assigned;
    }

    //
//    @Column(name = "NAME")
//    private String name;
//
//    @Column(name = "TITLE")
//    private String title;
//
//    @Column(name = "TYPE")
//    private String type;
//
//    @Column(name = "LEVEL$")
//    private Integer level;
//
//    @Column(name = "IS_POSITION")
//    private Boolean isPosition;
//
//    @Column(name ="FK_PV_DEGREE_ID")
//    private Long pvDegreeId;
//
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_POSITION_ID")
//    private OrganizationPosition organizationPosition;
//
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_UNIT_ID")
//    private OrganizationUnit organizationUnit;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_CHART")
//    private OrganizationChart organizationChart;
//
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_LOCATION_ID")
//    private OrganizationLocation organizationLocation;

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public Integer getLevel() {
//        return level;
//    }
//
//    public void setLevel(Integer level) {
//        this.level = level;
//    }
//
//    public Boolean getIsPosition() {
//        return isPosition;
//    }
//
//    public void setIsPosition(Boolean position) {
//        this.isPosition = position;
//    }
//
//    public Long getPvDegreeId() {
//        return pvDegreeId;
//    }
//
//    public void setPvDegreeId(Long pvDegreeId) {
//        this.pvDegreeId = pvDegreeId;
//    }

//    public OrganizationPosition getOrganizationPosition() {
//        return organizationPosition;
//    }
//
//    public void setOrganizationPosition(OrganizationPosition organizationPosition) {
//        this.organizationPosition = organizationPosition;
//    }

//    public OrganizationUnit getOrganizationUnit() {
//        return organizationUnit;
//    }
//
//    public void setOrganizationUnit(OrganizationUnit organizationUnit) {
//        this.organizationUnit = organizationUnit;
//    }
//
//    public OrganizationChart getOrganizationChart() {
//        return organizationChart;
//    }
//
//    public void setOrganizationChart(OrganizationChart organizationChart) {
//        this.organizationChart = organizationChart;
//    }
//
//    public OrganizationLocation getOrganizationLocation() {
//        return organizationLocation;
//    }
//
//    public void setOrganizationLocation(OrganizationLocation organizationLocation) {
//        this.organizationLocation = organizationLocation;
//    }
}
