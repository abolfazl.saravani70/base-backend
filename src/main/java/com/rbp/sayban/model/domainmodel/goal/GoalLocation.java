package com.rbp.sayban.model.domainmodel.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "GOL$GoalLocation")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_GoalLocation", allocationSize = 1)
public class GoalLocation extends LocationJunctionBaseEntity {

    private static final long serialVersionUID = -3290226215328169680L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_GOAL_ID")
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }
}
