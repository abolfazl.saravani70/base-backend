package com.rbp.sayban.model.domainmodel.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "GOL$Goal_HR")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_GoalHR", allocationSize = 1)
public class GoalHR extends HRJunctionBaseEntity {

    private static final long serialVersionUID = -6520985596947135591L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_GOAL_ID")
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }
}
