/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.project;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.domainmodel.planning.Plan;

import javax.persistence.*;

@Entity
@Table(name = "PRJ$ProjectTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ProjectTree", allocationSize = 1)
public class ProjectTree extends RootBaseEntity<Project,Plan> {

    private static final long serialVersionUID = 4096967152067450358L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Plan root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Project child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Project parent;

    @Override
    public Plan getRoot() {
        return root;
    }

    @Override
    public void setRoot(Plan root) {
        this.root = root;
    }

    @Override
    public Project getChild() {
        return child;
    }

    @Override
    public void setChild(Project child) {
        this.child = child;
    }

    @Override
    public Project getParent() {
        return parent;
    }

    @Override
    public void setParent(Project parent) {
        this.parent = parent;
    }
}
