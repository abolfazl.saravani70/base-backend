package com.rbp.sayban.model.domainmodel.project;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PRJ$ProjectBudget")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ProjectBudget", allocationSize = 1)
public class ProjectBudget extends BudgetJunctionBaseEntity {

    private static final long serialVersionUID = 1170078826386431951L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "PROJECT_ID")
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
