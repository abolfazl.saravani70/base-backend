/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "APP$AppScheduling")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_AppScheduling", allocationSize = 1)
public class AppScheduling extends BaseEntity {

    private static final long serialVersionUID = 2942363825529762610L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "INIT_DATE")
    private LocalDate initDate;

    @Column(name = "START_DATE")
    private LocalDate startDate;

    @Column(name = "END_DATE")
    private LocalDate endDate;

    @Column(name = "NUMBER_OF_REPEAT")
    private Integer numberOfRepeat;

    @Column(name = "REPEATABLE")
    private String repeatable;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "DB_JOB")
    private String dbJob;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_APPLICATION_ID")
    private Application application;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDate getInitDate() {
        return initDate;
    }

    public void setInitDate(LocalDate initDate) {
        this.initDate = initDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getNumberOfRepeat() {
        return numberOfRepeat;
    }

    public void setNumberOfRepeat(Integer numberOfRepeat) {
        this.numberOfRepeat = numberOfRepeat;
    }

    public String getRepeatable() {
        return repeatable;
    }

    public void setRepeatable(String repeatable) {
        this.repeatable = repeatable;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDbJob() {
        return dbJob;
    }

    public void setDbJob(String dbJob) {
        this.dbJob = dbJob;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
