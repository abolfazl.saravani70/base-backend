/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$PerformanceIndicator")
public class PerformanceIndicator extends BaseEntity {
    private static final long serialVersionUID = 6966220867388208236L;

    @Column(name = "INDICATOR_NAME")
    private String indicatorName;

    @Column(name = "INDEX_NUMBER")
    private Integer indexnumber;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Column(name = "BEST_VALUE")
    private Integer bestValue;

    @Column(name = "INDEICATOR_TYPE")
    private String IndicatorType;

    @Column(name = "KEYWORD")
    private String keyword;

    @Column(name = "VALUE")
    private Integer value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public Integer getIndexnumber() {
        return indexnumber;
    }

    public void setIndexnumber(Integer indexnumber) {
        this.indexnumber = indexnumber;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getBestValue() {
        return bestValue;
    }

    public void setBestValue(Integer bestValue) {
        this.bestValue = bestValue;
    }

    public String getIndicatorType() {
        return IndicatorType;
    }

    public void setIndicatorType(String indicatorType) {
        IndicatorType = indicatorType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
