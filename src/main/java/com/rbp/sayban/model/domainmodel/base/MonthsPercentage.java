/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.base;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MonthsPercentage {

    @Column(name = "M1")
    private Double m1;

    @Column(name = "M2")
    private Double m2;

    @Column(name = "M3")
    private Double m3;

    @Column(name = "M4")
    private Double m4;

    @Column(name = "M5")
    private Double m5;

    @Column(name = "M6")
    private Double m6;

    @Column(name = "M7")
    private Double m7;

    @Column(name = "M8")
    private Double m8;

    @Column(name = "M9")
    private Double m9;

    @Column(name = "M10")
    private Double m10;

    @Column(name = "M11")
    private Double m11;

    @Column(name = "M12")
    private Double m12;

    public Double getM1() {
        return m1;
    }

    public void setM1(Double m1) {
        this.m1 = m1;
    }

    public Double getm2() {
        return m2;
    }

    public void setm2(Double m2) {
        this.m2 = m2;
    }

    public Double getm3() {
        return m3;
    }

    public void setm3(Double m3) {
        this.m3 = m3;
    }

    public Double getm4() {
        return m4;
    }

    public void setm4(Double m4) {
        this.m4 = m4;
    }

    public Double getm5() {
        return m5;
    }

    public void setm5(Double m5) {
        this.m5 = m5;
    }

    public Double getm6() {
        return m6;
    }

    public void setm6(Double m6) {
        this.m6 = m6;
    }

    public Double getm7() {
        return m7;
    }

    public void setm7(Double m7) {
        this.m7 = m7;
    }

    public Double getm8() {
        return m8;
    }

    public void setm8(Double m8) {
        this.m8 = m8;
    }

    public Double getm9() {
        return m9;
    }

    public void setm9(Double m9) {
        this.m9 = m9;
    }

    public Double getm10() {
        return m10;
    }

    public void setm10(Double m10) {
        this.m10 = m10;
    }

    public Double getm11() {
        return m11;
    }

    public void setm11(Double m11) {
        this.m11 = m11;
    }

    public Double getm12() {
        return m12;
    }

    public void setm12(Double m12) {
        this.m12 = m12;
    }
}
