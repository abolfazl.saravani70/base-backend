/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.project;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PRJ$ProjectResource")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ProjectResource", allocationSize = 1)
public class ProjectResource extends BaseEntity {

    private static final long serialVersionUID = 6693528763993247178L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CODE")
    private String code;

    @Column(name = "FK_PV_PROJECT_RESOURCE_TYPE_ID")
    private Long pvProjectResourceTypeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PROJECT_RESOURCE_TYPE_ID")
    private ProjectResourceType projectResourceType;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPvProjectResourceTypeId() {
        return pvProjectResourceTypeId;
    }

    public void setPvProjectResourceTypeId(Long pvProjectResourceTypeId) {
        this.pvProjectResourceTypeId = pvProjectResourceTypeId;
    }

    public ProjectResourceType getProjectResourceType() {
        return projectResourceType;
    }

    public void setProjectResourceType(ProjectResourceType projectResourceType) {
        this.projectResourceType = projectResourceType;
    }
}
