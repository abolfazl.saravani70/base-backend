/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.risk;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "PLN$Risk")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Risk", allocationSize = 1)
public class Risk extends BaseEntity {

    private static final long serialVersionUID = 6742642707609624087L;

    @Override
    public int hashCode() {
        return Objects.hashCode(12345687);
    }
}
