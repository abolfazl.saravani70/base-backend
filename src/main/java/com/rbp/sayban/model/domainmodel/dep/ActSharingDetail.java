/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;

import javax.persistence.*;


@Entity
@Table(name = "PLN$ActSharingDetail")
public class ActSharingDetail extends BaseEntity {
    private static final long serialVersionUID = -4970028378658458797L;

    @Column(name = "BASE_AMOUNT")
    private Integer baseAmount;

    @Column(name = "BASE_KEY")
    private Integer baseKey;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SHARING_ID")
    private ActSharing actSharing;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public Integer getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(Integer baseAmount) {
        this.baseAmount = baseAmount;
    }

    public Integer getBaseKey() {
        return baseKey;
    }

    public void setBaseKey(Integer baseKey) {
        this.baseKey = baseKey;
    }

    public ActSharing getActSharing() {
        return actSharing;
    }

    public void setActSharing(ActSharing actSharing) {
        this.actSharing = actSharing;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
