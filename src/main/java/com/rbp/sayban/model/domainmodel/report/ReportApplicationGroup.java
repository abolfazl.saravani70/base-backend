/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.security.Group;

import javax.persistence.*;

@Entity
@Table(name = "RPT$ReportApplicationGroup")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ReportApplicationGroup", allocationSize = 1)
public class ReportApplicationGroup extends BaseEntity {

    private static final long serialVersionUID = 9173442328757974354L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_REPORT_FILTER_ID")
    private ReportApplication reportApplication;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_GROUP_ID")
    private Group group;

    public ReportApplication getReportApplication() {
        return reportApplication;
    }

    public void setReportApplication(ReportApplication reportApplication) {
        this.reportApplication = reportApplication;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
