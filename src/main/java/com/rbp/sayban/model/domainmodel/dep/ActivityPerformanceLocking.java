/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ActPerformanceLocking")
public class ActivityPerformanceLocking extends BaseEntity {

    private static final long serialVersionUID = 5440163214299138103L;

    @Column(name = "IS_LOCK")
    private Boolean isLock;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_PERFORMANCE_ID")
    private ActivityPerformance activityPerformance;

    public Boolean getIsLock() {
        return isLock;
    }

    public void setIsLock(Boolean lock) {
        this.isLock = lock;
    }

    public ActivityPerformance getActivityPerformance() {
        return activityPerformance;
    }

    public void setActivityPerformance(ActivityPerformance activityPerformance) {
        this.activityPerformance = activityPerformance;
    }
}
