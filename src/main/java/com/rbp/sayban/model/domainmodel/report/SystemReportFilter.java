/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.report;

import com.rbp.sayban.model.domainmodel.system.System;

import javax.persistence.*;

@Entity
@Table(name = "RPT$SystemReportFilter")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_SystemReportFilter", allocationSize = 1)
public class SystemReportFilter extends GeneralReportFilter {

    private static final long serialVersionUID = -1208526018346797324L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SYSTEM_ID")
    private System system;

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
}
