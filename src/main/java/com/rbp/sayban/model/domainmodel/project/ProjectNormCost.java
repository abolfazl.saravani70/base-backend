/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.project;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.NormCostJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PRJ$ProjectNormCost")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_ProjectNormCost", allocationSize = 1)
public class ProjectNormCost  extends NormCostJunctionBaseEntity {

    private static final long serialVersionUID = 8152143934080126436L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_PROJECT_ID")
    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
