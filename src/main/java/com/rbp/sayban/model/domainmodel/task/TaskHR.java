package com.rbp.sayban.model.domainmodel.task;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "TSK$Task_HR")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_TaskHR", allocationSize = 1)
public class TaskHR extends HRJunctionBaseEntity {

    private static final long serialVersionUID = -2993180792615529324L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_TASK_ID")
    private Task task;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
