package com.rbp.sayban.model.domainmodel.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$PlanLocation")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_PlanLocation", allocationSize = 1)
public class PlanLocation extends LocationJunctionBaseEntity {


    private static final long serialVersionUID = 9187846104193718338L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_PLAN_ID")
    private Plan plan;

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }
}
