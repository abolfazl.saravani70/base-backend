/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$ActSharingType")
public class ActSharingType extends StereotypeBaseEntity {
    private static final long serialVersionUID = -9187878081279468474L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
