package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "ORG$_HR")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_HR", allocationSize = 1)
public class HumanResource extends BaseEntity {

    private static final long serialVersionUID = -8656513042469722827L;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "IS_ENABLE")
    private Boolean isEnabled;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_CHART_ID")
//    private OrganizationChart organizationChart;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_ORG_ID")
//    private Organization organization;

    @Column(name = "FK_ORG_CHART_ID")
    private Long organizationChartId;

    @Column(name = "FK_ORG_ID")
    private Long organizationId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getEnable() {
        return isEnabled;
    }

    public void setEnable(Boolean enable) {
        isEnabled = enable;
    }

    public Long getOrganizationChartId() {
        return organizationChartId;
    }

    public void setOrganizationChartId(Long organizationChartId) {
        this.organizationChartId = organizationChartId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
}
