/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.strategies;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.KpiJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$StrategyKpi")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_StrategyKpi", allocationSize = 1)
public class StrategyKpi extends KpiJunctionBaseEntity {

    private static final long serialVersionUID = 4643742741151982951L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STRATEGY_ID")
    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
