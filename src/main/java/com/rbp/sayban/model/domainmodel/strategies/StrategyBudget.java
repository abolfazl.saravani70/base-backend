package com.rbp.sayban.model.domainmodel.strategies;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;

import javax.persistence.*;


@Entity
@Table(name = "PLN$StrategyBudget")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_StrategyBudget", allocationSize = 1)
public class StrategyBudget extends BudgetJunctionBaseEntity {

    private static final long serialVersionUID = 739755681220973843L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STRATEGY_ID")
    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
