/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.rule;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PLN$PlanningTypeRule")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_PlanningTypeRule", allocationSize = 1)
public class PlanningTypeRule extends BaseEntity {

    private static final long serialVersionUID = 8904637214079963255L;

    @Column(name = "FK_PV_PLANNING_TYPE_ID")
    private Long pvPlanningTypeId;

    @Column(name = "FK_PV_PLANNING_TYPE_ID_CHILD")
    private Long pvPlansTypeIdChild;

    public Long getPvPlanningTypeId() {
        return pvPlanningTypeId;
    }

    public void setPvPlanningTypeId(Long pvPlanningTypeId) {
        this.pvPlanningTypeId = pvPlanningTypeId;
    }

    public Long getPvPlansTypeIdChild() {
        return pvPlansTypeIdChild;
    }

    public void setPvPlansTypeIdChild(Long pvPlansTypeIdChild) {
        this.pvPlansTypeIdChild = pvPlansTypeIdChild;
    }
}
