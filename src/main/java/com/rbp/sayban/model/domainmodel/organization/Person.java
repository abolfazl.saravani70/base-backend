/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.organization;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.Blob;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "ORG$Person")
@SequenceGenerator(initialValue =100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Person",allocationSize = 1)
public class Person extends BaseEntity {

    private static final long serialVersionUID = 8961354086521760450L;

    @Column(name = "NATIONAL_ID")
    private Long nationalId;

    @Column(name = "IDENTITY_NUMBER")
    private Long identityNumber;

    @Column(name = "FATHER_NAME")
    private String fatherName;

    @Column(name = "BIRTH_DATE")
    private LocalDate birthDate;

    @Column(name = "DEGREE")
    private String degree;

    @Column(name = "UX_LEVEL")
    private Long uxLevel;

    @Column(name = "IMAGE")
    private Blob image;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "FK_PV_GENDER_ID")
    private Long pvGenderId;


    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_STAKEHOLDER_ID")
    private Stakeholder stakeholder;

//    @ManyToOne(fetch = FetchType.LAZY,optional = false)
//    @JoinColumn(name = "FK_ORG_UNIT_ID")
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    private OrganizationUnit organizationUnit;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long organizationUnitId;


    public Long getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Long organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    public Stakeholder getStakeholder() {
    return stakeholder;
}

    public void setStakeholder(Stakeholder stakeholder) {
        this.stakeholder = stakeholder;
    }

    public Long getNationalId() {
        return nationalId;
    }

    public void setNationalId(Long nationalId) {
        this.nationalId = nationalId;
    }

    public Long getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Long identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Long getUxLevel() {
        return uxLevel;
    }

    public void setUxLevel(Long uxLevel) {
        this.uxLevel = uxLevel;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getPvGenderId() {
        return pvGenderId;
    }

    public void setPvGenderId(Long pvGenderId) {
        this.pvGenderId = pvGenderId;
    }

}
