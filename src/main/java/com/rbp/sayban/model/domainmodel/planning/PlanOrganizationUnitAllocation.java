/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.Allocation;

import javax.persistence.*;

@Entity
@Table(name = "PLN$PlanOrgUnitAllocation")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_PlanOrgUnitAllocation", allocationSize = 1)
public class PlanOrganizationUnitAllocation extends BaseEntity {

    private static final long serialVersionUID = -773719930799534364L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_PlanOrgUnit_ID")
    private PlanOrganizationUnit planOrganizationUnit;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ALLOCATION_ID")
    private Allocation allocation;

    public PlanOrganizationUnit getPlanOrganizationUnit() {
        return planOrganizationUnit;
    }

    public void setPlanOrganizationUnit(PlanOrganizationUnit planOrganizationUnit) {
        this.planOrganizationUnit = planOrganizationUnit;
    }

    public Allocation getAllocation() {
        return allocation;
    }

    public void setAllocation(Allocation allocation) {
        this.allocation = allocation;
    }
}
