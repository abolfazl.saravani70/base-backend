/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.strategies;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;

import javax.persistence.*;

@Entity
@Table(name = "PLN$StrategyTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_StrategyTree", allocationSize = 1)
public class StrategyTree extends RootBaseEntity<Strategy,Objective> {

    private static final long serialVersionUID = 1293477062954193553L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Objective root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Strategy child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Strategy parent;

    @Override
    public Objective getRoot() {
        return root;
    }

    @Override
    public void setRoot(Objective root) {
        this.root = root;
    }

    @Override
    public Strategy getChild() {
        return child;
    }

    @Override
    public void setChild(Strategy child) {
        this.child = child;
    }

    @Override
    public Strategy getParent() {
        return parent;
    }

    @Override
    public void setParent(Strategy parent) {
        this.parent = parent;
    }
}
