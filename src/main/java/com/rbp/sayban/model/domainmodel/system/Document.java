/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SYS$Document")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Document", allocationSize = 1)
public class Document extends BaseEntity {

    private static final long serialVersionUID = -5593692041640149478L;

    @Column(name = "IS_TEMP")
    private Boolean isTemp;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "FK_PV_DOC_STATE_ID")
    private Long pvDocumentStateId;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_BLOB_ID")
    private Blob blob;

    @OneToMany(mappedBy = "document", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<DocumentDetail> documentDetail = new ArrayList<>();

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "FK_REQUEST_ID")
//    private Request request;
//
//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "FK_DOCUMENT_DUTY_ID")
//    private DocumentDuty documentDuty;


    public Boolean getIsTemp() {
        return isTemp;
    }

    public void setIsTemp(Boolean temp) {
        this.isTemp = temp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public Blob getBlob() {
        return blob;
    }

    public void setBlob(Blob blob) {
        this.blob = blob;
    }

//    public Request getRequest() {
//        return request;
//    }
//
//    public void setRequest(Request request) {
//        this.request = request;
//    }
//
//    public DocumentDuty getDocumentDuty() {
//        return documentDuty;
//    }
//
//    public void setDocumentDuty(DocumentDuty documentDuty) {
//        this.documentDuty = documentDuty;
//    }

    public Long getPvDocumentStateId() {
        return pvDocumentStateId;
    }

    public void setPvDocumentStateId(Long pvDocumentStateId) {
        this.pvDocumentStateId = pvDocumentStateId;
    }

    public List<DocumentDetail> getDocumentDetail() {
        return documentDetail;
    }

    public void setDocumentDetail(List<DocumentDetail> documentDetail) {
        this.documentDetail = documentDetail;
    }
}
