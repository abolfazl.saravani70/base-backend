/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.Document;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "FIN$Policy")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_Policy", allocationSize = 1)
public class Policy extends BaseEntity {

    private static final long serialVersionUID = 7787620751230755787L;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "SYS_POLICY_DATE")
    private LocalDate date;

    @Column(name = "REASON")
    private String reason;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_DOCUMENT_ID")
    private Document document;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "FK_POST_ID")
//    private OrganizationPosition post;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

//    public OrganizationPosition getPost() {
//        return post;
//    }
//
//    public void setPost(OrganizationPosition post) {
//        this.post = post;
//    }
}
