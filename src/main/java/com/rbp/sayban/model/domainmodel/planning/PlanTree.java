/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.RootBaseEntity;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.domainmodel.objectives.Objective;

import javax.persistence.*;

@Entity
@Table(name = "PLN$PlanTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_PlanTree", allocationSize = 1)
public class PlanTree extends RootBaseEntity<Plan,Goal> {

    private static final long serialVersionUID = 3023272493544760877L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ROOT_ID")
    private Goal root;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Plan child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Plan parent;

    @Override
    public Goal getRoot() {
        return root;
    }

    @Override
    public void setRoot(Goal root) {
        this.root = root;
    }

    @Override
    public Plan getChild() {
        return child;
    }

    @Override
    public void setChild(Plan child) {
        this.child = child;
    }

    @Override
    public Plan getParent() {
        return parent;
    }

    @Override
    public void setParent(Plan parent) {
        this.parent = parent;
    }
}
