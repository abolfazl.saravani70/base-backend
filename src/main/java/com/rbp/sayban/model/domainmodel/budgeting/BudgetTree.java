/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import com.rbp.sayban.model.domainmodel.project.Project;

import javax.persistence.*;

@Entity
@Table(name = "FIN$BudgetTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_BudgetTree", allocationSize = 1)
public class BudgetTree extends TreeBaseEntity<Budget> {

    private static final long serialVersionUID = 628173540713930874L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Budget child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Budget parent;

    @Override
    public Budget getChild() {
        return child;
    }

    @Override
    public void setChild(Budget child) {
        this.child = child;
    }

    @Override
    public Budget getParent() {
        return parent;
    }

    @Override
    public void setParent(Budget parent) {
        this.parent = parent;
    }
}
