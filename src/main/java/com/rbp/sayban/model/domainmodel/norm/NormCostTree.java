/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.norm;

import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.sayban.model.domainmodel.kpi.Kpi;

import javax.persistence.*;

@Entity
@Table(name = "PLN$NormCostTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_NormCostTree", allocationSize = 1)
public class NormCostTree extends TreeBaseEntity<NormCost> {
    private static final long serialVersionUID = 1830041604736894313L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private NormCost child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private NormCost parent;

    @Override
    public NormCost getChild() {
        return child;
    }

    @Override
    public void setChild(NormCost child) {
        this.child = child;
    }

    @Override
    public NormCost getParent() {
        return parent;
    }

    @Override
    public void setParent(NormCost parent) {
        this.parent = parent;
    }
}
