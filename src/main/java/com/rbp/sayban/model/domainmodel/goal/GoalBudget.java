package com.rbp.sayban.model.domainmodel.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "GOL$GoalBudget")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_GoalBudget", allocationSize = 1)
public class GoalBudget extends BudgetJunctionBaseEntity {

    private static final long serialVersionUID = 2690341247201526845L;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_GOAL_ID")
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }
}

