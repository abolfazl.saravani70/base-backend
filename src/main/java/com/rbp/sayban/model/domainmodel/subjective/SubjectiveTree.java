/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.subjective;

import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;

import javax.persistence.*;

@Entity
@Table(name = "PLN$SubjectiveTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_SubjectiveTree", allocationSize = 1)
public class SubjectiveTree extends TreeBaseEntity<Subjective> {

    private static final long serialVersionUID = 6005176329204778977L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Subjective child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Subjective parent;

    @Override
    public Subjective getChild() {
        return child;
    }

    @Override
    public void setChild(Subjective child) {
        this.child = child;
    }

    @Override
    public Subjective getParent() {
        return parent;
    }

    @Override
    public void setParent(Subjective parent) {
        this.parent = parent;
    }
}
