/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@MappedSuperclass
public abstract class GeneralReportFilter extends BaseEntity {

    private static final long serialVersionUID = -890946262494125790L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "QUERY")
    private String query;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "DEFAULT_VALUE")
    private String defualtValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_FILTER_OPERATOR_ID")
    private FilterOperator filterOperator;


    //------------------ Getter & Setter ------------------//
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDefualtValue() {
        return defualtValue;
    }

    public void setDefualtValue(String defualtValue) {
        this.defualtValue = defualtValue;
    }

    public FilterOperator getFilterOperator() {
        return filterOperator;
    }

    public void setFilterOperator(FilterOperator filterOperator) {
        this.filterOperator = filterOperator;
    }
}
