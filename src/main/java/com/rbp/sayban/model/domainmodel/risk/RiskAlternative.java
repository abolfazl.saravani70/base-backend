package com.rbp.sayban.model.domainmodel.risk;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PLN$RiskAlternative")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_RiskAlternative", allocationSize = 1)
public class RiskAlternative extends BaseEntity {

    private static final long serialVersionUID = 5303940119265413873L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_RISK_ID")
    private Risk risk;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ALTERNATIVE_ID")
    private Alternative alternative;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Risk getRisk() {
        return risk;
    }

    public void setRisk(Risk risk) {
        this.risk = risk;
    }

    public Alternative getAlternative() {
        return alternative;
    }

    public void setAlternative(Alternative alternative) {
        this.alternative = alternative;
    }
}
