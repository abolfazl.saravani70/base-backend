/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.domainmodel.risk;

import com.rbp.core.model.domainmodel.base.abstractClass.TreeBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;

import javax.persistence.*;

@Entity
@Table(name = "PLN$RiskTree")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_RiskTree", allocationSize = 1)
public class RiskTree extends TreeBaseEntity<Risk> {

    private static final long serialVersionUID = 8218355820314266326L;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CHILD_ID")
    private Risk child;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_PARENT_ID")
    private Risk parent;

    @Override
    public Risk getChild() {
        return child;
    }

    @Override
    public void setChild(Risk child) {
        this.child = child;
    }

    @Override
    public Risk getParent() {
        return parent;
    }

    @Override
    public void setParent(Risk parent) {
        this.parent = parent;
    }
}
