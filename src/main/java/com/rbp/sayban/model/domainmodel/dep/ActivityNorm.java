/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.dep;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import com.rbp.sayban.model.domainmodel.kpi.Unit;

import javax.persistence.*;
@Deprecated
@Entity
@Table(name = "PLN$ActivityNorm")
public class ActivityNorm extends BaseEntity {

    private static final long serialVersionUID = -4250897216253809340L;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_NORM_ID")
    private ActivityNorm activityNorm;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_TEMPLATE_ID")
    private ActivityTemplate activityTemplate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_UNIT_ID")
    private Unit activityUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_COST_ID")
    private ActivityCost activityCost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ACTIVITY_ID")
    private Activity activity;

    public ActivityNorm getActivityNorm() {
        return activityNorm;
    }

    public void setActivityNorm(ActivityNorm activityNorm) {
        this.activityNorm = activityNorm;
    }

    public ActivityTemplate getActivityTemplate() {
        return activityTemplate;
    }

    public void setActivityTemplate(ActivityTemplate activityTemplate) {
        this.activityTemplate = activityTemplate;
    }

    public Unit getActivityUnit() {
        return activityUnit;
    }

    public void setActivityUnit(Unit activityUnit) {
        this.activityUnit = activityUnit;
    }

    public ActivityCost getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(ActivityCost activityCost) {
        this.activityCost = activityCost;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
