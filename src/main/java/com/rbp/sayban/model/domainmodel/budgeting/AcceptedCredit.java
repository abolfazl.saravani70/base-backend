/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.domainmodel.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "FIN$AcceptedCredit")
@SequenceGenerator(initialValue = 100, name = "SEQ_GENERATOR", sequenceName = "SEQ_AcceptedCredit", allocationSize = 1)
public class AcceptedCredit extends BaseEntity{

    private static final long serialVersionUID = 5504685151351630567L;

    @Column(name = "LIMIT_AMOUNT")
    private Long limitAmount;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "START_DATE")
    private LocalDate startDate;

    @Column(name = "END_DATE")
    private LocalDate endDate;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "LETTER_NUMBER")
    private String letterNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_FUND_SHARING_ID")
    private FundSharing fundSharing;

    public Long getLimit() {
        return limitAmount;
    }

    public void setLimit(Long limit) {
        this.limitAmount = limit;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLetterNumber() {
        return letterNumber;
    }

    public void setLetterNumber(String letterNumber) {
        this.letterNumber = letterNumber;
    }

    public FundSharing getFundSharing() {
        return fundSharing;
    }

    public void setFundSharing(FundSharing fundSharing) {
        this.fundSharing = fundSharing;
    }
}
