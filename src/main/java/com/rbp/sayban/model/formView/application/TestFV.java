/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.model.domainmodel.system.System;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "TEST_VIEW")
public class TestFV extends BaseEntity {

    @Column(name = "TEST_NAME")
    private String testName;

    @Column(name = "TEST_CODE")
    private String testCode;

    @Column(name = "TEST_RESULT")
    private String testResult;

    private Integer testCaseID;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTestCode() {
        return testCode;
    }

    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public Integer getTestCaseID() {
        return testCaseID;
    }

    public void setTestCaseID(Integer testCaseID) {
        this.testCaseID = testCaseID;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }
}
