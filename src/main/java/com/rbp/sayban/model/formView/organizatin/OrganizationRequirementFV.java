/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "ORG_REQUIREMENT_VIEW")
public class OrganizationRequirementFV extends BaseEntity {

    @Column(name = "NUMBER$")
    private Long number;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "ATART_DATE")
    private LocalDate startDate;

    @Column(name = "END_DATE")
    private LocalDate endDate;

    @Column(name = "FK_REQUIREMENT_ID")
    private Long requirementId;

    @Column(name = "REQUIREMENT_NAME")
    private String requirementName;

    @Column(name = "REQUIREMENT_TITLE")
    private String requirementTitle;

    @Column(name = "REQUIREMENT_KEY")
    private String requirementKey;

    @Column(name = "REQUIREMENT_VALUE")
    private Long requirementValue;

    @Column(name = "REQUIREMENT_PRIORITY")
    private Integer requirementPriority;

    @Column(name = "REQUIREMENT_IMPORTANCE")
    private Double requirementImportance;

    @Column(name = "REQUIREMENT_FK_PV_TYPE_ID")
    private Long requirementPvTypeId;

    @Column(name = "REQUIREMENT_PV_TYPE_TITLE")
    private String requirementPvTypeTitle;

    @Column(name = "FK_ORG_ID")
    private Long organizationId;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }

    public String getRequirementName() {
        return requirementName;
    }

    public void setRequirementName(String requirementName) {
        this.requirementName = requirementName;
    }

    public String getRequirementTitle() {
        return requirementTitle;
    }

    public void setRequirementTitle(String requirementTitle) {
        this.requirementTitle = requirementTitle;
    }

    public String getRequirementKey() {
        return requirementKey;
    }

    public void setRequirementKey(String requirementKey) {
        this.requirementKey = requirementKey;
    }

    public Long getRequirementValue() {
        return requirementValue;
    }

    public void setRequirementValue(Long requirementValue) {
        this.requirementValue = requirementValue;
    }

    public Integer getRequirementPriority() {
        return requirementPriority;
    }

    public void setRequirementPriority(Integer requirementPriority) {
        this.requirementPriority = requirementPriority;
    }

    public Double getRequirementImportance() {
        return requirementImportance;
    }

    public void setRequirementImportance(Double requirementImportance) {
        this.requirementImportance = requirementImportance;
    }

    public Long getRequirementPvTypeId() {
        return requirementPvTypeId;
    }

    public void setRequirementPvTypeId(Long requirementPvTypeId) {
        this.requirementPvTypeId = requirementPvTypeId;
    }

    public String getRequirementPvTypeTitle() {
        return requirementPvTypeTitle;
    }

    public void setRequirementPvTypeTitle(String requirementPvTypeTitle) {
        this.requirementPvTypeTitle = requirementPvTypeTitle;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
}
