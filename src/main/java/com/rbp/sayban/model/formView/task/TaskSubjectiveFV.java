/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.task;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.SubjectiveJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.task.Task;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TASK_SUBJECTIVE_VIEW")
public class TaskSubjectiveFV extends EvalStateBaseEntity {

    @Column(name = "FK_TASK_ID")
    private Long taskId;

    @Column(name = "TASK_FK_PV_TYPE_ID")
    private Long taskPvTypeId;

    @Column(name = "TASK_PV_TYPE_TITLE")
    private String taskPvTypeTitle;

    @Column(name = "TASK_NAME")
    private String taskName;

    @Column(name = "TASK_TITLE")
    private String taskTitle;

    @Column(name = "TASK_IS_ROOT")
    private Boolean taskIsRoot;

    @Column(name = "TASK_INITIAL_PERCENTAGE")
    private Double taskInitialPercentage;

    @Column(name = "TASK_PERCENTAGE")
    private Double taskPercentage;

    @Column(name = "TASK_WEIGHT")
    private Long taskWeight;

    @Column(name = "TASK_VOLUME")
    private Long taskVolume;

    @Column(name = "TASK_PRIORITY")
    private Integer taskPriority;

    @Column(name = "TASK_IMPORTANCE")
    private Double taskImportance;

    @Column(name = "TASK_ACTUAL_START_DATE")
    private LocalDate taskActualStartDate;

    @Column(name = "TASK_ACTUAL_END_DATE")
    private LocalDate taskActualEndDate;

    @Column(name = "TASK_EXPIRATION_DATE")
    private LocalDate taskExpirationDate;

    @Column(name = "TASK_START_DATE")
    private LocalDate taskStartDate;

    @Column(name = "TASK_END_DATE")
    private LocalDate taskEndDate;

    @Column(name = "TASK_FISCAL_YEAR")
    private String taskFiscalYear;

    @Column(name = "TASK_LEVEL$")
    private Long taskLevel;

    @Column(name = "PROJECT_LEVEL$")
    private Long projectLevel;

    @Column(name = "FK_SUBJECTIVE_ID")
    private Long subjectiveId;

    @Column(name = "SUBJECTIVE_NAME")
    private String subjectiveName;

    @Column(name = "SUBJECTIVE_TITLE")
    private String subjectiveTitle;

    @Column(name = "SUBJECTIVE_IS_ROOT")
    private Boolean subjectiveIsRoot;

    @Column(name = "SUBJECTIVE_INITIAL_PERCENTAGE")
    private Double subjectiveInitialPercentage;

    @Column(name = "SUBJECTIVE_PERCENTAGE")
    private Double subjectivePercentage;

    @Column(name = "SUBJECTIVE_WEIGHT")
    private Long subjectiveWeight;

    @Column(name = "SUBJECTIVE_VOLUME")
    private Long subjectiveVolume;

    @Column(name = "SUBJECTIVE_PRIORITY")
    private Integer subjectivePriority;

    @Column(name = "SUBJECTIVE_IMPORTANCE")
    private Double subjectiveImportance;

    @Column(name = "SUBJECTIVE_ACTUAL_START_DATE")
    private LocalDate subjectiveActualStartDate;

    @Column(name = "SUBJECTIVE_ACTUAL_END_DATE")
    private LocalDate subjectiveActualEndDate;

    @Column(name = "SUBJECTIVE_EXPIRATION_DATE")
    private LocalDate subjectiveExpirationDate;

    @Column(name = "SUBJECTIVE_START_DATE")
    private LocalDate subjectiveStartDate;

    @Column(name = "SUBJECTIVE_END_DATE")
    private LocalDate subjectiveEndDate;

    @Column(name = "SUBJECTIVE_FISCAL_YEAR")
    private String subjectiveFiscalYear;

    @Column(name = "SUBJECTIVE_LEVEL$")
    private Long subjectiveLevel;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskPvTypeId() {
        return taskPvTypeId;
    }

    public void setTaskPvTypeId(Long taskPvTypeId) {
        this.taskPvTypeId = taskPvTypeId;
    }

    public String getTaskPvTypeTitle() {
        return taskPvTypeTitle;
    }

    public void setTaskPvTypeTitle(String taskPvTypeTitle) {
        this.taskPvTypeTitle = taskPvTypeTitle;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public Boolean getTaskIsRoot() {
        return taskIsRoot;
    }

    public void setTaskIsRoot(Boolean taskIsRoot) {
        this.taskIsRoot = taskIsRoot;
    }

    public Double getTaskInitialPercentage() {
        return taskInitialPercentage;
    }

    public void setTaskInitialPercentage(Double taskInitialPercentage) {
        this.taskInitialPercentage = taskInitialPercentage;
    }

    public Double getTaskPercentage() {
        return taskPercentage;
    }

    public void setTaskPercentage(Double taskPercentage) {
        this.taskPercentage = taskPercentage;
    }

    public Long getTaskWeight() {
        return taskWeight;
    }

    public void setTaskWeight(Long taskWeight) {
        this.taskWeight = taskWeight;
    }

    public Long getTaskVolume() {
        return taskVolume;
    }

    public void setTaskVolume(Long taskVolume) {
        this.taskVolume = taskVolume;
    }

    public Integer getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
    }

    public Double getTaskImportance() {
        return taskImportance;
    }

    public void setTaskImportance(Double taskImportance) {
        this.taskImportance = taskImportance;
    }

    public LocalDate getTaskActualStartDate() {
        return taskActualStartDate;
    }

    public void setTaskActualStartDate(LocalDate taskActualStartDate) {
        this.taskActualStartDate = taskActualStartDate;
    }

    public LocalDate getTaskActualEndDate() {
        return taskActualEndDate;
    }

    public void setTaskActualEndDate(LocalDate taskActualEndDate) {
        this.taskActualEndDate = taskActualEndDate;
    }

    public LocalDate getTaskExpirationDate() {
        return taskExpirationDate;
    }

    public void setTaskExpirationDate(LocalDate taskExpirationDate) {
        this.taskExpirationDate = taskExpirationDate;
    }

    public LocalDate getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(LocalDate taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public LocalDate getTaskEndDate() {
        return taskEndDate;
    }

    public void setTaskEndDate(LocalDate taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    public String getTaskFiscalYear() {
        return taskFiscalYear;
    }

    public void setTaskFiscalYear(String taskFiscalYear) {
        this.taskFiscalYear = taskFiscalYear;
    }

    public Long getTaskLevel() {
        return taskLevel;
    }

    public void setTaskLevel(Long taskLevel) {
        this.taskLevel = taskLevel;
    }

    public Long getProjectLevel() {
        return projectLevel;
    }

    public void setProjectLevel(Long projectLevel) {
        this.projectLevel = projectLevel;
    }

    public Long getSubjectiveId() {
        return subjectiveId;
    }

    public void setSubjectiveId(Long subjectiveId) {
        this.subjectiveId = subjectiveId;
    }

    public String getSubjectiveName() {
        return subjectiveName;
    }

    public void setSubjectiveName(String subjectiveName) {
        this.subjectiveName = subjectiveName;
    }

    public String getSubjectiveTitle() {
        return subjectiveTitle;
    }

    public void setSubjectiveTitle(String subjectiveTitle) {
        this.subjectiveTitle = subjectiveTitle;
    }

    public Boolean getSubjectiveIsRoot() {
        return subjectiveIsRoot;
    }

    public void setSubjectiveIsRoot(Boolean subjectiveIsRoot) {
        this.subjectiveIsRoot = subjectiveIsRoot;
    }

    public Double getSubjectiveInitialPercentage() {
        return subjectiveInitialPercentage;
    }

    public void setSubjectiveInitialPercentage(Double subjectiveInitialPercentage) {
        this.subjectiveInitialPercentage = subjectiveInitialPercentage;
    }

    public Double getSubjectivePercentage() {
        return subjectivePercentage;
    }

    public void setSubjectivePercentage(Double subjectivePercentage) {
        this.subjectivePercentage = subjectivePercentage;
    }

    public Long getSubjectiveWeight() {
        return subjectiveWeight;
    }

    public void setSubjectiveWeight(Long subjectiveWeight) {
        this.subjectiveWeight = subjectiveWeight;
    }

    public Long getSubjectiveVolume() {
        return subjectiveVolume;
    }

    public void setSubjectiveVolume(Long subjectiveVolume) {
        this.subjectiveVolume = subjectiveVolume;
    }

    public Integer getSubjectivePriority() {
        return subjectivePriority;
    }

    public void setSubjectivePriority(Integer subjectivePriority) {
        this.subjectivePriority = subjectivePriority;
    }

    public Double getSubjectiveImportance() {
        return subjectiveImportance;
    }

    public void setSubjectiveImportance(Double subjectiveImportance) {
        this.subjectiveImportance = subjectiveImportance;
    }

    public LocalDate getSubjectiveActualStartDate() {
        return subjectiveActualStartDate;
    }

    public void setSubjectiveActualStartDate(LocalDate subjectiveActualStartDate) {
        this.subjectiveActualStartDate = subjectiveActualStartDate;
    }

    public LocalDate getSubjectiveActualEndDate() {
        return subjectiveActualEndDate;
    }

    public void setSubjectiveActualEndDate(LocalDate subjectiveActualEndDate) {
        this.subjectiveActualEndDate = subjectiveActualEndDate;
    }

    public LocalDate getSubjectiveExpirationDate() {
        return subjectiveExpirationDate;
    }

    public void setSubjectiveExpirationDate(LocalDate subjectiveExpirationDate) {
        this.subjectiveExpirationDate = subjectiveExpirationDate;
    }

    public LocalDate getSubjectiveStartDate() {
        return subjectiveStartDate;
    }

    public void setSubjectiveStartDate(LocalDate subjectiveStartDate) {
        this.subjectiveStartDate = subjectiveStartDate;
    }

    public LocalDate getSubjectiveEndDate() {
        return subjectiveEndDate;
    }

    public void setSubjectiveEndDate(LocalDate subjectiveEndDate) {
        this.subjectiveEndDate = subjectiveEndDate;
    }

    public String getSubjectiveFiscalYear() {
        return subjectiveFiscalYear;
    }

    public void setSubjectiveFiscalYear(String subjectiveFiscalYear) {
        this.subjectiveFiscalYear = subjectiveFiscalYear;
    }

    public Long getSubjectiveLevel() {
        return subjectiveLevel;
    }

    public void setSubjectiveLevel(Long subjectiveLevel) {
        this.subjectiveLevel = subjectiveLevel;
    }
}
