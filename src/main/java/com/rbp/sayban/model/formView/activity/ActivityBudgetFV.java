package com.rbp.sayban.model.formView.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "ACTIVITY_BUDGET_VIEW")
public class ActivityBudgetFV extends BaseEntity {

    @Column(name = "ESTIMATE_BUDGET")
    private Long estimateBudget;

    @Column(name = "ACTUAL_BUDGET")
    private Long actualBudget;

    @Column(name = "FK_BUDGET_ID")
    private Long budgetId;

    @Column(name = "BUDGET_NAME")
    private String budgetName;

    @Column(name = "BUDGET_TITLE")
    private String budgetTitle;

    @Column(name = "BUDGET_IS_ROOT")
    private Boolean budgetIsRoot;

    @Column(name = "BUDGET_INITIAL_PERCENTAGE")
    private Double budgetInitialPercentage;

    @Column(name = "BUDGET_PERCENTAGE")
    private Double budgetPercentage;

    @Column(name = "BUDGET_WEIGHT")
    private Long budgetWeight;

    @Column(name = "BUDGET_VOLUME")
    private Long budgetVolume;

    @Column(name = "BUDGET_PRIORITY")
    private Integer budgetPriority;

    @Column(name = "BUDGET_IMPORTANCE")
    private Double budgetImportance;

    @Column(name = "BUDGET_ACTUAL_START_DATE")
    private LocalDate budgetActualStartDate;

    @Column(name = "BUDGET_ACTUAL_END_DATE")
    private LocalDate budgetActualEndDate;

    @Column(name = "BUDGET_EXPIRATION_DATE")
    private LocalDate budgetExpirationDate;

    @Column(name = "BUDGET_START_DATE")
    private LocalDate budgetStartDate;

    @Column(name = "BUDGET_END_DATE")
    private LocalDate budgetEndDate;

    @Column(name = "BUDGET_FISCAL_YEAR")
    private String budgetFiscalYear;

    @Column(name = "BUDGET_LEVEL$")
    private Long budgetLevel;

    @Column(name = "FK_ACTIVITY_ID")
    private Long activityId;

    @Column(name = "ACTIVITY_FK_PV_TYPE_ID")
    private Long activityPvTypeId;

    @Column(name = "ACTIVITY_PV_TYPE_TITLE")
    private String activityPvTypeTitle;

    @Column(name = "ACTIVITY_NAME")
    private String activityName;

    @Column(name = "ACTIVITY_TITLE")
    private String activityTitle;

    @Column(name = "ACTIVITY_IS_ROOT")
    private Boolean activityIsRoot;

    @Column(name = "ACTIVITY_INITIAL_PERCENTAGE")
    private Double activityInitialPercentage;

    @Column(name = "ACTIVITY_PERCENTAGE")
    private Double activityPercentage;

    @Column(name = "ACTIVITY_WEIGHT")
    private Long activityWeight;

    @Column(name = "ACTIVITY_VOLUME")
    private Long activityVolume;

    @Column(name = "ACTIVITY_PRIORITY")
    private Integer activityPriority;

    @Column(name = "ACTIVITY_IMPORTANCE")
    private Double activityImportance;

    @Column(name = "ACTIVITY_ACTUAL_START_DATE")
    private LocalDate activityActualStartDate;

    @Column(name = "ACTIVITY_ACTUAL_END_DATE")
    private LocalDate activityActualEndDate;

    @Column(name = "ACTIVITY_EXPIRATION_DATE")
    private LocalDate activityExpirationDate;

    @Column(name = "ACTIVITY_START_DATE")
    private LocalDate activityStartDate;

    @Column(name = "ACTIVITY_END_DATE")
    private LocalDate activityEndDate;

    @Column(name = "ACTIVITY_FISCAL_YEAR")
    private String activityFiscalYear;

    @Column(name = "ACTIVITY_LEVEL$")
    private Long activityLevel;

    public Long getEstimateBudget() {
        return estimateBudget;
    }

    public void setEstimateBudget(Long estimateBudget) {
        this.estimateBudget = estimateBudget;
    }

    public Long getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Long actualBudget) {
        this.actualBudget = actualBudget;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgetTitle() {
        return budgetTitle;
    }

    public void setBudgetTitle(String budgetTitle) {
        this.budgetTitle = budgetTitle;
    }

    public Boolean getBudgetIsRoot() {
        return budgetIsRoot;
    }

    public void setBudgetIsRoot(Boolean budgetIsRoot) {
        this.budgetIsRoot = budgetIsRoot;
    }

    public Double getBudgetInitialPercentage() {
        return budgetInitialPercentage;
    }

    public void setBudgetInitialPercentage(Double budgetInitialPercentage) {
        this.budgetInitialPercentage = budgetInitialPercentage;
    }

    public Double getBudgetPercentage() {
        return budgetPercentage;
    }

    public void setBudgetPercentage(Double budgetPercentage) {
        this.budgetPercentage = budgetPercentage;
    }

    public Long getBudgetWeight() {
        return budgetWeight;
    }

    public void setBudgetWeight(Long budgetWeight) {
        this.budgetWeight = budgetWeight;
    }

    public Long getBudgetVolume() {
        return budgetVolume;
    }

    public void setBudgetVolume(Long budgetVolume) {
        this.budgetVolume = budgetVolume;
    }

    public Integer getBudgetPriority() {
        return budgetPriority;
    }

    public void setBudgetPriority(Integer budgetPriority) {
        this.budgetPriority = budgetPriority;
    }

    public Double getBudgetImportance() {
        return budgetImportance;
    }

    public void setBudgetImportance(Double budgetImportance) {
        this.budgetImportance = budgetImportance;
    }

    public LocalDate getBudgetActualStartDate() {
        return budgetActualStartDate;
    }

    public void setBudgetActualStartDate(LocalDate budgetActualStartDate) {
        this.budgetActualStartDate = budgetActualStartDate;
    }

    public LocalDate getBudgetActualEndDate() {
        return budgetActualEndDate;
    }

    public void setBudgetActualEndDate(LocalDate budgetActualEndDate) {
        this.budgetActualEndDate = budgetActualEndDate;
    }

    public LocalDate getBudgetExpirationDate() {
        return budgetExpirationDate;
    }

    public void setBudgetExpirationDate(LocalDate budgetExpirationDate) {
        this.budgetExpirationDate = budgetExpirationDate;
    }

    public LocalDate getBudgetStartDate() {
        return budgetStartDate;
    }

    public void setBudgetStartDate(LocalDate budgetStartDate) {
        this.budgetStartDate = budgetStartDate;
    }

    public LocalDate getBudgetEndDate() {
        return budgetEndDate;
    }

    public void setBudgetEndDate(LocalDate budgetEndDate) {
        this.budgetEndDate = budgetEndDate;
    }

    public String getBudgetFiscalYear() {
        return budgetFiscalYear;
    }

    public void setBudgetFiscalYear(String budgetFiscalYear) {
        this.budgetFiscalYear = budgetFiscalYear;
    }

    public Long getBudgetLevel() {
        return budgetLevel;
    }

    public void setBudgetLevel(Long budgetLevel) {
        this.budgetLevel = budgetLevel;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getActivityPvTypeId() {
        return activityPvTypeId;
    }

    public void setActivityPvTypeId(Long activityPvTypeId) {
        this.activityPvTypeId = activityPvTypeId;
    }

    public String getActivityPvTypeTitle() {
        return activityPvTypeTitle;
    }

    public void setActivityPvTypeTitle(String activityPvTypeTitle) {
        this.activityPvTypeTitle = activityPvTypeTitle;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public Boolean getActivityIsRoot() {
        return activityIsRoot;
    }

    public void setActivityIsRoot(Boolean activityIsRoot) {
        this.activityIsRoot = activityIsRoot;
    }

    public Double getActivityInitialPercentage() {
        return activityInitialPercentage;
    }

    public void setActivityInitialPercentage(Double activityInitialPercentage) {
        this.activityInitialPercentage = activityInitialPercentage;
    }

    public Double getActivityPercentage() {
        return activityPercentage;
    }

    public void setActivityPercentage(Double activityPercentage) {
        this.activityPercentage = activityPercentage;
    }

    public Long getActivityWeight() {
        return activityWeight;
    }

    public void setActivityWeight(Long activityWeight) {
        this.activityWeight = activityWeight;
    }

    public Long getActivityVolume() {
        return activityVolume;
    }

    public void setActivityVolume(Long activityVolume) {
        this.activityVolume = activityVolume;
    }

    public Integer getActivityPriority() {
        return activityPriority;
    }

    public void setActivityPriority(Integer activityPriority) {
        this.activityPriority = activityPriority;
    }

    public Double getActivityImportance() {
        return activityImportance;
    }

    public void setActivityImportance(Double activityImportance) {
        this.activityImportance = activityImportance;
    }

    public LocalDate getActivityActualStartDate() {
        return activityActualStartDate;
    }

    public void setActivityActualStartDate(LocalDate activityActualStartDate) {
        this.activityActualStartDate = activityActualStartDate;
    }

    public LocalDate getActivityActualEndDate() {
        return activityActualEndDate;
    }

    public void setActivityActualEndDate(LocalDate activityActualEndDate) {
        this.activityActualEndDate = activityActualEndDate;
    }

    public LocalDate getActivityExpirationDate() {
        return activityExpirationDate;
    }

    public void setActivityExpirationDate(LocalDate activityExpirationDate) {
        this.activityExpirationDate = activityExpirationDate;
    }

    public LocalDate getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(LocalDate activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public LocalDate getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(LocalDate activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityFiscalYear() {
        return activityFiscalYear;
    }

    public void setActivityFiscalYear(String activityFiscalYear) {
        this.activityFiscalYear = activityFiscalYear;
    }

    public Long getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Long activityLevel) {
        this.activityLevel = activityLevel;
    }
}
