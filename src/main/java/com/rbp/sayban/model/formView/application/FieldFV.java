/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "FIELD_VIEW")
public class FieldFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "ENGLISH_NAME")
    private String engName;

    @Column(name = "KEY")
    private Integer key;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    @Column(name = "FK_ENTITY_ID")
    private Long appEntityId;

    @Column(name = "APP_ENTITY_ENTITY_NAME")
    private String appEntityEntityName;

    @Column(name = "APP_ENTITY_ENTITY_KEYWORD")
    private String appEntityEntityKeyword;

    @Column(name = "APP_ENTITY_ENTITY_TYPE")
    private String appEntityEntityType;

    @Column(name = "APP_ENTITY_ENTITY_CODE")
    private String appEntityEntityCode;

    @Column(name = "APP_ENTITY_ENTITY_IS_DEPRECATED")
    private Boolean appEntityIsDeprecated;

    @Column(name = "APP_ENTITY_ENTITY_REGULAR_EXP")
    private String appEntityRegularExp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        isDeprecated = deprecated;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }

    public Long getAppEntityId() {
        return appEntityId;
    }

    public void setAppEntityId(Long appEntityId) {
        this.appEntityId = appEntityId;
    }

    public String getAppEntityEntityName() {
        return appEntityEntityName;
    }

    public void setAppEntityEntityName(String appEntityEntityName) {
        this.appEntityEntityName = appEntityEntityName;
    }

    public String getAppEntityEntityKeyword() {
        return appEntityEntityKeyword;
    }

    public void setAppEntityEntityKeyword(String appEntityEntityKeyword) {
        this.appEntityEntityKeyword = appEntityEntityKeyword;
    }

    public String getAppEntityEntityType() {
        return appEntityEntityType;
    }

    public void setAppEntityEntityType(String appEntityEntityType) {
        this.appEntityEntityType = appEntityEntityType;
    }

    public String getAppEntityEntityCode() {
        return appEntityEntityCode;
    }

    public void setAppEntityEntityCode(String appEntityEntityCode) {
        this.appEntityEntityCode = appEntityEntityCode;
    }

    public Boolean getAppEntityIsDeprecated() {
        return appEntityIsDeprecated;
    }

    public void setAppEntityIsDeprecated(Boolean appEntityIsDeprecated) {
        this.appEntityIsDeprecated = appEntityIsDeprecated;
    }

    public String getAppEntityRegularExp() {
        return appEntityRegularExp;
    }

    public void setAppEntityRegularExp(String appEntityRegularExp) {
        this.appEntityRegularExp = appEntityRegularExp;
    }
}
