package com.rbp.sayban.model.formView.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.Budget;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "OBJECTIVE_BUDGET_VIEW")
public class ObjectiveBudgetFV extends EvalStateBaseEntity {

    @Column(name = "ESTIMATE_BUDGET")
    private Long estimateBudget;

    @Column(name = "ACTUAL_BUDGET")
    private Long actualBudget;

    @Column(name = "FK_OBJECTIVE_ID")
    private Long objectiveId;

    @Column(name = "OBJECTIVE_FK_PV_TYPE_ID")
    private Long objectivePvTypeId;

    @Column(name = "OBJECTIVE_FK_PV_TYPE_TITLE")
    private String objectivePvTypeTitle;

    @Column(name = "OBJECTIVE_NAME")
    private String objectiveName;

    @Column(name = "OBJECTIVE_TITLE")
    private String objectiveTitle;

    @Column(name = "OBJECTIVE_IS_ROOT")
    private Boolean objectiveIsRoot;

    @Column(name = "OBJECTIVE_INITIAL_PERCENTAGE")
    private Double objectiveInitialPercentage;

    @Column(name = "OBJECTIVE_PERCENTAGE")
    private Double objectivePercentage;

    @Column(name = "OBJECTIVE_WEIGHT")
    private Long objectiveWeight;

    @Column(name = "OBJECTIVE_VOLUME")
    private Long objectiveVolume;

    @Column(name = "OBJECTIVE_PRIORITY")
    private Integer objectivePriority;

    @Column(name = "OBJECTIVE_IMPORTANCE")
    private Double objectiveImportance;

    @Column(name = "OBJECTIVE_ACTUAL_START_DATE")
    private LocalDate objectiveActualStartDate;

    @Column(name = "OBJECTIVE_ACTUAL_END_DATE")
    private LocalDate objectiveActualEndDate;

    @Column(name = "OBJECTIVE_EXPIRATION_DATE")
    private LocalDate objectiveExpirationDate;

    @Column(name = "OBJECTIVE_START_DATE")
    private LocalDate objectiveStartDate;

    @Column(name = "OBJECTIVE_END_DATE")
    private LocalDate objectiveEndDate;

    @Column(name = "OBJECTIVE_FISCAL_YEAR")
    private String objectiveFiscalYear;

    @Column(name = "OBJECTIVE_LEVEL$")
    private Long objectiveLevel;

    @Column(name = "FK_BUDGET_ID")
    private Long budgetId;

    @Column(name = "BUDGET_NAME")
    private String budgetName;

    @Column(name = "BUDGET_TITLE")
    private String budgetTitle;

    @Column(name = "BUDGET_IS_ROOT")
    private Boolean budgetIsRoot;

    @Column(name = "BUDGET_INITIAL_PERCENTAGE")
    private Double budgetInitialPercentage;

    @Column(name = "BUDGET_PERCENTAGE")
    private Double budgetPercentage;

    @Column(name = "BUDGET_WEIGHT")
    private Long budgetWeight;

    @Column(name = "BUDGET_VOLUME")
    private Long budgetVolume;

    @Column(name = "BUDGET_PRIORITY")
    private Integer budgetPriority;

    @Column(name = "BUDGET_IMPORTANCE")
    private Double budgetImportance;

    @Column(name = "BUDGET_ACTUAL_START_DATE")
    private LocalDate budgetActualStartDate;

    @Column(name = "BUDGET_ACTUAL_END_DATE")
    private LocalDate budgetActualEndDate;

    @Column(name = "BUDGET_BUDGET_EXPIRATION_DATE")
    private LocalDate budgetExpirationDate;

    @Column(name = "BUDGET_START_DATE")
    private LocalDate budgetStartDate;

    @Column(name = "BUDGET_END_DATE")
    private LocalDate budgetEndDate;

    @Column(name = "BUDGET_FISCAL_YEAR")
    private String budgetFiscalYear;

    @Column(name = "BUDGET_LEVEL$")
    private Long budgetLevel;

    public Long getEstimateBudget() {
        return estimateBudget;
    }

    public void setEstimateBudget(Long estimateBudget) {
        this.estimateBudget = estimateBudget;
    }

    public Long getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Long actualBudget) {
        this.actualBudget = actualBudget;
    }

    public Long getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }

    public Long getObjectivePvTypeId() {
        return objectivePvTypeId;
    }

    public void setObjectivePvTypeId(Long objectivePvTypeId) {
        this.objectivePvTypeId = objectivePvTypeId;
    }

    public String getObjectivePvTypeTitle() {
        return objectivePvTypeTitle;
    }

    public void setObjectivePvTypeTitle(String objectivePvTypeTitle) {
        this.objectivePvTypeTitle = objectivePvTypeTitle;
    }

    public String getObjectiveName() {
        return objectiveName;
    }

    public void setObjectiveName(String objectiveName) {
        this.objectiveName = objectiveName;
    }

    public String getObjectiveTitle() {
        return objectiveTitle;
    }

    public void setObjectiveTitle(String objectiveTitle) {
        this.objectiveTitle = objectiveTitle;
    }

    public Boolean getObjectiveIsRoot() {
        return objectiveIsRoot;
    }

    public void setObjectiveIsRoot(Boolean objectiveIsRoot) {
        this.objectiveIsRoot = objectiveIsRoot;
    }

    public Double getObjectiveInitialPercentage() {
        return objectiveInitialPercentage;
    }

    public void setObjectiveInitialPercentage(Double objectiveInitialPercentage) {
        this.objectiveInitialPercentage = objectiveInitialPercentage;
    }

    public Double getObjectivePercentage() {
        return objectivePercentage;
    }

    public void setObjectivePercentage(Double objectivePercentage) {
        this.objectivePercentage = objectivePercentage;
    }

    public Long getObjectiveWeight() {
        return objectiveWeight;
    }

    public void setObjectiveWeight(Long objectiveWeight) {
        this.objectiveWeight = objectiveWeight;
    }

    public Long getObjectiveVolume() {
        return objectiveVolume;
    }

    public void setObjectiveVolume(Long objectiveVolume) {
        this.objectiveVolume = objectiveVolume;
    }

    public Integer getObjectivePriority() {
        return objectivePriority;
    }

    public void setObjectivePriority(Integer objectivePriority) {
        this.objectivePriority = objectivePriority;
    }

    public Double getObjectiveImportance() {
        return objectiveImportance;
    }

    public void setObjectiveImportance(Double objectiveImportance) {
        this.objectiveImportance = objectiveImportance;
    }

    public LocalDate getObjectiveActualStartDate() {
        return objectiveActualStartDate;
    }

    public void setObjectiveActualStartDate(LocalDate objectiveActualStartDate) {
        this.objectiveActualStartDate = objectiveActualStartDate;
    }

    public LocalDate getObjectiveActualEndDate() {
        return objectiveActualEndDate;
    }

    public void setObjectiveActualEndDate(LocalDate objectiveActualEndDate) {
        this.objectiveActualEndDate = objectiveActualEndDate;
    }

    public LocalDate getObjectiveExpirationDate() {
        return objectiveExpirationDate;
    }

    public void setObjectiveExpirationDate(LocalDate objectiveExpirationDate) {
        this.objectiveExpirationDate = objectiveExpirationDate;
    }

    public LocalDate getObjectiveStartDate() {
        return objectiveStartDate;
    }

    public void setObjectiveStartDate(LocalDate objectiveStartDate) {
        this.objectiveStartDate = objectiveStartDate;
    }

    public LocalDate getObjectiveEndDate() {
        return objectiveEndDate;
    }

    public void setObjectiveEndDate(LocalDate objectiveEndDate) {
        this.objectiveEndDate = objectiveEndDate;
    }

    public String getObjectiveFiscalYear() {
        return objectiveFiscalYear;
    }

    public void setObjectiveFiscalYear(String objectiveFiscalYear) {
        this.objectiveFiscalYear = objectiveFiscalYear;
    }

    public Long getObjectiveLevel() {
        return objectiveLevel;
    }

    public void setObjectiveLevel(Long objectiveLevel) {
        this.objectiveLevel = objectiveLevel;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgetTitle() {
        return budgetTitle;
    }

    public void setBudgetTitle(String budgetTitle) {
        this.budgetTitle = budgetTitle;
    }

    public Boolean getBudgetIsRoot() {
        return budgetIsRoot;
    }

    public void setBudgetIsRoot(Boolean budgetIsRoot) {
        this.budgetIsRoot = budgetIsRoot;
    }

    public Double getBudgetInitialPercentage() {
        return budgetInitialPercentage;
    }

    public void setBudgetInitialPercentage(Double budgetInitialPercentage) {
        this.budgetInitialPercentage = budgetInitialPercentage;
    }

    public Double getBudgetPercentage() {
        return budgetPercentage;
    }

    public void setBudgetPercentage(Double budgetPercentage) {
        this.budgetPercentage = budgetPercentage;
    }

    public Long getBudgetWeight() {
        return budgetWeight;
    }

    public void setBudgetWeight(Long budgetWeight) {
        this.budgetWeight = budgetWeight;
    }

    public Long getBudgetVolume() {
        return budgetVolume;
    }

    public void setBudgetVolume(Long budgetVolume) {
        this.budgetVolume = budgetVolume;
    }

    public Integer getBudgetPriority() {
        return budgetPriority;
    }

    public void setBudgetPriority(Integer budgetPriority) {
        this.budgetPriority = budgetPriority;
    }

    public Double getBudgetImportance() {
        return budgetImportance;
    }

    public void setBudgetImportance(Double budgetImportance) {
        this.budgetImportance = budgetImportance;
    }

    public LocalDate getBudgetActualStartDate() {
        return budgetActualStartDate;
    }

    public void setBudgetActualStartDate(LocalDate budgetActualStartDate) {
        this.budgetActualStartDate = budgetActualStartDate;
    }

    public LocalDate getBudgetActualEndDate() {
        return budgetActualEndDate;
    }

    public void setBudgetActualEndDate(LocalDate budgetActualEndDate) {
        this.budgetActualEndDate = budgetActualEndDate;
    }

    public LocalDate getBudgetExpirationDate() {
        return budgetExpirationDate;
    }

    public void setBudgetExpirationDate(LocalDate budgetExpirationDate) {
        this.budgetExpirationDate = budgetExpirationDate;
    }

    public LocalDate getBudgetStartDate() {
        return budgetStartDate;
    }

    public void setBudgetStartDate(LocalDate budgetStartDate) {
        this.budgetStartDate = budgetStartDate;
    }

    public LocalDate getBudgetEndDate() {
        return budgetEndDate;
    }

    public void setBudgetEndDate(LocalDate budgetEndDate) {
        this.budgetEndDate = budgetEndDate;
    }

    public String getBudgetFiscalYear() {
        return budgetFiscalYear;
    }

    public void setBudgetFiscalYear(String budgetFiscalYear) {
        this.budgetFiscalYear = budgetFiscalYear;
    }

    public Long getBudgetLevel() {
        return budgetLevel;
    }

    public void setBudgetLevel(Long budgetLevel) {
        this.budgetLevel = budgetLevel;
    }
}
