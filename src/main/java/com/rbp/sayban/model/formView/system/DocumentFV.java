/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.DocumentDetail;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

@Entity
@Immutable
@Table(name = "DOCUMENT_VIEW")
public class DocumentFV extends BaseEntity {


    @Column(name = "IS_TEMP")
    private Boolean isTemp;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "FK_PV_DOC_STATE_ID")
    private Long pvDocumentStateId;

    @Column(name = "PV_DOC_STATE_TITLE")
    private String  pvDocumentStateTitle;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "FK_BLOB_ID")
    private Long blobId;

    @Lob
    @Column(name = "BLOB_FILE$")
    private java.sql.Blob blobFile;

    @Column(name = "BLOB_FILE_TYPE")
    private String blobFileType;

    @Column(name = "BLOB_FILE_NAME")
    private String blobFileName;

    public Boolean getTemp() {
        return isTemp;
    }

    public void setTemp(Boolean temp) {
        isTemp = temp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPvDocumentStateTitle() {
        return pvDocumentStateTitle;
    }

    public void setPvDocumentStateTitle(String pvDocumentStateTitle) {
        this.pvDocumentStateTitle = pvDocumentStateTitle;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getPvDocumentStateId() {
        return pvDocumentStateId;
    }

    public void setPvDocumentStateId(Long pvDocumentStateId) {
        this.pvDocumentStateId = pvDocumentStateId;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public Long getBlobId() {
        return blobId;
    }

    public void setBlobId(Long blobId) {
        this.blobId = blobId;
    }

    public Blob getBlobFile() {
        return blobFile;
    }

    public void setBlobFile(Blob blobFile) {
        this.blobFile = blobFile;
    }

    public String getBlobFileType() {
        return blobFileType;
    }

    public void setBlobFileType(String blobFileType) {
        this.blobFileType = blobFileType;
    }

    public String getBlobFileName() {
        return blobFileName;
    }

    public void setBlobFileName(String blobFileName) {
        this.blobFileName = blobFileName;
    }
}
