package com.rbp.sayban.model.formView.risk;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.risk.Alternative;
import com.rbp.sayban.model.domainmodel.risk.Risk;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "RISK_ALTERNATIVE_VIEW")
public class RiskAlternativeFV extends BaseEntity {

    @Column(name = "FK_RISK_ID")
    private Long riskId;

    @Column(name = "FK_ALTERNATIVE_ID")
    private Long alternativeId;

    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    public Long getAlternativeId() {
        return alternativeId;
    }

    public void setAlternativeId(Long alternativeId) {
        this.alternativeId = alternativeId;
    }
}
