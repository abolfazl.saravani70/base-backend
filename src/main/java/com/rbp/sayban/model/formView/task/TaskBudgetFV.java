package com.rbp.sayban.model.formView.task;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.task.Task;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TASK_BUDGET_VIEW")
public class TaskBudgetFV extends EvalStateBaseEntity {

    @Column(name = "ESTIMATE_BUDGET")
    private Long estimateBudget;

    @Column(name = "ACTUAL_BUDGET")
    private Long actualBudget;

    @Column(name = "FK_TASK_ID")
    private Long taskId;

    @Column(name = "TASK_FK_PV_TYPE_ID")
    private Long taskPvTypeId;

    @Column(name = "TASK_PV_TYPE_TITLE")
    private String  taskPvTypeTitle;

    @Column(name = "FK_BUDGET_ID")
    private Long budgetId;

    @Column(name = "BUDGET_NAME")
    private String budgetName;

    @Column(name = "BUDGET_TITLE")
    private String budgetTitle;

    @Column(name = "BUDGET_IS_ROOT")
    private Boolean budgetIsRoot;

    @Column(name = "BUDGET_INITIAL_PERCENTAGE")
    private Double budgetInitialPercentage;

    @Column(name = "BUDGET_PERCENTAGE")
    private Double budgetPercentage;

    @Column(name = "BUDGET_WEIGHT")
    private Long budgetWeight;

    @Column(name = "BUDGET_VOLUME")
    private Long budgetVolume;

    @Column(name = "BUDGET_PRIORITY")
    private Integer budgetPriority;

    @Column(name = "BUDGET_IMPORTANCE")
    private Double budgetImportance;

    @Column(name = "BUDGET_ACTUAL_START_DATE")
    private LocalDate budgetActualStartDate;

    @Column(name = "BUDGET_ACTUAL_END_DATE")
    private LocalDate budgetActualEndDate;

    @Column(name = "BUDGET_BUDGET_EXPIRATION_DATE")
    private LocalDate budgetExpirationDate;

    @Column(name = "BUDGET_START_DATE")
    private LocalDate budgetStartDate;

    @Column(name = "BUDGET_END_DATE")
    private LocalDate budgetEndDate;

    @Column(name = "BUDGET_FISCAL_YEAR")
    private String budgetFiscalYear;

    @Column(name = "BUDGET_LEVEL$")
    private Long budgetLevel;

    public Long getEstimateBudget() {
        return estimateBudget;
    }

    public void setEstimateBudget(Long estimateBudget) {
        this.estimateBudget = estimateBudget;
    }

    public Long getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Long actualBudget) {
        this.actualBudget = actualBudget;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskPvTypeId() {
        return taskPvTypeId;
    }

    public void setTaskPvTypeId(Long taskPvTypeId) {
        this.taskPvTypeId = taskPvTypeId;
    }

    public String getTaskPvTypeTitle() {
        return taskPvTypeTitle;
    }

    public void setTaskPvTypeTitle(String taskPvTypeTitle) {
        this.taskPvTypeTitle = taskPvTypeTitle;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgetTitle() {
        return budgetTitle;
    }

    public void setBudgetTitle(String budgetTitle) {
        this.budgetTitle = budgetTitle;
    }

    public Boolean getBudgetIsRoot() {
        return budgetIsRoot;
    }

    public void setBudgetIsRoot(Boolean budgetIsRoot) {
        this.budgetIsRoot = budgetIsRoot;
    }

    public Double getBudgetInitialPercentage() {
        return budgetInitialPercentage;
    }

    public void setBudgetInitialPercentage(Double budgetInitialPercentage) {
        this.budgetInitialPercentage = budgetInitialPercentage;
    }

    public Double getBudgetPercentage() {
        return budgetPercentage;
    }

    public void setBudgetPercentage(Double budgetPercentage) {
        this.budgetPercentage = budgetPercentage;
    }

    public Long getBudgetWeight() {
        return budgetWeight;
    }

    public void setBudgetWeight(Long budgetWeight) {
        this.budgetWeight = budgetWeight;
    }

    public Long getBudgetVolume() {
        return budgetVolume;
    }

    public void setBudgetVolume(Long budgetVolume) {
        this.budgetVolume = budgetVolume;
    }

    public Integer getBudgetPriority() {
        return budgetPriority;
    }

    public void setBudgetPriority(Integer budgetPriority) {
        this.budgetPriority = budgetPriority;
    }

    public Double getBudgetImportance() {
        return budgetImportance;
    }

    public void setBudgetImportance(Double budgetImportance) {
        this.budgetImportance = budgetImportance;
    }

    public LocalDate getBudgetActualStartDate() {
        return budgetActualStartDate;
    }

    public void setBudgetActualStartDate(LocalDate budgetActualStartDate) {
        this.budgetActualStartDate = budgetActualStartDate;
    }

    public LocalDate getBudgetActualEndDate() {
        return budgetActualEndDate;
    }

    public void setBudgetActualEndDate(LocalDate budgetActualEndDate) {
        this.budgetActualEndDate = budgetActualEndDate;
    }

    public LocalDate getBudgetExpirationDate() {
        return budgetExpirationDate;
    }

    public void setBudgetExpirationDate(LocalDate budgetExpirationDate) {
        this.budgetExpirationDate = budgetExpirationDate;
    }

    public LocalDate getBudgetStartDate() {
        return budgetStartDate;
    }

    public void setBudgetStartDate(LocalDate budgetStartDate) {
        this.budgetStartDate = budgetStartDate;
    }

    public LocalDate getBudgetEndDate() {
        return budgetEndDate;
    }

    public void setBudgetEndDate(LocalDate budgetEndDate) {
        this.budgetEndDate = budgetEndDate;
    }

    public String getBudgetFiscalYear() {
        return budgetFiscalYear;
    }

    public void setBudgetFiscalYear(String budgetFiscalYear) {
        this.budgetFiscalYear = budgetFiscalYear;
    }

    public Long getBudgetLevel() {
        return budgetLevel;
    }

    public void setBudgetLevel(Long budgetLevel) {
        this.budgetLevel = budgetLevel;
    }
}
