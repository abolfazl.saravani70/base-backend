/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.norm;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.norm.NormCost;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "NORM_PARAMETER_VIEW")
public class NormParameterFV extends BaseEntity {

    @Column(name = "FK_NORM_COST_ID")
    private Long normCostId;

    @Column(name ="NORM_COST_NAME" )
    private String normCostName;

    @Column (name="NORM_COST_TITLE")
    private String normCostTitle;

    @Column(name="NORM_COST_COST")
    private Long normCostCost;

    @Column(name="NORM_COST_RATE")
    private Long normCostRate;

    @Column (name="NORM_COST_PRIORITORY")
    private Integer normCostPrioritory;

    @Column(name = "FK_PV_MESURE_UNIT_ID")
    private Long pvMesureUnitId;

    @Column(name = "PV_MESURE_UNIT_TITLE")
    private String pvMesureUnitTitle;

    public Long getNormCostId() {
        return normCostId;
    }

    public void setNormCostId(Long normCostId) {
        this.normCostId = normCostId;
    }

    public String getNormCostName() {
        return normCostName;
    }

    public void setNormCostName(String normCostName) {
        this.normCostName = normCostName;
    }

    public String getNormCostTitle() {
        return normCostTitle;
    }

    public void setNormCostTitle(String normCostTitle) {
        this.normCostTitle = normCostTitle;
    }

    public Long getNormCostCost() {
        return normCostCost;
    }

    public void setNormCostCost(Long normCostCost) {
        this.normCostCost = normCostCost;
    }

    public Long getNormCostRate() {
        return normCostRate;
    }

    public void setNormCostRate(Long normCostRate) {
        this.normCostRate = normCostRate;
    }

    public Integer getNormCostPrioritory() {
        return normCostPrioritory;
    }

    public void setNormCostPrioritory(Integer normCostPrioritory) {
        this.normCostPrioritory = normCostPrioritory;
    }

    public Long getPvMesureUnitId() {
        return pvMesureUnitId;
    }

    public void setPvMesureUnitId(Long pvMesureUnitId) {
        this.pvMesureUnitId = pvMesureUnitId;
    }

    public String getPvMesureUnitTitle() {
        return pvMesureUnitTitle;
    }

    public void setPvMesureUnitTitle(String pvMesureUnitTitle) {
        this.pvMesureUnitTitle = pvMesureUnitTitle;
    }
}
