/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.SubjectiveJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.domainmodel.subjective.Subjective;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "GOAL_SUBJECTIVE_VIEW")
public class GoalSubjectiveFV extends EvalStateBaseEntity {

    @Column(name = "PERCENTAGE")
    private Integer priority;

    @Column(name = "IMPORTANCE")
    private Double importance;

    @Column(name = "FK_SUBJECTIVE_ID")
    private Long subjectiveId;

    @Column(name = "SUBJECTIVE_NAME")
    private String subjectiveName;

    @Column(name = "SUBJECTIVE_TITLE")
    private String subjectiveTitle;

    @Column(name = "SUBJECTIVE_IS_ROOT")
    private Boolean subjectiveIsRoot;

    @Column(name = "SUBJECTIVE_INITIAL_PERCENTAGE")
    private Double subjectiveInitialPercentage;

    @Column(name = "SUBJECTIVE_PERCENTAGE")
    private Double subjectivePercentage;

    @Column(name = "SUBJECTIVE_WEIGHT")
    private Long subjectiveWeight;

    @Column(name = "SUBJECTIVE_VOLUME")
    private Long subjectiveVolume;

    @Column(name = "SUBJECTIVE_PRIORITY")
    private Integer subjectivePriority;

    @Column(name = "SUBJECTIVE_IMPORTANCE")
    private Double subjectiveImportance;

    @Column(name = "SUBJECTIVE_ACTUAL_START_DATE")
    private LocalDate subjectiveActualStartDate;

    @Column(name = "SUBJECTIVE_ACTUAL_END_DATE")
    private LocalDate subjectiveActualEndDate;

    @Column(name = "SUBJECTIVE_EXPIRATION_DATE")
    private LocalDate subjectiveExpirationDate;

    @Column(name = "SUBJECTIVE_START_DATE")
    private LocalDate subjectiveStartDate;

    @Column(name = "SUBJECTIVE_END_DATE")
    private LocalDate subjectiveEndDate;

    @Column(name = "SUBJECTIVE_FISCAL_YEAR")
    private String subjectiveFiscalYear;

    @Column(name = "SUBJECTIVE_LEVEL$")
    private Long subjectiveLevel;

    @Column(name = "FK_GOAL_ID")
    private Long goalId;

    @Column(name = "GOAL_FK_PV_TYPE_ID")
    private Long goalPvTypeId;

    @Column(name = "GOAL_PV_TYPE_TITLE")
    private String goalPvTypeTitle;

    @Column(name = "GOAL_NAME")
    private String goalName;

    @Column(name = "GOAL_TITLE")
    private String goalTitle;

    @Column(name = "GOAL_IS_ROOT")
    private Boolean goalIsRoot;

    @Column(name = "GOAL_INITIAL_PERCENTAGE")
    private Double goalInitialPercentage;

    @Column(name = "GOAL_PERCENTAGE")
    private Double goalPercentage;

    @Column(name = "GOAL_WEIGHT")
    private Long goalWeight;

    @Column(name = "GOAL_VOLUME")
    private Long goalVolume;

    @Column(name = "GOAL_PRIORITY")
    private Integer goalPriority;

    @Column(name = "GOAL_IMPORTANCE")
    private Double goalImportance;

    @Column(name = "GOAL_ACTUAL_START_DATE")
    private LocalDate goalActualStartDate;

    @Column(name = "GOAL_ACTUAL_END_DATE")
    private LocalDate goalActualEndDate;

    @Column(name = "GOAL_EXPIRATION_DATE")
    private LocalDate goalExpirationDate;

    @Column(name = "GOAL_START_DATE")
    private LocalDate goalStartDate;

    @Column(name = "GOAL_END_DATE")
    private LocalDate goalEndDate;

    @Column(name = "GOAL_FISCAL_YEAR")
    private String goalFiscalYear;

    @Column(name = "GOAL_LEVEL$")
    private Long goalLevel;

    @Override
    public Integer getPriority() {
        return priority;
    }

    @Override
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public Double getImportance() {
        return importance;
    }

    @Override
    public void setImportance(Double importance) {
        this.importance = importance;
    }

    public Long getSubjectiveId() {
        return subjectiveId;
    }

    public void setSubjectiveId(Long subjectiveId) {
        this.subjectiveId = subjectiveId;
    }

    public String getSubjectiveName() {
        return subjectiveName;
    }

    public void setSubjectiveName(String subjectiveName) {
        this.subjectiveName = subjectiveName;
    }

    public String getSubjectiveTitle() {
        return subjectiveTitle;
    }

    public void setSubjectiveTitle(String subjectiveTitle) {
        this.subjectiveTitle = subjectiveTitle;
    }

    public Boolean getSubjectiveIsRoot() {
        return subjectiveIsRoot;
    }

    public void setSubjectiveIsRoot(Boolean subjectiveIsRoot) {
        this.subjectiveIsRoot = subjectiveIsRoot;
    }

    public Double getSubjectiveInitialPercentage() {
        return subjectiveInitialPercentage;
    }

    public void setSubjectiveInitialPercentage(Double subjectiveInitialPercentage) {
        this.subjectiveInitialPercentage = subjectiveInitialPercentage;
    }

    public Double getSubjectivePercentage() {
        return subjectivePercentage;
    }

    public void setSubjectivePercentage(Double subjectivePercentage) {
        this.subjectivePercentage = subjectivePercentage;
    }

    public Long getSubjectiveWeight() {
        return subjectiveWeight;
    }

    public void setSubjectiveWeight(Long subjectiveWeight) {
        this.subjectiveWeight = subjectiveWeight;
    }

    public Long getSubjectiveVolume() {
        return subjectiveVolume;
    }

    public void setSubjectiveVolume(Long subjectiveVolume) {
        this.subjectiveVolume = subjectiveVolume;
    }

    public Integer getSubjectivePriority() {
        return subjectivePriority;
    }

    public void setSubjectivePriority(Integer subjectivePriority) {
        this.subjectivePriority = subjectivePriority;
    }

    public Double getSubjectiveImportance() {
        return subjectiveImportance;
    }

    public void setSubjectiveImportance(Double subjectiveImportance) {
        this.subjectiveImportance = subjectiveImportance;
    }

    public LocalDate getSubjectiveActualStartDate() {
        return subjectiveActualStartDate;
    }

    public void setSubjectiveActualStartDate(LocalDate subjectiveActualStartDate) {
        this.subjectiveActualStartDate = subjectiveActualStartDate;
    }

    public LocalDate getSubjectiveActualEndDate() {
        return subjectiveActualEndDate;
    }

    public void setSubjectiveActualEndDate(LocalDate subjectiveActualEndDate) {
        this.subjectiveActualEndDate = subjectiveActualEndDate;
    }

    public LocalDate getSubjectiveExpirationDate() {
        return subjectiveExpirationDate;
    }

    public void setSubjectiveExpirationDate(LocalDate subjectiveExpirationDate) {
        this.subjectiveExpirationDate = subjectiveExpirationDate;
    }

    public LocalDate getSubjectiveStartDate() {
        return subjectiveStartDate;
    }

    public void setSubjectiveStartDate(LocalDate subjectiveStartDate) {
        this.subjectiveStartDate = subjectiveStartDate;
    }

    public LocalDate getSubjectiveEndDate() {
        return subjectiveEndDate;
    }

    public void setSubjectiveEndDate(LocalDate subjectiveEndDate) {
        this.subjectiveEndDate = subjectiveEndDate;
    }

    public String getSubjectiveFiscalYear() {
        return subjectiveFiscalYear;
    }

    public void setSubjectiveFiscalYear(String subjectiveFiscalYear) {
        this.subjectiveFiscalYear = subjectiveFiscalYear;
    }

    public Long getSubjectiveLevel() {
        return subjectiveLevel;
    }

    public void setSubjectiveLevel(Long subjectiveLevel) {
        this.subjectiveLevel = subjectiveLevel;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }

    public Long getGoalPvTypeId() {
        return goalPvTypeId;
    }

    public void setGoalPvTypeId(Long goalPvTypeId) {
        this.goalPvTypeId = goalPvTypeId;
    }

    public String getGoalPvTypeTitle() {
        return goalPvTypeTitle;
    }

    public void setGoalPvTypeTitle(String goalPvTypeTitle) {
        this.goalPvTypeTitle = goalPvTypeTitle;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getGoalTitle() {
        return goalTitle;
    }

    public void setGoalTitle(String goalTitle) {
        this.goalTitle = goalTitle;
    }

    public Boolean getGoalIsRoot() {
        return goalIsRoot;
    }

    public void setGoalIsRoot(Boolean goalIsRoot) {
        this.goalIsRoot = goalIsRoot;
    }

    public Double getGoalInitialPercentage() {
        return goalInitialPercentage;
    }

    public void setGoalInitialPercentage(Double goalInitialPercentage) {
        this.goalInitialPercentage = goalInitialPercentage;
    }

    public Double getGoalPercentage() {
        return goalPercentage;
    }

    public void setGoalPercentage(Double goalPercentage) {
        this.goalPercentage = goalPercentage;
    }

    public Long getGoalWeight() {
        return goalWeight;
    }

    public void setGoalWeight(Long goalWeight) {
        this.goalWeight = goalWeight;
    }

    public Long getGoalVolume() {
        return goalVolume;
    }

    public void setGoalVolume(Long goalVolume) {
        this.goalVolume = goalVolume;
    }

    public Integer getGoalPriority() {
        return goalPriority;
    }

    public void setGoalPriority(Integer goalPriority) {
        this.goalPriority = goalPriority;
    }

    public Double getGoalImportance() {
        return goalImportance;
    }

    public void setGoalImportance(Double goalImportance) {
        this.goalImportance = goalImportance;
    }

    public LocalDate getGoalActualStartDate() {
        return goalActualStartDate;
    }

    public void setGoalActualStartDate(LocalDate goalActualStartDate) {
        this.goalActualStartDate = goalActualStartDate;
    }

    public LocalDate getGoalActualEndDate() {
        return goalActualEndDate;
    }

    public void setGoalActualEndDate(LocalDate goalActualEndDate) {
        this.goalActualEndDate = goalActualEndDate;
    }

    public LocalDate getGoalExpirationDate() {
        return goalExpirationDate;
    }

    public void setGoalExpirationDate(LocalDate goalExpirationDate) {
        this.goalExpirationDate = goalExpirationDate;
    }

    public LocalDate getGoalStartDate() {
        return goalStartDate;
    }

    public void setGoalStartDate(LocalDate goalStartDate) {
        this.goalStartDate = goalStartDate;
    }

    public LocalDate getGoalEndDate() {
        return goalEndDate;
    }

    public void setGoalEndDate(LocalDate goalEndDate) {
        this.goalEndDate = goalEndDate;
    }

    public String getGoalFiscalYear() {
        return goalFiscalYear;
    }

    public void setGoalFiscalYear(String goalFiscalYear) {
        this.goalFiscalYear = goalFiscalYear;
    }

    public Long getGoalLevel() {
        return goalLevel;
    }

    public void setGoalLevel(Long goalLevel) {
        this.goalLevel = goalLevel;
    }
}
