/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.model.domainmodel.application.Menu;
import com.rbp.sayban.model.domainmodel.system.System;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "APPLICATION_MENU_VIEW")
public class ApplicationMenuFV extends BaseEntity {

    @Column(name = "MENU_NAME")
    private String menuName;

    @Column(name = "MENU_TITLE")
    private String menuTitle;

    @Column(name = "MENU_URL")
    private String menuUrl;

    @Column(name = "MENU_CODE")
    private String menuCode;

    @Column(name = "FROM_DATE")
    private LocalDate fromDate;

    @Column(name = "TO_DATE")
    private LocalDate toDate;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    @Column(name = "FK_MENU_ID")
    private Long menuId;

    @Column(name = "MENU_MENU_NAME")
    private String menuMenuName;

    @Column(name = "MENU_MENU_TITLE")
    private String menuMenuTitle;

    @Column(name = "MENU_MENU_URL")
    private String menuMenuUrl;

    @Column(name = "MENU_MENU_CODE")
    private Integer menuMenuCode;

    @Column(name = "MENU_FROM_DATE")
    private LocalDate menuFromDate;

    @Column(name = "MENU_TO_DATE")
    private LocalDate menuToDate;

    @Column(name = "MENU_IS_DEPRECATED")
    private Boolean menuIsDeprecated;

    @Column(name = "MENU_IS_ACTIVE")
    private Boolean menuIsActive;


    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public Boolean getDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        isDeprecated = deprecated;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuMenuName() {
        return menuMenuName;
    }

    public void setMenuMenuName(String menuMenuName) {
        this.menuMenuName = menuMenuName;
    }

    public String getMenuMenuTitle() {
        return menuMenuTitle;
    }

    public void setMenuMenuTitle(String menuMenuTitle) {
        this.menuMenuTitle = menuMenuTitle;
    }

    public String getMenuMenuUrl() {
        return menuMenuUrl;
    }

    public void setMenuMenuUrl(String menuMenuUrl) {
        this.menuMenuUrl = menuMenuUrl;
    }

    public Integer getMenuMenuCode() {
        return menuMenuCode;
    }

    public void setMenuMenuCode(Integer menuMenuCode) {
        this.menuMenuCode = menuMenuCode;
    }

    public LocalDate getMenuFromDate() {
        return menuFromDate;
    }

    public void setMenuFromDate(LocalDate menuFromDate) {
        this.menuFromDate = menuFromDate;
    }

    public LocalDate getMenuToDate() {
        return menuToDate;
    }

    public void setMenuToDate(LocalDate menuToDate) {
        this.menuToDate = menuToDate;
    }

    public Boolean getMenuIsDeprecated() {
        return menuIsDeprecated;
    }

    public void setMenuIsDeprecated(Boolean menuIsDeprecated) {
        this.menuIsDeprecated = menuIsDeprecated;
    }

    public Boolean getMenuIsActive() {
        return menuIsActive;
    }

    public void setMenuIsActive(Boolean menuIsActive) {
        this.menuIsActive = menuIsActive;
    }
}
