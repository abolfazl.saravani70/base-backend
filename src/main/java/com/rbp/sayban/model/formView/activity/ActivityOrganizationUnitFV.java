/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.OrganizationUnitJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import com.rbp.sayban.model.domainmodel.base.MonthsPercentage;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "ACTIVITY_ORG_UNIT_VIEW")
public class ActivityOrganizationUnitFV extends EvalStateBaseEntity {

    @Column(name = "ASSIGN_DATE")
    private LocalDate assignDate;

    @Column(name = "DUE_DATE")
    private LocalDate dueDate;

    @Column(name = "FK_PV_STATE_ID")
    private Long pvStateId;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long orgUnitId;

    @Column(name = "M1")
    private Double m1;

    @Column(name = "M2")
    private Double m2;

    @Column(name = "M3")
    private Double m3;

    @Column(name = "M4")
    private Double m4;

    @Column(name = "M5")
    private Double m5;

    @Column(name = "M6")
    private Double m6;

    @Column(name = "M7")
    private Double m7;

    @Column(name = "M8")
    private Double m8;

    @Column(name = "M9")
    private Double m9;

    @Column(name = "M10")
    private Double m10;

    @Column(name = "M11")
    private Double m11;

    @Column(name = "M12")
    private Double m12;

    @Column(name = "FK_ACTIVITY_ID")
    private Long activityId;

    @Column(name = "ACTIVITY_FK_PV_TYPE_ID")
    private Long activityPvTypeId;

    @Column(name = "ACTIVITY_PV_TYPE_TITLE")
    private String activityPvTypeTitle;

    @Column(name = "ACTIVITY_NAME")
    private String activityName;

    @Column(name = "ACTIVITY_TITLE")
    private String activityTitle;

    @Column(name = "ACTIVITY_IS_ROOT")
    private Boolean activityIsRoot;

    @Column(name = "ACTIVITY_INITIAL_PERCENTAGE")
    private Double activityInitialPercentage;

    @Column(name = "ACTIVITY_PERCENTAGE")
    private Double activityPercentage;

    @Column(name = "ACTIVITY_WEIGHT")
    private Long activityWeight;

    @Column(name = "ACTIVITY_VOLUME")
    private Long activityVolume;

    @Column(name = "ACTIVITY_PRIORITY")
    private Integer activityPriority;

    @Column(name = "ACTIVITY_IMPORTANCE")
    private Double activityImportance;

    @Column(name = "ACTIVITY_ACTUAL_START_DATE")
    private LocalDate activityActualStartDate;

    @Column(name = "ACTIVITY_ACTUAL_END_DATE")
    private LocalDate activityActualEndDate;

    @Column(name = "ACTIVITY_EXPIRATION_DATE")
    private LocalDate activityExpirationDate;

    @Column(name = "ACTIVITY_START_DATE")
    private LocalDate activityStartDate;

    @Column(name = "ACTIVITY_END_DATE")
    private LocalDate activityEndDate;

    @Column(name = "ACTIVITY_FISCAL_YEAR")
    private String activityFiscalYear;

    @Column(name = "ACTIVITY_LEVEL$")
    private Long activityLevel;

    public LocalDate getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(LocalDate assignDate) {
        this.assignDate = assignDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getPvStateId() {
        return pvStateId;
    }

    public void setPvStateId(Long pvStateId) {
        this.pvStateId = pvStateId;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Double getM1() {
        return m1;
    }

    public void setM1(Double m1) {
        this.m1 = m1;
    }

    public Double getM2() {
        return m2;
    }

    public void setM2(Double m2) {
        this.m2 = m2;
    }

    public Double getM3() {
        return m3;
    }

    public void setM3(Double m3) {
        this.m3 = m3;
    }

    public Double getM4() {
        return m4;
    }

    public void setM4(Double m4) {
        this.m4 = m4;
    }

    public Double getM5() {
        return m5;
    }

    public void setM5(Double m5) {
        this.m5 = m5;
    }

    public Double getM6() {
        return m6;
    }

    public void setM6(Double m6) {
        this.m6 = m6;
    }

    public Double getM7() {
        return m7;
    }

    public void setM7(Double m7) {
        this.m7 = m7;
    }

    public Double getM8() {
        return m8;
    }

    public void setM8(Double m8) {
        this.m8 = m8;
    }

    public Double getM9() {
        return m9;
    }

    public void setM9(Double m9) {
        this.m9 = m9;
    }

    public Double getM10() {
        return m10;
    }

    public void setM10(Double m10) {
        this.m10 = m10;
    }

    public Double getM11() {
        return m11;
    }

    public void setM11(Double m11) {
        this.m11 = m11;
    }

    public Double getM12() {
        return m12;
    }

    public void setM12(Double m12) {
        this.m12 = m12;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getActivityPvTypeId() {
        return activityPvTypeId;
    }

    public void setActivityPvTypeId(Long activityPvTypeId) {
        this.activityPvTypeId = activityPvTypeId;
    }

    public String getActivityPvTypeTitle() {
        return activityPvTypeTitle;
    }

    public void setActivityPvTypeTitle(String activityPvTypeTitle) {
        this.activityPvTypeTitle = activityPvTypeTitle;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public Boolean getActivityIsRoot() {
        return activityIsRoot;
    }

    public void setActivityIsRoot(Boolean activityIsRoot) {
        this.activityIsRoot = activityIsRoot;
    }

    public Double getActivityInitialPercentage() {
        return activityInitialPercentage;
    }

    public void setActivityInitialPercentage(Double activityInitialPercentage) {
        this.activityInitialPercentage = activityInitialPercentage;
    }

    public Double getActivityPercentage() {
        return activityPercentage;
    }

    public void setActivityPercentage(Double activityPercentage) {
        this.activityPercentage = activityPercentage;
    }

    public Long getActivityWeight() {
        return activityWeight;
    }

    public void setActivityWeight(Long activityWeight) {
        this.activityWeight = activityWeight;
    }

    public Long getActivityVolume() {
        return activityVolume;
    }

    public void setActivityVolume(Long activityVolume) {
        this.activityVolume = activityVolume;
    }

    public Integer getActivityPriority() {
        return activityPriority;
    }

    public void setActivityPriority(Integer activityPriority) {
        this.activityPriority = activityPriority;
    }

    public Double getActivityImportance() {
        return activityImportance;
    }

    public void setActivityImportance(Double activityImportance) {
        this.activityImportance = activityImportance;
    }

    public LocalDate getActivityActualStartDate() {
        return activityActualStartDate;
    }

    public void setActivityActualStartDate(LocalDate activityActualStartDate) {
        this.activityActualStartDate = activityActualStartDate;
    }

    public LocalDate getActivityActualEndDate() {
        return activityActualEndDate;
    }

    public void setActivityActualEndDate(LocalDate activityActualEndDate) {
        this.activityActualEndDate = activityActualEndDate;
    }

    public LocalDate getActivityExpirationDate() {
        return activityExpirationDate;
    }

    public void setActivityExpirationDate(LocalDate activityExpirationDate) {
        this.activityExpirationDate = activityExpirationDate;
    }

    public LocalDate getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(LocalDate activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public LocalDate getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(LocalDate activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityFiscalYear() {
        return activityFiscalYear;
    }

    public void setActivityFiscalYear(String activityFiscalYear) {
        this.activityFiscalYear = activityFiscalYear;
    }

    public Long getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Long activityLevel) {
        this.activityLevel = activityLevel;
    }
}
