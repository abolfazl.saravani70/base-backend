/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.security.User;
import com.rbp.sayban.model.domainmodel.application.AppEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import org.hibernate.annotations.Immutable;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Set;

@Entity
@Immutable
@Table(name = "CONFIRMATION_VIEW")
public class ConfirmationFV extends BaseEntity {

    @Column(name = "FK_USER_ID")
    private Long userId;

    @Column(name = "USER_NAME")
    private String userUsername;

    @Column(name = "USER_FIRST_NAME")
    private String userFirstName;

    @Column(name = "USER_LAST_NAME")
    private String userLastName;

    @Column(name = "USER_EMAIL")
    private String userEmail;

    @Column(name = "USER_PASSWORD")
    private String userPassword;

    @Column(name = "USER_OBSOLETE")
    private Boolean userIsObsolete;

    @Column(name = "USER_LAST_LOGIN")
    private String userLastLogin;

    @Column(name = "USER_IS_ONLINE")
    private Boolean userIsOnline;

    @Column(name = "USER_EXPIRE_DATE")
    private String userExpireDate;

    @Column(name = "USER_LAST_IP")
    private String userLastIp;

    @Column(name = "USER_IS_ACTIVE")
    private Boolean userIsActive;

    @Column(name = "USER_TELL")
    private String userTell;

    @Column(name = "USER_MOBILE")
    private String userMobile;
    @Column
    private String userKowsarNumber;

    @Column(name = "USER_PICTURE")
    private String userPicture;

    @Column(name = "USER_ACTIVE_THEME")
    private String userActiveTheme;

    @Column(name = "USER_FORCE_CHANGE_PASSWORD")
    private Boolean userIsForceChangePassword;

    @Column(name = "USER_TOKEN")
    private String userToken;

    @Column(name = "FK_APP_ENTITY_ID")
    private Long appEntityId;

    @Column(name = "APP_ENTITY_ENTITY_NAME")
    private String appEntityEntityName;

    @Column(name = "APP_ENTITY_ENTITY_KEYWORD")
    private String appEntityEntityKeyword;

    @Column(name = "APP_ENTITY_ENTITY_TYPE")
    private String appEntityEntityType;

    @Column(name = "APP_ENTITY_ENTITY_CODE")
    private String appEntityEntityCode;

    @Column(name = "APP_ENTITY_IS_DEPRECATED")
    private Boolean appEntityIsDeprecated;

    @Column(name = "APP_ENTITY_REGULAR_EXP")
    private String appEntityRegularExp;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Boolean getUserIsObsolete() {
        return userIsObsolete;
    }

    public void setUserIsObsolete(Boolean userIsObsolete) {
        this.userIsObsolete = userIsObsolete;
    }

    public String getUserLastLogin() {
        return userLastLogin;
    }

    public void setUserLastLogin(String userLastLogin) {
        this.userLastLogin = userLastLogin;
    }

    public Boolean getUserIsOnline() {
        return userIsOnline;
    }

    public void setUserIsOnline(Boolean userIsOnline) {
        this.userIsOnline = userIsOnline;
    }

    public String getUserExpireDate() {
        return userExpireDate;
    }

    public void setUserExpireDate(String userExpireDate) {
        this.userExpireDate = userExpireDate;
    }

    public String getUserLastIp() {
        return userLastIp;
    }

    public void setUserLastIp(String userLastIp) {
        this.userLastIp = userLastIp;
    }

    public Boolean getUserIsActive() {
        return userIsActive;
    }

    public void setUserIsActive(Boolean userIsActive) {
        this.userIsActive = userIsActive;
    }

    public String getUserTell() {
        return userTell;
    }

    public void setUserTell(String userTell) {
        this.userTell = userTell;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserKowsarNumber() {
        return userKowsarNumber;
    }

    public void setUserKowsarNumber(String userKowsarNumber) {
        this.userKowsarNumber = userKowsarNumber;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public String getUserActiveTheme() {
        return userActiveTheme;
    }

    public void setUserActiveTheme(String userActiveTheme) {
        this.userActiveTheme = userActiveTheme;
    }

    public Boolean getUserIsForceChangePassword() {
        return userIsForceChangePassword;
    }

    public void setUserIsForceChangePassword(Boolean userIsForceChangePassword) {
        this.userIsForceChangePassword = userIsForceChangePassword;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public Long getAppEntityId() {
        return appEntityId;
    }

    public void setAppEntityId(Long appEntityId) {
        this.appEntityId = appEntityId;
    }

    public String getAppEntityEntityName() {
        return appEntityEntityName;
    }

    public void setAppEntityEntityName(String appEntityEntityName) {
        this.appEntityEntityName = appEntityEntityName;
    }

    public String getAppEntityEntityKeyword() {
        return appEntityEntityKeyword;
    }

    public void setAppEntityEntityKeyword(String appEntityEntityKeyword) {
        this.appEntityEntityKeyword = appEntityEntityKeyword;
    }

    public String getAppEntityEntityType() {
        return appEntityEntityType;
    }

    public void setAppEntityEntityType(String appEntityEntityType) {
        this.appEntityEntityType = appEntityEntityType;
    }

    public String getAppEntityEntityCode() {
        return appEntityEntityCode;
    }

    public void setAppEntityEntityCode(String appEntityEntityCode) {
        this.appEntityEntityCode = appEntityEntityCode;
    }

    public Boolean getAppEntityIsDeprecated() {
        return appEntityIsDeprecated;
    }

    public void setAppEntityIsDeprecated(Boolean appEntityIsDeprecated) {
        this.appEntityIsDeprecated = appEntityIsDeprecated;
    }

    public String getAppEntityRegularExp() {
        return appEntityRegularExp;
    }

    public void setAppEntityRegularExp(String appEntityRegularExp) {
        this.appEntityRegularExp = appEntityRegularExp;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }
}
