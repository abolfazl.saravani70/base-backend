/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.SubjectiveJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "ACTIVITY_SUBJECTIVE_VIEW")
public class ActivitySubjectiveFV extends EvalStateBaseEntity {

    @Column(name = "FK_SUBJECTIVE_ID")
    private Long subjectiveId;

    @Column(name = "SUBJECTIVE_NAME")
    private String subjectiveName;

    @Column(name = "SUBJECTIVE_TITLE")
    private String subjectiveTitle;

    @Column(name = "SUBJECTIVE_IS_ROOT")
    private Boolean subjectiveIsRoot;

    @Column(name = "SUBJECTIVE_INITIAL_PERCENTAGE")
    private Double subjectiveInitialPercentage;

    @Column(name = "SUBJECTIVE_PERCENTAGE")
    private Double subjectivePercentage;

    @Column(name = "SUBJECTIVE_WEIGHT")
    private Long subjectiveWeight;

    @Column(name = "SUBJECTIVE_VOLUME")
    private Long subjectiveVolume;

    @Column(name = "SUBJECTIVE_PRIORITY")
    private Integer subjectivePriority;

    @Column(name = "SUBJECTIVE_IMPORTANCE")
    private Double subjectiveImportance;

    @Column(name = "SUBJECTIVE_ACTUAL_START_DATE")
    private LocalDate subjectiveActualStartDate;

    @Column(name = "SUBJECTIVE_ACTUAL_END_DATE")
    private LocalDate subjectiveActualEndDate;

    @Column(name = "SUBJECTIVE_EXPIRATION_DATE")
    private LocalDate subjectiveExpirationDate;

    @Column(name = "SUBJECTIVE_START_DATE")
    private LocalDate subjectiveStartDate;

    @Column(name = "SUBJECTIVE_END_DATE")
    private LocalDate subjectiveEndDate;

    @Column(name = "SUBJECTIVE_FISCAL_YEAR")
    private String subjectiveFiscalYear;

    @Column(name = "SUBJECTIVE_LEVEL$")
    private Long subjectiveLevel;

    @Column(name = "FK_ACTIVITY_ID")
    private Long activityId;

    @Column(name = "ACTIVITY_FK_PV_TYPE_ID")
    private Long activityPvTypeId;

    @Column(name = "ACTIVITY_PV_TYPE_TITLE")
    private String activityPvTypeTitle;

    @Column(name = "ACTIVITY_NAME")
    private String activityName;

    @Column(name = "ACTIVITY_TITLE")
    private String activityTitle;

    @Column(name = "ACTIVITY_IS_ROOT")
    private Boolean activityIsRoot;

    @Column(name = "ACTIVITY_INITIAL_PERCENTAGE")
    private Double activityInitialPercentage;

    @Column(name = "ACTIVITY_PERCENTAGE")
    private Double activityPercentage;

    @Column(name = "ACTIVITY_WEIGHT")
    private Long activityWeight;

    @Column(name = "ACTIVITY_VOLUME")
    private Long activityVolume;

    @Column(name = "ACTIVITY_PRIORITY")
    private Integer activityPriority;

    @Column(name = "ACTIVITY_IMPORTANCE")
    private Double activityImportance;

    @Column(name = "ACTIVITY_ACTUAL_START_DATE")
    private LocalDate activityActualStartDate;

    @Column(name = "ACTIVITY_ACTUAL_END_DATE")
    private LocalDate activityActualEndDate;

    @Column(name = "ACTIVITY_EXPIRATION_DATE")
    private LocalDate activityExpirationDate;

    @Column(name = "ACTIVITY_START_DATE")
    private LocalDate activityStartDate;

    @Column(name = "ACTIVITY_END_DATE")
    private LocalDate activityEndDate;

    @Column(name = "ACTIVITY_FISCAL_YEAR")
    private String activityFiscalYear;

    @Column(name = "ACTIVITY_LEVEL$")
    private Long activityLevel;

    public Long getSubjectiveId() {
        return subjectiveId;
    }

    public void setSubjectiveId(Long subjectiveId) {
        this.subjectiveId = subjectiveId;
    }

    public String getSubjectiveName() {
        return subjectiveName;
    }

    public void setSubjectiveName(String subjectiveName) {
        this.subjectiveName = subjectiveName;
    }

    public String getSubjectiveTitle() {
        return subjectiveTitle;
    }

    public void setSubjectiveTitle(String subjectiveTitle) {
        this.subjectiveTitle = subjectiveTitle;
    }

    public Boolean getSubjectiveIsRoot() {
        return subjectiveIsRoot;
    }

    public void setSubjectiveIsRoot(Boolean subjectiveIsRoot) {
        this.subjectiveIsRoot = subjectiveIsRoot;
    }

    public Double getSubjectiveInitialPercentage() {
        return subjectiveInitialPercentage;
    }

    public void setSubjectiveInitialPercentage(Double subjectiveInitialPercentage) {
        this.subjectiveInitialPercentage = subjectiveInitialPercentage;
    }

    public Double getSubjectivePercentage() {
        return subjectivePercentage;
    }

    public void setSubjectivePercentage(Double subjectivePercentage) {
        this.subjectivePercentage = subjectivePercentage;
    }

    public Long getSubjectiveWeight() {
        return subjectiveWeight;
    }

    public void setSubjectiveWeight(Long subjectiveWeight) {
        this.subjectiveWeight = subjectiveWeight;
    }

    public Long getSubjectiveVolume() {
        return subjectiveVolume;
    }

    public void setSubjectiveVolume(Long subjectiveVolume) {
        this.subjectiveVolume = subjectiveVolume;
    }

    public Integer getSubjectivePriority() {
        return subjectivePriority;
    }

    public void setSubjectivePriority(Integer subjectivePriority) {
        this.subjectivePriority = subjectivePriority;
    }

    public Double getSubjectiveImportance() {
        return subjectiveImportance;
    }

    public void setSubjectiveImportance(Double subjectiveImportance) {
        this.subjectiveImportance = subjectiveImportance;
    }

    public LocalDate getSubjectiveActualStartDate() {
        return subjectiveActualStartDate;
    }

    public void setSubjectiveActualStartDate(LocalDate subjectiveActualStartDate) {
        this.subjectiveActualStartDate = subjectiveActualStartDate;
    }

    public LocalDate getSubjectiveActualEndDate() {
        return subjectiveActualEndDate;
    }

    public void setSubjectiveActualEndDate(LocalDate subjectiveActualEndDate) {
        this.subjectiveActualEndDate = subjectiveActualEndDate;
    }

    public LocalDate getSubjectiveExpirationDate() {
        return subjectiveExpirationDate;
    }

    public void setSubjectiveExpirationDate(LocalDate subjectiveExpirationDate) {
        this.subjectiveExpirationDate = subjectiveExpirationDate;
    }

    public LocalDate getSubjectiveStartDate() {
        return subjectiveStartDate;
    }

    public void setSubjectiveStartDate(LocalDate subjectiveStartDate) {
        this.subjectiveStartDate = subjectiveStartDate;
    }

    public LocalDate getSubjectiveEndDate() {
        return subjectiveEndDate;
    }

    public void setSubjectiveEndDate(LocalDate subjectiveEndDate) {
        this.subjectiveEndDate = subjectiveEndDate;
    }

    public String getSubjectiveFiscalYear() {
        return subjectiveFiscalYear;
    }

    public void setSubjectiveFiscalYear(String subjectiveFiscalYear) {
        this.subjectiveFiscalYear = subjectiveFiscalYear;
    }

    public Long getSubjectiveLevel() {
        return subjectiveLevel;
    }

    public void setSubjectiveLevel(Long subjectiveLevel) {
        this.subjectiveLevel = subjectiveLevel;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getActivityPvTypeId() {
        return activityPvTypeId;
    }

    public void setActivityPvTypeId(Long activityPvTypeId) {
        this.activityPvTypeId = activityPvTypeId;
    }

    public String getActivityPvTypeTitle() {
        return activityPvTypeTitle;
    }

    public void setActivityPvTypeTitle(String activityPvTypeTitle) {
        this.activityPvTypeTitle = activityPvTypeTitle;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public Boolean getActivityIsRoot() {
        return activityIsRoot;
    }

    public void setActivityIsRoot(Boolean activityIsRoot) {
        this.activityIsRoot = activityIsRoot;
    }

    public Double getActivityInitialPercentage() {
        return activityInitialPercentage;
    }

    public void setActivityInitialPercentage(Double activityInitialPercentage) {
        this.activityInitialPercentage = activityInitialPercentage;
    }

    public Double getActivityPercentage() {
        return activityPercentage;
    }

    public void setActivityPercentage(Double activityPercentage) {
        this.activityPercentage = activityPercentage;
    }

    public Long getActivityWeight() {
        return activityWeight;
    }

    public void setActivityWeight(Long activityWeight) {
        this.activityWeight = activityWeight;
    }

    public Long getActivityVolume() {
        return activityVolume;
    }

    public void setActivityVolume(Long activityVolume) {
        this.activityVolume = activityVolume;
    }

    public Integer getActivityPriority() {
        return activityPriority;
    }

    public void setActivityPriority(Integer activityPriority) {
        this.activityPriority = activityPriority;
    }

    public Double getActivityImportance() {
        return activityImportance;
    }

    public void setActivityImportance(Double activityImportance) {
        this.activityImportance = activityImportance;
    }

    public LocalDate getActivityActualStartDate() {
        return activityActualStartDate;
    }

    public void setActivityActualStartDate(LocalDate activityActualStartDate) {
        this.activityActualStartDate = activityActualStartDate;
    }

    public LocalDate getActivityActualEndDate() {
        return activityActualEndDate;
    }

    public void setActivityActualEndDate(LocalDate activityActualEndDate) {
        this.activityActualEndDate = activityActualEndDate;
    }

    public LocalDate getActivityExpirationDate() {
        return activityExpirationDate;
    }

    public void setActivityExpirationDate(LocalDate activityExpirationDate) {
        this.activityExpirationDate = activityExpirationDate;
    }

    public LocalDate getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(LocalDate activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public LocalDate getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(LocalDate activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityFiscalYear() {
        return activityFiscalYear;
    }

    public void setActivityFiscalYear(String activityFiscalYear) {
        this.activityFiscalYear = activityFiscalYear;
    }

    public Long getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Long activityLevel) {
        this.activityLevel = activityLevel;
    }
}
