/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "ACTIVITY_VIEW")
public class ActivityFV extends EvalStateBaseEntity {

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "PARENT_FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_FK_PV_TYPE_ID")
    private Long parentPvTypeId;

    @Column(name = "PARENT_PV_TYPE_TITLE")
    private String parentPvTypeTitle;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TITLE")
    private String parentTitle;

    @Column(name = "PARENT_IS_ROOT")
    private Boolean parentIsRoot;

    @Column(name = "PARENT_INITIAL_PERCENTAGE")
    private Double parentInitialPercentage;

    @Column(name = "PARENT_PERCENTAGE")
    private Double parentPercentage;

    @Column(name = "PARENT_WEIGHT")
    private Long parentWeight;

    @Column(name = "PARENT_VOLUME")
    private Long parentVolume;

    @Column(name = "PARENT_PRIORITY")
    private Integer parentPriority;

    @Column(name = "PARENT_IMPORTANCE")
    private Double parentImportance;

    @Column(name = "PARENT_ACTUAL_START_DATE")
    private LocalDate parentActualStartDate;

    @Column(name = "PARENT_ACTUAL_END_DATE")
    private LocalDate parentActualEndDate;

    @Column(name = "PARENT_EXPIRATION_DATE")
    private LocalDate parentExpirationDate;

    @Column(name = "PARENT_START_DATE")
    private LocalDate parentStartDate;

    @Column(name = "PARENT_END_DATE")
    private LocalDate parentEndDate;

    @Column(name = "PARENT_FISCAL_YEAR")
    private String parentFiscalYear;

    @Column(name = "PARENT_LEVEL$")
    private Long parentLevel;

    @Column(name = "PROJECT_FK_PROJECT_ID")
    private Long projectId;

    @Column(name = "PROJECT_FK_PV_TYPE_ID")
    private Long projectPvTypeId;

    @Column(name = "PROJECT_PV_TYPE_TITLE")
    private String projectPvTypeTitle;

    @Column(name = "PROJECT_NAME")
    private String projectName;

    @Column(name = "PROJECT_TITLE")
    private String projectTitle;

    @Column(name = "PROJECT_IS_ROOT")
    private Boolean projectIsRoot;

    @Column(name = "PROJECT_INITIAL_PERCENTAGE")
    private Double projectInitialPercentage;

    @Column(name = "PROJECT_PERCENTAGE")
    private Double projectPercentage;

    @Column(name = "PROJECT_WEIGHT")
    private Long projectWeight;

    @Column(name = "PROJECT_VOLUME")
    private Long projectVolume;

    @Column(name = "PROJECT_PRIORITY")
    private Integer projectPriority;

    @Column(name = "PROJECT_IMPORTANCE")
    private Double projectImportance;

    @Column(name = "PROJECT_ACTUAL_START_DATE")
    private LocalDate projectActualStartDate;

    @Column(name = "PROJECT_ACTUAL_END_DATE")
    private LocalDate projectActualEndDate;

    @Column(name = "PROJECT_EXPIRATION_DATE")
    private LocalDate projectExpirationDate;

    @Column(name = "PROJECT_START_DATE")
    private LocalDate projectStartDate;

    @Column(name = "PROJECT_END_DATE")
    private LocalDate projectEndDate;

    @Column(name = "PROJECT_FISCAL_YEAR")
    private String projectFiscalYear;

    @Column(name = "PROJECT_LEVEL$")
    private Long projectLevel;

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getParentPvTypeId() {
        return parentPvTypeId;
    }

    public void setParentPvTypeId(Long parentPvTypeId) {
        this.parentPvTypeId = parentPvTypeId;
    }

    public String getParentPvTypeTitle() {
        return parentPvTypeTitle;
    }

    public void setParentPvTypeTitle(String parentPvTypeTitle) {
        this.parentPvTypeTitle = parentPvTypeTitle;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public Boolean getParentIsRoot() {
        return parentIsRoot;
    }

    public void setParentIsRoot(Boolean parentIsRoot) {
        this.parentIsRoot = parentIsRoot;
    }

    public Double getParentInitialPercentage() {
        return parentInitialPercentage;
    }

    public void setParentInitialPercentage(Double parentInitialPercentage) {
        this.parentInitialPercentage = parentInitialPercentage;
    }

    public Double getParentPercentage() {
        return parentPercentage;
    }

    public void setParentPercentage(Double parentPercentage) {
        this.parentPercentage = parentPercentage;
    }

    public Long getParentWeight() {
        return parentWeight;
    }

    public void setParentWeight(Long parentWeight) {
        this.parentWeight = parentWeight;
    }

    public Long getParentVolume() {
        return parentVolume;
    }

    public void setParentVolume(Long parentVolume) {
        this.parentVolume = parentVolume;
    }

    public Integer getParentPriority() {
        return parentPriority;
    }

    public void setParentPriority(Integer parentPriority) {
        this.parentPriority = parentPriority;
    }

    public Double getParentImportance() {
        return parentImportance;
    }

    public void setParentImportance(Double parentImportance) {
        this.parentImportance = parentImportance;
    }

    public LocalDate getParentActualStartDate() {
        return parentActualStartDate;
    }

    public void setParentActualStartDate(LocalDate parentActualStartDate) {
        this.parentActualStartDate = parentActualStartDate;
    }

    public LocalDate getParentActualEndDate() {
        return parentActualEndDate;
    }

    public void setParentActualEndDate(LocalDate parentActualEndDate) {
        this.parentActualEndDate = parentActualEndDate;
    }

    public LocalDate getParentExpirationDate() {
        return parentExpirationDate;
    }

    public void setParentExpirationDate(LocalDate parentExpirationDate) {
        this.parentExpirationDate = parentExpirationDate;
    }

    public LocalDate getParentStartDate() {
        return parentStartDate;
    }

    public void setParentStartDate(LocalDate parentStartDate) {
        this.parentStartDate = parentStartDate;
    }

    public LocalDate getParentEndDate() {
        return parentEndDate;
    }

    public void setParentEndDate(LocalDate parentEndDate) {
        this.parentEndDate = parentEndDate;
    }

    public String getParentFiscalYear() {
        return parentFiscalYear;
    }

    public void setParentFiscalYear(String parentFiscalYear) {
        this.parentFiscalYear = parentFiscalYear;
    }

    public Long getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Long parentLevel) {
        this.parentLevel = parentLevel;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectPvTypeId() {
        return projectPvTypeId;
    }

    public void setProjectPvTypeId(Long projectPvTypeId) {
        this.projectPvTypeId = projectPvTypeId;
    }

    public String getProjectPvTypeTitle() {
        return projectPvTypeTitle;
    }

    public void setProjectPvTypeTitle(String projectPvTypeTitle) {
        this.projectPvTypeTitle = projectPvTypeTitle;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public Boolean getProjectIsRoot() {
        return projectIsRoot;
    }

    public void setProjectIsRoot(Boolean projectIsRoot) {
        this.projectIsRoot = projectIsRoot;
    }

    public Double getProjectInitialPercentage() {
        return projectInitialPercentage;
    }

    public void setProjectInitialPercentage(Double projectInitialPercentage) {
        this.projectInitialPercentage = projectInitialPercentage;
    }

    public Double getProjectPercentage() {
        return projectPercentage;
    }

    public void setProjectPercentage(Double projectPercentage) {
        this.projectPercentage = projectPercentage;
    }

    public Long getProjectWeight() {
        return projectWeight;
    }

    public void setProjectWeight(Long projectWeight) {
        this.projectWeight = projectWeight;
    }

    public Long getProjectVolume() {
        return projectVolume;
    }

    public void setProjectVolume(Long projectVolume) {
        this.projectVolume = projectVolume;
    }

    public Integer getProjectPriority() {
        return projectPriority;
    }

    public void setProjectPriority(Integer projectPriority) {
        this.projectPriority = projectPriority;
    }

    public Double getProjectImportance() {
        return projectImportance;
    }

    public void setProjectImportance(Double projectImportance) {
        this.projectImportance = projectImportance;
    }

    public LocalDate getProjectActualStartDate() {
        return projectActualStartDate;
    }

    public void setProjectActualStartDate(LocalDate projectActualStartDate) {
        this.projectActualStartDate = projectActualStartDate;
    }

    public LocalDate getProjectActualEndDate() {
        return projectActualEndDate;
    }

    public void setProjectActualEndDate(LocalDate projectActualEndDate) {
        this.projectActualEndDate = projectActualEndDate;
    }

    public LocalDate getProjectExpirationDate() {
        return projectExpirationDate;
    }

    public void setProjectExpirationDate(LocalDate projectExpirationDate) {
        this.projectExpirationDate = projectExpirationDate;
    }

    public LocalDate getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(LocalDate projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public LocalDate getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(LocalDate projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectFiscalYear() {
        return projectFiscalYear;
    }

    public void setProjectFiscalYear(String projectFiscalYear) {
        this.projectFiscalYear = projectFiscalYear;
    }

    public Long getProjectLevel() {
        return projectLevel;
    }

    public void setProjectLevel(Long projectLevel) {
        this.projectLevel = projectLevel;
    }
}