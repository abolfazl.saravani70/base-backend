/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.OrganizationUnitJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.base.MonthsPercentage;
import com.rbp.sayban.model.domainmodel.todo.Todo;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TODO_ORGANIZATION_UNIT_VIEW")
public class TodoOrganizationUnitFV extends EvalStateBaseEntity {

    @Column(name = "ASSIGN_DATE")
    private LocalDate assignDate;

    @Column(name = "DUE_DATE")
    private LocalDate dueDate;

    @Column(name = "FK_PV_STATE_ID")
    private Long pvStateId;

    @Column(name = "PV_STATE_TITLE")
    private String pvStateTitle;

    @Column(name = "FK_PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long orgUnitId;

    @Column(name = "M1")
    private Double m1;

    @Column(name = "M2")
    private Double m2;

    @Column(name = "M3")
    private Double m3;

    @Column(name = "M4")
    private Double m4;

    @Column(name = "M5")
    private Double m5;

    @Column(name = "M6")
    private Double m6;

    @Column(name = "M7")
    private Double m7;

    @Column(name = "M8")
    private Double m8;

    @Column(name = "M9")
    private Double m9;

    @Column(name = "M10")
    private Double m10;

    @Column(name = "M11")
    private Double m11;

    @Column(name = "M12")
    private Double m12;

    @Column(name = "FK_TODO_ID")
    private Long todoId;

    @Column(name = "TODO_FK_PV_TYPE_ID")
    private Long todoPvTypeId;

    @Column(name = "TODO_PV_TYPE_TITLE")
    private String  todoPvTypeTitle;

    @Column(name = "TODO_NAME")
    private String todoName;

    @Column(name = "TODO_TITLE")
    private String todoTitle;

    @Column(name = "TODO_IS_ROOT")
    private Boolean todoIsRoot;

    @Column(name = "TODO_INITIAL_PERCENTAGE")
    private Double todoInitialPercentage;

    @Column(name = "TODO_PERCENTAGE")
    private Double todoPercentage;

    @Column(name = "TODO_WEIGHT")
    private Long todoWeight;

    @Column(name = "TODO_VOLUME")
    private Long todoVolume;

    @Column(name = "TODO_PRIORITY")
    private Integer todoPriority;

    @Column(name = "TODO_IMPORTANCE")
    private Double todoImportance;

    @Column(name = "TODO_ACTUAL_START_DATE")
    private LocalDate todoActualStartDate;

    @Column(name = "TODO_ACTUAL_END_DATE")
    private LocalDate todoActualEndDate;

    @Column(name = "TODO_EXPIRATION_DATE")
    private LocalDate todoExpirationDate;

    @Column(name = "TODO_START_DATE")
    private LocalDate todoStartDate;

    @Column(name = "TODO_END_DATE")
    private LocalDate todoEndDate;

    @Column(name = "TODO_FISCAL_YEAR")
    private String todoFiscalYear;

    @Column(name = "TODO_LEVEL$")
    private Long todoLevel;

    public LocalDate getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(LocalDate assignDate) {
        this.assignDate = assignDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getPvStateId() {
        return pvStateId;
    }

    public void setPvStateId(Long pvStateId) {
        this.pvStateId = pvStateId;
    }

    public String getPvStateTitle() {
        return pvStateTitle;
    }

    public void setPvStateTitle(String pvStateTitle) {
        this.pvStateTitle = pvStateTitle;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Double getM1() {
        return m1;
    }

    public void setM1(Double m1) {
        this.m1 = m1;
    }

    public Double getM2() {
        return m2;
    }

    public void setM2(Double m2) {
        this.m2 = m2;
    }

    public Double getM3() {
        return m3;
    }

    public void setM3(Double m3) {
        this.m3 = m3;
    }

    public Double getM4() {
        return m4;
    }

    public void setM4(Double m4) {
        this.m4 = m4;
    }

    public Double getM5() {
        return m5;
    }

    public void setM5(Double m5) {
        this.m5 = m5;
    }

    public Double getM6() {
        return m6;
    }

    public void setM6(Double m6) {
        this.m6 = m6;
    }

    public Double getM7() {
        return m7;
    }

    public void setM7(Double m7) {
        this.m7 = m7;
    }

    public Double getM8() {
        return m8;
    }

    public void setM8(Double m8) {
        this.m8 = m8;
    }

    public Double getM9() {
        return m9;
    }

    public void setM9(Double m9) {
        this.m9 = m9;
    }

    public Double getM10() {
        return m10;
    }

    public void setM10(Double m10) {
        this.m10 = m10;
    }

    public Double getM11() {
        return m11;
    }

    public void setM11(Double m11) {
        this.m11 = m11;
    }

    public Double getM12() {
        return m12;
    }

    public void setM12(Double m12) {
        this.m12 = m12;
    }

    public Long getTodoId() {
        return todoId;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    public Long getTodoPvTypeId() {
        return todoPvTypeId;
    }

    public void setTodoPvTypeId(Long todoPvTypeId) {
        this.todoPvTypeId = todoPvTypeId;
    }

    public String getTodoPvTypeTitle() {
        return todoPvTypeTitle;
    }

    public void setTodoPvTypeTitle(String todoPvTypeTitle) {
        this.todoPvTypeTitle = todoPvTypeTitle;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    public Boolean getTodoIsRoot() {
        return todoIsRoot;
    }

    public void setTodoIsRoot(Boolean todoIsRoot) {
        this.todoIsRoot = todoIsRoot;
    }

    public Double getTodoInitialPercentage() {
        return todoInitialPercentage;
    }

    public void setTodoInitialPercentage(Double todoInitialPercentage) {
        this.todoInitialPercentage = todoInitialPercentage;
    }

    public Double getTodoPercentage() {
        return todoPercentage;
    }

    public void setTodoPercentage(Double todoPercentage) {
        this.todoPercentage = todoPercentage;
    }

    public Long getTodoWeight() {
        return todoWeight;
    }

    public void setTodoWeight(Long todoWeight) {
        this.todoWeight = todoWeight;
    }

    public Long getTodoVolume() {
        return todoVolume;
    }

    public void setTodoVolume(Long todoVolume) {
        this.todoVolume = todoVolume;
    }

    public Integer getTodoPriority() {
        return todoPriority;
    }

    public void setTodoPriority(Integer todoPriority) {
        this.todoPriority = todoPriority;
    }

    public Double getTodoImportance() {
        return todoImportance;
    }

    public void setTodoImportance(Double todoImportance) {
        this.todoImportance = todoImportance;
    }

    public LocalDate getTodoActualStartDate() {
        return todoActualStartDate;
    }

    public void setTodoActualStartDate(LocalDate todoActualStartDate) {
        this.todoActualStartDate = todoActualStartDate;
    }

    public LocalDate getTodoActualEndDate() {
        return todoActualEndDate;
    }

    public void setTodoActualEndDate(LocalDate todoActualEndDate) {
        this.todoActualEndDate = todoActualEndDate;
    }

    public LocalDate getTodoExpirationDate() {
        return todoExpirationDate;
    }

    public void setTodoExpirationDate(LocalDate todoExpirationDate) {
        this.todoExpirationDate = todoExpirationDate;
    }

    public LocalDate getTodoStartDate() {
        return todoStartDate;
    }

    public void setTodoStartDate(LocalDate todoStartDate) {
        this.todoStartDate = todoStartDate;
    }

    public LocalDate getTodoEndDate() {
        return todoEndDate;
    }

    public void setTodoEndDate(LocalDate todoEndDate) {
        this.todoEndDate = todoEndDate;
    }

    public String getTodoFiscalYear() {
        return todoFiscalYear;
    }

    public void setTodoFiscalYear(String todoFiscalYear) {
        this.todoFiscalYear = todoFiscalYear;
    }

    public Long getTodoLevel() {
        return todoLevel;
    }

    public void setTodoLevel(Long todoLevel) {
        this.todoLevel = todoLevel;
    }
}
