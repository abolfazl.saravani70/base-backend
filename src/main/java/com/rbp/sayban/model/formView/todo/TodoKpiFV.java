/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.KpiJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.todo.Todo;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TODO_KPI_VIEW")
public class TodoKpiFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_TODO_ID")
    private Long todoId;

    @Column(name = "TODO_FK_PV_TYPE_ID")
    private Long todoPvTypeId;

    @Column(name = "TODO_PV_TYPE_TITLE")
    private String  todoPvTypeTitle;

    @Column(name = "TODO_NAME")
    private String todoName;

    @Column(name = "TODO_TITLE")
    private String todoTitle;

    @Column(name = "TODO_IS_ROOT")
    private Boolean todoIsRoot;

    @Column(name = "TODO_INITIAL_PERCENTAGE")
    private Double todoInitialPercentage;

    @Column(name = "TODO_PERCENTAGE")
    private Double todoPercentage;

    @Column(name = "TODO_WEIGHT")
    private Long todoWeight;

    @Column(name = "TODO_VOLUME")
    private Long todoVolume;

    @Column(name = "TODO_PRIORITY")
    private Integer todoPriority;

    @Column(name = "TODO_IMPORTANCE")
    private Double todoImportance;

    @Column(name = "TODO_ACTUAL_START_DATE")
    private LocalDate todoActualStartDate;

    @Column(name = "TODO_ACTUAL_END_DATE")
    private LocalDate todoActualEndDate;

    @Column(name = "TODO_EXPIRATION_DATE")
    private LocalDate todoExpirationDate;

    @Column(name = "TODO_START_DATE")
    private LocalDate todoStartDate;

    @Column(name = "TODO_END_DATE")
    private LocalDate todoEndDate;

    @Column(name = "TODO_FISCAL_YEAR")
    private String todoFiscalYear;

    @Column(name = "TODO_LEVEL$")
    private Long todoLevel;

    @Column(name = "KPI_NAME")
    private String kpiName;

    @Column(name = "KPI_TITLE")
    private String kpiTitle;

    @Column(name = "KPI_IS_ROOT")
    private Boolean kpiIsRoot;

    @Column(name = "KPI_INITIAL_PERCENTAGE")
    private Double kpiInitialPercentage;

    @Column(name = "KPI_PERCENTAGE")
    private Double kpiPercentage;

    @Column(name = "KPI_WEIGHT")
    private Long kpiWeight;

    @Column(name = "KPI_VOLUME")
    private Long kpiVolume;

    @Column(name = "KPI_PRIORITY")
    private Integer kpiPriority;

    @Column(name = "KPI_IMPORTANCE")
    private Double kpiImportance;

    @Column(name = "KPI_ACTUAL_START_DATE")
    private LocalDate kpiActualStartDate;

    @Column(name = "KPI_ACTUAL_END_DATE")
    private LocalDate kpiActualEndDate;

    @Column(name = "KPI_EXPIRATION_DATE")
    private LocalDate kpiExpirationDate;

    @Column(name = "KPI_START_DATE")
    private LocalDate kpiStartDate;

    @Column(name = "KPI_END_DATE")
    private LocalDate kpiEndDate;

    @Column(name = "KPI_FISCAL_YEAR")
    private String kpiFiscalYear;

    @Column(name = "KPI_LEVEL$")
    private Long kpiLevel;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getTodoId() {
        return todoId;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    public Long getTodoPvTypeId() {
        return todoPvTypeId;
    }

    public void setTodoPvTypeId(Long todoPvTypeId) {
        this.todoPvTypeId = todoPvTypeId;
    }

    public String getTodoPvTypeTitle() {
        return todoPvTypeTitle;
    }

    public void setTodoPvTypeTitle(String todoPvTypeTitle) {
        this.todoPvTypeTitle = todoPvTypeTitle;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    public Boolean getTodoIsRoot() {
        return todoIsRoot;
    }

    public void setTodoIsRoot(Boolean todoIsRoot) {
        this.todoIsRoot = todoIsRoot;
    }

    public Double getTodoInitialPercentage() {
        return todoInitialPercentage;
    }

    public void setTodoInitialPercentage(Double todoInitialPercentage) {
        this.todoInitialPercentage = todoInitialPercentage;
    }

    public Double getTodoPercentage() {
        return todoPercentage;
    }

    public void setTodoPercentage(Double todoPercentage) {
        this.todoPercentage = todoPercentage;
    }

    public Long getTodoWeight() {
        return todoWeight;
    }

    public void setTodoWeight(Long todoWeight) {
        this.todoWeight = todoWeight;
    }

    public Long getTodoVolume() {
        return todoVolume;
    }

    public void setTodoVolume(Long todoVolume) {
        this.todoVolume = todoVolume;
    }

    public Integer getTodoPriority() {
        return todoPriority;
    }

    public void setTodoPriority(Integer todoPriority) {
        this.todoPriority = todoPriority;
    }

    public Double getTodoImportance() {
        return todoImportance;
    }

    public void setTodoImportance(Double todoImportance) {
        this.todoImportance = todoImportance;
    }

    public LocalDate getTodoActualStartDate() {
        return todoActualStartDate;
    }

    public void setTodoActualStartDate(LocalDate todoActualStartDate) {
        this.todoActualStartDate = todoActualStartDate;
    }

    public LocalDate getTodoActualEndDate() {
        return todoActualEndDate;
    }

    public void setTodoActualEndDate(LocalDate todoActualEndDate) {
        this.todoActualEndDate = todoActualEndDate;
    }

    public LocalDate getTodoExpirationDate() {
        return todoExpirationDate;
    }

    public void setTodoExpirationDate(LocalDate todoExpirationDate) {
        this.todoExpirationDate = todoExpirationDate;
    }

    public LocalDate getTodoStartDate() {
        return todoStartDate;
    }

    public void setTodoStartDate(LocalDate todoStartDate) {
        this.todoStartDate = todoStartDate;
    }

    public LocalDate getTodoEndDate() {
        return todoEndDate;
    }

    public void setTodoEndDate(LocalDate todoEndDate) {
        this.todoEndDate = todoEndDate;
    }

    public String getTodoFiscalYear() {
        return todoFiscalYear;
    }

    public void setTodoFiscalYear(String todoFiscalYear) {
        this.todoFiscalYear = todoFiscalYear;
    }

    public Long getTodoLevel() {
        return todoLevel;
    }

    public void setTodoLevel(Long todoLevel) {
        this.todoLevel = todoLevel;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getKpiTitle() {
        return kpiTitle;
    }

    public void setKpiTitle(String kpiTitle) {
        this.kpiTitle = kpiTitle;
    }

    public Boolean getKpiIsRoot() {
        return kpiIsRoot;
    }

    public void setKpiIsRoot(Boolean kpiIsRoot) {
        this.kpiIsRoot = kpiIsRoot;
    }

    public Double getKpiInitialPercentage() {
        return kpiInitialPercentage;
    }

    public void setKpiInitialPercentage(Double kpiInitialPercentage) {
        this.kpiInitialPercentage = kpiInitialPercentage;
    }

    public Double getKpiPercentage() {
        return kpiPercentage;
    }

    public void setKpiPercentage(Double kpiPercentage) {
        this.kpiPercentage = kpiPercentage;
    }

    public Long getKpiWeight() {
        return kpiWeight;
    }

    public void setKpiWeight(Long kpiWeight) {
        this.kpiWeight = kpiWeight;
    }

    public Long getKpiVolume() {
        return kpiVolume;
    }

    public void setKpiVolume(Long kpiVolume) {
        this.kpiVolume = kpiVolume;
    }

    public Integer getKpiPriority() {
        return kpiPriority;
    }

    public void setKpiPriority(Integer kpiPriority) {
        this.kpiPriority = kpiPriority;
    }

    public Double getKpiImportance() {
        return kpiImportance;
    }

    public void setKpiImportance(Double kpiImportance) {
        this.kpiImportance = kpiImportance;
    }

    public LocalDate getKpiActualStartDate() {
        return kpiActualStartDate;
    }

    public void setKpiActualStartDate(LocalDate kpiActualStartDate) {
        this.kpiActualStartDate = kpiActualStartDate;
    }

    public LocalDate getKpiActualEndDate() {
        return kpiActualEndDate;
    }

    public void setKpiActualEndDate(LocalDate kpiActualEndDate) {
        this.kpiActualEndDate = kpiActualEndDate;
    }

    public LocalDate getKpiExpirationDate() {
        return kpiExpirationDate;
    }

    public void setKpiExpirationDate(LocalDate kpiExpirationDate) {
        this.kpiExpirationDate = kpiExpirationDate;
    }

    public LocalDate getKpiStartDate() {
        return kpiStartDate;
    }

    public void setKpiStartDate(LocalDate kpiStartDate) {
        this.kpiStartDate = kpiStartDate;
    }

    public LocalDate getKpiEndDate() {
        return kpiEndDate;
    }

    public void setKpiEndDate(LocalDate kpiEndDate) {
        this.kpiEndDate = kpiEndDate;
    }

    public String getKpiFiscalYear() {
        return kpiFiscalYear;
    }

    public void setKpiFiscalYear(String kpiFiscalYear) {
        this.kpiFiscalYear = kpiFiscalYear;
    }

    public Long getKpiLevel() {
        return kpiLevel;
    }

    public void setKpiLevel(Long kpiLevel) {
        this.kpiLevel = kpiLevel;
    }
}
