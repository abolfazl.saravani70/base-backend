/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.model.domainmodel.application.Topic;
import com.rbp.sayban.model.domainmodel.system.System;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Immutable
@Table(name = "ISSUE_VIEW")
public class IssueFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "ENGLISH_NAME")
    private String engName;

    @Column(name = "KEY")
    private Integer key;

    @Column(name = "TYPE")
    private String  type;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column(name = "IS_RESOLVED")
    private Boolean isResolved;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    @Column(name = "FK_TOPIC_ID")
    private Long topicId;

    @Column(name = "TOPIC_NAME")
    private String topicName;

    @Column(name = "TOPIC_TITLE")
    private String topicTitle;

    @Column(name = "TOPIC_RESULT")
    private String topicResult;

    @Column(name = "TOPIC_FAQ_URL")
    private String topicFaqUrl;

    @Column(name = "TOPIC_TEXT")
    private String topicText;

    @Column(name = "TOPIC_FROM_DATE")
    private LocalDate topicFromDate;

    @Column(name = "TOPIC_TO_DATE")
    private LocalDate topicToDate;

    @Column(name = "TOPIC_EXPIRATION_DATE")
    private LocalDate topicExpirationDate;

    @Column(name = "TOPIC_IS_ACTIVE")
    private Boolean topicIsActive;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        isDeprecated = deprecated;
    }

    public Boolean getResolved() {
        return isResolved;
    }

    public void setResolved(Boolean resolved) {
        isResolved = resolved;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public String getTopicResult() {
        return topicResult;
    }

    public void setTopicResult(String topicResult) {
        this.topicResult = topicResult;
    }

    public String getTopicFaqUrl() {
        return topicFaqUrl;
    }

    public void setTopicFaqUrl(String topicFaqUrl) {
        this.topicFaqUrl = topicFaqUrl;
    }

    public String getTopicText() {
        return topicText;
    }

    public void setTopicText(String topicText) {
        this.topicText = topicText;
    }

    public LocalDate getTopicFromDate() {
        return topicFromDate;
    }

    public void setTopicFromDate(LocalDate topicFromDate) {
        this.topicFromDate = topicFromDate;
    }

    public LocalDate getTopicToDate() {
        return topicToDate;
    }

    public void setTopicToDate(LocalDate topicToDate) {
        this.topicToDate = topicToDate;
    }

    public LocalDate getTopicExpirationDate() {
        return topicExpirationDate;
    }

    public void setTopicExpirationDate(LocalDate topicExpirationDate) {
        this.topicExpirationDate = topicExpirationDate;
    }

    public Boolean getTopicIsActive() {
        return topicIsActive;
    }

    public void setTopicIsActive(Boolean topicIsActive) {
        this.topicIsActive = topicIsActive;
    }
}
