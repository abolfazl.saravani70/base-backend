/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.norm;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "COST_VIEW")
public class CostFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "FK_PV_CENTRALIZE_TYPE_ID")
    private Long pvCentralizeTypeId; // متمرکز، غیر متمرکز

    @Column(name = "PV_CENTRALIZE_TYPE_TITLE")
    private String pvCentralizeTypeTitle;

    @Column(name = "FK_PV_CASH_TYPE_ID")
    private Long pvCashTypeId; // نقدی، غیر نقدی

    @Column(name = "PV_CASH_TYPE_TITLE")
    private String pvCashTypeTitle;

    @Column(name = "FK_PV_ORG_LEVEL_ID")
    private Long pvOrgLevelId;

    @Column(name = "PV_ORG_LEVEL_TITLE")
    private String pvOrgLevelTitle;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long organizationUnitId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPvCentralizeTypeId() {
        return pvCentralizeTypeId;
    }

    public void setPvCentralizeTypeId(Long pvCentralizeTypeId) {
        this.pvCentralizeTypeId = pvCentralizeTypeId;
    }

    public String getPvCentralizeTypeTitle() {
        return pvCentralizeTypeTitle;
    }

    public void setPvCentralizeTypeTitle(String pvCentralizeTypeTitle) {
        this.pvCentralizeTypeTitle = pvCentralizeTypeTitle;
    }

    public Long getPvCashTypeId() {
        return pvCashTypeId;
    }

    public void setPvCashTypeId(Long pvCashTypeId) {
        this.pvCashTypeId = pvCashTypeId;
    }

    public String getPvCashTypeTitle() {
        return pvCashTypeTitle;
    }

    public void setPvCashTypeTitle(String pvCashTypeTitle) {
        this.pvCashTypeTitle = pvCashTypeTitle;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public String getPvOrgLevelTitle() {
        return pvOrgLevelTitle;
    }

    public void setPvOrgLevelTitle(String pvOrgLevelTitle) {
        this.pvOrgLevelTitle = pvOrgLevelTitle;
    }

    public Long getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Long organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }
}
