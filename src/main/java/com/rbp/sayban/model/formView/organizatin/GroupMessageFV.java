/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "GROUP_MESSAGE_VIEW")
public class GroupMessageFV extends BaseEntity {

    @Column(name = "FK_GROUP_ID")
    private Long groupId;

    @Column(name = "GROUP_NAME")
    private String groupName;

    @Column(name = "GROUP_PERSIAN_NAME")
    private String groupPersianName;

    @Column(name = "FK_MESSAGE_ID")
    private Long messageID;

    @Column(name = "MESSAGE_BODY")
    private String messageBody;

    @Column(name = "MESSAGE_MESSAGE_FROM")
    private String messageMessageFrom;

    @Column(name = "MESSAGE_SUBJECT")
    private String messageSubject;

    @Column(name = "MESSAGE_CC")
    private String messageCc;

    @Column(name = "TEXT")
    private String messageText;

    @Column(name = "ATTACH")
    private String messageAttach;

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupPersianName() {
        return groupPersianName;
    }

    public void setGroupPersianName(String groupPersianName) {
        this.groupPersianName = groupPersianName;
    }

    public Long getMessageID() {
        return messageID;
    }

    public void setMessageID(Long messageID) {
        this.messageID = messageID;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageMessageFrom() {
        return messageMessageFrom;
    }

    public void setMessageMessageFrom(String messageMessageFrom) {
        this.messageMessageFrom = messageMessageFrom;
    }

    public String getMessageSubject() {
        return messageSubject;
    }

    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }

    public String getMessageCc() {
        return messageCc;
    }

    public void setMessageCc(String messageCc) {
        this.messageCc = messageCc;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageAttach() {
        return messageAttach;
    }

    public void setMessageAttach(String messageAttach) {
        this.messageAttach = messageAttach;
    }
}
