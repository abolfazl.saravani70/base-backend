/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.project;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.SubjectiveJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.project.Project;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "PROJECT_SUBJECTIVE_VIEW")
public class ProjectSubjectiveFV extends EvalStateBaseEntity {

    @Column(name = "FK_PROJECT_ID")
    private Long projectId;

    @Column(name = "PROJECT_FK_PV_TYPE_ID")
    private Long projectPvTypeId;

    @Column(name = "PROJECT_PV_TYPE_TITLE")
    private String projectPvTypeTitle;

    @Column(name = "PROJECT_NAME")
    private String projectName;

    @Column(name = "PROJECT_TITLE")
    private String projectTitle;

    @Column(name = "PROJECT_IS_ROOT")
    private Boolean projectIsRoot;

    @Column(name = "PROJECT_INITIAL_PERCENTAGE")
    private Double projectInitialPercentage;

    @Column(name = "PROJECT_PERCENTAGE")
    private Double projectPercentage;

    @Column(name = "PROJECT_WEIGHT")
    private Long projectWeight;

    @Column(name = "PROJECT_VOLUME")
    private Long projectVolume;

    @Column(name = "PROJECT_PRIORITY")
    private Integer projectPriority;

    @Column(name = "PROJECT_IMPORTANCE")
    private Double projectImportance;

    @Column(name = "PROJECT_ACTUAL_START_DATE")
    private LocalDate projectActualStartDate;

    @Column(name = "PROJECT_ACTUAL_END_DATE")
    private LocalDate projectActualEndDate;

    @Column(name = "PROJECT_EXPIRATION_DATE")
    private LocalDate projectExpirationDate;

    @Column(name = "PROJECT_START_DATE")
    private LocalDate projectStartDate;

    @Column(name = "PROJECT_END_DATE")
    private LocalDate projectEndDate;

    @Column(name = "PROJECT_FISCAL_YEAR")
    private String projectFiscalYear;

    @Column(name = "PROJECT_LEVEL$")
    private Long projectLevel;

    @Column(name = "FK_SUBJECTIVE_ID")
    private Long subjectiveId;

    @Column(name = "SUBJECTIVE_NAME")
    private String subjectiveName;

    @Column(name = "SUBJECTIVE_TITLE")
    private String subjectiveTitle;

    @Column(name = "SUBJECTIVE_IS_ROOT")
    private Boolean subjectiveIsRoot;

    @Column(name = "SUBJECTIVE_INITIAL_PERCENTAGE")
    private Double subjectiveInitialPercentage;

    @Column(name = "SUBJECTIVE_PERCENTAGE")
    private Double subjectivePercentage;

    @Column(name = "SUBJECTIVE_WEIGHT")
    private Long subjectiveWeight;

    @Column(name = "SUBJECTIVE_VOLUME")
    private Long subjectiveVolume;

    @Column(name = "SUBJECTIVE_PRIORITY")
    private Integer subjectivePriority;

    @Column(name = "SUBJECTIVE_IMPORTANCE")
    private Double subjectiveImportance;

    @Column(name = "SUBJECTIVE_ACTUAL_START_DATE")
    private LocalDate subjectiveActualStartDate;

    @Column(name = "SUBJECTIVE_ACTUAL_END_DATE")
    private LocalDate subjectiveActualEndDate;

    @Column(name = "SUBJECTIVE_EXPIRATION_DATE")
    private LocalDate subjectiveExpirationDate;

    @Column(name = "SUBJECTIVE_START_DATE")
    private LocalDate subjectiveStartDate;

    @Column(name = "SUBJECTIVE_END_DATE")
    private LocalDate subjectiveEndDate;

    @Column(name = "SUBJECTIVE_FISCAL_YEAR")
    private String subjectiveFiscalYear;

    @Column(name = "SUBJECTIVE_LEVEL$")
    private Long subjectiveLevel;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectPvTypeId() {
        return projectPvTypeId;
    }

    public void setProjectPvTypeId(Long projectPvTypeId) {
        this.projectPvTypeId = projectPvTypeId;
    }

    public String getProjectPvTypeTitle() {
        return projectPvTypeTitle;
    }

    public void setProjectPvTypeTitle(String projectPvTypeTitle) {
        this.projectPvTypeTitle = projectPvTypeTitle;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public Boolean getProjectIsRoot() {
        return projectIsRoot;
    }

    public void setProjectIsRoot(Boolean projectIsRoot) {
        this.projectIsRoot = projectIsRoot;
    }

    public Double getProjectInitialPercentage() {
        return projectInitialPercentage;
    }

    public void setProjectInitialPercentage(Double projectInitialPercentage) {
        this.projectInitialPercentage = projectInitialPercentage;
    }

    public Double getProjectPercentage() {
        return projectPercentage;
    }

    public void setProjectPercentage(Double projectPercentage) {
        this.projectPercentage = projectPercentage;
    }

    public Long getProjectWeight() {
        return projectWeight;
    }

    public void setProjectWeight(Long projectWeight) {
        this.projectWeight = projectWeight;
    }

    public Long getProjectVolume() {
        return projectVolume;
    }

    public void setProjectVolume(Long projectVolume) {
        this.projectVolume = projectVolume;
    }

    public Integer getProjectPriority() {
        return projectPriority;
    }

    public void setProjectPriority(Integer projectPriority) {
        this.projectPriority = projectPriority;
    }

    public Double getProjectImportance() {
        return projectImportance;
    }

    public void setProjectImportance(Double projectImportance) {
        this.projectImportance = projectImportance;
    }

    public LocalDate getProjectActualStartDate() {
        return projectActualStartDate;
    }

    public void setProjectActualStartDate(LocalDate projectActualStartDate) {
        this.projectActualStartDate = projectActualStartDate;
    }

    public LocalDate getProjectActualEndDate() {
        return projectActualEndDate;
    }

    public void setProjectActualEndDate(LocalDate projectActualEndDate) {
        this.projectActualEndDate = projectActualEndDate;
    }

    public LocalDate getProjectExpirationDate() {
        return projectExpirationDate;
    }

    public void setProjectExpirationDate(LocalDate projectExpirationDate) {
        this.projectExpirationDate = projectExpirationDate;
    }

    public LocalDate getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(LocalDate projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public LocalDate getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(LocalDate projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectFiscalYear() {
        return projectFiscalYear;
    }

    public void setProjectFiscalYear(String projectFiscalYear) {
        this.projectFiscalYear = projectFiscalYear;
    }

    public Long getProjectLevel() {
        return projectLevel;
    }

    public void setProjectLevel(Long projectLevel) {
        this.projectLevel = projectLevel;
    }

    public Long getSubjectiveId() {
        return subjectiveId;
    }

    public void setSubjectiveId(Long subjectiveId) {
        this.subjectiveId = subjectiveId;
    }

    public String getSubjectiveName() {
        return subjectiveName;
    }

    public void setSubjectiveName(String subjectiveName) {
        this.subjectiveName = subjectiveName;
    }

    public String getSubjectiveTitle() {
        return subjectiveTitle;
    }

    public void setSubjectiveTitle(String subjectiveTitle) {
        this.subjectiveTitle = subjectiveTitle;
    }

    public Boolean getSubjectiveIsRoot() {
        return subjectiveIsRoot;
    }

    public void setSubjectiveIsRoot(Boolean subjectiveIsRoot) {
        this.subjectiveIsRoot = subjectiveIsRoot;
    }

    public Double getSubjectiveInitialPercentage() {
        return subjectiveInitialPercentage;
    }

    public void setSubjectiveInitialPercentage(Double subjectiveInitialPercentage) {
        this.subjectiveInitialPercentage = subjectiveInitialPercentage;
    }

    public Double getSubjectivePercentage() {
        return subjectivePercentage;
    }

    public void setSubjectivePercentage(Double subjectivePercentage) {
        this.subjectivePercentage = subjectivePercentage;
    }

    public Long getSubjectiveWeight() {
        return subjectiveWeight;
    }

    public void setSubjectiveWeight(Long subjectiveWeight) {
        this.subjectiveWeight = subjectiveWeight;
    }

    public Long getSubjectiveVolume() {
        return subjectiveVolume;
    }

    public void setSubjectiveVolume(Long subjectiveVolume) {
        this.subjectiveVolume = subjectiveVolume;
    }

    public Integer getSubjectivePriority() {
        return subjectivePriority;
    }

    public void setSubjectivePriority(Integer subjectivePriority) {
        this.subjectivePriority = subjectivePriority;
    }

    public Double getSubjectiveImportance() {
        return subjectiveImportance;
    }

    public void setSubjectiveImportance(Double subjectiveImportance) {
        this.subjectiveImportance = subjectiveImportance;
    }

    public LocalDate getSubjectiveActualStartDate() {
        return subjectiveActualStartDate;
    }

    public void setSubjectiveActualStartDate(LocalDate subjectiveActualStartDate) {
        this.subjectiveActualStartDate = subjectiveActualStartDate;
    }

    public LocalDate getSubjectiveActualEndDate() {
        return subjectiveActualEndDate;
    }

    public void setSubjectiveActualEndDate(LocalDate subjectiveActualEndDate) {
        this.subjectiveActualEndDate = subjectiveActualEndDate;
    }

    public LocalDate getSubjectiveExpirationDate() {
        return subjectiveExpirationDate;
    }

    public void setSubjectiveExpirationDate(LocalDate subjectiveExpirationDate) {
        this.subjectiveExpirationDate = subjectiveExpirationDate;
    }

    public LocalDate getSubjectiveStartDate() {
        return subjectiveStartDate;
    }

    public void setSubjectiveStartDate(LocalDate subjectiveStartDate) {
        this.subjectiveStartDate = subjectiveStartDate;
    }

    public LocalDate getSubjectiveEndDate() {
        return subjectiveEndDate;
    }

    public void setSubjectiveEndDate(LocalDate subjectiveEndDate) {
        this.subjectiveEndDate = subjectiveEndDate;
    }

    public String getSubjectiveFiscalYear() {
        return subjectiveFiscalYear;
    }

    public void setSubjectiveFiscalYear(String subjectiveFiscalYear) {
        this.subjectiveFiscalYear = subjectiveFiscalYear;
    }

    public Long getSubjectiveLevel() {
        return subjectiveLevel;
    }

    public void setSubjectiveLevel(Long subjectiveLevel) {
        this.subjectiveLevel = subjectiveLevel;
    }
}
