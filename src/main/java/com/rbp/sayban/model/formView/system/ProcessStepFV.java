/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.ProcessTemplate;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "PROCESS_STEP_VIEW")
public class ProcessStepFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CODE")
    private String code;

    @Column(name = "KEY")
    private String key;

    @Column(name = "STEP_NUMBER")
    private String stepNumber;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "FK_PROCESS_TEMPLATE_ID")
    private Long processTemplateId;

    @Column(name = "PROCESS_TEMPLATE_TYPE")
    private String processTemplateType;

    @Column(name = "PROCESS_TEMPLATE_NAME")
    private String processTemplateName;

    @Column(name = "PROCESS_TEMPLATE_TITLE")
    private String processTemplateTitle;

    @Column(name = "PROCESS_TEMPLATE_KEY")
    private String processTemplateKey;

    @Column(name = "PROCESS_TEMPLATE_CODE")
    private String processTemplateCode;

    @Column(name = "PROCESS_TEMPLATE_VALUE")
    private String processTemplateValue;

    @Column(name = "PROCESS_TEMPLATE_IS_DEPRECATED")
    private Boolean processTemplateIsDeprecated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(String stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getProcessTemplateId() {
        return processTemplateId;
    }

    public void setProcessTemplateId(Long processTemplateId) {
        this.processTemplateId = processTemplateId;
    }

    public String getProcessTemplateType() {
        return processTemplateType;
    }

    public void setProcessTemplateType(String processTemplateType) {
        this.processTemplateType = processTemplateType;
    }

    public String getProcessTemplateName() {
        return processTemplateName;
    }

    public void setProcessTemplateName(String processTemplateName) {
        this.processTemplateName = processTemplateName;
    }

    public String getProcessTemplateTitle() {
        return processTemplateTitle;
    }

    public void setProcessTemplateTitle(String processTemplateTitle) {
        this.processTemplateTitle = processTemplateTitle;
    }

    public String getProcessTemplateKey() {
        return processTemplateKey;
    }

    public void setProcessTemplateKey(String processTemplateKey) {
        this.processTemplateKey = processTemplateKey;
    }

    public String getProcessTemplateCode() {
        return processTemplateCode;
    }

    public void setProcessTemplateCode(String processTemplateCode) {
        this.processTemplateCode = processTemplateCode;
    }

    public String getProcessTemplateValue() {
        return processTemplateValue;
    }

    public void setProcessTemplateValue(String processTemplateValue) {
        this.processTemplateValue = processTemplateValue;
    }

    public Boolean getProcessTemplateIsDeprecated() {
        return processTemplateIsDeprecated;
    }

    public void setProcessTemplateIsDeprecated(Boolean processTemplateIsDeprecated) {
        this.processTemplateIsDeprecated = processTemplateIsDeprecated;
    }
}
