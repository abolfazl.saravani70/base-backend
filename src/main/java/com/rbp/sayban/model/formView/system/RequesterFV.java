/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.Person;
import com.rbp.sayban.model.domainmodel.system.Blob;
import com.rbp.sayban.model.domainmodel.system.Request;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "REQUESTER_VIEW")
public class RequesterFV extends BaseEntity {

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "CELL_PHONE_NUMBER")
    private String cellPhoneNumber;

    @Column(name = "WORK_PHONE_NUMBER")
    private String workPhoneNumber;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "FK_REQUEST_ID")
    private Long requestId;


    @Column(name = "REQUEST_SUBJECT")
    private String requestSubject;

    @Column(name = "REQUEST_TYPE")
    private String requestType;

    @Column(name = "REQUEST_NUMBER$")
    private String requestNumber;

    @Column(name = "REQUEST_FK_OU_FROM_ID")
    private Long requestOrgUnitIdFrom;

    @Column(name = "REQUEST_FK_OU_TO_ID")
    private Long requestOrgUnitIdTo;

    @Column(name = "REQUEST_PV_REASON_TITLE")
    private String requestPvReasonTitle;

    @Column(name = "REQUEST_FK_PV_REASON_ID")
    private Long requestPvReasonId;

    @Column(name = "REQUEST_PV_REJECT_REASON_TITLE")
    private String requestPvRejectReasonTitle;

    @Column(name = "REQUEST_FK_PV_REJECT_REASON_ID")
    private Long requestPvRejectReasonId;

    @Column(name = "REQUEST_PV_STATE_TITLE")
    private String requestPvStateTitle;

    @Column(name = "REQUEST_FK_PV_STATE_ID")
    private Long requestPvStateId;

    @Column(name = "REQUEST_FK_PV_REQUESTER_TITLE")
    private String requestPvRequesterTitle;

    @Column(name = "REQUEST_FK_PV_REQUESTER_ID")
    private Long requestPvRequesterId;

    // TODO: 07/12/2019
    @Column(name = "FK_PERSON_ID")
    private Long personId;

    @Column(name = "PERSON_NATIONAL_ID")
    private Long personNationalId;

    @Column(name = "PERSON_IDENTITY_NUMBER")
    private Long personIdentityNumber;

    @Column(name = "PERSON_FATHER_NAME")
    private String personFatherName;

    @Column(name = "PERSON_BIRTH_DATE")
    private LocalDate personBirthDate;

    @Column(name = "PERSON_DEGREE")
    private String personDegree;

    @Column(name = "PERSON_UX_LEVEL")
    private Long personUxLevel;

    @Column(name = "PERSON_IMAGE")
    private Blob personImage;

    @Column(name = "PERSON_TITLE")
    private String personTitle;

    @Column(name = "PERSON_NAME")
    private String personName;

    @Column(name = "PERSON_LAST_NAME")
    private String personLastName;

    @Column(name = "PERSON_FK_PV_GENDER_TITLE")
    private String personPvGenderTitle;

    @Column(name = "PERSON_FK_PV_GENDER_ID")
    private Long personPvGenderId;

    @Column(name = "PERSON_FK_ORG_UNIT_ID")
    private Long personOrganizationUnitId;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public Long getRequestId() {
        return requestId;
    }

    @Override
    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getRequestSubject() {
        return requestSubject;
    }

    public void setRequestSubject(String requestSubject) {
        this.requestSubject = requestSubject;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public Long getRequestOrgUnitIdFrom() {
        return requestOrgUnitIdFrom;
    }

    public void setRequestOrgUnitIdFrom(Long requestOrgUnitIdFrom) {
        this.requestOrgUnitIdFrom = requestOrgUnitIdFrom;
    }

    public Long getRequestOrgUnitIdTo() {
        return requestOrgUnitIdTo;
    }

    public void setRequestOrgUnitIdTo(Long requestOrgUnitIdTo) {
        this.requestOrgUnitIdTo = requestOrgUnitIdTo;
    }

    public String getRequestPvReasonTitle() {
        return requestPvReasonTitle;
    }

    public void setRequestPvReasonTitle(String requestPvReasonTitle) {
        this.requestPvReasonTitle = requestPvReasonTitle;
    }

    public Long getRequestPvReasonId() {
        return requestPvReasonId;
    }

    public void setRequestPvReasonId(Long requestPvReasonId) {
        this.requestPvReasonId = requestPvReasonId;
    }

    public String getRequestPvRejectReasonTitle() {
        return requestPvRejectReasonTitle;
    }

    public void setRequestPvRejectReasonTitle(String requestPvRejectReasonTitle) {
        this.requestPvRejectReasonTitle = requestPvRejectReasonTitle;
    }

    public Long getRequestPvRejectReasonId() {
        return requestPvRejectReasonId;
    }

    public void setRequestPvRejectReasonId(Long requestPvRejectReasonId) {
        this.requestPvRejectReasonId = requestPvRejectReasonId;
    }

    public String getRequestPvStateTitle() {
        return requestPvStateTitle;
    }

    public void setRequestPvStateTitle(String requestPvStateTitle) {
        this.requestPvStateTitle = requestPvStateTitle;
    }

    public Long getRequestPvStateId() {
        return requestPvStateId;
    }

    public void setRequestPvStateId(Long requestPvStateId) {
        this.requestPvStateId = requestPvStateId;
    }

    public String getRequestPvRequesterTitle() {
        return requestPvRequesterTitle;
    }

    public void setRequestPvRequesterTitle(String requestPvRequesterTitle) {
        this.requestPvRequesterTitle = requestPvRequesterTitle;
    }

    public Long getRequestPvRequesterId() {
        return requestPvRequesterId;
    }

    public void setRequestPvRequesterId(Long requestPvRequesterId) {
        this.requestPvRequesterId = requestPvRequesterId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getPersonNationalId() {
        return personNationalId;
    }

    public void setPersonNationalId(Long personNationalId) {
        this.personNationalId = personNationalId;
    }

    public Long getPersonIdentityNumber() {
        return personIdentityNumber;
    }

    public void setPersonIdentityNumber(Long personIdentityNumber) {
        this.personIdentityNumber = personIdentityNumber;
    }

    public String getPersonFatherName() {
        return personFatherName;
    }

    public void setPersonFatherName(String personFatherName) {
        this.personFatherName = personFatherName;
    }

    public LocalDate getPersonBirthDate() {
        return personBirthDate;
    }

    public void setPersonBirthDate(LocalDate personBirthDate) {
        this.personBirthDate = personBirthDate;
    }

    public String getPersonDegree() {
        return personDegree;
    }

    public void setPersonDegree(String personDegree) {
        this.personDegree = personDegree;
    }

    public Long getPersonUxLevel() {
        return personUxLevel;
    }

    public void setPersonUxLevel(Long personUxLevel) {
        this.personUxLevel = personUxLevel;
    }

    public Blob getPersonImage() {
        return personImage;
    }

    public void setPersonImage(Blob personImage) {
        this.personImage = personImage;
    }

    public String getPersonTitle() {
        return personTitle;
    }

    public void setPersonTitle(String personTitle) {
        this.personTitle = personTitle;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getPersonPvGenderTitle() {
        return personPvGenderTitle;
    }

    public void setPersonPvGenderTitle(String personPvGenderTitle) {
        this.personPvGenderTitle = personPvGenderTitle;
    }

    public Long getPersonPvGenderId() {
        return personPvGenderId;
    }

    public void setPersonPvGenderId(Long personPvGenderId) {
        this.personPvGenderId = personPvGenderId;
    }

    public Long getPersonOrganizationUnitId() {
        return personOrganizationUnitId;
    }

    public void setPersonOrganizationUnitId(Long personOrganizationUnitId) {
        this.personOrganizationUnitId = personOrganizationUnitId;
    }
}
