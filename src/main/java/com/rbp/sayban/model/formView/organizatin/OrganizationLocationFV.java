/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.Address;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ORG_LOCATION_VIEW")
public class OrganizationLocationFV extends BaseEntity {

    @Column(name = "FK_ORG_ID")
    private Long organizationId;

    @Column(name = "FK_ADDRESSES_ID")
    private Long addressid;

    @Column(name = "ADDRESSES_ADDRESS_TEXT")
    private String addressAddressText;

    @Column(name = "ADDRESSES_POSTAL_CODE")
    private String addressPostalCode;

    @Column(name = "ADDRESSES_STREET")
    private String addressStreet;

    @Column(name = "ADDRESSES_FK_PV_TYPE_ID")
    private Long addressPvTypeId;

    @Column(name = "ADDRESSES_PV_TYPE_TITLE")
    private String addressPvTypeTitle;

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getAddressid() {
        return addressid;
    }

    public void setAddressid(Long addressid) {
        this.addressid = addressid;
    }

    public String getAddressAddressText() {
        return addressAddressText;
    }

    public void setAddressAddressText(String addressAddressText) {
        this.addressAddressText = addressAddressText;
    }

    public String getAddressPostalCode() {
        return addressPostalCode;
    }

    public void setAddressPostalCode(String addressPostalCode) {
        this.addressPostalCode = addressPostalCode;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public Long getAddressPvTypeId() {
        return addressPvTypeId;
    }

    public void setAddressPvTypeId(Long addressPvTypeId) {
        this.addressPvTypeId = addressPvTypeId;
    }

    public String getAddressPvTypeTitle() {
        return addressPvTypeTitle;
    }

    public void setAddressPvTypeTitle(String addressPvTypeTitle) {
        this.addressPvTypeTitle = addressPvTypeTitle;
    }
}
