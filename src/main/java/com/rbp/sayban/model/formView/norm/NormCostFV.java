/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.norm;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "NORM_COST_VIEW")
public class NormCostFV extends BaseEntity {

    @Column(name ="NAME" )
    private String name;

    @Column (name="TITLE")
    private String title;
    
    @Column(name="COST")
    private Long cost;

    @Column(name="RATE")
    private Long rate;

    @Column (name="PRIORITORY")
    private Integer prioritory;

    @Column(name = "FK_NORM_PARENT_ID")
    private Long parentId;

    @Column(name ="PARENT_NAME" )
    private String parentName;

    @Column (name="PARENT_TITLE")
    private String parentTitle;

    @Column(name="PARENT_COST")
    private Long parentCost;

    @Column(name="PARENT_RATE")
    private Long parentRate;

    @Column (name="PARENT_PRIORITORY")
    private Integer parentPrioritory;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public Integer getPrioritory() {
        return prioritory;
    }

    public void setPrioritory(Integer prioritory) {
        this.prioritory = prioritory;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public Long getParentCost() {
        return parentCost;
    }

    public void setParentCost(Long parentCost) {
        this.parentCost = parentCost;
    }

    public Long getParentRate() {
        return parentRate;
    }

    public void setParentRate(Long parentRate) {
        this.parentRate = parentRate;
    }

    public Integer getParentPrioritory() {
        return parentPrioritory;
    }

    public void setParentPrioritory(Integer parentPrioritory) {
        this.parentPrioritory = parentPrioritory;
    }
}
