/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.sayban.model.domainmodel.report.FilterOperator;
import com.rbp.sayban.model.domainmodel.report.ReportFilter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "REPORT_FILTER_GROUP_VIEW")
public class ReportFilterGroupFV extends BaseEntity {

    // TODO: 07/12/2019

//    @Column(name = "FK_REPORT_FILTER_ID")
//    private Long reportFilterId;
//
//    @Column(name = "REPORT_FILTER_NAME")
//    private String reportFilterName;
//
//    @Column(name = "REPORT_FILTER_QUERY")
//    private String reportFilterQuery;
//
//    @Column(name = "REPORT_FILTER_NUMBER$")
//    private String reportFilterNumber;
//
//    @Column(name = "REPORT_FILTER_DEFAULT_VALUE")
//    private String reportFilterDefualtValue;
//
//    @Column(name = "FK_GROUP_ID")
//    private Long groupId;
//
//    @Column(name = "GROUP_NAME")
//    private String groupName;
//
//    @Column(name = "GROUP_PERSIAN_NAME")
//    private String groupPersianName;
//
//    public Long getReportFilterId() {
//        return reportFilterId;
//    }
//
//    public void setReportFilterId(Long reportFilterId) {
//        this.reportFilterId = reportFilterId;
//    }
//
//    public String getReportFilterName() {
//        return reportFilterName;
//    }
//
//    public void setReportFilterName(String reportFilterName) {
//        this.reportFilterName = reportFilterName;
//    }
//
//    public String getReportFilterQuery() {
//        return reportFilterQuery;
//    }
//
//    public void setReportFilterQuery(String reportFilterQuery) {
//        this.reportFilterQuery = reportFilterQuery;
//    }
//
//    public String getReportFilterNumber() {
//        return reportFilterNumber;
//    }
//
//    public void setReportFilterNumber(String reportFilterNumber) {
//        this.reportFilterNumber = reportFilterNumber;
//    }
//
//    public String getReportFilterDefualtValue() {
//        return reportFilterDefualtValue;
//    }
//
//    public void setReportFilterDefualtValue(String reportFilterDefualtValue) {
//        this.reportFilterDefualtValue = reportFilterDefualtValue;
//    }
//
//    public Long getGroupId() {
//        return groupId;
//    }
//
//    public void setGroupId(Long groupId) {
//        this.groupId = groupId;
//    }
//
//    public String getGroupName() {
//        return groupName;
//    }
//
//    public void setGroupName(String groupName) {
//        this.groupName = groupName;
//    }
//
//    public String getGroupPersianName() {
//        return groupPersianName;
//    }
//
//    public void setGroupPersianName(String groupPersianName) {
//        this.groupPersianName = groupPersianName;
//    }
}
