package com.rbp.sayban.model.formView.strategy;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.strategies.Strategy;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Immutable
@Table(name = "STRATEGY_BUDGET_VIEW")
public class StrategyBudgetFV extends EvalStateBaseEntity {

    @Column(name = "ESTIMATE_BUDGET")
    private Long estimateBudget;

    @Column(name = "ACTUAL_BUDGET")
    private Long actualBudget;

    @Column(name = "FK_STRATEGY_ID")
    private Long strategyId;

    @Column(name = "FK_PV_TYPE_ID")
    private Long strategyPvTypeId;

    @Column(name = "FK_PV_TYPE_TITLE")
    private String strategyPvTypeTitle;

    @Column(name = "STRATEGY_NAME")
    private String strategyName;

    @Column(name = "STRATEGY_TITLE")
    private String strategyTitle;

    @Column(name = "STRATEGY_IS_ROOT")
    private Boolean strategyIsRoot;

    @Column(name = "STRATEGY_INITIAL_PERCENTAGE")
    private Double strategyInitialPercentage;

    @Column(name = "STRATEGY_PERCENTAGE")
    private Double strategyPercentage;

    @Column(name = "STRATEGY_WEIGHT")
    private Long strategyWeight;

    @Column(name = "STRATEGY_VOLUME")
    private Long strategyVolume;

    @Column(name = "STRATEGY_PRIORITY")
    private Integer strategyPriority;

    @Column(name = "STRATEGY_IMPORTANCE")
    private Double strategyImportance;

    @Column(name = "STRATEGY_ACTUAL_START_DATE")
    private LocalDate strategyActualStartDate;

    @Column(name = "STRATEGY_ACTUAL_END_DATE")
    private LocalDate strategyActualEndDate;

    @Column(name = "STRATEGY_EXPIRATION_DATE")
    private LocalDate strategyExpirationDate;

    @Column(name = "STRATEGY_START_DATE")
    private LocalDate strategyStartDate;

    @Column(name = "STRATEGY_END_DATE")
    private LocalDate strategyEndDate;

    @Column(name = "STRATEGY_FISCAL_YEAR")
    private String strategyFiscalYear;

    @Column(name = "STRATEGY_LEVEL$")
    private Long strategyLevel;

    @Column(name = "FK_BUDGET_ID")
    private Long budgetId;

    @Column(name = "BUDGET_NAME")
    private String budgetName;

    @Column(name = "BUDGET_TITLE")
    private String budgetTitle;

    @Column(name = "BUDGET_IS_ROOT")
    private Boolean budgetIsRoot;

    @Column(name = "BUDGET_INITIAL_PERCENTAGE")
    private Double budgetInitialPercentage;

    @Column(name = "BUDGET_PERCENTAGE")
    private Double budgetPercentage;

    @Column(name = "BUDGET_WEIGHT")
    private Long budgetWeight;

    @Column(name = "BUDGET_VOLUME")
    private Long budgetVolume;

    @Column(name = "BUDGET_PRIORITY")
    private Integer budgetPriority;

    @Column(name = "BUDGET_IMPORTANCE")
    private Double budgetImportance;

    @Column(name = "BUDGET_ACTUAL_START_DATE")
    private LocalDate budgetActualStartDate;

    @Column(name = "BUDGET_ACTUAL_END_DATE")
    private LocalDate budgetActualEndDate;

    @Column(name = "BUDGET_BUDGET_EXPIRATION_DATE")
    private LocalDate budgetExpirationDate;

    @Column(name = "BUDGET_START_DATE")
    private LocalDate budgetStartDate;

    @Column(name = "BUDGET_END_DATE")
    private LocalDate budgetEndDate;

    @Column(name = "BUDGET_FISCAL_YEAR")
    private String budgetFiscalYear;

    @Column(name = "BUDGET_LEVEL$")
    private Long budgetLevel;

    public Long getEstimateBudget() {
        return estimateBudget;
    }

    public void setEstimateBudget(Long estimateBudget) {
        this.estimateBudget = estimateBudget;
    }

    public Long getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Long actualBudget) {
        this.actualBudget = actualBudget;
    }

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public Long getStrategyPvTypeId() {
        return strategyPvTypeId;
    }

    public void setStrategyPvTypeId(Long strategyPvTypeId) {
        this.strategyPvTypeId = strategyPvTypeId;
    }

    public String getStrategyPvTypeTitle() {
        return strategyPvTypeTitle;
    }

    public void setStrategyPvTypeTitle(String strategyPvTypeTitle) {
        this.strategyPvTypeTitle = strategyPvTypeTitle;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public String getStrategyTitle() {
        return strategyTitle;
    }

    public void setStrategyTitle(String strategyTitle) {
        this.strategyTitle = strategyTitle;
    }

    public Boolean getStrategyIsRoot() {
        return strategyIsRoot;
    }

    public void setStrategyIsRoot(Boolean strategyIsRoot) {
        this.strategyIsRoot = strategyIsRoot;
    }

    public Double getStrategyInitialPercentage() {
        return strategyInitialPercentage;
    }

    public void setStrategyInitialPercentage(Double strategyInitialPercentage) {
        this.strategyInitialPercentage = strategyInitialPercentage;
    }

    public Double getStrategyPercentage() {
        return strategyPercentage;
    }

    public void setStrategyPercentage(Double strategyPercentage) {
        this.strategyPercentage = strategyPercentage;
    }

    public Long getStrategyWeight() {
        return strategyWeight;
    }

    public void setStrategyWeight(Long strategyWeight) {
        this.strategyWeight = strategyWeight;
    }

    public Long getStrategyVolume() {
        return strategyVolume;
    }

    public void setStrategyVolume(Long strategyVolume) {
        this.strategyVolume = strategyVolume;
    }

    public Integer getStrategyPriority() {
        return strategyPriority;
    }

    public void setStrategyPriority(Integer strategyPriority) {
        this.strategyPriority = strategyPriority;
    }

    public Double getStrategyImportance() {
        return strategyImportance;
    }

    public void setStrategyImportance(Double strategyImportance) {
        this.strategyImportance = strategyImportance;
    }

    public LocalDate getStrategyActualStartDate() {
        return strategyActualStartDate;
    }

    public void setStrategyActualStartDate(LocalDate strategyActualStartDate) {
        this.strategyActualStartDate = strategyActualStartDate;
    }

    public LocalDate getStrategyActualEndDate() {
        return strategyActualEndDate;
    }

    public void setStrategyActualEndDate(LocalDate strategyActualEndDate) {
        this.strategyActualEndDate = strategyActualEndDate;
    }

    public LocalDate getStrategyExpirationDate() {
        return strategyExpirationDate;
    }

    public void setStrategyExpirationDate(LocalDate strategyExpirationDate) {
        this.strategyExpirationDate = strategyExpirationDate;
    }

    public LocalDate getStrategyStartDate() {
        return strategyStartDate;
    }

    public void setStrategyStartDate(LocalDate strategyStartDate) {
        this.strategyStartDate = strategyStartDate;
    }

    public LocalDate getStrategyEndDate() {
        return strategyEndDate;
    }

    public void setStrategyEndDate(LocalDate strategyEndDate) {
        this.strategyEndDate = strategyEndDate;
    }

    public String getStrategyFiscalYear() {
        return strategyFiscalYear;
    }

    public void setStrategyFiscalYear(String strategyFiscalYear) {
        this.strategyFiscalYear = strategyFiscalYear;
    }

    public Long getStrategyLevel() {
        return strategyLevel;
    }

    public void setStrategyLevel(Long strategyLevel) {
        this.strategyLevel = strategyLevel;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgetTitle() {
        return budgetTitle;
    }

    public void setBudgetTitle(String budgetTitle) {
        this.budgetTitle = budgetTitle;
    }

    public Boolean getBudgetIsRoot() {
        return budgetIsRoot;
    }

    public void setBudgetIsRoot(Boolean budgetIsRoot) {
        this.budgetIsRoot = budgetIsRoot;
    }

    public Double getBudgetInitialPercentage() {
        return budgetInitialPercentage;
    }

    public void setBudgetInitialPercentage(Double budgetInitialPercentage) {
        this.budgetInitialPercentage = budgetInitialPercentage;
    }

    public Double getBudgetPercentage() {
        return budgetPercentage;
    }

    public void setBudgetPercentage(Double budgetPercentage) {
        this.budgetPercentage = budgetPercentage;
    }

    public Long getBudgetWeight() {
        return budgetWeight;
    }

    public void setBudgetWeight(Long budgetWeight) {
        this.budgetWeight = budgetWeight;
    }

    public Long getBudgetVolume() {
        return budgetVolume;
    }

    public void setBudgetVolume(Long budgetVolume) {
        this.budgetVolume = budgetVolume;
    }

    public Integer getBudgetPriority() {
        return budgetPriority;
    }

    public void setBudgetPriority(Integer budgetPriority) {
        this.budgetPriority = budgetPriority;
    }

    public Double getBudgetImportance() {
        return budgetImportance;
    }

    public void setBudgetImportance(Double budgetImportance) {
        this.budgetImportance = budgetImportance;
    }

    public LocalDate getBudgetActualStartDate() {
        return budgetActualStartDate;
    }

    public void setBudgetActualStartDate(LocalDate budgetActualStartDate) {
        this.budgetActualStartDate = budgetActualStartDate;
    }

    public LocalDate getBudgetActualEndDate() {
        return budgetActualEndDate;
    }

    public void setBudgetActualEndDate(LocalDate budgetActualEndDate) {
        this.budgetActualEndDate = budgetActualEndDate;
    }

    public LocalDate getBudgetExpirationDate() {
        return budgetExpirationDate;
    }

    public void setBudgetExpirationDate(LocalDate budgetExpirationDate) {
        this.budgetExpirationDate = budgetExpirationDate;
    }

    public LocalDate getBudgetStartDate() {
        return budgetStartDate;
    }

    public void setBudgetStartDate(LocalDate budgetStartDate) {
        this.budgetStartDate = budgetStartDate;
    }

    public LocalDate getBudgetEndDate() {
        return budgetEndDate;
    }

    public void setBudgetEndDate(LocalDate budgetEndDate) {
        this.budgetEndDate = budgetEndDate;
    }

    public String getBudgetFiscalYear() {
        return budgetFiscalYear;
    }

    public void setBudgetFiscalYear(String budgetFiscalYear) {
        this.budgetFiscalYear = budgetFiscalYear;
    }

    public Long getBudgetLevel() {
        return budgetLevel;
    }

    public void setBudgetLevel(Long budgetLevel) {
        this.budgetLevel = budgetLevel;
    }
}
