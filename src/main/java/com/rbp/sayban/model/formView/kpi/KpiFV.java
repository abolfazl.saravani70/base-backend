/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.kpi;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.sayban.model.domainmodel.kpi.Unit;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "KPI_VIEW")
public class KpiFV extends  EvalStateBaseEntity {

    @Column(name = "KEY")
    private Integer key;

    @Column(name = "CODE")
    private String code;

    @Column(name = "VALUE")
    private Long value;

    @Column(name = "BEST_VALUE")
    private Long bestValue;

    @Column(name = "KPI_PRIORITY")
    private Integer kpiPriority;

    @Column(name = "COST")
    private Integer cost;

    @Column(name = "MINIMUM")
    private Long minimum;

    @Column(name = "MAXIMUM")
    private Long maximum;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "FK_PV_KPI_TYPE_ID")
    private Long pvKpiTypeId;

    @Column(name = "PV_KPI_TYPE_TITLE")
    private String pvKpiTypeTitle;

    @Column(name = "FK_PV_MESURE_UNIT_ID")
    private Long pvMesureUnitId;

    @Column(name = "PV_MESURE_UNIT_TITLE")
    private String pvMesureUnitTitle;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_KEY")
    private Integer parentKey;

    @Column(name = "PARENT_CODE")
    private String parentCode;

    @Column(name = "PARENT_VALUE")
    private Long parentValue;

    @Column(name = "PARENT_BEST_VALUE")
    private Long parentBestValue;

    @Column(name = "PARENT_KPI_PRIORITY")
    private Integer parentKpiPriority;

    @Column(name = "PARENT_COST")
    private Integer parentCost;

    @Column(name = "PARENT_MINIMUM")
    private Long parentMinimum;

    @Column(name = "PARENT_MAXIMUM")
    private Long parentMaximum;

    @Column(name = "PARENT_NORMAL")
    private Long parentNormal;

    @Column(name = "PARENT_FK_PV_KPI_TYPE_ID")
    private Long parentPvKpiTypeId;

    @Column(name = "PARENT_PV_KPI_TYPE_TITLE")
    private String parentPvKpiTypeTitle;

    @Column(name = "PARENT_FK_PV_MESURE_UNIT_ID")
    private Long parentPvMesureUnitId;

    @Column(name = "PARENT_PV_MESURE_UNIT_TITLE")
    private String parentPvMesureUnitTitle;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TITLE")
    private String parentTitle;

    @Column(name = "PARENT_IS_ROOT")
    private Boolean parentIsRoot;

    @Column(name = "PARENT_INITIAL_PERCENTAGE")
    private Double parentInitialPercentage;

    @Column(name = "PARENT_PERCENTAGE")
    private Double parentPercentage;

    @Column(name = "PARENT_WEIGHT")
    private Long parentWeight;

    @Column(name = "PARENT_VOLUME")
    private Long parentVolume;

    @Column(name = "PARENT_PRIORITY")
    private Integer parentPriority;

    @Column(name = "PARENT_IMPORTANCE")
    private Double parentImportance;

    @Column(name = "PARENT_ACTUAL_START_DATE")
    private LocalDate parentActualStartDate;

    @Column(name = "PARENT_ACTUAL_END_DATE")
    private LocalDate parentActualEndDate;

    @Column(name = "PARENT_EXPIRATION_DATE")
    private LocalDate parentExpirationDate;

    @Column(name = "PARENT_START_DATE")
    private LocalDate parentStartDate;

    @Column(name = "PARENT_END_DATE")
    private LocalDate parentEndDate;

    @Column(name = "PARENT_FISCAL_YEAR")
    private String parentFiscalYear;

    @Column(name = "PARENT_LEVEL$")
    private Long parentLevel;

    @Column(name = "FK_UNIT_ID")
    private Long unitId;

    @Column(name = "UNIT_KEY_NAME")
    private String unitkeyName;

    @Column(name = "UNIT_KEY_VALUE")
    private String unitkeyValue;

    @Column(name = "UNIT_RATE")
    private Integer unitRate;

    @Column(name = "UNIT_PERCENT")
    private Double unitPercent;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getBestValue() {
        return bestValue;
    }

    public void setBestValue(Long bestValue) {
        this.bestValue = bestValue;
    }

    public Integer getKpiPriority() {
        return kpiPriority;
    }

    public void setKpiPriority(Integer kpiPriority) {
        this.kpiPriority = kpiPriority;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Long getMinimum() {
        return minimum;
    }

    public void setMinimum(Long minimum) {
        this.minimum = minimum;
    }

    public Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        this.maximum = maximum;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Long getPvKpiTypeId() {
        return pvKpiTypeId;
    }

    public void setPvKpiTypeId(Long pvKpiTypeId) {
        this.pvKpiTypeId = pvKpiTypeId;
    }

    public String getPvKpiTypeTitle() {
        return pvKpiTypeTitle;
    }

    public void setPvKpiTypeTitle(String pvKpiTypeTitle) {
        this.pvKpiTypeTitle = pvKpiTypeTitle;
    }

    public Long getPvMesureUnitId() {
        return pvMesureUnitId;
    }

    public void setPvMesureUnitId(Long pvMesureUnitId) {
        this.pvMesureUnitId = pvMesureUnitId;
    }

    public String getPvMesureUnitTitle() {
        return pvMesureUnitTitle;
    }

    public void setPvMesureUnitTitle(String pvMesureUnitTitle) {
        this.pvMesureUnitTitle = pvMesureUnitTitle;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getParentKey() {
        return parentKey;
    }

    public void setParentKey(Integer parentKey) {
        this.parentKey = parentKey;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public Long getParentValue() {
        return parentValue;
    }

    public void setParentValue(Long parentValue) {
        this.parentValue = parentValue;
    }

    public Long getParentBestValue() {
        return parentBestValue;
    }

    public void setParentBestValue(Long parentBestValue) {
        this.parentBestValue = parentBestValue;
    }

    public Integer getParentKpiPriority() {
        return parentKpiPriority;
    }

    public void setParentKpiPriority(Integer parentKpiPriority) {
        this.parentKpiPriority = parentKpiPriority;
    }

    public Integer getParentCost() {
        return parentCost;
    }

    public void setParentCost(Integer parentCost) {
        this.parentCost = parentCost;
    }

    public Long getParentMinimum() {
        return parentMinimum;
    }

    public void setParentMinimum(Long parentMinimum) {
        this.parentMinimum = parentMinimum;
    }

    public Long getParentMaximum() {
        return parentMaximum;
    }

    public void setParentMaximum(Long parentMaximum) {
        this.parentMaximum = parentMaximum;
    }

    public Long getParentNormal() {
        return parentNormal;
    }

    public void setParentNormal(Long parentNormal) {
        this.parentNormal = parentNormal;
    }

    public Long getParentPvKpiTypeId() {
        return parentPvKpiTypeId;
    }

    public void setParentPvKpiTypeId(Long parentPvKpiTypeId) {
        this.parentPvKpiTypeId = parentPvKpiTypeId;
    }

    public String getParentPvKpiTypeTitle() {
        return parentPvKpiTypeTitle;
    }

    public void setParentPvKpiTypeTitle(String parentPvKpiTypeTitle) {
        this.parentPvKpiTypeTitle = parentPvKpiTypeTitle;
    }

    public Long getParentPvMesureUnitId() {
        return parentPvMesureUnitId;
    }

    public void setParentPvMesureUnitId(Long parentPvMesureUnitId) {
        this.parentPvMesureUnitId = parentPvMesureUnitId;
    }

    public String getParentPvMesureUnitTitle() {
        return parentPvMesureUnitTitle;
    }

    public void setParentPvMesureUnitTitle(String parentPvMesureUnitTitle) {
        this.parentPvMesureUnitTitle = parentPvMesureUnitTitle;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public Boolean getParentIsRoot() {
        return parentIsRoot;
    }

    public void setParentIsRoot(Boolean parentIsRoot) {
        this.parentIsRoot = parentIsRoot;
    }

    public Double getParentInitialPercentage() {
        return parentInitialPercentage;
    }

    public void setParentInitialPercentage(Double parentInitialPercentage) {
        this.parentInitialPercentage = parentInitialPercentage;
    }

    public Double getParentPercentage() {
        return parentPercentage;
    }

    public void setParentPercentage(Double parentPercentage) {
        this.parentPercentage = parentPercentage;
    }

    public Long getParentWeight() {
        return parentWeight;
    }

    public void setParentWeight(Long parentWeight) {
        this.parentWeight = parentWeight;
    }

    public Long getParentVolume() {
        return parentVolume;
    }

    public void setParentVolume(Long parentVolume) {
        this.parentVolume = parentVolume;
    }

    public Integer getParentPriority() {
        return parentPriority;
    }

    public void setParentPriority(Integer parentPriority) {
        this.parentPriority = parentPriority;
    }

    public Double getParentImportance() {
        return parentImportance;
    }

    public void setParentImportance(Double parentImportance) {
        this.parentImportance = parentImportance;
    }

    public LocalDate getParentActualStartDate() {
        return parentActualStartDate;
    }

    public void setParentActualStartDate(LocalDate parentActualStartDate) {
        this.parentActualStartDate = parentActualStartDate;
    }

    public LocalDate getParentActualEndDate() {
        return parentActualEndDate;
    }

    public void setParentActualEndDate(LocalDate parentActualEndDate) {
        this.parentActualEndDate = parentActualEndDate;
    }

    public LocalDate getParentExpirationDate() {
        return parentExpirationDate;
    }

    public void setParentExpirationDate(LocalDate parentExpirationDate) {
        this.parentExpirationDate = parentExpirationDate;
    }

    public LocalDate getParentStartDate() {
        return parentStartDate;
    }

    public void setParentStartDate(LocalDate parentStartDate) {
        this.parentStartDate = parentStartDate;
    }

    public LocalDate getParentEndDate() {
        return parentEndDate;
    }

    public void setParentEndDate(LocalDate parentEndDate) {
        this.parentEndDate = parentEndDate;
    }

    public String getParentFiscalYear() {
        return parentFiscalYear;
    }

    public void setParentFiscalYear(String parentFiscalYear) {
        this.parentFiscalYear = parentFiscalYear;
    }

    public Long getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Long parentLevel) {
        this.parentLevel = parentLevel;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getUnitkeyName() {
        return unitkeyName;
    }

    public void setUnitkeyName(String unitkeyName) {
        this.unitkeyName = unitkeyName;
    }

    public String getUnitkeyValue() {
        return unitkeyValue;
    }

    public void setUnitkeyValue(String unitkeyValue) {
        this.unitkeyValue = unitkeyValue;
    }

    public Integer getUnitRate() {
        return unitRate;
    }

    public void setUnitRate(Integer unitRate) {
        this.unitRate = unitRate;
    }

    public Double getUnitPercent() {
        return unitPercent;
    }

    public void setUnitPercent(Double unitPercent) {
        this.unitPercent = unitPercent;
    }
}
