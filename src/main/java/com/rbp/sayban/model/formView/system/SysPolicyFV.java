/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.model.domainmodel.application.EnumValue;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "SYS_POLICY_VIEW")
public class SysPolicyFV extends StereotypeBaseEntity {

    @Column(name = "FK_ENUM_VALUE_ID")
    private Long enumValueId;

    @Column(name = "ENUM_VALUE_NAME")
    private String enumValueName;

    @Column(name = "ENUM_VALUE_TITLE")
    private String enumValueTitle;

    @Column(name = "ENUM_VALUE_KEY")
    private String enumValueKey;

    @Column(name = "ENUM_VALUE_CODE")
    private String enumValueCode;

    @Column(name = "ENUM_VALUE_VALUE")
    private String enumValueValue;

    @Column(name = "ENUM_VALUE_IS_DEPRECATED")
    private Boolean enumValueVsDeprecated;

    @Column(name = "ENUM_VALUE_IS_OBSOLETE")
    private Boolean enumValueIsObsolete;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    public Long getEnumValueId() {
        return enumValueId;
    }

    public void setEnumValueId(Long enumValueId) {
        this.enumValueId = enumValueId;
    }

    public String getEnumValueName() {
        return enumValueName;
    }

    public void setEnumValueName(String enumValueName) {
        this.enumValueName = enumValueName;
    }

    public String getEnumValueTitle() {
        return enumValueTitle;
    }

    public void setEnumValueTitle(String enumValueTitle) {
        this.enumValueTitle = enumValueTitle;
    }

    public String getEnumValueKey() {
        return enumValueKey;
    }

    public void setEnumValueKey(String enumValueKey) {
        this.enumValueKey = enumValueKey;
    }

    public String getEnumValueCode() {
        return enumValueCode;
    }

    public void setEnumValueCode(String enumValueCode) {
        this.enumValueCode = enumValueCode;
    }

    public String getEnumValueValue() {
        return enumValueValue;
    }

    public void setEnumValueValue(String enumValueValue) {
        this.enumValueValue = enumValueValue;
    }

    public Boolean getEnumValueVsDeprecated() {
        return enumValueVsDeprecated;
    }

    public void setEnumValueVsDeprecated(Boolean enumValueVsDeprecated) {
        this.enumValueVsDeprecated = enumValueVsDeprecated;
    }

    public Boolean getEnumValueIsObsolete() {
        return enumValueIsObsolete;
    }

    public void setEnumValueIsObsolete(Boolean enumValueIsObsolete) {
        this.enumValueIsObsolete = enumValueIsObsolete;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }
}
