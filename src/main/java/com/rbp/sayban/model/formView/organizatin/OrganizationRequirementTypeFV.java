/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "ORG_REQUIREMENT_TYPE_VIEW")
public class OrganizationRequirementTypeFV extends StereotypeBaseEntity {

}
