/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.SubjectiveJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.domainmodel.subjective.Subjective;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "OBJECTIVE_SUBJECTIVE_VIEW")
public class ObjectiveSubjectiveFV extends EvalStateBaseEntity {

    @Column(name = "FK_OBJECTIVE_ID")
    private Long objectiveId;

    @Column(name = "OBJECTIVE_FK_PV_TYPE_ID")
    private Long objectivePvTypeId;

    @Column(name = "OBJECTIVE_PV_TYPE_TITLE")
    private String objectivePvTypeTitle;

    @Column(name = "OBJECTIVE_NAME")
    private String objectiveName;

    @Column(name = "OBJECTIVE_TITLE")
    private String objectiveTitle;

    @Column(name = "OBJECTIVE_IS_ROOT")
    private Boolean objectiveIsRoot;

    @Column(name = "OBJECTIVE_INITIAL_PERCENTAGE")
    private Double objectiveInitialPercentage;

    @Column(name = "OBJECTIVE_PERCENTAGE")
    private Double objectivePercentage;

    @Column(name = "OBJECTIVE_WEIGHT")
    private Long objectiveWeight;

    @Column(name = "OBJECTIVE_VOLUME")
    private Long objectiveVolume;

    @Column(name = "OBJECTIVE_PRIORITY")
    private Integer objectivePriority;

    @Column(name = "OBJECTIVE_IMPORTANCE")
    private Double objectiveImportance;

    @Column(name = "OBJECTIVE_ACTUAL_START_DATE")
    private LocalDate objectiveActualStartDate;

    @Column(name = "OBJECTIVE_ACTUAL_END_DATE")
    private LocalDate objectiveActualEndDate;

    @Column(name = "OBJECTIVE_EXPIRATION_DATE")
    private LocalDate objectiveExpirationDate;

    @Column(name = "OBJECTIVE_START_DATE")
    private LocalDate objectiveStartDate;

    @Column(name = "OBJECTIVE_END_DATE")
    private LocalDate objectiveEndDate;

    @Column(name = "OBJECTIVE_FISCAL_YEAR")
    private String objectiveFiscalYear;

    @Column(name = "OBJECTIVE_LEVEL$")
    private Long objectiveLevel;

    @Column(name = "FK_SUBJECTIVE_ID")
    private Long subjectiveId;

    @Column(name = "SUBJECTIVE_NAME")
    private String subjectiveName;

    @Column(name = "SUBJECTIVE_TITLE")
    private String subjectiveTitle;

    @Column(name = "SUBJECTIVE_IS_ROOT")
    private Boolean subjectiveIsRoot;

    @Column(name = "SUBJECTIVE_INITIAL_PERCENTAGE")
    private Double subjectiveInitialPercentage;

    @Column(name = "SUBJECTIVE_PERCENTAGE")
    private Double subjectivePercentage;

    @Column(name = "SUBJECTIVE_WEIGHT")
    private Long subjectiveWeight;

    @Column(name = "SUBJECTIVE_VOLUME")
    private Long subjectiveVolume;

    @Column(name = "SUBJECTIVE_PRIORITY")
    private Integer subjectivePriority;

    @Column(name = "SUBJECTIVE_IMPORTANCE")
    private Double subjectiveImportance;

    @Column(name = "SUBJECTIVE_ACTUAL_START_DATE")
    private LocalDate subjectiveActualStartDate;

    @Column(name = "SUBJECTIVE_ACTUAL_END_DATE")
    private LocalDate subjectiveActualEndDate;

    @Column(name = "SUBJECTIVE_EXPIRATION_DATE")
    private LocalDate subjectiveExpirationDate;

    @Column(name = "SUBJECTIVE_START_DATE")
    private LocalDate subjectiveStartDate;

    @Column(name = "SUBJECTIVE_END_DATE")
    private LocalDate subjectiveEndDate;

    @Column(name = "SUBJECTIVE_FISCAL_YEAR")
    private String subjectiveFiscalYear;

    @Column(name = "SUBJECTIVE_LEVEL$")
    private Long subjectiveLevel;

    public Long getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }

    public Long getObjectivePvTypeId() {
        return objectivePvTypeId;
    }

    public void setObjectivePvTypeId(Long objectivePvTypeId) {
        this.objectivePvTypeId = objectivePvTypeId;
    }

    public String getObjectivePvTypeTitle() {
        return objectivePvTypeTitle;
    }

    public void setObjectivePvTypeTitle(String objectivePvTypeTitle) {
        this.objectivePvTypeTitle = objectivePvTypeTitle;
    }

    public String getObjectiveName() {
        return objectiveName;
    }

    public void setObjectiveName(String objectiveName) {
        this.objectiveName = objectiveName;
    }

    public String getObjectiveTitle() {
        return objectiveTitle;
    }

    public void setObjectiveTitle(String objectiveTitle) {
        this.objectiveTitle = objectiveTitle;
    }

    public Boolean getObjectiveIsRoot() {
        return objectiveIsRoot;
    }

    public void setObjectiveIsRoot(Boolean objectiveIsRoot) {
        this.objectiveIsRoot = objectiveIsRoot;
    }

    public Double getObjectiveInitialPercentage() {
        return objectiveInitialPercentage;
    }

    public void setObjectiveInitialPercentage(Double objectiveInitialPercentage) {
        this.objectiveInitialPercentage = objectiveInitialPercentage;
    }

    public Double getObjectivePercentage() {
        return objectivePercentage;
    }

    public void setObjectivePercentage(Double objectivePercentage) {
        this.objectivePercentage = objectivePercentage;
    }

    public Long getObjectiveWeight() {
        return objectiveWeight;
    }

    public void setObjectiveWeight(Long objectiveWeight) {
        this.objectiveWeight = objectiveWeight;
    }

    public Long getObjectiveVolume() {
        return objectiveVolume;
    }

    public void setObjectiveVolume(Long objectiveVolume) {
        this.objectiveVolume = objectiveVolume;
    }

    public Integer getObjectivePriority() {
        return objectivePriority;
    }

    public void setObjectivePriority(Integer objectivePriority) {
        this.objectivePriority = objectivePriority;
    }

    public Double getObjectiveImportance() {
        return objectiveImportance;
    }

    public void setObjectiveImportance(Double objectiveImportance) {
        this.objectiveImportance = objectiveImportance;
    }

    public LocalDate getObjectiveActualStartDate() {
        return objectiveActualStartDate;
    }

    public void setObjectiveActualStartDate(LocalDate objectiveActualStartDate) {
        this.objectiveActualStartDate = objectiveActualStartDate;
    }

    public LocalDate getObjectiveActualEndDate() {
        return objectiveActualEndDate;
    }

    public void setObjectiveActualEndDate(LocalDate objectiveActualEndDate) {
        this.objectiveActualEndDate = objectiveActualEndDate;
    }

    public LocalDate getObjectiveExpirationDate() {
        return objectiveExpirationDate;
    }

    public void setObjectiveExpirationDate(LocalDate objectiveExpirationDate) {
        this.objectiveExpirationDate = objectiveExpirationDate;
    }

    public LocalDate getObjectiveStartDate() {
        return objectiveStartDate;
    }

    public void setObjectiveStartDate(LocalDate objectiveStartDate) {
        this.objectiveStartDate = objectiveStartDate;
    }

    public LocalDate getObjectiveEndDate() {
        return objectiveEndDate;
    }

    public void setObjectiveEndDate(LocalDate objectiveEndDate) {
        this.objectiveEndDate = objectiveEndDate;
    }

    public String getObjectiveFiscalYear() {
        return objectiveFiscalYear;
    }

    public void setObjectiveFiscalYear(String objectiveFiscalYear) {
        this.objectiveFiscalYear = objectiveFiscalYear;
    }

    public Long getObjectiveLevel() {
        return objectiveLevel;
    }

    public void setObjectiveLevel(Long objectiveLevel) {
        this.objectiveLevel = objectiveLevel;
    }

    public Long getSubjectiveId() {
        return subjectiveId;
    }

    public void setSubjectiveId(Long subjectiveId) {
        this.subjectiveId = subjectiveId;
    }

    public String getSubjectiveName() {
        return subjectiveName;
    }

    public void setSubjectiveName(String subjectiveName) {
        this.subjectiveName = subjectiveName;
    }

    public String getSubjectiveTitle() {
        return subjectiveTitle;
    }

    public void setSubjectiveTitle(String subjectiveTitle) {
        this.subjectiveTitle = subjectiveTitle;
    }

    public Boolean getSubjectiveIsRoot() {
        return subjectiveIsRoot;
    }

    public void setSubjectiveIsRoot(Boolean subjectiveIsRoot) {
        this.subjectiveIsRoot = subjectiveIsRoot;
    }

    public Double getSubjectiveInitialPercentage() {
        return subjectiveInitialPercentage;
    }

    public void setSubjectiveInitialPercentage(Double subjectiveInitialPercentage) {
        this.subjectiveInitialPercentage = subjectiveInitialPercentage;
    }

    public Double getSubjectivePercentage() {
        return subjectivePercentage;
    }

    public void setSubjectivePercentage(Double subjectivePercentage) {
        this.subjectivePercentage = subjectivePercentage;
    }

    public Long getSubjectiveWeight() {
        return subjectiveWeight;
    }

    public void setSubjectiveWeight(Long subjectiveWeight) {
        this.subjectiveWeight = subjectiveWeight;
    }

    public Long getSubjectiveVolume() {
        return subjectiveVolume;
    }

    public void setSubjectiveVolume(Long subjectiveVolume) {
        this.subjectiveVolume = subjectiveVolume;
    }

    public Integer getSubjectivePriority() {
        return subjectivePriority;
    }

    public void setSubjectivePriority(Integer subjectivePriority) {
        this.subjectivePriority = subjectivePriority;
    }

    public Double getSubjectiveImportance() {
        return subjectiveImportance;
    }

    public void setSubjectiveImportance(Double subjectiveImportance) {
        this.subjectiveImportance = subjectiveImportance;
    }

    public LocalDate getSubjectiveActualStartDate() {
        return subjectiveActualStartDate;
    }

    public void setSubjectiveActualStartDate(LocalDate subjectiveActualStartDate) {
        this.subjectiveActualStartDate = subjectiveActualStartDate;
    }

    public LocalDate getSubjectiveActualEndDate() {
        return subjectiveActualEndDate;
    }

    public void setSubjectiveActualEndDate(LocalDate subjectiveActualEndDate) {
        this.subjectiveActualEndDate = subjectiveActualEndDate;
    }

    public LocalDate getSubjectiveExpirationDate() {
        return subjectiveExpirationDate;
    }

    public void setSubjectiveExpirationDate(LocalDate subjectiveExpirationDate) {
        this.subjectiveExpirationDate = subjectiveExpirationDate;
    }

    public LocalDate getSubjectiveStartDate() {
        return subjectiveStartDate;
    }

    public void setSubjectiveStartDate(LocalDate subjectiveStartDate) {
        this.subjectiveStartDate = subjectiveStartDate;
    }

    public LocalDate getSubjectiveEndDate() {
        return subjectiveEndDate;
    }

    public void setSubjectiveEndDate(LocalDate subjectiveEndDate) {
        this.subjectiveEndDate = subjectiveEndDate;
    }

    public String getSubjectiveFiscalYear() {
        return subjectiveFiscalYear;
    }

    public void setSubjectiveFiscalYear(String subjectiveFiscalYear) {
        this.subjectiveFiscalYear = subjectiveFiscalYear;
    }

    public Long getSubjectiveLevel() {
        return subjectiveLevel;
    }

    public void setSubjectiveLevel(Long subjectiveLevel) {
        this.subjectiveLevel = subjectiveLevel;
    }
}
