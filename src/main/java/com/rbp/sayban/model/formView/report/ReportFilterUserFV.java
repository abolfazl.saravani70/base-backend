/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "REPORT_FILTER-USER-VIEW")
public class ReportFilterUserFV extends BaseEntity {
    // TODO: 07/12/2019

//    @Column(name = "FK_REPORT_FILTER_ID")
//    private Long reportFilterId;
//
//    @Column(name = "REPORT_FILTER_NAME")
//    private String reportFilterName;
//
//    @Column(name = "REPORT_FILTER_QUERY")
//    private String reportFilterQuery;
//
//    @Column(name = "REPORT_FILTER_NUMBER$")
//    private String reportFilterNumber;
//
//    @Column(name = "REPORT_FILTER_DEFAULT_VALUE")
//    private String reportFilterDefualtValue;
//
//    @Column(name = "FK_USER_ID")
//    private Long userId;
//
//    @Column(name = "USER_NAME")
//    private String userUsername;
//
//    @Column(name = "USER_FIRST_NAME")
//    private String userFirstName;
//
//    @Column(name = "USER_LAST_NAME")
//    private String userLastName;
//
//    @Column(name = "USER_EMAIL")
//    private String userEmail;
//
//    @Column(name = "USER_PASSWORD")
//    private String userPassword;
//
//    @Column(name = "USER_OBSOLETE")
//    private Boolean userIsObsolete;
//
//    @Column(name = "USER_LAST_LOGIN")
//    private String userLastLogin;
//
//    @Column(name = "USER_IS_ONLINE")
//    private Boolean userIsOnline;
//
//    @Column(name = "USER_EXPIRE_DATE")
//    private String userExpireDate;
//
//    @Column(name = "USER_LAST_IP")
//    private String userLastIp;
//
//    @Column(name = "USER_IS_ACTIVE")
//    private Boolean userIsActive;
//
//    @Column(name = "USER_TELL")
//    private String userTell;
//
//    @Column(name = "USER_MOBILE")
//    private String userMobile;
//
//    @Column
//    private String userKowsarNumber;
//
//    @Column(name = "USER_PICTURE")
//    private String userPicture;
//
//    @Column(name = "USER_ACTIVE_THEME")
//    private String userActiveTheme;
//
//    @Column(name = "USER_FORCE_CHANGE_PASSWORD")
//    private Boolean userIsForceChangePassword;
//
//    @Column(name = "USER_TOKEN")
//    private String userToken;
//
//    public Long getReportFilterId() {
//        return reportFilterId;
//    }
//
//    public void setReportFilterId(Long reportFilterId) {
//        this.reportFilterId = reportFilterId;
//    }
//
//    public String getReportFilterName() {
//        return reportFilterName;
//    }
//
//    public void setReportFilterName(String reportFilterName) {
//        this.reportFilterName = reportFilterName;
//    }
//
//    public String getReportFilterQuery() {
//        return reportFilterQuery;
//    }
//
//    public void setReportFilterQuery(String reportFilterQuery) {
//        this.reportFilterQuery = reportFilterQuery;
//    }
//
//    public String getReportFilterNumber() {
//        return reportFilterNumber;
//    }
//
//    public void setReportFilterNumber(String reportFilterNumber) {
//        this.reportFilterNumber = reportFilterNumber;
//    }
//
//    public String getReportFilterDefualtValue() {
//        return reportFilterDefualtValue;
//    }
//
//    public void setReportFilterDefualtValue(String reportFilterDefualtValue) {
//        this.reportFilterDefualtValue = reportFilterDefualtValue;
//    }
//
//    public Long getUserId() {
//        return userId;
//    }
//
//    public void setUserId(Long userId) {
//        this.userId = userId;
//    }
//
//    public String getUserUsername() {
//        return userUsername;
//    }
//
//    public void setUserUsername(String userUsername) {
//        this.userUsername = userUsername;
//    }
//
//    public String getUserFirstName() {
//        return userFirstName;
//    }
//
//    public void setUserFirstName(String userFirstName) {
//        this.userFirstName = userFirstName;
//    }
//
//    public String getUserLastName() {
//        return userLastName;
//    }
//
//    public void setUserLastName(String userLastName) {
//        this.userLastName = userLastName;
//    }
//
//    public String getUserEmail() {
//        return userEmail;
//    }
//
//    public void setUserEmail(String userEmail) {
//        this.userEmail = userEmail;
//    }
//
//    public String getUserPassword() {
//        return userPassword;
//    }
//
//    public void setUserPassword(String userPassword) {
//        this.userPassword = userPassword;
//    }
//
//    public Boolean getUserIsObsolete() {
//        return userIsObsolete;
//    }
//
//    public void setUserIsObsolete(Boolean userIsObsolete) {
//        this.userIsObsolete = userIsObsolete;
//    }
//
//    public String getUserLastLogin() {
//        return userLastLogin;
//    }
//
//    public void setUserLastLogin(String userLastLogin) {
//        this.userLastLogin = userLastLogin;
//    }
//
//    public Boolean getUserIsOnline() {
//        return userIsOnline;
//    }
//
//    public void setUserIsOnline(Boolean userIsOnline) {
//        this.userIsOnline = userIsOnline;
//    }
//
//    public String getUserExpireDate() {
//        return userExpireDate;
//    }
//
//    public void setUserExpireDate(String userExpireDate) {
//        this.userExpireDate = userExpireDate;
//    }
//
//    public String getUserLastIp() {
//        return userLastIp;
//    }
//
//    public void setUserLastIp(String userLastIp) {
//        this.userLastIp = userLastIp;
//    }
//
//    public Boolean getUserIsActive() {
//        return userIsActive;
//    }
//
//    public void setUserIsActive(Boolean userIsActive) {
//        this.userIsActive = userIsActive;
//    }
//
//    public String getUserTell() {
//        return userTell;
//    }
//
//    public void setUserTell(String userTell) {
//        this.userTell = userTell;
//    }
//
//    public String getUserMobile() {
//        return userMobile;
//    }
//
//    public void setUserMobile(String userMobile) {
//        this.userMobile = userMobile;
//    }
//
//    public String getUserKowsarNumber() {
//        return userKowsarNumber;
//    }
//
//    public void setUserKowsarNumber(String userKowsarNumber) {
//        this.userKowsarNumber = userKowsarNumber;
//    }
//
//    public String getUserPicture() {
//        return userPicture;
//    }
//
//    public void setUserPicture(String userPicture) {
//        this.userPicture = userPicture;
//    }
//
//    public String getUserActiveTheme() {
//        return userActiveTheme;
//    }
//
//    public void setUserActiveTheme(String userActiveTheme) {
//        this.userActiveTheme = userActiveTheme;
//    }
//
//    public Boolean getUserIsForceChangePassword() {
//        return userIsForceChangePassword;
//    }
//
//    public void setUserIsForceChangePassword(Boolean userIsForceChangePassword) {
//        this.userIsForceChangePassword = userIsForceChangePassword;
//    }
//
//    public String getUserToken() {
//        return userToken;
//    }
//
//    public void setUserToken(String userToken) {
//        this.userToken = userToken;
//    }
}
