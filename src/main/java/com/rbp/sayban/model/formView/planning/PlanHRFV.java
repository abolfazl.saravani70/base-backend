package com.rbp.sayban.model.formView.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.planning.Plan;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "PLAN_HR_VIEW")
public class PlanHRFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_PLAN_ID")
    private Long planId;

    @Column(name = "PLAN_FK_PV_TYPE_ID")
    private Long planPvTypeId;

    @Column(name = "PLAN_PV_TYPE_TITLE")
    private String planPvTypeTitle;

    @Column(name = "PLAN_NAME")
    private String planName;

    @Column(name = "PLAN_TITLE")
    private String planTitle;

    @Column(name = "PLAN_IS_ROOT")
    private Boolean planIsRoot;

    @Column(name = "PLAN_INITIAL_PERCENTAGE")
    private Double planInitialPercentage;

    @Column(name = "PLAN_PERCENTAGE")
    private Double planPercentage;

    @Column(name = "PLAN_WEIGHT")
    private Long planWeight;

    @Column(name = "PLAN_VOLUME")
    private Long planVolume;

    @Column(name = "PLAN_PRIORITY")
    private Integer planPriority;

    @Column(name = "PLAN_IMPORTANCE")
    private Double planImportance;

    @Column(name = "PLAN_ACTUAL_START_DATE")
    private LocalDate planActualStartDate;

    @Column(name = "PLAN_ACTUAL_END_DATE")
    private LocalDate planActualEndDate;

    @Column(name = "PLAN_EXPIRATION_DATE")
    private LocalDate planExpirationDate;

    @Column(name = "PLAN_START_DATE")
    private LocalDate planStartDate;

    @Column(name = "PLAN_END_DATE")
    private LocalDate planEndDate;

    @Column(name = "PLAN_FISCAL_YEAR")
    private String planFiscalYear;

    @Column(name = "PLAN_LEVEL$")
    private Long planLevel;

    @Column(name = "FK_HR_ID")
    private Long humanResourceId;

    @Column(name = "HR_TITLE")
    private String hRTitle;

    @Column(name = "HR_IS_ENABLE")
    private Boolean hRIsEnabled;

    @Column(name = "HR_FK_ORG_CHART_ID")
    private Long hROrganizationChartId;

    @Column(name = "HR_FK_ORG_ID")
    private Long hROrganizationId;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanPvTypeId() {
        return planPvTypeId;
    }

    public void setPlanPvTypeId(Long planPvTypeId) {
        this.planPvTypeId = planPvTypeId;
    }

    public String getPlanPvTypeTitle() {
        return planPvTypeTitle;
    }

    public void setPlanPvTypeTitle(String planPvTypeTitle) {
        this.planPvTypeTitle = planPvTypeTitle;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanTitle() {
        return planTitle;
    }

    public void setPlanTitle(String planTitle) {
        this.planTitle = planTitle;
    }

    public Boolean getPlanIsRoot() {
        return planIsRoot;
    }

    public void setPlanIsRoot(Boolean planIsRoot) {
        this.planIsRoot = planIsRoot;
    }

    public Double getPlanInitialPercentage() {
        return planInitialPercentage;
    }

    public void setPlanInitialPercentage(Double planInitialPercentage) {
        this.planInitialPercentage = planInitialPercentage;
    }

    public Double getPlanPercentage() {
        return planPercentage;
    }

    public void setPlanPercentage(Double planPercentage) {
        this.planPercentage = planPercentage;
    }

    public Long getPlanWeight() {
        return planWeight;
    }

    public void setPlanWeight(Long planWeight) {
        this.planWeight = planWeight;
    }

    public Long getPlanVolume() {
        return planVolume;
    }

    public void setPlanVolume(Long planVolume) {
        this.planVolume = planVolume;
    }

    public Integer getPlanPriority() {
        return planPriority;
    }

    public void setPlanPriority(Integer planPriority) {
        this.planPriority = planPriority;
    }

    public Double getPlanImportance() {
        return planImportance;
    }

    public void setPlanImportance(Double planImportance) {
        this.planImportance = planImportance;
    }

    public LocalDate getPlanActualStartDate() {
        return planActualStartDate;
    }

    public void setPlanActualStartDate(LocalDate planActualStartDate) {
        this.planActualStartDate = planActualStartDate;
    }

    public LocalDate getPlanActualEndDate() {
        return planActualEndDate;
    }

    public void setPlanActualEndDate(LocalDate planActualEndDate) {
        this.planActualEndDate = planActualEndDate;
    }

    public LocalDate getPlanExpirationDate() {
        return planExpirationDate;
    }

    public void setPlanExpirationDate(LocalDate planExpirationDate) {
        this.planExpirationDate = planExpirationDate;
    }

    public LocalDate getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(LocalDate planStartDate) {
        this.planStartDate = planStartDate;
    }

    public LocalDate getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(LocalDate planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanFiscalYear() {
        return planFiscalYear;
    }

    public void setPlanFiscalYear(String planFiscalYear) {
        this.planFiscalYear = planFiscalYear;
    }

    public Long getPlanLevel() {
        return planLevel;
    }

    public void setPlanLevel(Long planLevel) {
        this.planLevel = planLevel;
    }

    public Long getHumanResourceId() {
        return humanResourceId;
    }

    public void setHumanResourceId(Long humanResourceId) {
        this.humanResourceId = humanResourceId;
    }

    public String gethRTitle() {
        return hRTitle;
    }

    public void sethRTitle(String hRTitle) {
        this.hRTitle = hRTitle;
    }

    public Boolean gethRIsEnabled() {
        return hRIsEnabled;
    }

    public void sethRIsEnabled(Boolean hRIsEnabled) {
        this.hRIsEnabled = hRIsEnabled;
    }

    public Long gethROrganizationChartId() {
        return hROrganizationChartId;
    }

    public void sethROrganizationChartId(Long hROrganizationChartId) {
        this.hROrganizationChartId = hROrganizationChartId;
    }

    public Long gethROrganizationId() {
        return hROrganizationId;
    }

    public void sethROrganizationId(Long hROrganizationId) {
        this.hROrganizationId = hROrganizationId;
    }
}
