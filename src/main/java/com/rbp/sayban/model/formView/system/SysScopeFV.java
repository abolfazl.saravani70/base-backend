/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;
import com.rbp.sayban.model.domainmodel.system.System;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "SYS_SCOPE_VIEW")
public class SysScopeFV extends StereotypeBaseEntity {

    @Column(name = "FK_SYSTEM_ID")
    private Long systemId;

    @Column(name = "SYSTEM_TYPE")
    private String systemType;

    @Column(name = "SYSTEM_TITLE")
    private String systemTitle;

    @Column(name = "SYSTEM_VERSION")
    private String systemVersion;

    @Column(name = "SYSTEM_NAME")
    private  String systemName;

    @Column(name = "SYSTEM_CODE")
    private  Integer systemCode;

    @Column(name = "SYSTEM_KEY")
    private String systemKey;

    @Column(name = "SYSTEM_DB")
    private  String systemDb;

    @Column(name = "SYSTEM_BASE_CONFIG")
    private String systemBaseConfig;

    @Column(name = "SYSTEM_HOST")
    private String systemHost;

    @Column(name = "SYSTEM_API")
    private String systemApi;

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getSystemType() {
        return systemType;
    }

    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }

    public String getSystemTitle() {
        return systemTitle;
    }

    public void setSystemTitle(String systemTitle) {
        this.systemTitle = systemTitle;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public Integer getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(Integer systemCode) {
        this.systemCode = systemCode;
    }

    public String getSystemKey() {
        return systemKey;
    }

    public void setSystemKey(String systemKey) {
        this.systemKey = systemKey;
    }

    public String getSystemDb() {
        return systemDb;
    }

    public void setSystemDb(String systemDb) {
        this.systemDb = systemDb;
    }

    public String getSystemBaseConfig() {
        return systemBaseConfig;
    }

    public void setSystemBaseConfig(String systemBaseConfig) {
        this.systemBaseConfig = systemBaseConfig;
    }

    public String getSystemHost() {
        return systemHost;
    }

    public void setSystemHost(String systemHost) {
        this.systemHost = systemHost;
    }

    public String getSystemApi() {
        return systemApi;
    }

    public void setSystemApi(String systemApi) {
        this.systemApi = systemApi;
    }
}
