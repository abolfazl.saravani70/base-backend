/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.activity;


import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "ACTIVITY_KPI_VIEW")
public class ActivityKpiFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @JoinColumn(name = "FK_KPI_ID")
    private Long kpiId;

    @Column(name = "KPI_NAME")
    private String kpiName;

    @Column(name = "KPI_TITLE")
    private String kpiTitle;

    @Column(name = "KPI_IS_ROOT")
    private Boolean kpiIsRoot;

    @Column(name = "KPI_INITIAL_PERCENTAGE")
    private Double kpiInitialPercentage;

    @Column(name = "KPI_PERCENTAGE")
    private Double kpiPercentage;

    @Column(name = "KPI_WEIGHT")
    private Long kpiWeight;

    @Column(name = "KPI_VOLUME")
    private Long kpiVolume;

    @Column(name = "KPI_PRIORITY")
    private Integer kpiPriority;

    @Column(name = "KPI_IMPORTANCE")
    private Double KpiImportance;

    @Column(name = "KPI_ACTUAL_START_DATE")
    private LocalDate kpiActualStartDate;

    @Column(name = "KPI_ACTUAL_END_DATE")
    private LocalDate kpiActualEndDate;

    @Column(name = "KPI_EXPIRATION_DATE")
    private LocalDate kpiExpirationDate;

    @Column(name = "KPI_START_DATE")
    private LocalDate kpiStartDate;

    @Column(name = "KPI_END_DATE")
    private LocalDate kpiEndDate;

    @Column(name = "KPI_FISCAL_YEAR")
    private String kpiFiscalYear;

    @Column(name = "KPI_LEVEL$")
    private Long kpiLevel;

    @Column(name = "KPI_KEY")
    private Integer kpiKey;

    @Column(name = "KPI_CODE")
    private String kpiCode;

    @Column(name = "KPI_VALUE")
    private Long kpiValue;

    @Column(name = "KPI_BEST_VALUE")
    private Long kpiBestValue;

    @Column(name = "KPI_KPI_PRIORITY")
    private Integer kpiKpiPriority;

    @Column(name = "KPI_COST")
    private Integer kpiCost;

    @Column(name = "KPI_MINIMUM")
    private Long kpiMinimum;

    @Column(name = "KPI_MAXIMUM")
    private Long kpiMaximum;

    @Column(name = "KPI_NORMAL")
    private Long kpiNormal;

    @Column(name = "KPI_FK_PV_KPI_TYPE_ID")
    private Long kpiPvKpiTypeId;

    @Column(name = "KPI_PV_KPI_TYPE_TITLE")
    private String kpiPvKpiTypeTitle;

    @Column(name = "KPI_FK_PV_MESURE_UNIT_ID")
    private Long kpiPvMesureUnitId;

    @Column(name = "KPI_PV_MESURE_UNIT_TITLE")
    private String kpiPvMesureUnitTitle;

    @Column(name = "FK_ACTIVITY_ID")
    private Long activityId;

    @Column(name = "ACTIVITY_FK_PV_TYPE_ID")
    private Long activityPvTypeId;

    @Column(name = "ACTIVITY_PV_TYPE_TITLE")
    private String activityPvTypeTitle;

    @Column(name = "ACTIVITY_NAME")
    private String activityName;

    @Column(name = "ACTIVITY_TITLE")
    private String activityTitle;

    @Column(name = "ACTIVITY_IS_ROOT")
    private Boolean activityIsRoot;

    @Column(name = "ACTIVITY_INITIAL_PERCENTAGE")
    private Double activityInitialPercentage;

    @Column(name = "ACTIVITY_PERCENTAGE")
    private Double activityPercentage;

    @Column(name = "ACTIVITY_WEIGHT")
    private Long activityWeight;

    @Column(name = "ACTIVITY_VOLUME")
    private Long activityVolume;

    @Column(name = "ACTIVITY_PRIORITY")
    private Integer activityPriority;

    @Column(name = "ACTIVITY_IMPORTANCE")
    private Double activityImportance;

    @Column(name = "ACTIVITY_ACTUAL_START_DATE")
    private LocalDate activityActualStartDate;

    @Column(name = "ACTIVITY_ACTUAL_END_DATE")
    private LocalDate activityActualEndDate;

    @Column(name = "ACTIVITY_EXPIRATION_DATE")
    private LocalDate activityExpirationDate;

    @Column(name = "ACTIVITY_START_DATE")
    private LocalDate activityStartDate;

    @Column(name = "ACTIVITY_END_DATE")
    private LocalDate activityEndDate;

    @Column(name = "ACTIVITY_FISCAL_YEAR")
    private String activityFiscalYear;

    @Column(name = "ACTIVITY_LEVEL$")
    private Long activityLevel;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getKpiId() {
        return kpiId;
    }

    public void setKpiId(Long kpiId) {
        this.kpiId = kpiId;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getKpiTitle() {
        return kpiTitle;
    }

    public void setKpiTitle(String kpiTitle) {
        this.kpiTitle = kpiTitle;
    }

    public Boolean getKpiIsRoot() {
        return kpiIsRoot;
    }

    public void setKpiIsRoot(Boolean kpiIsRoot) {
        this.kpiIsRoot = kpiIsRoot;
    }

    public Double getKpiInitialPercentage() {
        return kpiInitialPercentage;
    }

    public void setKpiInitialPercentage(Double kpiInitialPercentage) {
        this.kpiInitialPercentage = kpiInitialPercentage;
    }

    public Double getKpiPercentage() {
        return kpiPercentage;
    }

    public void setKpiPercentage(Double kpiPercentage) {
        this.kpiPercentage = kpiPercentage;
    }

    public Long getKpiWeight() {
        return kpiWeight;
    }

    public void setKpiWeight(Long kpiWeight) {
        this.kpiWeight = kpiWeight;
    }

    public Long getKpiVolume() {
        return kpiVolume;
    }

    public void setKpiVolume(Long kpiVolume) {
        this.kpiVolume = kpiVolume;
    }

    public Integer getKpiPriority() {
        return kpiPriority;
    }

    public void setKpiPriority(Integer kpiPriority) {
        this.kpiPriority = kpiPriority;
    }

    public Double getKpiImportance() {
        return KpiImportance;
    }

    public void setKpiImportance(Double kpiImportance) {
        KpiImportance = kpiImportance;
    }

    public LocalDate getKpiActualStartDate() {
        return kpiActualStartDate;
    }

    public void setKpiActualStartDate(LocalDate kpiActualStartDate) {
        this.kpiActualStartDate = kpiActualStartDate;
    }

    public LocalDate getKpiActualEndDate() {
        return kpiActualEndDate;
    }

    public void setKpiActualEndDate(LocalDate kpiActualEndDate) {
        this.kpiActualEndDate = kpiActualEndDate;
    }

    public LocalDate getKpiExpirationDate() {
        return kpiExpirationDate;
    }

    public void setKpiExpirationDate(LocalDate kpiExpirationDate) {
        this.kpiExpirationDate = kpiExpirationDate;
    }

    public LocalDate getKpiStartDate() {
        return kpiStartDate;
    }

    public void setKpiStartDate(LocalDate kpiStartDate) {
        this.kpiStartDate = kpiStartDate;
    }

    public LocalDate getKpiEndDate() {
        return kpiEndDate;
    }

    public void setKpiEndDate(LocalDate kpiEndDate) {
        this.kpiEndDate = kpiEndDate;
    }

    public String getKpiFiscalYear() {
        return kpiFiscalYear;
    }

    public void setKpiFiscalYear(String kpiFiscalYear) {
        this.kpiFiscalYear = kpiFiscalYear;
    }

    public Long getKpiLevel() {
        return kpiLevel;
    }

    public void setKpiLevel(Long kpiLevel) {
        this.kpiLevel = kpiLevel;
    }

    public Integer getKpiKey() {
        return kpiKey;
    }

    public void setKpiKey(Integer kpiKey) {
        this.kpiKey = kpiKey;
    }

    public String getKpiCode() {
        return kpiCode;
    }

    public void setKpiCode(String kpiCode) {
        this.kpiCode = kpiCode;
    }

    public Long getKpiValue() {
        return kpiValue;
    }

    public void setKpiValue(Long kpiValue) {
        this.kpiValue = kpiValue;
    }

    public Long getKpiBestValue() {
        return kpiBestValue;
    }

    public void setKpiBestValue(Long kpiBestValue) {
        this.kpiBestValue = kpiBestValue;
    }

    public Integer getKpiKpiPriority() {
        return kpiKpiPriority;
    }

    public void setKpiKpiPriority(Integer kpiKpiPriority) {
        this.kpiKpiPriority = kpiKpiPriority;
    }

    public Integer getKpiCost() {
        return kpiCost;
    }

    public void setKpiCost(Integer kpiCost) {
        this.kpiCost = kpiCost;
    }

    public Long getKpiMinimum() {
        return kpiMinimum;
    }

    public void setKpiMinimum(Long kpiMinimum) {
        this.kpiMinimum = kpiMinimum;
    }

    public Long getKpiMaximum() {
        return kpiMaximum;
    }

    public void setKpiMaximum(Long kpiMaximum) {
        this.kpiMaximum = kpiMaximum;
    }

    public Long getKpiNormal() {
        return kpiNormal;
    }

    public void setKpiNormal(Long kpiNormal) {
        this.kpiNormal = kpiNormal;
    }

    public Long getKpiPvKpiTypeId() {
        return kpiPvKpiTypeId;
    }

    public void setKpiPvKpiTypeId(Long kpiPvKpiTypeId) {
        this.kpiPvKpiTypeId = kpiPvKpiTypeId;
    }

    public String getKpiPvKpiTypeTitle() {
        return kpiPvKpiTypeTitle;
    }

    public void setKpiPvKpiTypeTitle(String kpiPvKpiTypeTitle) {
        this.kpiPvKpiTypeTitle = kpiPvKpiTypeTitle;
    }

    public Long getKpiPvMesureUnitId() {
        return kpiPvMesureUnitId;
    }

    public void setKpiPvMesureUnitId(Long kpiPvMesureUnitId) {
        this.kpiPvMesureUnitId = kpiPvMesureUnitId;
    }

    public String getKpiPvMesureUnitTitle() {
        return kpiPvMesureUnitTitle;
    }

    public void setKpiPvMesureUnitTitle(String kpiPvMesureUnitTitle) {
        this.kpiPvMesureUnitTitle = kpiPvMesureUnitTitle;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getActivityPvTypeId() {
        return activityPvTypeId;
    }

    public void setActivityPvTypeId(Long activityPvTypeId) {
        this.activityPvTypeId = activityPvTypeId;
    }

    public String getActivityPvTypeTitle() {
        return activityPvTypeTitle;
    }

    public void setActivityPvTypeTitle(String activityPvTypeTitle) {
        this.activityPvTypeTitle = activityPvTypeTitle;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public Boolean getActivityIsRoot() {
        return activityIsRoot;
    }

    public void setActivityIsRoot(Boolean activityIsRoot) {
        this.activityIsRoot = activityIsRoot;
    }

    public Double getActivityInitialPercentage() {
        return activityInitialPercentage;
    }

    public void setActivityInitialPercentage(Double activityInitialPercentage) {
        this.activityInitialPercentage = activityInitialPercentage;
    }

    public Double getActivityPercentage() {
        return activityPercentage;
    }

    public void setActivityPercentage(Double activityPercentage) {
        this.activityPercentage = activityPercentage;
    }

    public Long getActivityWeight() {
        return activityWeight;
    }

    public void setActivityWeight(Long activityWeight) {
        this.activityWeight = activityWeight;
    }

    public Long getActivityVolume() {
        return activityVolume;
    }

    public void setActivityVolume(Long activityVolume) {
        this.activityVolume = activityVolume;
    }

    public Integer getActivityPriority() {
        return activityPriority;
    }

    public void setActivityPriority(Integer activityPriority) {
        this.activityPriority = activityPriority;
    }

    public Double getActivityImportance() {
        return activityImportance;
    }

    public void setActivityImportance(Double activityImportance) {
        this.activityImportance = activityImportance;
    }

    public LocalDate getActivityActualStartDate() {
        return activityActualStartDate;
    }

    public void setActivityActualStartDate(LocalDate activityActualStartDate) {
        this.activityActualStartDate = activityActualStartDate;
    }

    public LocalDate getActivityActualEndDate() {
        return activityActualEndDate;
    }

    public void setActivityActualEndDate(LocalDate activityActualEndDate) {
        this.activityActualEndDate = activityActualEndDate;
    }

    public LocalDate getActivityExpirationDate() {
        return activityExpirationDate;
    }

    public void setActivityExpirationDate(LocalDate activityExpirationDate) {
        this.activityExpirationDate = activityExpirationDate;
    }

    public LocalDate getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(LocalDate activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public LocalDate getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(LocalDate activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityFiscalYear() {
        return activityFiscalYear;
    }

    public void setActivityFiscalYear(String activityFiscalYear) {
        this.activityFiscalYear = activityFiscalYear;
    }

    public Long getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Long activityLevel) {
        this.activityLevel = activityLevel;
    }
}
