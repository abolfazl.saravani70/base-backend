/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.task;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TASK_VIEW")
public class TaskFV extends EvalStateBaseEntity {

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "PARENT_FK_PV_TYPE_ID")
    private Long parentPvTypeId;

    @Column(name = "PARENT_PV_TYPE_TITLE")
    private String parentPvTypeTitle;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TITLE")
    private String parentTitle;

    @Column(name = "PARENT_IS_ROOT")
    private Boolean parentIsRoot;

    @Column(name = "PARENT_INITIAL_PERCENTAGE")
    private Double parentInitialPercentage;

    @Column(name = "PARENT_PERCENTAGE")
    private Double parentPercentage;

    @Column(name = "PARENT_WEIGHT")
    private Long parentWeight;

    @Column(name = "PARENT_VOLUME")
    private Long parentVolume;

    @Column(name = "PARENT_PRIORITY")
    private Integer parentPriority;

    @Column(name = "PARENT_IMPORTANCE")
    private Double parentImportance;

    @Column(name = "PARENT_ACTUAL_START_DATE")
    private LocalDate parentActualStartDate;

    @Column(name = "PARENT_ACTUAL_END_DATE")
    private LocalDate parentActualEndDate;

    @Column(name = "PARENT_EXPIRATION_DATE")
    private LocalDate parentExpirationDate;

    @Column(name = "PARENT_START_DATE")
    private LocalDate parentStartDate;

    @Column(name = "PARENT_END_DATE")
    private LocalDate parentEndDate;

    @Column(name = "PARENT_FISCAL_YEAR")
    private String parentFiscalYear;

    @Column(name = "PARENT_LEVEL$")
    private Long parentLevel;

    @Column(name = "FK_PV_ACTIVITY_ID")
    private Long activityPvTypeId;

    @Column(name = "ACTIVITY_PV_TYPE_TITLE")
    private String  activityPvTypeTitle;

    @Column(name = "ACTIVITY_NAME")
    private String activityName;

    @Column(name = "ACTIVITY_TITLE")
    private String activityTitle;

    @Column(name = "ACTIVITY_IS_ROOT")
    private Boolean activityIsRoot;

    @Column(name = "ACTIVITY_INITIAL_PERCENTAGE")
    private Double activityInitialPercentage;

    @Column(name = "ACTIVITY_PERCENTAGE")
    private Double activityPercentage;

    @Column(name = "ACTIVITY_WEIGHT")
    private Long activityWeight;

    @Column(name = "ACTIVITY_VOLUME")
    private Long activityVolume;

    @Column(name = "ACTIVITY_PRIORITY")
    private Integer activityPriority;

    @Column(name = "ACTIVITY_IMPORTANCE")
    private Double activityImportance;

    @Column(name = "ACTIVITY_ACTUAL_START_DATE")
    private LocalDate activityActualStartDate;

    @Column(name = "ACTIVITY_ACTUAL_END_DATE")
    private LocalDate activityActualEndDate;

    @Column(name = "ACTIVITY_EXPIRATION_DATE")
    private LocalDate activityExpirationDate;

    @Column(name = "ACTIVITY_START_DATE")
    private LocalDate activityStartDate;

    @Column(name = "ACTIVITY_END_DATE")
    private LocalDate activityEndDate;

    @Column(name = "ACTIVITY_FISCAL_YEAR")
    private String activityFiscalYear;

    @Column(name = "ACTIVITY_LEVEL$")
    private Long activityLevel;

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getParentPvTypeId() {
        return parentPvTypeId;
    }

    public void setParentPvTypeId(Long parentPvTypeId) {
        this.parentPvTypeId = parentPvTypeId;
    }

    public String getParentPvTypeTitle() {
        return parentPvTypeTitle;
    }

    public void setParentPvTypeTitle(String parentPvTypeTitle) {
        this.parentPvTypeTitle = parentPvTypeTitle;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public Boolean getParentIsRoot() {
        return parentIsRoot;
    }

    public void setParentIsRoot(Boolean parentIsRoot) {
        this.parentIsRoot = parentIsRoot;
    }

    public Double getParentInitialPercentage() {
        return parentInitialPercentage;
    }

    public void setParentInitialPercentage(Double parentInitialPercentage) {
        this.parentInitialPercentage = parentInitialPercentage;
    }

    public Double getParentPercentage() {
        return parentPercentage;
    }

    public void setParentPercentage(Double parentPercentage) {
        this.parentPercentage = parentPercentage;
    }

    public Long getParentWeight() {
        return parentWeight;
    }

    public void setParentWeight(Long parentWeight) {
        this.parentWeight = parentWeight;
    }

    public Long getParentVolume() {
        return parentVolume;
    }

    public void setParentVolume(Long parentVolume) {
        this.parentVolume = parentVolume;
    }

    public Integer getParentPriority() {
        return parentPriority;
    }

    public void setParentPriority(Integer parentPriority) {
        this.parentPriority = parentPriority;
    }

    public Double getParentImportance() {
        return parentImportance;
    }

    public void setParentImportance(Double parentImportance) {
        this.parentImportance = parentImportance;
    }

    public LocalDate getParentActualStartDate() {
        return parentActualStartDate;
    }

    public void setParentActualStartDate(LocalDate parentActualStartDate) {
        this.parentActualStartDate = parentActualStartDate;
    }

    public LocalDate getParentActualEndDate() {
        return parentActualEndDate;
    }

    public void setParentActualEndDate(LocalDate parentActualEndDate) {
        this.parentActualEndDate = parentActualEndDate;
    }

    public LocalDate getParentExpirationDate() {
        return parentExpirationDate;
    }

    public void setParentExpirationDate(LocalDate parentExpirationDate) {
        this.parentExpirationDate = parentExpirationDate;
    }

    public LocalDate getParentStartDate() {
        return parentStartDate;
    }

    public void setParentStartDate(LocalDate parentStartDate) {
        this.parentStartDate = parentStartDate;
    }

    public LocalDate getParentEndDate() {
        return parentEndDate;
    }

    public void setParentEndDate(LocalDate parentEndDate) {
        this.parentEndDate = parentEndDate;
    }

    public String getParentFiscalYear() {
        return parentFiscalYear;
    }

    public void setParentFiscalYear(String parentFiscalYear) {
        this.parentFiscalYear = parentFiscalYear;
    }

    public Long getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Long parentLevel) {
        this.parentLevel = parentLevel;
    }

    public Long getActivityPvTypeId() {
        return activityPvTypeId;
    }

    public void setActivityPvTypeId(Long activityPvTypeId) {
        this.activityPvTypeId = activityPvTypeId;
    }

    public String getActivityPvTypeTitle() {
        return activityPvTypeTitle;
    }

    public void setActivityPvTypeTitle(String activityPvTypeTitle) {
        this.activityPvTypeTitle = activityPvTypeTitle;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public Boolean getActivityIsRoot() {
        return activityIsRoot;
    }

    public void setActivityIsRoot(Boolean activityIsRoot) {
        this.activityIsRoot = activityIsRoot;
    }

    public Double getActivityInitialPercentage() {
        return activityInitialPercentage;
    }

    public void setActivityInitialPercentage(Double activityInitialPercentage) {
        this.activityInitialPercentage = activityInitialPercentage;
    }

    public Double getActivityPercentage() {
        return activityPercentage;
    }

    public void setActivityPercentage(Double activityPercentage) {
        this.activityPercentage = activityPercentage;
    }

    public Long getActivityWeight() {
        return activityWeight;
    }

    public void setActivityWeight(Long activityWeight) {
        this.activityWeight = activityWeight;
    }

    public Long getActivityVolume() {
        return activityVolume;
    }

    public void setActivityVolume(Long activityVolume) {
        this.activityVolume = activityVolume;
    }

    public Integer getActivityPriority() {
        return activityPriority;
    }

    public void setActivityPriority(Integer activityPriority) {
        this.activityPriority = activityPriority;
    }

    public Double getActivityImportance() {
        return activityImportance;
    }

    public void setActivityImportance(Double activityImportance) {
        this.activityImportance = activityImportance;
    }

    public LocalDate getActivityActualStartDate() {
        return activityActualStartDate;
    }

    public void setActivityActualStartDate(LocalDate activityActualStartDate) {
        this.activityActualStartDate = activityActualStartDate;
    }

    public LocalDate getActivityActualEndDate() {
        return activityActualEndDate;
    }

    public void setActivityActualEndDate(LocalDate activityActualEndDate) {
        this.activityActualEndDate = activityActualEndDate;
    }

    public LocalDate getActivityExpirationDate() {
        return activityExpirationDate;
    }

    public void setActivityExpirationDate(LocalDate activityExpirationDate) {
        this.activityExpirationDate = activityExpirationDate;
    }

    public LocalDate getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(LocalDate activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public LocalDate getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(LocalDate activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityFiscalYear() {
        return activityFiscalYear;
    }

    public void setActivityFiscalYear(String activityFiscalYear) {
        this.activityFiscalYear = activityFiscalYear;
    }

    public Long getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Long activityLevel) {
        this.activityLevel = activityLevel;
    }
}
