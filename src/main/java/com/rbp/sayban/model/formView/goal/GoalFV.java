/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.sayban.model.domainmodel.system.System;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "GOAL_VIEW")
public class GoalFV extends EvalStateBaseEntity {

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_FK_PV_TYPE_ID")
    private Long parentPvTypeId;

    @Column(name = "PARENT_PV_TYPE_TITLE")
    private String parentPvTypeTitle;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TITLE")
    private String parentTitle;

    @Column(name = "PARENT_IS_ROOT")
    private Boolean parentIsRoot;

    @Column(name = "PARENT_INITIAL_PERCENTAGE")
    private Double parentInitialPercentage;

    @Column(name = "PARENT_PERCENTAGE")
    private Double parentPercentage;

    @Column(name = "PARENT_WEIGHT")
    private Long parentWeight;

    @Column(name = "PARENT_VOLUME")
    private Long parentVolume;

    @Column(name = "PARENT_PRIORITY")
    private Integer parentPriority;

    @Column(name = "PARENT_IMPORTANCE")
    private Double parentImportance;

    @Column(name = "PARENT_ACTUAL_START_DATE")
    private LocalDate parentActualStartDate;

    @Column(name = "PARENT_ACTUAL_END_DATE")
    private LocalDate parentActualEndDate;

    @Column(name = "PARENT_EXPIRATION_DATE")
    private LocalDate parentExpirationDate;

    @Column(name = "PARENT_START_DATE")
    private LocalDate parentStartDate;

    @Column(name = "PARENT_END_DATE")
    private LocalDate parentEndDate;

    @Column(name = "PARENT_FISCAL_YEAR")
    private String parentFiscalYear;

    @Column(name = "PARENT_LEVEL$")
    private Long parentLevel;

    @Column(name = "FK_STRATEGY_ID")
    private Long strategyId;

    @Column(name = "STRATEGY_FK_PV_TYPE_ID")
    private Long strategyPvTypeId;

    @Column(name = "STRATEGY_PV_TYPE_TITLE")
    private String strategyPvTypeTitle;

    @Column(name = "STRATEGY_NAME")
    private String stratEgyname;

    @Column(name = "STRATEGY_TITLE")
    private String stratEgytitle;

    @Column(name = "STRATEGY_IS_ROOT")
    private Boolean strategyStrategyisRoot;

    @Column(name = "STRATEGY_INITIAL_PERCENTAGE")
    private Double strategyInitialPercentage;

    @Column(name = "STRATEGY_PERCENTAGE")
    private Double strategyPercentage;

    @Column(name = "STRATEGY_WEIGHT")
    private Long strategyWeight;

    @Column(name = "STRATEGY_VOLUME")
    private Long strategyVolume;

    @Column(name = "STRATEGY_PRIORITY")
    private Integer strategyPriority;

    @Column(name = "STRATEGY_IMPORTANCE")
    private Double strategyImportance;

    @Column(name = "STRATEGY_ACTUAL_START_DATE")
    private LocalDate strategyActualStartDate;

    @Column(name = "STRATEGY_ACTUAL_END_DATE")
    private LocalDate strategyActualEndDate;

    @Column(name = "STRATEGY_EXPIRATION_DATE")
    private LocalDate strategyExpirationDate;

    @Column(name = "STRATEGY_START_DATE")
    private LocalDate strategyStartDate;

    @Column(name = "STRATEGY_END_DATE")
    private LocalDate strategyEndDate;

    @Column(name = "STRATEGY_FISCAL_YEAR")
    private String strategyFiscalYear;

    @Column(name = "STRATEGY_LEVEL$")
    private Long strategyLevel;

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getParentPvTypeId() {
        return parentPvTypeId;
    }

    public void setParentPvTypeId(Long parentPvTypeId) {
        this.parentPvTypeId = parentPvTypeId;
    }

    public String getParentPvTypeTitle() {
        return parentPvTypeTitle;
    }

    public void setParentPvTypeTitle(String parentPvTypeTitle) {
        this.parentPvTypeTitle = parentPvTypeTitle;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public Boolean getParentIsRoot() {
        return parentIsRoot;
    }

    public void setParentIsRoot(Boolean parentIsRoot) {
        this.parentIsRoot = parentIsRoot;
    }

    public Double getParentInitialPercentage() {
        return parentInitialPercentage;
    }

    public void setParentInitialPercentage(Double parentInitialPercentage) {
        this.parentInitialPercentage = parentInitialPercentage;
    }

    public Double getParentPercentage() {
        return parentPercentage;
    }

    public void setParentPercentage(Double parentPercentage) {
        this.parentPercentage = parentPercentage;
    }

    public Long getParentWeight() {
        return parentWeight;
    }

    public void setParentWeight(Long parentWeight) {
        this.parentWeight = parentWeight;
    }

    public Long getParentVolume() {
        return parentVolume;
    }

    public void setParentVolume(Long parentVolume) {
        this.parentVolume = parentVolume;
    }

    public Integer getParentPriority() {
        return parentPriority;
    }

    public void setParentPriority(Integer parentPriority) {
        this.parentPriority = parentPriority;
    }

    public Double getParentImportance() {
        return parentImportance;
    }

    public void setParentImportance(Double parentImportance) {
        this.parentImportance = parentImportance;
    }

    public LocalDate getParentActualStartDate() {
        return parentActualStartDate;
    }

    public void setParentActualStartDate(LocalDate parentActualStartDate) {
        this.parentActualStartDate = parentActualStartDate;
    }

    public LocalDate getParentActualEndDate() {
        return parentActualEndDate;
    }

    public void setParentActualEndDate(LocalDate parentActualEndDate) {
        this.parentActualEndDate = parentActualEndDate;
    }

    public LocalDate getParentExpirationDate() {
        return parentExpirationDate;
    }

    public void setParentExpirationDate(LocalDate parentExpirationDate) {
        this.parentExpirationDate = parentExpirationDate;
    }

    public LocalDate getParentStartDate() {
        return parentStartDate;
    }

    public void setParentStartDate(LocalDate parentStartDate) {
        this.parentStartDate = parentStartDate;
    }

    public LocalDate getParentEndDate() {
        return parentEndDate;
    }

    public void setParentEndDate(LocalDate parentEndDate) {
        this.parentEndDate = parentEndDate;
    }

    public String getParentFiscalYear() {
        return parentFiscalYear;
    }

    public void setParentFiscalYear(String parentFiscalYear) {
        this.parentFiscalYear = parentFiscalYear;
    }

    public Long getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Long parentLevel) {
        this.parentLevel = parentLevel;
    }

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public Long getStrategyPvTypeId() {
        return strategyPvTypeId;
    }

    public void setStrategyPvTypeId(Long strategyPvTypeId) {
        this.strategyPvTypeId = strategyPvTypeId;
    }

    public String getStrategyPvTypeTitle() {
        return strategyPvTypeTitle;
    }

    public void setStrategyPvTypeTitle(String strategyPvTypeTitle) {
        this.strategyPvTypeTitle = strategyPvTypeTitle;
    }

    public String getStratEgyname() {
        return stratEgyname;
    }

    public void setStratEgyname(String stratEgyname) {
        this.stratEgyname = stratEgyname;
    }

    public String getStratEgytitle() {
        return stratEgytitle;
    }

    public void setStratEgytitle(String stratEgytitle) {
        this.stratEgytitle = stratEgytitle;
    }

    public Boolean getStrategyStrategyisRoot() {
        return strategyStrategyisRoot;
    }

    public void setStrategyStrategyisRoot(Boolean strategyStrategyisRoot) {
        this.strategyStrategyisRoot = strategyStrategyisRoot;
    }

    public Double getStrategyInitialPercentage() {
        return strategyInitialPercentage;
    }

    public void setStrategyInitialPercentage(Double strategyInitialPercentage) {
        this.strategyInitialPercentage = strategyInitialPercentage;
    }

    public Double getStrategyPercentage() {
        return strategyPercentage;
    }

    public void setStrategyPercentage(Double strategyPercentage) {
        this.strategyPercentage = strategyPercentage;
    }

    public Long getStrategyWeight() {
        return strategyWeight;
    }

    public void setStrategyWeight(Long strategyWeight) {
        this.strategyWeight = strategyWeight;
    }

    public Long getStrategyVolume() {
        return strategyVolume;
    }

    public void setStrategyVolume(Long strategyVolume) {
        this.strategyVolume = strategyVolume;
    }

    public Integer getStrategyPriority() {
        return strategyPriority;
    }

    public void setStrategyPriority(Integer strategyPriority) {
        this.strategyPriority = strategyPriority;
    }

    public Double getStrategyImportance() {
        return strategyImportance;
    }

    public void setStrategyImportance(Double strategyImportance) {
        this.strategyImportance = strategyImportance;
    }

    public LocalDate getStrategyActualStartDate() {
        return strategyActualStartDate;
    }

    public void setStrategyActualStartDate(LocalDate strategyActualStartDate) {
        this.strategyActualStartDate = strategyActualStartDate;
    }

    public LocalDate getStrategyActualEndDate() {
        return strategyActualEndDate;
    }

    public void setStrategyActualEndDate(LocalDate strategyActualEndDate) {
        this.strategyActualEndDate = strategyActualEndDate;
    }

    public LocalDate getStrategyExpirationDate() {
        return strategyExpirationDate;
    }

    public void setStrategyExpirationDate(LocalDate strategyExpirationDate) {
        this.strategyExpirationDate = strategyExpirationDate;
    }

    public LocalDate getStrategyStartDate() {
        return strategyStartDate;
    }

    public void setStrategyStartDate(LocalDate strategyStartDate) {
        this.strategyStartDate = strategyStartDate;
    }

    public LocalDate getStrategyEndDate() {
        return strategyEndDate;
    }

    public void setStrategyEndDate(LocalDate strategyEndDate) {
        this.strategyEndDate = strategyEndDate;
    }

    public String getStrategyFiscalYear() {
        return strategyFiscalYear;
    }

    public void setStrategyFiscalYear(String strategyFiscalYear) {
        this.strategyFiscalYear = strategyFiscalYear;
    }

    public Long getStrategyLevel() {
        return strategyLevel;
    }

    public void setStrategyLevel(Long strategyLevel) {
        this.strategyLevel = strategyLevel;
    }
}
