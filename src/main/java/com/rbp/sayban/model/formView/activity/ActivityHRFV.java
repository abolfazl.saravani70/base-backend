package com.rbp.sayban.model.formView.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "ACTIVITY_HR_VIEW")
public class ActivityHRFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_HR_ID")
    private Long hResourceId;

    @Column(name = "HR_TITLE")
    private String hRTitle;

    @Column(name = "HR_IS_ENABLE")
    private Boolean hRIsEnabled;

    @Column(name = "HR_FK_ORG_CHART_ID")
    private Long hROrganizationChartId;

    @Column(name = "HR_FK_ORG_ID")
    private Long hROrganizationId;

    @Column(name = "FK_ACTIVITY_ID")
    private Long activityId;

    @Column(name = "ACTIVITY_FK_PV_TYPE_ID")
    private Long activityPvTypeId;

    @Column(name = "ACTIVITY_PV_TYPE_TITLE")
    private String activityPvTypeTitle;

    @Column(name = "ACTIVITY_NAME")
    private String activityName;

    @Column(name = "ACTIVITY_TITLE")
    private String activityTitle;

    @Column(name = "ACTIVITY_IS_ROOT")
    private Boolean activityIsRoot;

    @Column(name = "ACTIVITY_INITIAL_PERCENTAGE")
    private Double activityInitialPercentage;

    @Column(name = "ACTIVITY_PERCENTAGE")
    private Double activityPercentage;

    @Column(name = "ACTIVITY_WEIGHT")
    private Long activityWeight;

    @Column(name = "ACTIVITY_VOLUME")
    private Long activityVolume;

    @Column(name = "ACTIVITY_PRIORITY")
    private Integer activityPriority;

    @Column(name = "ACTIVITY_IMPORTANCE")
    private Double activityImportance;

    @Column(name = "ACTIVITY_ACTUAL_START_DATE")
    private LocalDate activityActualStartDate;

    @Column(name = "ACTIVITY_ACTUAL_END_DATE")
    private LocalDate activityActualEndDate;

    @Column(name = "ACTIVITY_EXPIRATION_DATE")
    private LocalDate activityExpirationDate;

    @Column(name = "ACTIVITY_START_DATE")
    private LocalDate activityStartDate;

    @Column(name = "ACTIVITY_END_DATE")
    private LocalDate activityEndDate;

    @Column(name = "ACTIVITY_FISCAL_YEAR")
    private String activityFiscalYear;

    @Column(name = "ACTIVITY_LEVEL$")
    private Long activityLevel;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long gethResourceId() {
        return hResourceId;
    }

    public void sethResourceId(Long hResourceId) {
        this.hResourceId = hResourceId;
    }

    public String gethRTitle() {
        return hRTitle;
    }

    public void sethRTitle(String hRTitle) {
        this.hRTitle = hRTitle;
    }

    public Boolean gethRIsEnabled() {
        return hRIsEnabled;
    }

    public void sethRIsEnabled(Boolean hRIsEnabled) {
        this.hRIsEnabled = hRIsEnabled;
    }

    public Long gethROrganizationChartId() {
        return hROrganizationChartId;
    }

    public void sethROrganizationChartId(Long hROrganizationChartId) {
        this.hROrganizationChartId = hROrganizationChartId;
    }

    public Long gethROrganizationId() {
        return hROrganizationId;
    }

    public void sethROrganizationId(Long hROrganizationId) {
        this.hROrganizationId = hROrganizationId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getActivityPvTypeId() {
        return activityPvTypeId;
    }

    public void setActivityPvTypeId(Long activityPvTypeId) {
        this.activityPvTypeId = activityPvTypeId;
    }

    public String getActivityPvTypeTitle() {
        return activityPvTypeTitle;
    }

    public void setActivityPvTypeTitle(String activityPvTypeTitle) {
        this.activityPvTypeTitle = activityPvTypeTitle;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle;
    }

    public Boolean getActivityIsRoot() {
        return activityIsRoot;
    }

    public void setActivityIsRoot(Boolean activityIsRoot) {
        this.activityIsRoot = activityIsRoot;
    }

    public Double getActivityInitialPercentage() {
        return activityInitialPercentage;
    }

    public void setActivityInitialPercentage(Double activityInitialPercentage) {
        this.activityInitialPercentage = activityInitialPercentage;
    }

    public Double getActivityPercentage() {
        return activityPercentage;
    }

    public void setActivityPercentage(Double activityPercentage) {
        this.activityPercentage = activityPercentage;
    }

    public Long getActivityWeight() {
        return activityWeight;
    }

    public void setActivityWeight(Long activityWeight) {
        this.activityWeight = activityWeight;
    }

    public Long getActivityVolume() {
        return activityVolume;
    }

    public void setActivityVolume(Long activityVolume) {
        this.activityVolume = activityVolume;
    }

    public Integer getActivityPriority() {
        return activityPriority;
    }

    public void setActivityPriority(Integer activityPriority) {
        this.activityPriority = activityPriority;
    }

    public Double getActivityImportance() {
        return activityImportance;
    }

    public void setActivityImportance(Double activityImportance) {
        this.activityImportance = activityImportance;
    }

    public LocalDate getActivityActualStartDate() {
        return activityActualStartDate;
    }

    public void setActivityActualStartDate(LocalDate activityActualStartDate) {
        this.activityActualStartDate = activityActualStartDate;
    }

    public LocalDate getActivityActualEndDate() {
        return activityActualEndDate;
    }

    public void setActivityActualEndDate(LocalDate activityActualEndDate) {
        this.activityActualEndDate = activityActualEndDate;
    }

    public LocalDate getActivityExpirationDate() {
        return activityExpirationDate;
    }

    public void setActivityExpirationDate(LocalDate activityExpirationDate) {
        this.activityExpirationDate = activityExpirationDate;
    }

    public LocalDate getActivityStartDate() {
        return activityStartDate;
    }

    public void setActivityStartDate(LocalDate activityStartDate) {
        this.activityStartDate = activityStartDate;
    }

    public LocalDate getActivityEndDate() {
        return activityEndDate;
    }

    public void setActivityEndDate(LocalDate activityEndDate) {
        this.activityEndDate = activityEndDate;
    }

    public String getActivityFiscalYear() {
        return activityFiscalYear;
    }

    public void setActivityFiscalYear(String activityFiscalYear) {
        this.activityFiscalYear = activityFiscalYear;
    }

    public Long getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(Long activityLevel) {
        this.activityLevel = activityLevel;
    }

}
