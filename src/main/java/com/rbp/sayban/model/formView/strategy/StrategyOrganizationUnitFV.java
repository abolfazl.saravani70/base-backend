/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.strategy;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.OrganizationUnitJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.base.MonthsPercentage;
import com.rbp.sayban.model.domainmodel.strategies.Strategy;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "STRATEGY_ORGANIZATION_UTIL_VIEW")
public class StrategyOrganizationUnitFV extends EvalStateBaseEntity {

    @Column(name = "ASSIGN_DATE")
    private LocalDate assignDate;

    @Column(name = "DUE_DATE")
    private LocalDate dueDate;

    @Column(name = "FK_PV_STATE_ID")
    private Long pvStateId;

    @Column(name = "PV_STATE_TITLE")
    private String pvStateTitle;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "FK_ORG_UNIT_ID")
    private Long orgUnitId;

    @Column(name = "M1")
    private Double m1;

    @Column(name = "M2")
    private Double m2;

    @Column(name = "M3")
    private Double m3;

    @Column(name = "M4")
    private Double m4;

    @Column(name = "M5")
    private Double m5;

    @Column(name = "M6")
    private Double m6;

    @Column(name = "M7")
    private Double m7;

    @Column(name = "M8")
    private Double m8;

    @Column(name = "M9")
    private Double m9;

    @Column(name = "M10")
    private Double m10;

    @Column(name = "M11")
    private Double m11;

    @Column(name = "M12")
    private Double m12;

    @Column(name = "FK_STRATEGY_ID")
    private Long strategyId;

    @Column(name = "STRATEGY_FK_PV_TYPE_ID")
    private Long strategyPvTypeId;

    @Column(name = "STRATEGY_PV_TYPE_TITLE")
    private String strategyPvTypeTitle;

    @Column(name = "STRATEGY_NAME")
    private String strategyName;

    @Column(name = "STRATEGY_TITLE")
    private String strategyTitle;

    @Column(name = "STRATEGY_IS_ROOT")
    private Boolean strategyIsRoot;

    @Column(name = "STRATEGY_INITIAL_PERCENTAGE")
    private Double strategyInitialPercentage;

    @Column(name = "STRATEGY_PERCENTAGE")
    private Double strategyPercentage;

    @Column(name = "STRATEGY_WEIGHT")
    private Long strategyWeight;

    @Column(name = "STRATEGY_VOLUME")
    private Long strategyVolume;

    @Column(name = "STRATEGY_PRIORITY")
    private Integer strategyPriority;

    @Column(name = "STRATEGY_IMPORTANCE")
    private Double strategyImportance;

    @Column(name = "STRATEGY_ACTUAL_START_DATE")
    private LocalDate strategyActualStartDate;

    @Column(name = "STRATEGY_ACTUAL_END_DATE")
    private LocalDate strategyActualEndDate;

    @Column(name = "STRATEGY_EXPIRATION_DATE")
    private LocalDate strategyExpirationDate;

    @Column(name = "STRATEGY_START_DATE")
    private LocalDate strategyStartDate;

    @Column(name = "STRATEGY_END_DATE")
    private LocalDate strategyEndDate;

    @Column(name = "STRATEGY_FISCAL_YEAR")
    private String strategyFiscalYear;

    @Column(name = "STRATEGY_LEVEL$")
    private Long strategyLevel;

    public LocalDate getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(LocalDate assignDate) {
        this.assignDate = assignDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getPvStateId() {
        return pvStateId;
    }

    public void setPvStateId(Long pvStateId) {
        this.pvStateId = pvStateId;
    }

    public String getPvStateTitle() {
        return pvStateTitle;
    }

    public void setPvStateTitle(String pvStateTitle) {
        this.pvStateTitle = pvStateTitle;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Double getM1() {
        return m1;
    }

    public void setM1(Double m1) {
        this.m1 = m1;
    }

    public Double getM2() {
        return m2;
    }

    public void setM2(Double m2) {
        this.m2 = m2;
    }

    public Double getM3() {
        return m3;
    }

    public void setM3(Double m3) {
        this.m3 = m3;
    }

    public Double getM4() {
        return m4;
    }

    public void setM4(Double m4) {
        this.m4 = m4;
    }

    public Double getM5() {
        return m5;
    }

    public void setM5(Double m5) {
        this.m5 = m5;
    }

    public Double getM6() {
        return m6;
    }

    public void setM6(Double m6) {
        this.m6 = m6;
    }

    public Double getM7() {
        return m7;
    }

    public void setM7(Double m7) {
        this.m7 = m7;
    }

    public Double getM8() {
        return m8;
    }

    public void setM8(Double m8) {
        this.m8 = m8;
    }

    public Double getM9() {
        return m9;
    }

    public void setM9(Double m9) {
        this.m9 = m9;
    }

    public Double getM10() {
        return m10;
    }

    public void setM10(Double m10) {
        this.m10 = m10;
    }

    public Double getM11() {
        return m11;
    }

    public void setM11(Double m11) {
        this.m11 = m11;
    }

    public Double getM12() {
        return m12;
    }

    public void setM12(Double m12) {
        this.m12 = m12;
    }

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public Long getStrategyPvTypeId() {
        return strategyPvTypeId;
    }

    public void setStrategyPvTypeId(Long strategyPvTypeId) {
        this.strategyPvTypeId = strategyPvTypeId;
    }

    public String getStrategyPvTypeTitle() {
        return strategyPvTypeTitle;
    }

    public void setStrategyPvTypeTitle(String strategyPvTypeTitle) {
        this.strategyPvTypeTitle = strategyPvTypeTitle;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public String getStrategyTitle() {
        return strategyTitle;
    }

    public void setStrategyTitle(String strategyTitle) {
        this.strategyTitle = strategyTitle;
    }

    public Boolean getStrategyIsRoot() {
        return strategyIsRoot;
    }

    public void setStrategyIsRoot(Boolean strategyIsRoot) {
        this.strategyIsRoot = strategyIsRoot;
    }

    public Double getStrategyInitialPercentage() {
        return strategyInitialPercentage;
    }

    public void setStrategyInitialPercentage(Double strategyInitialPercentage) {
        this.strategyInitialPercentage = strategyInitialPercentage;
    }

    public Double getStrategyPercentage() {
        return strategyPercentage;
    }

    public void setStrategyPercentage(Double strategyPercentage) {
        this.strategyPercentage = strategyPercentage;
    }

    public Long getStrategyWeight() {
        return strategyWeight;
    }

    public void setStrategyWeight(Long strategyWeight) {
        this.strategyWeight = strategyWeight;
    }

    public Long getStrategyVolume() {
        return strategyVolume;
    }

    public void setStrategyVolume(Long strategyVolume) {
        this.strategyVolume = strategyVolume;
    }

    public Integer getStrategyPriority() {
        return strategyPriority;
    }

    public void setStrategyPriority(Integer strategyPriority) {
        this.strategyPriority = strategyPriority;
    }

    public Double getStrategyImportance() {
        return strategyImportance;
    }

    public void setStrategyImportance(Double strategyImportance) {
        this.strategyImportance = strategyImportance;
    }

    public LocalDate getStrategyActualStartDate() {
        return strategyActualStartDate;
    }

    public void setStrategyActualStartDate(LocalDate strategyActualStartDate) {
        this.strategyActualStartDate = strategyActualStartDate;
    }

    public LocalDate getStrategyActualEndDate() {
        return strategyActualEndDate;
    }

    public void setStrategyActualEndDate(LocalDate strategyActualEndDate) {
        this.strategyActualEndDate = strategyActualEndDate;
    }

    public LocalDate getStrategyExpirationDate() {
        return strategyExpirationDate;
    }

    public void setStrategyExpirationDate(LocalDate strategyExpirationDate) {
        this.strategyExpirationDate = strategyExpirationDate;
    }

    public LocalDate getStrategyStartDate() {
        return strategyStartDate;
    }

    public void setStrategyStartDate(LocalDate strategyStartDate) {
        this.strategyStartDate = strategyStartDate;
    }

    public LocalDate getStrategyEndDate() {
        return strategyEndDate;
    }

    public void setStrategyEndDate(LocalDate strategyEndDate) {
        this.strategyEndDate = strategyEndDate;
    }

    public String getStrategyFiscalYear() {
        return strategyFiscalYear;
    }

    public void setStrategyFiscalYear(String strategyFiscalYear) {
        this.strategyFiscalYear = strategyFiscalYear;
    }

    public Long getStrategyLevel() {
        return strategyLevel;
    }

    public void setStrategyLevel(Long strategyLevel) {
        this.strategyLevel = strategyLevel;
    }
}
