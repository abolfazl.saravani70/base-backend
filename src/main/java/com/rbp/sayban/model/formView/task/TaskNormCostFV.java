/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.task;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.NormCostJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.task.Task;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TASK_NORM_COST_VIEW")
public class TaskNormCostFV extends EvalStateBaseEntity {

    @Column(name = "COUNT")
    private Long count;

    @Column(name = "MINIMUM")
    private Long minimum;

    @Column(name = "MAXIMUM")
    private Long maximum;

    @Column(name = "AVERAGE")
    private Long average;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "RATE")
    private Double rate;

    @Column(name = "PREDICATE")
    private Long predicate;

    @Column(name = "FK_ORG_CORTEX_ID")
    private Long cortexId;

    @Column(name = "FK_TASK_ID")
    private Long taskId;

    @Column(name = "TASK_FK_PV_TYPE_ID")
    private Long taskPvTypeId;

    @Column(name = "TASK_PV_TYPE_TITLE")
    private String taskPvTypeTitle;

    @Column(name = "TASK_NAME")
    private String taskName;

    @Column(name = "TASK_TITLE")
    private String taskTitle;

    @Column(name = "TASK_IS_ROOT")
    private Boolean taskIsRoot;

    @Column(name = "TASK_INITIAL_PERCENTAGE")
    private Double taskInitialPercentage;

    @Column(name = "TASK_PERCENTAGE")
    private Double taskPercentage;

    @Column(name = "TASK_WEIGHT")
    private Long taskWeight;

    @Column(name = "TASK_VOLUME")
    private Long taskVolume;

    @Column(name = "TASK_PRIORITY")
    private Integer taskPriority;

    @Column(name = "TASK_IMPORTANCE")
    private Double taskImportance;

    @Column(name = "TASK_ACTUAL_START_DATE")
    private LocalDate taskActualStartDate;

    @Column(name = "TASK_ACTUAL_END_DATE")
    private LocalDate taskActualEndDate;

    @Column(name = "TASK_EXPIRATION_DATE")
    private LocalDate taskExpirationDate;

    @Column(name = "TASK_START_DATE")
    private LocalDate taskStartDate;

    @Column(name = "TASK_END_DATE")
    private LocalDate taskEndDate;

    @Column(name = "TASK_FISCAL_YEAR")
    private String taskFiscalYear;

    @Column(name = "TASK_LEVEL$")
    private Long taskLevel;

    @Column(name = "FK_NORM_COST_ID")
    private Long normCostId;

    @Column(name ="NORM_COST_NAME" )
    private String normCostName;

    @Column (name="NORM_COST_TITLE")
    private String normCostTitle;

    @Column(name="NORM_COST_COST")
    private Long normCostCost;

    @Column(name="NORM_COST_RATE")
    private Long normCostRate;

    @Column (name="NORM_COST_PRIORITORY")
    private Integer normCostPrioritory;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getMinimum() {
        return minimum;
    }

    public void setMinimum(Long minimum) {
        this.minimum = minimum;
    }

    public Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        this.maximum = maximum;
    }

    public Long getAverage() {
        return average;
    }

    public void setAverage(Long average) {
        this.average = average;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Long getPredicate() {
        return predicate;
    }

    public void setPredicate(Long predicate) {
        this.predicate = predicate;
    }

    public Long getCortexId() {
        return cortexId;
    }

    public void setCortexId(Long cortexId) {
        this.cortexId = cortexId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTaskPvTypeId() {
        return taskPvTypeId;
    }

    public void setTaskPvTypeId(Long taskPvTypeId) {
        this.taskPvTypeId = taskPvTypeId;
    }

    public String getTaskPvTypeTitle() {
        return taskPvTypeTitle;
    }

    public void setTaskPvTypeTitle(String taskPvTypeTitle) {
        this.taskPvTypeTitle = taskPvTypeTitle;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public Boolean getTaskIsRoot() {
        return taskIsRoot;
    }

    public void setTaskIsRoot(Boolean taskIsRoot) {
        this.taskIsRoot = taskIsRoot;
    }

    public Double getTaskInitialPercentage() {
        return taskInitialPercentage;
    }

    public void setTaskInitialPercentage(Double taskInitialPercentage) {
        this.taskInitialPercentage = taskInitialPercentage;
    }

    public Double getTaskPercentage() {
        return taskPercentage;
    }

    public void setTaskPercentage(Double taskPercentage) {
        this.taskPercentage = taskPercentage;
    }

    public Long getTaskWeight() {
        return taskWeight;
    }

    public void setTaskWeight(Long taskWeight) {
        this.taskWeight = taskWeight;
    }

    public Long getTaskVolume() {
        return taskVolume;
    }

    public void setTaskVolume(Long taskVolume) {
        this.taskVolume = taskVolume;
    }

    public Integer getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
    }

    public Double getTaskImportance() {
        return taskImportance;
    }

    public void setTaskImportance(Double taskImportance) {
        this.taskImportance = taskImportance;
    }

    public LocalDate getTaskActualStartDate() {
        return taskActualStartDate;
    }

    public void setTaskActualStartDate(LocalDate taskActualStartDate) {
        this.taskActualStartDate = taskActualStartDate;
    }

    public LocalDate getTaskActualEndDate() {
        return taskActualEndDate;
    }

    public void setTaskActualEndDate(LocalDate taskActualEndDate) {
        this.taskActualEndDate = taskActualEndDate;
    }

    public LocalDate getTaskExpirationDate() {
        return taskExpirationDate;
    }

    public void setTaskExpirationDate(LocalDate taskExpirationDate) {
        this.taskExpirationDate = taskExpirationDate;
    }

    public LocalDate getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(LocalDate taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public LocalDate getTaskEndDate() {
        return taskEndDate;
    }

    public void setTaskEndDate(LocalDate taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    public String getTaskFiscalYear() {
        return taskFiscalYear;
    }

    public void setTaskFiscalYear(String taskFiscalYear) {
        this.taskFiscalYear = taskFiscalYear;
    }

    public Long getTaskLevel() {
        return taskLevel;
    }

    public void setTaskLevel(Long taskLevel) {
        this.taskLevel = taskLevel;
    }

    public Long getNormCostId() {
        return normCostId;
    }

    public void setNormCostId(Long normCostId) {
        this.normCostId = normCostId;
    }

    public String getNormCostName() {
        return normCostName;
    }

    public void setNormCostName(String normCostName) {
        this.normCostName = normCostName;
    }

    public String getNormCostTitle() {
        return normCostTitle;
    }

    public void setNormCostTitle(String normCostTitle) {
        this.normCostTitle = normCostTitle;
    }

    public Long getNormCostCost() {
        return normCostCost;
    }

    public void setNormCostCost(Long normCostCost) {
        this.normCostCost = normCostCost;
    }

    public Long getNormCostRate() {
        return normCostRate;
    }

    public void setNormCostRate(Long normCostRate) {
        this.normCostRate = normCostRate;
    }

    public Integer getNormCostPrioritory() {
        return normCostPrioritory;
    }

    public void setNormCostPrioritory(Integer normCostPrioritory) {
        this.normCostPrioritory = normCostPrioritory;
    }
}
