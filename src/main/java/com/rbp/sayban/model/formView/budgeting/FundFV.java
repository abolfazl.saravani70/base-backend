/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "FUND_VIEW")
public class FundFV extends BaseEntity {

    @Column(name = "FK_BUDGET_RESOURCES_ID")
    private Long budgetResourceId;

    @Column(name = "BUDGET_RESOURCES_NAME")
    private String budgetResourceName;

    @Column(name = "BUDGET_RESOURCES_TITLE")
    private String budgetResourceTitle;

    @Column(name = "BUDGET_RESOURCES_IS_ROOT")
    private Boolean budgetResourceIsRoot;

    @Column(name = "BUDGET_RESOURCES_INITIAL_PERCENTAGE")
    private Double budgetResourceInitialPercentage;

    @Column(name = "BUDGET_RESOURCES_PERCENTAGE")
    private Double budgetResourcePercentage;

    @Column(name = "BUDGET_RESOURCES_WEIGHT")
    private Long budgetResourceWeight;

    @Column(name = "BUDGET_RESOURCES_VOLUME")
    private Long budgetResourceVolume;

    @Column(name = "BUDGET_RESOURCES_PRIORITY")
    private Integer budgetResourcePriority;

    @Column(name = "BUDGET_RESOURCES_IMPORTANCE")
    private Double budgetResourceImportance;

    @Column(name = "BUDGET_RESOURC_ACTUAL_START_DATE")
    private LocalDate budgetResourceActualStartDate;

    @Column(name = "BUDGET_RESOURCES_ACTUAL_END_DATE")
    private LocalDate budgetResourceActualEndDate;

    @Column(name = "BUDGET_RESOURCES_EXPIRATION_DATE")
    private LocalDate budgetResourceExpirationDate;

    @Column(name = "BUDGET_RESOURCES_START_DATE")
    private LocalDate budgetResourceStartDate;

    @Column(name = "BUDGET_RESOURCES_END_DATE")
    private LocalDate budgetResourceEndDate;

    @Column(name = "BUDGET_RESOURCES_FISCAL_YEAR")
    private String budgetResourceFiscalYear;

    @Column(name = "BUDGET_RESOURCES_LEVEL$")
    private Long budgetResourceLevel;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "FK_POLICY_ID")
    private Long policyId;

    @Column(name = "POLICY_NUMBER")
    private String policyNumber;

    @Column(name = "POLICY_TITLE")
    private String policyTitle;

    @Column(name = "POLICY_SYS_POLICY_DATE")
    private LocalDate policyDate;

    @Column(name = "POLICY_REASON")
    private String policyReason;

    public Long getBudgetResourceId() {
        return budgetResourceId;
    }

    public void setBudgetResourceId(Long budgetResourceId) {
        this.budgetResourceId = budgetResourceId;
    }

    public String getBudgetResourceName() {
        return budgetResourceName;
    }

    public void setBudgetResourceName(String budgetResourceName) {
        this.budgetResourceName = budgetResourceName;
    }

    public String getBudgetResourceTitle() {
        return budgetResourceTitle;
    }

    public void setBudgetResourceTitle(String budgetResourceTitle) {
        this.budgetResourceTitle = budgetResourceTitle;
    }

    public Boolean getBudgetResourceIsRoot() {
        return budgetResourceIsRoot;
    }

    public void setBudgetResourceIsRoot(Boolean budgetResourceIsRoot) {
        this.budgetResourceIsRoot = budgetResourceIsRoot;
    }

    public Double getBudgetResourceInitialPercentage() {
        return budgetResourceInitialPercentage;
    }

    public void setBudgetResourceInitialPercentage(Double budgetResourceInitialPercentage) {
        this.budgetResourceInitialPercentage = budgetResourceInitialPercentage;
    }

    public Double getBudgetResourcePercentage() {
        return budgetResourcePercentage;
    }

    public void setBudgetResourcePercentage(Double budgetResourcePercentage) {
        this.budgetResourcePercentage = budgetResourcePercentage;
    }

    public Long getBudgetResourceWeight() {
        return budgetResourceWeight;
    }

    public void setBudgetResourceWeight(Long budgetResourceWeight) {
        this.budgetResourceWeight = budgetResourceWeight;
    }

    public Long getBudgetResourceVolume() {
        return budgetResourceVolume;
    }

    public void setBudgetResourceVolume(Long budgetResourceVolume) {
        this.budgetResourceVolume = budgetResourceVolume;
    }

    public Integer getBudgetResourcePriority() {
        return budgetResourcePriority;
    }

    public void setBudgetResourcePriority(Integer budgetResourcePriority) {
        this.budgetResourcePriority = budgetResourcePriority;
    }

    public Double getBudgetResourceImportance() {
        return budgetResourceImportance;
    }

    public void setBudgetResourceImportance(Double budgetResourceImportance) {
        this.budgetResourceImportance = budgetResourceImportance;
    }

    public LocalDate getBudgetResourceActualStartDate() {
        return budgetResourceActualStartDate;
    }

    public void setBudgetResourceActualStartDate(LocalDate budgetResourceActualStartDate) {
        this.budgetResourceActualStartDate = budgetResourceActualStartDate;
    }

    public LocalDate getBudgetResourceActualEndDate() {
        return budgetResourceActualEndDate;
    }

    public void setBudgetResourceActualEndDate(LocalDate budgetResourceActualEndDate) {
        this.budgetResourceActualEndDate = budgetResourceActualEndDate;
    }

    public LocalDate getBudgetResourceExpirationDate() {
        return budgetResourceExpirationDate;
    }

    public void setBudgetResourceExpirationDate(LocalDate budgetResourceExpirationDate) {
        this.budgetResourceExpirationDate = budgetResourceExpirationDate;
    }

    public LocalDate getBudgetResourceStartDate() {
        return budgetResourceStartDate;
    }

    public void setBudgetResourceStartDate(LocalDate budgetResourceStartDate) {
        this.budgetResourceStartDate = budgetResourceStartDate;
    }

    public LocalDate getBudgetResourceEndDate() {
        return budgetResourceEndDate;
    }

    public void setBudgetResourceEndDate(LocalDate budgetResourceEndDate) {
        this.budgetResourceEndDate = budgetResourceEndDate;
    }

    public String getBudgetResourceFiscalYear() {
        return budgetResourceFiscalYear;
    }

    public void setBudgetResourceFiscalYear(String budgetResourceFiscalYear) {
        this.budgetResourceFiscalYear = budgetResourceFiscalYear;
    }

    public Long getBudgetResourceLevel() {
        return budgetResourceLevel;
    }

    public void setBudgetResourceLevel(Long budgetResourceLevel) {
        this.budgetResourceLevel = budgetResourceLevel;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyTitle() {
        return policyTitle;
    }

    public void setPolicyTitle(String policyTitle) {
        this.policyTitle = policyTitle;
    }

    public LocalDate getPolicyDate() {
        return policyDate;
    }

    public void setPolicyDate(LocalDate policyDate) {
        this.policyDate = policyDate;
    }

    public String getPolicyReason() {
        return policyReason;
    }

    public void setPolicyReason(String policyReason) {
        this.policyReason = policyReason;
    }
}
