/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Zone;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ADDRESS_VIEW")
public class AddressFV extends BaseEntity {

    @Column(name = "ADDRESS_TEXT")
    private String addressText;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "STREET")
    private String street;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "FK_STAKEHOLDERS_ID")
    private Long stakeholderId;

    @Column(name = "STAKEHOLDERS_TYPE")
    private String stakeholderType;

    @Column(name = "STAKEHOLDERS_IS_PERSON")
    private Boolean stakeholderIsPerson;

    @Column(name = "FK_ZONE_ID")
    private Zone zoneId;

    @Column(name = "ZONE_NAME")
    private String zoneName;

    @Column(name = "ZONE_NUMBER$")
    private String zoneNumber;

    @Column(name = "ZONE_TITLE")
    private String zoneTitle;

    public String getAddressText() {
        return addressText;
    }

    public void setAddressText(String addressText) {
        this.addressText = addressText;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(Long stakeholderId) {
        this.stakeholderId = stakeholderId;
    }

    public String getStakeholderType() {
        return stakeholderType;
    }

    public void setStakeholderType(String stakeholderType) {
        this.stakeholderType = stakeholderType;
    }

    public Boolean getStakeholderIsPerson() {
        return stakeholderIsPerson;
    }

    public void setStakeholderIsPerson(Boolean stakeholderIsPerson) {
        this.stakeholderIsPerson = stakeholderIsPerson;
    }

    public Zone getZoneId() {
        return zoneId;
    }

    public void setZoneId(Zone zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getZoneNumber() {
        return zoneNumber;
    }

    public void setZoneNumber(String zoneNumber) {
        this.zoneNumber = zoneNumber;
    }

    public String getZoneTitle() {
        return zoneTitle;
    }

    public void setZoneTitle(String zoneTitle) {
        this.zoneTitle = zoneTitle;
    }
}
