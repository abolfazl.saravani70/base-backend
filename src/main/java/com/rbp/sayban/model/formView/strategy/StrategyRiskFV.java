/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.strategy;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.RiskJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.strategies.Strategy;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "STRATEGY_RISK_VIEW")
public class StrategyRiskFV extends EvalStateBaseEntity {

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "ACTIVATED")
    private Boolean isActivated;

    @Column(name = "FK_STRATEGY_ID")
    private Long strategyId;

    @Column(name = "STRATEGY_FK_PV_TYPE_ID")
    private Long strategyPvTypeId;

    @Column(name = "STRATEGY_PV_TYPE_TITLE")
    private String strategyPvTypeTitle;

    @Column(name = "STRATEGY_NAME")
    private String strategyName;

    @Column(name = "STRATEGY_TITLE")
    private String strategyTitle;

    @Column(name = "STRATEGY_IS_ROOT")
    private Boolean strategyIsRoot;

    @Column(name = "STRATEGY_INITIAL_PERCENTAGE")
    private Double strategyInitialPercentage;

    @Column(name = "STRATEGY_PERCENTAGE")
    private Double strategyPercentage;

    @Column(name = "STRATEGY_WEIGHT")
    private Long strategyWeight;

    @Column(name = "STRATEGY_VOLUME")
    private Long strategyVolume;

    @Column(name = "STRATEGY_PRIORITY")
    private Integer strategyPriority;

    @Column(name = "STRATEGY_IMPORTANCE")
    private Double strategyImportance;

    @Column(name = "STRATEGY_ACTUAL_START_DATE")
    private LocalDate strategyActualStartDate;

    @Column(name = "STRATEGY_ACTUAL_END_DATE")
    private LocalDate strategyActualEndDate;

    @Column(name = "STRATEGY_EXPIRATION_DATE")
    private LocalDate strategyExpirationDate;

    @Column(name = "STRATEGY_START_DATE")
    private LocalDate strategyStartDate;

    @Column(name = "STRATEGY_END_DATE")
    private LocalDate strategyEndDate;

    @Column(name = "STRATEGY_FISCAL_YEAR")
    private String strategyFiscalYear;

    @Column(name = "STRATEGY_LEVEL$")
    private Long strategyLevel;

    @Column(name = "FK_RISK_ID")
    private Long riskId;

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getActivated() {
        return isActivated;
    }

    public void setActivated(Boolean activated) {
        isActivated = activated;
    }

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public Long getStrategyPvTypeId() {
        return strategyPvTypeId;
    }

    public void setStrategyPvTypeId(Long strategyPvTypeId) {
        this.strategyPvTypeId = strategyPvTypeId;
    }

    public String getStrategyPvTypeTitle() {
        return strategyPvTypeTitle;
    }

    public void setStrategyPvTypeTitle(String strategyPvTypeTitle) {
        this.strategyPvTypeTitle = strategyPvTypeTitle;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public String getStrategyTitle() {
        return strategyTitle;
    }

    public void setStrategyTitle(String strategyTitle) {
        this.strategyTitle = strategyTitle;
    }

    public Boolean getStrategyIsRoot() {
        return strategyIsRoot;
    }

    public void setStrategyIsRoot(Boolean strategyIsRoot) {
        this.strategyIsRoot = strategyIsRoot;
    }

    public Double getStrategyInitialPercentage() {
        return strategyInitialPercentage;
    }

    public void setStrategyInitialPercentage(Double strategyInitialPercentage) {
        this.strategyInitialPercentage = strategyInitialPercentage;
    }

    public Double getStrategyPercentage() {
        return strategyPercentage;
    }

    public void setStrategyPercentage(Double strategyPercentage) {
        this.strategyPercentage = strategyPercentage;
    }

    public Long getStrategyWeight() {
        return strategyWeight;
    }

    public void setStrategyWeight(Long strategyWeight) {
        this.strategyWeight = strategyWeight;
    }

    public Long getStrategyVolume() {
        return strategyVolume;
    }

    public void setStrategyVolume(Long strategyVolume) {
        this.strategyVolume = strategyVolume;
    }

    public Integer getStrategyPriority() {
        return strategyPriority;
    }

    public void setStrategyPriority(Integer strategyPriority) {
        this.strategyPriority = strategyPriority;
    }

    public Double getStrategyImportance() {
        return strategyImportance;
    }

    public void setStrategyImportance(Double strategyImportance) {
        this.strategyImportance = strategyImportance;
    }

    public LocalDate getStrategyActualStartDate() {
        return strategyActualStartDate;
    }

    public void setStrategyActualStartDate(LocalDate strategyActualStartDate) {
        this.strategyActualStartDate = strategyActualStartDate;
    }

    public LocalDate getStrategyActualEndDate() {
        return strategyActualEndDate;
    }

    public void setStrategyActualEndDate(LocalDate strategyActualEndDate) {
        this.strategyActualEndDate = strategyActualEndDate;
    }

    public LocalDate getStrategyExpirationDate() {
        return strategyExpirationDate;
    }

    public void setStrategyExpirationDate(LocalDate strategyExpirationDate) {
        this.strategyExpirationDate = strategyExpirationDate;
    }

    public LocalDate getStrategyStartDate() {
        return strategyStartDate;
    }

    public void setStrategyStartDate(LocalDate strategyStartDate) {
        this.strategyStartDate = strategyStartDate;
    }

    public LocalDate getStrategyEndDate() {
        return strategyEndDate;
    }

    public void setStrategyEndDate(LocalDate strategyEndDate) {
        this.strategyEndDate = strategyEndDate;
    }

    public String getStrategyFiscalYear() {
        return strategyFiscalYear;
    }

    public void setStrategyFiscalYear(String strategyFiscalYear) {
        this.strategyFiscalYear = strategyFiscalYear;
    }

    public Long getStrategyLevel() {
        return strategyLevel;
    }

    public void setStrategyLevel(Long strategyLevel) {
        this.strategyLevel = strategyLevel;
    }

    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }
}
