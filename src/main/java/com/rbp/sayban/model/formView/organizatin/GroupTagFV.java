/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.sayban.model.domainmodel.system.Tag;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "GROUP_TAG_VIEW")
public class GroupTagFV extends BaseEntity {

    @Column(name = "FK_GROUP_ID")
    private Long groupId;

    @Column(name = "GROUP_NAME")
    private String groupName;

    @Column(name = "GROUP_PERSIAN_NAME")
    private String groupPersianName;

    @Column(name = "FK_TAG_ID")
    private Long tagId;

    @Column(name = "TAG_NAME")
    private String tagName;

    @Column(name = "TAG_TITLE")
    private String tagTitle;

    @Column(name = "TAG_KEYWORD")
    private String tagKeyWord;

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupPersianName() {
        return groupPersianName;
    }

    public void setGroupPersianName(String groupPersianName) {
        this.groupPersianName = groupPersianName;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagTitle() {
        return tagTitle;
    }

    public void setTagTitle(String tagTitle) {
        this.tagTitle = tagTitle;
    }

    public String getTagKeyWord() {
        return tagKeyWord;
    }

    public void setTagKeyWord(String tagKeyWord) {
        this.tagKeyWord = tagKeyWord;
    }
}
