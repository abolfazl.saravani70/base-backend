/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.report.FilterOperator;
import com.rbp.sayban.model.domainmodel.report.GeneralReportFilter;
import com.rbp.sayban.model.domainmodel.system.System;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "SYSTEM_REPORT_FILTER_VIEW")
public class SystemReportFilterFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "QUERY")
    private String query;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "DEFAULT_VALUE")
    private String defualtValue;

    @Column(name = "FK_FILTER_OPERATOR_ID")
    private Long filterOperatorId;

    @Column(name = "FILTER_OPERATOR_NAME")
    private String filterOperatorName;

    @Column(name = "FILTER_OPERATOR_TITLE")
    private String filterOperatorTitle;

    @Column(name = "FILTER_OPERATOR_ABBR")
    private String filterOperatorAbbr;

    @Column(name = "FILTER_OPERATOR_SIGN")
    private String filterOperatorSign;

    @Column(name = "FILTER_OPERATOR_PRIORITY")
    private Integer filterOperatorPriority;

    @Column(name = "FK_SYSTEM_ID")
    private Long systemid;

    @Column(name = "SYSTEM_TYPE")
    private String systemType;

    @Column(name = "SYSTEM_TITLE")
    private String systemTitle;

    @Column(name = "SYSTEM_SYSTEM_VERSION")
    private String systemSystemVersion;

    @Column(name = "SYSTEM_NAME")
    private  String systemName;

    @Column(name = "SYSTEM_CODE")
    private  Integer systemCode;

    @Column(name = "SYSTEM_KEY")
    private String systemKey;

    @Column(name = "SYSTEM_DB")
    private  String systemDb;

    @Column(name = "SYSTEM_BASE_CONFIG")
    private String systemBaseConfig;

    @Column(name = "SYSTEM_HOST")
    private String systemHost;

    @Column(name = "SYSTEM_API")
    private String systemApi;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDefualtValue() {
        return defualtValue;
    }

    public void setDefualtValue(String defualtValue) {
        this.defualtValue = defualtValue;
    }

    public Long getFilterOperatorId() {
        return filterOperatorId;
    }

    public void setFilterOperatorId(Long filterOperatorId) {
        this.filterOperatorId = filterOperatorId;
    }

    public String getFilterOperatorName() {
        return filterOperatorName;
    }

    public void setFilterOperatorName(String filterOperatorName) {
        this.filterOperatorName = filterOperatorName;
    }

    public String getFilterOperatorTitle() {
        return filterOperatorTitle;
    }

    public void setFilterOperatorTitle(String filterOperatorTitle) {
        this.filterOperatorTitle = filterOperatorTitle;
    }

    public String getFilterOperatorAbbr() {
        return filterOperatorAbbr;
    }

    public void setFilterOperatorAbbr(String filterOperatorAbbr) {
        this.filterOperatorAbbr = filterOperatorAbbr;
    }

    public String getFilterOperatorSign() {
        return filterOperatorSign;
    }

    public void setFilterOperatorSign(String filterOperatorSign) {
        this.filterOperatorSign = filterOperatorSign;
    }

    public Integer getFilterOperatorPriority() {
        return filterOperatorPriority;
    }

    public void setFilterOperatorPriority(Integer filterOperatorPriority) {
        this.filterOperatorPriority = filterOperatorPriority;
    }

    public Long getSystemid() {
        return systemid;
    }

    public void setSystemid(Long systemid) {
        this.systemid = systemid;
    }

    public String getSystemType() {
        return systemType;
    }

    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }

    public String getSystemTitle() {
        return systemTitle;
    }

    public void setSystemTitle(String systemTitle) {
        this.systemTitle = systemTitle;
    }

    public String getSystemSystemVersion() {
        return systemSystemVersion;
    }

    public void setSystemSystemVersion(String systemSystemVersion) {
        this.systemSystemVersion = systemSystemVersion;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public Integer getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(Integer systemCode) {
        this.systemCode = systemCode;
    }

    public String getSystemKey() {
        return systemKey;
    }

    public void setSystemKey(String systemKey) {
        this.systemKey = systemKey;
    }

    public String getSystemDb() {
        return systemDb;
    }

    public void setSystemDb(String systemDb) {
        this.systemDb = systemDb;
    }

    public String getSystemBaseConfig() {
        return systemBaseConfig;
    }

    public void setSystemBaseConfig(String systemBaseConfig) {
        this.systemBaseConfig = systemBaseConfig;
    }

    public String getSystemHost() {
        return systemHost;
    }

    public void setSystemHost(String systemHost) {
        this.systemHost = systemHost;
    }

    public String getSystemApi() {
        return systemApi;
    }

    public void setSystemApi(String systemApi) {
        this.systemApi = systemApi;
    }
}
