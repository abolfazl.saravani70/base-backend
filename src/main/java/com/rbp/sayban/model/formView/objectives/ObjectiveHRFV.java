package com.rbp.sayban.model.formView.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "OBJECTIVE_HR_VIEW")
public class ObjectiveHRFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_OBJECTIVE_ID")
    private Long objectiveId;

    @Column(name = "OBJECTIVE_FK_PV_TYPE_ID")
    private Long objectivePvTypeId;

    @Column(name = "OBJECTIVE_PV_TYPE_TITLE")
    private String objectivePvTypeTitle;

    @Column(name = "OBJECTIVE_NAME")
    private String objectiveName;

    @Column(name = "OBJECTIVE_TITLE")
    private String objectiveTitle;

    @Column(name = "OBJECTIVE_IS_ROOT")
    private Boolean objectiveIsRoot;

    @Column(name = "OBJECTIVE_INITIAL_PERCENTAGE")
    private Double objectiveInitialPercentage;

    @Column(name = "OBJECTIVE_PERCENTAGE")
    private Double objectivePercentage;

    @Column(name = "OBJECTIVE_WEIGHT")
    private Long objectiveWeight;

    @Column(name = "OBJECTIVE_VOLUME")
    private Long objectiveVolume;

    @Column(name = "OBJECTIVE_PRIORITY")
    private Integer objectivePriority;

    @Column(name = "OBJECTIVE_IMPORTANCE")
    private Double objectiveImportance;

    @Column(name = "OBJECTIVE_ACTUAL_START_DATE")
    private LocalDate objectiveActualStartDate;

    @Column(name = "OBJECTIVE_ACTUAL_END_DATE")
    private LocalDate objectiveActualEndDate;

    @Column(name = "OBJECTIVE_EXPIRATION_DATE")
    private LocalDate objectiveExpirationDate;

    @Column(name = "OBJECTIVE_START_DATE")
    private LocalDate objectiveStartDate;

    @Column(name = "OBJECTIVE_END_DATE")
    private LocalDate objectiveEndDate;

    @Column(name = "OBJECTIVE_FISCAL_YEAR")
    private String objectiveFiscalYear;

    @Column(name = "OBJECTIVE_LEVEL$")
    private Long objectiveLevel;

    @Column(name = "FK_HR_ID")
    private Long humanResourceId;

    @Column(name = "HR_TITLE")
    private String hRTitle;

    @Column(name = "HR_IS_ENABLE")
    private Boolean hRIsEnabled;

    @Column(name = "HR_FK_ORG_CHART_ID")
    private Long hROrganizationChartId;

    @Column(name = "HR_FK_ORG_ID")
    private Long hROrganizationId;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }

    public Long getObjectivePvTypeId() {
        return objectivePvTypeId;
    }

    public void setObjectivePvTypeId(Long objectivePvTypeId) {
        this.objectivePvTypeId = objectivePvTypeId;
    }

    public String getObjectivePvTypeTitle() {
        return objectivePvTypeTitle;
    }

    public void setObjectivePvTypeTitle(String objectivePvTypeTitle) {
        this.objectivePvTypeTitle = objectivePvTypeTitle;
    }

    public String getObjectiveName() {
        return objectiveName;
    }

    public void setObjectiveName(String objectiveName) {
        this.objectiveName = objectiveName;
    }

    public String getObjectiveTitle() {
        return objectiveTitle;
    }

    public void setObjectiveTitle(String objectiveTitle) {
        this.objectiveTitle = objectiveTitle;
    }

    public Boolean getObjectiveIsRoot() {
        return objectiveIsRoot;
    }

    public void setObjectiveIsRoot(Boolean objectiveIsRoot) {
        this.objectiveIsRoot = objectiveIsRoot;
    }

    public Double getObjectiveInitialPercentage() {
        return objectiveInitialPercentage;
    }

    public void setObjectiveInitialPercentage(Double objectiveInitialPercentage) {
        this.objectiveInitialPercentage = objectiveInitialPercentage;
    }

    public Double getObjectivePercentage() {
        return objectivePercentage;
    }

    public void setObjectivePercentage(Double objectivePercentage) {
        this.objectivePercentage = objectivePercentage;
    }

    public Long getObjectiveWeight() {
        return objectiveWeight;
    }

    public void setObjectiveWeight(Long objectiveWeight) {
        this.objectiveWeight = objectiveWeight;
    }

    public Long getObjectiveVolume() {
        return objectiveVolume;
    }

    public void setObjectiveVolume(Long objectiveVolume) {
        this.objectiveVolume = objectiveVolume;
    }

    public Integer getObjectivePriority() {
        return objectivePriority;
    }

    public void setObjectivePriority(Integer objectivePriority) {
        this.objectivePriority = objectivePriority;
    }

    public Double getObjectiveImportance() {
        return objectiveImportance;
    }

    public void setObjectiveImportance(Double objectiveImportance) {
        this.objectiveImportance = objectiveImportance;
    }

    public LocalDate getObjectiveActualStartDate() {
        return objectiveActualStartDate;
    }

    public void setObjectiveActualStartDate(LocalDate objectiveActualStartDate) {
        this.objectiveActualStartDate = objectiveActualStartDate;
    }

    public LocalDate getObjectiveActualEndDate() {
        return objectiveActualEndDate;
    }

    public void setObjectiveActualEndDate(LocalDate objectiveActualEndDate) {
        this.objectiveActualEndDate = objectiveActualEndDate;
    }

    public LocalDate getObjectiveExpirationDate() {
        return objectiveExpirationDate;
    }

    public void setObjectiveExpirationDate(LocalDate objectiveExpirationDate) {
        this.objectiveExpirationDate = objectiveExpirationDate;
    }

    public LocalDate getObjectiveStartDate() {
        return objectiveStartDate;
    }

    public void setObjectiveStartDate(LocalDate objectiveStartDate) {
        this.objectiveStartDate = objectiveStartDate;
    }

    public LocalDate getObjectiveEndDate() {
        return objectiveEndDate;
    }

    public void setObjectiveEndDate(LocalDate objectiveEndDate) {
        this.objectiveEndDate = objectiveEndDate;
    }

    public String getObjectiveFiscalYear() {
        return objectiveFiscalYear;
    }

    public void setObjectiveFiscalYear(String objectiveFiscalYear) {
        this.objectiveFiscalYear = objectiveFiscalYear;
    }

    public Long getObjectiveLevel() {
        return objectiveLevel;
    }

    public void setObjectiveLevel(Long objectiveLevel) {
        this.objectiveLevel = objectiveLevel;
    }

    public Long getHumanResourceId() {
        return humanResourceId;
    }

    public void setHumanResourceId(Long humanResourceId) {
        this.humanResourceId = humanResourceId;
    }

    public String gethRTitle() {
        return hRTitle;
    }

    public void sethRTitle(String hRTitle) {
        this.hRTitle = hRTitle;
    }

    public Boolean gethRIsEnabled() {
        return hRIsEnabled;
    }

    public void sethRIsEnabled(Boolean hRIsEnabled) {
        this.hRIsEnabled = hRIsEnabled;
    }

    public Long gethROrganizationChartId() {
        return hROrganizationChartId;
    }

    public void sethROrganizationChartId(Long hROrganizationChartId) {
        this.hROrganizationChartId = hROrganizationChartId;
    }

    public Long gethROrganizationId() {
        return hROrganizationId;
    }

    public void sethROrganizationId(Long hROrganizationId) {
        this.hROrganizationId = hROrganizationId;
    }
}
