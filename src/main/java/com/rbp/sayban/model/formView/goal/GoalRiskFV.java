/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.RiskJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.domainmodel.risk.Risk;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "GOAL_RISK_VIEW")
public class GoalRiskFV extends EvalStateBaseEntity {

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "ACTIVATED")
    private Boolean isActivated;

    @Column(name = "FK_RISK_ID")
    private Long riskId;

    @Column(name = "FK_GOAL_ID")
    private Long goalId;

    @Column(name = "GOAL_FK_PV_TYPE_ID")
    private Long goalPvTypeId;

    @Column(name = "GOAL_PV_TYPE_TITLE")
    private String goalPvTypeTitle;

    @Column(name = "GOAL_NAME")
    private String goalName;

    @Column(name = "GOAL_TITLE")
    private String goalTitle;

    @Column(name = "GOAL_IS_ROOT")
    private Boolean goalIsRoot;

    @Column(name = "GOAL_INITIAL_PERCENTAGE")
    private Double goalInitialPercentage;

    @Column(name = "GOAL_PERCENTAGE")
    private Double goalPercentage;

    @Column(name = "GOAL_WEIGHT")
    private Long goalWeight;

    @Column(name = "GOAL_VOLUME")
    private Long goalVolume;

    @Column(name = "GOAL_PRIORITY")
    private Integer goalPriority;

    @Column(name = "GOAL_IMPORTANCE")
    private Double goalImportance;

    @Column(name = "GOAL_ACTUAL_START_DATE")
    private LocalDate goalActualStartDate;

    @Column(name = "GOAL_ACTUAL_END_DATE")
    private LocalDate goalActualEndDate;

    @Column(name = "GOAL_EXPIRATION_DATE")
    private LocalDate goalExpirationDate;

    @Column(name = "GOAL_START_DATE")
    private LocalDate goalStartDate;

    @Column(name = "GOAL_END_DATE")
    private LocalDate goalEndDate;

    @Column(name = "GOAL_FISCAL_YEAR")
    private String goalFiscalYear;

    @Column(name = "GOAL_LEVEL$")
    private Long goalLevel;

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getActivated() {
        return isActivated;
    }

    public void setActivated(Boolean activated) {
        isActivated = activated;
    }

    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }

    public Long getGoalPvTypeId() {
        return goalPvTypeId;
    }

    public void setGoalPvTypeId(Long goalPvTypeId) {
        this.goalPvTypeId = goalPvTypeId;
    }

    public String getGoalPvTypeTitle() {
        return goalPvTypeTitle;
    }

    public void setGoalPvTypeTitle(String goalPvTypeTitle) {
        this.goalPvTypeTitle = goalPvTypeTitle;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getGoalTitle() {
        return goalTitle;
    }

    public void setGoalTitle(String goalTitle) {
        this.goalTitle = goalTitle;
    }

    public Boolean getGoalIsRoot() {
        return goalIsRoot;
    }

    public void setGoalIsRoot(Boolean goalIsRoot) {
        this.goalIsRoot = goalIsRoot;
    }

    public Double getGoalInitialPercentage() {
        return goalInitialPercentage;
    }

    public void setGoalInitialPercentage(Double goalInitialPercentage) {
        this.goalInitialPercentage = goalInitialPercentage;
    }

    public Double getGoalPercentage() {
        return goalPercentage;
    }

    public void setGoalPercentage(Double goalPercentage) {
        this.goalPercentage = goalPercentage;
    }

    public Long getGoalWeight() {
        return goalWeight;
    }

    public void setGoalWeight(Long goalWeight) {
        this.goalWeight = goalWeight;
    }

    public Long getGoalVolume() {
        return goalVolume;
    }

    public void setGoalVolume(Long goalVolume) {
        this.goalVolume = goalVolume;
    }

    public Integer getGoalPriority() {
        return goalPriority;
    }

    public void setGoalPriority(Integer goalPriority) {
        this.goalPriority = goalPriority;
    }

    public Double getGoalImportance() {
        return goalImportance;
    }

    public void setGoalImportance(Double goalImportance) {
        this.goalImportance = goalImportance;
    }

    public LocalDate getGoalActualStartDate() {
        return goalActualStartDate;
    }

    public void setGoalActualStartDate(LocalDate goalActualStartDate) {
        this.goalActualStartDate = goalActualStartDate;
    }

    public LocalDate getGoalActualEndDate() {
        return goalActualEndDate;
    }

    public void setGoalActualEndDate(LocalDate goalActualEndDate) {
        this.goalActualEndDate = goalActualEndDate;
    }

    public LocalDate getGoalExpirationDate() {
        return goalExpirationDate;
    }

    public void setGoalExpirationDate(LocalDate goalExpirationDate) {
        this.goalExpirationDate = goalExpirationDate;
    }

    public LocalDate getGoalStartDate() {
        return goalStartDate;
    }

    public void setGoalStartDate(LocalDate goalStartDate) {
        this.goalStartDate = goalStartDate;
    }

    public LocalDate getGoalEndDate() {
        return goalEndDate;
    }

    public void setGoalEndDate(LocalDate goalEndDate) {
        this.goalEndDate = goalEndDate;
    }

    public String getGoalFiscalYear() {
        return goalFiscalYear;
    }

    public void setGoalFiscalYear(String goalFiscalYear) {
        this.goalFiscalYear = goalFiscalYear;
    }

    public Long getGoalLevel() {
        return goalLevel;
    }

    public void setGoalLevel(Long goalLevel) {
        this.goalLevel = goalLevel;
    }
}
