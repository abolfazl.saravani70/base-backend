package com.rbp.sayban.model.formView.project;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.project.Project;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "PROJECT_LOCATION_VIEW")
public class ProjectLocationFV extends EvalStateBaseEntity {

    @Column(name = "CAPACITY")
    private Long capacity;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "USAGE_TYPE")
    private String usageType;

    @Column(name = "FK_PROJECT_ID")
    private Long projectId;

    @Column(name = "PROJECT_FK_PV_TYPE_ID")
    private Long projectPvTypeId;

    @Column(name = "PROJECT_PV_TYPE_TITLE")
    private String projectPvTypeTitle;

    @Column(name = "PROJECT_NAME")
    private String projectName;

    @Column(name = "PROJECT_TITLE")
    private String projectTitle;

    @Column(name = "PROJECT_IS_ROOT")
    private Boolean projectIsRoot;

    @Column(name = "PROJECT_INITIAL_PERCENTAGE")
    private Double projectInitialPercentage;

    @Column(name = "PROJECT_PERCENTAGE")
    private Double projectPercentage;

    @Column(name = "PROJECT_WEIGHT")
    private Long projectWeight;

    @Column(name = "PROJECT_VOLUME")
    private Long projectVolume;

    @Column(name = "PROJECT_PRIORITY")
    private Integer projectPriority;

    @Column(name = "PROJECT_IMPORTANCE")
    private Double projectImportance;

    @Column(name = "PROJECT_ACTUAL_START_DATE")
    private LocalDate projectActualStartDate;

    @Column(name = "PROJECT_ACTUAL_END_DATE")
    private LocalDate projectActualEndDate;

    @Column(name = "PROJECT_EXPIRATION_DATE")
    private LocalDate projectExpirationDate;

    @Column(name = "PROJECT_START_DATE")
    private LocalDate projectStartDate;

    @Column(name = "PROJECT_END_DATE")
    private LocalDate projectEndDate;

    @Column(name = "PROJECT_FISCAL_YEAR")
    private String projectFiscalYear;

    @Column(name = "PROJECT_LEVEL$")
    private Long projectLevel;

    @Column(name = "FK_LOCATION_ID")
    private Long locationId;

    @Column(name = "LOCATION_NAME")
    private String locationName;

    @Column(name = "LOCATION_NUMBER$")
    private String locationNumber;

    @Column(name = "LOCATION_TITLE")
    private String locationTitle;

    @Column(name = "LOCATION_Type")
    private String locationType;

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectPvTypeId() {
        return projectPvTypeId;
    }

    public void setProjectPvTypeId(Long projectPvTypeId) {
        this.projectPvTypeId = projectPvTypeId;
    }

    public String getProjectPvTypeTitle() {
        return projectPvTypeTitle;
    }

    public void setProjectPvTypeTitle(String projectPvTypeTitle) {
        this.projectPvTypeTitle = projectPvTypeTitle;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public Boolean getProjectIsRoot() {
        return projectIsRoot;
    }

    public void setProjectIsRoot(Boolean projectIsRoot) {
        this.projectIsRoot = projectIsRoot;
    }

    public Double getProjectInitialPercentage() {
        return projectInitialPercentage;
    }

    public void setProjectInitialPercentage(Double projectInitialPercentage) {
        this.projectInitialPercentage = projectInitialPercentage;
    }

    public Double getProjectPercentage() {
        return projectPercentage;
    }

    public void setProjectPercentage(Double projectPercentage) {
        this.projectPercentage = projectPercentage;
    }

    public Long getProjectWeight() {
        return projectWeight;
    }

    public void setProjectWeight(Long projectWeight) {
        this.projectWeight = projectWeight;
    }

    public Long getProjectVolume() {
        return projectVolume;
    }

    public void setProjectVolume(Long projectVolume) {
        this.projectVolume = projectVolume;
    }

    public Integer getProjectPriority() {
        return projectPriority;
    }

    public void setProjectPriority(Integer projectPriority) {
        this.projectPriority = projectPriority;
    }

    public Double getProjectImportance() {
        return projectImportance;
    }

    public void setProjectImportance(Double projectImportance) {
        this.projectImportance = projectImportance;
    }

    public LocalDate getProjectActualStartDate() {
        return projectActualStartDate;
    }

    public void setProjectActualStartDate(LocalDate projectActualStartDate) {
        this.projectActualStartDate = projectActualStartDate;
    }

    public LocalDate getProjectActualEndDate() {
        return projectActualEndDate;
    }

    public void setProjectActualEndDate(LocalDate projectActualEndDate) {
        this.projectActualEndDate = projectActualEndDate;
    }

    public LocalDate getProjectExpirationDate() {
        return projectExpirationDate;
    }

    public void setProjectExpirationDate(LocalDate projectExpirationDate) {
        this.projectExpirationDate = projectExpirationDate;
    }

    public LocalDate getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(LocalDate projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public LocalDate getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(LocalDate projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectFiscalYear() {
        return projectFiscalYear;
    }

    public void setProjectFiscalYear(String projectFiscalYear) {
        this.projectFiscalYear = projectFiscalYear;
    }

    public Long getProjectLevel() {
        return projectLevel;
    }

    public void setProjectLevel(Long projectLevel) {
        this.projectLevel = projectLevel;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public String getLocationTitle() {
        return locationTitle;
    }

    public void setLocationTitle(String locationTitle) {
        this.locationTitle = locationTitle;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
}
