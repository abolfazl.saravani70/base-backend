/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.kpi;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "UNIT_COST_VIEW")
public class UnitCostFV extends BaseEntity {

    @Column(name = "KEY")
    private String key;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "IS_OBSOLETE")
    private Boolean isObsolete;

    @Column(name = "CODE")
    private Integer code;

    @Column(name = "FK_UNIT_ID")
    private Long unitId;

    @Column(name = "UNIT_KEY_NAME")
    private String unitKeyName;

    @Column(name = "UNIT_KEY_VALUE")
    private String unitKeyValue;

    @Column(name = "UNIT_RATE")
    private Integer unitRate;

    @Column(name = "UNIT_PERCENT")
    private Double unitPercent;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getObsolete() {
        return isObsolete;
    }

    public void setObsolete(Boolean obsolete) {
        isObsolete = obsolete;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getUnitKeyName() {
        return unitKeyName;
    }

    public void setUnitKeyName(String unitKeyName) {
        this.unitKeyName = unitKeyName;
    }

    public String getUnitKeyValue() {
        return unitKeyValue;
    }

    public void setUnitKeyValue(String unitKeyValue) {
        this.unitKeyValue = unitKeyValue;
    }

    public Integer getUnitRate() {
        return unitRate;
    }

    public void setUnitRate(Integer unitRate) {
        this.unitRate = unitRate;
    }

    public Double getUnitPercent() {
        return unitPercent;
    }

    public void setUnitPercent(Double unitPercent) {
        this.unitPercent = unitPercent;
    }
}
