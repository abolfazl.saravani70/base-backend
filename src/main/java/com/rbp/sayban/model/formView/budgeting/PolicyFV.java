/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.Document;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "POLICY_VIEW")
public class PolicyFV extends BaseEntity {

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "SYS_POLICY_DATE")
    private LocalDate date;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "FK_DOCUMENT_ID")
    private Long documentId;

    @Column(name = "DOCUMENT_IS_TEMP")
    private Boolean documentIsTemp;

    @Column(name = "DOCUMENT_NAME")
    private String documentName;

    @Column(name = "DOCUMENT_TITLE")
    private String documentTitle;

    @Column(name = "DOCUMENT_NUMBER$")
    private String documentNumber;

    @Column(name = "DOCUMENT_FK_PV_DOC_STATE_ID")
    private Long documentPvDocumentStateId;

    @Column(name = "DOCUMENT_PV_DOC_STATE_TITLE")
    private String documentPvDocumentStateTitle;

    @Column(name = "DOCUMENT_FK_PV_TYPE_ID")
    private Long documentPvTypeId;

    @Column(name = "DOCUMENT_PV_TYPE_TITLE")
    private String documentPvTypeTitle;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Boolean getDocumentIsTemp() {
        return documentIsTemp;
    }

    public void setDocumentIsTemp(Boolean documentIsTemp) {
        this.documentIsTemp = documentIsTemp;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Long getDocumentPvDocumentStateId() {
        return documentPvDocumentStateId;
    }

    public void setDocumentPvDocumentStateId(Long documentPvDocumentStateId) {
        this.documentPvDocumentStateId = documentPvDocumentStateId;
    }

    public String getDocumentPvDocumentStateTitle() {
        return documentPvDocumentStateTitle;
    }

    public void setDocumentPvDocumentStateTitle(String documentPvDocumentStateTitle) {
        this.documentPvDocumentStateTitle = documentPvDocumentStateTitle;
    }

    public Long getDocumentPvTypeId() {
        return documentPvTypeId;
    }

    public void setDocumentPvTypeId(Long documentPvTypeId) {
        this.documentPvTypeId = documentPvTypeId;
    }

    public String getDocumentPvTypeTitle() {
        return documentPvTypeTitle;
    }

    public void setDocumentPvTypeTitle(String documentPvTypeTitle) {
        this.documentPvTypeTitle = documentPvTypeTitle;
    }
}
