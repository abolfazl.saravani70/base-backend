/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.FundSharing;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "ACCEPTED_CREDIT_VIEW")
public class AcceptedCreditFV extends BaseEntity{

    @Column(name = "LIMIT_AMOUNT")
    private Long limitAmount;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "START_DATE")
    private LocalDate startDate;

    @Column(name = "END_DATE")
    private LocalDate endDate;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "LETTER_NUMBER")
    private String letterNumber;

    @Column(name = "FK_FUND_SHARING_ID")
    private Long fundSharingId;

    @Column(name = "FUND_SHARING_ACCEPTED_AMOUNT")
    private Long fundSharingAcceptedAmount;

    @Column(name = "FUND_SHARING_ACCEPTED_DATE")
    private LocalDate fundSharingAcceptedDate;

    public Long getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(Long limitAmount) {
        this.limitAmount = limitAmount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLetterNumber() {
        return letterNumber;
    }

    public void setLetterNumber(String letterNumber) {
        this.letterNumber = letterNumber;
    }

    public Long getFundSharingId() {
        return fundSharingId;
    }

    public void setFundSharingId(Long fundSharingId) {
        this.fundSharingId = fundSharingId;
    }

    public Long getFundSharingAcceptedAmount() {
        return fundSharingAcceptedAmount;
    }

    public void setFundSharingAcceptedAmount(Long fundSharingAcceptedAmount) {
        this.fundSharingAcceptedAmount = fundSharingAcceptedAmount;
    }

    public LocalDate getFundSharingAcceptedDate() {
        return fundSharingAcceptedDate;
    }

    public void setFundSharingAcceptedDate(LocalDate fundSharingAcceptedDate) {
        this.fundSharingAcceptedDate = fundSharingAcceptedDate;
    }
}
