/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.SysRule;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "SYS_RULE_FORMULA_VIEW")
public class SysRuleFormulaFV extends BaseEntity {

    @Column(name = "KEY")
    private String key;

    @Column(name = "VALUE")
    private String value;
    // TODO: 07/12/2019
    @Column(name = "FK_SYS_RULE_ID")
    private Long sysRuleId;

    @Column(name = "SYS_RULE_TITLE")
    private String sysRuleTitle;

    @Column(name = "SYS_RULE_NAME")
    private String sysRuleName;

    @Column(name = "SYS_RULE_CODE")
    private String sysRuleCode;

    @Column(name = "SYS_RULE_START_DATE")
    private LocalDate sysRuleStartDate;

    @Column(name = "SYS_RULE_EXPIRATION_DATE")
    private LocalDate sysRuleExpirationDate;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getSysRuleId() {
        return sysRuleId;
    }

    public void setSysRuleId(Long sysRuleId) {
        this.sysRuleId = sysRuleId;
    }

    public String getSysRuleTitle() {
        return sysRuleTitle;
    }

    public void setSysRuleTitle(String sysRuleTitle) {
        this.sysRuleTitle = sysRuleTitle;
    }

    public String getSysRuleName() {
        return sysRuleName;
    }

    public void setSysRuleName(String sysRuleName) {
        this.sysRuleName = sysRuleName;
    }

    public String getSysRuleCode() {
        return sysRuleCode;
    }

    public void setSysRuleCode(String sysRuleCode) {
        this.sysRuleCode = sysRuleCode;
    }

    public LocalDate getSysRuleStartDate() {
        return sysRuleStartDate;
    }

    public void setSysRuleStartDate(LocalDate sysRuleStartDate) {
        this.sysRuleStartDate = sysRuleStartDate;
    }

    public LocalDate getSysRuleExpirationDate() {
        return sysRuleExpirationDate;
    }

    public void setSysRuleExpirationDate(LocalDate sysRuleExpirationDate) {
        this.sysRuleExpirationDate = sysRuleExpirationDate;
    }
}
