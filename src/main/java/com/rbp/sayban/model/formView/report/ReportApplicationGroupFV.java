/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.sayban.model.domainmodel.report.ReportApplication;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "REPORT_APPLICATION_GROUP_VIEW")
public class ReportApplicationGroupFV extends BaseEntity {

//    @Column(name = "FK_REPORT_FILTER_ID")
//    private Long reportApplicationId;
//
//    @Column(name = "FK_GROUP_ID")
//    private Long groupId;
//
//    @Column(name = "GROUP_NAME")
//    private String groupName;
//
//    @Column(name = "GROUP_PERSIAN_NAME")
//    private String groupPersianName;
//
//    public Long getReportApplicationId() {
//        return reportApplicationId;
//    }
//
//    public void setReportApplicationId(Long reportApplicationId) {
//        this.reportApplicationId = reportApplicationId;
//    }
//
//    public Long getGroupId() {
//        return groupId;
//    }
//
//    public void setGroupId(Long groupId) {
//        this.groupId = groupId;
//    }
//
//    public String getGroupName() {
//        return groupName;
//    }
//
//    public void setGroupName(String groupName) {
//        this.groupName = groupName;
//    }
//
//    public String getGroupPersianName() {
//        return groupPersianName;
//    }
//
//    public void setGroupPersianName(String groupPersianName) {
//        this.groupPersianName = groupPersianName;
//    }
}
