package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "HUMAN_RESOURCE_VIEW")
public class HumanResourceFV extends BaseEntity {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "IS_ENABLE")
    private Boolean isEnabled;

    @Column(name = "FK_ORG_CHART_ID")
    private Long organizationChartId;

    @Column(name = "FK_ORG_ID")
    private Long organizationId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getEnable() {
        return isEnabled;
    }

    public void setEnable(Boolean enable) {
        isEnabled = enable;
    }

    public Long getOrganizationChartId() {
        return organizationChartId;
    }

    public void setOrganizationChartId(Long organizationChartId) {
        this.organizationChartId = organizationChartId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
}
