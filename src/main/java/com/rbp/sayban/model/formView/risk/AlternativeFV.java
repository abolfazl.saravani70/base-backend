/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.risk;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.risk.Solution;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ALTERNATIVE_VIEW")
public class AlternativeFV extends BaseEntity {

    @Column(name = "FK_SOLUTION_ID")
    private Long solutionId;

    @Column(name = "NAME")
    private String solutionName;

    @Column(name = "CODE")
    private String solutionCode;

    @Column(name = "TEXT")
    private String solutionText;

    @Column(name = "PRIORITY")
    private Integer solutionPriority;

    public Long getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(Long solutionId) {
        this.solutionId = solutionId;
    }

    public String getSolutionName() {
        return solutionName;
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public String getSolutionCode() {
        return solutionCode;
    }

    public void setSolutionCode(String solutionCode) {
        this.solutionCode = solutionCode;
    }

    public String getSolutionText() {
        return solutionText;
    }

    public void setSolutionText(String solutionText) {
        this.solutionText = solutionText;
    }

    public Integer getSolutionPriority() {
        return solutionPriority;
    }

    public void setSolutionPriority(Integer solutionPriority) {
        this.solutionPriority = solutionPriority;
    }
}
