/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.base.MonthsPercentage;
import com.rbp.sayban.model.domainmodel.budgeting.Allocation;
import com.rbp.sayban.model.domainmodel.planning.PlanOrganizationUnit;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "PLN_ORG_UNIT_ALLOCATION_VIEW")
public class PlanOrganizationUnitAllocationFV extends BaseEntity {

    @Column(name = "FK_PLAN_ORG_UNIT_ID")
    private Long planOrganizationUnitId;

    @Column(name = "PLAN_ORG_UNIT_ASSIGN_DATE")
    private LocalDate planOrganizationUnitAssignDate;

    @Column(name = "PLAN_ORG_UNIT_DUE_DATE")
    private LocalDate planOrganizationUnitDueDate;

    @Column(name = "PLAN_ORG_UNIT_FK_PV_STATE_ID")
    private Long planOrganizationUnitPvStateId;

    @Column(name = "PLAN_ORG_UNIT_PV_STATE_TITLE")
    private String planOrganizationUnitPvStateTitle;

    @Column(name = "PLAN_ORG_UNIT_FK_PV_TYPE_ID")
    private Long planOrganizationUnitPvTypeId;

    @Column(name = "PLAN_ORG_UNIT_PV_TYPE_TITLE")
    private String planOrganizationUnitPvTypeTitle;

    @Column(name = "PLAN_ORG_UNIT_FK_ORG_UNIT_ID")
    private Long planOrganizationUnitOrgUnitId;

    @Column(name = "PLAN_ORG_UNIT_M1")
    private Double planOrganizationUnitM1;

    @Column(name = "PLAN_ORG_UNIT_M2")
    private Double planOrganizationUnitM2;

    @Column(name = "PLAN_ORG_UNIT_M3")
    private Double planOrganizationUnitM3;

    @Column(name = "PLAN_ORG_UNIT_M4")
    private Double planOrganizationUnitM4;

    @Column(name = "PLAN_ORG_UNIT_M5")
    private Double planOrganizationUnitM5;

    @Column(name = "PLAN_ORG_UNIT_M6")
    private Double planOrganizationUnitM6;

    @Column(name = "PLAN_ORG_UNIT_M7")
    private Double planOrganizationUnitM7;

    @Column(name = "PLAN_ORG_UNIT_M8")
    private Double planOrganizationUnitM8;

    @Column(name = "PLAN_ORG_UNIT_M9")
    private Double planOrganizationUnitM9;

    @Column(name = "PLAN_ORG_UNIT_M10")
    private Double planOrganizationUnitM10;

    @Column(name = "PLAN_ORG_UNIT_M11")
    private Double planOrganizationUnitM11;

    @Column(name = "PLAN_ORG_UNIT_M12")
    private Double planOrganizationUnitM12;

    @Column(name = "PLAN_ORG_UNIT_NAME")
    private String planOrganizationUnitName;

    @Column(name = "PlanOrgUnit_TITLE")
    private String planOrganizationUnitTitle;

    @Column(name = "PLAN_ORG_UNIT_IS_ROOT")
    private Boolean planOrganizationUnitIsRoot;

    @Column(name = "PLAN_ORG_UNIT_INITIAL_PERCENTAGE")
    private Double planOrganizationUnitInitialPercentage;

    @Column(name = "PLAN_ORG_UNIT_PERCENTAGE")
    private Double planOrganizationUnitPercentage;

    @Column(name = "PLAN_ORG_UNIT_WEIGHT")
    private Long planOrganizationUnitWeight;

    @Column(name = "PLAN_ORG_UNIT_VOLUME")
    private Long planOrganizationUnitVolume;

    @Column(name = "PLAN_ORG_UNIT_PRIORITY")
    private Integer planOrganizationUnitPriority;

    @Column(name = "PLAN_ORG_UNIT_IMPORTANCE")
    private Double planOrganizationUnitImportance;

    @Column(name = "PLAN_ORG_UNIT_ACTUAL_START_DATE")
    private LocalDate planOrganizationUnitActualStartDate;

    @Column(name = "PLAN_ORG_UNIT_ACTUAL_END_DATE")
    private LocalDate planOrganizationUnitActualEndDate;

    @Column(name = "PLAN_ORG_UNIT_EXPIRATION_DATE")
    private LocalDate planOrganizationUnitExpirationDate;

    @Column(name = "PLAN_ORG_UNIT_START_DATE")
    private LocalDate planOrganizationUnitStartDate;

    @Column(name = "PLAN_ORG_UNIT_END_DATE")
    private LocalDate planOrganizationUnitEndDate;

    @Column(name = "PLAN_ORG_UNIT_FISCAL_YEAR")
    private String planOrganizationUnitFiscalYear;

    @Column(name = "PLAN_ORG_UNIT_LEVEL$")
    private Long planOrganizationUnitLevel;

    @Column(name = "FK_ALLOCATION_ID")
    private Long allocationId;

    @Column(name = "ALLOCATION_NUMBER$")
    private String allocationNumber;

    @Column(name = "ALLOCATION_ALLOCATION_DATE")
    private LocalDate allocationDate;

    @Column(name = "ALLOCATION_TITLE")
    private String allocationTitle;

    public Long getPlanOrganizationUnitId() {
        return planOrganizationUnitId;
    }

    public void setPlanOrganizationUnitId(Long planOrganizationUnitId) {
        this.planOrganizationUnitId = planOrganizationUnitId;
    }

    public LocalDate getPlanOrganizationUnitAssignDate() {
        return planOrganizationUnitAssignDate;
    }

    public void setPlanOrganizationUnitAssignDate(LocalDate planOrganizationUnitAssignDate) {
        this.planOrganizationUnitAssignDate = planOrganizationUnitAssignDate;
    }

    public LocalDate getPlanOrganizationUnitDueDate() {
        return planOrganizationUnitDueDate;
    }

    public void setPlanOrganizationUnitDueDate(LocalDate planOrganizationUnitDueDate) {
        this.planOrganizationUnitDueDate = planOrganizationUnitDueDate;
    }

    public Long getPlanOrganizationUnitPvStateId() {
        return planOrganizationUnitPvStateId;
    }

    public void setPlanOrganizationUnitPvStateId(Long planOrganizationUnitPvStateId) {
        this.planOrganizationUnitPvStateId = planOrganizationUnitPvStateId;
    }

    public String getPlanOrganizationUnitPvStateTitle() {
        return planOrganizationUnitPvStateTitle;
    }

    public void setPlanOrganizationUnitPvStateTitle(String planOrganizationUnitPvStateTitle) {
        this.planOrganizationUnitPvStateTitle = planOrganizationUnitPvStateTitle;
    }

    public Long getPlanOrganizationUnitPvTypeId() {
        return planOrganizationUnitPvTypeId;
    }

    public void setPlanOrganizationUnitPvTypeId(Long planOrganizationUnitPvTypeId) {
        this.planOrganizationUnitPvTypeId = planOrganizationUnitPvTypeId;
    }

    public String getPlanOrganizationUnitPvTypeTitle() {
        return planOrganizationUnitPvTypeTitle;
    }

    public void setPlanOrganizationUnitPvTypeTitle(String planOrganizationUnitPvTypeTitle) {
        this.planOrganizationUnitPvTypeTitle = planOrganizationUnitPvTypeTitle;
    }

    public Long getPlanOrganizationUnitOrgUnitId() {
        return planOrganizationUnitOrgUnitId;
    }

    public void setPlanOrganizationUnitOrgUnitId(Long planOrganizationUnitOrgUnitId) {
        this.planOrganizationUnitOrgUnitId = planOrganizationUnitOrgUnitId;
    }

    public Double getPlanOrganizationUnitM1() {
        return planOrganizationUnitM1;
    }

    public void setPlanOrganizationUnitM1(Double planOrganizationUnitM1) {
        this.planOrganizationUnitM1 = planOrganizationUnitM1;
    }

    public Double getPlanOrganizationUnitM2() {
        return planOrganizationUnitM2;
    }

    public void setPlanOrganizationUnitM2(Double planOrganizationUnitM2) {
        this.planOrganizationUnitM2 = planOrganizationUnitM2;
    }

    public Double getPlanOrganizationUnitM3() {
        return planOrganizationUnitM3;
    }

    public void setPlanOrganizationUnitM3(Double planOrganizationUnitM3) {
        this.planOrganizationUnitM3 = planOrganizationUnitM3;
    }

    public Double getPlanOrganizationUnitM4() {
        return planOrganizationUnitM4;
    }

    public void setPlanOrganizationUnitM4(Double planOrganizationUnitM4) {
        this.planOrganizationUnitM4 = planOrganizationUnitM4;
    }

    public Double getPlanOrganizationUnitM5() {
        return planOrganizationUnitM5;
    }

    public void setPlanOrganizationUnitM5(Double planOrganizationUnitM5) {
        this.planOrganizationUnitM5 = planOrganizationUnitM5;
    }

    public Double getPlanOrganizationUnitM6() {
        return planOrganizationUnitM6;
    }

    public void setPlanOrganizationUnitM6(Double planOrganizationUnitM6) {
        this.planOrganizationUnitM6 = planOrganizationUnitM6;
    }

    public Double getPlanOrganizationUnitM7() {
        return planOrganizationUnitM7;
    }

    public void setPlanOrganizationUnitM7(Double planOrganizationUnitM7) {
        this.planOrganizationUnitM7 = planOrganizationUnitM7;
    }

    public Double getPlanOrganizationUnitM8() {
        return planOrganizationUnitM8;
    }

    public void setPlanOrganizationUnitM8(Double planOrganizationUnitM8) {
        this.planOrganizationUnitM8 = planOrganizationUnitM8;
    }

    public Double getPlanOrganizationUnitM9() {
        return planOrganizationUnitM9;
    }

    public void setPlanOrganizationUnitM9(Double planOrganizationUnitM9) {
        this.planOrganizationUnitM9 = planOrganizationUnitM9;
    }

    public Double getPlanOrganizationUnitM10() {
        return planOrganizationUnitM10;
    }

    public void setPlanOrganizationUnitM10(Double planOrganizationUnitM10) {
        this.planOrganizationUnitM10 = planOrganizationUnitM10;
    }

    public Double getPlanOrganizationUnitM11() {
        return planOrganizationUnitM11;
    }

    public void setPlanOrganizationUnitM11(Double planOrganizationUnitM11) {
        this.planOrganizationUnitM11 = planOrganizationUnitM11;
    }

    public Double getPlanOrganizationUnitM12() {
        return planOrganizationUnitM12;
    }

    public void setPlanOrganizationUnitM12(Double planOrganizationUnitM12) {
        this.planOrganizationUnitM12 = planOrganizationUnitM12;
    }

    public String getPlanOrganizationUnitName() {
        return planOrganizationUnitName;
    }

    public void setPlanOrganizationUnitName(String planOrganizationUnitName) {
        this.planOrganizationUnitName = planOrganizationUnitName;
    }

    public String getPlanOrganizationUnitTitle() {
        return planOrganizationUnitTitle;
    }

    public void setPlanOrganizationUnitTitle(String planOrganizationUnitTitle) {
        this.planOrganizationUnitTitle = planOrganizationUnitTitle;
    }

    public Boolean getPlanOrganizationUnitIsRoot() {
        return planOrganizationUnitIsRoot;
    }

    public void setPlanOrganizationUnitIsRoot(Boolean planOrganizationUnitIsRoot) {
        this.planOrganizationUnitIsRoot = planOrganizationUnitIsRoot;
    }

    public Double getPlanOrganizationUnitInitialPercentage() {
        return planOrganizationUnitInitialPercentage;
    }

    public void setPlanOrganizationUnitInitialPercentage(Double planOrganizationUnitInitialPercentage) {
        this.planOrganizationUnitInitialPercentage = planOrganizationUnitInitialPercentage;
    }

    public Double getPlanOrganizationUnitPercentage() {
        return planOrganizationUnitPercentage;
    }

    public void setPlanOrganizationUnitPercentage(Double planOrganizationUnitPercentage) {
        this.planOrganizationUnitPercentage = planOrganizationUnitPercentage;
    }

    public Long getPlanOrganizationUnitWeight() {
        return planOrganizationUnitWeight;
    }

    public void setPlanOrganizationUnitWeight(Long planOrganizationUnitWeight) {
        this.planOrganizationUnitWeight = planOrganizationUnitWeight;
    }

    public Long getPlanOrganizationUnitVolume() {
        return planOrganizationUnitVolume;
    }

    public void setPlanOrganizationUnitVolume(Long planOrganizationUnitVolume) {
        this.planOrganizationUnitVolume = planOrganizationUnitVolume;
    }

    public Integer getPlanOrganizationUnitPriority() {
        return planOrganizationUnitPriority;
    }

    public void setPlanOrganizationUnitPriority(Integer planOrganizationUnitPriority) {
        this.planOrganizationUnitPriority = planOrganizationUnitPriority;
    }

    public Double getPlanOrganizationUnitImportance() {
        return planOrganizationUnitImportance;
    }

    public void setPlanOrganizationUnitImportance(Double planOrganizationUnitImportance) {
        this.planOrganizationUnitImportance = planOrganizationUnitImportance;
    }

    public LocalDate getPlanOrganizationUnitActualStartDate() {
        return planOrganizationUnitActualStartDate;
    }

    public void setPlanOrganizationUnitActualStartDate(LocalDate planOrganizationUnitActualStartDate) {
        this.planOrganizationUnitActualStartDate = planOrganizationUnitActualStartDate;
    }

    public LocalDate getPlanOrganizationUnitActualEndDate() {
        return planOrganizationUnitActualEndDate;
    }

    public void setPlanOrganizationUnitActualEndDate(LocalDate planOrganizationUnitActualEndDate) {
        this.planOrganizationUnitActualEndDate = planOrganizationUnitActualEndDate;
    }

    public LocalDate getPlanOrganizationUnitExpirationDate() {
        return planOrganizationUnitExpirationDate;
    }

    public void setPlanOrganizationUnitExpirationDate(LocalDate planOrganizationUnitExpirationDate) {
        this.planOrganizationUnitExpirationDate = planOrganizationUnitExpirationDate;
    }

    public LocalDate getPlanOrganizationUnitStartDate() {
        return planOrganizationUnitStartDate;
    }

    public void setPlanOrganizationUnitStartDate(LocalDate planOrganizationUnitStartDate) {
        this.planOrganizationUnitStartDate = planOrganizationUnitStartDate;
    }

    public LocalDate getPlanOrganizationUnitEndDate() {
        return planOrganizationUnitEndDate;
    }

    public void setPlanOrganizationUnitEndDate(LocalDate planOrganizationUnitEndDate) {
        this.planOrganizationUnitEndDate = planOrganizationUnitEndDate;
    }

    public String getPlanOrganizationUnitFiscalYear() {
        return planOrganizationUnitFiscalYear;
    }

    public void setPlanOrganizationUnitFiscalYear(String planOrganizationUnitFiscalYear) {
        this.planOrganizationUnitFiscalYear = planOrganizationUnitFiscalYear;
    }

    public Long getPlanOrganizationUnitLevel() {
        return planOrganizationUnitLevel;
    }

    public void setPlanOrganizationUnitLevel(Long planOrganizationUnitLevel) {
        this.planOrganizationUnitLevel = planOrganizationUnitLevel;
    }

    public Long getAllocationId() {
        return allocationId;
    }

    public void setAllocationId(Long allocationId) {
        this.allocationId = allocationId;
    }

    public String getAllocationNumber() {
        return allocationNumber;
    }

    public void setAllocationNumber(String allocationNumber) {
        this.allocationNumber = allocationNumber;
    }

    public LocalDate getAllocationDate() {
        return allocationDate;
    }

    public void setAllocationDate(LocalDate allocationDate) {
        this.allocationDate = allocationDate;
    }

    public String getAllocationTitle() {
        return allocationTitle;
    }

    public void setAllocationTitle(String allocationTitle) {
        this.allocationTitle = allocationTitle;
    }
}
