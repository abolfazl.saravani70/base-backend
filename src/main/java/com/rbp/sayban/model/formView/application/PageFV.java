/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Immutable
@Table(name = "PAGE_VIEW")
public class PageFV extends BaseEntity {

    @Column(name = "PAGE_NAME")
    private String pageName;

    @Column(name = "PAGE_TITLE")
    private String pageTitle;

    @Column(name = "PAGE_URL")
    private String pageUrl;

    @Column(name = "PAGE_CODE")
    private String pageCode;

    @Column(name = "PAGE_HELP_URL")
    private String pageHelpUrl;

    @Column(name = "FROM_DATE")
    private LocalDate fromDate;

    @Column(name = "TO_DATE")
    private LocalDate toDate;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_PAGE_NAME")
    private String parentPageName;

    @Column(name = "PARENT_PAGE_TITLE")
    private String parentPageTitle;

    @Column(name = "PARENT_PAGE_URL")
    private String parentPageUrl;

    @Column(name = "PARENT_PAGE_CODE")
    private String parentPageCode;

    @Column(name = "PARENT_PAGE_HELP_URL")
    private String parentPageHelpUrl;

    @Column(name = "PARENT_FROM_DATE")
    private LocalDate parentFromDate;

    @Column(name = "PARENT_TO_DATE")
    private LocalDate parentToDate;

    @Column(name = "PARENT_TEXT")
    private String parentText;

    @Column(name = "PARENT_IS_ACTIVE")
    private Boolean parentIsActive;

    @Column(name = "PARENT_IS_DEPRECATED")
    private Boolean parentIsDeprecated;

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageCode() {
        return pageCode;
    }

    public void setPageCode(String pageCode) {
        this.pageCode = pageCode;
    }

    public String getPageHelpUrl() {
        return pageHelpUrl;
    }

    public void setPageHelpUrl(String pageHelpUrl) {
        this.pageHelpUrl = pageHelpUrl;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        isDeprecated = deprecated;
    }

    public Long getParent() {
        return parentId;
    }

    public void setParent(Long parent) {
        this.parentId = parent;
    }

    public String getParentPageName() {
        return parentPageName;
    }

    public void setParentPageName(String parentPageName) {
        this.parentPageName = parentPageName;
    }

    public String getParentPageTitle() {
        return parentPageTitle;
    }

    public void setParentPageTitle(String parentPageTitle) {
        this.parentPageTitle = parentPageTitle;
    }

    public String getParentPageUrl() {
        return parentPageUrl;
    }

    public void setParentPageUrl(String parentPageUrl) {
        this.parentPageUrl = parentPageUrl;
    }

    public String getParentPageCode() {
        return parentPageCode;
    }

    public void setParentPageCode(String parentPageCode) {
        this.parentPageCode = parentPageCode;
    }

    public String getParentPageHelpUrl() {
        return parentPageHelpUrl;
    }

    public void setParentPageHelpUrl(String parentPageHelpUrl) {
        this.parentPageHelpUrl = parentPageHelpUrl;
    }

    public LocalDate getParentFromDate() {
        return parentFromDate;
    }

    public void setParentFromDate(LocalDate parentFromDate) {
        this.parentFromDate = parentFromDate;
    }

    public LocalDate getParentToDate() {
        return parentToDate;
    }

    public void setParentToDate(LocalDate parentToDate) {
        this.parentToDate = parentToDate;
    }

    public String getParentText() {
        return parentText;
    }

    public void setParentText(String parentText) {
        this.parentText = parentText;
    }

    public Boolean getParentIsActive() {
        return parentIsActive;
    }

    public void setParentIsActive(Boolean parentIsActive) {
        this.parentIsActive = parentIsActive;
    }

    public Boolean getParentIsDeprecated() {
        return parentIsDeprecated;
    }

    public void setParentIsDeprecated(Boolean parentIsDeprecated) {
        this.parentIsDeprecated = parentIsDeprecated;
    }
}




