/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.Document;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "LETTER_VIEW")
public class LetterFV extends BaseEntity {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "LETTER_FROM")
    private String letterFrom;

    @Column(name = "LETTER_TO")
    private String letterTo;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "TEMPLATE")
    private String template;

    @Column(name = "KEY_WORD")
    private String keyWord;

    @Column(name = "FK_DOCUMENT_ID")
    private Long documentId;

    @Column(name = "DOCUMENT_IS_TEMP")
    private Boolean documentIsTemp;

    @Column(name = "DOCUMENT_NAME")
    private String documentName;

    @Column(name = "DOCUMENT_TITLE")
    private String documentTitle;

    @Column(name = "DOCUMENT_NUMBER$")
    private String documentNumber;

    @Column(name = "DOCUMENT_FK_PV_DOC_STATE_ID")
    private Long documentPvDocumentStateId;

    @Column(name = "DOCUMENT_PV_DOC_STATE_TITLE")
    private String  documentPvDocumentStateTitle;

    @Column(name = "DOCUMENT_FK_PV_TYPE_ID")
    private Long  documentPvTypeId;

    @Column(name = "DOCUMENT_FK_TYPE_ID_TITLE")
    private String  documentPvTypeTitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLetterFrom() {
        return letterFrom;
    }

    public void setLetterFrom(String letterFrom) {
        this.letterFrom = letterFrom;
    }

    public String getLetterTo() {
        return letterTo;
    }

    public void setLetterTo(String letterTo) {
        this.letterTo = letterTo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Boolean getDocumentIsTemp() {
        return documentIsTemp;
    }

    public void setDocumentIsTemp(Boolean documentIsTemp) {
        this.documentIsTemp = documentIsTemp;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentPvDocumentStateTitle() {
        return documentPvDocumentStateTitle;
    }

    public void setDocumentPvDocumentStateTitle(String documentPvDocumentStateTitle) {
        this.documentPvDocumentStateTitle = documentPvDocumentStateTitle;
    }

    public String getDocumentPvTypeTitle() {
        return documentPvTypeTitle;
    }

    public void setDocumentPvTypeTitle(String documentPvTypeTitle) {
        this.documentPvTypeTitle = documentPvTypeTitle;
    }

    public Long getDocumentPvDocumentStateId() {
        return documentPvDocumentStateId;
    }

    public void setDocumentPvDocumentStateId(Long documentPvDocumentStateId) {
        this.documentPvDocumentStateId = documentPvDocumentStateId;
    }

    public Long getDocumentPvTypeId() {
        return documentPvTypeId;
    }

    public void setDocumentPvTypeId(Long documentPvTypeId) {
        this.documentPvTypeId = documentPvTypeId;
    }
}
