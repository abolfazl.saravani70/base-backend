/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.Fund;
import com.rbp.sayban.model.domainmodel.budgeting.Sharing;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "FUND_SHARING_VIEW")
public class FundSharingFV extends BaseEntity {

    @Column(name = "ACCEPTED_AMOUNT")
    private Long acceptedAmount;

    @Column(name = "ACCEPTED_DATE")
    private LocalDate acceptedDate;

    @Column(name = "FK_FUNDING_ID")
    private Long fundId;

    @Column(name = "FK_SHARING_ID")
    private Long sharingId;

    @Column(name = "SHARING_NUMBER$")
    private String sharingNumber;

    @Column(name = "SHARING_AMOUNT")
    private Long sharingAmount;

    @Column(name = "SHARING_PERCENT")
    private Double sharingPercent;

    @Column(name = "SHARING_ACT_SHARING_DATE")
    private LocalDate sharingDate;

    public Long getAcceptedAmount() {
        return acceptedAmount;
    }

    public void setAcceptedAmount(Long acceptedAmount) {
        this.acceptedAmount = acceptedAmount;
    }

    public LocalDate getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(LocalDate acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public Long getFundId() {
        return fundId;
    }

    public void setFundId(Long fundId) {
        this.fundId = fundId;
    }

    public Long getSharingId() {
        return sharingId;
    }

    public void setSharingId(Long sharingId) {
        this.sharingId = sharingId;
    }

    public String getSharingNumber() {
        return sharingNumber;
    }

    public void setSharingNumber(String sharingNumber) {
        this.sharingNumber = sharingNumber;
    }

    public Long getSharingAmount() {
        return sharingAmount;
    }

    public void setSharingAmount(Long sharingAmount) {
        this.sharingAmount = sharingAmount;
    }

    public Double getSharingPercent() {
        return sharingPercent;
    }

    public void setSharingPercent(Double sharingPercent) {
        this.sharingPercent = sharingPercent;
    }

    public LocalDate getSharingDate() {
        return sharingDate;
    }

    public void setSharingDate(LocalDate sharingDate) {
        this.sharingDate = sharingDate;
    }
}
