package com.rbp.sayban.model.formView.objectives;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Location;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "OBJECTIVE_LOCATION_VIEW")
public class ObjectiveLocationFV extends EvalStateBaseEntity {

    @Column(name = "CAPACITY")
    private Long capacity;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "USAGE_TYPE")
    private String usageType;

    @Column(name = "FK_OBJECTIVE_ID")
    private Long objectiveId;

    @Column(name = "OBJECTIVE_FK_PV_TYPE_ID")
    private Long objectivePvTypeId;

    @Column(name = "OBJECTIVE_PV_TYPE_TITLE")
    private String objectivePvTypeTitle;

    @Column(name = "OBJECTIVE_NAME")
    private String objectiveName;

    @Column(name = "OBJECTIVE_TITLE")
    private String objectiveTitle;

    @Column(name = "OBJECTIVE_IS_ROOT")
    private Boolean objectiveIsRoot;

    @Column(name = "OBJECTIVE_INITIAL_PERCENTAGE")
    private Double objectiveInitialPercentage;

    @Column(name = "OBJECTIVE_PERCENTAGE")
    private Double objectivePercentage;

    @Column(name = "OBJECTIVE_WEIGHT")
    private Long objectiveWeight;

    @Column(name = "OBJECTIVE_VOLUME")
    private Long objectiveVolume;

    @Column(name = "OBJECTIVE_PRIORITY")
    private Integer objectivePriority;

    @Column(name = "OBJECTIVE_IMPORTANCE")
    private Double objectiveImportance;

    @Column(name = "OBJECTIVE_ACTUAL_START_DATE")
    private LocalDate objectiveActualStartDate;

    @Column(name = "OBJECTIVE_ACTUAL_END_DATE")
    private LocalDate objectiveActualEndDate;

    @Column(name = "OBJECTIVE_EXPIRATION_DATE")
    private LocalDate objectiveExpirationDate;

    @Column(name = "OBJECTIVE_START_DATE")
    private LocalDate objectiveStartDate;

    @Column(name = "OBJECTIVE_END_DATE")
    private LocalDate objectiveEndDate;

    @Column(name = "OBJECTIVE_FISCAL_YEAR")
    private String objectiveFiscalYear;

    @Column(name = "OBJECTIVE_LEVEL$")
    private Long objectiveLevel;

    @Column(name = "FK_LOCATION_ID")
    private Long locationId;

    @Column(name = "LOCATION_NAME")
    private String locationName;

    @Column(name = "LOCATION_NUMBER$")
    private String locationNumber;

    @Column(name = "LOCATION_TITLE")
    private String locationTitle;

    @Column(name = "LOCATION_Type")
    private String locationType;

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public Long getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }

    public Long getObjectivePvTypeId() {
        return objectivePvTypeId;
    }

    public void setObjectivePvTypeId(Long objectivePvTypeId) {
        this.objectivePvTypeId = objectivePvTypeId;
    }

    public String getObjectivePvTypeTitle() {
        return objectivePvTypeTitle;
    }

    public void setObjectivePvTypeTitle(String objectivePvTypeTitle) {
        this.objectivePvTypeTitle = objectivePvTypeTitle;
    }

    public String getObjectiveName() {
        return objectiveName;
    }

    public void setObjectiveName(String objectiveName) {
        this.objectiveName = objectiveName;
    }

    public String getObjectiveTitle() {
        return objectiveTitle;
    }

    public void setObjectiveTitle(String objectiveTitle) {
        this.objectiveTitle = objectiveTitle;
    }

    public Boolean getObjectiveIsRoot() {
        return objectiveIsRoot;
    }

    public void setObjectiveIsRoot(Boolean objectiveIsRoot) {
        this.objectiveIsRoot = objectiveIsRoot;
    }

    public Double getObjectiveInitialPercentage() {
        return objectiveInitialPercentage;
    }

    public void setObjectiveInitialPercentage(Double objectiveInitialPercentage) {
        this.objectiveInitialPercentage = objectiveInitialPercentage;
    }

    public Double getObjectivePercentage() {
        return objectivePercentage;
    }

    public void setObjectivePercentage(Double objectivePercentage) {
        this.objectivePercentage = objectivePercentage;
    }

    public Long getObjectiveWeight() {
        return objectiveWeight;
    }

    public void setObjectiveWeight(Long objectiveWeight) {
        this.objectiveWeight = objectiveWeight;
    }

    public Long getObjectiveVolume() {
        return objectiveVolume;
    }

    public void setObjectiveVolume(Long objectiveVolume) {
        this.objectiveVolume = objectiveVolume;
    }

    public Integer getObjectivePriority() {
        return objectivePriority;
    }

    public void setObjectivePriority(Integer objectivePriority) {
        this.objectivePriority = objectivePriority;
    }

    public Double getObjectiveImportance() {
        return objectiveImportance;
    }

    public void setObjectiveImportance(Double objectiveImportance) {
        this.objectiveImportance = objectiveImportance;
    }

    public LocalDate getObjectiveActualStartDate() {
        return objectiveActualStartDate;
    }

    public void setObjectiveActualStartDate(LocalDate objectiveActualStartDate) {
        this.objectiveActualStartDate = objectiveActualStartDate;
    }

    public LocalDate getObjectiveActualEndDate() {
        return objectiveActualEndDate;
    }

    public void setObjectiveActualEndDate(LocalDate objectiveActualEndDate) {
        this.objectiveActualEndDate = objectiveActualEndDate;
    }

    public LocalDate getObjectiveExpirationDate() {
        return objectiveExpirationDate;
    }

    public void setObjectiveExpirationDate(LocalDate objectiveExpirationDate) {
        this.objectiveExpirationDate = objectiveExpirationDate;
    }

    public LocalDate getObjectiveStartDate() {
        return objectiveStartDate;
    }

    public void setObjectiveStartDate(LocalDate objectiveStartDate) {
        this.objectiveStartDate = objectiveStartDate;
    }

    public LocalDate getObjectiveEndDate() {
        return objectiveEndDate;
    }

    public void setObjectiveEndDate(LocalDate objectiveEndDate) {
        this.objectiveEndDate = objectiveEndDate;
    }

    public String getObjectiveFiscalYear() {
        return objectiveFiscalYear;
    }

    public void setObjectiveFiscalYear(String objectiveFiscalYear) {
        this.objectiveFiscalYear = objectiveFiscalYear;
    }

    public Long getObjectiveLevel() {
        return objectiveLevel;
    }

    public void setObjectiveLevel(Long objectiveLevel) {
        this.objectiveLevel = objectiveLevel;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public String getLocationTitle() {
        return locationTitle;
    }

    public void setLocationTitle(String locationTitle) {
        this.locationTitle = locationTitle;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
}
