/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.project;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.KpiJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.project.Project;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "PROJECT_KPI_VIEW")
public class ProjectKpiFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_PROJECT_ID")
    private Long projectId;

    @Column(name = "PROJECT_FK_PV_TYPE_ID")
    private Long projectPvTypeId;

    @Column(name = "PROJECT_PV_TYPE_TITLE")
    private String projectPvTypeTitle;

    @Column(name = "PROJECT_NAME")
    private String projectName;

    @Column(name = "PROJECT_TITLE")
    private String projectTitle;

    @Column(name = "PROJECT_IS_ROOT")
    private Boolean projectIsRoot;

    @Column(name = "PROJECT_INITIAL_PERCENTAGE")
    private Double projectInitialPercentage;

    @Column(name = "PROJECT_PERCENTAGE")
    private Double projectPercentage;

    @Column(name = "PROJECT_WEIGHT")
    private Long projectWeight;

    @Column(name = "PROJECT_VOLUME")
    private Long projectVolume;

    @Column(name = "PROJECT_PRIORITY")
    private Integer projectPriority;

    @Column(name = "PROJECT_IMPORTANCE")
    private Double projectImportance;

    @Column(name = "PROJECT_ACTUAL_START_DATE")
    private LocalDate projectActualStartDate;

    @Column(name = "PROJECT_ACTUAL_END_DATE")
    private LocalDate projectActualEndDate;

    @Column(name = "PROJECT_EXPIRATION_DATE")
    private LocalDate projectExpirationDate;

    @Column(name = "PROJECT_START_DATE")
    private LocalDate projectStartDate;

    @Column(name = "PROJECT_END_DATE")
    private LocalDate projectEndDate;

    @Column(name = "PROJECT_FISCAL_YEAR")
    private String projectFiscalYear;

    @Column(name = "PROJECT_LEVEL$")
    private Long projectLevel;

    @Column(name = "FK_KPI_ID")
    private Long kpiId;

    @Column(name = "KPI_KEY")
    private Integer kpiKey;

    @Column(name = "KPI_CODE")
    private String kpiCode;

    @Column(name = "KPI_VALUE")
    private Long kpiValue;

    @Column(name = "KPI_BEST_VALUE")
    private Long kpiBestValue;

    @Column(name = "KPI_KPI_PRIORITY")
    private Integer kpiKpiPriority;

    @Column(name = "KPI_COST")
    private Integer kpiCost;

    @Column(name = "KPI_MINIMUM")
    private Long kpiMinimum;

    @Column(name = "KPI_MAXIMUM")
    private Long kpiMaximum;

    @Column(name = "KPI_NORMAL")
    private Long kpiNormal;

    @Column(name = "KPI_FK_PV_KPI_TYPE_ID")
    private Long kpiPvKpiTypeId;

    @Column(name = "KPI_PV_KPI_TYPE_TITLE")
    private String kpiPvKpiTypeTitle;

    @Column(name = "KPI_FK_PV_MESURE_UNIT_ID")
    private Long kpiPvMesureUnitId;

    @Column(name = "KPI_PV_MESURE_UNIT_TITLE")
    private String kpiPvMesureUnitTitle;

    @Column(name = "KPI_NAME")
    private String kpiName;

    @Column(name = "KPI_TITLE")
    private String kpiTitle;

    @Column(name = "KPI_IS_ROOT")
    private Boolean kpiIsRoot;

    @Column(name = "KPI_INITIAL_PERCENTAGE")
    private Double kpiInitialPercentage;

    @Column(name = "KPI_PERCENTAGE")
    private Double kpiPercentage;

    @Column(name = "KPI_WEIGHT")
    private Long kpiWeight;

    @Column(name = "KPI_VOLUME")
    private Long kpiVolume;

    @Column(name = "KPI_PRIORITY")
    private Integer kpiPriority;

    @Column(name = "KPI_IMPORTANCE")
    private Double kpiImportance;

    @Column(name = "KPI_ACTUAL_START_DATE")
    private LocalDate kpiActualStartDate;

    @Column(name = "KPI_ACTUAL_END_DATE")
    private LocalDate kpiActualEndDate;

    @Column(name = "KPI_EXPIRATION_DATE")
    private LocalDate kpiExpirationDate;

    @Column(name = "KPI_START_DATE")
    private LocalDate kpiStartDate;

    @Column(name = "KPI_END_DATE")
    private LocalDate kpiEndDate;

    @Column(name = "KPI_FISCAL_YEAR")
    private String kpiFiscalYear;

    @Column(name = "KPI_LEVEL$")
    private Long kpiLevel;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectPvTypeId() {
        return projectPvTypeId;
    }

    public void setProjectPvTypeId(Long projectPvTypeId) {
        this.projectPvTypeId = projectPvTypeId;
    }

    public String getProjectPvTypeTitle() {
        return projectPvTypeTitle;
    }

    public void setProjectPvTypeTitle(String projectPvTypeTitle) {
        this.projectPvTypeTitle = projectPvTypeTitle;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public Boolean getProjectIsRoot() {
        return projectIsRoot;
    }

    public void setProjectIsRoot(Boolean projectIsRoot) {
        this.projectIsRoot = projectIsRoot;
    }

    public Double getProjectInitialPercentage() {
        return projectInitialPercentage;
    }

    public void setProjectInitialPercentage(Double projectInitialPercentage) {
        this.projectInitialPercentage = projectInitialPercentage;
    }

    public Double getProjectPercentage() {
        return projectPercentage;
    }

    public void setProjectPercentage(Double projectPercentage) {
        this.projectPercentage = projectPercentage;
    }

    public Long getProjectWeight() {
        return projectWeight;
    }

    public void setProjectWeight(Long projectWeight) {
        this.projectWeight = projectWeight;
    }

    public Long getProjectVolume() {
        return projectVolume;
    }

    public void setProjectVolume(Long projectVolume) {
        this.projectVolume = projectVolume;
    }

    public Integer getProjectPriority() {
        return projectPriority;
    }

    public void setProjectPriority(Integer projectPriority) {
        this.projectPriority = projectPriority;
    }

    public Double getProjectImportance() {
        return projectImportance;
    }

    public void setProjectImportance(Double projectImportance) {
        this.projectImportance = projectImportance;
    }

    public LocalDate getProjectActualStartDate() {
        return projectActualStartDate;
    }

    public void setProjectActualStartDate(LocalDate projectActualStartDate) {
        this.projectActualStartDate = projectActualStartDate;
    }

    public LocalDate getProjectActualEndDate() {
        return projectActualEndDate;
    }

    public void setProjectActualEndDate(LocalDate projectActualEndDate) {
        this.projectActualEndDate = projectActualEndDate;
    }

    public LocalDate getProjectExpirationDate() {
        return projectExpirationDate;
    }

    public void setProjectExpirationDate(LocalDate projectExpirationDate) {
        this.projectExpirationDate = projectExpirationDate;
    }

    public LocalDate getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(LocalDate projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public LocalDate getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(LocalDate projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectFiscalYear() {
        return projectFiscalYear;
    }

    public void setProjectFiscalYear(String projectFiscalYear) {
        this.projectFiscalYear = projectFiscalYear;
    }

    public Long getProjectLevel() {
        return projectLevel;
    }

    public void setProjectLevel(Long projectLevel) {
        this.projectLevel = projectLevel;
    }

    public Long getKpiId() {
        return kpiId;
    }

    public void setKpiId(Long kpiId) {
        this.kpiId = kpiId;
    }

    public Integer getKpiKey() {
        return kpiKey;
    }

    public void setKpiKey(Integer kpiKey) {
        this.kpiKey = kpiKey;
    }

    public String getKpiCode() {
        return kpiCode;
    }

    public void setKpiCode(String kpiCode) {
        this.kpiCode = kpiCode;
    }

    public Long getKpiValue() {
        return kpiValue;
    }

    public void setKpiValue(Long kpiValue) {
        this.kpiValue = kpiValue;
    }

    public Long getKpiBestValue() {
        return kpiBestValue;
    }

    public void setKpiBestValue(Long kpiBestValue) {
        this.kpiBestValue = kpiBestValue;
    }

    public Integer getKpiKpiPriority() {
        return kpiKpiPriority;
    }

    public void setKpiKpiPriority(Integer kpiKpiPriority) {
        this.kpiKpiPriority = kpiKpiPriority;
    }

    public Integer getKpiCost() {
        return kpiCost;
    }

    public void setKpiCost(Integer kpiCost) {
        this.kpiCost = kpiCost;
    }

    public Long getKpiMinimum() {
        return kpiMinimum;
    }

    public void setKpiMinimum(Long kpiMinimum) {
        this.kpiMinimum = kpiMinimum;
    }

    public Long getKpiMaximum() {
        return kpiMaximum;
    }

    public void setKpiMaximum(Long kpiMaximum) {
        this.kpiMaximum = kpiMaximum;
    }

    public Long getKpiNormal() {
        return kpiNormal;
    }

    public void setKpiNormal(Long kpiNormal) {
        this.kpiNormal = kpiNormal;
    }

    public Long getKpiPvKpiTypeId() {
        return kpiPvKpiTypeId;
    }

    public void setKpiPvKpiTypeId(Long kpiPvKpiTypeId) {
        this.kpiPvKpiTypeId = kpiPvKpiTypeId;
    }

    public String getKpiPvKpiTypeTitle() {
        return kpiPvKpiTypeTitle;
    }

    public void setKpiPvKpiTypeTitle(String kpiPvKpiTypeTitle) {
        this.kpiPvKpiTypeTitle = kpiPvKpiTypeTitle;
    }

    public Long getKpiPvMesureUnitId() {
        return kpiPvMesureUnitId;
    }

    public void setKpiPvMesureUnitId(Long kpiPvMesureUnitId) {
        this.kpiPvMesureUnitId = kpiPvMesureUnitId;
    }

    public String getKpiPvMesureUnitTitle() {
        return kpiPvMesureUnitTitle;
    }

    public void setKpiPvMesureUnitTitle(String kpiPvMesureUnitTitle) {
        this.kpiPvMesureUnitTitle = kpiPvMesureUnitTitle;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getKpiTitle() {
        return kpiTitle;
    }

    public void setKpiTitle(String kpiTitle) {
        this.kpiTitle = kpiTitle;
    }

    public Boolean getKpiIsRoot() {
        return kpiIsRoot;
    }

    public void setKpiIsRoot(Boolean kpiIsRoot) {
        this.kpiIsRoot = kpiIsRoot;
    }

    public Double getKpiInitialPercentage() {
        return kpiInitialPercentage;
    }

    public void setKpiInitialPercentage(Double kpiInitialPercentage) {
        this.kpiInitialPercentage = kpiInitialPercentage;
    }

    public Double getKpiPercentage() {
        return kpiPercentage;
    }

    public void setKpiPercentage(Double kpiPercentage) {
        this.kpiPercentage = kpiPercentage;
    }

    public Long getKpiWeight() {
        return kpiWeight;
    }

    public void setKpiWeight(Long kpiWeight) {
        this.kpiWeight = kpiWeight;
    }

    public Long getKpiVolume() {
        return kpiVolume;
    }

    public void setKpiVolume(Long kpiVolume) {
        this.kpiVolume = kpiVolume;
    }

    public Integer getKpiPriority() {
        return kpiPriority;
    }

    public void setKpiPriority(Integer kpiPriority) {
        this.kpiPriority = kpiPriority;
    }

    public Double getKpiImportance() {
        return kpiImportance;
    }

    public void setKpiImportance(Double kpiImportance) {
        this.kpiImportance = kpiImportance;
    }

    public LocalDate getKpiActualStartDate() {
        return kpiActualStartDate;
    }

    public void setKpiActualStartDate(LocalDate kpiActualStartDate) {
        this.kpiActualStartDate = kpiActualStartDate;
    }

    public LocalDate getKpiActualEndDate() {
        return kpiActualEndDate;
    }

    public void setKpiActualEndDate(LocalDate kpiActualEndDate) {
        this.kpiActualEndDate = kpiActualEndDate;
    }

    public LocalDate getKpiExpirationDate() {
        return kpiExpirationDate;
    }

    public void setKpiExpirationDate(LocalDate kpiExpirationDate) {
        this.kpiExpirationDate = kpiExpirationDate;
    }

    public LocalDate getKpiStartDate() {
        return kpiStartDate;
    }

    public void setKpiStartDate(LocalDate kpiStartDate) {
        this.kpiStartDate = kpiStartDate;
    }

    public LocalDate getKpiEndDate() {
        return kpiEndDate;
    }

    public void setKpiEndDate(LocalDate kpiEndDate) {
        this.kpiEndDate = kpiEndDate;
    }

    public String getKpiFiscalYear() {
        return kpiFiscalYear;
    }

    public void setKpiFiscalYear(String kpiFiscalYear) {
        this.kpiFiscalYear = kpiFiscalYear;
    }

    public Long getKpiLevel() {
        return kpiLevel;
    }

    public void setKpiLevel(Long kpiLevel) {
        this.kpiLevel = kpiLevel;
    }
}
