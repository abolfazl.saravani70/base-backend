/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.SysRuleFormula;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "SYS_RULE_VIEW")
public class SysRuleFV extends BaseEntity {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    @Column(name = "START_DATE")
    private LocalDate startDate;

    @Column(name = "EXPIRATION_DATE")
    private LocalDate expirationDate;
    // TODO: 07/12/2019
    @Column(name = "FK_SYS_RULE_FORMULA_ID")
    private Long sysRuleFormulaId;

    @Column(name = "SYS_RULE_FORMULA_KEY")
    private String sysRuleFormulaKey;

    @Column(name = "SYS_RULE_FORMULA_VALUE")
    private String sysRuleFormulaValue;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getSysRuleFormulaId() {
        return sysRuleFormulaId;
    }

    public void setSysRuleFormulaId(Long sysRuleFormulaId) {
        this.sysRuleFormulaId = sysRuleFormulaId;
    }

    public String getSysRuleFormulaKey() {
        return sysRuleFormulaKey;
    }

    public void setSysRuleFormulaKey(String sysRuleFormulaKey) {
        this.sysRuleFormulaKey = sysRuleFormulaKey;
    }

    public String getSysRuleFormulaValue() {
        return sysRuleFormulaValue;
    }

    public void setSysRuleFormulaValue(String sysRuleFormulaValue) {
        this.sysRuleFormulaValue = sysRuleFormulaValue;
    }
}
