/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "ORG_UNIT_RULE_VIEW")
public class OrganizationUnitRuleFV extends BaseEntity {

    @Column(name = "FK_ORG_UNIT_ID")
    private Long organizationUnitId;

    @Column(name = "FK_RULE_ID")
    private Long ruleId;

    @Column(name = "RULE_NAME")
    private String ruleName;

    @Column(name = "RULE_TYPE")
    private String ruleType;

    @Column(name = "RULE_ENGLISH_NAME")
    private String ruleEnglishName;

    @Column(name = "RULE_LEVEL$")
    private String ruleLevel;

    @Column(name = "RULE_KEY")
    private String ruleKey;

    @Column(name = "RULE_IS_ACTIVE")
    private Boolean ruleIsActive;

    @Column(name = "RULE_START_DATE")
    private LocalDate ruleStartDate;

    @Column(name = "RULE_END_DATE")
    private LocalDate ruleEndDate;

    public Long getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Long organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getRuleEnglishName() {
        return ruleEnglishName;
    }

    public void setRuleEnglishName(String ruleEnglishName) {
        this.ruleEnglishName = ruleEnglishName;
    }

    public String getRuleLevel() {
        return ruleLevel;
    }

    public void setRuleLevel(String ruleLevel) {
        this.ruleLevel = ruleLevel;
    }

    public String getRuleKey() {
        return ruleKey;
    }

    public void setRuleKey(String ruleKey) {
        this.ruleKey = ruleKey;
    }

    public Boolean getRuleIsActive() {
        return ruleIsActive;
    }

    public void setRuleIsActive(Boolean ruleIsActive) {
        this.ruleIsActive = ruleIsActive;
    }

    public LocalDate getRuleStartDate() {
        return ruleStartDate;
    }

    public void setRuleStartDate(LocalDate ruleStartDate) {
        this.ruleStartDate = ruleStartDate;
    }

    public LocalDate getRuleEndDate() {
        return ruleEndDate;
    }

    public void setRuleEndDate(LocalDate ruleEndDate) {
        this.ruleEndDate = ruleEndDate;
    }
}
