/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.report.GeneralReportFilter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "REPORT_FILTER_VIEW")
public class ReportFilterFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "QUERY")
    private String query;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "DEFAULT_VALUE")
    private String defualtValue;

    @Column(name = "FK_FILTER_OPERATOR_ID")
    private Long filterOperatorId;

    @Column(name = "FILTER_OPERATOR_NAME")
    private String filterOperatorName;

    @Column(name = "FILTER_OPERATOR_TITLE")
    private String filterOperatorTitle;

    @Column(name = "FILTER_OPERATOR_ABBR")
    private String filterOperatorAbbr;

    @Column(name = "FILTER_OPERATOR_SIGN")
    private String filterOperatorSign;

    @Column(name = "FILTER_OPERATOR_PRIORITY")
    private Integer filterOperatorPriority;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDefualtValue() {
        return defualtValue;
    }

    public void setDefualtValue(String defualtValue) {
        this.defualtValue = defualtValue;
    }

    public Long getFilterOperatorId() {
        return filterOperatorId;
    }

    public void setFilterOperatorId(Long filterOperatorId) {
        this.filterOperatorId = filterOperatorId;
    }

    public String getFilterOperatorName() {
        return filterOperatorName;
    }

    public void setFilterOperatorName(String filterOperatorName) {
        this.filterOperatorName = filterOperatorName;
    }

    public String getFilterOperatorTitle() {
        return filterOperatorTitle;
    }

    public void setFilterOperatorTitle(String filterOperatorTitle) {
        this.filterOperatorTitle = filterOperatorTitle;
    }

    public String getFilterOperatorAbbr() {
        return filterOperatorAbbr;
    }

    public void setFilterOperatorAbbr(String filterOperatorAbbr) {
        this.filterOperatorAbbr = filterOperatorAbbr;
    }

    public String getFilterOperatorSign() {
        return filterOperatorSign;
    }

    public void setFilterOperatorSign(String filterOperatorSign) {
        this.filterOperatorSign = filterOperatorSign;
    }

    public Integer getFilterOperatorPriority() {
        return filterOperatorPriority;
    }

    public void setFilterOperatorPriority(Integer filterOperatorPriority) {
        this.filterOperatorPriority = filterOperatorPriority;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }
}
