/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.project;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.project.ProjectResourceType;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "PROJECT_RESOURCE_VIEW")
public class ProjectResourceFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CODE")
    private String code;

    @Column(name = "FK_PV_PROJECT_RESOURCE_TYPE_ID")
    private Long pvProjectResourceTypeId;

    @Column(name = "PV_PROJECT_RESOURCE_TYPE_TITLE")
    private String pvProjectResourceTypeTitle;

    @Column(name = "FK_PROJECT_RESOURCE_TYPE_ID")
    private Long projectResourceTypeId;

    @Column(name = "PROJECT_RESOURCE_TYPE_NAME")
    private String projectResourceTypeName;

    @Column(name = "PROJECT_RESOURCE_TYPE_TITLE")
    private String projectResourceTypeTitle;

    @Column(name = "PROJECT_RESOURCE_TYPE_KEY")
    private String projectResourceTypeKey;

    @Column(name = "PROJECT_RESOURCE_TYPE_CODE")
    private String projectResourceTypeCode;

    @Column(name = "PRO_RES_TYPE_VALUE")
    private String projectResourceTypeValue;

    @Column(name = "PRO_RES_TYPE_IS_DEPRECATED")
    private Boolean projectResourceTypeIsDeprecated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPvProjectResourceTypeId() {
        return pvProjectResourceTypeId;
    }

    public void setPvProjectResourceTypeId(Long pvProjectResourceTypeId) {
        this.pvProjectResourceTypeId = pvProjectResourceTypeId;
    }

    public String getPvProjectResourceTypeTitle() {
        return pvProjectResourceTypeTitle;
    }

    public void setPvProjectResourceTypeTitle(String pvProjectResourceTypeTitle) {
        this.pvProjectResourceTypeTitle = pvProjectResourceTypeTitle;
    }

    public Long getProjectResourceTypeId() {
        return projectResourceTypeId;
    }

    public void setProjectResourceTypeId(Long projectResourceTypeId) {
        this.projectResourceTypeId = projectResourceTypeId;
    }

    public String getProjectResourceTypeName() {
        return projectResourceTypeName;
    }

    public void setProjectResourceTypeName(String projectResourceTypeName) {
        this.projectResourceTypeName = projectResourceTypeName;
    }

    public String getProjectResourceTypeTitle() {
        return projectResourceTypeTitle;
    }

    public void setProjectResourceTypeTitle(String projectResourceTypeTitle) {
        this.projectResourceTypeTitle = projectResourceTypeTitle;
    }

    public String getProjectResourceTypeKey() {
        return projectResourceTypeKey;
    }

    public void setProjectResourceTypeKey(String projectResourceTypeKey) {
        this.projectResourceTypeKey = projectResourceTypeKey;
    }

    public String getProjectResourceTypeCode() {
        return projectResourceTypeCode;
    }

    public void setProjectResourceTypeCode(String projectResourceTypeCode) {
        this.projectResourceTypeCode = projectResourceTypeCode;
    }

    public String getProjectResourceTypeValue() {
        return projectResourceTypeValue;
    }

    public void setProjectResourceTypeValue(String projectResourceTypeValue) {
        this.projectResourceTypeValue = projectResourceTypeValue;
    }

    public Boolean getProjectResourceTypeIsDeprecated() {
        return projectResourceTypeIsDeprecated;
    }

    public void setProjectResourceTypeIsDeprecated(Boolean projectResourceTypeIsDeprecated) {
        this.projectResourceTypeIsDeprecated = projectResourceTypeIsDeprecated;
    }
}
