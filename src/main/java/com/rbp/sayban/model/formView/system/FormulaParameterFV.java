/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;


import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.SysRuleFormula;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "FORMULA_PARAMETER_VIEW")
public class FormulaParameterFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "IS_NULLABLE")
    private Boolean isNullable;

    @Column(name = "IS_OPTIONAL")
    private Boolean isOptional;

    @Column(name = "BASE")
    private String base;

    @Column(name = "FK_SYS_RULE_FORMULA_ID")
    private Long sysRuleFormulaId;

    @Column(name = "SYS_RULE_FORMULA_KEY")
    private String sysRuleFormulaKey;

    @Column(name = "SYS_RULE_FORMULA_VALUE")
    private String sysRuleFormulaValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getNullable() {
        return isNullable;
    }

    public void setNullable(Boolean nullable) {
        isNullable = nullable;
    }

    public Boolean getOptional() {
        return isOptional;
    }

    public void setOptional(Boolean optional) {
        isOptional = optional;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Long getSysRuleFormulaId() {
        return sysRuleFormulaId;
    }

    public void setSysRuleFormulaId(Long sysRuleFormulaId) {
        this.sysRuleFormulaId = sysRuleFormulaId;
    }

    public String getSysRuleFormulaKey() {
        return sysRuleFormulaKey;
    }

    public void setSysRuleFormulaKey(String sysRuleFormulaKey) {
        this.sysRuleFormulaKey = sysRuleFormulaKey;
    }

    public String getSysRuleFormulaValue() {
        return sysRuleFormulaValue;
    }

    public void setSysRuleFormulaValue(String sysRuleFormulaValue) {
        this.sysRuleFormulaValue = sysRuleFormulaValue;
    }
}
