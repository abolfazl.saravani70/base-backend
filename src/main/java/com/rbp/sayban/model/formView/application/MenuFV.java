/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Immutable
@Table(name = "MENU_VIEW")
public class MenuFV extends BaseEntity {

    @Column(name = "MENU_NAME")
    private String menuName;

    @Column(name = "MENU_TITLE")
    private String menuTitle;

    @Column(name = "MENU_URL")
    private String menuUrl;

    @Column(name = "MENU_CODE")
    private Integer menuCode;

    @Column(name = "FROM_DATE")
    private LocalDate fromDate;

    @Column(name = "TO_DATE")
    private LocalDate toDate;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_MENU_NAME")
    private String parentMenuName;

    @Column(name = "PARENT_MENU_TITLE")
    private String parentMenuTitle;

    @Column(name = "PARENT_MENU_URL")
    private String parentMenuUrl;

    @Column(name = "PARENT_MENU_CODE")
    private Integer parentMenuCode;

    @Column(name = "PARENT_FROM_DATE")
    private LocalDate parentFromDate;

    @Column(name = "PARENT_TO_DATE")
    private LocalDate parentToDate;

    @Column(name = "PARENT_IS_DEPRECATED")
    private Boolean parentIsDeprecated;

    @Column(name = "PARENT_IS_ACTIVE")
    private Boolean parentIsActive;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Integer getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(Integer menuCode) {
        this.menuCode = menuCode;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public Boolean getDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        isDeprecated = deprecated;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentMenuName() {
        return parentMenuName;
    }

    public void setParentMenuName(String parentMenuName) {
        this.parentMenuName = parentMenuName;
    }

    public String getParentMenuTitle() {
        return parentMenuTitle;
    }

    public void setParentMenuTitle(String parentMenuTitle) {
        this.parentMenuTitle = parentMenuTitle;
    }

    public String getParentMenuUrl() {
        return parentMenuUrl;
    }

    public void setParentMenuUrl(String parentMenuUrl) {
        this.parentMenuUrl = parentMenuUrl;
    }

    public Integer getParentMenuCode() {
        return parentMenuCode;
    }

    public void setParentMenuCode(Integer parentMenuCode) {
        this.parentMenuCode = parentMenuCode;
    }

    public LocalDate getParentFromDate() {
        return parentFromDate;
    }

    public void setParentFromDate(LocalDate parentFromDate) {
        this.parentFromDate = parentFromDate;
    }

    public LocalDate getParentToDate() {
        return parentToDate;
    }

    public void setParentToDate(LocalDate parentToDate) {
        this.parentToDate = parentToDate;
    }

    public Boolean getParentIsDeprecated() {
        return parentIsDeprecated;
    }

    public void setParentIsDeprecated(Boolean parentIsDeprecated) {
        this.parentIsDeprecated = parentIsDeprecated;
    }

    public Boolean getParentIsActive() {
        return parentIsActive;
    }

    public void setParentIsActive(Boolean parentIsActive) {
        this.parentIsActive = parentIsActive;
    }
}
