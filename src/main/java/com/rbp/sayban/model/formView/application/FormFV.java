/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "FORM_VIEW")
public class FormFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "URL")
    private String url;

    @Column(name = "CODE")
    private String code;

    @Column(name = "HELP_URL")
    private String helpUrl;

    @Column(name = "FROM_DATE")
    private LocalDate fromDate;

    @Column(name = "TO_DATE")
    private LocalDate toDate;

    @Column(name = "BODY")
    private String body;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TITLE")
    private String parentTitle;

    @Column(name = "PARENT_URL")
    private String parentUrl;

    @Column(name = "PARENT_CODE")
    private String parentCode;

    @Column(name = "PARENT_HELP_URL")
    private String parentHelpUrl;

    @Column(name = "PARENT_FROM_DATE")
    private LocalDate parentFromDate;

    @Column(name = "PARENT_TO_DATE")
    private LocalDate parentToDate;

    @Column(name = "PARENT_BODY")
    private String parentBody;

    @Column(name = "PARENT_IS_ACTIVE")
    private Boolean parentIsActive;

    @Column(name = "PARENT_IS_DEPRECATED")
    private Boolean parentIsDeprecated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHelpUrl() {
        return helpUrl;
    }

    public void setHelpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        isDeprecated = deprecated;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentUrl() {
        return parentUrl;
    }

    public void setParentUrl(String parentUrl) {
        this.parentUrl = parentUrl;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getParentHelpUrl() {
        return parentHelpUrl;
    }

    public void setParentHelpUrl(String parentHelpUrl) {
        this.parentHelpUrl = parentHelpUrl;
    }

    public LocalDate getParentFromDate() {
        return parentFromDate;
    }

    public void setParentFromDate(LocalDate parentFromDate) {
        this.parentFromDate = parentFromDate;
    }

    public LocalDate getParentToDate() {
        return parentToDate;
    }

    public void setParentToDate(LocalDate parentToDate) {
        this.parentToDate = parentToDate;
    }

    public String getParentBody() {
        return parentBody;
    }

    public void setParentBody(String parentBody) {
        this.parentBody = parentBody;
    }

    public Boolean getParentIsActive() {
        return parentIsActive;
    }

    public void setParentIsActive(Boolean parentIsActive) {
        this.parentIsActive = parentIsActive;
    }

    public Boolean getParentIsDeprecated() {
        return parentIsDeprecated;
    }

    public void setParentIsDeprecated(Boolean parentIsDeprecated) {
        this.parentIsDeprecated = parentIsDeprecated;
    }
}
