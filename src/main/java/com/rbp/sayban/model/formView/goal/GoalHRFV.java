package com.rbp.sayban.model.formView.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "GOAL_HR_VIEW")
public class GoalHRFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_GOAL_ID")
    private Long goalId;

    @Column(name = "GOAL_NAME")
    private String goalName;

    @Column(name = "GOAL_FK_PV_TYPE_ID")
    private Long goalPvTypeId;

    @Column(name = "GOAL_PV_TYPE_TITLE")
    private String goalPvTypeTitle;

    @Column(name = "GOAL_TITLE")
    private String goalTitle;

    @Column(name = "GOAL_IS_ROOT")
    private Boolean goalIsRoot;

    @Column(name = "GOAL_INITIAL_PERCENTAGE")
    private Double goalInitialPercentage;

    @Column(name = "GOAL_PERCENTAGE")
    private Double goalPercentage;

    @Column(name = "GOAL_WEIGHT")
    private Long goalWeight;

    @Column(name = "GOAL_VOLUME")
    private Long goalVolume;

    @Column(name = "GOAL_PRIORITY")
    private Integer goalPriority;

    @Column(name = "GOAL_IMPORTANCE")
    private Double goalImportance;

    @Column(name = "GOAL_ACTUAL_START_DATE")
    private LocalDate goalActualStartDate;

    @Column(name = "GOAL_ACTUAL_END_DATE")
    private LocalDate goalActualEndDate;

    @Column(name = "GOAL_EXPIRATION_DATE")
    private LocalDate goalExpirationDate;

    @Column(name = "GOAL_START_DATE")
    private LocalDate goalStartDate;

    @Column(name = "GOAL_END_DATE")
    private LocalDate goalEndDate;

    @Column(name = "GOAL_FISCAL_YEAR")
    private String goalFiscalYear;

    @Column(name = "GOAL_LEVEL$")
    private Long goalLevel;

    @Column(name = "FK_HR_ID")
    private Long hRId;

    @Column(name = "HR_TITLE")
    private String  hRTitle;

    @Column(name = "HR_IS_ENABLE")
    private Boolean  hRIsEnabled;

    @Column(name = "HR_FK_ORG_CHART_ID")
    private Long  hROrgChartId;

    @Column(name = "HR_FK_ORG_ID")
    private Long  hROrgId;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public Long getGoalPvTypeId() {
        return goalPvTypeId;
    }

    public void setGoalPvTypeId(Long goalPvTypeId) {
        this.goalPvTypeId = goalPvTypeId;
    }

    public String getGoalPvTypeTitle() {
        return goalPvTypeTitle;
    }

    public void setGoalPvTypeTitle(String goalPvTypeTitle) {
        this.goalPvTypeTitle = goalPvTypeTitle;
    }

    public String getGoalTitle() {
        return goalTitle;
    }

    public void setGoalTitle(String goalTitle) {
        this.goalTitle = goalTitle;
    }

    public Boolean getGoalIsRoot() {
        return goalIsRoot;
    }

    public void setGoalIsRoot(Boolean goalIsRoot) {
        this.goalIsRoot = goalIsRoot;
    }

    public Double getGoalInitialPercentage() {
        return goalInitialPercentage;
    }

    public void setGoalInitialPercentage(Double goalInitialPercentage) {
        this.goalInitialPercentage = goalInitialPercentage;
    }

    public Double getGoalPercentage() {
        return goalPercentage;
    }

    public void setGoalPercentage(Double goalPercentage) {
        this.goalPercentage = goalPercentage;
    }

    public Long getGoalWeight() {
        return goalWeight;
    }

    public void setGoalWeight(Long goalWeight) {
        this.goalWeight = goalWeight;
    }

    public Long getGoalVolume() {
        return goalVolume;
    }

    public void setGoalVolume(Long goalVolume) {
        this.goalVolume = goalVolume;
    }

    public Integer getGoalPriority() {
        return goalPriority;
    }

    public void setGoalPriority(Integer goalPriority) {
        this.goalPriority = goalPriority;
    }

    public Double getGoalImportance() {
        return goalImportance;
    }

    public void setGoalImportance(Double goalImportance) {
        this.goalImportance = goalImportance;
    }

    public LocalDate getGoalActualStartDate() {
        return goalActualStartDate;
    }

    public void setGoalActualStartDate(LocalDate goalActualStartDate) {
        this.goalActualStartDate = goalActualStartDate;
    }

    public LocalDate getGoalActualEndDate() {
        return goalActualEndDate;
    }

    public void setGoalActualEndDate(LocalDate goalActualEndDate) {
        this.goalActualEndDate = goalActualEndDate;
    }

    public LocalDate getGoalExpirationDate() {
        return goalExpirationDate;
    }

    public void setGoalExpirationDate(LocalDate goalExpirationDate) {
        this.goalExpirationDate = goalExpirationDate;
    }

    public LocalDate getGoalStartDate() {
        return goalStartDate;
    }

    public void setGoalStartDate(LocalDate goalStartDate) {
        this.goalStartDate = goalStartDate;
    }

    public LocalDate getGoalEndDate() {
        return goalEndDate;
    }

    public void setGoalEndDate(LocalDate goalEndDate) {
        this.goalEndDate = goalEndDate;
    }

    public String getGoalFiscalYear() {
        return goalFiscalYear;
    }

    public void setGoalFiscalYear(String goalFiscalYear) {
        this.goalFiscalYear = goalFiscalYear;
    }

    public Long getGoalLevel() {
        return goalLevel;
    }

    public void setGoalLevel(Long goalLevel) {
        this.goalLevel = goalLevel;
    }

    public Long gethRId() {
        return hRId;
    }

    public void sethRId(Long hRId) {
        this.hRId = hRId;
    }

    public String gethRTitle() {
        return hRTitle;
    }

    public void sethRTitle(String hRTitle) {
        this.hRTitle = hRTitle;
    }

    public Boolean gethRIsEnabled() {
        return hRIsEnabled;
    }

    public void sethRIsEnabled(Boolean hRIsEnabled) {
        this.hRIsEnabled = hRIsEnabled;
    }

    public Long gethROrgChartId() {
        return hROrgChartId;
    }

    public void sethROrgChartId(Long hROrgChartId) {
        this.hROrgChartId = hROrgChartId;
    }

    public Long gethROrgId() {
        return hROrgId;
    }

    public void sethROrgId(Long hROrgId) {
        this.hROrgId = hROrgId;
    }
}
