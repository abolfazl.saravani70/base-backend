/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Immutable
@Table(name = "ORG_CHART_VIEW")
public class OrganizationChartFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    @Column(name = "IS_VIRTUAL")
    private Boolean isVirtual;

    @Column(name = "FV_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "FK_PV_ORG_LEVEL_ID")
    private Long pvOrgLevelId;

    @Column(name = "PV_ORG_LEVEL_TITLE")
    private String pvOrgLevelTitle;

    @Column(name = "FK_PV_ORG_DEGREE_ID")
    private Long pvOrgDegreeId;

    @Column(name = "PV_ORG_DEGREE_TITLE")
    private String pvOrgDegreeTitle;

    @Column(name = "FK_PV_ORGANIZE_TYPE_ID")
    private Long pvOrganizeTypeId;

    @Column(name = "PV_ORGANIZE_TYPE_TITLE")
    private String pvOrganizeTypeTitle;

    @Column(name = "FK_PV_POSITION_NUMBER_ID")
    private Long pvPositionNumberId;

    @Column(name = "PV_POSITION_NUMBER_TITLE")
    private String pvPositionNumberTitle;

    @Column(name = "FK_PV_POSITION_TITLE_ID")
    private Long pvPositionTitleId;

    @Column(name = "PV_POSITION_TITLE_TITLE")
    private String pvPositionTitleTitle;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_CODE")
    private String parentCode;

    @Column(name = "PARENT_IS_VIRTUAL")
    private Boolean parentIsVirtual;

    @Column(name = "PARENT_FK_PV_TYPE_ID")
    private Long parentPvTypeId;

    @Column(name = "PARENT_PV_TYPE_TITLE")
    private String parentPvTypeTitle;

    @Column(name = "PARENT_FK_PV_ORG_LEVEL_ID")
    private Long parentPvOrgLevelId;

    @Column(name = "PARENT_PV_ORG_LEVEL_TITLE")
    private String parentPvOrgLevelTitle;

    @Column(name = "PARENT_FK_PV_ORG_DEGREE_ID")
    private Long parentPvOrgDegreeId;

    @Column(name = "PARENT_PV_ORG_DEGREE_TITLE")
    private String parentPvOrgDegreeTitle;

    @Column(name = "PARENT_FK_PV_ORGANIZE_TYPE_ID")
    private Long parentPvOrganizeTypeId;

    @Column(name = "PARENT_PV_ORGANIZE_TYPE_TITLE")
    private String parentPvOrganizeTypeTitle;

    @Column(name = "PARENT_FK_PV_POSITION_NUMBER_ID")
    private Long parentPvPositionNumberId;

    @Column(name = "PARENT_PV_POSITION_NUMBER_TITLE")
    private String parentPvPositionNumberTitle;

    @Column(name = "PARENT_FK_PV_POSITION_TITLE_ID")
    private Long parentPvPositionTitleId;

    @Column(name = "PARENT_PV_POSITION_TITLE_TITLE")
    private String parentPvPositionTitleTitle;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getVirtual() {
        return isVirtual;
    }

    public void setVirtual(Boolean virtual) {
        isVirtual = virtual;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public String getPvOrgLevelTitle() {
        return pvOrgLevelTitle;
    }

    public void setPvOrgLevelTitle(String pvOrgLevelTitle) {
        this.pvOrgLevelTitle = pvOrgLevelTitle;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }

    public String getPvOrgDegreeTitle() {
        return pvOrgDegreeTitle;
    }

    public void setPvOrgDegreeTitle(String pvOrgDegreeTitle) {
        this.pvOrgDegreeTitle = pvOrgDegreeTitle;
    }

    public Long getPvOrganizeTypeId() {
        return pvOrganizeTypeId;
    }

    public void setPvOrganizeTypeId(Long pvOrganizeTypeId) {
        this.pvOrganizeTypeId = pvOrganizeTypeId;
    }

    public String getPvOrganizeTypeTitle() {
        return pvOrganizeTypeTitle;
    }

    public void setPvOrganizeTypeTitle(String pvOrganizeTypeTitle) {
        this.pvOrganizeTypeTitle = pvOrganizeTypeTitle;
    }

    public Long getPvPositionNumberId() {
        return pvPositionNumberId;
    }

    public void setPvPositionNumberId(Long pvPositionNumberId) {
        this.pvPositionNumberId = pvPositionNumberId;
    }

    public String getPvPositionNumberTitle() {
        return pvPositionNumberTitle;
    }

    public void setPvPositionNumberTitle(String pvPositionNumberTitle) {
        this.pvPositionNumberTitle = pvPositionNumberTitle;
    }

    public Long getPvPositionTitleId() {
        return pvPositionTitleId;
    }

    public void setPvPositionTitleId(Long pvPositionTitleId) {
        this.pvPositionTitleId = pvPositionTitleId;
    }

    public String getPvPositionTitleTitle() {
        return pvPositionTitleTitle;
    }

    public void setPvPositionTitleTitle(String pvPositionTitleTitle) {
        this.pvPositionTitleTitle = pvPositionTitleTitle;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public Boolean getParentIsVirtual() {
        return parentIsVirtual;
    }

    public void setParentIsVirtual(Boolean parentIsVirtual) {
        this.parentIsVirtual = parentIsVirtual;
    }

    public Long getParentPvTypeId() {
        return parentPvTypeId;
    }

    public void setParentPvTypeId(Long parentPvTypeId) {
        this.parentPvTypeId = parentPvTypeId;
    }

    public String getParentPvTypeTitle() {
        return parentPvTypeTitle;
    }

    public void setParentPvTypeTitle(String parentPvTypeTitle) {
        this.parentPvTypeTitle = parentPvTypeTitle;
    }

    public Long getParentPvOrgLevelId() {
        return parentPvOrgLevelId;
    }

    public void setParentPvOrgLevelId(Long parentPvOrgLevelId) {
        this.parentPvOrgLevelId = parentPvOrgLevelId;
    }

    public String getParentPvOrgLevelTitle() {
        return parentPvOrgLevelTitle;
    }

    public void setParentPvOrgLevelTitle(String parentPvOrgLevelTitle) {
        this.parentPvOrgLevelTitle = parentPvOrgLevelTitle;
    }

    public Long getParentPvOrgDegreeId() {
        return parentPvOrgDegreeId;
    }

    public void setParentPvOrgDegreeId(Long parentPvOrgDegreeId) {
        this.parentPvOrgDegreeId = parentPvOrgDegreeId;
    }

    public String getParentPvOrgDegreeTitle() {
        return parentPvOrgDegreeTitle;
    }

    public void setParentPvOrgDegreeTitle(String parentPvOrgDegreeTitle) {
        this.parentPvOrgDegreeTitle = parentPvOrgDegreeTitle;
    }

    public Long getParentPvOrganizeTypeId() {
        return parentPvOrganizeTypeId;
    }

    public void setParentPvOrganizeTypeId(Long parentPvOrganizeTypeId) {
        this.parentPvOrganizeTypeId = parentPvOrganizeTypeId;
    }

    public String getParentPvOrganizeTypeTitle() {
        return parentPvOrganizeTypeTitle;
    }

    public void setParentPvOrganizeTypeTitle(String parentPvOrganizeTypeTitle) {
        this.parentPvOrganizeTypeTitle = parentPvOrganizeTypeTitle;
    }

    public Long getParentPvPositionNumberId() {
        return parentPvPositionNumberId;
    }

    public void setParentPvPositionNumberId(Long parentPvPositionNumberId) {
        this.parentPvPositionNumberId = parentPvPositionNumberId;
    }

    public String getParentPvPositionNumberTitle() {
        return parentPvPositionNumberTitle;
    }

    public void setParentPvPositionNumberTitle(String parentPvPositionNumberTitle) {
        this.parentPvPositionNumberTitle = parentPvPositionNumberTitle;
    }

    public Long getParentPvPositionTitleId() {
        return parentPvPositionTitleId;
    }

    public void setParentPvPositionTitleId(Long parentPvPositionTitleId) {
        this.parentPvPositionTitleId = parentPvPositionTitleId;
    }

    public String getParentPvPositionTitleTitle() {
        return parentPvPositionTitleTitle;
    }

    public void setParentPvPositionTitleTitle(String parentPvPositionTitleTitle) {
        this.parentPvPositionTitleTitle = parentPvPositionTitleTitle;
    }
}
