/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.system.ProcessStep;
import com.rbp.sayban.model.domainmodel.system.SysRule;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "PROCESS_STEP_SYS_RULE_VIEW")
public class ProcessStepSysRuleFV extends BaseEntity{


    @Column(name = "FK_PROCESS_STEP_ID")
    private Long processStepId;

    @Column(name = "PROCESS_STEP_NAME")
    private String processStepName;

    @Column(name = "PROCESS_STEP_TITLE")
    private String processStepTitle;

    @Column(name = "PROCESS_STEP_CODE")
    private String processStepCode;

    @Column(name = "PROCESS_STEP_KEY")
    private String processStepKey;

    @Column(name = "PROCESS_STEP_NUMBER")
    private String processStepNumber;

    @Column(name = "PROCESS_STEP_TYPE")
    private String processStepType;

    @Column(name = "FK_SYS_RULE_ID")
    private Long sysRuleId;

    @Column(name = "SYS_RULE_TITLE")
    private String sysRuleTitle;

    @Column(name = "SYS_RULE_NAME")
    private String sysRuleName;

    @Column(name = "SYS_RULE_CODE")
    private String sysRuleCode;

    @Column(name = "SYS_RULE_START_DATE")
    private LocalDate sysRuleStartDate;

    @Column(name = "SYS_RULE_EXPIRATION_DATE")
    private LocalDate sysRuleExpirationDate;

    public Long getProcessStepId() {
        return processStepId;
    }

    public void setProcessStepId(Long processStepId) {
        this.processStepId = processStepId;
    }

    public String getProcessStepName() {
        return processStepName;
    }

    public void setProcessStepName(String processStepName) {
        this.processStepName = processStepName;
    }

    public String getProcessStepTitle() {
        return processStepTitle;
    }

    public void setProcessStepTitle(String processStepTitle) {
        this.processStepTitle = processStepTitle;
    }

    public String getProcessStepCode() {
        return processStepCode;
    }

    public void setProcessStepCode(String processStepCode) {
        this.processStepCode = processStepCode;
    }

    public String getProcessStepKey() {
        return processStepKey;
    }

    public void setProcessStepKey(String processStepKey) {
        this.processStepKey = processStepKey;
    }

    public String getProcessStepNumber() {
        return processStepNumber;
    }

    public void setProcessStepNumber(String processStepNumber) {
        this.processStepNumber = processStepNumber;
    }

    public String getProcessStepType() {
        return processStepType;
    }

    public void setProcessStepType(String processStepType) {
        this.processStepType = processStepType;
    }

    public Long getSysRuleId() {
        return sysRuleId;
    }

    public void setSysRuleId(Long sysRuleId) {
        this.sysRuleId = sysRuleId;
    }

    public String getSysRuleTitle() {
        return sysRuleTitle;
    }

    public void setSysRuleTitle(String sysRuleTitle) {
        this.sysRuleTitle = sysRuleTitle;
    }

    public String getSysRuleName() {
        return sysRuleName;
    }

    public void setSysRuleName(String sysRuleName) {
        this.sysRuleName = sysRuleName;
    }

    public String getSysRuleCode() {
        return sysRuleCode;
    }

    public void setSysRuleCode(String sysRuleCode) {
        this.sysRuleCode = sysRuleCode;
    }

    public LocalDate getSysRuleStartDate() {
        return sysRuleStartDate;
    }

    public void setSysRuleStartDate(LocalDate sysRuleStartDate) {
        this.sysRuleStartDate = sysRuleStartDate;
    }

    public LocalDate getSysRuleExpirationDate() {
        return sysRuleExpirationDate;
    }

    public void setSysRuleExpirationDate(LocalDate sysRuleExpirationDate) {
        this.sysRuleExpirationDate = sysRuleExpirationDate;
    }
}
