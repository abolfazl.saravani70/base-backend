/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ORG_VIEW")
public class OrganizationFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "ORG_CODE")
    private String orgCode;

    @Column(name = "ZAMZAM_CODE")
    private String zamzamCode;

    @Column(name = "STRATEGIC_CODE")
    private String strategicCode;

    @Column(name = "IS_ORGANIZED")
    private Boolean isOrganized;

    @Column(name = "FK_PV_ORG_LEVEL_ID")
    private Long pvOrgLevelId;

    @Column(name = "PV_ORG_LEVEL_TITLE")
    private String pvOrgLevelTitle;

    @Column(name = "FK_PV_ORG_DEGREE_ID")
    private Long pvOrgDegreeId;

    @Column(name = "PV_ORG_DEGREE_TITLE")
    private String pvOrgDegreeTitle;

    @Column(name = "FK_PV_ORG_TYPE_ID")
    private Long pvOrgTypeId;

    @Column(name = "PV_ORG_TYPE_TITLE")
    private String pvOrgTypeTitle;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TITLE")
    private String parentTitle;

    @Column(name = "PARENT_ORG_CODE")
    private String parentOrgCode;

    @Column(name = "PARENT_ZAMZAM_CODE")
    private String parentZamzamCode;

    @Column(name = "PARENT_STRATEGIC_CODE")
    private String parentStrategicCode;

    @Column(name = "PARENT_IS_ORGANIZED")
    private Boolean parentIsOrganized;

    @Column(name = "PARENT_FK_PV_ORG_LEVEL_ID")
    private Long parentPvOrgLevelId;

    @Column(name = "PARENT_PV_ORG_LEVEL_TITLE")
    private String parentPvOrgLevelTitle;

    @Column(name = "PARENT_FK_PV_ORG_DEGREE_ID")
    private Long parentPvOrgDegreeId;

    @Column(name = "PARENT_PV_ORG_DEGREE_TITLE")
    private String parentPvOrgDegreeTitle;

    @Column(name = "PARENT_FK_PV_ORG_TYPE_ID")
    private Long parentPvOrgTypeId;

    @Column(name = "PARENT_PV_ORG_TYPE_TITLE")
    private String parentPvOrgTypeTitle;

    @Column(name = "PARENT_FK_STAKEHOLDERS_ID")
    private Long parentStakeholderId;

    @Column(name = "PARENT_STAKEHOLDERS_TYPE")
    private String parentStakeholderType;

    @Column(name = "PARENT_STAKEHOLDERS_IS_PERSON")
    private Boolean parentStakeholderIsPerson;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getZamzamCode() {
        return zamzamCode;
    }

    public void setZamzamCode(String zamzamCode) {
        this.zamzamCode = zamzamCode;
    }

    public String getStrategicCode() {
        return strategicCode;
    }

    public void setStrategicCode(String strategicCode) {
        this.strategicCode = strategicCode;
    }

    public Boolean getOrganized() {
        return isOrganized;
    }

    public void setOrganized(Boolean organized) {
        isOrganized = organized;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public String getPvOrgLevelTitle() {
        return pvOrgLevelTitle;
    }

    public void setPvOrgLevelTitle(String pvOrgLevelTitle) {
        this.pvOrgLevelTitle = pvOrgLevelTitle;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }

    public String getPvOrgDegreeTitle() {
        return pvOrgDegreeTitle;
    }

    public void setPvOrgDegreeTitle(String pvOrgDegreeTitle) {
        this.pvOrgDegreeTitle = pvOrgDegreeTitle;
    }

    public Long getPvOrgTypeId() {
        return pvOrgTypeId;
    }

    public void setPvOrgTypeId(Long pvOrgTypeId) {
        this.pvOrgTypeId = pvOrgTypeId;
    }

    public String getPvOrgTypeTitle() {
        return pvOrgTypeTitle;
    }

    public void setPvOrgTypeTitle(String pvOrgTypeTitle) {
        this.pvOrgTypeTitle = pvOrgTypeTitle;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentOrgCode() {
        return parentOrgCode;
    }

    public void setParentOrgCode(String parentOrgCode) {
        this.parentOrgCode = parentOrgCode;
    }

    public String getParentZamzamCode() {
        return parentZamzamCode;
    }

    public void setParentZamzamCode(String parentZamzamCode) {
        this.parentZamzamCode = parentZamzamCode;
    }

    public String getParentStrategicCode() {
        return parentStrategicCode;
    }

    public void setParentStrategicCode(String parentStrategicCode) {
        this.parentStrategicCode = parentStrategicCode;
    }

    public Boolean getParentIsOrganized() {
        return parentIsOrganized;
    }

    public void setParentIsOrganized(Boolean parentIsOrganized) {
        this.parentIsOrganized = parentIsOrganized;
    }

    public Long getParentPvOrgLevelId() {
        return parentPvOrgLevelId;
    }

    public void setParentPvOrgLevelId(Long parentPvOrgLevelId) {
        this.parentPvOrgLevelId = parentPvOrgLevelId;
    }

    public String getParentPvOrgLevelTitle() {
        return parentPvOrgLevelTitle;
    }

    public void setParentPvOrgLevelTitle(String parentPvOrgLevelTitle) {
        this.parentPvOrgLevelTitle = parentPvOrgLevelTitle;
    }

    public Long getParentPvOrgDegreeId() {
        return parentPvOrgDegreeId;
    }

    public void setParentPvOrgDegreeId(Long parentPvOrgDegreeId) {
        this.parentPvOrgDegreeId = parentPvOrgDegreeId;
    }

    public String getParentPvOrgDegreeTitle() {
        return parentPvOrgDegreeTitle;
    }

    public void setParentPvOrgDegreeTitle(String parentPvOrgDegreeTitle) {
        this.parentPvOrgDegreeTitle = parentPvOrgDegreeTitle;
    }

    public Long getParentPvOrgTypeId() {
        return parentPvOrgTypeId;
    }

    public void setParentPvOrgTypeId(Long parentPvOrgTypeId) {
        this.parentPvOrgTypeId = parentPvOrgTypeId;
    }

    public String getParentPvOrgTypeTitle() {
        return parentPvOrgTypeTitle;
    }

    public void setParentPvOrgTypeTitle(String parentPvOrgTypeTitle) {
        this.parentPvOrgTypeTitle = parentPvOrgTypeTitle;
    }

    public Long getParentStakeholderId() {
        return parentStakeholderId;
    }

    public void setParentStakeholderId(Long parentStakeholderId) {
        this.parentStakeholderId = parentStakeholderId;
    }

    public String getParentStakeholderType() {
        return parentStakeholderType;
    }

    public void setParentStakeholderType(String parentStakeholderType) {
        this.parentStakeholderType = parentStakeholderType;
    }

    public Boolean getParentStakeholderIsPerson() {
        return parentStakeholderIsPerson;
    }

    public void setParentStakeholderIsPerson(Boolean parentStakeholderIsPerson) {
        this.parentStakeholderIsPerson = parentStakeholderIsPerson;
    }
}
