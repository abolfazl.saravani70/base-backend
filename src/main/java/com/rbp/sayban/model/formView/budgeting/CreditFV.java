/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.Allocation;
import com.rbp.sayban.model.domainmodel.budgeting.FundSharing;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "CREDIT_VIEW")
public class CreditFV extends BaseEntity {

    @Column(name = "FK_FUND_SHARING_ID")
    private Long fundSharingId;

    @Column(name = "FUND_SHARING_ACCEPTED_AMOUNT")
    private Long fundSharingAcceptedAmount;

    @Column(name = "FUND_SHARING_ACCEPTED_DATE")
    private LocalDate fundSharingAcceptedDate;

    @Column(name = "FK_ALLOCATION_ID")
    private Long allocationId;

    @Column(name = "ALLOCATION_NUMBER$")
    private String allocationNumber;

    @Column(name = "ALLOCATION_ALLOCATION_DATE")
    private LocalDate allocationDate;

    @Column(name = "ALLOCATION_TITLE")
    private String allocationTitle;

    public Long getFundSharingId() {
        return fundSharingId;
    }

    public void setFundSharingId(Long fundSharingId) {
        this.fundSharingId = fundSharingId;
    }

    public Long getFundSharingAcceptedAmount() {
        return fundSharingAcceptedAmount;
    }

    public void setFundSharingAcceptedAmount(Long fundSharingAcceptedAmount) {
        this.fundSharingAcceptedAmount = fundSharingAcceptedAmount;
    }

    public LocalDate getFundSharingAcceptedDate() {
        return fundSharingAcceptedDate;
    }

    public void setFundSharingAcceptedDate(LocalDate fundSharingAcceptedDate) {
        this.fundSharingAcceptedDate = fundSharingAcceptedDate;
    }

    public Long getAllocationId() {
        return allocationId;
    }

    public void setAllocationId(Long allocationId) {
        this.allocationId = allocationId;
    }

    public String getAllocationNumber() {
        return allocationNumber;
    }

    public void setAllocationNumber(String allocationNumber) {
        this.allocationNumber = allocationNumber;
    }

    public LocalDate getAllocationDate() {
        return allocationDate;
    }

    public void setAllocationDate(LocalDate allocationDate) {
        this.allocationDate = allocationDate;
    }

    public String getAllocationTitle() {
        return allocationTitle;
    }

    public void setAllocationTitle(String allocationTitle) {
        this.allocationTitle = allocationTitle;
    }
}
