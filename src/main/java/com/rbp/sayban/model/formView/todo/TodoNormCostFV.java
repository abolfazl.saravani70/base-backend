/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.NormCostJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.todo.Todo;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TODO_NORM_COST_VIEW")
public class TodoNormCostFV extends EvalStateBaseEntity {

    @Column(name = "COUNT")
    private Long count;

    @Column(name = "MINIMUM")
    private Long minimum;

    @Column(name = "MAXIMUM")
    private Long maximum;

    @Column(name = "AVERAGE")
    private Long average;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "RATE")
    private Double rate;

    @Column(name = "PREDICATE")
    private Long predicate;

    @Column(name = "FK_ORG_CORTEX_ID")
    private Long cortexId;

    @Column(name = "FK_TODO_ID")
    private Long todoId;

    @Column(name = "TODO_FK_PV_TYPE_ID")
    private Long  todoPvTypeId;

    @Column(name = "TODO_PV_TYPE_TITLE")
    private String  todoPvTypeTitle;

    @Column(name = "TODO_NAME")
    private String todoName;

    @Column(name = "TODO_TITLE")
    private String todoTitle;

    @Column(name = "TODO_IS_ROOT")
    private Boolean todoIsRoot;

    @Column(name = "TODO_INITIAL_PERCENTAGE")
    private Double todoInitialPercentage;

    @Column(name = "TODO_PERCENTAGE")
    private Double todoPercentage;

    @Column(name = "TODO_WEIGHT")
    private Long todoWeight;

    @Column(name = "TODO_VOLUME")
    private Long todoVolume;

    @Column(name = "TODO_PRIORITY")
    private Integer todoPriority;

    @Column(name = "TODO_IMPORTANCE")
    private Double todoImportance;

    @Column(name = "TODO_ACTUAL_START_DATE")
    private LocalDate todoActualStartDate;

    @Column(name = "TODO_ACTUAL_END_DATE")
    private LocalDate todoActualEndDate;

    @Column(name = "TODO_EXPIRATION_DATE")
    private LocalDate todoExpirationDate;

    @Column(name = "TODO_START_DATE")
    private LocalDate todoStartDate;

    @Column(name = "TODO_END_DATE")
    private LocalDate todoEndDate;

    @Column(name = "TODO_FISCAL_YEAR")
    private String todoFiscalYear;

    @Column(name = "TODO_LEVEL$")
    private Long todoLevel;

    @Column(name = "FK_NORM_COST_ID")
    private Long normCostId;

    @Column(name ="NORM_COST_NAME" )
    private String normCostName;

    @Column (name="NORM_COST_TITLE")
    private String normCostTitle;

    @Column(name="NORM_COST_COST")
    private Long normCostCost;

    @Column(name="NORM_COST_RATE")
    private Long normCostRate;

    @Column (name="NORM_COST_PRIORITORY")
    private Integer normCostPrioritory;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getMinimum() {
        return minimum;
    }

    public void setMinimum(Long minimum) {
        this.minimum = minimum;
    }

    public Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        this.maximum = maximum;
    }

    public Long getAverage() {
        return average;
    }

    public void setAverage(Long average) {
        this.average = average;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Long getPredicate() {
        return predicate;
    }

    public void setPredicate(Long predicate) {
        this.predicate = predicate;
    }

    public Long getCortexId() {
        return cortexId;
    }

    public void setCortexId(Long cortexId) {
        this.cortexId = cortexId;
    }

    public Long getTodoId() {
        return todoId;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    public Long getTodoPvTypeId() {
        return todoPvTypeId;
    }

    public void setTodoPvTypeId(Long todoPvTypeId) {
        this.todoPvTypeId = todoPvTypeId;
    }

    public String getTodoPvTypeTitle() {
        return todoPvTypeTitle;
    }

    public void setTodoPvTypeTitle(String todoPvTypeTitle) {
        this.todoPvTypeTitle = todoPvTypeTitle;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    public Boolean getTodoIsRoot() {
        return todoIsRoot;
    }

    public void setTodoIsRoot(Boolean todoIsRoot) {
        this.todoIsRoot = todoIsRoot;
    }

    public Double getTodoInitialPercentage() {
        return todoInitialPercentage;
    }

    public void setTodoInitialPercentage(Double todoInitialPercentage) {
        this.todoInitialPercentage = todoInitialPercentage;
    }

    public Double getTodoPercentage() {
        return todoPercentage;
    }

    public void setTodoPercentage(Double todoPercentage) {
        this.todoPercentage = todoPercentage;
    }

    public Long getTodoWeight() {
        return todoWeight;
    }

    public void setTodoWeight(Long todoWeight) {
        this.todoWeight = todoWeight;
    }

    public Long getTodoVolume() {
        return todoVolume;
    }

    public void setTodoVolume(Long todoVolume) {
        this.todoVolume = todoVolume;
    }

    public Integer getTodoPriority() {
        return todoPriority;
    }

    public void setTodoPriority(Integer todoPriority) {
        this.todoPriority = todoPriority;
    }

    public Double getTodoImportance() {
        return todoImportance;
    }

    public void setTodoImportance(Double todoImportance) {
        this.todoImportance = todoImportance;
    }

    public LocalDate getTodoActualStartDate() {
        return todoActualStartDate;
    }

    public void setTodoActualStartDate(LocalDate todoActualStartDate) {
        this.todoActualStartDate = todoActualStartDate;
    }

    public LocalDate getTodoActualEndDate() {
        return todoActualEndDate;
    }

    public void setTodoActualEndDate(LocalDate todoActualEndDate) {
        this.todoActualEndDate = todoActualEndDate;
    }

    public LocalDate getTodoExpirationDate() {
        return todoExpirationDate;
    }

    public void setTodoExpirationDate(LocalDate todoExpirationDate) {
        this.todoExpirationDate = todoExpirationDate;
    }

    public LocalDate getTodoStartDate() {
        return todoStartDate;
    }

    public void setTodoStartDate(LocalDate todoStartDate) {
        this.todoStartDate = todoStartDate;
    }

    public LocalDate getTodoEndDate() {
        return todoEndDate;
    }

    public void setTodoEndDate(LocalDate todoEndDate) {
        this.todoEndDate = todoEndDate;
    }

    public String getTodoFiscalYear() {
        return todoFiscalYear;
    }

    public void setTodoFiscalYear(String todoFiscalYear) {
        this.todoFiscalYear = todoFiscalYear;
    }

    public Long getTodoLevel() {
        return todoLevel;
    }

    public void setTodoLevel(Long todoLevel) {
        this.todoLevel = todoLevel;
    }

    public Long getNormCostId() {
        return normCostId;
    }

    public void setNormCostId(Long normCostId) {
        this.normCostId = normCostId;
    }

    public String getNormCostName() {
        return normCostName;
    }

    public void setNormCostName(String normCostName) {
        this.normCostName = normCostName;
    }

    public String getNormCostTitle() {
        return normCostTitle;
    }

    public void setNormCostTitle(String normCostTitle) {
        this.normCostTitle = normCostTitle;
    }

    public Long getNormCostCost() {
        return normCostCost;
    }

    public void setNormCostCost(Long normCostCost) {
        this.normCostCost = normCostCost;
    }

    public Long getNormCostRate() {
        return normCostRate;
    }

    public void setNormCostRate(Long normCostRate) {
        this.normCostRate = normCostRate;
    }

    public Integer getNormCostPrioritory() {
        return normCostPrioritory;
    }

    public void setNormCostPrioritory(Integer normCostPrioritory) {
        this.normCostPrioritory = normCostPrioritory;
    }
}
