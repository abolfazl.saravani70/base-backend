package com.rbp.sayban.model.formView.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.LocationJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.planning.Plan;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "PLAN_LOCATION_VIEW")
public class PlanLocationFV extends EvalStateBaseEntity {

    @Column(name = "CAPACITY")
    private Long capacity;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "USAGE_TYPE")
    private String usageType;

    @Column(name = "FK_PLAN_ID")
    private Long planId;

    @Column(name = "PLAN_FK_PV_TYPE_ID")
    private Long planPvTypeId;

    @Column(name = "PLAN_PV_TYPE_TITLE")
    private String planPvTypeTitle;

    @Column(name = "PLAN_NAME")
    private String planName;

    @Column(name = "PLAN_TITLE")
    private String planTitle;

    @Column(name = "PLAN_IS_ROOT")
    private Boolean planIsRoot;

    @Column(name = "PLAN_INITIAL_PERCENTAGE")
    private Double planInitialPercentage;

    @Column(name = "PLAN_PERCENTAGE")
    private Double planPercentage;

    @Column(name = "PLAN_WEIGHT")
    private Long planWeight;

    @Column(name = "PLAN_VOLUME")
    private Long planVolume;

    @Column(name = "PLAN_PRIORITY")
    private Integer planPriority;

    @Column(name = "PLAN_IMPORTANCE")
    private Double planImportance;

    @Column(name = "PLAN_ACTUAL_START_DATE")
    private LocalDate planActualStartDate;

    @Column(name = "PLAN_ACTUAL_END_DATE")
    private LocalDate planActualEndDate;

    @Column(name = "PLAN_EXPIRATION_DATE")
    private LocalDate planExpirationDate;

    @Column(name = "PLAN_START_DATE")
    private LocalDate planStartDate;

    @Column(name = "PLAN_END_DATE")
    private LocalDate planEndDate;

    @Column(name = "PLAN_FISCAL_YEAR")
    private String planFiscalYear;

    @Column(name = "PLAN_LEVEL$")
    private Long planLevel;

    @Column(name = "FK_LOCATION_ID")
    private Long locationId;

    @Column(name = "LOCATION_NAME")
    private String locationName;

    @Column(name = "LOCATION_NUMBER$")
    private String locationNumber;

    @Column(name = "LOCATION_TITLE")
    private String locationTitle;

    @Column(name = "LOCATION_Type")
    private String locationType;

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanPvTypeId() {
        return planPvTypeId;
    }

    public void setPlanPvTypeId(Long planPvTypeId) {
        this.planPvTypeId = planPvTypeId;
    }

    public String getPlanPvTypeTitle() {
        return planPvTypeTitle;
    }

    public void setPlanPvTypeTitle(String planPvTypeTitle) {
        this.planPvTypeTitle = planPvTypeTitle;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanTitle() {
        return planTitle;
    }

    public void setPlanTitle(String planTitle) {
        this.planTitle = planTitle;
    }

    public Boolean getPlanIsRoot() {
        return planIsRoot;
    }

    public void setPlanIsRoot(Boolean planIsRoot) {
        this.planIsRoot = planIsRoot;
    }

    public Double getPlanInitialPercentage() {
        return planInitialPercentage;
    }

    public void setPlanInitialPercentage(Double planInitialPercentage) {
        this.planInitialPercentage = planInitialPercentage;
    }

    public Double getPlanPercentage() {
        return planPercentage;
    }

    public void setPlanPercentage(Double planPercentage) {
        this.planPercentage = planPercentage;
    }

    public Long getPlanWeight() {
        return planWeight;
    }

    public void setPlanWeight(Long planWeight) {
        this.planWeight = planWeight;
    }

    public Long getPlanVolume() {
        return planVolume;
    }

    public void setPlanVolume(Long planVolume) {
        this.planVolume = planVolume;
    }

    public Integer getPlanPriority() {
        return planPriority;
    }

    public void setPlanPriority(Integer planPriority) {
        this.planPriority = planPriority;
    }

    public Double getPlanImportance() {
        return planImportance;
    }

    public void setPlanImportance(Double planImportance) {
        this.planImportance = planImportance;
    }

    public LocalDate getPlanActualStartDate() {
        return planActualStartDate;
    }

    public void setPlanActualStartDate(LocalDate planActualStartDate) {
        this.planActualStartDate = planActualStartDate;
    }

    public LocalDate getPlanActualEndDate() {
        return planActualEndDate;
    }

    public void setPlanActualEndDate(LocalDate planActualEndDate) {
        this.planActualEndDate = planActualEndDate;
    }

    public LocalDate getPlanExpirationDate() {
        return planExpirationDate;
    }

    public void setPlanExpirationDate(LocalDate planExpirationDate) {
        this.planExpirationDate = planExpirationDate;
    }

    public LocalDate getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(LocalDate planStartDate) {
        this.planStartDate = planStartDate;
    }

    public LocalDate getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(LocalDate planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanFiscalYear() {
        return planFiscalYear;
    }

    public void setPlanFiscalYear(String planFiscalYear) {
        this.planFiscalYear = planFiscalYear;
    }

    public Long getPlanLevel() {
        return planLevel;
    }

    public void setPlanLevel(Long planLevel) {
        this.planLevel = planLevel;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public String getLocationTitle() {
        return locationTitle;
    }

    public void setLocationTitle(String locationTitle) {
        this.locationTitle = locationTitle;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
}
