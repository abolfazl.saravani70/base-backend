/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.project;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.project.Contract;
import com.rbp.sayban.model.domainmodel.project.Project;
import com.rbp.sayban.model.domainmodel.system.Document;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "CONTRACT_VIEW")
public class ContractFV extends BaseEntity {

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "TITLE")
    private String parentTitle;

    @Column(name = "NUMBER$")
    private String parentNumber;

    @Column(name = "TEXT")
    private String parentText;

    @Column(name = "CONTRACTOR")
    private String parentContractor;

    @Column(name = "CONTRACTEE")
    private String parentContractee;

    @Column(name = "NUMBER_OF_SECTION")
    private Long parentNumberOfSection;

    @Column(name = "HAS_ATTACHMENT")
    private Boolean parentIsAttachment;

    @Column(name = "IS_COMPLEMENT")
    private Boolean parentIsComplement;

    @Column(name = "DOCUMENT")
    private Document parentDocument;

    @Column(name = "NUMBER_COPY")
    private Long parentNumberCopy;

    @Column(name = "VERSION_OF_CONTRACT")
    private Long parentVersionOfContract;

    @Column(name = "FK_PROJECT_ID")
    private Long projectId;

    @Column(name = "PROJECT_FK_PV_TYPE_ID")
    private Long projectPvTypeId;

    @Column(name = "PROJECT_PV_TYPE_TITLE")
    private String projectPvTypeTitle;

    @Column(name = "PROJECT_NAME")
    private String projectName;

    @Column(name = "PROJECT_TITLE")
    private String projectTitle;

    @Column(name = "PROJECT_IS_ROOT")
    private Boolean projectIsRoot;

    @Column(name = "PROJECT_INITIAL_PERCENTAGE")
    private Double projectInitialPercentage;

    @Column(name = "PROJECT_PERCENTAGE")
    private Double projectPercentage;

    @Column(name = "PROJECT_WEIGHT")
    private Long projectWeight;

    @Column(name = "PROJECT_VOLUME")
    private Long projectVolume;

    @Column(name = "PROJECT_PRIORITY")
    private Integer projectPriority;

    @Column(name = "PROJECT_IMPORTANCE")
    private Double projectImportance;

    @Column(name = "PROJECT_ACTUAL_START_DATE")
    private LocalDate projectActualStartDate;

    @Column(name = "PROJECT_ACTUAL_END_DATE")
    private LocalDate projectActualEndDate;

    @Column(name = "PROJECT_EXPIRATION_DATE")
    private LocalDate projectExpirationDate;

    @Column(name = "PROJECT_START_DATE")
    private LocalDate projectStartDate;

    @Column(name = "PROJECT_END_DATE")
    private LocalDate projectEndDate;

    @Column(name = "PROJECT_FISCAL_YEAR")
    private String projectFiscalYear;

    @Column(name = "PROJECT_LEVEL$")
    private Long projectLevel;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentNumber() {
        return parentNumber;
    }

    public void setParentNumber(String parentNumber) {
        this.parentNumber = parentNumber;
    }

    public String getParentText() {
        return parentText;
    }

    public void setParentText(String parentText) {
        this.parentText = parentText;
    }

    public String getParentContractor() {
        return parentContractor;
    }

    public void setParentContractor(String parentContractor) {
        this.parentContractor = parentContractor;
    }

    public String getParentContractee() {
        return parentContractee;
    }

    public void setParentContractee(String parentContractee) {
        this.parentContractee = parentContractee;
    }

    public Long getParentNumberOfSection() {
        return parentNumberOfSection;
    }

    public void setParentNumberOfSection(Long parentNumberOfSection) {
        this.parentNumberOfSection = parentNumberOfSection;
    }

    public Boolean getParentIsAttachment() {
        return parentIsAttachment;
    }

    public void setParentIsAttachment(Boolean parentIsAttachment) {
        this.parentIsAttachment = parentIsAttachment;
    }

    public Boolean getParentIsComplement() {
        return parentIsComplement;
    }

    public void setParentIsComplement(Boolean parentIsComplement) {
        this.parentIsComplement = parentIsComplement;
    }

    public Document getParentDocument() {
        return parentDocument;
    }

    public void setParentDocument(Document parentDocument) {
        this.parentDocument = parentDocument;
    }

    public Long getParentNumberCopy() {
        return parentNumberCopy;
    }

    public void setParentNumberCopy(Long parentNumberCopy) {
        this.parentNumberCopy = parentNumberCopy;
    }

    public Long getParentVersionOfContract() {
        return parentVersionOfContract;
    }

    public void setParentVersionOfContract(Long parentVersionOfContract) {
        this.parentVersionOfContract = parentVersionOfContract;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getProjectPvTypeId() {
        return projectPvTypeId;
    }

    public void setProjectPvTypeId(Long projectPvTypeId) {
        this.projectPvTypeId = projectPvTypeId;
    }

    public String getProjectPvTypeTitle() {
        return projectPvTypeTitle;
    }

    public void setProjectPvTypeTitle(String projectPvTypeTitle) {
        this.projectPvTypeTitle = projectPvTypeTitle;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public Boolean getProjectIsRoot() {
        return projectIsRoot;
    }

    public void setProjectIsRoot(Boolean projectIsRoot) {
        this.projectIsRoot = projectIsRoot;
    }

    public Double getProjectInitialPercentage() {
        return projectInitialPercentage;
    }

    public void setProjectInitialPercentage(Double projectInitialPercentage) {
        this.projectInitialPercentage = projectInitialPercentage;
    }

    public Double getProjectPercentage() {
        return projectPercentage;
    }

    public void setProjectPercentage(Double projectPercentage) {
        this.projectPercentage = projectPercentage;
    }

    public Long getProjectWeight() {
        return projectWeight;
    }

    public void setProjectWeight(Long projectWeight) {
        this.projectWeight = projectWeight;
    }

    public Long getProjectVolume() {
        return projectVolume;
    }

    public void setProjectVolume(Long projectVolume) {
        this.projectVolume = projectVolume;
    }

    public Integer getProjectPriority() {
        return projectPriority;
    }

    public void setProjectPriority(Integer projectPriority) {
        this.projectPriority = projectPriority;
    }

    public Double getProjectImportance() {
        return projectImportance;
    }

    public void setProjectImportance(Double projectImportance) {
        this.projectImportance = projectImportance;
    }

    public LocalDate getProjectActualStartDate() {
        return projectActualStartDate;
    }

    public void setProjectActualStartDate(LocalDate projectActualStartDate) {
        this.projectActualStartDate = projectActualStartDate;
    }

    public LocalDate getProjectActualEndDate() {
        return projectActualEndDate;
    }

    public void setProjectActualEndDate(LocalDate projectActualEndDate) {
        this.projectActualEndDate = projectActualEndDate;
    }

    public LocalDate getProjectExpirationDate() {
        return projectExpirationDate;
    }

    public void setProjectExpirationDate(LocalDate projectExpirationDate) {
        this.projectExpirationDate = projectExpirationDate;
    }

    public LocalDate getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(LocalDate projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public LocalDate getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(LocalDate projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectFiscalYear() {
        return projectFiscalYear;
    }

    public void setProjectFiscalYear(String projectFiscalYear) {
        this.projectFiscalYear = projectFiscalYear;
    }

    public Long getProjectLevel() {
        return projectLevel;
    }

    public void setProjectLevel(Long projectLevel) {
        this.projectLevel = projectLevel;
    }
}
