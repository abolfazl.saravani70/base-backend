/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "EXPENSE_VIEW")
public class ExpenseFV extends StereotypeBaseEntity {

    @Column(name = "FK_BUDGET_ID")
    private Long budgetId;

    @Column(name = "BUDGET_NAME")
    private String budgetName;

    @Column(name = "BUDGET_TITLE")
    private String budgetTitle;

    @Column(name = "BUDGET_IS_ROOT")
    private Boolean budgetIsRoot;

    @Column(name = "BUDGET_INITIAL_PERCENTAGE")
    private Double budgetInitialPercentage;

    @Column(name = "BUDGET_PERCENTAGE")
    private Double budgetPercentage;

    @Column(name = "BUDGET_WEIGHT")
    private Long budgetWeight;

    @Column(name = "BUDGET_VOLUME")
    private Long budgetVolume;

    @Column(name = "BUDGET_PRIORITY")
    private Integer budgetPriority;

    @Column(name = "BUDGET_IMPORTANCE")
    private Double budgetImportance;

    @Column(name = "BUDGET_ACTUAL_START_DATE")
    private LocalDate budgetActualStartDate;

    @Column(name = "BUDGET_ACTUAL_END_DATE")
    private LocalDate budgetActualEndDate;

    @Column(name = "BUDGET_EXPIRATION_DATE")
    private LocalDate budgetExpirationDate;

    @Column(name = "BUDGET_START_DATE")
    private LocalDate budgetStartDate;

    @Column(name = "BUDGET_END_DATE")
    private LocalDate budgetEndDate;

    @Column(name = "BUDGET_FISCAL_YEAR")
    private String budgetFiscalYear;

    @Column(name = "BUDGET_LEVEL$")
    private Long budgetLevel;

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgetTitle() {
        return budgetTitle;
    }

    public void setBudgetTitle(String budgetTitle) {
        this.budgetTitle = budgetTitle;
    }

    public Boolean getBudgetIsRoot() {
        return budgetIsRoot;
    }

    public void setBudgetIsRoot(Boolean budgetIsRoot) {
        this.budgetIsRoot = budgetIsRoot;
    }

    public Double getBudgetInitialPercentage() {
        return budgetInitialPercentage;
    }

    public void setBudgetInitialPercentage(Double budgetInitialPercentage) {
        this.budgetInitialPercentage = budgetInitialPercentage;
    }

    public Double getBudgetPercentage() {
        return budgetPercentage;
    }

    public void setBudgetPercentage(Double budgetPercentage) {
        this.budgetPercentage = budgetPercentage;
    }

    public Long getBudgetWeight() {
        return budgetWeight;
    }

    public void setBudgetWeight(Long budgetWeight) {
        this.budgetWeight = budgetWeight;
    }

    public Long getBudgetVolume() {
        return budgetVolume;
    }

    public void setBudgetVolume(Long budgetVolume) {
        this.budgetVolume = budgetVolume;
    }

    public Integer getBudgetPriority() {
        return budgetPriority;
    }

    public void setBudgetPriority(Integer budgetPriority) {
        this.budgetPriority = budgetPriority;
    }

    public Double getBudgetImportance() {
        return budgetImportance;
    }

    public void setBudgetImportance(Double budgetImportance) {
        this.budgetImportance = budgetImportance;
    }

    public LocalDate getBudgetActualStartDate() {
        return budgetActualStartDate;
    }

    public void setBudgetActualStartDate(LocalDate budgetActualStartDate) {
        this.budgetActualStartDate = budgetActualStartDate;
    }

    public LocalDate getBudgetActualEndDate() {
        return budgetActualEndDate;
    }

    public void setBudgetActualEndDate(LocalDate budgetActualEndDate) {
        this.budgetActualEndDate = budgetActualEndDate;
    }

    public LocalDate getBudgetExpirationDate() {
        return budgetExpirationDate;
    }

    public void setBudgetExpirationDate(LocalDate budgetExpirationDate) {
        this.budgetExpirationDate = budgetExpirationDate;
    }

    public LocalDate getBudgetStartDate() {
        return budgetStartDate;
    }

    public void setBudgetStartDate(LocalDate budgetStartDate) {
        this.budgetStartDate = budgetStartDate;
    }

    public LocalDate getBudgetEndDate() {
        return budgetEndDate;
    }

    public void setBudgetEndDate(LocalDate budgetEndDate) {
        this.budgetEndDate = budgetEndDate;
    }

    public String getBudgetFiscalYear() {
        return budgetFiscalYear;
    }

    public void setBudgetFiscalYear(String budgetFiscalYear) {
        this.budgetFiscalYear = budgetFiscalYear;
    }

    public Long getBudgetLevel() {
        return budgetLevel;
    }

    public void setBudgetLevel(Long budgetLevel) {
        this.budgetLevel = budgetLevel;
    }
}
