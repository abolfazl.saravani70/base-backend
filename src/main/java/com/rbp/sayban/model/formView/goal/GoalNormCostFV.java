/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.goal;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.NormCostJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.domainmodel.norm.NormCost;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "GOAL_NORM_COST_VIEW")
public class GoalNormCostFV extends EvalStateBaseEntity {

    @Column(name = "FK_GOAL_ID")
    private Long goalId;

    @Column(name = "GOAL_FK_PV_TYPE_ID")
    private Long goalPvTypeId;

    @Column(name = "GOAL_PV_TYPE_TITLE")
    private String goalPvTypeTitle;

    @Column(name = "GOAL_NAME")
    private String goalName;

    @Column(name = "GOAL_TITLE")
    private String goalTitle;

    @Column(name = "GOAL_IS_ROOT")
    private Boolean goalIsRoot;

    @Column(name = "GOAL_INITIAL_PERCENTAGE")
    private Double goalInitialPercentage;

    @Column(name = "GOAL_PERCENTAGE")
    private Double goalPercentage;

    @Column(name = "GOAL_WEIGHT")
    private Long goalWeight;

    @Column(name = "GOAL_VOLUME")
    private Long goalVolume;

    @Column(name = "GOAL_PRIORITY")
    private Integer goalPriority;

    @Column(name = "GOAL_IMPORTANCE")
    private Double goalImportance;

    @Column(name = "GOAL_ACTUAL_START_DATE")
    private LocalDate goalActualStartDate;

    @Column(name = "GOAL_ACTUAL_END_DATE")
    private LocalDate goalActualEndDate;

    @Column(name = "GOAL_EXPIRATION_DATE")
    private LocalDate goalExpirationDate;

    @Column(name = "GOAL_START_DATE")
    private LocalDate goalStartDate;

    @Column(name = "GOAL_END_DATE")
    private LocalDate goalEndDate;

    @Column(name = "GOAL_FISCAL_YEAR")
    private String goalFiscalYear;

    @Column(name = "GOAL_LEVEL$")
    private Long goalLevel;

    @Column(name = "COUNT")
    private Long count;

    @Column(name = "MINIMUM")
    private Long minimum;

    @Column(name = "MAXIMUM")
    private Long maximum;

    @Column(name = "AVERAGE")
    private Long average;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "RATE")
    private Double rate;

    @Column(name = "PREDICATE")
    private Long predicate;

    @Column(name = "FK_ORG_CORTEX_ID")
    private Long cortexId;

    @Column(name = "FK_NORM_COST_ID")
    private Long normCostId;

    @Column(name ="NORM_COST_NAME" )
    private String normCostName;

    @Column (name="NORM_COST_TITLE")
    private String normCostTitle;

    @Column(name="NORM_COST_COST")
    private Long normCostCost;

    @Column(name="NORM_COST_RATE")
    private Long normCostRate;

    @Column (name="NORM_COST_PRIORITORY")
    private Integer normCostPrioritory;

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }

    public Long getGoalPvTypeId() {
        return goalPvTypeId;
    }

    public void setGoalPvTypeId(Long goalPvTypeId) {
        this.goalPvTypeId = goalPvTypeId;
    }

    public String getGoalPvTypeTitle() {
        return goalPvTypeTitle;
    }

    public void setGoalPvTypeTitle(String goalPvTypeTitle) {
        this.goalPvTypeTitle = goalPvTypeTitle;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getGoalTitle() {
        return goalTitle;
    }

    public void setGoalTitle(String goalTitle) {
        this.goalTitle = goalTitle;
    }

    public Boolean getGoalIsRoot() {
        return goalIsRoot;
    }

    public void setGoalIsRoot(Boolean goalIsRoot) {
        this.goalIsRoot = goalIsRoot;
    }

    public Double getGoalInitialPercentage() {
        return goalInitialPercentage;
    }

    public void setGoalInitialPercentage(Double goalInitialPercentage) {
        this.goalInitialPercentage = goalInitialPercentage;
    }

    public Double getGoalPercentage() {
        return goalPercentage;
    }

    public void setGoalPercentage(Double goalPercentage) {
        this.goalPercentage = goalPercentage;
    }

    public Long getGoalWeight() {
        return goalWeight;
    }

    public void setGoalWeight(Long goalWeight) {
        this.goalWeight = goalWeight;
    }

    public Long getGoalVolume() {
        return goalVolume;
    }

    public void setGoalVolume(Long goalVolume) {
        this.goalVolume = goalVolume;
    }

    public Integer getGoalPriority() {
        return goalPriority;
    }

    public void setGoalPriority(Integer goalPriority) {
        this.goalPriority = goalPriority;
    }

    public Double getGoalImportance() {
        return goalImportance;
    }

    public void setGoalImportance(Double goalImportance) {
        this.goalImportance = goalImportance;
    }

    public LocalDate getGoalActualStartDate() {
        return goalActualStartDate;
    }

    public void setGoalActualStartDate(LocalDate goalActualStartDate) {
        this.goalActualStartDate = goalActualStartDate;
    }

    public LocalDate getGoalActualEndDate() {
        return goalActualEndDate;
    }

    public void setGoalActualEndDate(LocalDate goalActualEndDate) {
        this.goalActualEndDate = goalActualEndDate;
    }

    public LocalDate getGoalExpirationDate() {
        return goalExpirationDate;
    }

    public void setGoalExpirationDate(LocalDate goalExpirationDate) {
        this.goalExpirationDate = goalExpirationDate;
    }

    public LocalDate getGoalStartDate() {
        return goalStartDate;
    }

    public void setGoalStartDate(LocalDate goalStartDate) {
        this.goalStartDate = goalStartDate;
    }

    public LocalDate getGoalEndDate() {
        return goalEndDate;
    }

    public void setGoalEndDate(LocalDate goalEndDate) {
        this.goalEndDate = goalEndDate;
    }

    public String getGoalFiscalYear() {
        return goalFiscalYear;
    }

    public void setGoalFiscalYear(String goalFiscalYear) {
        this.goalFiscalYear = goalFiscalYear;
    }

    public Long getGoalLevel() {
        return goalLevel;
    }

    public void setGoalLevel(Long goalLevel) {
        this.goalLevel = goalLevel;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getMinimum() {
        return minimum;
    }

    public void setMinimum(Long minimum) {
        this.minimum = minimum;
    }

    public Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        this.maximum = maximum;
    }

    public Long getAverage() {
        return average;
    }

    public void setAverage(Long average) {
        this.average = average;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Long getPredicate() {
        return predicate;
    }

    public void setPredicate(Long predicate) {
        this.predicate = predicate;
    }

    public Long getCortexId() {
        return cortexId;
    }

    public void setCortexId(Long cortexId) {
        this.cortexId = cortexId;
    }

    public Long getNormCostId() {
        return normCostId;
    }

    public void setNormCostId(Long normCostId) {
        this.normCostId = normCostId;
    }

    public String getNormCostName() {
        return normCostName;
    }

    public void setNormCostName(String normCostName) {
        this.normCostName = normCostName;
    }

    public String getNormCostTitle() {
        return normCostTitle;
    }

    public void setNormCostTitle(String normCostTitle) {
        this.normCostTitle = normCostTitle;
    }

    public Long getNormCostCost() {
        return normCostCost;
    }

    public void setNormCostCost(Long normCostCost) {
        this.normCostCost = normCostCost;
    }

    public Long getNormCostRate() {
        return normCostRate;
    }

    public void setNormCostRate(Long normCostRate) {
        this.normCostRate = normCostRate;
    }

    public Integer getNormCostPrioritory() {
        return normCostPrioritory;
    }

    public void setNormCostPrioritory(Integer normCostPrioritory) {
        this.normCostPrioritory = normCostPrioritory;
    }
}
