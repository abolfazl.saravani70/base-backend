/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "REPORT_VIEW")
public class ReportFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "QUERY")
    private String query;

    @Column(name = "REPORT_FILE")
    private String reportFile;

    @Column(name = "REPORT_PATH")
    private String reportPath;

    @Column(name = "IS_VIEW")
    private Boolean isViewFlag;

    @Column(name = "IS_TABLE")
    private Boolean isTable;

    @Column(name = "IS_ONLINE")
    private Boolean isOnline;

    @Column(name = "IS_STORE_PROCEDURE")
    private Boolean isStoreProcedure;

    @Column(name = "IS_DYNAMIC")
    private Boolean isDynamic;

    @Column(name = "FK_VIEW_ID")
    private Long viewId;

    @Column(name = "VIEW_VIEW_NAME")
    private String viewViewName;

    @Column(name = "VIEW_QUERY")
    private String viewQuery;

    @Column(name = "VIEW_KEYWORD")
    private String viewKeyWord;

    @Column(name = "VIEW_EXCEPTION")
    private String viewException;

    @Column(name = "FK_STORE_PROCEDURE_ID")
    private Long storeProcedureId;

    @Column(name = "STORE_PROCEDURE_NAME")
    private String storeProcedureName;

    @Column(name = "STORE_PROCEDURE_TITLE")
    private String storeProcedureTitle;

    @Column(name = "STORE_PROCEDURE_SQL$")
    private String storeProcedureSql;

    @Column(name = "STORE_PROCEDURE_EXCEPTION$")
    private String storeProcedureException;

    @Column(name = "STORE_PROCEDURE_RESOURCE$")
    private String storeProcedureResource;

    @Column(name = "STORE_PROCEDURE_IS_DEPRECATED")
    private Boolean storeProcedureIsDeprecated;

    @Column(name = "FK_APP_ENTITY_ID")
    private Long appEntityId;

    @Column(name = "APP_ENTITY_NAME")
    private String appEntityName;

    @Column(name = "APP_ENTITY_KEYWORD")
    private String appEntityKeyword;

    @Column(name = "APP_ENTITY_TYPE")
    private String appEntityType;

    @Column(name = "APP_ENTITY_CODE")
    private String appEntityCode;

    @Column(name = "APP_ENTITY_IS_DEPRECATED")
    private Boolean appEntityIsDeprecated;

    @Column(name = "APP_ENTITY_REGULAR_EXP")
    private String appEntityRegularExp;

    @Column(name = "FK_REPORT_FILTER_ID")
    private Long reportFilterId;

    @Column(name = "REPORT_FILTER_NAME")
    private String reportFilterName;

    @Column(name = "_REPORT_FILTER_QUERY")
    private String reportFilterQuery;

    @Column(name = "REPORT_FILTER_NUMBER$")
    private String reportFilterNumber;

    @Column(name = "REPORT_FILTER_DFAULT_VALUE")
    private String reportFilterDefualtValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public Boolean getViewFlag() {
        return isViewFlag;
    }

    public void setViewFlag(Boolean viewFlag) {
        isViewFlag = viewFlag;
    }

    public Boolean getTable() {
        return isTable;
    }

    public void setTable(Boolean table) {
        isTable = table;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public Boolean getStoreProcedure() {
        return isStoreProcedure;
    }

    public void setStoreProcedure(Boolean storeProcedure) {
        isStoreProcedure = storeProcedure;
    }

    public Boolean getDynamic() {
        return isDynamic;
    }

    public void setDynamic(Boolean dynamic) {
        isDynamic = dynamic;
    }

    public Long getViewId() {
        return viewId;
    }

    public void setViewId(Long viewId) {
        this.viewId = viewId;
    }

    public String getViewViewName() {
        return viewViewName;
    }

    public void setViewViewName(String viewViewName) {
        this.viewViewName = viewViewName;
    }

    public String getViewQuery() {
        return viewQuery;
    }

    public void setViewQuery(String viewQuery) {
        this.viewQuery = viewQuery;
    }

    public String getViewKeyWord() {
        return viewKeyWord;
    }

    public void setViewKeyWord(String viewKeyWord) {
        this.viewKeyWord = viewKeyWord;
    }

    public String getViewException() {
        return viewException;
    }

    public void setViewException(String viewException) {
        this.viewException = viewException;
    }

    public Long getStoreProcedureId() {
        return storeProcedureId;
    }

    public void setStoreProcedureId(Long storeProcedureId) {
        this.storeProcedureId = storeProcedureId;
    }

    public String getStoreProcedureName() {
        return storeProcedureName;
    }

    public void setStoreProcedureName(String storeProcedureName) {
        this.storeProcedureName = storeProcedureName;
    }

    public String getStoreProcedureTitle() {
        return storeProcedureTitle;
    }

    public void setStoreProcedureTitle(String storeProcedureTitle) {
        this.storeProcedureTitle = storeProcedureTitle;
    }

    public String getStoreProcedureSql() {
        return storeProcedureSql;
    }

    public void setStoreProcedureSql(String storeProcedureSql) {
        this.storeProcedureSql = storeProcedureSql;
    }

    public String getStoreProcedureException() {
        return storeProcedureException;
    }

    public void setStoreProcedureException(String storeProcedureException) {
        this.storeProcedureException = storeProcedureException;
    }

    public String getStoreProcedureResource() {
        return storeProcedureResource;
    }

    public void setStoreProcedureResource(String storeProcedureResource) {
        this.storeProcedureResource = storeProcedureResource;
    }

    public Boolean getStoreProcedureIsDeprecated() {
        return storeProcedureIsDeprecated;
    }

    public void setStoreProcedureIsDeprecated(Boolean storeProcedureIsDeprecated) {
        this.storeProcedureIsDeprecated = storeProcedureIsDeprecated;
    }

    public Long getAppEntityId() {
        return appEntityId;
    }

    public void setAppEntityId(Long appEntityId) {
        this.appEntityId = appEntityId;
    }

    public String getAppEntityName() {
        return appEntityName;
    }

    public void setAppEntityName(String appEntityName) {
        this.appEntityName = appEntityName;
    }

    public String getAppEntityKeyword() {
        return appEntityKeyword;
    }

    public void setAppEntityKeyword(String appEntityKeyword) {
        this.appEntityKeyword = appEntityKeyword;
    }

    public String getAppEntityType() {
        return appEntityType;
    }

    public void setAppEntityType(String appEntityType) {
        this.appEntityType = appEntityType;
    }

    public String getAppEntityCode() {
        return appEntityCode;
    }

    public void setAppEntityCode(String appEntityCode) {
        this.appEntityCode = appEntityCode;
    }

    public Boolean getAppEntityIsDeprecated() {
        return appEntityIsDeprecated;
    }

    public void setAppEntityIsDeprecated(Boolean appEntityIsDeprecated) {
        this.appEntityIsDeprecated = appEntityIsDeprecated;
    }

    public String getAppEntityRegularExp() {
        return appEntityRegularExp;
    }

    public void setAppEntityRegularExp(String appEntityRegularExp) {
        this.appEntityRegularExp = appEntityRegularExp;
    }

    public Long getReportFilterId() {
        return reportFilterId;
    }

    public void setReportFilterId(Long reportFilterId) {
        this.reportFilterId = reportFilterId;
    }

    public String getReportFilterName() {
        return reportFilterName;
    }

    public void setReportFilterName(String reportFilterName) {
        this.reportFilterName = reportFilterName;
    }

    public String getReportFilterQuery() {
        return reportFilterQuery;
    }

    public void setReportFilterQuery(String reportFilterQuery) {
        this.reportFilterQuery = reportFilterQuery;
    }

    public String getReportFilterNumber() {
        return reportFilterNumber;
    }

    public void setReportFilterNumber(String reportFilterNumber) {
        this.reportFilterNumber = reportFilterNumber;
    }

    public String getReportFilterDefualtValue() {
        return reportFilterDefualtValue;
    }

    public void setReportFilterDefualtValue(String reportFilterDefualtValue) {
        this.reportFilterDefualtValue = reportFilterDefualtValue;
    }
}
