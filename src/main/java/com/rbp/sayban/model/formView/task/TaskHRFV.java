package com.rbp.sayban.model.formView.task;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;
import com.rbp.sayban.model.domainmodel.task.Task;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TASK_HR_VIEW")
public class TaskHRFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_TASK_ID")
    private Long taskId;

    @Column(name = "FK_PV_TYPE_ID")
    private Long pvTypeId;

    @Column(name = "PV_TYPE_TITLE")
    private String pvTypeTitle;

    @Column(name = "TASK_NAME")
    private String taskName;

    @Column(name = "TASK_TITLE")
    private String taskTitle;

    @Column(name = "TASK_IS_ROOT")
    private Boolean taskIsRoot;

    @Column(name = "TASK_INITIAL_PERCENTAGE")
    private Double taskInitialPercentage;

    @Column(name = "TASK_PERCENTAGE")
    private Double taskPercentage;

    @Column(name = "TASK_WEIGHT")
    private Long taskWeight;

    @Column(name = "TASK_VOLUME")
    private Long taskVolume;

    @Column(name = "TASK_PRIORITY")
    private Integer taskPriority;

    @Column(name = "TASK_IMPORTANCE")
    private Double taskImportance;

    @Column(name = "TASK_ACTUAL_START_DATE")
    private LocalDate taskActualStartDate;

    @Column(name = "TASK_ACTUAL_END_DATE")
    private LocalDate taskActualEndDate;

    @Column(name = "TASK_EXPIRATION_DATE")
    private LocalDate taskExpirationDate;

    @Column(name = "TASK_START_DATE")
    private LocalDate taskStartDate;

    @Column(name = "TASK_END_DATE")
    private LocalDate taskEndDate;

    @Column(name = "TASK_FISCAL_YEAR")
    private String taskFiscalYear;

    @Column(name = "TASK_LEVEL$")
    private Long taskLevel;

    @Column(name = "FK_HR_ID")
    private Long humanResourceId;

    @Column(name = "HR_TITLE")
    private String hRTitle;

    @Column(name = "HR_IS_ENABLE")
    private Boolean hRIsEnabled;

    @Column(name = "HR_FK_ORG_CHART_ID")
    private Long hROrganizationChartId;

    @Column(name = "HR_FK_ORG_ID")
    private Long hROrganizationId;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public String getPvTypeTitle() {
        return pvTypeTitle;
    }

    public void setPvTypeTitle(String pvTypeTitle) {
        this.pvTypeTitle = pvTypeTitle;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public Boolean getTaskIsRoot() {
        return taskIsRoot;
    }

    public void setTaskIsRoot(Boolean taskIsRoot) {
        this.taskIsRoot = taskIsRoot;
    }

    public Double getTaskInitialPercentage() {
        return taskInitialPercentage;
    }

    public void setTaskInitialPercentage(Double taskInitialPercentage) {
        this.taskInitialPercentage = taskInitialPercentage;
    }

    public Double getTaskPercentage() {
        return taskPercentage;
    }

    public void setTaskPercentage(Double taskPercentage) {
        this.taskPercentage = taskPercentage;
    }

    public Long getTaskWeight() {
        return taskWeight;
    }

    public void setTaskWeight(Long taskWeight) {
        this.taskWeight = taskWeight;
    }

    public Long getTaskVolume() {
        return taskVolume;
    }

    public void setTaskVolume(Long taskVolume) {
        this.taskVolume = taskVolume;
    }

    public Integer getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
    }

    public Double getTaskImportance() {
        return taskImportance;
    }

    public void setTaskImportance(Double taskImportance) {
        this.taskImportance = taskImportance;
    }

    public LocalDate getTaskActualStartDate() {
        return taskActualStartDate;
    }

    public void setTaskActualStartDate(LocalDate taskActualStartDate) {
        this.taskActualStartDate = taskActualStartDate;
    }

    public LocalDate getTaskActualEndDate() {
        return taskActualEndDate;
    }

    public void setTaskActualEndDate(LocalDate taskActualEndDate) {
        this.taskActualEndDate = taskActualEndDate;
    }

    public LocalDate getTaskExpirationDate() {
        return taskExpirationDate;
    }

    public void setTaskExpirationDate(LocalDate taskExpirationDate) {
        this.taskExpirationDate = taskExpirationDate;
    }

    public LocalDate getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(LocalDate taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public LocalDate getTaskEndDate() {
        return taskEndDate;
    }

    public void setTaskEndDate(LocalDate taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    public String getTaskFiscalYear() {
        return taskFiscalYear;
    }

    public void setTaskFiscalYear(String taskFiscalYear) {
        this.taskFiscalYear = taskFiscalYear;
    }

    public Long getTaskLevel() {
        return taskLevel;
    }

    public void setTaskLevel(Long taskLevel) {
        this.taskLevel = taskLevel;
    }

    public Long getHumanResourceId() {
        return humanResourceId;
    }

    public void setHumanResourceId(Long humanResourceId) {
        this.humanResourceId = humanResourceId;
    }

    public String gethRTitle() {
        return hRTitle;
    }

    public void sethRTitle(String hRTitle) {
        this.hRTitle = hRTitle;
    }

    public Boolean gethRIsEnabled() {
        return hRIsEnabled;
    }

    public void sethRIsEnabled(Boolean hRIsEnabled) {
        this.hRIsEnabled = hRIsEnabled;
    }

    public Long gethROrganizationChartId() {
        return hROrganizationChartId;
    }

    public void sethROrganizationChartId(Long hROrganizationChartId) {
        this.hROrganizationChartId = hROrganizationChartId;
    }

    public Long gethROrganizationId() {
        return hROrganizationId;
    }

    public void sethROrganizationId(Long hROrganizationId) {
        this.hROrganizationId = hROrganizationId;
    }
}
