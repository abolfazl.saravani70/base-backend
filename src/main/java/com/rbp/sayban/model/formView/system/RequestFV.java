/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.system;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "REQUEST_VIEW")
public class RequestFV extends BaseEntity {

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "REQUEST_TYPE")
    private String requestType;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "FK_OU_FROM_ID")
    private Long orgUnitIdFrom;

    @Column(name = "FK_OU_TO_ID")
    private Long orgUnitIdTo;

    @Column(name = "PV_REASON_TITLE")
    private String  pvReasonTitle;

    @Column(name = "FK_PV_REASON_ID")
    private Long pvReasonId;

    @Column(name = "PV_REJECT_REASON_TITLE")
    private String pvRejectReasonTitle;

    @Column(name = "FK_PV_REJECT_REASON_ID")
    private Long pvRejectReasonId;

    @Column(name = "PV_STATE_TITLE")
    private String pvStateTitle;

    @Column(name = "FK_PV_STATE_ID")
    private Long pvStateId;

    @Column(name = "PV_REQUESTER_TITLE")
    private String pvRequesterTitle;

    @Column(name = "FK_PV_REQUESTER_ID")
    private Long pvRequesterId;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getOrgUnitIdFrom() {
        return orgUnitIdFrom;
    }

    public void setOrgUnitIdFrom(Long orgUnitIdFrom) {
        this.orgUnitIdFrom = orgUnitIdFrom;
    }

    public Long getOrgUnitIdTo() {
        return orgUnitIdTo;
    }

    public void setOrgUnitIdTo(Long orgUnitIdTo) {
        this.orgUnitIdTo = orgUnitIdTo;
    }

    public String getPvReasonTitle() {
        return pvReasonTitle;
    }

    public void setPvReasonTitle(String pvReasonTitle) {
        this.pvReasonTitle = pvReasonTitle;
    }

    public Long getPvReasonId() {
        return pvReasonId;
    }

    public void setPvReasonId(Long pvReasonId) {
        this.pvReasonId = pvReasonId;
    }

    public String getPvRejectReasonTitle() {
        return pvRejectReasonTitle;
    }

    public void setPvRejectReasonTitle(String pvRejectReasonTitle) {
        this.pvRejectReasonTitle = pvRejectReasonTitle;
    }

    public Long getPvRejectReasonId() {
        return pvRejectReasonId;
    }

    public void setPvRejectReasonId(Long pvRejectReasonId) {
        this.pvRejectReasonId = pvRejectReasonId;
    }

    public String getPvStateTitle() {
        return pvStateTitle;
    }

    public void setPvStateTitle(String pvStateTitle) {
        this.pvStateTitle = pvStateTitle;
    }

    public Long getPvStateId() {
        return pvStateId;
    }

    public void setPvStateId(Long pvStateId) {
        this.pvStateId = pvStateId;
    }

    public String getPvRequesterTitle() {
        return pvRequesterTitle;
    }

    public void setPvRequesterTitle(String pvRequesterTitle) {
        this.pvRequesterTitle = pvRequesterTitle;
    }

    public Long getPvRequesterId() {
        return pvRequesterId;
    }

    public void setPvRequesterId(Long pvRequesterId) {
        this.pvRequesterId = pvRequesterId;
    }
}
