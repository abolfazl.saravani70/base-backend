/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.StereotypeBaseEntity;
import com.rbp.sayban.model.domainmodel.application.AppEnum;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ENUM_VALUE_VIEW")
public class EnumValueFV extends StereotypeBaseEntity {

    @Column(name = "IS_OBSOLETE")
    private Boolean isObsolete;

    @Column(name = "FK_APP_ENUM_ID")
    private Long appEnumId;

    @Column(name = "APP_ENUM_IS_OBSOLETE")
    private Boolean appEnumIsObsolete;

    @Column(name = "APP_ENUM_NAME")
    private String appEnumName;

    @Column(name = "APP_ENUM_TITLE")
    private String appEnumTitle;

    @Column(name = "APP_ENUM_KEY")
    private String appEnumKey;

    @Column(name = "APP_ENUM_CODE")
    private String appEnumCode;

    @Column(name = "APP_ENUM_VALUE")
    private String appEnumValue;

    @Column(name = "APP_ENUM_IS_DEPRECATED")
    private Boolean appEnumIsDeprecated;

    public Boolean getObsolete() {
        return isObsolete;
    }

    public void setObsolete(Boolean obsolete) {
        isObsolete = obsolete;
    }

    public Long getAppEnumId() {
        return appEnumId;
    }

    public void setAppEnumId(Long appEnumId) {
        this.appEnumId = appEnumId;
    }

    public Boolean getAppEnumIsObsolete() {
        return appEnumIsObsolete;
    }

    public void setAppEnumIsObsolete(Boolean appEnumIsObsolete) {
        this.appEnumIsObsolete = appEnumIsObsolete;
    }

    public String getAppEnumName() {
        return appEnumName;
    }

    public void setAppEnumName(String appEnumName) {
        this.appEnumName = appEnumName;
    }

    public String getAppEnumTitle() {
        return appEnumTitle;
    }

    public void setAppEnumTitle(String appEnumTitle) {
        this.appEnumTitle = appEnumTitle;
    }

    public String getAppEnumKey() {
        return appEnumKey;
    }

    public void setAppEnumKey(String appEnumKey) {
        this.appEnumKey = appEnumKey;
    }

    public String getAppEnumCode() {
        return appEnumCode;
    }

    public void setAppEnumCode(String appEnumCode) {
        this.appEnumCode = appEnumCode;
    }

    public String getAppEnumValue() {
        return appEnumValue;
    }

    public void setAppEnumValue(String appEnumValue) {
        this.appEnumValue = appEnumValue;
    }

    public Boolean getAppEnumIsDeprecated() {
        return appEnumIsDeprecated;
    }

    public void setAppEnumIsDeprecated(Boolean appEnumIsDeprecated) {
        this.appEnumIsDeprecated = appEnumIsDeprecated;
    }
}
