/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.subjective;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "SUBJECTIVE_VIEW")
public class SubjectiveFV extends EvalStateBaseEntity {

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TITLE")
    private String parentTitle;

    @Column(name = "PARENT_IS_ROOT")
    private Boolean parentIsRoot;

    @Column(name = "PARENT_INITIAL_PERCENTAGE")
    private Double parentInitialPercentage;

    @Column(name = "PARENT_PERCENTAGE")
    private Double parentPercentage;

    @Column(name = "PARENT_WEIGHT")
    private Long parentWeight;

    @Column(name = "PARENT_VOLUME")
    private Long parentVolume;

    @Column(name = "PARENT_PRIORITY")
    private Integer parentPriority;

    @Column(name = "PARENT_IMPORTANCE")
    private Double parentImportance;

    @Column(name = "PARENT_ACTUAL_START_DATE")
    private LocalDate parentActualStartDate;

    @Column(name = "PARENT_ACTUAL_END_DATE")
    private LocalDate parentActualEndDate;

    @Column(name = "PARENT_EXPIRATION_DATE")
    private LocalDate parentExpirationDate;

    @Column(name = "PARENT_START_DATE")
    private LocalDate parentStartDate;

    @Column(name = "PARENT_END_DATE")
    private LocalDate parentEndDate;

    @Column(name = "PARENT_FISCAL_YEAR")
    private String parentFiscalYear;

    @Column(name = "PARENT_LEVEL$")
    private Long parentLevel;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public Boolean getParentIsRoot() {
        return parentIsRoot;
    }

    public void setParentIsRoot(Boolean parentIsRoot) {
        this.parentIsRoot = parentIsRoot;
    }

    public Double getParentInitialPercentage() {
        return parentInitialPercentage;
    }

    public void setParentInitialPercentage(Double parentInitialPercentage) {
        this.parentInitialPercentage = parentInitialPercentage;
    }

    public Double getParentPercentage() {
        return parentPercentage;
    }

    public void setParentPercentage(Double parentPercentage) {
        this.parentPercentage = parentPercentage;
    }

    public Long getParentWeight() {
        return parentWeight;
    }

    public void setParentWeight(Long parentWeight) {
        this.parentWeight = parentWeight;
    }

    public Long getParentVolume() {
        return parentVolume;
    }

    public void setParentVolume(Long parentVolume) {
        this.parentVolume = parentVolume;
    }

    public Integer getParentPriority() {
        return parentPriority;
    }

    public void setParentPriority(Integer parentPriority) {
        this.parentPriority = parentPriority;
    }

    public Double getParentImportance() {
        return parentImportance;
    }

    public void setParentImportance(Double parentImportance) {
        this.parentImportance = parentImportance;
    }

    public LocalDate getParentActualStartDate() {
        return parentActualStartDate;
    }

    public void setParentActualStartDate(LocalDate parentActualStartDate) {
        this.parentActualStartDate = parentActualStartDate;
    }

    public LocalDate getParentActualEndDate() {
        return parentActualEndDate;
    }

    public void setParentActualEndDate(LocalDate parentActualEndDate) {
        this.parentActualEndDate = parentActualEndDate;
    }

    public LocalDate getParentExpirationDate() {
        return parentExpirationDate;
    }

    public void setParentExpirationDate(LocalDate parentExpirationDate) {
        this.parentExpirationDate = parentExpirationDate;
    }

    public LocalDate getParentStartDate() {
        return parentStartDate;
    }

    public void setParentStartDate(LocalDate parentStartDate) {
        this.parentStartDate = parentStartDate;
    }

    public LocalDate getParentEndDate() {
        return parentEndDate;
    }

    public void setParentEndDate(LocalDate parentEndDate) {
        this.parentEndDate = parentEndDate;
    }

    public String getParentFiscalYear() {
        return parentFiscalYear;
    }

    public void setParentFiscalYear(String parentFiscalYear) {
        this.parentFiscalYear = parentFiscalYear;
    }

    public Long getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Long parentLevel) {
        this.parentLevel = parentLevel;
    }
}
