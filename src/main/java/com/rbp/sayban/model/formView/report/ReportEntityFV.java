/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.AppEntity;
import com.rbp.sayban.model.domainmodel.report.Report;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "REPORT_ENTITY_VIEW")
public class ReportEntityFV extends BaseEntity {

    @Column(name = "FK_REPORT_ID")
    private Long reportId;

    @Column(name = "REPORT_NAME")
    private String reportName;

    @Column(name = "REPORT_NUMBER$")
    private String reportNumber;

    @Column(name = "REPORT_QUERY")
    private String reportQuery;

    @Column(name = "REPORT_REPORT_FILE")
    private String reportReportFile;

    @Column(name = "REPORT_REPORT_PATH")
    private String reportReportPath;

    @Column(name = "REPORT_IS_VIEW")
    private Boolean reportIsViewFlag;

    @Column(name = "REPORT_IS_TABLE")
    private Boolean reportIsTable;

    @Column(name = "REPORT_IS_ONLINE")
    private Boolean reportIsOnline;

    @Column(name = "REPORT_IS_STORE_PROCEDURE")
    private Boolean reportIsStoreProcedure;

    @Column(name = "REPORT_IS_DYNAMIC")
    private Boolean reportIsDynamic;

    @Column(name = "FK_APP_ENTITY_ID")
    private Long appEntityId;

    @Column(name = "APP_ENTITY_ENTITY_NAME")
    private String appEntityEntityName;

    @Column(name = "APP_ENTITY_ENTITY_KEYWORD")
    private String appEntityEntityKeyword;

    @Column(name = "APP_ENTITY_ENTITY_TYPE")
    private String appEntityEntityType;

    @Column(name = "APP_ENTITY_ENTITY_CODE")
    private String appEntityEntityCode;

    @Column(name = "APP_ENTITY_IS_DEPRECATED")
    private Boolean appEntityIsDeprecated;

    @Column(name = "APP_ENTITY_REGULAR_EXP")
    private String appEntityRegularExp;

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getReportQuery() {
        return reportQuery;
    }

    public void setReportQuery(String reportQuery) {
        this.reportQuery = reportQuery;
    }

    public String getReportReportFile() {
        return reportReportFile;
    }

    public void setReportReportFile(String reportReportFile) {
        this.reportReportFile = reportReportFile;
    }

    public String getReportReportPath() {
        return reportReportPath;
    }

    public void setReportReportPath(String reportReportPath) {
        this.reportReportPath = reportReportPath;
    }

    public Boolean getReportIsViewFlag() {
        return reportIsViewFlag;
    }

    public void setReportIsViewFlag(Boolean reportIsViewFlag) {
        this.reportIsViewFlag = reportIsViewFlag;
    }

    public Boolean getReportIsTable() {
        return reportIsTable;
    }

    public void setReportIsTable(Boolean reportIsTable) {
        this.reportIsTable = reportIsTable;
    }

    public Boolean getReportIsOnline() {
        return reportIsOnline;
    }

    public void setReportIsOnline(Boolean reportIsOnline) {
        this.reportIsOnline = reportIsOnline;
    }

    public Boolean getReportIsStoreProcedure() {
        return reportIsStoreProcedure;
    }

    public void setReportIsStoreProcedure(Boolean reportIsStoreProcedure) {
        this.reportIsStoreProcedure = reportIsStoreProcedure;
    }

    public Boolean getReportIsDynamic() {
        return reportIsDynamic;
    }

    public void setReportIsDynamic(Boolean reportIsDynamic) {
        this.reportIsDynamic = reportIsDynamic;
    }

    public Long getAppEntityId() {
        return appEntityId;
    }

    public void setAppEntityId(Long appEntityId) {
        this.appEntityId = appEntityId;
    }

    public String getAppEntityEntityName() {
        return appEntityEntityName;
    }

    public void setAppEntityEntityName(String appEntityEntityName) {
        this.appEntityEntityName = appEntityEntityName;
    }

    public String getAppEntityEntityKeyword() {
        return appEntityEntityKeyword;
    }

    public void setAppEntityEntityKeyword(String appEntityEntityKeyword) {
        this.appEntityEntityKeyword = appEntityEntityKeyword;
    }

    public String getAppEntityEntityType() {
        return appEntityEntityType;
    }

    public void setAppEntityEntityType(String appEntityEntityType) {
        this.appEntityEntityType = appEntityEntityType;
    }

    public String getAppEntityEntityCode() {
        return appEntityEntityCode;
    }

    public void setAppEntityEntityCode(String appEntityEntityCode) {
        this.appEntityEntityCode = appEntityEntityCode;
    }

    public Boolean getAppEntityIsDeprecated() {
        return appEntityIsDeprecated;
    }

    public void setAppEntityIsDeprecated(Boolean appEntityIsDeprecated) {
        this.appEntityIsDeprecated = appEntityIsDeprecated;
    }

    public String getAppEntityRegularExp() {
        return appEntityRegularExp;
    }

    public void setAppEntityRegularExp(String appEntityRegularExp) {
        this.appEntityRegularExp = appEntityRegularExp;
    }
}
