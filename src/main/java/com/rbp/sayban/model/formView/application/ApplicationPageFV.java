/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.model.domainmodel.application.Page;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "APPLICATION_PAGE_VIEW")
public class ApplicationPageFV extends BaseEntity {

    @Column(name = "PAGE_NAME")
    private String pageName;

    @Column(name = "PAGE_TITLE")
    private String pageTitle;

    @Column(name = "PAGE_URL")
    private String pageUrl;

    @Column(name = "PAGE_CODE")
    private String pageCode;

    @Column(name = "PAGE_HELP_URL")
    private String pageHelpUrl;

    @Column(name = "FROM_DATE")
    private LocalDate fromDate;

    @Column(name = "TO_DATE")
    private LocalDate toDate;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @Column(name = "IS_DEPRECATED")
    private Boolean isDeprecated;

    @Column (name = "FK_APPLICATION_ID")
    private Application applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    @Column(name = "FK_PAGE_ID")
    private Long pageId;

    @Column(name = "PAGE_PAGE_NAME")
    private String pagePageName;

    @Column(name = "PAGE_PAGE_TITLE")
    private String pagePageTitle;

    @Column(name = "PAGE_PAGE_URL")
    private String pagePageUrl;

    @Column(name = "PAGE_PAGE_CODE")
    private String pagePageCode;

    @Column(name = "PAGE_PAGE_HELP_URL")
    private String pagePageHelpUrl;

    @Column(name = "PAGE_FROM_DATE")
    private LocalDate pageFromDate;

    @Column(name = "PAGE_TO_DATE")
    private LocalDate pageToDate;

    @Column(name = "PAGE_TEXT")
    private String pageText;

    @Column(name = "PAGE_IS_ACTIVE")
    private Boolean pageIsActive;

    @Column(name = "PAGE_IS_DEPRECATED")
    private Boolean pageIsDeprecated;

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageCode() {
        return pageCode;
    }

    public void setPageCode(String pageCode) {
        this.pageCode = pageCode;
    }

    public String getPageHelpUrl() {
        return pageHelpUrl;
    }

    public void setPageHelpUrl(String pageHelpUrl) {
        this.pageHelpUrl = pageHelpUrl;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getDeprecated() {
        return isDeprecated;
    }

    public void setDeprecated(Boolean deprecated) {
        isDeprecated = deprecated;
    }

    public Application getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Application applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getPagePageName() {
        return pagePageName;
    }

    public void setPagePageName(String pagePageName) {
        this.pagePageName = pagePageName;
    }

    public String getPagePageTitle() {
        return pagePageTitle;
    }

    public void setPagePageTitle(String pagePageTitle) {
        this.pagePageTitle = pagePageTitle;
    }

    public String getPagePageUrl() {
        return pagePageUrl;
    }

    public void setPagePageUrl(String pagePageUrl) {
        this.pagePageUrl = pagePageUrl;
    }

    public String getPagePageCode() {
        return pagePageCode;
    }

    public void setPagePageCode(String pagePageCode) {
        this.pagePageCode = pagePageCode;
    }

    public String getPagePageHelpUrl() {
        return pagePageHelpUrl;
    }

    public void setPagePageHelpUrl(String pagePageHelpUrl) {
        this.pagePageHelpUrl = pagePageHelpUrl;
    }

    public LocalDate getPageFromDate() {
        return pageFromDate;
    }

    public void setPageFromDate(LocalDate pageFromDate) {
        this.pageFromDate = pageFromDate;
    }

    public LocalDate getPageToDate() {
        return pageToDate;
    }

    public void setPageToDate(LocalDate pageToDate) {
        this.pageToDate = pageToDate;
    }

    public String getPageText() {
        return pageText;
    }

    public void setPageText(String pageText) {
        this.pageText = pageText;
    }

    public Boolean getPageIsActive() {
        return pageIsActive;
    }

    public void setPageIsActive(Boolean pageIsActive) {
        this.pageIsActive = pageIsActive;
    }

    public Boolean getPageIsDeprecated() {
        return pageIsDeprecated;
    }

    public void setPageIsDeprecated(Boolean pageIsDeprecated) {
        this.pageIsDeprecated = pageIsDeprecated;
    }
}
