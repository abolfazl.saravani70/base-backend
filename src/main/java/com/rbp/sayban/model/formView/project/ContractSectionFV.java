/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.project;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.project.Contract;
import com.rbp.sayban.model.domainmodel.system.Document;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "CONTRACT_SECTION_VIEW")
public class ContractSectionFV extends BaseEntity {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "NUMBER$")
    private String number;

    @Column(name = "FK_CONTRACT_ID")
    private Long contractId;

    @Column(name = "CONTRACT_TITLE")
    private String contractTitle;

    @Column(name = "CONTRACT_NUMBER$")
    private String contractNumber;

    @Column(name = "CONTRACT_TEXT")
    private String contractText;

    @Column(name = "CONTRACT_CONTRACTOR")
    private String contractContractor;

    @Column(name = "CONTRACT_CONTRACTEE")
    private String contractContractee;

    @Column(name = "CONTRACT_NUMBER_OF_SECTION")
    private Long contractNumberOfSection;

    @Column(name = "CONTRACT_HAS_ATTACHMENT")
    private Boolean contractIsAttachment;

    @Column(name = "CONTRACT_IS_COMPLEMENT")
    private Boolean contractIsComplement;

    @Column(name = "CONTRACT_DOCUMENT")
    private Document contractDocument;

    @Column(name = "CONTRACT_NUMBER_COPY")
    private Long contractNumberCopy;

    @Column(name = "CONTRACT_VERSION_OF_CONTRACT")
    private Long contractVersionOfContract;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getContractTitle() {
        return contractTitle;
    }

    public void setContractTitle(String contractTitle) {
        this.contractTitle = contractTitle;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContractText() {
        return contractText;
    }

    public void setContractText(String contractText) {
        this.contractText = contractText;
    }

    public String getContractContractor() {
        return contractContractor;
    }

    public void setContractContractor(String contractContractor) {
        this.contractContractor = contractContractor;
    }

    public String getContractContractee() {
        return contractContractee;
    }

    public void setContractContractee(String contractContractee) {
        this.contractContractee = contractContractee;
    }

    public Long getContractNumberOfSection() {
        return contractNumberOfSection;
    }

    public void setContractNumberOfSection(Long contractNumberOfSection) {
        this.contractNumberOfSection = contractNumberOfSection;
    }

    public Boolean getContractIsAttachment() {
        return contractIsAttachment;
    }

    public void setContractIsAttachment(Boolean contractIsAttachment) {
        this.contractIsAttachment = contractIsAttachment;
    }

    public Boolean getContractIsComplement() {
        return contractIsComplement;
    }

    public void setContractIsComplement(Boolean contractIsComplement) {
        this.contractIsComplement = contractIsComplement;
    }

    public Document getContractDocument() {
        return contractDocument;
    }

    public void setContractDocument(Document contractDocument) {
        this.contractDocument = contractDocument;
    }

    public Long getContractNumberCopy() {
        return contractNumberCopy;
    }

    public void setContractNumberCopy(Long contractNumberCopy) {
        this.contractNumberCopy = contractNumberCopy;
    }

    public Long getContractVersionOfContract() {
        return contractVersionOfContract;
    }

    public void setContractVersionOfContract(Long contractVersionOfContract) {
        this.contractVersionOfContract = contractVersionOfContract;
    }
}
