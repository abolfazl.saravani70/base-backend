/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "APPLICATION_KPI_VIEW")
public class ApplicationKpiFV extends BaseEntity {

    @Column(name = "APP_KPI_NAME")
    private String appKpiName;

    @Column(name = "APP_KPI_KEY")
    private Integer appKpiKey;

    @Column(name = "APP_KPI_CODE")
    private String appKpiCode;

    @Column(name = "APP_KPI_VALUE")
    private String appKpiValue;

    @Column(name = "APP_KPI_BEST_VALUE")
    private String appKpiBestValue;

    @Column(name = "APP_KPI_PRIORITY")
    private Integer appKpiPriority;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    public String getAppKpiName() {
        return appKpiName;
    }

    public void setAppKpiName(String appKpiName) {
        this.appKpiName = appKpiName;
    }

    public Integer getAppKpiKey() {
        return appKpiKey;
    }

    public void setAppKpiKey(Integer appKpiKey) {
        this.appKpiKey = appKpiKey;
    }

    public String getAppKpiCode() {
        return appKpiCode;
    }

    public void setAppKpiCode(String appKpiCode) {
        this.appKpiCode = appKpiCode;
    }

    public String getAppKpiValue() {
        return appKpiValue;
    }

    public void setAppKpiValue(String appKpiValue) {
        this.appKpiValue = appKpiValue;
    }

    public String getAppKpiBestValue() {
        return appKpiBestValue;
    }

    public void setAppKpiBestValue(String appKpiBestValue) {
        this.appKpiBestValue = appKpiBestValue;
    }

    public Integer getAppKpiPriority() {
        return appKpiPriority;
    }

    public void setAppKpiPriority(Integer appKpiPriority) {
        this.appKpiPriority = appKpiPriority;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }
}
