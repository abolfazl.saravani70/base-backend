/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.Application;
import com.rbp.sayban.model.domainmodel.report.Report;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "REPORT_APPLICATION_VIEW")
public class ReportApplicationFV extends BaseEntity {

    @Column(name = "FK_REPORT_ID")
    private Long reportId;

    @Column(name = "REPORT_NAME")
    private String reportName;

    @Column(name = "REPORT_NUMBER$")
    private String reportNumber;

    @Column(name = "REPORT_QUERY")
    private String reportQuery;

    @Column(name = "REPORT_REPORT_FILE")
    private String reportReportFile;

    @Column(name = "REPORT_REPORT_PATH")
    private String reportReportPath;

    @Column(name = "REPORT_IS_VIEW")
    private Boolean reportIsViewFlag;

    @Column(name = "REPORT_IS_TABLE")
    private Boolean reportIsTable;

    @Column(name = "REPORT_IS_ONLINE")
    private Boolean reportIsOnline;

    @Column(name = "REPORT_IS_STORE_PROCEDURE")
    private Boolean reportIsStoreProcedure;

    @Column(name = "REPORT_IS_DYNAMIC")
    private Boolean reportIsDynamic;

    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getReportQuery() {
        return reportQuery;
    }

    public void setReportQuery(String reportQuery) {
        this.reportQuery = reportQuery;
    }

    public String getReportReportFile() {
        return reportReportFile;
    }

    public void setReportReportFile(String reportReportFile) {
        this.reportReportFile = reportReportFile;
    }

    public String getReportReportPath() {
        return reportReportPath;
    }

    public void setReportReportPath(String reportReportPath) {
        this.reportReportPath = reportReportPath;
    }

    public Boolean getReportIsViewFlag() {
        return reportIsViewFlag;
    }

    public void setReportIsViewFlag(Boolean reportIsViewFlag) {
        this.reportIsViewFlag = reportIsViewFlag;
    }

    public Boolean getReportIsTable() {
        return reportIsTable;
    }

    public void setReportIsTable(Boolean reportIsTable) {
        this.reportIsTable = reportIsTable;
    }

    public Boolean getReportIsOnline() {
        return reportIsOnline;
    }

    public void setReportIsOnline(Boolean reportIsOnline) {
        this.reportIsOnline = reportIsOnline;
    }

    public Boolean getReportIsStoreProcedure() {
        return reportIsStoreProcedure;
    }

    public void setReportIsStoreProcedure(Boolean reportIsStoreProcedure) {
        this.reportIsStoreProcedure = reportIsStoreProcedure;
    }

    public Boolean getReportIsDynamic() {
        return reportIsDynamic;
    }

    public void setReportIsDynamic(Boolean reportIsDynamic) {
        this.reportIsDynamic = reportIsDynamic;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }
}
