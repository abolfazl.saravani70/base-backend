/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.planning;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.budgeting.Sharing;
import com.rbp.sayban.model.domainmodel.planning.Plan;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "PLAN_SHARING_VIEW")
public class PlanSharingFV extends BaseEntity {

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "PERCENT")
    private Double percent;

    @Column(name = "PRIDIِCATE")
    private Long pridicate;

    @Column(name = "ACCEPTED_AMOUNT")
    private Long acceptedAmount;

    @Column(name = "FK_PLAN_ID")
    private Long planId;

    @Column(name = "PLAN_FK_PV_TYPE_ID")
    private Long planPvTypeId;

    @Column(name = "PLAN_PV_TYPE_TITLE")
    private String planPvTypeTitle;

    @Column(name = "PLAN_NAME")
    private String planName;

    @Column(name = "PLAN_TITLE")
    private String planTitle;

    @Column(name = "PLAN_IS_ROOT")
    private Boolean planIsRoot;

    @Column(name = "PLAN_INITIAL_PERCENTAGE")
    private Double planInitialPercentage;

    @Column(name = "PLAN_PERCENTAGE")
    private Double planPercentage;

    @Column(name = "PLAN_WEIGHT")
    private Long planWeight;

    @Column(name = "PLAN_VOLUME")
    private Long planVolume;

    @Column(name = "PLAN_PRIORITY")
    private Integer planPriority;

    @Column(name = "PLAN_IMPORTANCE")
    private Double planImportance;

    @Column(name = "PLAN_ACTUAL_START_DATE")
    private LocalDate planActualStartDate;

    @Column(name = "PLAN_ACTUAL_END_DATE")
    private LocalDate planActualEndDate;

    @Column(name = "PLAN_EXPIRATION_DATE")
    private LocalDate planExpirationDate;

    @Column(name = "PLAN_START_DATE")
    private LocalDate planStartDate;

    @Column(name = "PLAN_END_DATE")
    private LocalDate planEndDate;

    @Column(name = "PLAN_FISCAL_YEAR")
    private String planFiscalYear;

    @Column(name = "PLAN_LEVEL$")
    private Long planLevel;

    @Column(name = "FK_SHARING_ID")
    private Long sharingId;

    @Column(name = "SHARING_NUMBER$")
    private String sharingNumber;

    @Column(name = "SHARING_AMOUNT")
    private Long sharingAmount;

    @Column(name = "SHARING_PERCENT")
    private Double sharingPercent;

    @Column(name = "SHARING_ACT_SHARING_DATE")
    private LocalDate sharingDate;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Long getPridicate() {
        return pridicate;
    }

    public void setPridicate(Long pridicate) {
        this.pridicate = pridicate;
    }

    public Long getAcceptedAmount() {
        return acceptedAmount;
    }

    public void setAcceptedAmount(Long acceptedAmount) {
        this.acceptedAmount = acceptedAmount;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanPvTypeId() {
        return planPvTypeId;
    }

    public void setPlanPvTypeId(Long planPvTypeId) {
        this.planPvTypeId = planPvTypeId;
    }

    public String getPlanPvTypeTitle() {
        return planPvTypeTitle;
    }

    public void setPlanPvTypeTitle(String planPvTypeTitle) {
        this.planPvTypeTitle = planPvTypeTitle;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanTitle() {
        return planTitle;
    }

    public void setPlanTitle(String planTitle) {
        this.planTitle = planTitle;
    }

    public Boolean getPlanIsRoot() {
        return planIsRoot;
    }

    public void setPlanIsRoot(Boolean planIsRoot) {
        this.planIsRoot = planIsRoot;
    }

    public Double getPlanInitialPercentage() {
        return planInitialPercentage;
    }

    public void setPlanInitialPercentage(Double planInitialPercentage) {
        this.planInitialPercentage = planInitialPercentage;
    }

    public Double getPlanPercentage() {
        return planPercentage;
    }

    public void setPlanPercentage(Double planPercentage) {
        this.planPercentage = planPercentage;
    }

    public Long getPlanWeight() {
        return planWeight;
    }

    public void setPlanWeight(Long planWeight) {
        this.planWeight = planWeight;
    }

    public Long getPlanVolume() {
        return planVolume;
    }

    public void setPlanVolume(Long planVolume) {
        this.planVolume = planVolume;
    }

    public Integer getPlanPriority() {
        return planPriority;
    }

    public void setPlanPriority(Integer planPriority) {
        this.planPriority = planPriority;
    }

    public Double getPlanImportance() {
        return planImportance;
    }

    public void setPlanImportance(Double planImportance) {
        this.planImportance = planImportance;
    }

    public LocalDate getPlanActualStartDate() {
        return planActualStartDate;
    }

    public void setPlanActualStartDate(LocalDate planActualStartDate) {
        this.planActualStartDate = planActualStartDate;
    }

    public LocalDate getPlanActualEndDate() {
        return planActualEndDate;
    }

    public void setPlanActualEndDate(LocalDate planActualEndDate) {
        this.planActualEndDate = planActualEndDate;
    }

    public LocalDate getPlanExpirationDate() {
        return planExpirationDate;
    }

    public void setPlanExpirationDate(LocalDate planExpirationDate) {
        this.planExpirationDate = planExpirationDate;
    }

    public LocalDate getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(LocalDate planStartDate) {
        this.planStartDate = planStartDate;
    }

    public LocalDate getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(LocalDate planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanFiscalYear() {
        return planFiscalYear;
    }

    public void setPlanFiscalYear(String planFiscalYear) {
        this.planFiscalYear = planFiscalYear;
    }

    public Long getPlanLevel() {
        return planLevel;
    }

    public void setPlanLevel(Long planLevel) {
        this.planLevel = planLevel;
    }

    public Long getSharingId() {
        return sharingId;
    }

    public void setSharingId(Long sharingId) {
        this.sharingId = sharingId;
    }

    public String getSharingNumber() {
        return sharingNumber;
    }

    public void setSharingNumber(String sharingNumber) {
        this.sharingNumber = sharingNumber;
    }

    public Long getSharingAmount() {
        return sharingAmount;
    }

    public void setSharingAmount(Long sharingAmount) {
        this.sharingAmount = sharingAmount;
    }

    public Double getSharingPercent() {
        return sharingPercent;
    }

    public void setSharingPercent(Double sharingPercent) {
        this.sharingPercent = sharingPercent;
    }

    public LocalDate getSharingDate() {
        return sharingDate;
    }

    public void setSharingDate(LocalDate sharingDate) {
        this.sharingDate = sharingDate;
    }
}
