/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChartTemplate;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnitTemplate;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ORG_UNIT_TEMPLATE_VIEW")
public class OrganizationUnitTemplateFV extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "LEVEL$")
    private Integer level;

    @Column(name = "IS_POSITION")
    private Boolean isPosition;

    @Column(name = "FK_PV_DEGREE_ID")
    private Long pvDegreeId;

    @Column(name = "PV_DEGREE_TITLE")
    private String pvDegreeTitle;

    @Column(name = "FK_ORG_CHART_TEMPLATE_ID")
    private Long orgChartTemplateId;

    @Column(name = "ORG_CHART_TEMPLATE_NAME")
    private String orgCartTemplateName;

    @Column(name = "ORG_CHART_TEMPLATE_LEVEL$")
    private Integer orgCartTemplateLevel;

    @Column(name = "ORG_CHART_TEMPLATE_TYPE")
    private String orgCartTemplateType;

    @Column(name = "ORG_CHART_TEMPLATE_CODE")
    private String orgCartTemplateCode;

    @Column(name = "FK_PARENT_ID")
    private Long parentId;

    @Column(name = "PARENT_NAME")
    private String parentName;

    @Column(name = "PARENT_TYPE")
    private String parentType;

    @Column(name = "PARENT_LEVEL$")
    private Integer parentLevel;

    @Column(name = "PARENT_IS_POSITION")
    private Boolean parentIsPosition;

    @Column(name = "PARENT_FK_PV_DEGREE_ID")
    private Long parentPvDegreeId;

    @Column(name = "PARENT_PV_DEGREE_TITLE")
    private String parentPvDegreeTitle;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getPosition() {
        return isPosition;
    }

    public void setPosition(Boolean position) {
        isPosition = position;
    }

    public Long getPvDegreeId() {
        return pvDegreeId;
    }

    public void setPvDegreeId(Long pvDegreeId) {
        this.pvDegreeId = pvDegreeId;
    }

    public String getPvDegreeTitle() {
        return pvDegreeTitle;
    }

    public void setPvDegreeTitle(String pvDegreeTitle) {
        this.pvDegreeTitle = pvDegreeTitle;
    }

    public Long getOrgChartTemplateId() {
        return orgChartTemplateId;
    }

    public void setOrgChartTemplateId(Long orgChartTemplateId) {
        this.orgChartTemplateId = orgChartTemplateId;
    }

    public String getOrgCartTemplateName() {
        return orgCartTemplateName;
    }

    public void setOrgCartTemplateName(String orgCartTemplateName) {
        this.orgCartTemplateName = orgCartTemplateName;
    }

    public Integer getOrgCartTemplateLevel() {
        return orgCartTemplateLevel;
    }

    public void setOrgCartTemplateLevel(Integer orgCartTemplateLevel) {
        this.orgCartTemplateLevel = orgCartTemplateLevel;
    }

    public String getOrgCartTemplateType() {
        return orgCartTemplateType;
    }

    public void setOrgCartTemplateType(String orgCartTemplateType) {
        this.orgCartTemplateType = orgCartTemplateType;
    }

    public String getOrgCartTemplateCode() {
        return orgCartTemplateCode;
    }

    public void setOrgCartTemplateCode(String orgCartTemplateCode) {
        this.orgCartTemplateCode = orgCartTemplateCode;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentType() {
        return parentType;
    }

    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    public Integer getParentLevel() {
        return parentLevel;
    }

    public void setParentLevel(Integer parentLevel) {
        this.parentLevel = parentLevel;
    }

    public Boolean getParentIsPosition() {
        return parentIsPosition;
    }

    public void setParentIsPosition(Boolean parentIsPosition) {
        this.parentIsPosition = parentIsPosition;
    }

    public Long getParentPvDegreeId() {
        return parentPvDegreeId;
    }

    public void setParentPvDegreeId(Long parentPvDegreeId) {
        this.parentPvDegreeId = parentPvDegreeId;
    }

    public String getParentPvDegreeTitle() {
        return parentPvDegreeTitle;
    }

    public void setParentPvDegreeTitle(String parentPvDegreeTitle) {
        this.parentPvDegreeTitle = parentPvDegreeTitle;
    }
}
