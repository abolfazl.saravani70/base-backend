/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.strategy;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.KpiJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.strategies.Strategy;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "STRATEGY_KPI_VIEW")
public class StrategyKpiFV extends EvalStateBaseEntity {

    @Column(name = "LOW")
    private Long low;

    @Column(name = "HIGH")
    private Long high;

    @Column(name = "NORMAL")
    private Long normal;

    @Column(name = "ACTUAL_COST")
    private Double actualCost;

    @Column(name = "ACTUAL_VALUE")
    private Long actualValue;

    @Column(name = "AUDIT_COST")
    private Double auditCost;

    @Column(name = "AUDIT_VALUE")
    private Long auditValue;

    @Column(name = "FK_STRATEGY_ID")
    private Long strategyId;

    @Column(name = "STRATEGY_FK_PV_TYPE_ID")
    private Long strategyPvTypeId;

    @Column(name = "STRATEGY_PV_TYPE_TITLE")
    private String strategyPvTypeTitle;

    @Column(name = "STRATEGY_NAME")
    private String strategyName;

    @Column(name = "STRATEGY_TITLE")
    private String strategyTitle;

    @Column(name = "STRATEGY_IS_ROOT")
    private Boolean strategyIsRoot;

    @Column(name = "STRATEGY_INITIAL_PERCENTAGE")
    private Double strategyInitialPercentage;

    @Column(name = "STRATEGY_PERCENTAGE")
    private Double strategyPercentage;

    @Column(name = "STRATEGY_WEIGHT")
    private Long strategyWeight;

    @Column(name = "STRATEGY_VOLUME")
    private Long strategyVolume;

    @Column(name = "STRATEGY_PRIORITY")
    private Integer strategyPriority;

    @Column(name = "STRATEGY_IMPORTANCE")
    private Double strategyImportance;

    @Column(name = "STRATEGY_ACTUAL_START_DATE")
    private LocalDate strategyActualStartDate;

    @Column(name = "STRATEGY_ACTUAL_END_DATE")
    private LocalDate strategyActualEndDate;

    @Column(name = "STRATEGY_EXPIRATION_DATE")
    private LocalDate strategyExpirationDate;

    @Column(name = "STRATEGY_START_DATE")
    private LocalDate strategyStartDate;

    @Column(name = "STRATEGY_END_DATE")
    private LocalDate strategyEndDate;

    @Column(name = "STRATEGY_FISCAL_YEAR")
    private String strategyFiscalYear;

    @Column(name = "STRATEGY_LEVEL$")
    private Long strategyLevel;

    @Column(name = "FK_KPI_ID")
    private Long kpiId;

    @Column(name = "KPI_KEY")
    private Integer kpiKey;

    @Column(name = "KPI_CODE")
    private String kpiCode;

    @Column(name = "KPI_VALUE")
    private Long kpiValue;

    @Column(name = "KPI_BEST_VALUE")
    private Long kpiBestValue;

    @Column(name = "KPI_KPI_PRIORITY")
    private Integer kpiKpiPriority;

    @Column(name = "KPI_COST")
    private Integer kpiCost;

    @Column(name = "KPI_MINIMUM")
    private Long kpiMinimum;

    @Column(name = "KPI_MAXIMUM")
    private Long kpiMaximum;

    @Column(name = "KPI_NORMAL")
    private Long kpiNormal;

    @Column(name = "KPI_FK_PV_KPI_TYPE_ID")
    private Long kpiPvKpiTypeId;

    @Column(name = "KPI_PV_KPI_TYPE_TITLE")
    private String kpiPvKpiTypeTitle;

    @Column(name = "KPI_FK_PV_MESURE_UNIT_ID")
    private Long kpiPvMesureUnitId;

    @Column(name = "KPI_PV_MESURE_UNIT_TITLE")
    private String kpiPvMesureUnitTitle;

    @Column(name = "KPI_NAME")
    private String kpiName;

    @Column(name = "KPI_TITLE")
    private String kpiTitle;

    @Column(name = "KPI_IS_ROOT")
    private Boolean kpiIsRoot;

    @Column(name = "KPI_INITIAL_PERCENTAGE")
    private Double kpiInitialPercentage;

    @Column(name = "KPI_PERCENTAGE")
    private Double kpiPercentage;

    @Column(name = "KPI_WEIGHT")
    private Long kpiWeight;

    @Column(name = "KPI_VOLUME")
    private Long kpiVolume;

    @Column(name = "KPI_PRIORITY")
    private Integer kpiPriority;

    @Column(name = "KPI_IMPORTANCE")
    private Double kpiImportance;

    @Column(name = "KPI_ACTUAL_START_DATE")
    private LocalDate kpiActualStartDate;

    @Column(name = "KPI_ACTUAL_END_DATE")
    private LocalDate kpiActualEndDate;

    @Column(name = "KPI_EXPIRATION_DATE")
    private LocalDate kpiExpirationDate;

    @Column(name = "KPI_START_DATE")
    private LocalDate kpiStartDate;

    @Column(name = "KPI_END_DATE")
    private LocalDate kpiEndDate;

    @Column(name = "KPI_FISCAL_YEAR")
    private String kpiFiscalYear;

    @Column(name = "KPI_LEVEL$")
    private Long kpiLevel;

    public Long getLow() {
        return low;
    }

    public void setLow(Long low) {
        this.low = low;
    }

    public Long getHigh() {
        return high;
    }

    public void setHigh(Long high) {
        this.high = high;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public Long getActualValue() {
        return actualValue;
    }

    public void setActualValue(Long actualValue) {
        this.actualValue = actualValue;
    }

    public Double getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(Double auditCost) {
        this.auditCost = auditCost;
    }

    public Long getAuditValue() {
        return auditValue;
    }

    public void setAuditValue(Long auditValue) {
        this.auditValue = auditValue;
    }

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public Long getStrategyPvTypeId() {
        return strategyPvTypeId;
    }

    public void setStrategyPvTypeId(Long strategyPvTypeId) {
        this.strategyPvTypeId = strategyPvTypeId;
    }

    public String getStrategyPvTypeTitle() {
        return strategyPvTypeTitle;
    }

    public void setStrategyPvTypeTitle(String strategyPvTypeTitle) {
        this.strategyPvTypeTitle = strategyPvTypeTitle;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public String getStrategyTitle() {
        return strategyTitle;
    }

    public void setStrategyTitle(String strategyTitle) {
        this.strategyTitle = strategyTitle;
    }

    public Boolean getStrategyIsRoot() {
        return strategyIsRoot;
    }

    public void setStrategyIsRoot(Boolean strategyIsRoot) {
        this.strategyIsRoot = strategyIsRoot;
    }

    public Double getStrategyInitialPercentage() {
        return strategyInitialPercentage;
    }

    public void setStrategyInitialPercentage(Double strategyInitialPercentage) {
        this.strategyInitialPercentage = strategyInitialPercentage;
    }

    public Double getStrategyPercentage() {
        return strategyPercentage;
    }

    public void setStrategyPercentage(Double strategyPercentage) {
        this.strategyPercentage = strategyPercentage;
    }

    public Long getStrategyWeight() {
        return strategyWeight;
    }

    public void setStrategyWeight(Long strategyWeight) {
        this.strategyWeight = strategyWeight;
    }

    public Long getStrategyVolume() {
        return strategyVolume;
    }

    public void setStrategyVolume(Long strategyVolume) {
        this.strategyVolume = strategyVolume;
    }

    public Integer getStrategyPriority() {
        return strategyPriority;
    }

    public void setStrategyPriority(Integer strategyPriority) {
        this.strategyPriority = strategyPriority;
    }

    public Double getStrategyImportance() {
        return strategyImportance;
    }

    public void setStrategyImportance(Double strategyImportance) {
        this.strategyImportance = strategyImportance;
    }

    public LocalDate getStrategyActualStartDate() {
        return strategyActualStartDate;
    }

    public void setStrategyActualStartDate(LocalDate strategyActualStartDate) {
        this.strategyActualStartDate = strategyActualStartDate;
    }

    public LocalDate getStrategyActualEndDate() {
        return strategyActualEndDate;
    }

    public void setStrategyActualEndDate(LocalDate strategyActualEndDate) {
        this.strategyActualEndDate = strategyActualEndDate;
    }

    public LocalDate getStrategyExpirationDate() {
        return strategyExpirationDate;
    }

    public void setStrategyExpirationDate(LocalDate strategyExpirationDate) {
        this.strategyExpirationDate = strategyExpirationDate;
    }

    public LocalDate getStrategyStartDate() {
        return strategyStartDate;
    }

    public void setStrategyStartDate(LocalDate strategyStartDate) {
        this.strategyStartDate = strategyStartDate;
    }

    public LocalDate getStrategyEndDate() {
        return strategyEndDate;
    }

    public void setStrategyEndDate(LocalDate strategyEndDate) {
        this.strategyEndDate = strategyEndDate;
    }

    public String getStrategyFiscalYear() {
        return strategyFiscalYear;
    }

    public void setStrategyFiscalYear(String strategyFiscalYear) {
        this.strategyFiscalYear = strategyFiscalYear;
    }

    public Long getStrategyLevel() {
        return strategyLevel;
    }

    public void setStrategyLevel(Long strategyLevel) {
        this.strategyLevel = strategyLevel;
    }

    public Long getKpiId() {
        return kpiId;
    }

    public void setKpiId(Long kpiId) {
        this.kpiId = kpiId;
    }

    public Integer getKpiKey() {
        return kpiKey;
    }

    public void setKpiKey(Integer kpiKey) {
        this.kpiKey = kpiKey;
    }

    public String getKpiCode() {
        return kpiCode;
    }

    public void setKpiCode(String kpiCode) {
        this.kpiCode = kpiCode;
    }

    public Long getKpiValue() {
        return kpiValue;
    }

    public void setKpiValue(Long kpiValue) {
        this.kpiValue = kpiValue;
    }

    public Long getKpiBestValue() {
        return kpiBestValue;
    }

    public void setKpiBestValue(Long kpiBestValue) {
        this.kpiBestValue = kpiBestValue;
    }

    public Integer getKpiKpiPriority() {
        return kpiKpiPriority;
    }

    public void setKpiKpiPriority(Integer kpiKpiPriority) {
        this.kpiKpiPriority = kpiKpiPriority;
    }

    public Integer getKpiCost() {
        return kpiCost;
    }

    public void setKpiCost(Integer kpiCost) {
        this.kpiCost = kpiCost;
    }

    public Long getKpiMinimum() {
        return kpiMinimum;
    }

    public void setKpiMinimum(Long kpiMinimum) {
        this.kpiMinimum = kpiMinimum;
    }

    public Long getKpiMaximum() {
        return kpiMaximum;
    }

    public void setKpiMaximum(Long kpiMaximum) {
        this.kpiMaximum = kpiMaximum;
    }

    public Long getKpiNormal() {
        return kpiNormal;
    }

    public void setKpiNormal(Long kpiNormal) {
        this.kpiNormal = kpiNormal;
    }

    public Long getKpiPvKpiTypeId() {
        return kpiPvKpiTypeId;
    }

    public void setKpiPvKpiTypeId(Long kpiPvKpiTypeId) {
        this.kpiPvKpiTypeId = kpiPvKpiTypeId;
    }

    public String getKpiPvKpiTypeTitle() {
        return kpiPvKpiTypeTitle;
    }

    public void setKpiPvKpiTypeTitle(String kpiPvKpiTypeTitle) {
        this.kpiPvKpiTypeTitle = kpiPvKpiTypeTitle;
    }

    public Long getKpiPvMesureUnitId() {
        return kpiPvMesureUnitId;
    }

    public void setKpiPvMesureUnitId(Long kpiPvMesureUnitId) {
        this.kpiPvMesureUnitId = kpiPvMesureUnitId;
    }

    public String getKpiPvMesureUnitTitle() {
        return kpiPvMesureUnitTitle;
    }

    public void setKpiPvMesureUnitTitle(String kpiPvMesureUnitTitle) {
        this.kpiPvMesureUnitTitle = kpiPvMesureUnitTitle;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getKpiTitle() {
        return kpiTitle;
    }

    public void setKpiTitle(String kpiTitle) {
        this.kpiTitle = kpiTitle;
    }

    public Boolean getKpiIsRoot() {
        return kpiIsRoot;
    }

    public void setKpiIsRoot(Boolean kpiIsRoot) {
        this.kpiIsRoot = kpiIsRoot;
    }

    public Double getKpiInitialPercentage() {
        return kpiInitialPercentage;
    }

    public void setKpiInitialPercentage(Double kpiInitialPercentage) {
        this.kpiInitialPercentage = kpiInitialPercentage;
    }

    public Double getKpiPercentage() {
        return kpiPercentage;
    }

    public void setKpiPercentage(Double kpiPercentage) {
        this.kpiPercentage = kpiPercentage;
    }

    public Long getKpiWeight() {
        return kpiWeight;
    }

    public void setKpiWeight(Long kpiWeight) {
        this.kpiWeight = kpiWeight;
    }

    public Long getKpiVolume() {
        return kpiVolume;
    }

    public void setKpiVolume(Long kpiVolume) {
        this.kpiVolume = kpiVolume;
    }

    public Integer getKpiPriority() {
        return kpiPriority;
    }

    public void setKpiPriority(Integer kpiPriority) {
        this.kpiPriority = kpiPriority;
    }

    public Double getKpiImportance() {
        return kpiImportance;
    }

    public void setKpiImportance(Double kpiImportance) {
        this.kpiImportance = kpiImportance;
    }

    public LocalDate getKpiActualStartDate() {
        return kpiActualStartDate;
    }

    public void setKpiActualStartDate(LocalDate kpiActualStartDate) {
        this.kpiActualStartDate = kpiActualStartDate;
    }

    public LocalDate getKpiActualEndDate() {
        return kpiActualEndDate;
    }

    public void setKpiActualEndDate(LocalDate kpiActualEndDate) {
        this.kpiActualEndDate = kpiActualEndDate;
    }

    public LocalDate getKpiExpirationDate() {
        return kpiExpirationDate;
    }

    public void setKpiExpirationDate(LocalDate kpiExpirationDate) {
        this.kpiExpirationDate = kpiExpirationDate;
    }

    public LocalDate getKpiStartDate() {
        return kpiStartDate;
    }

    public void setKpiStartDate(LocalDate kpiStartDate) {
        this.kpiStartDate = kpiStartDate;
    }

    public LocalDate getKpiEndDate() {
        return kpiEndDate;
    }

    public void setKpiEndDate(LocalDate kpiEndDate) {
        this.kpiEndDate = kpiEndDate;
    }

    public String getKpiFiscalYear() {
        return kpiFiscalYear;
    }

    public void setKpiFiscalYear(String kpiFiscalYear) {
        this.kpiFiscalYear = kpiFiscalYear;
    }

    public Long getKpiLevel() {
        return kpiLevel;
    }

    public void setKpiLevel(Long kpiLevel) {
        this.kpiLevel = kpiLevel;
    }
}
