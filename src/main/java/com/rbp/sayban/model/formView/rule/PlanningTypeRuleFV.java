/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.formView.rule;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name = "PLANNING_TYPE_RULE_VIEW")
public class PlanningTypeRuleFV extends BaseEntity {

    @Column(name = "FK_PV_PLANNING_TYPE_ID")
    private Long pvPlanningTypeId;

    @Column(name = "PV_PLANNING_TYPE_TITLE")
    private String pvPlanningTypeTitle;

    @Column(name = "FK_PV_PLANNING_TYPE_ID_CHILD")
    private Long pvPlansTypeIdChild;

    @Column(name = "PV_PLANNING_TYPE_TITLE_CHILD")
    private String pvPlansTypeTitleChild;

    public Long getPvPlanningTypeId() {
        return pvPlanningTypeId;
    }

    public void setPvPlanningTypeId(Long pvPlanningTypeId) {
        this.pvPlanningTypeId = pvPlanningTypeId;
    }

    public String getPvPlanningTypeTitle() {
        return pvPlanningTypeTitle;
    }

    public void setPvPlanningTypeTitle(String pvPlanningTypeTitle) {
        this.pvPlanningTypeTitle = pvPlanningTypeTitle;
    }

    public Long getPvPlansTypeIdChild() {
        return pvPlansTypeIdChild;
    }

    public void setPvPlansTypeIdChild(Long pvPlansTypeIdChild) {
        this.pvPlansTypeIdChild = pvPlansTypeIdChild;
    }

    public String getPvPlansTypeTitleChild() {
        return pvPlansTypeTitleChild;
    }

    public void setPvPlansTypeTitleChild(String pvPlansTypeTitleChild) {
        this.pvPlansTypeTitleChild = pvPlansTypeTitleChild;
    }
}
