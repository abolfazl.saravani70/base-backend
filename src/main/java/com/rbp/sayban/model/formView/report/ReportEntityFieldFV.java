/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.report;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.application.Field;
import com.rbp.sayban.model.domainmodel.report.OperandType;
import com.rbp.sayban.model.domainmodel.report.ReportEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "REPORT_ENTITY_FIELD_VIEW")
public class ReportEntityFieldFV extends BaseEntity {

    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;

    @Column(name = "FK_OPERAND_TYPE_ID")
    private Long operandTypeId;

    @Column(name = "OPERAND_TYPE_NAME")
    private String operandTypeName;

    @Column(name = "OPERAND_TYPE_TITLE")
    private String operandTypeTitle;

    @Column(name = "OPERAND_TYPE_KEY")
    private String operandTypeKey;

    @Column(name = "OPERAND_TYPE_CODE")
    private String operandTypeCode;

    @Column(name = "OPERAND_TYPE_VALUE")
    private String operandTypeValue;

    @Column(name = "OPERAND_TYPE_IS_DEPRECATED")
    private Boolean operandTypeIsDeprecated;

    @Column(name = "FK_REPORT_ENTITY_ID")
    private Long reportEntityId;

    @Column(name = "FK_FIELD_ID")
    private Long fieldId;

    @Column(name = "FIELD_NAME")
    private String fieldName;

    @Column(name = "FIELD_ENGLISH_NAME")
    private String fieldEngName;

    @Column(name = "FIELD_KEY")
    private Integer fieldKey;

    @Column(name = "FIELD_TYPE")
    private String fieldType;

    @Column(name = "FIELD_IS_DEPRECATED")
    private Boolean fieldIsDeprecated;

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Long getOperandTypeId() {
        return operandTypeId;
    }

    public void setOperandTypeId(Long operandTypeId) {
        this.operandTypeId = operandTypeId;
    }

    public String getOperandTypeName() {
        return operandTypeName;
    }

    public void setOperandTypeName(String operandTypeName) {
        this.operandTypeName = operandTypeName;
    }

    public String getOperandTypeTitle() {
        return operandTypeTitle;
    }

    public void setOperandTypeTitle(String operandTypeTitle) {
        this.operandTypeTitle = operandTypeTitle;
    }

    public String getOperandTypeKey() {
        return operandTypeKey;
    }

    public void setOperandTypeKey(String operandTypeKey) {
        this.operandTypeKey = operandTypeKey;
    }

    public String getOperandTypeCode() {
        return operandTypeCode;
    }

    public void setOperandTypeCode(String operandTypeCode) {
        this.operandTypeCode = operandTypeCode;
    }

    public String getOperandTypeValue() {
        return operandTypeValue;
    }

    public void setOperandTypeValue(String operandTypeValue) {
        this.operandTypeValue = operandTypeValue;
    }

    public Boolean getOperandTypeIsDeprecated() {
        return operandTypeIsDeprecated;
    }

    public void setOperandTypeIsDeprecated(Boolean operandTypeIsDeprecated) {
        this.operandTypeIsDeprecated = operandTypeIsDeprecated;
    }

    public Long getReportEntityId() {
        return reportEntityId;
    }

    public void setReportEntityId(Long reportEntityId) {
        this.reportEntityId = reportEntityId;
    }

    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldEngName() {
        return fieldEngName;
    }

    public void setFieldEngName(String fieldEngName) {
        this.fieldEngName = fieldEngName;
    }

    public Integer getFieldKey() {
        return fieldKey;
    }

    public void setFieldKey(Integer fieldKey) {
        this.fieldKey = fieldKey;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public Boolean getFieldIsDeprecated() {
        return fieldIsDeprecated;
    }

    public void setFieldIsDeprecated(Boolean fieldIsDeprecated) {
        this.fieldIsDeprecated = fieldIsDeprecated;
    }
}
