/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.application;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Immutable
@Table(name = "APPLICATION_FORM_VIEW")
public class ApplicationFormFV extends BaseEntity {


    @Column(name = "FK_APPLICATION_ID")
    private Long applicationId;

    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    @Column(name = "APPLICATION_TITLE")
    private String applicationTitle;

    @Column(name = "APPLICATION_CODE")
    private String applicationCode;

    @Column(name = "APPLICATION_PRIORITY")
    private Integer applicationPriority;

    @Column(name = "APPLICATION_NUMBER$")
    private Long applicationNumber;

    @Column(name = "APPLICATION_KEYWORDS")
    private String applicationKeyWords;

    @Column(name = "FK_FORM_ID")
    private Long formId;

    @Column(name = "FROM_NAME")
    private String formName;

    @Column(name = "FROM_TITLE")
    private String formTitle;

    @Column(name = "FROM_URL")
    private String formUrl;

    @Column(name = "FROM_CODE")
    private String formCode;

    @Column(name = "FROM_HELP_URL")
    private String formHelpUrl;

    @Column(name = "FROM_FROM_DATE")
    private LocalDate formFromDate;

    @Column(name = "FROM_FROM_TO_DATE")
    private LocalDate formToDate;

    @Column(name = "FROM_BODY")
    private String formBody;

    @Column(name = "FROM_IS_ACTIVE")
    private Boolean formIsActive;

    @Column(name = "FROM_IS_DEPRECATED")
    private Boolean formIsDeprecated;

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public Integer getApplicationPriority() {
        return applicationPriority;
    }

    public void setApplicationPriority(Integer applicationPriority) {
        this.applicationPriority = applicationPriority;
    }

    public Long getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(Long applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public String getApplicationKeyWords() {
        return applicationKeyWords;
    }

    public void setApplicationKeyWords(String applicationKeyWords) {
        this.applicationKeyWords = applicationKeyWords;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    public String getFormUrl() {
        return formUrl;
    }

    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getFormHelpUrl() {
        return formHelpUrl;
    }

    public void setFormHelpUrl(String formHelpUrl) {
        this.formHelpUrl = formHelpUrl;
    }

    public LocalDate getFormFromDate() {
        return formFromDate;
    }

    public void setFormFromDate(LocalDate formFromDate) {
        this.formFromDate = formFromDate;
    }

    public LocalDate getFormToDate() {
        return formToDate;
    }

    public void setFormToDate(LocalDate formToDate) {
        this.formToDate = formToDate;
    }

    public String getFormBody() {
        return formBody;
    }

    public void setFormBody(String formBody) {
        this.formBody = formBody;
    }

    public Boolean getFormIsActive() {
        return formIsActive;
    }

    public void setFormIsActive(Boolean formIsActive) {
        this.formIsActive = formIsActive;
    }

    public Boolean getFormIsDeprecated() {
        return formIsDeprecated;
    }

    public void setFormIsDeprecated(Boolean formIsDeprecated) {
        this.formIsDeprecated = formIsDeprecated;
    }
}
