package com.rbp.sayban.model.formView.todo;

import com.rbp.core.model.domainmodel.base.abstractClass.EvalStateBaseEntity;
import com.rbp.core.model.domainmodel.base.abstractClass.junction.BudgetJunctionBaseEntity;
import com.rbp.sayban.model.domainmodel.todo.Todo;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "TODO_BUDGET_VIEW")
public class TodoBudgetFV extends EvalStateBaseEntity {

    @Column(name = "ESTIMATE_BUDGET")
    private Long estimateBudget;

    @Column(name = "ACTUAL_BUDGET")
    private Long actualBudget;

    @Column(name = "FK_TODO_ID")
    private Long todoId;

    @Column(name = "TODO_PV_TYPE_TITLE")
    private String todoPvTypeTitle;

    @Column(name = "TODO_FK_PV_TYPE_ID")
    private Long  todoPvTypeId;

    @Column(name = "TODO_NAME")
    private String todoName;

    @Column(name = "TODO_TITLE")
    private String todoTitle;

    @Column(name = "TODO_IS_ROOT")
    private Boolean todoIsRoot;

    @Column(name = "TODO_INITIAL_PERCENTAGE")
    private Double todoInitialPercentage;

    @Column(name = "TODO_PERCENTAGE")
    private Double todoPercentage;

    @Column(name = "TODO_WEIGHT")
    private Long todoWeight;

    @Column(name = "TODO_VOLUME")
    private Long todoVolume;

    @Column(name = "TODO_PRIORITY")
    private Integer todoPriority;

    @Column(name = "TODO_IMPORTANCE")
    private Double todoImportance;

    @Column(name = "TODO_ACTUAL_START_DATE")
    private LocalDate todoActualStartDate;

    @Column(name = "TODO_ACTUAL_END_DATE")
    private LocalDate todoActualEndDate;

    @Column(name = "TODO_EXPIRATION_DATE")
    private LocalDate todoExpirationDate;

    @Column(name = "TODO_START_DATE")
    private LocalDate todoStartDate;

    @Column(name = "TODO_END_DATE")
    private LocalDate todoEndDate;

    @Column(name = "TODO_FISCAL_YEAR")
    private String todoFiscalYear;

    @Column(name = "TODO_LEVEL$")
    private Long todoLevel;

    @Column(name = "FK_BUDGET_ID")
    private Long budgetId;

    @Column(name = "BUDGET_NAME")
    private String budgetName;

    @Column(name = "BUDGET_TITLE")
    private String budgetTitle;

    @Column(name = "BUDGET_IS_ROOT")
    private Boolean budgetIsRoot;

    @Column(name = "BUDGET_INITIAL_PERCENTAGE")
    private Double budgetInitialPercentage;

    @Column(name = "BUDGET_PERCENTAGE")
    private Double budgetPercentage;

    @Column(name = "BUDGET_WEIGHT")
    private Long budgetWeight;

    @Column(name = "BUDGET_VOLUME")
    private Long budgetVolume;

    @Column(name = "BUDGET_PRIORITY")
    private Integer budgetPriority;

    @Column(name = "BUDGET_IMPORTANCE")
    private Double budgetImportance;

    @Column(name = "BUDGET_ACTUAL_START_DATE")
    private LocalDate budgetActualStartDate;

    @Column(name = "BUDGET_ACTUAL_END_DATE")
    private LocalDate budgetActualEndDate;

    @Column(name = "BUDGET_BUDGET_EXPIRATION_DATE")
    private LocalDate budgetExpirationDate;

    @Column(name = "BUDGET_START_DATE")
    private LocalDate budgetStartDate;

    @Column(name = "BUDGET_END_DATE")
    private LocalDate budgetEndDate;

    @Column(name = "BUDGET_FISCAL_YEAR")
    private String budgetFiscalYear;

    @Column(name = "BUDGET_LEVEL$")
    private Long budgetLevel;

    public Long getEstimateBudget() {
        return estimateBudget;
    }

    public void setEstimateBudget(Long estimateBudget) {
        this.estimateBudget = estimateBudget;
    }

    public Long getActualBudget() {
        return actualBudget;
    }

    public void setActualBudget(Long actualBudget) {
        this.actualBudget = actualBudget;
    }

    public Long getTodoId() {
        return todoId;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    public String getTodoPvTypeTitle() {
        return todoPvTypeTitle;
    }

    public void setTodoPvTypeTitle(String todoPvTypeTitle) {
        this.todoPvTypeTitle = todoPvTypeTitle;
    }

    public Long getTodoPvTypeId() {
        return todoPvTypeId;
    }

    public void setTodoPvTypeId(Long todoPvTypeId) {
        this.todoPvTypeId = todoPvTypeId;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    public Boolean getTodoIsRoot() {
        return todoIsRoot;
    }

    public void setTodoIsRoot(Boolean todoIsRoot) {
        this.todoIsRoot = todoIsRoot;
    }

    public Double getTodoInitialPercentage() {
        return todoInitialPercentage;
    }

    public void setTodoInitialPercentage(Double todoInitialPercentage) {
        this.todoInitialPercentage = todoInitialPercentage;
    }

    public Double getTodoPercentage() {
        return todoPercentage;
    }

    public void setTodoPercentage(Double todoPercentage) {
        this.todoPercentage = todoPercentage;
    }

    public Long getTodoWeight() {
        return todoWeight;
    }

    public void setTodoWeight(Long todoWeight) {
        this.todoWeight = todoWeight;
    }

    public Long getTodoVolume() {
        return todoVolume;
    }

    public void setTodoVolume(Long todoVolume) {
        this.todoVolume = todoVolume;
    }

    public Integer getTodoPriority() {
        return todoPriority;
    }

    public void setTodoPriority(Integer todoPriority) {
        this.todoPriority = todoPriority;
    }

    public Double getTodoImportance() {
        return todoImportance;
    }

    public void setTodoImportance(Double todoImportance) {
        this.todoImportance = todoImportance;
    }

    public LocalDate getTodoActualStartDate() {
        return todoActualStartDate;
    }

    public void setTodoActualStartDate(LocalDate todoActualStartDate) {
        this.todoActualStartDate = todoActualStartDate;
    }

    public LocalDate getTodoActualEndDate() {
        return todoActualEndDate;
    }

    public void setTodoActualEndDate(LocalDate todoActualEndDate) {
        this.todoActualEndDate = todoActualEndDate;
    }

    public LocalDate getTodoExpirationDate() {
        return todoExpirationDate;
    }

    public void setTodoExpirationDate(LocalDate todoExpirationDate) {
        this.todoExpirationDate = todoExpirationDate;
    }

    public LocalDate getTodoStartDate() {
        return todoStartDate;
    }

    public void setTodoStartDate(LocalDate todoStartDate) {
        this.todoStartDate = todoStartDate;
    }

    public LocalDate getTodoEndDate() {
        return todoEndDate;
    }

    public void setTodoEndDate(LocalDate todoEndDate) {
        this.todoEndDate = todoEndDate;
    }

    public String getTodoFiscalYear() {
        return todoFiscalYear;
    }

    public void setTodoFiscalYear(String todoFiscalYear) {
        this.todoFiscalYear = todoFiscalYear;
    }

    public Long getTodoLevel() {
        return todoLevel;
    }

    public void setTodoLevel(Long todoLevel) {
        this.todoLevel = todoLevel;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgetTitle() {
        return budgetTitle;
    }

    public void setBudgetTitle(String budgetTitle) {
        this.budgetTitle = budgetTitle;
    }

    public Boolean getBudgetIsRoot() {
        return budgetIsRoot;
    }

    public void setBudgetIsRoot(Boolean budgetIsRoot) {
        this.budgetIsRoot = budgetIsRoot;
    }

    public Double getBudgetInitialPercentage() {
        return budgetInitialPercentage;
    }

    public void setBudgetInitialPercentage(Double budgetInitialPercentage) {
        this.budgetInitialPercentage = budgetInitialPercentage;
    }

    public Double getBudgetPercentage() {
        return budgetPercentage;
    }

    public void setBudgetPercentage(Double budgetPercentage) {
        this.budgetPercentage = budgetPercentage;
    }

    public Long getBudgetWeight() {
        return budgetWeight;
    }

    public void setBudgetWeight(Long budgetWeight) {
        this.budgetWeight = budgetWeight;
    }

    public Long getBudgetVolume() {
        return budgetVolume;
    }

    public void setBudgetVolume(Long budgetVolume) {
        this.budgetVolume = budgetVolume;
    }

    public Integer getBudgetPriority() {
        return budgetPriority;
    }

    public void setBudgetPriority(Integer budgetPriority) {
        this.budgetPriority = budgetPriority;
    }

    public Double getBudgetImportance() {
        return budgetImportance;
    }

    public void setBudgetImportance(Double budgetImportance) {
        this.budgetImportance = budgetImportance;
    }

    public LocalDate getBudgetActualStartDate() {
        return budgetActualStartDate;
    }

    public void setBudgetActualStartDate(LocalDate budgetActualStartDate) {
        this.budgetActualStartDate = budgetActualStartDate;
    }

    public LocalDate getBudgetActualEndDate() {
        return budgetActualEndDate;
    }

    public void setBudgetActualEndDate(LocalDate budgetActualEndDate) {
        this.budgetActualEndDate = budgetActualEndDate;
    }

    public LocalDate getBudgetExpirationDate() {
        return budgetExpirationDate;
    }

    public void setBudgetExpirationDate(LocalDate budgetExpirationDate) {
        this.budgetExpirationDate = budgetExpirationDate;
    }

    public LocalDate getBudgetStartDate() {
        return budgetStartDate;
    }

    public void setBudgetStartDate(LocalDate budgetStartDate) {
        this.budgetStartDate = budgetStartDate;
    }

    public LocalDate getBudgetEndDate() {
        return budgetEndDate;
    }

    public void setBudgetEndDate(LocalDate budgetEndDate) {
        this.budgetEndDate = budgetEndDate;
    }

    public String getBudgetFiscalYear() {
        return budgetFiscalYear;
    }

    public void setBudgetFiscalYear(String budgetFiscalYear) {
        this.budgetFiscalYear = budgetFiscalYear;
    }

    public Long getBudgetLevel() {
        return budgetLevel;
    }

    public void setBudgetLevel(Long budgetLevel) {
        this.budgetLevel = budgetLevel;
    }
}
