/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.organizatin;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import com.rbp.sayban.model.domainmodel.organization.Organization;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ORG_UNIT_VIEW")
public class OrganizationUnitFV extends BaseEntity {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "IS_ENABLE")
    private Boolean isEnabled;

    @Column(name = "IS_ASSIGNED")
    Boolean isAssigned;

    @Column(name = "FK_ORG_CHART_ID")
    private Long organizationChartId;

    @Column(name = "ORG_CHART_NAME")
    private String orgCartName;

    @Column(name = "ORG_CHART_CODE")
    private String orgCartCode;

    @Column(name = "ORG_CHART_IS_VIRTUAL")
    private Boolean orgCartIsVirtual;

    @Column(name = "ORG_CHART_FV_PV_TYPE_ID")
    private Long orgCartPvTypeId;

    @Column(name = "ORG_CHART_PV_TYPE_TITLE")
    private String orgCartPvTypeTitle;

    @Column(name = "ORG_CHART_FK_PV_ORG_LEVEL_ID")
    private Long orgCartPvOrgLevelId;

    @Column(name = "ORG_CHART_PV_ORG_LEVEL_TITLE")
    private String orgCartPvOrgLevelTitle;

    @Column(name = "ORG_CHART_FK_PV_ORG_DEGREE_ID")
    private Long orgCartPvOrgDegreeId;

    @Column(name = "ORG_CHART_PV_ORG_DEGREE_TITLE")
    private String orgCartPvOrgDegreeTitle;

    @Column(name = "ORG_CHART_FK_PV_ORGANIZE_TYPE_ID")
    private Long orgCartPvOrganizeTypeId;

    @Column(name = "ORG_CHART_PV_ORGANIZE_TYPE_TITLE")
    private String orgCartPvOrganizeTypeTitle;

    @Column(name = "ORG_CHART_FK_PV_POSITION_NUM_ID")
    private Long orgCartPvPositionNumberId;

    @Column(name = "ORG_CHART_PV_POSITION_NUM_TITLE")
    private String orgCartPvPositionNumberTitle;

    @Column(name = "ORG_CHART_FK_PV_POSITION_TIT_ID")
    private Long orgCartPvPositionTitleId;

    @Column(name = "ORG_CHART_PV_POSITION_TIT_TITLE")
    private String orgCartPvPositionTitleTitle;

    @Column(name = "FK_ORG_ID")
    private Long orgId;

    @Column(name = "ORG_NAME")
    private String orgName;

    @Column(name = "ORG_TITLE")
    private String orgTitle;

    @Column(name = "ORG_ORG_CODE")
    private String orgOrgCode;

    @Column(name = "ORG_ZAMZAM_CODE")
    private String orgZamzamCode;

    @Column(name = "ORG_STRATEGIC_CODE")
    private String orgStrategicCode;

    @Column(name = "ORG_IS_ORGANIZED")
    private Boolean orgIsOrganized;

    @Column(name = "ORG_FK_PV_ORG_LEVEL_ID")
    private Long orgPvOrgLevelId;

    @Column(name = "ORG_PV_ORG_LEVEL_TITLE")
    private String orgPvOrgLevelTitle;

    @Column(name = "ORG_FK_PV_ORG_DEGREE_ID")
    private Long orgPvOrgDegreeId;

    @Column(name = "ORG_PV_ORG_DEGREE_TITLE")
    private String orgPvOrgDegreeTitle;

    @Column(name = "ORG_FK_PV_ORG_TYPE_ID")
    private Long orgPvOrgTypeId;

    @Column(name = "ORG_PV_ORG_TYPE_TITLE")
    private String orgPvOrgTypeTitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public Boolean getAssigned() {
        return isAssigned;
    }

    public void setAssigned(Boolean assigned) {
        isAssigned = assigned;
    }

    public Long getOrganizationChartId() {
        return organizationChartId;
    }

    public void setOrganizationChartId(Long organizationChartId) {
        this.organizationChartId = organizationChartId;
    }

    public String getOrgCartName() {
        return orgCartName;
    }

    public void setOrgCartName(String orgCartName) {
        this.orgCartName = orgCartName;
    }

    public String getOrgCartCode() {
        return orgCartCode;
    }

    public void setOrgCartCode(String orgCartCode) {
        this.orgCartCode = orgCartCode;
    }

    public Boolean getOrgCartIsVirtual() {
        return orgCartIsVirtual;
    }

    public void setOrgCartIsVirtual(Boolean orgCartIsVirtual) {
        this.orgCartIsVirtual = orgCartIsVirtual;
    }

    public Long getOrgCartPvTypeId() {
        return orgCartPvTypeId;
    }

    public void setOrgCartPvTypeId(Long orgCartPvTypeId) {
        this.orgCartPvTypeId = orgCartPvTypeId;
    }

    public String getOrgCartPvTypeTitle() {
        return orgCartPvTypeTitle;
    }

    public void setOrgCartPvTypeTitle(String orgCartPvTypeTitle) {
        this.orgCartPvTypeTitle = orgCartPvTypeTitle;
    }

    public Long getOrgCartPvOrgLevelId() {
        return orgCartPvOrgLevelId;
    }

    public void setOrgCartPvOrgLevelId(Long orgCartPvOrgLevelId) {
        this.orgCartPvOrgLevelId = orgCartPvOrgLevelId;
    }

    public String getOrgCartPvOrgLevelTitle() {
        return orgCartPvOrgLevelTitle;
    }

    public void setOrgCartPvOrgLevelTitle(String orgCartPvOrgLevelTitle) {
        this.orgCartPvOrgLevelTitle = orgCartPvOrgLevelTitle;
    }

    public Long getOrgCartPvOrgDegreeId() {
        return orgCartPvOrgDegreeId;
    }

    public void setOrgCartPvOrgDegreeId(Long orgCartPvOrgDegreeId) {
        this.orgCartPvOrgDegreeId = orgCartPvOrgDegreeId;
    }

    public String getOrgCartPvOrgDegreeTitle() {
        return orgCartPvOrgDegreeTitle;
    }

    public void setOrgCartPvOrgDegreeTitle(String orgCartPvOrgDegreeTitle) {
        this.orgCartPvOrgDegreeTitle = orgCartPvOrgDegreeTitle;
    }

    public Long getOrgCartPvOrganizeTypeId() {
        return orgCartPvOrganizeTypeId;
    }

    public void setOrgCartPvOrganizeTypeId(Long orgCartPvOrganizeTypeId) {
        this.orgCartPvOrganizeTypeId = orgCartPvOrganizeTypeId;
    }

    public String getOrgCartPvOrganizeTypeTitle() {
        return orgCartPvOrganizeTypeTitle;
    }

    public void setOrgCartPvOrganizeTypeTitle(String orgCartPvOrganizeTypeTitle) {
        this.orgCartPvOrganizeTypeTitle = orgCartPvOrganizeTypeTitle;
    }

    public Long getOrgCartPvPositionNumberId() {
        return orgCartPvPositionNumberId;
    }

    public void setOrgCartPvPositionNumberId(Long orgCartPvPositionNumberId) {
        this.orgCartPvPositionNumberId = orgCartPvPositionNumberId;
    }

    public String getOrgCartPvPositionNumberTitle() {
        return orgCartPvPositionNumberTitle;
    }

    public void setOrgCartPvPositionNumberTitle(String orgCartPvPositionNumberTitle) {
        this.orgCartPvPositionNumberTitle = orgCartPvPositionNumberTitle;
    }

    public Long getOrgCartPvPositionTitleId() {
        return orgCartPvPositionTitleId;
    }

    public void setOrgCartPvPositionTitleId(Long orgCartPvPositionTitleId) {
        this.orgCartPvPositionTitleId = orgCartPvPositionTitleId;
    }

    public String getOrgCartPvPositionTitleTitle() {
        return orgCartPvPositionTitleTitle;
    }

    public void setOrgCartPvPositionTitleTitle(String orgCartPvPositionTitleTitle) {
        this.orgCartPvPositionTitleTitle = orgCartPvPositionTitleTitle;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgTitle() {
        return orgTitle;
    }

    public void setOrgTitle(String orgTitle) {
        this.orgTitle = orgTitle;
    }

    public String getOrgOrgCode() {
        return orgOrgCode;
    }

    public void setOrgOrgCode(String orgOrgCode) {
        this.orgOrgCode = orgOrgCode;
    }

    public String getOrgZamzamCode() {
        return orgZamzamCode;
    }

    public void setOrgZamzamCode(String orgZamzamCode) {
        this.orgZamzamCode = orgZamzamCode;
    }

    public String getOrgStrategicCode() {
        return orgStrategicCode;
    }

    public void setOrgStrategicCode(String orgStrategicCode) {
        this.orgStrategicCode = orgStrategicCode;
    }

    public Boolean getOrgIsOrganized() {
        return orgIsOrganized;
    }

    public void setOrgIsOrganized(Boolean orgIsOrganized) {
        this.orgIsOrganized = orgIsOrganized;
    }

    public Long getOrgPvOrgLevelId() {
        return orgPvOrgLevelId;
    }

    public void setOrgPvOrgLevelId(Long orgPvOrgLevelId) {
        this.orgPvOrgLevelId = orgPvOrgLevelId;
    }

    public String getOrgPvOrgLevelTitle() {
        return orgPvOrgLevelTitle;
    }

    public void setOrgPvOrgLevelTitle(String orgPvOrgLevelTitle) {
        this.orgPvOrgLevelTitle = orgPvOrgLevelTitle;
    }

    public Long getOrgPvOrgDegreeId() {
        return orgPvOrgDegreeId;
    }

    public void setOrgPvOrgDegreeId(Long orgPvOrgDegreeId) {
        this.orgPvOrgDegreeId = orgPvOrgDegreeId;
    }

    public String getOrgPvOrgDegreeTitle() {
        return orgPvOrgDegreeTitle;
    }

    public void setOrgPvOrgDegreeTitle(String orgPvOrgDegreeTitle) {
        this.orgPvOrgDegreeTitle = orgPvOrgDegreeTitle;
    }

    public Long getOrgPvOrgTypeId() {
        return orgPvOrgTypeId;
    }

    public void setOrgPvOrgTypeId(Long orgPvOrgTypeId) {
        this.orgPvOrgTypeId = orgPvOrgTypeId;
    }

    public String getOrgPvOrgTypeTitle() {
        return orgPvOrgTypeTitle;
    }

    public void setOrgPvOrgTypeTitle(String orgPvOrgTypeTitle) {
        this.orgPvOrgTypeTitle = orgPvOrgTypeTitle;
    }
}
