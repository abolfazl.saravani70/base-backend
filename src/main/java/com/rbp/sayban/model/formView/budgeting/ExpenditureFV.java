/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.formView.budgeting;

import com.rbp.core.model.domainmodel.base.abstractClass.BaseEntity;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Table(name = "EXPENDITURE_VIEW")
public class ExpenditureFV extends BaseEntity {

    @Column(name = "FK_TASK_ID")
    private Long taskId;

    @Column(name = "TASK_NAME")
    private String taskName;

    @Column(name = "TASK_TITLE")
    private String taskTitle;

    @Column(name = "TASK_IS_ROOT")
    private Boolean taskIsRoot;

    @Column(name = "TASK_INITIAL_PERCENTAGE")
    private Double taskInitialPercentage;

    @Column(name = "TASK_PERCENTAGE")
    private Double taskPercentage;

    @Column(name = "TASK_WEIGHT")
    private Long taskWeight;

    @Column(name = "TASK_VOLUME")
    private Long taskVolume;

    @Column(name = "TASK_PRIORITY")
    private Integer taskPriority;

    @Column(name = "TASK_IMPORTANCE")
    private Double taskImportance;

    @Column(name = "TASK_ACTUAL_START_DATE")
    private LocalDate taskActualStartDate;

    @Column(name = "TASK_ACTUAL_END_DATE")
    private LocalDate taskActualEndDate;

    @Column(name = "TASK_EXPIRATION_DATE")
    private LocalDate taskExpirationDate;

    @Column(name = "TASK_START_DATE")
    private LocalDate taskStartDate;

    @Column(name = "TASK_END_DATE")
    private LocalDate taskEndDate;

    @Column(name = "TASK_FISCAL_YEAR")
    private String taskFiscalYear;

    @Column(name = "TASK_LEVEL$")
    private Long taskLevel;

    @Column(name = "TASK_FK_PV_TYPE_ID")
    private Long taskPvTypeId;

    @Column(name = "TASK_PV_TYPE_TITLE")
    private String taskPvTypeTitle;

    @Column(name = "FK_TODO_ID")
    private Long todoId;

    @Column(name = "TODO_FK_PV_TYPE_ID")
    private Long todoPvTypeId;

    @Column(name = "TODO_PV_TYPE_TITLE")
    private String todoPvTypeTitle;

    @Column(name = "TODO_NAME")
    private String todoName;

    @Column(name = "TODO_TITLE")
    private String todoTitle;

    @Column(name = "TODO_IS_ROOT")
    private Boolean todoIsRoot;

    @Column(name = "TODO_INITIAL_PERCENTAGE")
    private Double todoInitialPercentage;

    @Column(name = "TODO_PERCENTAGE")
    private Double todoPercentage;

    @Column(name = "TODO_WEIGHT")
    private Long todoWeight;

    @Column(name = "TODO_VOLUME")
    private Long todoVolume;

    @Column(name = "TODO_PRIORITY")
    private Integer todoPriority;

    @Column(name = "TODO_IMPORTANCE")
    private Double todoImportance;

    @Column(name = "TODO_ACTUAL_START_DATE")
    private LocalDate todoActualStartDate;

    @Column(name = "TODO_ACTUAL_END_DATE")
    private LocalDate todoActualEndDate;

    @Column(name = "TODO_EXPIRATION_DATE")
    private LocalDate todoExpirationDate;

    @Column(name = "TODO_START_DATE")
    private LocalDate todoStartDate;

    @Column(name = "TODO_END_DATE")
    private LocalDate todoEndDate;

    @Column(name = "TODO_FISCAL_YEAR")
    private String todoFiscalYear;

    @Column(name = "TODO_LEVEL$")
    private Long todoLevel;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public Boolean getTaskIsRoot() {
        return taskIsRoot;
    }

    public void setTaskIsRoot(Boolean taskIsRoot) {
        this.taskIsRoot = taskIsRoot;
    }

    public Double getTaskInitialPercentage() {
        return taskInitialPercentage;
    }

    public void setTaskInitialPercentage(Double taskInitialPercentage) {
        this.taskInitialPercentage = taskInitialPercentage;
    }

    public Double getTaskPercentage() {
        return taskPercentage;
    }

    public void setTaskPercentage(Double taskPercentage) {
        this.taskPercentage = taskPercentage;
    }

    public Long getTaskWeight() {
        return taskWeight;
    }

    public void setTaskWeight(Long taskWeight) {
        this.taskWeight = taskWeight;
    }

    public Long getTaskVolume() {
        return taskVolume;
    }

    public void setTaskVolume(Long taskVolume) {
        this.taskVolume = taskVolume;
    }

    public Integer getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
    }

    public Double getTaskImportance() {
        return taskImportance;
    }

    public void setTaskImportance(Double taskImportance) {
        this.taskImportance = taskImportance;
    }

    public LocalDate getTaskActualStartDate() {
        return taskActualStartDate;
    }

    public void setTaskActualStartDate(LocalDate taskActualStartDate) {
        this.taskActualStartDate = taskActualStartDate;
    }

    public LocalDate getTaskActualEndDate() {
        return taskActualEndDate;
    }

    public void setTaskActualEndDate(LocalDate taskActualEndDate) {
        this.taskActualEndDate = taskActualEndDate;
    }

    public LocalDate getTaskExpirationDate() {
        return taskExpirationDate;
    }

    public void setTaskExpirationDate(LocalDate taskExpirationDate) {
        this.taskExpirationDate = taskExpirationDate;
    }

    public LocalDate getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(LocalDate taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public LocalDate getTaskEndDate() {
        return taskEndDate;
    }

    public void setTaskEndDate(LocalDate taskEndDate) {
        this.taskEndDate = taskEndDate;
    }

    public String getTaskFiscalYear() {
        return taskFiscalYear;
    }

    public void setTaskFiscalYear(String taskFiscalYear) {
        this.taskFiscalYear = taskFiscalYear;
    }

    public Long getTaskLevel() {
        return taskLevel;
    }

    public void setTaskLevel(Long taskLevel) {
        this.taskLevel = taskLevel;
    }

    public Long getTaskPvTypeId() {
        return taskPvTypeId;
    }

    public void setTaskPvTypeId(Long taskPvTypeId) {
        this.taskPvTypeId = taskPvTypeId;
    }

    public String getTaskPvTypeTitle() {
        return taskPvTypeTitle;
    }

    public void setTaskPvTypeTitle(String taskPvTypeTitle) {
        this.taskPvTypeTitle = taskPvTypeTitle;
    }

    public Long getTodoId() {
        return todoId;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    public Long getTodoPvTypeId() {
        return todoPvTypeId;
    }

    public void setTodoPvTypeId(Long todoPvTypeId) {
        this.todoPvTypeId = todoPvTypeId;
    }

    public String getTodoPvTypeTitle() {
        return todoPvTypeTitle;
    }

    public void setTodoPvTypeTitle(String todoPvTypeTitle) {
        this.todoPvTypeTitle = todoPvTypeTitle;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoTitle() {
        return todoTitle;
    }

    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    public Boolean getTodoIsRoot() {
        return todoIsRoot;
    }

    public void setTodoIsRoot(Boolean todoIsRoot) {
        this.todoIsRoot = todoIsRoot;
    }

    public Double getTodoInitialPercentage() {
        return todoInitialPercentage;
    }

    public void setTodoInitialPercentage(Double todoInitialPercentage) {
        this.todoInitialPercentage = todoInitialPercentage;
    }

    public Double getTodoPercentage() {
        return todoPercentage;
    }

    public void setTodoPercentage(Double todoPercentage) {
        this.todoPercentage = todoPercentage;
    }

    public Long getTodoWeight() {
        return todoWeight;
    }

    public void setTodoWeight(Long todoWeight) {
        this.todoWeight = todoWeight;
    }

    public Long getTodoVolume() {
        return todoVolume;
    }

    public void setTodoVolume(Long todoVolume) {
        this.todoVolume = todoVolume;
    }

    public Integer getTodoPriority() {
        return todoPriority;
    }

    public void setTodoPriority(Integer todoPriority) {
        this.todoPriority = todoPriority;
    }

    public Double getTodoImportance() {
        return todoImportance;
    }

    public void setTodoImportance(Double todoImportance) {
        this.todoImportance = todoImportance;
    }

    public LocalDate getTodoActualStartDate() {
        return todoActualStartDate;
    }

    public void setTodoActualStartDate(LocalDate todoActualStartDate) {
        this.todoActualStartDate = todoActualStartDate;
    }

    public LocalDate getTodoActualEndDate() {
        return todoActualEndDate;
    }

    public void setTodoActualEndDate(LocalDate todoActualEndDate) {
        this.todoActualEndDate = todoActualEndDate;
    }

    public LocalDate getTodoExpirationDate() {
        return todoExpirationDate;
    }

    public void setTodoExpirationDate(LocalDate todoExpirationDate) {
        this.todoExpirationDate = todoExpirationDate;
    }

    public LocalDate getTodoStartDate() {
        return todoStartDate;
    }

    public void setTodoStartDate(LocalDate todoStartDate) {
        this.todoStartDate = todoStartDate;
    }

    public LocalDate getTodoEndDate() {
        return todoEndDate;
    }

    public void setTodoEndDate(LocalDate todoEndDate) {
        this.todoEndDate = todoEndDate;
    }

    public String getTodoFiscalYear() {
        return todoFiscalYear;
    }

    public void setTodoFiscalYear(String todoFiscalYear) {
        this.todoFiscalYear = todoFiscalYear;
    }

    public Long getTodoLevel() {
        return todoLevel;
    }

    public void setTodoLevel(Long todoLevel) {
        this.todoLevel = todoLevel;
    }
}
