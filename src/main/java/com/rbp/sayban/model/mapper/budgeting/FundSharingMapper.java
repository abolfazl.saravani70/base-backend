/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.FundSharing;
import com.rbp.sayban.model.dto.budgeting.FundSharingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface FundSharingMapper extends GenericMapper<FundSharingDTO, FundSharing> {
    FundSharingMapper INSTANCE = Mappers.getMapper(FundSharingMapper.class);

    @Mapping(source = "fund", target = "fundId")
    @Mapping(source = "sharing", target = "sharingId")
    FundSharingDTO toView(FundSharing fundSharing);

    @Mapping(source = "fundId", target = "fund")
    @Mapping(source = "sharingId", target = "sharing")
    FundSharing toDomainModel(FundSharingDTO fundSharingDTO);
}
