/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.ProcessTemplate;
import com.rbp.sayban.model.dto.system.ProcessTemplateDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProcessTemplateMapper extends GenericMapper<ProcessTemplateDTO, ProcessTemplate> {

    ProcessTemplateMapper INSTANCE = Mappers.getMapper(ProcessTemplateMapper.class);

    @Mapping(source = "application", target = "applicationId")
    ProcessTemplateDTO toView(ProcessTemplate ProcessTemplate);

//    @Mapping(source = "applicationId", target = "application")
//    SysRule toDomainModel(ProcessTemplateDTO ProcessTemplateDTO);

}