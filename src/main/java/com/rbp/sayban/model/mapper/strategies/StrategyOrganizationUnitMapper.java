package com.rbp.sayban.model.mapper.strategies;


import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.domainmodel.strategies.StrategyOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.strategies.StrategyOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyOrganizationUnitMapper extends GenericMapper<StrategyOrganizationUnitViewModel, StrategyOrganizationUnit> {
    StrategyOrganizationUnitMapper INSTANCE = Mappers.getMapper(StrategyOrganizationUnitMapper.class);

    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    StrategyOrganizationUnit toDomainModel(StrategyOrganizationUnitViewModel strategyOrganizationUnitViewModel);


    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    StrategyOrganizationUnitViewModel toView(StrategyOrganizationUnit strategyOrganizationUnit);
}
