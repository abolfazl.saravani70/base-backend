/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;

import com.rbp.sayban.model.domainmodel.application.Field;
import com.rbp.sayban.model.dto.application.FieldDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface FieldMapper extends GenericMapper<FieldDTO, Field> {
    FieldMapper INSTANCE = Mappers.getMapper(FieldMapper.class);

    @Mapping(source = "application",target = "applicationId")
    @Mapping(source = "appEntity",target = "appEntityId")
    FieldDTO toView(Field field);

    @Mapping(source = "applicationId",target = "application")
    @Mapping(source = "appEntityId",target = "appEntity")
    Field toDomainModel(FieldDTO fieldDTO);
}
