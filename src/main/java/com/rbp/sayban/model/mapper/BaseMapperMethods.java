/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.co
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper;

import com.rbp.core.model.domainmodel.base.FrameworkProperty;
import com.rbp.core.model.domainmodel.base.FrameworkPropertyValue;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Bank;
import com.rbp.core.model.domainmodel.basicInformation.bankingInfo.Branch;
import com.rbp.core.model.domainmodel.basicInformation.geographical.*;
import com.rbp.core.model.domainmodel.security.Group;
import com.rbp.core.model.domainmodel.security.Permission;
import com.rbp.core.model.domainmodel.security.User;
import com.rbp.core.model.dto.base.FilterJsonInput;
import com.rbp.core.service.base.IFrameWorkPropertyService;
import com.rbp.core.service.base.IFrameWorkPropertyValueService;
import com.rbp.core.service.basicInformation.bankingInfo.IBankService;
import com.rbp.core.service.basicInformation.bankingInfo.IBranchService;
import com.rbp.core.service.basicInformation.geographical.*;
import com.rbp.core.service.security.IGroupService;
import com.rbp.core.service.security.IPermissionService;
import com.rbp.core.service.security.IUserService;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.model.domainmodel.activity.*;
import com.rbp.sayban.model.domainmodel.application.*;
import com.rbp.sayban.model.domainmodel.budgeting.*;
import com.rbp.sayban.model.domainmodel.dep.*;
import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.domainmodel.kpi.Kpi;
import com.rbp.sayban.model.domainmodel.kpi.Unit;
import com.rbp.sayban.model.domainmodel.norm.NormCost;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.domainmodel.organization.*;
import com.rbp.sayban.model.domainmodel.planning.Plan;
import com.rbp.sayban.model.domainmodel.project.Contract;
import com.rbp.sayban.model.domainmodel.project.Project;
import com.rbp.sayban.model.domainmodel.project.ProjectResourceType;
import com.rbp.sayban.model.domainmodel.report.FilterOperator;
import com.rbp.sayban.model.domainmodel.report.ReportFilter;
import com.rbp.sayban.model.domainmodel.risk.Alternative;
import com.rbp.sayban.model.domainmodel.risk.Risk;
import com.rbp.sayban.model.domainmodel.risk.Solution;
import com.rbp.sayban.model.domainmodel.strategies.Strategy;
import com.rbp.sayban.model.domainmodel.subjective.Subjective;
import com.rbp.sayban.model.domainmodel.system.System;
import com.rbp.sayban.model.domainmodel.system.*;
import com.rbp.sayban.model.domainmodel.task.Task;
import com.rbp.sayban.model.domainmodel.todo.Todo;
import com.rbp.sayban.service.activity.IActivityService;
import com.rbp.sayban.service.activity.INormCostService;
import com.rbp.sayban.service.goal.IGoalService;
import com.rbp.sayban.service.kpi.IKpiService;
import com.rbp.sayban.service.objectives.IObjectiveService;
import com.rbp.sayban.service.organization.*;
import com.rbp.sayban.service.planning.IPlanService;
import com.rbp.sayban.service.project.IProjectService;
import com.rbp.sayban.service.risk.IRiskService;
import com.rbp.sayban.service.strategies.IStrategyService;
import com.rbp.sayban.service.subjective.ISubjectiveService;
import com.rbp.sayban.service.task.ITaskService;
import com.rbp.sayban.service.todo.ITodoService;
import org.hibernate.engine.jdbc.BlobProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Base64;

/**
 * This class has all the methods to convert and map types.
 * All the mappers use this class if they don't know how to map A->B
 * Usually here we use Object ->Long and vice versa.
 * THIS CLASS DOES NOT DO REFERENCE TO REFERENCE MAPPING!
 * EACH METHOD HERE CREATES A NEW OBJECT!
 * IF YOU WANT YOUR OBJECT NOT TO GET A NEW REFERENCE, YOU HVE TO USE UPDATE FROM MAPSTRUCT!
 * all the methods must be public so mapstruct can find them!
 */

@Component
public class BaseMapperMethods {
    //TODO:AutoWire all the services you need here!
    //TODO:All the service beans must be proxed and change to Prototype Scope!


    @Autowired
    GenericMapperService genericMapperService;

    @Autowired
    IEmailService iEmailService;
    public Long emailToLong(Email e){
        if(e!=null) return e.getId();
        else return null;
    }
    public Email longToEmail(Long l){
        return iEmailService.getReference(l);
    }

    @Autowired
    IAddressService iAddressService;
    public Long addressToLong(Address a){
        if(a!=null) return a.getId();
        else return null;
    }
    public Address longToAddress(Long l){
        return iAddressService.getReference(l);
    }

    @Autowired
    IPhoneService iPhoneService;
    public Long phoneToLong(Phone a){
        if(a!=null) return a.getId();
        else return null;
    }
    public Phone longToPhone(Long l){
        return iPhoneService.getReference(l);
    }

    public java.sql.Blob ByteArrarytoBlob(byte[] input) {
        if (input == null) return null;
        return BlobProxy.generateProxy(input);
    }

    public byte[] BlobToByteArray(java.sql.Blob input) {
        if (input == null) return null;
        try {
            return input.getBytes(0, (int) input.length());
        } catch (SQLException e) {
            throw new ApplicationException(1, "خطا در دریافت بایت از Blob.(Mapper)");
        }
    }


    @Autowired
    IFrameWorkPropertyValueService iFrameWorkPropertyValueService;

    public String pvIdToTitle(Long id) {
        FilterJsonInput filter=new FilterJsonInput();
        filter.where("id=" + id);
        filter.pageSize(1);
        if (id != null) {
            FrameworkPropertyValue pv = iFrameWorkPropertyValueService
                    .searchEntity(filter, false)
                    .getRecords().get(0);
            return pv.getName();
        } else
            return null;
    }


    public String ByteArrayToString(byte[] bytes) throws UnsupportedEncodingException {
        return new String(bytes, "UTF-8");
    }


    public byte[] StringToByteArray(String file) {

        return Base64.getDecoder().decode(file);

    }

    public Long Subjective(Subjective subjective) {
        return subjective.getId();
    }

    @Autowired
    ISubjectiveService iSubjectiveService;

    public Subjective LongToSubjective(Long id) {
        return iSubjectiveService.getReference(id);
    }

    public Long FrameWorkProperty(FrameworkProperty frameWorkProperty) {
        return frameWorkProperty.getCode().longValue();
    }

    @Autowired
    IFrameWorkPropertyService iFrameWorkPropertyService;

    public FrameworkProperty LongToFrameworkProperty(Long code) {
        FilterJsonInput filter=new FilterJsonInput();
        filter.where("code=" + code);
        filter.pageSize(1);
        return iFrameWorkPropertyService
                .searchEntity(filter, false)
                .getRecords().get(0);
    }

    public Long OrganizationChartToLong(OrganizationChart organizationChart) {
        return organizationChart.getId();
    }

    @Autowired
    IOrganizationChartService iOrganizationChartService;

    public OrganizationChart LongToOrganizationChart(Long OrganizationChartId) {
        OrganizationChart organizationChart;
        organizationChart = iOrganizationChartService.getReference(OrganizationChartId);
        return organizationChart;
    }

    public Long OrganizationLocationToLong(OrganizationLocation organizationLocation) {
        return organizationLocation.getId();
    }

    public OrganizationLocation LongToOrganizationLocation(Long OrganizationLocationId) {
        OrganizationLocation organizationLocation = new OrganizationLocation();
        organizationLocation.setId(OrganizationLocationId);
        //@TODO:write service to get this
        return organizationLocation;
    }

    public Long LocationToLong(Location location) {
        return location.getId();
    }

    @Autowired
    ILocationService iLocationService;

    public Location LongToLocation(Long locationId) {
        return iLocationService.getReference(locationId);
    }

    public Long CommentToLong(Comment comment) {
        return comment.getId();
    }

    public Comment LongToComment(Long CommentId) {
        Comment comment = new Comment();
        comment.setId(CommentId);
        //TODO: write service to get this
        return comment;
    }

    public Long HumanResourceToLong(HumanResource humanResource) {
        return humanResource.getId();
    }

    @Autowired
    IHumanResourceService iHumanResourceService;

    public HumanResource LongToHumanResource(Long humanResourceId) {
        return iHumanResourceService.getReference(humanResourceId);
    }

    public Long SysScopeToLong(SysScope sysScope) {
        return sysScope.getId();
    }

    public SysScope LongToSysScope(Long sysScopeId) {
        SysScope sysScope = new SysScope();
        sysScope.setId(sysScopeId);
        //TODO: write service to get this
        return sysScope;
    }


    public Long SysRuleToLong(SysRule sysRule) {
        return sysRule.getId();
    }

    public SysRule LongToSysRule(Long SysRuleId) {
        SysRule sysRule = new SysRule();
        sysRule.setId(SysRuleId);
        //TODO: write service to get this
        return sysRule;
    }

    public Long PlanToLong(Plan plan) {

        if (plan != null)
            return plan.getId();
        else
            return null;
    }

    @Autowired
    IPlanService iPlanService;

    public Plan LongToPlan(Long planId) {
        if (planId != null)
            return iPlanService.getReference(planId);
        else
            return null;
    }

    public Long ZoneToLong(Zone zone) {
        return zone.getId();
    }

    @Autowired
    IZoneService iZoneService;

    public Zone LongToZone(Long zoneId) {
        return iZoneService.getReference(zoneId);
    }

    public Long VillageToLong(Village village) {
        return village.getId();
    }

    @Autowired
    IVillageService iVillageService;

    public Village LongToVillage(Long villageId) {
        Village village = iVillageService.getReference(villageId);

        return village;
    }

    public Long CityToLong(City city) {
        return city.getId();
    }

    @Autowired
    ICityService iCityService;

    public City LongToCity(Long cityId) {
        return iCityService.getReference(cityId);
    }

    public Long ProjectResourceTypeToLong(ProjectResourceType projectResourceType) {
        return projectResourceType.getId();
    }

    public ProjectResourceType LongToProjectResourceType(Long projectResourceTypeId) {
        ProjectResourceType projectResourceType = new ProjectResourceType();
        projectResourceType.setId(projectResourceTypeId);
        //@TODO: write service to get this.

        return projectResourceType;
    }

    public Long ProjectToLong(Project project) {
        if (project != null)
            return project.getId();
        else
            return null;
    }

    @Autowired
    IProjectService iProjectService;

    public Project LongToProject(Long projectId) {
        if (projectId != null)
            return iProjectService.getReference(projectId);
        else
            return null;
    }

    public Long ActSharingToLong(ActSharing actSharing) {
        return actSharing.getId();
    }

    public ActSharing LongToActSharing(Long actSharingId) {
        ActSharing actSharing = new ActSharing();
        actSharing.setId(actSharingId);
        //@TODO: write service to get this.
        return actSharing;
    }

    public Long RuleFormulaToLong(SysRuleFormula ruleFormula) {
        return ruleFormula.getId();
    }

    public SysRuleFormula LongToRuleFormula(Long ruleFormulaId) {
        SysRuleFormula ruleFormula = new SysRuleFormula();
        ruleFormula.setId(ruleFormulaId);
        //@TODO: write service to get this
        return ruleFormula;
    }

    public Long RuleToLong(Rule rule) {
        return rule.getId();
    }

    public Rule LongToRule(Long ruleId) {
        Rule rule = new Rule();
        rule.setId(ruleId);
        //@TODO: write service to get this.

        return rule;
    }


    public Long TaskToLong(Task task) {

        if (task != null)
            return task.getId();
        else
            return null;
    }

    @Autowired
    ITaskService iTaskService;

    public Task LongToTask(Long taskId) {

        if (taskId != null)
            return iTaskService.getReference(taskId);
        else
            return null;

    }

    public Long StateToLong(State state) {
        return state.getId();
    }

    @Autowired
    IStateService iStateService;

    public State LongToState(Long stateId) {
        return iStateService.getReference(stateId);
    }

    public Long AllocationToLong(Allocation allocation) {
        return allocation.getId();
    }

    public Allocation LongToAllocation(Long allocationId) {
        Allocation allocation = new Allocation();
        allocation.setId(allocationId);
        //TODO: writer service to get this.

        return allocation;
    }

    public Long BankToLong(Bank bank) {
        return bank.getId();
    }

    @Autowired
    IBankService iBankService;

    public Bank LongToBank(Long bankId) {
        Bank bank = iBankService.getReference(bankId);
        return bank;
    }

    public Long UnitToUnit(Unit unit) {
        return unit.getId();
    }

    public Unit LongToUnit(Long unitId) {
        Unit unit = new Unit();
        unit.setId(unitId);
        //TODO: write service to get this.
        return unit;
    }

    public Long RequesterToLong(Requester requester) {
        return requester.getId();
    }

    public Requester LongToRequester(Long requesterId) {
        Requester requester = new Requester();
        requester.setId(requesterId);
        //TODO: write service to get this.
        return requester;
    }

    public Long RequestToLong(Request request) {
        return request.getId();
    }

    public Request LongToRequest(Long requestId) {
        Request request = new Request();
        request.setId(requestId);
        //@TODO: write service to get this.

        return request;
    }

    public Long BranchToLong(Branch branch) {
        return branch.getId();
    }

    @Autowired
    IBranchService iBranchService;

    public Branch LongToBranch(Long branchId) {
        Branch branch = iBranchService.getReference(branchId);
        return branch;
    }

    public Long UserToLong(User user) {
        return user.getId();
    }

    @Autowired
    IUserService iUserService;

    public User LongToUser(Long userId) {
        return iUserService.getReference(userId);
    }

//    public Long OrganizationPosiotionToLong(OrganizationPosition organizationPosition) {
//        return organizationPosition.getId();
//    }
//
//    @Autowired
//    IOrganizationPositionService iOrganizationPositionService;
//
//    public OrganizationPosition LongToOrganizationPosition(Long organizationPositionId) {
//        return iOrganizationPositionService.loadById(organizationPositionId);
//    }

    public Long CountryToLong(Country country) {
        return country.getId();
    }

    @Autowired
    ICountryService iCountryService;

    public Country LongToCountry(Long countryId) {
        Country country = iCountryService.getReference(countryId);

        return country;
    }

    public Long DocumentToLong(Document document) {
        return document.getId();
    }

    public Document LongToDocument(Long documentId) {
        Document document = new Document();
        //TODO: write service to get this.
        document.setId(documentId);
        return document;
    }

    public Long ProcessTemplateToLong(ProcessTemplate processTemplate) {
        return processTemplate.getId();
    }

    public ProcessTemplate LongToProcessTemplate(Long processTemplateId) {
        ProcessTemplate processTemplate = new ProcessTemplate();
        processTemplate.setId(processTemplateId);
        //@TODO: write service to get this.

        return processTemplate;
    }

    public Long ReportFilterToLong(ReportFilter reportFilter) {
        return reportFilter.getId();
    }

    public ReportFilter LongToReportFilter(Long reportFilterId) {
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setId(reportFilterId);
        //@TODO: write service to get this.
        return reportFilter;
    }

    public Long StoreProcedureToLong(StoreProcedure storeProcedure) {
        return storeProcedure.getId();
    }

    public StoreProcedure LongToStoreProcedure(Long storeProcedureId) {
        StoreProcedure storeProcedure = new StoreProcedure();
        storeProcedure.setId(storeProcedureId);
        //@TODO: write service to get this.
        return storeProcedure;
    }

    public Long ViewToLong(View view) {
        return view.getId();
    }

    public View LongToView(Long viewId) {
        View view = new View();
        view.setId(viewId);
        //@TODO: write service to get this.
        return view;
    }

    public Long MessageToLong(Message message) {
        return message.getId();
    }

    public Message LongToMessage(Long messageId) {
        Message message = new Message();
        message.setId(messageId);
        //@TODO: writer service to get this

        return message;
    }

    public Long LongToAlternative(Alternative alternative) {
        return alternative.getId();
    }

    public Alternative LongToAlternative(Long alternativeId) {
        Alternative alternative = new Alternative();
        alternative.setId(alternativeId);
        //TODO: write service to get this.
        return alternative;
    }

    public Long FundSharingToLong(FundSharing fundSharing) {
        return fundSharing.getId();
    }

    public FundSharing LongToFundSharing(Long fundSharingId) {
        FundSharing fundSharing = new FundSharing();
        fundSharing.setId(fundSharingId);
        //TODO: write service to get this.
        return fundSharing;
    }

    public Long PersonToLong(Person person) {
        return person.getId();
    }

    public Person LongToPerson(Long personId) {
        Person person = new Person();
        person.setId(personId);
        //TODO: write service to get this.
        return person;
    }

    public Long DocumentDutyToLong(DocumentDuty documentDuty) {
        return documentDuty.getId();
    }

    public DocumentDuty LongToDocumentDuty(Long DocumentDutyId) {
        DocumentDuty documentDuty = new DocumentDuty();
        documentDuty.setId(DocumentDutyId);
        //TODO: write service to get this
        return documentDuty;

    }


    public Long GroupToLong(Group group) {
        if (group == null) return null;
        return group.getId();
    }

    @Autowired
    IGroupService iGroupService;

    public Group LongToGroup(Long groupId) {
        Group g = iGroupService.getReference(groupId);
        return g;
    }

    public Long TagToLong(Tag tag) {
        return tag.getId();
    }

    public Tag LongToTag(Long tagId) {
        Tag tag = new Tag();
        tag.setId(tagId);
        //@TODO: write service to get this
        return tag;
    }

    public Long ObjectivesToLong(Objective objectives) {

        if (objectives != null)
            return objectives.getId();
        else
            return null;
    }

    @Autowired
    IObjectiveService iObjectiveService;

    public Objective LongToObjectives(Long objectivesId) {
        if (objectivesId != null)
            return iObjectiveService.getReference(objectivesId);
        else
            return null;
    }

    public Long PolicyToLong(Policy policy) {
        return policy.getId();
    }

    public Policy LongToPolicy(Long policyId) {
        Policy policy = new Policy();
        policy.setId(policyId);
        //@TODO: write service to get this
        return policy;
    }

    public Long TodoToLong(Todo todo) {
        if (todo != null)
            return todo.getId();
        else
            return null;
    }

    @Autowired
    ITodoService iTodoService;

    public Todo LongToTodo(Long todoId) {
        if (todoId != null)
            return iTodoService.getReference(todoId);
        else
            return null;
    }

    public Long ActivityPerformanceToLong(ActivityPerformance activityPerformance) {
        return activityPerformance.getId();
    }

    public ActivityPerformance LongToActivityPerformance(Long activityPerformanceId) {
        ActivityPerformance activityPerformance = new ActivityPerformance();
        activityPerformance.setId(activityPerformanceId);
        //@TODO: write service to get this

        return activityPerformance;
    }

    public Long ActivityCostToLong(ActivityCost activityCost) {
        return activityCost.getId();
    }

    public ActivityCost LongToActivityCost(Long activityCostId) {
        ActivityCost activityCost = new ActivityCost();
        activityCost.setId(activityCostId);
        return activityCost;
    }

    public Long ActivityNormToLong(ActivityNorm activityNorm) {
        return activityNorm.getId();
    }

    public ActivityNorm LongToActivityNorm(Long activityNormId) {
        ActivityNorm activityNorm = new ActivityNorm();
        activityNorm.setId(activityNormId);
        return activityNorm;
    }

    public Long PerformanceToLong(Performance performance) {
        return performance.getId();
    }

    public Performance LongToPerformance(Long performanceId) {
        Performance performance = new Performance();
        performance.setId(performanceId);
        return performance;
    }

    public Long StakeholderToLong(Stakeholder stakeholder) {
        if (stakeholder == null) return null;
        return stakeholder.getId();
    }

    @Autowired
    IStakeholderService iStakeholderService;

    public Stakeholder LongToStakeholder(Long stakeholderId) {
        return iStakeholderService.getReference(stakeholderId);
    }


    public Long FilterOperatorToLong(FilterOperator filterOperator) {
        return filterOperator.getId();
    }

    public FilterOperator LongToFilterOperator(Long filterOperatorId) {
        FilterOperator filterOperator = new FilterOperator();
        filterOperator.setId(filterOperatorId);
        //@TODO: write service to get this!
        return filterOperator;
    }

    public Long SigningListToLong(Signing signing) {
        return signing.getId();
    }

    public Signing LongToSigingList(Long signingListId) {
        Signing signing = new Signing();
        signing.setId(signingListId);
        //@TODO: writer service to get this
        return signing;
    }

    public Long SharingToLong(Sharing sharing) {
        return sharing.getId();
    }

    public Sharing LongToSharing(Long sharingId) {
        Sharing sharing = new Sharing();
        sharing.setId(sharingId);
        //@TODO write service to get this

        return sharing;
    }

    public Long BlobToLong(Blob blob) {
        return blob.getId();
    }

    public Blob LongToBlob(Long blobId) {
        Blob blob = new Blob();
        blob.setId(blobId);
        //@TODO: writer service to get this
        return blob;
    }

    public Long ActivityToLong(Activity activity) {

        if (activity != null)
            return activity.getId();
        else
            return null;
    }

    @Autowired
    IActivityService iActivityService;

    public Activity LongToActivity(Long activityId) {
        if (activityId != null)
            return iActivityService.getReference(activityId);
        else
            return null;
    }

    public Long EnumValueToLong(EnumValue enumValue) {
        return enumValue.getId();
    }

    public EnumValue LongToEnumValue(Long enumValueId) {
        EnumValue enumValue = new EnumValue();
        enumValue.setId(enumValueId);
        //TODO: writer service to get this
        return enumValue;
    }

    public Long FundingToLong(Fund funding) {
        return funding.getId();
    }

    public Fund LongToFunding(Long fundingId) {
        Fund funding = new Fund();
        funding.setId(fundingId);
        //@TODO: write service to get this
        return funding;
    }

    public Long FramworkPropertyValueToLong(FrameworkPropertyValue frameWorkPropertyValue) {
        return frameWorkPropertyValue.getId();
    }

    public FrameworkPropertyValue LongToFramWorkPropertyValue(Long frameWorkPropertyValueId) {
        FrameworkPropertyValue frameWorkPropertyValue = new FrameworkPropertyValue();
        frameWorkPropertyValue.setId(frameWorkPropertyValueId);
        //@TODO: writer service to get this
        return frameWorkPropertyValue;
    }

    public Long ContractToLong(Contract contract) {
        return contract.getId();
    }

    public Contract LongToContract(Long contractId) {
        Contract contract = new Contract();
        contract.setId(contractId);
        //@TODO: write service to get this
        return contract;
    }

    public Long BudgetResourceToLong(BudgetResource budgetResource) {
        return budgetResource.getId();
    }

    public BudgetResource LongToBudgeResource(Long budgetResourceId) {
        BudgetResource budgetResource = new BudgetResource();
        budgetResource.setId(budgetResourceId);
        //@TODO:write service to get this!
        return budgetResource;
    }

    public Long BudgetTypeToLong(BudgetType budgetType) {
        return budgetType.getId();
    }

    public BudgetType LongToBudgetType(Long budgetTypeId) {
        BudgetType budgetType = new BudgetType();
        budgetType.setId(budgetTypeId);
        //@TODO: write service to get this
        return budgetType;
    }

    public Long BudgetToLong(Budget budget) {
        return budget.getId();
    }

    public Budget LongToBudget(Long budgetId) {
        Budget budget = new Budget();
        budget.setId(budgetId);
        //@TODO: write service to get this!

        return budget;
    }

    public Long OrganizationUnitToLong(OrganizationUnit organizationUnit) {
        return organizationUnit.getId();
    }

    @Autowired
    IOrganizationUnitService iOrganizationUnitService;

    public OrganizationUnit LongToOrganizationUnit(Long organizatioUnitId) {
        return iOrganizationUnitService.getReference(organizatioUnitId);
    }

    public System LongToSystem(Long systemId) {
        System s = new System();
        s.setId(systemId);
        //@TODO: Write Service to get this!
        return s;
    }

    public Long SystemToLong(System system) {
        return system.getId();
    }

    public Solution LongtoSolution(Long solutionId) {
        Solution s = new Solution();
        s.setId(solutionId);
        //@TODO: Write Service to get this!
        return s;
    }

    public Long SolutionToLong(Solution solution) {
        return solution.getId();
    }

    public Scope LongToScope(Long scopeId) {
        Scope s = new Scope();
        s.setId(scopeId);
        //@TODO: write service to get this!
        return s;
    }

    public Long ScopeToLong(Scope scope) {
        return scope.getId();
    }

    public ActivityTemplate LongToActivityTemplate(Long activityTemplateId) {
        ActivityTemplate t = new ActivityTemplate();
        //@TODO: write service to get this!
        t.setId(activityTemplateId);
        return t;
    }

    public Long ActivityTempalteToLong(ActivityTemplate activityTemplate) {
        return activityTemplate.getId();
    }


    public StereoType longToStereoType(Long stereoType) {
        //@TODO: Write queries to make this right!
        StereoType st = new StereoType();
        st.setId(stereoType);
        return st;
    }

    public Long stereoTypeToLong(StereoType stereoType) {
        return stereoType.getId();
    }


    public GroupTag longToGroupTag(Long groupId) {
        GroupTag gt = new GroupTag();
        gt.setId(groupId);
        return gt;
    }

    public Long groupTagToLong(GroupTag tg) {
        return tg.getId();
    }

    public Application applicationToLong(Long applicationId) {
        Application app = new Application();
        app.setId(applicationId);
        return app;
    }

    public Long longToApplication(Application application) {
        return application.getId();
    }

    public Form formToLong(Long formId) {
        Form form = new Form();
        form.setId(formId);
        return form;
    }

    public Long longToForm(Form form) {
        return form.getId();
    }

    public Menu menu(Long menuId) {
        Menu menu = new Menu();
        menu.setId(menuId);
        return menu;
    }

    public Long longToMenu(Menu menu) {
        return menu.getId();
    }

    public Page pageToLong(Long pageId) {
        Page page = new Page();
        page.setId(pageId);
        return page;
    }

    public Long longToPage(Page page) {
        return page.getId();
    }

    public AppEnum appEnumToLong(Long appEnumId) {
        AppEnum appEnum = new AppEnum();
        appEnum.setId(appEnumId);
        return appEnum;
    }

    public Long longToAppEnum(AppEnum appEnum) {
        return appEnum.getId();
    }

    public AppEntity appEntityToLong(Long appEntityId) {
        AppEntity appEntity = new AppEntity();
        appEntity.setId(appEntityId);
        return appEntity;
    }

    public Long longToAppEntity(AppEntity appEntity) {
        return appEntity.getId();
    }

    public Topic topicToLong(Long topicId) {
        Topic topic = new Topic();
        topic.setId(topicId);
        return topic;
    }

    public Long longToTopic(Topic topic) {
        return topic.getId();
    }

    @Autowired
    IGoalService iGoalService;

    public Goal goalToLong(Long goalId) {
        if (goalId != null)
            return iGoalService.getReference(goalId);
        else
            return null;
    }

    public Long longToGoal(Goal goal) {
        if (goal != null)
            return goal.getId();
        else
            return null;
    }

    @Autowired
    IStrategyService iStrategyService;

    public Strategy longToStrategy(Long strategyId) {
        if (strategyId != null)
            return iStrategyService.getReference(strategyId);
        else
            return null;
    }


    public Long strategyToLong(Strategy strategy) {
        if (strategy != null)
            return strategy.getId();
        else
            return null;
    }

    @Autowired
    IKpiService iKpiService;

    public Kpi kpiToLong(Long kpiId) {
        return iKpiService.getReference(kpiId);
    }

    public Long longToKpi(Kpi kpi) {
        return kpi.getId();
    }

    @Autowired
    INormCostService iNormCostService;

    public NormCost normCostToLong(Long normCostId) {
        return iNormCostService.getReference(normCostId);
    }

    public Long longToNormCost(NormCost normCost) {
        return normCost.getId();
    }

    @Autowired
    IOrganizationService iOrganizationService;

    public Organization organizationToLong(Long organizationId) {
        return iOrganizationService.getReference(organizationId);
    }

    public Long longToOrganization(Organization organization) {
        return organization.getId();
    }

    @Autowired
    IRiskService iRiskService;

    public Risk riskToLong(Long riskId) {
        return iRiskService.getReference(riskId);
    }

    public Long longTorisk(Risk risk) {
        return risk.getId();
    }

    public Long longToPermission(Permission permission) {
        return permission.getId();
    }

    @Autowired
    IPermissionService iPermissionService;

    public Permission permissionToLong(Long permissionId) {
        return iPermissionService.getReference(permissionId);
    }
}
