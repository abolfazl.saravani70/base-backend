/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.SysPolicy;
import com.rbp.sayban.model.dto.system.SysPolicyDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface SysPolicyMapper extends GenericMapper<SysPolicyDTO, SysPolicy> {

    SysPolicyMapper INSTANCE = Mappers.getMapper(SysPolicyMapper.class);

    @Mapping(source = "enumValue", target = "enumValueId")
    @Mapping(source = "application", target = "applicationId")
    SysPolicyDTO toView(SysPolicy SysPolicy);

    @Mapping(source = "enumValueId", target = "enumValue")
    @Mapping(source = "applicationId", target = "application")
    SysPolicy toDomainModel(SysPolicyDTO SysPolicyDTO);

}