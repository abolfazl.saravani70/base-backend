package com.rbp.sayban.model.mapper.subjective;


import com.rbp.sayban.model.domainmodel.subjective.Subjective;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface SubjectiveMapper extends GenericMapper<SubjectiveDTO, Subjective> {

    SubjectiveMapper INSTANCE= Mappers.getMapper(SubjectiveMapper.class);


    SubjectiveDTO toView(Subjective subjective);

    Subjective toDomainModel(SubjectiveDTO subjectiveDTO);

}