package com.rbp.sayban.model.mapper.task;


import com.rbp.sayban.model.domainmodel.task.TaskSubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.task.TaskSubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskSubjectiveMapper extends GenericMapper<TaskSubjectiveViewModel, TaskSubjective> {

    TaskSubjectiveMapper INSTANCE= Mappers.getMapper(TaskSubjectiveMapper.class);


    TaskSubjectiveViewModel toView(TaskSubjective taskSubjective);
    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "subjective.id", target = "subjective")
    TaskSubjective toDomainModel(TaskSubjectiveViewModel taskSubjectiveViewModel);

}