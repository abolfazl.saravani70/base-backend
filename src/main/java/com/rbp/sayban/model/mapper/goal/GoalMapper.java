/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.goal;

import com.rbp.sayban.model.domainmodel.goal.Goal;
import com.rbp.sayban.model.dto.goal.GoalDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalMapper extends GenericMapper<GoalDTO, Goal> {

    GoalMapper INSTANCE = Mappers.getMapper(GoalMapper.class);

    GoalDTO toView(Goal goal);

    Goal toDomainModel(GoalDTO goalDTO);

}
