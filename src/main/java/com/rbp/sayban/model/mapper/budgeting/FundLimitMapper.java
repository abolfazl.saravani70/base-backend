/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.FundLimit;
import com.rbp.sayban.model.dto.budgeting.FundLimitDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface FundLimitMapper extends GenericMapper<FundLimitDTO, FundLimit> {
    FundLimitMapper INSTANCE = Mappers.getMapper(FundLimitMapper.class);

    @Mapping(source = "fund", target = "fundId")
    FundLimitDTO toView(FundLimit fundLimit);

    @Mapping(source = "fundId", target = "fund")
    FundLimit toDomainModel(FundLimitDTO fundLimitDTO);
}
