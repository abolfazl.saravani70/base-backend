package com.rbp.sayban.model.mapper.project;


import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.domainmodel.project.ProjectOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.project.ProjectOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectOrganizationUnitMapper extends GenericMapper<ProjectOrganizationUnitViewModel, ProjectOrganizationUnit> {
    ProjectOrganizationUnitMapper INSTANCE = Mappers.getMapper(ProjectOrganizationUnitMapper.class);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    ProjectOrganizationUnit toDomainModel(ProjectOrganizationUnitViewModel projectOrganizationUnitViewModel);


    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    ProjectOrganizationUnitViewModel toView(ProjectOrganizationUnit projectOrganizationUnit);
}
