package com.rbp.sayban.model.mapper.budgeting;


import com.rbp.sayban.model.domainmodel.budgeting.BudgetTree;
import com.rbp.sayban.model.dto.budgeting.BudgetTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface BudgetTreeMapper extends GenericMapper<BudgetTreeDTO, BudgetTree> {

    BudgetTreeMapper INSTANCE= Mappers.getMapper(BudgetTreeMapper.class);


    BudgetTreeDTO toView(BudgetTree budgetTree);

    BudgetTree toDomainModel(BudgetTreeDTO budgetTreeDTO);

}