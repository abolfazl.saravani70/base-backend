package com.rbp.sayban.model.mapper.plan;


import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.domainmodel.planning.PlanOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.planning.PlanOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanOrganizationUnitMapper extends GenericMapper<PlanOrganizationUnitViewModel, PlanOrganizationUnit> {
    PlanOrganizationUnitMapper INSTANCE = Mappers.getMapper(PlanOrganizationUnitMapper.class);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    PlanOrganizationUnit toDomainModel(PlanOrganizationUnitViewModel planOrganizationUnitViewModel);


    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    PlanOrganizationUnitViewModel toView(PlanOrganizationUnit planOrganizationUnit);
}
