package com.rbp.sayban.model.mapper.plan;

import com.rbp.sayban.model.domainmodel.planning.PlanLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.planning.PlanLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanLocationMapper extends GenericMapper<PlanLocationViewModel, PlanLocation> {
    PlanLocationMapper INSTANCE = Mappers.getMapper(PlanLocationMapper.class);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "location.id", target = "location")
    PlanLocation toDomainModel(PlanLocationViewModel planLocationViewModel);
}
