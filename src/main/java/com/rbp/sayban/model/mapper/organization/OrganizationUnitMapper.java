/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import com.rbp.sayban.model.dto.organization.OrganizationUnitDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.organization.OrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
@Mapper(config = BaseEntityMapper.class,uses = BaseMapperMethods.class)
public interface OrganizationUnitMapper extends GenericMapper<OrganizationUnitDTO, OrganizationUnit> {
    OrganizationUnitMapper INSTANCE= Mappers.getMapper(OrganizationUnitMapper.class);

//    @Mapping(source = "organizationChart.id",target = "organizationChartId")
//    @Mapping(source = "organizationChart.name",target = "orgChartName")
//    @Mapping(source = "organizationChart.isVirtual",target = "isVirtual")
//    @Mapping(source = "organizationChart.pvOrgLevelId",target = "pvOrgLevelId")
//    @Mapping(source = "organizationChart.pvOrgDegreeId",target = "pvOrgDegreeId")
//    @Mapping(source = "organizationChart.pvPositionNumberId",target = "pvPositionNumberId")
//    @Mapping(source = "organization.id",target = "organizationId")
//    @Mapping(source = "organization.name",target = "orgName")
//    @Mapping(source = "organization.pvOrgTypeId",target = "pvOrgTypeId")
//    @Mapping(source = "organizationChart.parent.id",target = "orgChartParentId")
//    @Mapping(source = "organizationChart.parent.name",target = "orgChartParentName")

//    @Mapping(source = "organization.pvOrgLevelId",target = "organization.pvOrgLevelTitle")
//    @Mapping(source = "organization.pvOrgTypeId",target = "organization.pvOrgTypeTitle")
//    @Mapping(source = "organization.pvOrgDegreeId",target = "organization.pvOrgDegreeTitle")
//    @Mapping(source = "organizationChart.pvOrgDegreeId",target = "organizationChart.pvOrgDegreeTitle")
//    @Mapping(source = "organizationChart.pvOrgLevelId",target = "organizationChart.pvOrgLevelTitle")
//    @Mapping(source = "organizationChart.pvTypeId",target = "organizationChart.pvTypeTitle")
//    @Mapping(source = "organizationChart.pvPositionTitleId",target = "organizationChart.pvPositionTitleTitle")
//    @Mapping(source = "organizationChart.pvPositionNumberId",target = "organizationChart.pvPositionNumberTitle")
//    @Mapping(source = "organizationChart.pvOrganizeTypeId",target = "organizationChart.pvOrganizeTypeTitle")
//    @Mapping(source = "organizationChart.parent.id",target = "organizationChart.parentId")
    OrganizationUnitDTO toView(OrganizationUnit organizationUnit);
}
