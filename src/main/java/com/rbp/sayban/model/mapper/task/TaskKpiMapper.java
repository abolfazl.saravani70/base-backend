package com.rbp.sayban.model.mapper.task;


import com.rbp.sayban.model.domainmodel.task.TaskKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.task.TaskKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskKpiMapper extends GenericMapper<TaskKpiViewModel, TaskKpi> {
    TaskKpiMapper INSTANCE = Mappers.getMapper(TaskKpiMapper.class);

    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "kpi.id", target = "kpi")
    TaskKpi toDomainModel(TaskKpiViewModel taskKpiViewModel);
}
