package com.rbp.sayban.model.mapper.task;

import com.rbp.sayban.model.domainmodel.task.TaskLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.task.TaskLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskLocationMapper extends GenericMapper<TaskLocationViewModel, TaskLocation> {

    TaskLocationMapper INSTANCE = Mappers.getMapper(TaskLocationMapper.class);

    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "location.id", target = "location")
    TaskLocation toDomainModel(TaskLocationViewModel taskLocationViewModel);
}
