/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Phone;
import com.rbp.sayban.model.dto.organization.AddressDTO;
import com.rbp.sayban.model.dto.organization.PhoneDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Set;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PhoneMapper extends GenericMapper<PhoneDTO, Phone> {
    PhoneMapper INSTANCE = Mappers.getMapper(PhoneMapper.class);

    PhoneDTO toView(Phone phone);

    Phone toDomainModel(PhoneDTO phoneDTO);

    Set<Phone> setLongToPhone(Set<Long> longs);
    Set<Long> setPhoneToLong(Set<Phone> addresses);
}
