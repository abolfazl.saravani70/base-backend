/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper;

import java.io.IOException;
import java.util.List;

/**
 * @param <S> View(Dtos for basic objects) ->Anything that extends BaseEntityViewModel
 * @param <T> Entity/domainmodel ->Anything that extends BaseEntity
 *            this interface has function to do two sided MAPPING!
 *            So don't worry which Target/Source you should set.
 *            just set Source to
 */
public interface GenericMapper<S, T> {

    /**
     * Converts given model to dto.
     */
    T toDomainModel(S source);

    /**
     * Converts given dto to model.
     */
    S toView(T target);


    /**
     * Converts given model list to dto list.
     */
    List<T> toDomainModel(List<S> sourceList);

    /**
     * Converts given dto list to model list.
     */
    List<S> toView(List<T> targetList);


}