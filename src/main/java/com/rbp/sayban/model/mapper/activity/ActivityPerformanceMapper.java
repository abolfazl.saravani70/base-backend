/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActivityPerformance;
import com.rbp.sayban.model.dto.activity.ActivityPerformanceDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityPerformanceMapper extends GenericMapper<ActivityPerformanceDTO, ActivityPerformance> {
    ActivityPerformanceMapper INSTANCE = Mappers.getMapper(ActivityPerformanceMapper.class);

    @Mapping(source = "activity", target = "activityId")
    @Mapping(source = "performance", target = "performanceId")
    ActivityPerformanceDTO toView(ActivityPerformance activityPerformance);

    @Mapping(source = "activityId", target = "activity")
    @Mapping(source = "performanceId", target = "performance")
    ActivityPerformance toDomainModel(ActivityPerformanceDTO activityPerformanceDTO);

}
