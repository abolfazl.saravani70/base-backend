package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.norm.NormCost;
import com.rbp.sayban.model.dto.activity.NormCostDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface NormCostMapper extends GenericMapper<NormCostDTO, NormCost> {
    NormCostMapper INSTANCE= Mappers.getMapper(NormCostMapper.class);

    NormCostDTO toView(NormCost normCost);

    NormCost toDomainModel(NormCostDTO normCostDTO);
}
