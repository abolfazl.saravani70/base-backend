/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;

import com.rbp.sayban.model.domainmodel.application.AppMessageType;
import com.rbp.sayban.model.dto.application.AppMessageTypeDTO;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.factory.Mappers;

public interface AppMessageTypeMapper extends GenericMapper<AppMessageTypeDTO, AppMessageType> {
    AppMessageTypeMapper INSTANCE = Mappers.getMapper(AppMessageTypeMapper.class);
}
