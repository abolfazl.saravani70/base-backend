package com.rbp.sayban.model.mapper.project;


import com.rbp.sayban.model.domainmodel.project.ProjectSubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.project.ProjectSubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectSubjectiveMapper extends GenericMapper<ProjectSubjectiveViewModel, ProjectSubjective> {

    ProjectSubjectiveMapper INSTANCE = Mappers.getMapper(ProjectSubjectiveMapper.class);


    ProjectSubjectiveViewModel toView(ProjectSubjective projectSubjective);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "subjective.id", target = "subjective")
    ProjectSubjective toDomainModel(ProjectSubjectiveViewModel projectSubjectiveViewModel);

}