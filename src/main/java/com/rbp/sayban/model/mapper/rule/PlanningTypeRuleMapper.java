/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.rule;

import com.rbp.sayban.model.domainmodel.rule.PlanningTypeRule;
import com.rbp.sayban.model.dto.rule.PlanningTypeRuleDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanningTypeRuleMapper extends GenericMapper<PlanningTypeRuleDTO, PlanningTypeRule> {
    PlanningTypeRuleMapper INSTANCE = Mappers.getMapper(PlanningTypeRuleMapper.class);

    PlanningTypeRuleDTO toView(PlanningTypeRule planningTypeRule);

    PlanningTypeRule toDomainModel(PlanningTypeRuleDTO planTypeRuleDTO);
}
