package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.OrganizationAccount;

import com.rbp.sayban.model.dto.organization.OrganizationAccountDTO;

import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface OrganizationAccountMapper extends GenericMapper<OrganizationAccountDTO, OrganizationAccount> {
    OrganizationAccountMapper INSTANCE = Mappers.getMapper(OrganizationAccountMapper.class);
}
