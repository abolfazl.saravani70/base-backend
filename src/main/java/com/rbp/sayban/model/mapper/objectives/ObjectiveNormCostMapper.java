package com.rbp.sayban.model.mapper.objectives;


import com.rbp.sayban.model.domainmodel.objectives.ObjectiveNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveNormCostMapper extends GenericMapper<ObjectiveNormCostViewModel, ObjectiveNormCost> {
    ObjectiveNormCostMapper INSTANCE = Mappers.getMapper(ObjectiveNormCostMapper.class);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "normCost.id", target = "normCost")
    ObjectiveNormCost toDomainModel(ObjectiveNormCostViewModel objectiveNormCostViewModel);
}
