package com.rbp.sayban.model.mapper.strategies;


import com.rbp.sayban.model.domainmodel.strategies.StrategySubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.strategies.StrategySubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategySubjectiveMapper extends GenericMapper<StrategySubjectiveViewModel, StrategySubjective> {

    StrategySubjectiveMapper INSTANCE= Mappers.getMapper(StrategySubjectiveMapper.class);


    StrategySubjectiveViewModel toView(StrategySubjective strategySubjective);
    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "subjective.id", target = "subjective")
    StrategySubjective toDomainModel(StrategySubjectiveViewModel strategySubjectiveViewModel);

}