/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.plan;

import com.rbp.sayban.model.domainmodel.planning.PlanTree;
import com.rbp.sayban.model.dto.planning.PlanTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.planning.IPlanService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public  abstract class PlanTreeMapper implements GenericMapper<PlanTreeDTO, PlanTree> {
    @Autowired
    IPlanService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract PlanTreeDTO toView(PlanTree planTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract PlanTree toDomainModel(PlanTreeDTO planTreeDTO);

    @BeforeMapping
    void beforeMapping(@MappingTarget PlanTree pl, PlanTreeDTO dto){
        if(dto.getChild().getId() != null)
            pl.setChild(service.getReference(dto.getChild().getId()));
    }
}
