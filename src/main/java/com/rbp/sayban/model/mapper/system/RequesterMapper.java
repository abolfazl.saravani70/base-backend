/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.Requester;
import com.rbp.sayban.model.dto.system.RequesterDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface RequesterMapper extends GenericMapper<RequesterDTO, Requester> {

    RequesterMapper INSTANCE = Mappers.getMapper(RequesterMapper.class);

    @Mapping(source = "person", target = "personId")
    RequesterDTO toView(Requester Requester);

    @Mapping(source = "personId", target = "person")
    Requester toDomainModel(RequesterDTO RequesterDTO);

}