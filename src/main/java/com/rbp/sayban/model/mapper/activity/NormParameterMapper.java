package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.norm.NormParameter;
import com.rbp.sayban.model.dto.activity.NormParameterDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface NormParameterMapper extends GenericMapper<NormParameterDTO, NormParameter> {
    NormParameterMapper INSTANCE= Mappers.getMapper(NormParameterMapper.class);

    @Mapping(source = "normCost",target = "normCostId")
    NormParameterDTO toView(NormParameter normParameter);

    @Mapping(source = "normCostId",target = "normCost")
    NormParameter toDomainModel(NormParameterDTO normParameterDTO);


}
