/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.objectives;

import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveMapper extends GenericMapper<ObjectiveDTO, Objective> {
    ObjectiveMapper INSTANCE = Mappers.getMapper(ObjectiveMapper.class);

    ObjectiveDTO toView(Objective objective);

    Objective toDomainModel(ObjectiveDTO objectiveDTO);

}
