package com.rbp.sayban.model.mapper.goal;


import com.rbp.sayban.model.domainmodel.goal.GoalSubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.goal.GoalSubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalSubjectiveMapper extends GenericMapper<GoalSubjectiveViewModel, GoalSubjective> {

    GoalSubjectiveMapper INSTANCE= Mappers.getMapper(GoalSubjectiveMapper.class);


    GoalSubjectiveViewModel toView(GoalSubjective goalSubjective);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "subjective.id", target = "subjective")
    GoalSubjective toDomainModel(GoalSubjectiveViewModel goalSubjectiveViewModel);

}