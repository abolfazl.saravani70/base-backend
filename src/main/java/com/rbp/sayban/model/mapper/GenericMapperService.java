/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
public class GenericMapperService {

    /**
     * Holds GenericMapper information with source and target types as keys.
     * Source=ViewModel
     * Target=DomainModel
     */
    private Map<String, GenericMapper> mapperInfos = new HashMap<>();


    @Autowired
    private ApplicationContext appContext;


    @PostConstruct
    private void init() {

        Collection<GenericMapper> foundMappers = findGenericMappers();


        for (GenericMapper foundMapper : foundMappers) {

            // source type of the mapper..
            String sourceType = getMapperSourceType(foundMapper);

            // target type of the mapper..
            String targetType = getMapperTargetType(foundMapper);

            // initialize mapper infos..
            mapperInfos.put(sourceType + "-" + targetType, foundMapper);
            // mapperInfos.put( targetType + "-" + sourceType, foundMapper );

        }

    }

    public GenericMapper getMapperByEntityClass(String className){
        String finalName=className+"DTO-"+className;
        return mapperInfos.get(finalName);
    }
    /**
     * Searches for the mappers in Spring context that is inherited from
     * GenericMapper interface.
     */
    private Collection<GenericMapper> findGenericMappers() {
        return appContext.getBeansOfType(GenericMapper.class).values();
    }


    /**
     * Returns the source type of GenericMapper.
     */
    private String getMapperSourceType(GenericMapper mapper) {
        //Resolve type argument checks an implmentation of an interface and finds out what type
        //of classes as been passed instead of generic classes.
        Class<?>[] mapperTypeInfos =
                GenericTypeResolver.resolveTypeArguments(
                        mapper.getClass(),
                        GenericMapper.class
                );

        return mapperTypeInfos[0].getSimpleName();
    }


    /**
     * Returns the target type of GenericMapper.
     */
    private String getMapperTargetType(GenericMapper mapper) {

        Class<?>[] mapperTypeInfos =
                GenericTypeResolver.resolveTypeArguments(
                        mapper.getClass(),
                        GenericMapper.class
                );


        return mapperTypeInfos[1].getSimpleName();
    }


    /**
     * Returns the GenericMapper for the given source and the target type.
     * S=ViewModel
     * T=DTO
     */
    public <S, T> GenericMapper<S, T> getMapper(Class<S> sourceType, Class<T> targetType) {

        String mapperKey = sourceType.getSimpleName() + "-" + targetType.getSimpleName();
        return mapperInfos.get(mapperKey);
    }


}