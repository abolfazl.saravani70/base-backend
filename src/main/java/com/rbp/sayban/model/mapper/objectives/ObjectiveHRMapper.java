package com.rbp.sayban.model.mapper.objectives;

import com.rbp.sayban.model.domainmodel.objectives.ObjectiveHR;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveHRViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveHRMapper extends GenericMapper<ObjectiveHRViewModel, ObjectiveHR> {
    ObjectiveHRMapper INSTANCE = Mappers.getMapper(ObjectiveHRMapper.class);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "humanResource.id", target = "humanResource")
    ObjectiveHR toDomainModel(ObjectiveHRViewModel objectiveHRViewModel);
}
