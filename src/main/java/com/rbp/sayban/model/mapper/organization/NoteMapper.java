/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Note;
import com.rbp.sayban.model.dto.organization.NoteDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface NoteMapper extends GenericMapper<NoteDTO, Note> {
    NoteMapper INSTANCE = Mappers.getMapper(NoteMapper.class);

    @Mapping(source = "user", target = "userId")
    NoteDTO toView(Note note);

//    @Mapping(source = "userId", target = "user")
//    SysRule toDomainModel(NoteDTO noteDTO);
}
