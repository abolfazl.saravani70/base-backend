package com.rbp.sayban.model.mapper.project;


import com.rbp.sayban.model.domainmodel.project.ProjectNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.project.ProjectNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectNormCostMapper extends GenericMapper<ProjectNormCostViewModel, ProjectNormCost> {
    ProjectNormCostMapper INSTANCE = Mappers.getMapper(ProjectNormCostMapper.class);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "normCost.id", target = "normCost")
    ProjectNormCost toDomainModel(ProjectNormCostViewModel projectNormCostViewModel);
}
