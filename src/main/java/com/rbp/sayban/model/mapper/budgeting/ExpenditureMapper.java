/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.Expenditure;
import com.rbp.sayban.model.dto.budgeting.ExpenditureDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ExpenditureMapper extends GenericMapper<ExpenditureDTO, Expenditure> {
    ExpenditureMapper INSTANCE = Mappers.getMapper(ExpenditureMapper.class);

//    @Mapping(source = "activityPerformance", target = "activityPerformanceId")
    @Mapping(source = "task", target = "taskId")
    @Mapping(source = "todo", target = "todoId")
    ExpenditureDTO toView(Expenditure expenditure);

//    @Mapping(source = "activityPerformanceId", target = "activityPerformance")
    @Mapping(source = "taskId", target = "task")
    @Mapping(source = "todoId", target = "todo")
    Expenditure toDomainModel(ExpenditureDTO expenditureDTO);
}
