package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.Performance;
import com.rbp.sayban.model.dto.activity.PerformanceDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PerformanceMapper extends GenericMapper<PerformanceDTO, Performance> {
    PerformanceMapper INSTANCE= Mappers.getMapper(PerformanceMapper.class);
}
