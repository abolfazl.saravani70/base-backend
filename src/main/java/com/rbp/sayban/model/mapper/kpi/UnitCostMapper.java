/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.kpi;

import com.rbp.sayban.model.domainmodel.kpi.UnitCost;
import com.rbp.sayban.model.dto.kpi.UnitCostDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface UnitCostMapper extends GenericMapper<UnitCostDTO, UnitCost> {
    UnitCostMapper INSTANCE = Mappers.getMapper(UnitCostMapper.class);

    @Mapping(source = "unit", target = "unitId")
    UnitCostDTO toView(UnitCost unitCost);

    @Mapping(source = "unitId", target = "unit")
    UnitCost toDomainModel(UnitCostDTO unitCostDTO);
}
