package com.rbp.sayban.model.mapper.plan;

import com.rbp.sayban.model.domainmodel.planning.PlanHR;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.planning.PlanHRViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanHRMapper extends GenericMapper<PlanHRViewModel, PlanHR> {

    PlanHRMapper INSTANCE = Mappers.getMapper(PlanHRMapper.class);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "humanResource.id", target = "humanResource")
    PlanHR toDomainModel(PlanHRViewModel planHRViewModel);
}
