/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActivityCost;
import com.rbp.sayban.model.dto.activity.ActivityCostDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityCostMapper extends GenericMapper<ActivityCostDTO, ActivityCost> {
    ActivityCostMapper INSTANCE= Mappers.getMapper(ActivityCostMapper.class);

    @Mapping(source = "activity", target = "activityId")
    ActivityCostDTO toView(ActivityCost activityCosts);

    @Mapping(source = "activityId", target = "activity")
    ActivityCost toDomainModel(ActivityCostDTO activityCostsDTO);
}
