/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.Document;
import com.rbp.sayban.model.dto.system.DocumentDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface DocumentMapper extends GenericMapper<DocumentDTO, Document> {

    DocumentMapper INSTANCE = Mappers.getMapper(DocumentMapper.class);


//    @Mapping(source = "request", target = "requestId")
//    @Mapping(source = "documentDuty", target = "documentDutyId")
    DocumentDTO toView(Document Document);

   //    @Mapping(source = "requestId", target = "request")
//    @Mapping(source = "documentDutyId", target = "documentDuty")
    Document toDomainModel(DocumentDTO DocumentDTO);

}
