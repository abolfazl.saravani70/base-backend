/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.task;

import com.rbp.sayban.model.domainmodel.task.Task;
import com.rbp.sayban.model.dto.task.TaskDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskMapper extends GenericMapper<TaskDTO, Task> {
    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    TaskDTO toView(Task task);

    Task toDomainModel(TaskDTO taskDTO);
}
