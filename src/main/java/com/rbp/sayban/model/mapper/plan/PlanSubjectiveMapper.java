package com.rbp.sayban.model.mapper.plan;


import com.rbp.sayban.model.domainmodel.planning.PlanSubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.planning.PlanSubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanSubjectiveMapper extends GenericMapper<PlanSubjectiveViewModel, PlanSubjective> {

    PlanSubjectiveMapper INSTANCE = Mappers.getMapper(PlanSubjectiveMapper.class);


    PlanSubjectiveViewModel toView(PlanSubjective planSubjective);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "subjective.id", target = "subjective")
    PlanSubjective toDomainModel(PlanSubjectiveViewModel planSubjectiveViewModel);

}