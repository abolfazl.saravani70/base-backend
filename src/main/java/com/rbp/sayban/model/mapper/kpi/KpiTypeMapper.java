/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.kpi;

import com.rbp.sayban.model.domainmodel.kpi.KpiType;
import com.rbp.sayban.model.dto.kpi.KpiTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface KpiTypeMapper extends GenericMapper<KpiTypeDTO, KpiType> {
    KpiTypeMapper INSTANCE = Mappers.getMapper(KpiTypeMapper.class);
}
