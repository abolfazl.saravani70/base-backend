/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.GroupScheduling;
import com.rbp.sayban.model.dto.organization.GroupSchedulingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GroupSchedulingMapper extends GenericMapper<GroupSchedulingDTO, GroupScheduling> {
    GroupSchedulingMapper INSTANCE = Mappers.getMapper(GroupSchedulingMapper.class);

    @Mapping(source = "group", target = "groupId")
    GroupSchedulingDTO toView(GroupScheduling groupScheduling);

//    @Mapping(source = "groupId", target = "group")
//    SysRule toDomainModel(GroupSchedulingDTO groupSchedulingDTO);
}
