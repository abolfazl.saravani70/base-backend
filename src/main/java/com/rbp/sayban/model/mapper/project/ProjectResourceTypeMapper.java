package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.ProjectResourceType;
import com.rbp.sayban.model.dto.project.ProjectResourceTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectResourceTypeMapper extends GenericMapper<ProjectResourceTypeDTO, ProjectResourceType> {
    ProjectResourceTypeMapper INSTANCE= Mappers.getMapper(ProjectResourceTypeMapper.class);
}
