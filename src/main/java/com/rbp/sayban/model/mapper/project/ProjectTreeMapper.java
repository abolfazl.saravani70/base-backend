/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.ProjectTree;
import com.rbp.sayban.model.dto.project.ProjectTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.project.IProjectService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class ProjectTreeMapper implements GenericMapper<ProjectTreeDTO, ProjectTree> {
    @Autowired
    IProjectService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract ProjectTreeDTO toView(ProjectTree projectTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract ProjectTree toDomainModel(ProjectTreeDTO projectTreeDTO);

    @BeforeMapping
    void beforeMapping(@MappingTarget ProjectTree pr, ProjectTreeDTO dto){
        if(dto.getChild().getId() != null)
            pr.setChild(service.getReference(dto.getChild().getId()));
    }
}
