package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.PaidCredit;
import com.rbp.sayban.model.dto.activity.PaidCreditDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PaidCreditMapper extends GenericMapper<PaidCreditDTO, PaidCredit> {
    PaidCreditMapper INSTANCE= Mappers.getMapper(PaidCreditMapper.class);

    @Mapping(source = "activity",target = "activityId")
    PaidCreditDTO toView(PaidCredit paidCredit);

    @Mapping(source = "activityId",target = "activity")
    PaidCredit toDomainModel(PaidCreditDTO paidCreditDTO);
}
