/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.BudgetType;
import com.rbp.sayban.model.dto.budgeting.BudgetTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface BudgetTypeMappr extends GenericMapper<BudgetTypeDTO, BudgetType> {
    BudgetTypeMappr INSTANCE = Mappers.getMapper(BudgetTypeMappr.class);

}
