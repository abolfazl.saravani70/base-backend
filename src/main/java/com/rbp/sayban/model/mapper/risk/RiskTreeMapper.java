package com.rbp.sayban.model.mapper.risk;


import com.rbp.sayban.model.domainmodel.risk.RiskTree;
import com.rbp.sayban.model.dto.risk.RiskTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface RiskTreeMapper extends GenericMapper<RiskTreeDTO, RiskTree> {

    RiskTreeMapper INSTANCE= Mappers.getMapper(RiskTreeMapper.class);


    RiskTreeDTO toView(RiskTree riskTree);

    RiskTree toDomainModel(RiskTreeDTO riskTreeDTO);

}