package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityKpiMapper extends GenericMapper<ActivityKpiViewModel, ActivityKpi> {

    ActivityKpiMapper INSTANCE= Mappers.getMapper(ActivityKpiMapper.class);

    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "kpi.id", target = "kpi")
    ActivityKpi toDomainModel(ActivityKpiViewModel activityKpiViewModel);
}
