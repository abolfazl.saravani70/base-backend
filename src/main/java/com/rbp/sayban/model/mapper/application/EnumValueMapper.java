/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;

import com.rbp.sayban.model.domainmodel.application.EnumValue;
import com.rbp.sayban.model.dto.application.EnumValueDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface EnumValueMapper extends GenericMapper<EnumValueDTO, EnumValue> {
    EnumValueMapper INSTANCE = Mappers.getMapper( EnumValueMapper.class);

    @Mapping(source = "appEnum",target = "appEnumId")
    EnumValueDTO toView(EnumValue enumValue);

    @Mapping(source = "appEnumId",target = "appEnum")
    EnumValue toDomainModel(EnumValueDTO enumValueDTO);

}
