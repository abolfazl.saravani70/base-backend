/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;


import com.rbp.sayban.model.domainmodel.application.SignBoard;
import com.rbp.sayban.model.dto.application.SignBoardDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface SignBoardMapper extends GenericMapper<SignBoardDTO, SignBoard> {
    SignBoardMapper INSTANCE = Mappers.getMapper(SignBoardMapper.class);

    @Mapping(source = "application",target = "applicationId")
    SignBoardDTO toView(SignBoard signBoard);

    @Mapping(source = "applicationId",target = "application")
    SignBoard toDomainModel(SignBoardDTO signBoardDTO);


}
