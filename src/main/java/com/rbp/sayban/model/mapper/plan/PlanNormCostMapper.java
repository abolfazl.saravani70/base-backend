package com.rbp.sayban.model.mapper.plan;


import com.rbp.sayban.model.domainmodel.planning.PlanNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.planning.PlanNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanNormCostMapper extends GenericMapper<PlanNormCostViewModel, PlanNormCost> {
    PlanNormCostMapper INSTANCE = Mappers.getMapper(PlanNormCostMapper.class);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "normCost.id", target = "normCost")
    PlanNormCost toDomainModel(PlanNormCostViewModel planNormCostViewModel);
}
