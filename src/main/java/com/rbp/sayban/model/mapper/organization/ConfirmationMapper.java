/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Confirmation;
import com.rbp.sayban.model.dto.organization.ConfirmationDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ConfirmationMapper extends GenericMapper<ConfirmationDTO, Confirmation> {
    ConfirmationMapper INSTANCE = Mappers.getMapper(ConfirmationMapper.class);

    @Mapping(source = "application", target = "applicationId")
    @Mapping(source = "appEntity", target = "appEntityId")
    @Mapping(source = "user", target = "userId")
    ConfirmationDTO toView(Confirmation confirmation);

//    @Mapping(source = "applicationId", target = "application")
//    @Mapping(source = "appEntityId", target = "appEntity")
//    @Mapping(source = "userId", target = "user")
//    SysRule toDomainModel(ConfirmationDTO confirmationDTO);
}
