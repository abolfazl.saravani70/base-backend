/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.Credit;
import com.rbp.sayban.model.dto.budgeting.CreditDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface CreditMapper extends GenericMapper<CreditDTO, Credit> {
    CreditMapper INSTANCE = Mappers.getMapper(CreditMapper.class);

    @Mapping(source = "fundSharing", target = "fundSharingId")
    @Mapping(source = "allocation", target = "allocationId")
    CreditDTO toView(Credit credit);

    @Mapping(source = "fundSharingId", target = "fundSharing")
    @Mapping(source = "allocationId", target = "allocation")
    Credit toDomainModel(CreditDTO creditDTO);
}
