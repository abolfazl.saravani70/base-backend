package com.rbp.sayban.model.mapper.project;


import com.rbp.sayban.model.domainmodel.project.ProjectRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.project.ProjectRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectRiskMapper extends GenericMapper<ProjectRiskViewModel, ProjectRisk> {
    ProjectRiskMapper INSTANCE = Mappers.getMapper(ProjectRiskMapper.class);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "risk.id", target = "risk")
    ProjectRisk toDomainModel(ProjectRiskViewModel projectRiskViewModel);
}
