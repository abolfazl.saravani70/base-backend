/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.goal;

import com.rbp.sayban.model.domainmodel.goal.GoalTree;
import com.rbp.sayban.model.dto.goal.GoalTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.goal.IGoalService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class GoalTreeMapper implements GenericMapper<GoalTreeDTO, GoalTree> {

    @Autowired
    IGoalService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract GoalTreeDTO toView(GoalTree goalTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract GoalTree toDomainModel(GoalTreeDTO goalTreeDTO);

    @BeforeMapping
    void beforeMapping(@MappingTarget GoalTree go, GoalTreeDTO dto){
        if(dto.getChild().getId() != null)
            go.setChild(service.getReference(dto.getChild().getId()));
    }
}
