package com.rbp.sayban.model.mapper.goal;


import com.rbp.sayban.model.domainmodel.goal.GoalRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.goal.GoalRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalRiskMapper extends GenericMapper<GoalRiskViewModel, GoalRisk> {
    GoalRiskMapper INSTANCE = Mappers.getMapper(GoalRiskMapper.class);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "risk.id", target = "risk")
    GoalRisk toDomainModel(GoalRiskViewModel goalRiskViewModel);
}
