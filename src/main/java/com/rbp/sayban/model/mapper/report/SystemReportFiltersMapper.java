/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.report;

import com.rbp.sayban.model.domainmodel.report.SystemReportFilter;
import com.rbp.sayban.model.dto.report.SystemReportFilterDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface SystemReportFiltersMapper extends GenericMapper<SystemReportFilterDTO, SystemReportFilter> {

    SystemReportFiltersMapper INSTANCE = Mappers.getMapper(SystemReportFiltersMapper.class);

    @Mapping(source = "system", target = "systemId")
    @Mapping(source = "filterOperator", target = "filterOperatorId")
    SystemReportFilterDTO toView(SystemReportFilter SystemReportFilters);

    @Mapping(source = "systemId", target = "system")
    @Mapping(source = "filterOperatorId", target = "filterOperator")
    SystemReportFilter toDomainModel(SystemReportFilterDTO SystemReportFilterDTO);

}