package com.rbp.sayban.model.mapper.project;


import com.rbp.sayban.model.domainmodel.project.ProjectKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.project.ProjectKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectKpiMapper extends GenericMapper<ProjectKpiViewModel, ProjectKpi> {
    ProjectKpiMapper INSTANCE = Mappers.getMapper(ProjectKpiMapper.class);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "kpi.id", target = "kpi")
    ProjectKpi toDomainModel(ProjectKpiViewModel projectKpiViewModel);
}
