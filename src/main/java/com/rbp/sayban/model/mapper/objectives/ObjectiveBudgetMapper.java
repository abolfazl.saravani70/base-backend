package com.rbp.sayban.model.mapper.objectives;

import com.rbp.sayban.model.domainmodel.objectives.ObjectiveBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveBudgetMapper extends GenericMapper<ObjectiveBudgetViewModel, ObjectiveBudget> {

    ObjectiveBudgetMapper INSTANCE = Mappers.getMapper(ObjectiveBudgetMapper.class);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "budget.id", target = "budget")
    ObjectiveBudget toDomainModel(ObjectiveBudgetViewModel objectiveBudgetViewModel);
}
