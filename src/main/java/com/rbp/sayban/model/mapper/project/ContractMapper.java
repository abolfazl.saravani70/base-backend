package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.Contract;
import com.rbp.sayban.model.dto.project.ContractDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ContractMapper extends GenericMapper<ContractDTO, Contract>{
    ContractMapper INSTANCE= Mappers.getMapper(ContractMapper.class);

    @Mapping(source = "document",target = "documentId")
    @Mapping(source = "baseContract",target = "baseContractId")
    @Mapping(source = "project",target = "projectId")
    ContractDTO toView(Contract contract);

    @Mapping(source = "documentId",target = "document")
    @Mapping(source = "baseContractId",target = "baseContract")
    @Mapping(source = "projectId",target = "project")
    Contract toDomainModel(ContractDTO contractDTO);
}
