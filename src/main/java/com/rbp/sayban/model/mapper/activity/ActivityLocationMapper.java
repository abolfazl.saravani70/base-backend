package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityKpi;
import com.rbp.sayban.model.domainmodel.activity.ActivityLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityKpiViewModel;
import com.rbp.sayban.model.viewModel.activity.ActivityLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityLocationMapper  extends GenericMapper<ActivityLocationViewModel, ActivityLocation> {

    ActivityLocationMapper INSTANCE= Mappers.getMapper(ActivityLocationMapper.class);

    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "location.id", target = "location")
    ActivityLocation toDomainModel(ActivityLocationViewModel activityLocationViewModel);
}
