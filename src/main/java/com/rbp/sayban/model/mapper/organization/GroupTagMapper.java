/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.GroupTag;
import com.rbp.sayban.model.dto.organization.GroupTagDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GroupTagMapper extends GenericMapper<GroupTagDTO, GroupTag> {
    GroupTagMapper INSTANCE = Mappers.getMapper(GroupTagMapper.class);

    @Mapping(source = "group", target = "groupId")
    @Mapping(source = "tag", target = "tagId")
    GroupTagDTO toView(GroupTag groupTag);

//    @Mapping(source = "groupId", target = "group")
//    @Mapping(source = "tagId", target = "tag")
//    SysRule toDomainModel(GroupTagDTO groupTagDTO);
}
