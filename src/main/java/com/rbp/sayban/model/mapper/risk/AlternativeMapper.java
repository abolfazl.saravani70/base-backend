/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.risk;

import com.rbp.sayban.model.domainmodel.risk.Alternative;
import com.rbp.sayban.model.dto.risk.AlternativeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface AlternativeMapper extends GenericMapper<AlternativeDTO, Alternative> {
    AlternativeMapper INSTANCE = Mappers.getMapper(AlternativeMapper.class);

    @Mapping(source = "solution", target = "solutionId")
    AlternativeDTO toView(Alternative alternative);

    @Mapping(source = "solutionId", target = "solution")
    Alternative toDomainModel(AlternativeDTO alternativeDTO);
}
