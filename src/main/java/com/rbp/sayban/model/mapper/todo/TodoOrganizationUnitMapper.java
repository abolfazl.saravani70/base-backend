package com.rbp.sayban.model.mapper.todo;


import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.domainmodel.todo.TodoOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.todo.TodoOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoOrganizationUnitMapper extends GenericMapper<TodoOrganizationUnitViewModel, TodoOrganizationUnit> {
    TodoOrganizationUnitMapper INSTANCE = Mappers.getMapper(TodoOrganizationUnitMapper.class);

    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    TodoOrganizationUnit toDomainModel(TodoOrganizationUnitViewModel TodoOrganizationUnitViewModel);

    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    TodoOrganizationUnitViewModel toView(TodoOrganizationUnit todoOrganizationUnit);
}
