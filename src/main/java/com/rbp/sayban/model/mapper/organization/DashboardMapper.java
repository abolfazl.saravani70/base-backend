/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Dashboard;
import com.rbp.sayban.model.dto.organization.DashboardDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface DashboardMapper extends GenericMapper<DashboardDTO, Dashboard> {
    DashboardMapper INSTANCE = Mappers.getMapper(DashboardMapper.class);

    @Mapping(source = "system", target = "systemId")
    @Mapping(source = "application", target = "applicationId")
    @Mapping(source = "user", target = "userId")
    @Mapping(source = "group", target = "groupId")
    DashboardDTO toView(Dashboard dashboard);

//    @Mapping(source = "systemId", target = "system")
//    @Mapping(source = "applicationId", target = "application")
//    @Mapping(source = "userId", target = "user")
//    @Mapping(source = "groupId", target = "group")
//    SysRule toDomainModel(DashboardDTO dashboardDTO);

}
