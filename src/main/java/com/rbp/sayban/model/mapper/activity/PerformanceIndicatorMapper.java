package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.PerformanceIndicator;
import com.rbp.sayban.model.dto.activity.PerformanceIndicatorDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PerformanceIndicatorMapper extends GenericMapper<PerformanceIndicatorDTO, PerformanceIndicator> {
    PerformanceIndicatorMapper INSTANCE= Mappers.getMapper(PerformanceIndicatorMapper.class);

    @Mapping(source = "activity",target = "activityId")
    PerformanceIndicatorDTO toView(PerformanceIndicator performanceIndicator);

    @Mapping(source = "activityId",target = "activity")
    PerformanceIndicator toDomainModel(PerformanceIndicatorDTO performanceIndicatorDTO);
}
