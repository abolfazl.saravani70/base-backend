package com.rbp.sayban.model.mapper.task;


import com.rbp.sayban.model.domainmodel.task.TaskNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.task.TaskNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskNormCostMapper extends GenericMapper<TaskNormCostViewModel, TaskNormCost> {
    TaskNormCostMapper INSTANCE = Mappers.getMapper(TaskNormCostMapper.class);

    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "normCost.id", target = "normCost")
    TaskNormCost toDomainModel(TaskNormCostViewModel taskNormCostViewModel);
}
