package com.rbp.sayban.model.mapper.goal;

import com.rbp.sayban.model.domainmodel.goal.GoalBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.goal.GoalBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalBudgetMapper extends GenericMapper<GoalBudgetViewModel, GoalBudget> {

    GoalBudgetMapper INSTANCE = Mappers.getMapper(GoalBudgetMapper.class);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "budget.id", target = "budget")
    GoalBudget toDomainModel(GoalBudgetViewModel goalBudgetViewModel);
}
