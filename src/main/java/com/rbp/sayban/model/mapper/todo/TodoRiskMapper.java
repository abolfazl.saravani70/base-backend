package com.rbp.sayban.model.mapper.todo;


import com.rbp.sayban.model.domainmodel.todo.TodoRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.todo.TodoRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoRiskMapper extends GenericMapper<TodoRiskViewModel, TodoRisk> {
    TodoRiskMapper INSTANCE = Mappers.getMapper(TodoRiskMapper.class);

    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "risk.id", target = "risk")
    TodoRisk toDomainModel(TodoRiskViewModel TodoRiskViewModel);
}
