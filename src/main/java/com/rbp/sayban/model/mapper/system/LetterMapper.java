/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.Letter;
import com.rbp.sayban.model.dto.system.LetterDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface LetterMapper extends GenericMapper<LetterDTO, Letter> {

    LetterMapper INSTANCE = Mappers.getMapper(LetterMapper.class);

    @Mapping(source = "document", target = "documentId")
    LetterDTO toView(Letter Letter);

    @Mapping(source = "documentId", target = "document")
    Letter toDomainModel(LetterDTO LetterDTO);
}