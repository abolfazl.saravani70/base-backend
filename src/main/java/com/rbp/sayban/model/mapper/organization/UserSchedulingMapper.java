/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.UserScheduling;
import com.rbp.sayban.model.dto.organization.UserSchedulingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface UserSchedulingMapper extends GenericMapper<UserSchedulingDTO, UserScheduling> {
    UserSchedulingMapper INSTANCE = Mappers.getMapper(UserSchedulingMapper.class);

    @Mapping(source = "user", target = "userId")
    UserSchedulingDTO toView(UserScheduling userScheduling);
//
//    @Mapping(source = "userId", target = "user")
//    SysRule toDomainModel(UserSchedulingDTO userSchedulingDTO);

}
