/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;


import com.rbp.sayban.model.domainmodel.application.Scope;
import com.rbp.sayban.model.dto.application.ScopeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ScopeMapper extends GenericMapper<ScopeDTO, Scope> {
    ScopeMapper INSTANCE = Mappers.getMapper(ScopeMapper.class);

    @Mapping(source = "application",target = "applicationId")
    ScopeDTO toView(Scope scope);

    @Mapping(source = "applicationId",target = "application")
    Scope toDomainModel(ScopeDTO scopeDTO);

}
