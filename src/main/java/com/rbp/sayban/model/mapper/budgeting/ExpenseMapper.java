/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.Expense;
import com.rbp.sayban.model.dto.budgeting.ExpenseDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ExpenseMapper extends GenericMapper<ExpenseDTO, Expense> {
    ExpenseMapper INSTANCE = Mappers.getMapper(ExpenseMapper.class);

    @Mapping(source = "budget", target = "budgetId")
    ExpenseDTO toView(Expense expense);

    @Mapping(source = "budgetId", target = "budget")
    Expense toDomainModel(ExpenseDTO expenseDTO);
}
