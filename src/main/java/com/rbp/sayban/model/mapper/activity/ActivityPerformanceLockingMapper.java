package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActivityPerformanceLocking;
import com.rbp.sayban.model.dto.activity.ActivityPerformanceLockingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityPerformanceLockingMapper extends GenericMapper<ActivityPerformanceLockingDTO, ActivityPerformanceLocking> {
    ActivityPerformanceLockingMapper INSTANCE= Mappers.getMapper(ActivityPerformanceLockingMapper.class);

    @Mapping(source = "activityPerformance",target = "activityPerformanceId")
    ActivityPerformanceLockingDTO toView(ActivityPerformanceLocking activityPerformanceLocking);

    @Mapping(source = "activityPerformanceId",target = "activityPerformance")
    ActivityPerformanceLocking toDomainModel(ActivityPerformanceLockingDTO activityPerformanceLockingDTO);
}
