package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityOrganizationUnitMapper extends GenericMapper<ActivityOrganizationUnitViewModel, ActivityOrganizationUnit> {

    ActivityOrganizationUnitMapper INSTANCE= Mappers.getMapper(ActivityOrganizationUnitMapper.class);

    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    ActivityOrganizationUnit toDomainModel(ActivityOrganizationUnitViewModel activityOrganizationUnitViewModel);

    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    ActivityOrganizationUnitViewModel toView(ActivityOrganizationUnit activityOrganizationUnit);
}
