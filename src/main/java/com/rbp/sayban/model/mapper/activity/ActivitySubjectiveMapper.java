package com.rbp.sayban.model.mapper.activity;


import com.rbp.sayban.model.domainmodel.activity.ActivitySubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivitySubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivitySubjectiveMapper extends GenericMapper<ActivitySubjectiveViewModel, ActivitySubjective> {

    ActivitySubjectiveMapper INSTANCE= Mappers.getMapper(ActivitySubjectiveMapper.class);

    ActivitySubjectiveViewModel toView(ActivitySubjective activitySubjective);
    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "subjective.id", target = "subjective")
    ActivitySubjective toDomainModel(ActivitySubjectiveViewModel activitySubjectiveViewModel);

}