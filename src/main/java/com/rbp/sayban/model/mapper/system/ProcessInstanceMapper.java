/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.ProcessInstance;
import com.rbp.sayban.model.dto.system.ProcessInstanceDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProcessInstanceMapper extends GenericMapper<ProcessInstanceDTO, ProcessInstance> {

    ProcessInstanceMapper INSTANCE = Mappers.getMapper(ProcessInstanceMapper.class);

    @Mapping(source = "processTemplate", target = "processTemplateId")
    ProcessInstanceDTO toView(ProcessInstance ProcessInstance);

    @Mapping(source = "processTemplateId", target = "processTemplate")
    ProcessInstance toDomainModel(ProcessInstanceDTO ProcessInstanceDTO);

}