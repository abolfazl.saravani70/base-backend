/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityTree;
import com.rbp.sayban.model.dto.activity.ActivityTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.activity.IActivityService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class ActivityTreeMapper implements GenericMapper<ActivityTreeDTO, ActivityTree> {
    @Autowired
    IActivityService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract ActivityTreeDTO toView(ActivityTree activityTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract ActivityTree toDomainModel(ActivityTreeDTO activityTreeDTO);

    @BeforeMapping
    void beforeMapping(@MappingTarget ActivityTree act, ActivityTreeDTO dto){
        if(dto.getChild().getId() != null)
            act.setChild(service.getReference(dto.getChild().getId()));
    }
}
