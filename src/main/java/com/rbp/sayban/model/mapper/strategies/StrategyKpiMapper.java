package com.rbp.sayban.model.mapper.strategies;


import com.rbp.sayban.model.domainmodel.strategies.StrategyKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.strategies.StrategyKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyKpiMapper extends GenericMapper<StrategyKpiViewModel, StrategyKpi> {
    StrategyKpiMapper INSTANCE = Mappers.getMapper(StrategyKpiMapper.class);

    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "kpi.id", target = "kpi")
    StrategyKpi toDomainModel(StrategyKpiViewModel strategyKpiViewModel);
}
