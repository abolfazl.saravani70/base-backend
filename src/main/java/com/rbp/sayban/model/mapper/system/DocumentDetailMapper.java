/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.DocumentDetail;
import com.rbp.sayban.model.dto.system.DocumentDetailDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface DocumentDetailMapper extends GenericMapper<DocumentDetailDTO, DocumentDetail> {
    DocumentDetailMapper INSTANCE = Mappers.getMapper(DocumentDetailMapper.class);

    @Mapping(source = "document", target = "documentId")
    DocumentDetailDTO toView(DocumentDetail documentDetail);

    @Mapping(source = "documentId", target = "document")
    DocumentDetail toDomainModel(DocumentDetailDTO documentDetailDTO);
}
