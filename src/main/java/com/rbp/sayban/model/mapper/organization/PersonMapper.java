/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Person;
import com.rbp.sayban.model.dto.organization.PersonDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PersonMapper extends GenericMapper<PersonDTO, Person> {
    PersonMapper INSTANCE = Mappers.getMapper(PersonMapper.class);

    @Mapping(source = "stakeholder", target = "stakeholderId")
//    @Mapping(source = "organizationUnit",target = "organizationUnitId")
    PersonDTO toView(Person person);

    @Mapping(source = "stakeholderId",target = "stakeholder")
//    @Mapping(source = "organizationUnitId",target = "organizationUnit")
    Person toDomainModel(PersonDTO personDTO);
//    @Mapping(source = "imageId", target = "image")
//    @Mapping(source = "stakeholderId", target = "stakeholder")
//   // @Mapping(source = "organizationPositionId", target = "organizationPosition")
////    SysRule toDomainModel(PersonDTO personDTO);
}
