package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.Payment;
import com.rbp.sayban.model.dto.activity.PaymentDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PaymentMapper extends GenericMapper<PaymentDTO, Payment> {
    PaymentMapper INSTANCE= Mappers.getMapper(PaymentMapper.class);

    @Mapping(source = "activity",target = "activityId")
    PaymentDTO toView(Payment payment);

    @Mapping(source = "activityId",target = "activity")
    Payment toDomainModel(PaymentDTO paymentDTO);
}
