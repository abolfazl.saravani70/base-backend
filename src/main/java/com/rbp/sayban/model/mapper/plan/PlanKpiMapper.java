package com.rbp.sayban.model.mapper.plan;


import com.rbp.sayban.model.domainmodel.planning.PlanKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.planning.PlanKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanKpiMapper extends GenericMapper<PlanKpiViewModel, PlanKpi> {
    PlanKpiMapper INSTANCE = Mappers.getMapper(PlanKpiMapper.class);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "kpi.id", target = "kpi")
    PlanKpi toDomainModel(PlanKpiViewModel planKpiViewModel);
}
