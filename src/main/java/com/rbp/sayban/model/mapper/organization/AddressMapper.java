/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Address;
import com.rbp.sayban.model.domainmodel.system.SysRule;
import com.rbp.sayban.model.dto.organization.AddressDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface AddressMapper extends GenericMapper<AddressDTO, Address> {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    @Mapping(source = "stakeholder", target = "stakeholderId")
    @Mapping(source = "zone", target = "zoneId")
    AddressDTO toView(Address addreses);

    @Mapping(source = "stakeholderId", target = "stakeholder")
    @Mapping(source = "zoneId", target = "zone")
    Address toDomainModel(AddressDTO addressDTO);

    Set<Address> setLongToAddress(Set<Long> longs);
    Set<Long> setAddressToLong(Set<Address> addresses);

}
