/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.todo;

import com.rbp.sayban.model.domainmodel.todo.Todo;
import com.rbp.sayban.model.dto.todo.TodoDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoMapper extends GenericMapper<TodoDTO, Todo> {
    TodoMapper INSTANCE = Mappers.getMapper(TodoMapper.class);

    TodoDTO toView(Todo todo);

    Todo toDomainModel(TodoDTO todoDTO);
}
