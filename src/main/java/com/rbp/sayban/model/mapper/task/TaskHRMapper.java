package com.rbp.sayban.model.mapper.task;

import com.rbp.sayban.model.domainmodel.task.TaskHR;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.task.TaskHRViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskHRMapper extends GenericMapper<TaskHRViewModel, TaskHR> {

    TaskHRMapper INSTANCE = Mappers.getMapper(TaskHRMapper.class);

    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "humanResource.id", target = "humanResource")
    TaskHR toDomainModel(TaskHRViewModel taskHRViewModel);
}
