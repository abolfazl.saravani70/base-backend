package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.ProjectBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.project.ProjectBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectBudgetMapper  extends GenericMapper<ProjectBudgetViewModel, ProjectBudget> {

    ProjectBudgetMapper INSTANCE = Mappers.getMapper(ProjectBudgetMapper.class);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "budget.id", target = "budget")
    ProjectBudget toDomainModel(ProjectBudgetViewModel projectBudgetViewModel);
}
