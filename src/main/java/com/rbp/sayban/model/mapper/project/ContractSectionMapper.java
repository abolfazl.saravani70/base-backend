package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.ContractSection;
import com.rbp.sayban.model.dto.project.ContractSectionDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ContractSectionMapper extends GenericMapper<ContractSectionDTO, ContractSection> {
    ContractSectionMapper INSTANCE= Mappers.getMapper(ContractSectionMapper.class);

    @Mapping(source = "contract",target = "contractId")
    ContractSectionDTO toView(ContractSection contractSection);

    @Mapping(source ="contractId",target = "contract")
    ContractSection toDomainModel(ContractSectionDTO contractSectionDTO);
}
