/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.SystemConfig;
import com.rbp.sayban.model.dto.system.SystemConfigDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface SystemConfigMapper extends GenericMapper<SystemConfigDTO, SystemConfig> {

    SystemConfigMapper INSTANCE = Mappers.getMapper(SystemConfigMapper.class);

    @Mapping(source = "system", target = "systemId")
    @Mapping(source = "enumValue",target = "enumValueId")
    SystemConfigDTO toView(SystemConfig SystemConfig);

    @Mapping(source = "systemId", target = "system")
    @Mapping(source = "enumValueId",target = "enumValue")
    SystemConfig toDomainModel(SystemConfigDTO SystemConfigDTO);

}