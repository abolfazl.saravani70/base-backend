package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateType;
import com.rbp.sayban.model.dto.activity.ActivityTemplateTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityTemplateTypeMapper extends GenericMapper<ActivityTemplateTypeDTO, ActivityTemplateType> {
    ActivityTemplateTypeMapper INSTANCE= Mappers.getMapper(ActivityTemplateTypeMapper.class);

}
