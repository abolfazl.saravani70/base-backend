/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ScalarIndicator;
import com.rbp.sayban.model.dto.activity.ScalarIndicatorDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ScalarIndicatorMapper extends GenericMapper<ScalarIndicatorDTO, ScalarIndicator> {
    ScalarIndicatorMapper INSTANCE = Mappers.getMapper(ScalarIndicatorMapper.class);

    @Mapping(source = "activity", target = "activityId")
    ScalarIndicatorDTO toView(ScalarIndicator scalarIndicator);

    @Mapping(source = "activityId", target = "activity")
    ScalarIndicator toDomainModel(ScalarIndicatorDTO scalarIndicatorDTO);
}
