/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.Budget;
import com.rbp.sayban.model.dto.budgeting.BudgetDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface BudgetMapper extends GenericMapper<BudgetDTO, Budget> {
    BudgetMapper INSTANCE = Mappers.getMapper(BudgetMapper.class);

    @Mapping(source = "budgetType", target = "budgetTypeId")
//    @Mapping(source = "budget", target = "budgetId")
    BudgetDTO toView(Budget budget);

    @Mapping(source = "budgetTypeId", target = "budgetType")
//    @Mapping(source = "budgetId", target = "budget")
    Budget toDomainModel(BudgetDTO budgetDTO);
}
