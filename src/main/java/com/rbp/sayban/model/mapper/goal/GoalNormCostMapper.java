package com.rbp.sayban.model.mapper.goal;


import com.rbp.sayban.model.domainmodel.goal.GoalNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.goal.GoalNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalNormCostMapper extends GenericMapper<GoalNormCostViewModel, GoalNormCost> {
    GoalNormCostMapper INSTANCE = Mappers.getMapper(GoalNormCostMapper.class);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "normCost.id", target = "normCost")
    GoalNormCost toDomainModel(GoalNormCostViewModel goalNormCostViewModel);
}
