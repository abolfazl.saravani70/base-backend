package com.rbp.sayban.model.mapper.plan;


import com.rbp.sayban.model.domainmodel.planning.PlanRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.planning.PlanRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanRiskMapper extends GenericMapper<PlanRiskViewModel, PlanRisk> {
    PlanRiskMapper INSTANCE = Mappers.getMapper(PlanRiskMapper.class);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "risk.id", target = "risk")
    PlanRisk toDomainModel(PlanRiskViewModel planRiskViewModel);
}
