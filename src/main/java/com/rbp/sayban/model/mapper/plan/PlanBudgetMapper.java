package com.rbp.sayban.model.mapper.plan;

import com.rbp.sayban.model.domainmodel.planning.PlanBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.planning.PlanBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PlanBudgetMapper extends GenericMapper<PlanBudgetViewModel, PlanBudget> {

    PlanBudgetMapper INSTANCE = Mappers.getMapper(PlanBudgetMapper.class);

    @Mapping(source = "plan.id", target = "plan")
    @Mapping(source = "budget.id", target = "budget")
    PlanBudget toDomainModel(PlanBudgetViewModel planBudgetViewModel);
}
