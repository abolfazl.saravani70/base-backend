package com.rbp.sayban.model.mapper.task;


import com.rbp.sayban.model.domainmodel.task.TaskRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.task.TaskRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskRiskMapper extends GenericMapper<TaskRiskViewModel, TaskRisk> {
    TaskRiskMapper INSTANCE = Mappers.getMapper(TaskRiskMapper.class);

    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "risk.id", target = "risk")
    TaskRisk toDomainModel(TaskRiskViewModel taskRiskViewModel);
}
