package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.kpi.Kpi;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import com.rbp.sayban.model.dto.kpi.KpiDTO;
import com.rbp.sayban.model.dto.organization.HumanResourceDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.organization.OrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class,uses = BaseMapperMethods.class)
public interface HumanResourceMapper extends GenericMapper<HumanResourceDTO, HumanResource> {

    HumanResourceMapper INSTANCE= Mappers.getMapper(HumanResourceMapper.class);

    HumanResourceDTO toView(HumanResource humanResource);

    HumanResource toDomainModel(HumanResourceDTO humanResourceDTO);
}
