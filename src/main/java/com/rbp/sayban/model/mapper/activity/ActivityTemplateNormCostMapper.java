package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateNormCost;
import com.rbp.sayban.model.dto.activity.ActivityTemplateNormCostDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityTemplateNormCostMapper extends GenericMapper<ActivityTemplateNormCostDTO, ActivityTemplateNormCost> {
    ActivityTemplateNormCostMapper INSTANCE= Mappers.getMapper(ActivityTemplateNormCostMapper.class);

    @Mapping(source = "activityTemplate",target = "activityTemplateId")
//    @Mapping(source = "normCost", target = "normCostId")
    ActivityTemplateNormCostDTO toView(ActivityTemplateNormCost activityTemplateNormCost);

    @Mapping(source = "activityTemplateId",target = "activityTemplate")
//    @Mapping(source = "normCost", target = "normCost")
    ActivityTemplateNormCost toDomainModel(ActivityTemplateNormCostDTO activityTemplateNormCostDTO);
}
