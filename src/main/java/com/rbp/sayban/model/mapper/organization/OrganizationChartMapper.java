/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.organization.OrganizationChartViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = {BaseMapperMethods.class})
public interface OrganizationChartMapper extends GenericMapper<OrganizationChartViewModel, OrganizationChart> {
    OrganizationChartMapper INSTANCE = Mappers.getMapper(OrganizationChartMapper.class);


    @Mapping(source = "pvOrganizeTypeId", target = "pvOrganizeType")
    @Mapping(source = "pvOrgDegreeId", target = "pvOrgDegree")
    @Mapping(source = "pvOrgLevelId", target = "pvOrgLevel")
    @Mapping(source = "pvPositionNumberId", target = "pvPositionNumber")
    @Mapping(source = "pvPositionTitleId", target = "pvPositionTitle")
    @Mapping(source = "parent.id", target = "parentId")
    @Mapping(source = "parent.name", target = "parentName")
    OrganizationChartViewModel toView(OrganizationChart organizationChart);

    @Mapping(source = "parentId", target = "parent")
    OrganizationChart toDomainModel(OrganizationChartViewModel organizationChartViewModel);

}
