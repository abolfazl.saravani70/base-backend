/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.strategies;

import com.rbp.sayban.model.domainmodel.strategies.Strategy;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyMapper extends GenericMapper<StrategyDTO, Strategy> {
    StrategyMapper INSTANCE = Mappers.getMapper(StrategyMapper.class);


    StrategyDTO toView(Strategy strategy);


    Strategy toDomainModel(StrategyDTO strategyDTO);
}
