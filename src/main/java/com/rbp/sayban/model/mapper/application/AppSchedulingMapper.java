/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;

import com.rbp.sayban.model.domainmodel.application.AppScheduling;
import com.rbp.sayban.model.dto.application.AppSchedulingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface AppSchedulingMapper extends GenericMapper<AppSchedulingDTO, AppScheduling> {

    AppSchedulingMapper INSTANCE = Mappers.getMapper(AppSchedulingMapper.class);

    @Mapping(source = "application", target = "applicationId")
    AppSchedulingDTO toView(AppScheduling AppScheduling);

    @Mapping(source = "applicationId", target = "application")
    AppScheduling toDomainModel(AppSchedulingDTO SchedulingListDTO);

}