package com.rbp.sayban.model.mapper.todo;


import com.rbp.sayban.model.domainmodel.todo.TodoSubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.todo.TodoSubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoSubjectiveMapper extends GenericMapper<TodoSubjectiveViewModel, TodoSubjective> {

    TodoSubjectiveMapper INSTANCE= Mappers.getMapper(TodoSubjectiveMapper.class);


    TodoSubjectiveViewModel toView(TodoSubjective todoSubjective);
    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "subjective.id", target = "subjective")
    TodoSubjective toDomainModel(TodoSubjectiveViewModel todoSubjectiveViewModel);

}