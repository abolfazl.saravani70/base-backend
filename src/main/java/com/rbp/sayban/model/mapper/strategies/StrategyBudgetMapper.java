package com.rbp.sayban.model.mapper.strategies;

import com.rbp.sayban.model.domainmodel.strategies.StrategyBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.strategies.StrategyBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyBudgetMapper  extends GenericMapper<StrategyBudgetViewModel, StrategyBudget> {
    StrategyBudgetMapper INSTANCE = Mappers.getMapper(StrategyBudgetMapper.class);

    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "budget.id", target = "budget")
    StrategyBudget toDomainModel(StrategyBudgetViewModel strategyBudgetViewModel);
}
