/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.todo;

import com.rbp.sayban.model.domainmodel.todo.TodoTree;
import com.rbp.sayban.model.dto.todo.TodoTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.todo.ITodoService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class TodoTreeMapper implements GenericMapper<TodoTreeDTO, TodoTree> {
    @Autowired
    ITodoService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract TodoTreeDTO toView(TodoTree todoTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract TodoTree toDomainModel(TodoTreeDTO todoTreeDTO);

    @BeforeMapping
    void beforeMapping(@MappingTarget TodoTree to, TodoTreeDTO dto){
        if(dto.getChild().getId() != null)
            to.setChild(service.getReference(dto.getChild().getId()));
    }
}
