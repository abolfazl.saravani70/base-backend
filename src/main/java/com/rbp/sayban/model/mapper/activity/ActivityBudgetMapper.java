package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityBudgetMapper extends GenericMapper<ActivityBudgetViewModel, ActivityBudget> {

    ActivityBudgetMapper INSTANCE= Mappers.getMapper(ActivityBudgetMapper.class);

    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "budget.id", target = "budget")
    ActivityBudget toDomainModel(ActivityBudgetViewModel activityBudgetViewModel);
}
