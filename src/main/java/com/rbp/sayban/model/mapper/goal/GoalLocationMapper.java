package com.rbp.sayban.model.mapper.goal;

import com.rbp.sayban.model.domainmodel.goal.GoalKpi;
import com.rbp.sayban.model.domainmodel.goal.GoalLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.goal.GoalKpiViewModel;
import com.rbp.sayban.model.viewModel.goal.GoalLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalLocationMapper  extends GenericMapper<GoalLocationViewModel, GoalLocation> {
    GoalLocationMapper INSTANCE = Mappers.getMapper(GoalLocationMapper.class);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "location.id", target = "location")
    GoalLocation toDomainModel(GoalLocationViewModel goalLocationViewModel);
}
