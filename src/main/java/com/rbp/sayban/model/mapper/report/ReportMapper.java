/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.report;

import com.rbp.sayban.model.domainmodel.report.Report;
import com.rbp.sayban.model.dto.report.ReportDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ReportMapper extends GenericMapper<ReportDTO, Report> {

    ReportMapper INSTANCE = Mappers.getMapper(ReportMapper.class);

    @Mapping(source = "view", target = "viewId")
    @Mapping(source = "storeProcedure", target = "storeProcedureId")
    @Mapping(source = "appEntity", target = "appEntityId")
    @Mapping(source = "reportFilter", target = "reportFilterId")
    ReportDTO toView(Report Report);

    @Mapping(source = "viewId", target = "view")
    @Mapping(source = "storeProcedureId", target = "storeProcedure")
    @Mapping(source = "appEntityId", target = "appEntity")
    @Mapping(source = "reportFilterId", target = "reportFilter")
    Report toDomainModel(ReportDTO ReportDTO);

}