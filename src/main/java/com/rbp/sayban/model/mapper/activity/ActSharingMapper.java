/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActSharing;
import com.rbp.sayban.model.dto.activity.ActSharingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActSharingMapper extends GenericMapper<ActSharingDTO, ActSharing> {
    ActSharingMapper INSTANCE = Mappers.getMapper(ActSharingMapper.class);

    @Mapping(source = "activity", target = "activityId")
    ActSharingDTO toView(ActSharing actSharing);

    @Mapping(source = "activityId", target = "activity")
    ActSharing toDomainModel(ActSharingDTO actSharingDTO);
}
