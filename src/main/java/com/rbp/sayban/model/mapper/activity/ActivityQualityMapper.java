package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActivityQuality;
import com.rbp.sayban.model.dto.activity.ActivityQualityDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityQualityMapper extends GenericMapper<ActivityQualityDTO, ActivityQuality> {
    ActivityQualityMapper INSTANCE= Mappers.getMapper(ActivityQualityMapper.class);

    @Mapping(source = "activity",target = "activityId")
    ActivityQualityDTO toView(ActivityQuality activityQuality);

    @Mapping(source = "activityId",target = "activity")
    ActivityQuality toDomainModel(ActivityQualityDTO activityQualityDTO);
}
