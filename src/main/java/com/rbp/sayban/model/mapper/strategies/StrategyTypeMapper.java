/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.strategies;

import com.rbp.sayban.model.domainmodel.strategies.StrategyType;
import com.rbp.sayban.model.dto.strategies.StrategyTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyTypeMapper extends GenericMapper<StrategyTypeDTO, StrategyType> {
    StrategyTypeMapper INSTANCE = Mappers.getMapper(StrategyTypeMapper.class);
}
