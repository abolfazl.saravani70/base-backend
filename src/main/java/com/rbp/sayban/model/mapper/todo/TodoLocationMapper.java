package com.rbp.sayban.model.mapper.todo;

import com.rbp.sayban.model.domainmodel.todo.TodoLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.todo.TodoLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoLocationMapper extends GenericMapper<TodoLocationViewModel, TodoLocation> {

    TodoLocationMapper INSTANCE = Mappers.getMapper(TodoLocationMapper.class);

    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "location.id", target = "location")
    TodoLocation toDomainModel(TodoLocationViewModel todoLocationViewModel);
}
