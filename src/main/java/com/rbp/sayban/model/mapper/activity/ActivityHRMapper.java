package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityHR;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityHRViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityHRMapper  extends GenericMapper<ActivityHRViewModel, ActivityHR> {

    ActivityHRMapper INSTANCE= Mappers.getMapper(ActivityHRMapper.class);

    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "humanResource.id", target = "humanResource")
    ActivityHR toDomainModel(ActivityHRViewModel activityHRViewModel);
}
