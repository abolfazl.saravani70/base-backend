/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.strategies;

import com.rbp.sayban.model.domainmodel.strategies.StrategyExecutionType;
import com.rbp.sayban.model.dto.strategies.StrategyExecutionTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyExecutionTypeMapper extends GenericMapper<StrategyExecutionTypeDTO, StrategyExecutionType> {
    StrategyExecutionTypeMapper INSTANCE = Mappers.getMapper(StrategyExecutionTypeMapper.class);

}
