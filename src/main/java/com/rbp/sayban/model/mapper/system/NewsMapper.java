/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.News;
import com.rbp.sayban.model.dto.system.NewsDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface NewsMapper extends GenericMapper<NewsDTO, News> {

    NewsMapper INSTANCE = Mappers.getMapper(NewsMapper.class);

    @Mapping(source = "reciever", target = "recieverId")
    @Mapping(source = "system", target = "systemId")
    NewsDTO toView(News News);

    @Mapping(source = "recieverId", target = "reciever")
    @Mapping(source = "systemId", target = "system")
    News toDomainModel(NewsDTO NewsDTO);

}
