/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.Policy;
import com.rbp.sayban.model.dto.budgeting.PolicyDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PolicyMapper extends GenericMapper<PolicyDTO, Policy> {
    PolicyMapper INSTANCE = Mappers.getMapper(PolicyMapper.class);

    @Mapping(source = "document", target = "documentId")
    //@Mapping(source = "post", target = "postId")
    PolicyDTO toView(Policy policy);

    @Mapping(source = "documentId", target = "document")
   // @Mapping(source = "postId", target = "post")
    Policy toDomainModel(PolicyDTO policyDTO);

}
