/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.Fund;
import com.rbp.sayban.model.dto.budgeting.FundDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface FundMapper extends GenericMapper<FundDTO, Fund> {

    FundMapper INSTANCE = Mappers.getMapper(FundMapper.class);

    @Mapping(source = "budgetResource", target = "budgetResourceId")
    @Mapping(source = "fund", target = "fundId")
    @Mapping(source = "policy", target = "policyId")
    FundDTO toView(Fund fund);

    @Mapping(source = "budgetResourceId", target = "budgetResource")
    @Mapping(source = "fundId", target = "fund")
    @Mapping(source = "policyId", target = "policy")
    Fund toDomainModel(FundDTO fundDTO);

}
