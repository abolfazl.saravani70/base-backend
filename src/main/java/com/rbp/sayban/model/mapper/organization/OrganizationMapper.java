/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Organization;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.organization.OrganizationViewModel;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.TargetType;
import org.mapstruct.factory.Mappers;

import java.util.Set;

@Mapper(config = BaseEntityMapper.class, uses = {BaseMapperMethods.class})
public interface OrganizationMapper extends GenericMapper<OrganizationViewModel, Organization> {
    OrganizationMapper INSTANCE = Mappers.getMapper(OrganizationMapper.class);

    //    @Mapping(source = "organization", target = "organizationId")
//    @Mapping(source = "stakeholder", target = "stakeholderId")
    @Mapping(source = "pvOrgTypeId", target = "pvOrganizationType")
    @Mapping(source = "pvOrgDegreeId", target = "pvOrgDegree")
    @Mapping(source = "pvOrgLevelId", target = "pvOrgLevel")
    @Mapping(source = "organization.id", target = "parentId")
    @Mapping(source = "organization.name", target = "parentTitle")
    OrganizationViewModel toView(Organization organization);

    //    @Mapping(source = "organizationId", target = "organization")
//    @Mapping(source = "stakeholderId", target = "stakeholder")
    Organization toDomainModel(OrganizationViewModel organizationVm);

    @AfterMapping
    default void update(OrganizationViewModel organizationViewModel,@TargetType Organization organization){

    }
    @AfterMapping
    default void update(Organization organization, OrganizationViewModel organizationViewModel){

    }

    Set<Organization> OrganizationViewToDomainSet(Set<OrganizationViewModel> organizationViewModelSet);
}
