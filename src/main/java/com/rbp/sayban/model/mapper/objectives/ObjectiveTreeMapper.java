/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.objectives;

import com.rbp.sayban.model.domainmodel.objectives.ObjectiveTree;
import com.rbp.sayban.model.dto.objectives.ObjectiveTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.objectives.IObjectiveService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class ObjectiveTreeMapper implements GenericMapper<ObjectiveTreeDTO, ObjectiveTree> {
    @Autowired
    IObjectiveService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract ObjectiveTreeDTO toView(ObjectiveTree objectiveTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract ObjectiveTree toDomainModel(ObjectiveTreeDTO objectiveTreeDTO);

    @BeforeMapping
    void beforeMapping(@MappingTarget ObjectiveTree ob, ObjectiveTreeDTO dto){
        if(dto.getChild().getId() != null)
            ob.setChild(service.getReference(dto.getChild().getId()));
    }
}
