package com.rbp.sayban.model.mapper.task;

import com.rbp.sayban.model.domainmodel.task.TaskBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.task.TaskBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskBudgetMapper extends GenericMapper<TaskBudgetViewModel, TaskBudget> {

    TaskBudgetMapper INSTANCE = Mappers.getMapper(TaskBudgetMapper.class);

    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "budget.id", target = "budget")
    TaskBudget toDomainModel(TaskBudgetViewModel taskBudgetViewModel);
}
