package com.rbp.sayban.model.mapper.goal;


import com.rbp.sayban.model.domainmodel.goal.GoalKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.goal.GoalKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalKpiMapper extends GenericMapper<GoalKpiViewModel, GoalKpi> {
    GoalKpiMapper INSTANCE = Mappers.getMapper(GoalKpiMapper.class);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "kpi.id", target = "kpi")
    GoalKpi toDomainModel(GoalKpiViewModel goalKpiViewModel);
}
