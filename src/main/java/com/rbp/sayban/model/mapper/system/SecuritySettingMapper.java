/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.SecuritySetting;
import com.rbp.sayban.model.dto.system.SecuritySettingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface SecuritySettingMapper extends GenericMapper<SecuritySettingDTO, SecuritySetting> {

    SecuritySettingMapper INSTANCE = Mappers.getMapper(SecuritySettingMapper.class);

    @Mapping(source = "system", target = "systemId")
    @Mapping(source = "enumValue", target = "enumValueId")
    SecuritySettingDTO toView(SecuritySetting SecuritySetting);

    @Mapping(source = "systemId", target = "system")
    @Mapping(source = "enumValueId", target = "enumValue")
    SecuritySetting toDomainModel(SecuritySettingDTO SecuritySettingDTO);

}
