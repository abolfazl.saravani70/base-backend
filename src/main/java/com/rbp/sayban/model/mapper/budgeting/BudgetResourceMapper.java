/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.budgeting;

import com.rbp.sayban.model.domainmodel.budgeting.BudgetResource;
import com.rbp.sayban.model.dto.budgeting.BudgetResourceDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class)
public interface BudgetResourceMapper extends GenericMapper<BudgetResourceDTO, BudgetResource> {
    BudgetResourceMapper INSTANCE = Mappers.getMapper(BudgetResourceMapper.class);

    @Mapping(source = "budgetResource", target = "budgetResourceId")
    @Mapping(source = "budgetType", target = "budgetTypeId")
    BudgetResourceDTO toView(BudgetResource budgetResource);

    @Mapping(source = "budgetResourceId", target = "budgetResource")
    @Mapping(source = "budgetTypeId", target = "budgetType")
    BudgetResource toDomainModel(BudgetResourceDTO budgetResourceDTO);
}
