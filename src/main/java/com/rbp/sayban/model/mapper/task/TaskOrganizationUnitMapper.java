package com.rbp.sayban.model.mapper.task;


import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.domainmodel.task.TaskOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.task.TaskOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TaskOrganizationUnitMapper extends GenericMapper<TaskOrganizationUnitViewModel, TaskOrganizationUnit> {
    TaskOrganizationUnitMapper INSTANCE = Mappers.getMapper(TaskOrganizationUnitMapper.class);

    @Mapping(source = "task.id", target = "task")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    TaskOrganizationUnit toDomainModel(TaskOrganizationUnitViewModel taskOrganizationUnitViewModel);


    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    TaskOrganizationUnitViewModel toView(TaskOrganizationUnit taskOrganizationUnit);
}
