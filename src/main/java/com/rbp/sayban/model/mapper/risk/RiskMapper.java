/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.risk;

import com.rbp.sayban.model.domainmodel.risk.Risk;
import com.rbp.sayban.model.dto.risk.RiskDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface RiskMapper extends GenericMapper<RiskDTO, Risk> {
    RiskMapper INSTANCE = Mappers.getMapper(RiskMapper.class);

    //@Mapping(source = "alternative", target = "alternativeId")
//    @Mapping(source = "risk", target = "riskId")
    RiskDTO toView(Risk risk);

  //  @Mapping(source = "alternativeId", target = "alternative")
//    @Mapping(source = "riskId", target = "risk")
    Risk toDomainModel(RiskDTO riskDTO);
}
