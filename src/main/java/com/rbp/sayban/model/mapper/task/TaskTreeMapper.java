/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.task;

import com.rbp.sayban.model.domainmodel.task.TaskTree;
import com.rbp.sayban.model.dto.task.TaskTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.task.ITaskService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class TaskTreeMapper implements GenericMapper<TaskTreeDTO, TaskTree> {
    @Autowired
    ITaskService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract TaskTreeDTO toView(TaskTree taskTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract TaskTree toDomainModel(TaskTreeDTO taskTreeDTO);

    @BeforeMapping
    void beforeMapping(@MappingTarget TaskTree tas, TaskTreeDTO dto){
        if(dto.getChild().getId() != null)
            tas.setChild(service.getReference(dto.getChild().getId()));
    }
}
