/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.License;
import com.rbp.sayban.model.dto.system.LicenseDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface LicenseMapper1 extends GenericMapper<LicenseDTO, License> {

    LicenseMapper1 INSTANCE = Mappers.getMapper(LicenseMapper1.class);

    @Mapping(source = "signing", target = "signingId")
    @Mapping(source = "document", target = "documentId")
    LicenseDTO toView(License License);

    @Mapping(source = "signingId", target = "signing")
    @Mapping(source = "documentId", target = "document")
    License toDomainModel(LicenseDTO LicenseDTO);


}