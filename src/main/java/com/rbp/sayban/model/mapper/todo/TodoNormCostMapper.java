package com.rbp.sayban.model.mapper.todo;


import com.rbp.sayban.model.domainmodel.todo.TodoNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.todo.TodoNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoNormCostMapper extends GenericMapper<TodoNormCostViewModel, TodoNormCost> {
    TodoNormCostMapper INSTANCE = Mappers.getMapper(TodoNormCostMapper.class);

    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "normCost.id", target = "normCost")
    TodoNormCost toDomainModel(TodoNormCostViewModel TodoNormCostViewModel);
}
