package com.rbp.sayban.model.mapper.strategies;

import com.rbp.sayban.model.domainmodel.strategies.StrategyLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.strategies.StrategyLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyLocationMapper  extends GenericMapper<StrategyLocationViewModel, StrategyLocation> {

    StrategyLocationMapper INSTANCE = Mappers.getMapper(StrategyLocationMapper.class);

    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "location.id", target = "location")
    StrategyLocation toDomainModel(StrategyLocationViewModel strategyLocationViewModel);
}
