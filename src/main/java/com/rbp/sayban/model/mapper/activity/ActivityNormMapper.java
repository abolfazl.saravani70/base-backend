/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActivityNorm;
import com.rbp.sayban.model.dto.activity.ActivityNormDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityNormMapper extends GenericMapper<ActivityNormDTO, ActivityNorm> {
    ActivityNormMapper INSTANCE = Mappers.getMapper(ActivityNormMapper.class);

    @Mapping(source = "activity", target = "activityId")
    @Mapping(source = "activityCost", target = "activityCostId")
    @Mapping(source = "activityNorm", target = "activityNormId")
    @Mapping(source = "activityTemplate", target = "activityTemplateId")
    @Mapping(source = "activityUnit", target = "activityUnitId")
    ActivityNormDTO toView(ActivityNorm activityNorms);

    @Mapping(source = "activityId", target = "activity")
    @Mapping(source = "activityCostId", target = "activityCost")
    @Mapping(source = "activityNormId", target = "activityNorm")
    @Mapping(source = "activityTemplateId", target = "activityTemplate")
    @Mapping(source = "activityUnitId", target = "activityUnit")
    ActivityNorm toDomainModel(ActivityNormDTO activityNormsDTO);


}
