package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.ProjectResource;
import com.rbp.sayban.model.dto.project.ProjectResourceDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectResourceMapper extends GenericMapper<ProjectResourceDTO, ProjectResource> {
    ProjectResourceMapper INSTANCE= Mappers.getMapper(ProjectResourceMapper.class);

    @Mapping(source = "projectResourceType",target = "projectResourceTypeId")
    ProjectResourceDTO toView(ProjectResource projectResource);

    @Mapping(source = "projectResourceTypeId",target = "projectResourceType")
    ProjectResource toDomainModel(ProjectResourceDTO projectResourceDTO);
}
