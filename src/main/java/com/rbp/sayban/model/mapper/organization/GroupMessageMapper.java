/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.GroupMessage;
import com.rbp.sayban.model.dto.organization.GroupMessageDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GroupMessageMapper extends GenericMapper<GroupMessageDTO, GroupMessage> {
    GroupMessageMapper INSTANCE = Mappers.getMapper(GroupMessageMapper.class);

    @Mapping(source = "group", target = "groupId")
    @Mapping(source = "message", target = "messageId")
    GroupMessageDTO toView(GroupMessage groupMessage);

//    @Mapping(source = "groupId", target = "group")
//    @Mapping(source = "messageId", target = "message")
//    SysRule toDomainModel(GroupMessageDTO groupMessageDTO);

}
