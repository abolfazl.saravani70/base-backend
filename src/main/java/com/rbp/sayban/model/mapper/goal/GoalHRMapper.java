package com.rbp.sayban.model.mapper.goal;

import com.rbp.sayban.model.domainmodel.goal.GoalHR;
import com.rbp.sayban.model.domainmodel.goal.GoalKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.goal.GoalHRViewModel;
import com.rbp.sayban.model.viewModel.goal.GoalKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalHRMapper  extends GenericMapper<GoalHRViewModel, GoalHR> {
    GoalHRMapper INSTANCE = Mappers.getMapper(GoalHRMapper.class);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "humanResource.id", target = "humanResource")
    GoalHR toDomainModel(GoalHRViewModel goalHRViewModel);
}
