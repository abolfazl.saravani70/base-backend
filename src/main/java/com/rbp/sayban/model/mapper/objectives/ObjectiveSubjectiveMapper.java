package com.rbp.sayban.model.mapper.objectives;


import com.rbp.sayban.model.domainmodel.objectives.ObjectiveSubjective;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveSubjectiveViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveSubjectiveMapper extends GenericMapper<ObjectiveSubjectiveViewModel, ObjectiveSubjective> {

    ObjectiveSubjectiveMapper INSTANCE= Mappers.getMapper(ObjectiveSubjectiveMapper.class);


    ObjectiveSubjectiveViewModel toView(ObjectiveSubjective objectiveSubjective);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "subjective.id", target = "subjective")
    ObjectiveSubjective toDomainModel(ObjectiveSubjectiveViewModel objectiveSubjectiveViewModel);

}