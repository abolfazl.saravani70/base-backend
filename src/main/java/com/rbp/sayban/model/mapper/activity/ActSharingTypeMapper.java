/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.ActSharingType;
import com.rbp.sayban.model.dto.activity.ActSharingTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActSharingTypeMapper extends GenericMapper<ActSharingTypeDTO, ActSharingType> {
    ActSharingTypeMapper INSTANCE = Mappers.getMapper(ActSharingTypeMapper.class);

    @Mapping(source = "activity", target = "activityId")
    ActSharingTypeDTO toView(ActSharingType actsharingType);

    @Mapping(source = "activityId", target = "activity")
    ActSharingType toDomainModel(ActSharingTypeDTO actsharingTypeDTO);
}
