package com.rbp.sayban.model.mapper.todo;

import com.rbp.sayban.model.domainmodel.todo.TodoBudget;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.todo.TodoBudgetViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoBudgetMapper extends GenericMapper<TodoBudgetViewModel, TodoBudget> {

    TodoBudgetMapper INSTANCE = Mappers.getMapper(TodoBudgetMapper.class);

    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "budget.id", target = "budget")
    TodoBudget toDomainModel(TodoBudgetViewModel todoBudgetViewModel);
}
