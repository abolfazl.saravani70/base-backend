package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.Project;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectMapper extends GenericMapper<ProjectDTO, Project> {
    ProjectMapper INSTANCE = Mappers.getMapper(ProjectMapper.class);

    ProjectDTO toView(Project project);

    Project toDomainModel(ProjectDTO projectDTO);
}
