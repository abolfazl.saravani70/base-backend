/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;


import com.rbp.sayban.model.domainmodel.application.Test;
import com.rbp.sayban.model.dto.application.TestDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TestMapper extends GenericMapper<TestDTO, Test> {
    TestMapper INSTANCE = Mappers.getMapper(TestMapper.class);

    @Mapping(source = "application",target = "applicationId")
    TestDTO toView(Test test);

    @Mapping(source = "applicationId",target = "application")
    Test toDomainModel(TestDTO testDTO);

}
