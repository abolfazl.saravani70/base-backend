package com.rbp.sayban.model.mapper.strategies;


import com.rbp.sayban.model.domainmodel.strategies.StrategyNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.strategies.StrategyNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyNormCostMapper extends GenericMapper<StrategyNormCostViewModel, StrategyNormCost> {
    StrategyNormCostMapper INSTANCE = Mappers.getMapper(StrategyNormCostMapper.class);

    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "normCost.id", target = "normCost")
    StrategyNormCost toDomainModel(StrategyNormCostViewModel strategyNormCostViewModel);
}
