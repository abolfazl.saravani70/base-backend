package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.ProjectKpi;
import com.rbp.sayban.model.domainmodel.project.ProjectLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.project.ProjectKpiViewModel;
import com.rbp.sayban.model.viewModel.project.ProjectLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectLocationMapper extends GenericMapper<ProjectLocationViewModel, ProjectLocation> {
    ProjectLocationMapper INSTANCE = Mappers.getMapper(ProjectLocationMapper.class);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "location.id", target = "location")
    ProjectLocation toDomainModel(ProjectLocationViewModel projectLocationViewModel);
}
