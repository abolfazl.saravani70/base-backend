/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;

import com.rbp.sayban.model.domainmodel.application.ApplicationKpi;

import com.rbp.sayban.model.dto.application.ApplicationKpiDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ApplicationKpiMapper extends GenericMapper<ApplicationKpiDTO, ApplicationKpi> {
    ApplicationKpiMapper INSTANCE = Mappers.getMapper(ApplicationKpiMapper.class);

    @Mapping(source = "application", target = "applicationId")
    ApplicationKpiDTO toView(ApplicationKpi applicationKpi);

    @Mapping(source = "applicationId", target = "application")
    ApplicationKpi toDomainModel(ApplicationKpiDTO ApplicationKPIDTO);


}
