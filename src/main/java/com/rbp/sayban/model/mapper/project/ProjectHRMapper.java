package com.rbp.sayban.model.mapper.project;

import com.rbp.sayban.model.domainmodel.project.ProjectHR;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.project.ProjectHRViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ProjectHRMapper extends GenericMapper<ProjectHRViewModel, ProjectHR> {
    ProjectHRMapper INSTANCE = Mappers.getMapper(ProjectHRMapper.class);

    @Mapping(source = "project.id", target = "project")
    @Mapping(source = "humanResource.id", target = "humanResource")
    ProjectHR toDomainModel(ProjectHRViewModel projectHRViewModel);
}
