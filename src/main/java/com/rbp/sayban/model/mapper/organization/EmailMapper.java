/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.organization;

import com.rbp.sayban.model.domainmodel.organization.Email;
import com.rbp.sayban.model.dto.organization.EmailDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface EmailMapper extends GenericMapper<EmailDTO, Email> {

    EmailMapper INSTANCE = Mappers.getMapper(EmailMapper.class);

    @Mapping(source = "stakeholder", target = "stakeholderId")
    EmailDTO toView(Email email);

    @Mapping(source = "stakeholderId", target = "stakeholder")
    Email toDomainModel(EmailDTO emailDTO);

    Set<Email> setLongToEmail(Set<Long> longs);
    Set<Long> setEmailToLong(Set<Email> emails);
}
