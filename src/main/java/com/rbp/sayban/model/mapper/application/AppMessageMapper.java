/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;


import com.rbp.sayban.model.domainmodel.application.AppMessage;
import com.rbp.sayban.model.dto.application.AppMessageDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface AppMessageMapper extends GenericMapper<AppMessageDTO, AppMessage> {
    AppMessageMapper INSTANCE = Mappers.getMapper(AppMessageMapper.class);

    @Mapping(source = "application",target = "applicationId")
    AppMessageDTO toView(AppMessage appMessage);

    @Mapping(source = "applicationId",target = "application")
    AppMessage toDomainModel(AppMessageDTO appMessageDTO);


}
