package com.rbp.sayban.model.mapper.todo;


import com.rbp.sayban.model.domainmodel.todo.TodoKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.todo.TodoKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoKpiMapper extends GenericMapper<TodoKpiViewModel, TodoKpi> {
    TodoKpiMapper INSTANCE = Mappers.getMapper(TodoKpiMapper.class);

    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "kpi.id", target = "kpi")
    TodoKpi toDomainModel(TodoKpiViewModel todoKpiViewModel);
}
