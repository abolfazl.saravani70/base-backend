/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.application;


import com.rbp.sayban.model.domainmodel.application.Page;
import com.rbp.sayban.model.dto.application.PageDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface PageMapper extends GenericMapper<PageDTO, Page> {
    PageMapper INSTANCE = Mappers.getMapper(PageMapper.class);

    @Mapping(source = "page",target = "pageId")
    PageDTO toView(Page page);

    @Mapping(source = "pageId",target = "page")
    Page toDomainModel(PageDTO pagesDTO);
}




