/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.Duty;
import com.rbp.sayban.model.dto.system.DutyDTO;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.factory.Mappers;

public interface DutyMapper extends GenericMapper<DutyDTO, Duty> {
    DutyMapper INSTANCE = Mappers.getMapper(DutyMapper.class);

}
