package com.rbp.sayban.model.mapper.objectives;


import com.rbp.sayban.model.domainmodel.objectives.ObjectiveRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveRiskMapper extends GenericMapper<ObjectiveRiskViewModel, ObjectiveRisk> {
    ObjectiveRiskMapper INSTANCE = Mappers.getMapper(ObjectiveRiskMapper.class);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "risk.id", target = "risk")
    ObjectiveRisk toDomainModel(ObjectiveRiskViewModel objectiveRiskViewModel);
}
