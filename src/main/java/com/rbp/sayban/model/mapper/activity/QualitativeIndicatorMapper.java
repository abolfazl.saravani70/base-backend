package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.dep.QualitativeIndicator;
import com.rbp.sayban.model.dto.activity.QualitativeIndicatorDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface QualitativeIndicatorMapper extends GenericMapper<QualitativeIndicatorDTO, QualitativeIndicator> {
    QualitativeIndicatorMapper INSTANCE = Mappers.getMapper(QualitativeIndicatorMapper.class);

    @Mapping(source = "activity", target = "activityId")
    QualitativeIndicatorDTO toView(QualitativeIndicator qualitativeIndicator);

    @Mapping(source = "activityId", target = "activity")
    QualitativeIndicator toDomainModel(QualitativeIndicatorDTO qualitativeIndicatorDTO);
}
