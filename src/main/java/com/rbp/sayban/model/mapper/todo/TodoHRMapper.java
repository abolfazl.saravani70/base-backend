package com.rbp.sayban.model.mapper.todo;

import com.rbp.sayban.model.domainmodel.todo.TodoHR;
import com.rbp.sayban.model.domainmodel.todo.TodoKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.todo.TodoHRViewModel;
import com.rbp.sayban.model.viewModel.todo.TodoKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface TodoHRMapper extends GenericMapper<TodoHRViewModel, TodoHR> {
    TodoHRMapper INSTANCE = Mappers.getMapper(TodoHRMapper.class);

    @Mapping(source = "todo.id", target = "todo")
    @Mapping(source = "humanResource.id", target = "humanResource")
    TodoHR toDomainModel(TodoHRViewModel todoHRViewModel);
}
