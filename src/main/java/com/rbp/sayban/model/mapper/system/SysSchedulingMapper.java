/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.SysScheduling;
import com.rbp.sayban.model.dto.system.SysSchedulingDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface SysSchedulingMapper extends GenericMapper<SysSchedulingDTO, SysScheduling> {

    SysSchedulingMapper INSTANCE = Mappers.getMapper(SysSchedulingMapper.class);

    @Mapping(source = "system", target = "systemId")
    SysSchedulingDTO toView(SysScheduling SysScheduling);

    @Mapping(source = "systemId", target = "system")
    SysScheduling toDomainModel(SysSchedulingDTO SysSchedulingDTO);

}
