package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityRiskMapper extends GenericMapper<ActivityRiskViewModel, ActivityRisk> {

    ActivityRiskMapper INSTANCE= Mappers.getMapper(ActivityRiskMapper.class);

    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "risk.id", target = "risk")
    ActivityRisk toDomainModel(ActivityRiskViewModel activityRiskViewModel);
}
