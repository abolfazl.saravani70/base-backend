package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.norm.NormType;
import com.rbp.sayban.model.dto.activity.NormTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface NormTypeMapper extends GenericMapper<NormTypeDTO, NormType> {
    NormTypeMapper INSTANCE= Mappers.getMapper(NormTypeMapper.class);

}
