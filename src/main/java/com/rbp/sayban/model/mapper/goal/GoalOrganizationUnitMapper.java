package com.rbp.sayban.model.mapper.goal;


import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.domainmodel.goal.GoalOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.goal.GoalOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface GoalOrganizationUnitMapper extends GenericMapper<GoalOrganizationUnitViewModel, GoalOrganizationUnit> {
    GoalOrganizationUnitMapper INSTANCE = Mappers.getMapper(GoalOrganizationUnitMapper.class);

    @Mapping(source = "goal.id", target = "goal")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    GoalOrganizationUnit toDomainModel(GoalOrganizationUnitViewModel goalOrganizationUnitViewModel);

    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    GoalOrganizationUnitViewModel toView(GoalOrganizationUnit goalOrganizationUnit);
}
