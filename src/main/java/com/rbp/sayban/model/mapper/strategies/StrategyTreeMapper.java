/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.mapper.strategies;

import com.rbp.sayban.model.domainmodel.strategies.StrategyTree;
import com.rbp.sayban.model.dto.strategies.StrategyTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.service.strategies.IStrategyService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public abstract class StrategyTreeMapper implements GenericMapper<StrategyTreeDTO, StrategyTree> {
    @Autowired
    IStrategyService service;

    @Mapping(source = "root", target = "root.id")
    @Mapping(source = "parent", target = "parent.id")
    public abstract StrategyTreeDTO toView(StrategyTree strategyTree);

    @Mapping(source = "root.id",target = "root")
    @Mapping(source = "parent.id",target = "parent")
    public abstract StrategyTree toDomainModel(StrategyTreeDTO strategyTreeDTO);

    @BeforeMapping
     void beforeMapping(@MappingTarget StrategyTree s, StrategyTreeDTO dto){
        if(dto.getChild().getId() != null)
            s.setChild(service.getReference(dto.getChild().getId()));
    }
}
