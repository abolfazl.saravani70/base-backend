package com.rbp.sayban.model.mapper.objectives;

import com.rbp.sayban.model.domainmodel.objectives.ObjectiveKpi;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveKpiViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveKpiMapper extends GenericMapper<ObjectiveKpiViewModel, ObjectiveKpi> {
    ObjectiveKpiMapper INSTANCE = Mappers.getMapper(ObjectiveKpiMapper.class);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "kpi.id", target = "kpi")
    ObjectiveKpi toDomainModel(ObjectiveKpiViewModel objectiveKpiViewModel);
}
