/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.mapper.system;

import com.rbp.sayban.model.domainmodel.system.MonitorConfig;
import com.rbp.sayban.model.dto.system.MonitorConfigDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface MonitorConfigMapper extends GenericMapper<MonitorConfigDTO, MonitorConfig> {

    MonitorConfigMapper INSTANCE = Mappers.getMapper(MonitorConfigMapper.class);

    @Mapping(source = "application", target = "applicationId")
    @Mapping(source = "enumValue", target = "enumValueId")
    MonitorConfigDTO toView(MonitorConfig MonitorConfig);

    @Mapping(source = "applicationId", target = "application")
    @Mapping(source = "enumValueId", target = "enumValue")
    MonitorConfig toDomainModel(MonitorConfigDTO MonitorConfigDTO);

}