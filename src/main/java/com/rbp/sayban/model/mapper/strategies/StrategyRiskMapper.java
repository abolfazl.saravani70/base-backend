package com.rbp.sayban.model.mapper.strategies;


import com.rbp.sayban.model.domainmodel.strategies.StrategyRisk;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.strategies.StrategyRiskViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyRiskMapper extends GenericMapper<StrategyRiskViewModel, StrategyRisk> {
    StrategyRiskMapper INSTANCE = Mappers.getMapper(StrategyRiskMapper.class);

    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "risk.id", target = "risk")
    StrategyRisk toDomainModel(StrategyRiskViewModel strategyRiskViewModel);
}
