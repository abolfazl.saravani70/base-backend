package com.rbp.sayban.model.mapper.strategies;

import com.rbp.sayban.model.domainmodel.strategies.StrategyHR;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.strategies.StrategyHRViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface StrategyHRMapper  extends GenericMapper<StrategyHRViewModel, StrategyHR> {
    StrategyHRMapper INSTANCE = Mappers.getMapper(StrategyHRMapper.class);

    @Mapping(source = "strategy.id", target = "strategy")
    @Mapping(source = "humanResource.id", target = "humanResource")
    StrategyHR toDomainModel(StrategyHRViewModel strategyHRViewModel);
}
