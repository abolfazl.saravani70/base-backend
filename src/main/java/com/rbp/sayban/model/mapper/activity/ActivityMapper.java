package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.Activity;
import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityMapper extends GenericMapper<ActivityDTO, Activity> {
    ActivityMapper INSTANCE= Mappers.getMapper(ActivityMapper.class);



    ActivityDTO toView(Activity activity);


    Activity toDomainModel(ActivityDTO activityDTO );
}
