package com.rbp.sayban.model.mapper.objectives;

import com.rbp.sayban.model.domainmodel.objectives.ObjectiveLocation;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveLocationViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveLocationMapper extends GenericMapper<ObjectiveLocationViewModel, ObjectiveLocation> {
    ObjectiveLocationMapper INSTANCE = Mappers.getMapper(ObjectiveLocationMapper.class);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "location.id", target = "location")
    ObjectiveLocation toDomainModel(ObjectiveLocationViewModel objectiveLocationViewModel);
}
