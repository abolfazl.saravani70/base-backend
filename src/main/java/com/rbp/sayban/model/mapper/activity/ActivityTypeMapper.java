package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityType;
import com.rbp.sayban.model.dto.activity.ActivityTypeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityTypeMapper extends GenericMapper<ActivityTypeDTO, ActivityType> {
    ActivityTypeMapper INSTANCE= Mappers.getMapper(ActivityTypeMapper.class);
}
