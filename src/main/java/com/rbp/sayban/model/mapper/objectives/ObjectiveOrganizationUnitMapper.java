package com.rbp.sayban.model.mapper.objectives;


import com.rbp.sayban.model.domainmodel.objectives.ObjectiveOrganizationUnit;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveOrganizationUnitViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;



@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ObjectiveOrganizationUnitMapper extends GenericMapper<ObjectiveOrganizationUnitViewModel, ObjectiveOrganizationUnit> {
    ObjectiveOrganizationUnitMapper INSTANCE = Mappers.getMapper(ObjectiveOrganizationUnitMapper.class);

    @Mapping(source = "objective.id", target = "objective")
    @Mapping(source = "organizationUnit.id", target = "orgUnitId")
    ObjectiveOrganizationUnit toDomainModel(ObjectiveOrganizationUnitViewModel objectiveORganizationUnitViewModel);

    @Mapping(source = "pvTypeId", target = "pvTypeTitle")
    @Mapping(source = "pvStateId", target = "pvStateTitle")
    ObjectiveOrganizationUnitViewModel toView(ObjectiveOrganizationUnit objectiveOrganizationUnit);
}
