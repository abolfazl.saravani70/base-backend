package com.rbp.sayban.model.mapper.activity;

import com.rbp.sayban.model.domainmodel.activity.ActivityNormCost;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import com.rbp.sayban.model.viewModel.activity.ActivityNormCostViewModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface ActivityNormCostMapper extends GenericMapper<ActivityNormCostViewModel, ActivityNormCost> {
    ActivityNormCostMapper INSTANCE= Mappers.getMapper(ActivityNormCostMapper.class);

    @Mapping(source = "activity.id", target = "activity")
    @Mapping(source = "normCost.id", target = "normCost")
    ActivityNormCost toDomainModel(ActivityNormCostViewModel activityNormCostViewModel);

}
