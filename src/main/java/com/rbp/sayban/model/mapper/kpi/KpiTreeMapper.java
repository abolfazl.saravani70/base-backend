package com.rbp.sayban.model.mapper.kpi;


import com.rbp.sayban.model.domainmodel.kpi.KpiTree;
import com.rbp.sayban.model.dto.kpi.KpiTreeDTO;
import com.rbp.sayban.model.mapper.BaseEntityMapper;
import com.rbp.sayban.model.mapper.BaseMapperMethods;
import com.rbp.sayban.model.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(config = BaseEntityMapper.class, uses = BaseMapperMethods.class)
public interface KpiTreeMapper extends GenericMapper<KpiTreeDTO, KpiTree> {

    KpiTreeMapper INSTANCE= Mappers.getMapper(KpiTreeMapper.class);


    KpiTreeDTO toView(KpiTree kpiTree);

    KpiTree toDomainModel(KpiTreeDTO kpiTreeDTO);

}