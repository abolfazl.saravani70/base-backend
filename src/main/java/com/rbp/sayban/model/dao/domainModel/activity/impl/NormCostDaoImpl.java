package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.INormCostDao;
import com.rbp.sayban.model.domainmodel.norm.NormCost;
import org.springframework.stereotype.Repository;

@Repository
public class NormCostDaoImpl extends GenericRepository<NormCost> implements INormCostDao {
    @Override
    protected Class<NormCost> getDomainClass() {
        return NormCost.class;
    }
}
