package com.rbp.sayban.model.dao.domainModel.planning.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanLocationDao;
import com.rbp.sayban.model.domainmodel.planning.PlanLocation;
import org.springframework.stereotype.Repository;

@Repository
public class PlanLocationDaoImpl extends GenericRepository<PlanLocation> implements IPlanLocationDao {
    @Override
    protected Class<PlanLocation> getDomainClass() {
        return PlanLocation.class;
    }
}
