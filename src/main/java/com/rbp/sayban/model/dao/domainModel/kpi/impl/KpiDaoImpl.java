/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.kpi.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.kpi.IKpiDao;
import com.rbp.sayban.model.domainmodel.kpi.Kpi;
import org.springframework.stereotype.Repository;

@Repository
public class KpiDaoImpl extends GenericRepository<Kpi> implements IKpiDao {


    @Override
    protected Class<Kpi> getDomainClass() {
        return Kpi.class;
    }
}
