package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityExpenditureDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityExpenditure;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityExpenditureDaoImpl extends GenericRepository<ActivityExpenditure> implements IActivityExpenditureDao {
    @Override
    protected Class<ActivityExpenditure> getDomainClass() {
        return ActivityExpenditure.class;
    }
}
