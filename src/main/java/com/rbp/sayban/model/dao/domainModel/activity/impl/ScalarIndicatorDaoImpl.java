package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IScalarIndicatorDao;
import com.rbp.sayban.model.domainmodel.dep.ScalarIndicator;
import org.springframework.stereotype.Repository;

@Repository
public class ScalarIndicatorDaoImpl extends GenericRepository<ScalarIndicator> implements IScalarIndicatorDao {
    @Override
    protected Class<ScalarIndicator> getDomainClass() {
        return ScalarIndicator.class;
    }
}
