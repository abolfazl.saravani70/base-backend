package com.rbp.sayban.model.dao.domainModel.planning.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanHRDao;
import com.rbp.sayban.model.domainmodel.planning.PlanHR;
import org.springframework.stereotype.Repository;

@Repository
public class PlanHRDaoImpl extends GenericRepository<PlanHR> implements IPlanHRDao {
    @Override
    protected Class<PlanHR> getDomainClass() {
        return PlanHR.class;
    }
}
