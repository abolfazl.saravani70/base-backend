/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.project.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.project.IProjectAllocationDao;
import com.rbp.sayban.model.domainmodel.project.ProjectAllocation;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectAllocationDaoImpl extends GenericRepository<ProjectAllocation> implements IProjectAllocationDao {
    @Override
    protected Class<ProjectAllocation> getDomainClass() {
        return ProjectAllocation.class;
    }
}
