package com.rbp.sayban.model.dao.domainModel.organization.impl;

import com.rbp.core.model.dao.base.impl.GenericTreeRepository;
import com.rbp.sayban.model.dao.domainModel.organization.IOrgTreeDao;
import com.rbp.sayban.model.domainmodel.organization.OrgTree;
import org.springframework.stereotype.Repository;

@Repository
public class OrgTreeDaoImpl extends GenericTreeRepository<OrgTree> implements IOrgTreeDao {
    @Override
    protected Class<OrgTree> getDomainClass() {
        return OrgTree.class;
    }
}
