package com.rbp.sayban.model.dao.domainModel.project.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.project.IProjectSubjectiveDao;
import com.rbp.sayban.model.domainmodel.project.ProjectSubjective;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectSubjectiveDaoImpl extends GenericRepository<ProjectSubjective> implements IProjectSubjectiveDao {
    @Override
    protected Class<ProjectSubjective> getDomainClass() {
        return ProjectSubjective.class;
    }

}
