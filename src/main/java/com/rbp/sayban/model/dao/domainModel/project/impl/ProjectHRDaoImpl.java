package com.rbp.sayban.model.dao.domainModel.project.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.project.IProjectHRDao;
import com.rbp.sayban.model.domainmodel.project.ProjectHR;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectHRDaoImpl extends GenericRepository<ProjectHR> implements IProjectHRDao {
    @Override
    protected Class<ProjectHR> getDomainClass() {
        return ProjectHR.class;
    }
}
