package com.rbp.sayban.model.dao.domainModel.task;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.task.TaskLocation;

public interface ITaskLocationDao  extends IGenericRepository<TaskLocation> {
}
