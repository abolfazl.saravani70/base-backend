/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */
package com.rbp.sayban.model.dao.domainModel.system.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.system.IBlobDao;
import com.rbp.sayban.model.domainmodel.system.Blob;
import org.springframework.session.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class BlobDaoImpl extends GenericRepository<Blob> implements IBlobDao {
    @Override
    protected Class<Blob> getDomainClass() {
        return Blob.class;
    }

}
