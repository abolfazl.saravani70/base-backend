package com.rbp.sayban.model.dao.domainModel.activity;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.activity.ActivityNormCost;

public interface IActivityNormCostDao extends IGenericRepository<ActivityNormCost> {
}
