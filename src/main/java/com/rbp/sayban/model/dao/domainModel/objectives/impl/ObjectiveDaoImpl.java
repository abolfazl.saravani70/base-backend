/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.objectives.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveDao;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import org.springframework.stereotype.Repository;

@Repository
public class ObjectiveDaoImpl extends GenericRepository<Objective> implements IObjectiveDao {


    @Override
    protected Class<Objective> getDomainClass() {
        return Objective.class;
    }
}
