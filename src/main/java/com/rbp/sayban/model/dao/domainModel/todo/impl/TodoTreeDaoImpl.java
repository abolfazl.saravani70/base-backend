/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.todo.impl;

import com.rbp.core.model.dao.base.impl.GenericRootRepository;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoTreeDao;
import com.rbp.sayban.model.domainmodel.todo.TodoTree;
import org.springframework.stereotype.Repository;

@Repository
public class TodoTreeDaoImpl extends GenericRootRepository<TodoTree> implements ITodoTreeDao {
    @Override
    protected Class<TodoTree> getDomainClass() {
        return TodoTree.class;
    }
}
