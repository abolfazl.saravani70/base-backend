package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityLocationDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityLocation;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityLocationDaoImpl  extends GenericRepository<ActivityLocation> implements IActivityLocationDao {
    @Override
    protected Class<ActivityLocation> getDomainClass() {
        return ActivityLocation.class;
    }
}
