/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */


package com.rbp.sayban.model.dao.domainModel.report.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.report.IReportTypeDao;
import com.rbp.sayban.model.domainmodel.report.ReportType;
import org.springframework.stereotype.Repository;

@Repository
public class ReportTypeDaoImpl extends GenericRepository<ReportType> implements IReportTypeDao {
    @Override
    protected Class<ReportType> getDomainClass() {
        return ReportType.class;
    }
}