/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.strategies.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyExecutionTypeDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyExecutionType;
import org.springframework.stereotype.Repository;

@Repository
public class StrategyExecutionTypeDaoImpl extends GenericRepository<StrategyExecutionType> implements IStrategyExecutionTypeDao {
    @Override
    protected Class<StrategyExecutionType> getDomainClass() {
        return StrategyExecutionType.class;
    }
}

