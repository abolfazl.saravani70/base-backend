/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.system.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.system.IProcessStepSysRuleDao;
import com.rbp.sayban.model.domainmodel.system.ProcessStepSysRule;
import org.springframework.stereotype.Repository;

@Repository
public class ProcessStepSysRuleDaoImpl extends GenericRepository<ProcessStepSysRule> implements IProcessStepSysRuleDao {
    @Override
    protected Class<ProcessStepSysRule> getDomainClass() {
        return ProcessStepSysRule.class;
    }
}
