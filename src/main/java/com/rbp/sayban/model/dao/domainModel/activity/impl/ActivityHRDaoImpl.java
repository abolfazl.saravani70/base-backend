package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityBudgetDao;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityHRDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityBudget;
import com.rbp.sayban.model.domainmodel.activity.ActivityHR;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityHRDaoImpl  extends GenericRepository<ActivityHR> implements IActivityHRDao {
    @Override
    protected Class<ActivityHR> getDomainClass() {
        return ActivityHR.class;
    }
}
