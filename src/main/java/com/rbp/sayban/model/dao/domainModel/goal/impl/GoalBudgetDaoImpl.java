package com.rbp.sayban.model.dao.domainModel.goal.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalBudgetDao;
import com.rbp.sayban.model.domainmodel.goal.GoalBudget;
import org.springframework.stereotype.Repository;

@Repository
public class GoalBudgetDaoImpl extends GenericRepository<GoalBudget> implements IGoalBudgetDao {


    @Override
    protected Class<GoalBudget> getDomainClass() {
        return GoalBudget.class;
    }
}
