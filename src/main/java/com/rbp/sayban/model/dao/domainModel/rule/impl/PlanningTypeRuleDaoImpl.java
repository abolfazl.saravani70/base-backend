/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.rule.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.rule.IPlanningTypeRuleDao;
import com.rbp.sayban.model.domainmodel.rule.PlanningTypeRule;
import org.springframework.stereotype.Repository;

@Repository
public class PlanningTypeRuleDaoImpl extends GenericRepository<PlanningTypeRule> implements IPlanningTypeRuleDao {

    @Override
    protected Class<PlanningTypeRule> getDomainClass() {
        return PlanningTypeRule.class;
    }
}
