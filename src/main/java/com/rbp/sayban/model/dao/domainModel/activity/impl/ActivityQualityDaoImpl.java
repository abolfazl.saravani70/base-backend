package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityQualityDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityQuality;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityQualityDaoImpl extends GenericRepository<ActivityQuality> implements IActivityQualityDao {


    @Override
    protected Class<ActivityQuality> getDomainClass() {
        return ActivityQuality.class;
    }
}

