package com.rbp.sayban.model.dao.domainModel.task.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.task.ITaskLocationDao;
import com.rbp.sayban.model.domainmodel.task.TaskLocation;
import org.springframework.stereotype.Repository;

@Repository
public class TaskLocationDaoImpl  extends GenericRepository<TaskLocation> implements ITaskLocationDao {


    @Override
    protected Class<TaskLocation> getDomainClass() {
        return TaskLocation.class;
    }
}
