/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.organization;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.viewModel.basicinformation.bankingInfo.AccountViewModel;
import com.rbp.sayban.model.domainmodel.organization.OrganizationAccount;

import java.util.List;

public interface IOrganizationAccountDao extends IGenericRepository<OrganizationAccount> {

    List<Object[]> insertNewAccount(AccountViewModel entity);
    List<Object[]> getAccountsDetail(Long orgId);
    List<OrganizationAccount> findByOrgId(Long orgId);
}