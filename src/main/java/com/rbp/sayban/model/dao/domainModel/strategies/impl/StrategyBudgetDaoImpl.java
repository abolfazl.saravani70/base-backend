package com.rbp.sayban.model.dao.domainModel.strategies.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyBudgetDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyBudget;
import org.springframework.stereotype.Repository;

@Repository
public class StrategyBudgetDaoImpl extends GenericRepository<StrategyBudget> implements IStrategyBudgetDao {

    @Override
    protected Class<StrategyBudget> getDomainClass() {
        return StrategyBudget.class;
    }
}
