/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.report.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.report.IReportFilterUserDao;
import com.rbp.sayban.model.domainmodel.report.ReportFilterUser;
import org.springframework.stereotype.Repository;

@Repository
public class ReportFilterUserDaoImpl extends GenericRepository<ReportFilterUser> implements IReportFilterUserDao {
    @Override
    protected Class<ReportFilterUser> getDomainClass() {
        return ReportFilterUser.class;
    }
}