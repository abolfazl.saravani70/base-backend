package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActSharingDetailDao;
import com.rbp.sayban.model.domainmodel.dep.ActSharingDetail;
import org.springframework.stereotype.Repository;

@Repository
public class ActSharingDetailDaoImpl extends GenericRepository<ActSharingDetail>implements IActSharingDetailDao {
    @Override
    protected Class<ActSharingDetail> getDomainClass() {
        return ActSharingDetail.class;
    }
}
