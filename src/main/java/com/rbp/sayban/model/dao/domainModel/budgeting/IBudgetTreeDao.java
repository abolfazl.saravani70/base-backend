package com.rbp.sayban.model.dao.domainModel.budgeting;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.sayban.model.domainmodel.budgeting.BudgetTree;

public interface IBudgetTreeDao extends IGenericTreeRepository<BudgetTree> {
}