/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */
package com.rbp.sayban.model.dao.domainModel.strategies;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.strategies.StrategyType;

public interface IStrategyTypeDao extends IGenericRepository<StrategyType> {
}