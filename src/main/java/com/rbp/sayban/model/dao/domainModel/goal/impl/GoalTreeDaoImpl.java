/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.goal.impl;

import com.rbp.core.model.dao.base.impl.GenericRootRepository;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalTreeDao;
import com.rbp.sayban.model.domainmodel.goal.GoalTree;
import org.springframework.stereotype.Repository;

@Repository
public class GoalTreeDaoImpl extends GenericRootRepository<GoalTree> implements IGoalTreeDao {
    @Override
    protected Class<GoalTree> getDomainClass() {
        return GoalTree.class;
    }
}
