/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.system.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.system.IDocumentDao;
import com.rbp.sayban.model.domainmodel.system.Document;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentDaoImpl extends GenericRepository<Document> implements IDocumentDao {
    @Override
    protected Class<Document> getDomainClass() {
        return Document.class;
    }
}