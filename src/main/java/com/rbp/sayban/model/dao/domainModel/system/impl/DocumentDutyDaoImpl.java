/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.system.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.system.IDocumentDutyDao;
import com.rbp.sayban.model.domainmodel.system.DocumentDuty;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentDutyDaoImpl extends GenericRepository<DocumentDuty> implements IDocumentDutyDao {
    @Override
    protected Class<DocumentDuty> getDomainClass() {
        return DocumentDuty.class;
    }
}
