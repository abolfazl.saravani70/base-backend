package com.rbp.sayban.model.dao.domainModel.strategies.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyHRDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyHR;
import org.springframework.stereotype.Repository;

@Repository
public class StrategyHRDaoImpl extends GenericRepository<StrategyHR> implements IStrategyHRDao {

    @Override
    protected Class<StrategyHR> getDomainClass() {
        return StrategyHR.class;
    }
}
