/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.task.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.task.ITaskOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.task.TaskOrganizationUnit;
import org.springframework.stereotype.Repository;

@Repository
public class TaskOrganizationUnitDaoImpl extends GenericRepository<TaskOrganizationUnit> implements ITaskOrganizationUnitDao {


@Override
protected Class<TaskOrganizationUnit> getDomainClass() {
        return TaskOrganizationUnit.class;
    }


            }
