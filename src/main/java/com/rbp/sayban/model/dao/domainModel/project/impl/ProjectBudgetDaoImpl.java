package com.rbp.sayban.model.dao.domainModel.project.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.project.IProjectBudgetDao;
import com.rbp.sayban.model.domainmodel.project.ProjectBudget;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectBudgetDaoImpl extends GenericRepository<ProjectBudget> implements IProjectBudgetDao {
    @Override
    protected Class<ProjectBudget> getDomainClass() {
        return ProjectBudget.class;
    }
}
