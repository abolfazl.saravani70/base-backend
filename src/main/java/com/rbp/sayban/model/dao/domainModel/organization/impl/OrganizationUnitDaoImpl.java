/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.organization.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.domainmodel.search.AdvancedSearchResult;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OrganizationUnitDaoImpl extends GenericRepository<OrganizationUnit> implements IOrganizationUnitDao {
@Override
protected Class<OrganizationUnit> getDomainClass() {
        return OrganizationUnit.class;
    }

//    @Override
//    public List<OrganizationUnit> findByOrgChartId(Long chartId, int pageNumber, int pageSize) {
//
//        List<OrganizationUnit> ou = getEntityManager().createQuery("FROM " + getDomainClass().getSimpleName() +
//                " WHERE organizationChart = " + chartId)
//                .setFirstResult((pageNumber - 1) * pageSize)
//                .setMaxResults(pageSize)
//                .getResultList();
//
//        return ou;
//    }

//    @Override
//    public List<OrganizationUnit> findByOrgId(Long orgId, int pageNumber, int pageSize) {
//        AdvancedSearchResult searchResult=searchEntity("organization.id="+orgId,"",pageNumber,pageSize,false);
//        List<OrganizationUnit> ou = searchResult.getRecords();
//
//        return ou;
//    }


    /**
     * returns a path from the child to the root.
     * @param child
     * @return
     */
//    @Override
//    public List<OrganizationUnit> findOrgPathToParent(OrganizationUnit child) {
//        List<OrganizationUnit> result=new ArrayList<>();
//        result.add(child);
//        while(true){
//                OrganizationUnit temp=result.get(result.size()-1);
//                if(temp.getId().equals(1L)) break;
//                result.add(temp);
//        }
//
//        return result;
//    }

}
