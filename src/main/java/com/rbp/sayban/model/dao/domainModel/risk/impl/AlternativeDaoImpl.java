/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.risk.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.risk.IAlternativeDao;
import com.rbp.sayban.model.domainmodel.risk.Alternative;
import org.springframework.stereotype.Repository;

@Repository
public class AlternativeDaoImpl extends GenericRepository<Alternative> implements IAlternativeDao {


    @Override
    protected Class<Alternative> getDomainClass() {
        return Alternative.class;
    }
}
