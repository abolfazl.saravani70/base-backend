package com.rbp.sayban.model.dao.domainModel.task.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.task.ITaskSubjectiveDao;
import com.rbp.sayban.model.domainmodel.task.TaskSubjective;
import org.springframework.stereotype.Repository;

@Repository
public class TaskSubjectiveDaoImpl extends GenericRepository<TaskSubjective> implements ITaskSubjectiveDao {


    @Override
    protected Class<TaskSubjective> getDomainClass() {
        return TaskSubjective.class;
    }
}
