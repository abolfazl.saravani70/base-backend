package com.rbp.sayban.model.dao.domainModel.organization;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;

import java.util.List;

public interface IHumanResourceDao extends IGenericRepository<HumanResource> {

}
