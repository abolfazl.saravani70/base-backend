package com.rbp.sayban.model.dao.domainModel.project;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.project.ProjectSubjective;

public interface IProjectSubjectiveDao extends IGenericRepository<ProjectSubjective> {
}
