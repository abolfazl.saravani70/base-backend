/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.application.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.application.IAppSchedulingDao;
import com.rbp.sayban.model.domainmodel.application.AppScheduling;
import org.springframework.stereotype.Repository;

@Repository
public class AppSchedulingDaoImpl extends GenericRepository<AppScheduling> implements IAppSchedulingDao {
    @Override
    protected Class<AppScheduling> getDomainClass() {
        return AppScheduling.class;
    }
}

