/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.planning.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanTypeDao;
import com.rbp.sayban.model.domainmodel.planning.PlanType;
import org.springframework.stereotype.Repository;

@Repository
public class PlanTypeDaoImpl extends GenericRepository<PlanType> implements IPlanTypeDao {
    @Override
    protected Class<PlanType> getDomainClass() {
        return PlanType.class;
    }
}
