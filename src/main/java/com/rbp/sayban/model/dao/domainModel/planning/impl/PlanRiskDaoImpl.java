/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.planning.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanRiskDao;
import com.rbp.sayban.model.domainmodel.planning.PlanRisk;
import org.springframework.stereotype.Repository;

@Repository
public class PlanRiskDaoImpl extends GenericRepository<PlanRisk> implements IPlanRiskDao {
    @Override
    protected Class<PlanRisk> getDomainClass() {
        return PlanRisk.class;
    }

}
