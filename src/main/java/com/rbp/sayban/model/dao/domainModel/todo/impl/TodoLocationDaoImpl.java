package com.rbp.sayban.model.dao.domainModel.todo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoLocationDao;
import com.rbp.sayban.model.domainmodel.todo.TodoLocation;
import org.springframework.stereotype.Repository;

@Repository
public class TodoLocationDaoImpl  extends GenericRepository<TodoLocation> implements ITodoLocationDao {


    @Override
    protected Class<TodoLocation> getDomainClass() {
        return TodoLocation.class;
    }
}
