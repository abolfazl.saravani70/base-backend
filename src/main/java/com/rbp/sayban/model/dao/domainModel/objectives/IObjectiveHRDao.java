package com.rbp.sayban.model.dao.domainModel.objectives;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveHR;

public interface IObjectiveHRDao extends IGenericRepository<ObjectiveHR> {
}
