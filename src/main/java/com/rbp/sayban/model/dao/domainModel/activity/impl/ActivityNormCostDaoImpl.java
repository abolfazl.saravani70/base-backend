package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityNormCostDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityNormCost;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityNormCostDaoImpl extends GenericRepository<ActivityNormCost>implements IActivityNormCostDao {
    @Override
    protected Class<ActivityNormCost> getDomainClass() {
        return ActivityNormCost.class;
    }
}
