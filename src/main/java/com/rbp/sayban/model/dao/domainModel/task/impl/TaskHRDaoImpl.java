package com.rbp.sayban.model.dao.domainModel.task.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.task.ITaskHRDao;
import com.rbp.sayban.model.dao.domainModel.task.ITaskLocationDao;
import com.rbp.sayban.model.domainmodel.task.TaskHR;
import com.rbp.sayban.model.domainmodel.task.TaskLocation;
import org.springframework.stereotype.Repository;

@Repository
public class TaskHRDaoImpl extends GenericRepository<TaskHR> implements ITaskHRDao {


    @Override
    protected Class<TaskHR> getDomainClass() {
        return TaskHR.class;
    }
}
