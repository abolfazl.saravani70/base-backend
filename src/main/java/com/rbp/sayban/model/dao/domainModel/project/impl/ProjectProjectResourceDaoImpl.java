package com.rbp.sayban.model.dao.domainModel.project.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.project.IProjectProjectResourceDao;
import com.rbp.sayban.model.domainmodel.project.ProjectProjectResource;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectProjectResourceDaoImpl extends GenericRepository<ProjectProjectResource> implements IProjectProjectResourceDao {

    @Override
    protected Class<ProjectProjectResource> getDomainClass() {
        return ProjectProjectResource.class;
    }

}
