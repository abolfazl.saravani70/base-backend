package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityOrganizationUnitDaoImpl extends GenericRepository<ActivityOrganizationUnit
        > implements IActivityOrganizationUnitDao {
    @Override
    protected Class<ActivityOrganizationUnit> getDomainClass() {
        return ActivityOrganizationUnit.class;
    }
}
