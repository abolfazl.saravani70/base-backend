package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IQulitiveIndicatorDao;
import com.rbp.sayban.model.domainmodel.dep.QualitativeIndicator;
import org.springframework.stereotype.Repository;

@Repository
public class QulitiveIndicatorDaoImpl extends GenericRepository<QualitativeIndicator> implements IQulitiveIndicatorDao {
    @Override
    protected Class<QualitativeIndicator> getDomainClass() {
        return QualitativeIndicator.class;
    }
}
