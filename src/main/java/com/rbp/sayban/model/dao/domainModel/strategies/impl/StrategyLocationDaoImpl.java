package com.rbp.sayban.model.dao.domainModel.strategies.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyLocationDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyLocation;
import org.springframework.stereotype.Repository;

@Repository
public class StrategyLocationDaoImpl extends GenericRepository<StrategyLocation> implements IStrategyLocationDao {

    @Override
    protected Class<StrategyLocation> getDomainClass() {
        return StrategyLocation.class;
    }
}
