/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.task.impl;

import com.rbp.core.model.dao.base.impl.GenericRootRepository;
import com.rbp.sayban.model.dao.domainModel.task.ITaskTreeDao;
import com.rbp.sayban.model.domainmodel.task.TaskTree;
import org.springframework.stereotype.Repository;

@Repository
public class TaskTreeDaoImpl extends GenericRootRepository<TaskTree>
        implements ITaskTreeDao {
    @Override
    protected Class<TaskTree> getDomainClass() {
        return TaskTree.class;
    }
}
