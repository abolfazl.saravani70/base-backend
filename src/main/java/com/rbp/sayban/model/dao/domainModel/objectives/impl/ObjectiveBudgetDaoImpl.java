package com.rbp.sayban.model.dao.domainModel.objectives.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveBudgetDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveBudget;
import org.springframework.stereotype.Repository;

@Repository
public class ObjectiveBudgetDaoImpl extends GenericRepository<ObjectiveBudget> implements IObjectiveBudgetDao {


    @Override
    protected Class<ObjectiveBudget> getDomainClass() {
        return ObjectiveBudget.class;
    }
}
