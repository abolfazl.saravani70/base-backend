package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.INormCostOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.dep.NormCostOrganizationUnit;
import org.springframework.stereotype.Repository;

@Repository
public class NormCostOrganizationUnitDaoImpl extends GenericRepository<NormCostOrganizationUnit>implements INormCostOrganizationUnitDao {
    @Override
    protected Class<NormCostOrganizationUnit> getDomainClass() {
        return NormCostOrganizationUnit.class;
    }
}
