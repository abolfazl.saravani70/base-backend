package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTemplateTypeDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateType;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityTemplateTypeDaoImpl extends GenericRepository<ActivityTemplateType>implements IActivityTemplateTypeDao {
    @Override
    protected Class<ActivityTemplateType> getDomainClass() {
        return ActivityTemplateType.class;
    }
}
