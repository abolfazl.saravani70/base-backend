package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTypeDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityType;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityTypeDaoImpl extends GenericRepository<ActivityType> implements IActivityTypeDao {
    @Override
    protected Class<ActivityType> getDomainClass() {
        return ActivityType.class;
    }
}
