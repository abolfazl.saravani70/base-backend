package com.rbp.sayban.model.dao.domainModel.kpi;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.sayban.model.domainmodel.kpi.KpiTree;

public interface IKpiTreeDao extends IGenericTreeRepository<KpiTree> {
}