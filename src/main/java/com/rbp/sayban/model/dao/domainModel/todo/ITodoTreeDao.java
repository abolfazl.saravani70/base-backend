/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.todo;

import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.sayban.model.domainmodel.todo.TodoTree;

public interface ITodoTreeDao extends IGenericRootRepository<TodoTree> {
}
