package com.rbp.sayban.model.dao.domainModel.todo;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.todo.TodoBudget;

public interface ITodoBudgetDao extends IGenericRepository<TodoBudget> {
}
