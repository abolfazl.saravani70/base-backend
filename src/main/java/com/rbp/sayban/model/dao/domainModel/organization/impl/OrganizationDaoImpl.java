/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.organization.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationDao;
import com.rbp.sayban.model.domainmodel.organization.Organization;
import org.springframework.stereotype.Repository;

@Repository
public class OrganizationDaoImpl  extends GenericRepository<Organization> implements IOrganizationDao {
    @Override
    protected Class<Organization> getDomainClass() {
        return Organization.class;
    }

}
