/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRootRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTreeDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityTree;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityTreeDaoImpl extends GenericRootRepository<ActivityTree>
        implements IActivityTreeDao {
    @Override
    protected Class<ActivityTree> getDomainClass() {
        return ActivityTree.class;
    }
}
