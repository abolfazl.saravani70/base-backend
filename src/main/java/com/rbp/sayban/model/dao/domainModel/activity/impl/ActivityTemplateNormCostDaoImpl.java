package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTemplateNormCostDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateNormCost;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityTemplateNormCostDaoImpl extends GenericRepository<ActivityTemplateNormCost>implements IActivityTemplateNormCostDao {

    @Override
    protected Class<ActivityTemplateNormCost> getDomainClass() {
        return ActivityTemplateNormCost.class;
    }
}
