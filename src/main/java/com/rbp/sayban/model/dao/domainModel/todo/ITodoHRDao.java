package com.rbp.sayban.model.dao.domainModel.todo;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.todo.TodoHR;

public interface ITodoHRDao extends IGenericRepository<TodoHR> {
}
