package com.rbp.sayban.model.dao.domainModel.objectives.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveLocationDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveLocation;
import org.springframework.stereotype.Repository;

@Repository
public class ObjectiveLocationDaoImpl extends GenericRepository<ObjectiveLocation> implements IObjectiveLocationDao {


    @Override
    protected Class<ObjectiveLocation> getDomainClass() {
        return ObjectiveLocation.class;
    }
}
