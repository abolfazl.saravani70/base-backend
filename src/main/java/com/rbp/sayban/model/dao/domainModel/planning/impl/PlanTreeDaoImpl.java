/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.planning.impl;

import com.rbp.core.model.dao.base.impl.GenericRootRepository;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanTreeDao;
import com.rbp.sayban.model.domainmodel.planning.PlanTree;
import org.springframework.stereotype.Repository;

@Repository
public class PlanTreeDaoImpl extends GenericRootRepository<PlanTree> implements IPlanTreeDao {
    @Override
    protected Class<PlanTree> getDomainClass() {
        return PlanTree.class;
    }
}
