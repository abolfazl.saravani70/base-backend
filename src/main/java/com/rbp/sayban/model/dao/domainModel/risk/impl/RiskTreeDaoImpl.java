package com.rbp.sayban.model.dao.domainModel.risk.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.base.impl.GenericTreeRepository;
import com.rbp.sayban.model.dao.domainModel.risk.IRiskTreeDao;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;
import org.springframework.stereotype.Repository;

@Repository
public class RiskTreeDaoImpl extends GenericTreeRepository<RiskTree> implements IRiskTreeDao {

    @Override
    protected Class<RiskTree> getDomainClass() {
        return RiskTree.class;
    } 
}