package com.rbp.sayban.model.dao.domainModel.todo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoBudgetDao;
import com.rbp.sayban.model.domainmodel.todo.TodoBudget;
import org.springframework.stereotype.Repository;

@Repository
public class TodoBudgetDaoImpl extends GenericRepository<TodoBudget> implements ITodoBudgetDao {


    @Override
    protected Class<TodoBudget> getDomainClass() {
        return TodoBudget.class;
    }
}
