package com.rbp.sayban.model.dao.domainModel.organization;

import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.sayban.model.domainmodel.organization.OrgTree;

public interface IOrgTreeDao extends IGenericTreeRepository<OrgTree> {
}
