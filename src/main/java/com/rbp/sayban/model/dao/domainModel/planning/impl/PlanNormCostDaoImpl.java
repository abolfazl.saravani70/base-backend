/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.planning.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanNormCostDao;
import com.rbp.sayban.model.domainmodel.planning.PlanNormCost;
import org.springframework.stereotype.Repository;

@Repository
public class PlanNormCostDaoImpl extends GenericRepository<PlanNormCost> implements IPlanNormCostDao {
    @Override
    protected Class<PlanNormCost> getDomainClass() {
        return PlanNormCost.class;
    }


}
