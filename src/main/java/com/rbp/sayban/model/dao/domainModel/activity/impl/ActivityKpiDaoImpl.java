package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityKpiDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityKpi;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityKpiDaoImpl extends GenericRepository<ActivityKpi> implements IActivityKpiDao {
    @Override
    protected Class<ActivityKpi> getDomainClass() {
        return ActivityKpi.class;
    }
}
