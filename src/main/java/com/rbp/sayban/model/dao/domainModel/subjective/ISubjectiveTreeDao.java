package com.rbp.sayban.model.dao.domainModel.subjective;


import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;
import com.rbp.sayban.model.domainmodel.subjective.SubjectiveTree;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;
import sun.reflect.generics.tree.Tree;

public interface ISubjectiveTreeDao extends IGenericTreeRepository<SubjectiveTree> {
}