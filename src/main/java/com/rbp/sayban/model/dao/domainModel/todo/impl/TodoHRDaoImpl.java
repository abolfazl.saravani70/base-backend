package com.rbp.sayban.model.dao.domainModel.todo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoHRDao;
import com.rbp.sayban.model.domainmodel.todo.TodoHR;
import org.springframework.stereotype.Repository;

@Repository
public class TodoHRDaoImpl extends GenericRepository<TodoHR> implements ITodoHRDao {


    @Override
    protected Class<TodoHR> getDomainClass() {
        return TodoHR.class;
    }
}
