package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IPerformanceDao;
import com.rbp.sayban.model.domainmodel.dep.Performance;
import org.springframework.stereotype.Repository;

@Repository
public class PerformanceDaoImpl extends GenericRepository<Performance> implements IPerformanceDao {
    @Override
    protected Class<Performance> getDomainClass() {
        return Performance.class;
    }
}
