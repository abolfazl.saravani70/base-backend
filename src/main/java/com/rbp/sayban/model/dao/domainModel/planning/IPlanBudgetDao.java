package com.rbp.sayban.model.dao.domainModel.planning;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.planning.PlanBudget;

public interface IPlanBudgetDao extends IGenericRepository<PlanBudget> {
}
