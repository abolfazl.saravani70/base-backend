package com.rbp.sayban.model.dao.domainModel.activity;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.dep.NormCostOrganizationUnit;

public interface INormCostOrganizationUnitDao extends IGenericRepository<NormCostOrganizationUnit> {
}
