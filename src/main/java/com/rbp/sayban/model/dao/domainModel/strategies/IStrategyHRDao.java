package com.rbp.sayban.model.dao.domainModel.strategies;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.strategies.StrategyHR;

public interface IStrategyHRDao extends IGenericRepository<StrategyHR> {
}
