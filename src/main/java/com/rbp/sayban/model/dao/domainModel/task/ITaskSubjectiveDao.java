package com.rbp.sayban.model.dao.domainModel.task;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.task.TaskSubjective;

public interface ITaskSubjectiveDao extends IGenericRepository<TaskSubjective> {
}
