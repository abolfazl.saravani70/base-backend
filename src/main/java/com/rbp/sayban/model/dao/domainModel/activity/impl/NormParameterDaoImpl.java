package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.INormParameterDao;
import com.rbp.sayban.model.domainmodel.norm.NormParameter;
import org.springframework.stereotype.Repository;

@Repository
public class NormParameterDaoImpl extends GenericRepository<NormParameter>implements INormParameterDao {
    @Override
    protected Class<NormParameter> getDomainClass() {
        return NormParameter.class;
    }
}
