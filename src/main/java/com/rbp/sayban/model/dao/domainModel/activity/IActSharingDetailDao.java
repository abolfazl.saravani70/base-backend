package com.rbp.sayban.model.dao.domainModel.activity;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.dep.ActSharingDetail;

public interface IActSharingDetailDao extends IGenericRepository<ActSharingDetail> {
}
