/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.application.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.application.IAppEnumDao;
import com.rbp.sayban.model.domainmodel.application.AppEnum;
import org.springframework.stereotype.Repository;

@Repository
public class AppEnumDaoImpl extends GenericRepository<AppEnum> implements IAppEnumDao {
    @Override
    protected Class<AppEnum> getDomainClass() {
        return AppEnum.class;
    }
}
