package com.rbp.sayban.model.dao.domainModel.todo;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.todo.TodoSubjective;

public interface ITodoSubjectiveDao extends IGenericRepository<TodoSubjective> {
}
