package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IPerformanceIndicatorDao;
import com.rbp.sayban.model.domainmodel.dep.PerformanceIndicator;
import org.springframework.stereotype.Repository;

@Repository
public class PerformanceIndicatorDaoImpl extends GenericRepository<PerformanceIndicator> implements IPerformanceIndicatorDao {
    @Override
    protected Class<PerformanceIndicator> getDomainClass() {
        return PerformanceIndicator.class;
    }
}
