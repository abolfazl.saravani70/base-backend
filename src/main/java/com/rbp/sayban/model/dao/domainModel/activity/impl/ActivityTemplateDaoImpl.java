package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityTemplateDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityTemplateDaoImpl extends GenericRepository<ActivityTemplate> implements IActivityTemplateDao {
    @Override
    protected Class<ActivityTemplate> getDomainClass() {
        return ActivityTemplate.class;
    }
}
