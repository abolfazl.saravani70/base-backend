/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.organization.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.viewModel.basicinformation.bankingInfo.AccountViewModel;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationAccountDao;
import com.rbp.sayban.model.domainmodel.organization.OrganizationAccount;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrganizationAccountDaoImpl extends GenericRepository<OrganizationAccount> implements IOrganizationAccountDao {
    @Override
    protected Class<OrganizationAccount> getDomainClass() {
        return OrganizationAccount.class;
    }

    @Override
    public List<Object[]> insertNewAccount(AccountViewModel entity) {
        return null;

    }

    @Override
    public List<Object[]> getAccountsDetail(Long orgId) {

        List<Object[]> records = getEntityManager()
                .createQuery("SELECT acc.id as numberId, orgAcc.isMain as isMain, acc.number as number, " +
                "br.id as branchId, br.name as branchName, bnk.id as bankId, bnk.name as bankName, orgAcc.organization.id as orgId " +
                "FROM " + getDomainClass().getSimpleName() + " orgAcc " +
                "JOIN orgAcc.account acc "+
                "JOIN acc.branch br " +
                "JOIN br.bank bnk " +
                "WHERE orgAcc.organization = " + orgId
                 )
                .getResultList();

        return records;
    }

    @Override
    public List<OrganizationAccount> findByOrgId(Long orgId) {
        List<OrganizationAccount> organizationAccounts=getEntityManager()
                .createQuery("from "+getDomainClass().getSimpleName() +" where organization = " +orgId)
                .getResultList();
        return organizationAccounts;
    }
}
