package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityRiskDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityRisk;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityRiskDaoImpl extends GenericRepository<ActivityRisk>implements IActivityRiskDao {
    @Override
    protected Class<ActivityRisk> getDomainClass() {
        return ActivityRisk.class;
    }
}
