package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityAnalysisDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityAnalysis;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityAnalysisDaoImpl extends GenericRepository<ActivityAnalysis> implements IActivityAnalysisDao {


    @Override
    protected Class<ActivityAnalysis> getDomainClass() {
        return ActivityAnalysis.class;
    }
}
