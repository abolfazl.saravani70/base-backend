/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.application.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.application.IAppMessageTypeDao;
import com.rbp.sayban.model.domainmodel.application.AppMessageType;
import org.springframework.stereotype.Repository;

@Repository
public class AppMessageTypeDaoImpl extends GenericRepository<AppMessageType> implements IAppMessageTypeDao {

    @Override
    protected Class<AppMessageType> getDomainClass() {
        return AppMessageType.class;
    }
}
