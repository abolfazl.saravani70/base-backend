package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.INormTypeDao;
import com.rbp.sayban.model.domainmodel.norm.NormType;
import org.springframework.stereotype.Repository;

@Repository
public class NormTypeDaoImpl extends GenericRepository<NormType> implements INormTypeDao {
    @Override
    protected Class<NormType> getDomainClass() {
            return NormType.class;
    }
}
