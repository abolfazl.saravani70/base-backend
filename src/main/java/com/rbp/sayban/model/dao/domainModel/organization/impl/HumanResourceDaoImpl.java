package com.rbp.sayban.model.dao.domainModel.organization.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.organization.IHumanResourceDao;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.organization.HumanResource;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class HumanResourceDaoImpl extends GenericRepository<HumanResource> implements IHumanResourceDao {
    @Override
    protected Class<HumanResource> getDomainClass() {
        return HumanResource.class;
    }

}
