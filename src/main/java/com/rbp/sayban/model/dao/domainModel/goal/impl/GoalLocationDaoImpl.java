package com.rbp.sayban.model.dao.domainModel.goal.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalLocationDao;
import com.rbp.sayban.model.domainmodel.goal.GoalLocation;
import org.springframework.stereotype.Repository;

@Repository
public class GoalLocationDaoImpl extends GenericRepository<GoalLocation> implements IGoalLocationDao {


    @Override
    protected Class<GoalLocation> getDomainClass() {
        return GoalLocation.class;
    }
}
