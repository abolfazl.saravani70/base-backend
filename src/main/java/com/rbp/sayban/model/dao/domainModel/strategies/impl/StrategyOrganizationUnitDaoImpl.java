/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.strategies.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyOrganizationUnit;
import org.springframework.stereotype.Repository;

@Repository
public class StrategyOrganizationUnitDaoImpl extends GenericRepository<StrategyOrganizationUnit> implements IStrategyOrganizationUnitDao {


    @Override
    protected Class<StrategyOrganizationUnit> getDomainClass() {
        return StrategyOrganizationUnit.class;
    }
}
