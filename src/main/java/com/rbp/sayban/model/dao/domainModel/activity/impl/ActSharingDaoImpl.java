package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActSharingDao;
import com.rbp.sayban.model.domainmodel.dep.ActSharing;
import org.springframework.stereotype.Repository;

@Repository
public class ActSharingDaoImpl extends GenericRepository<ActSharing> implements IActSharingDao {
    @Override
    protected Class<ActSharing> getDomainClass() {
        return ActSharing.class;
    }
}
