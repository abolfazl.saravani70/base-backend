/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.objectives.impl;

import com.rbp.core.model.dao.base.impl.GenericRootRepository;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveTreeDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveTree;
import org.springframework.stereotype.Repository;

@Repository
public class ObjectiveTreeDaoImpl extends GenericRootRepository<ObjectiveTree>
        implements IObjectiveTreeDao {
    @Override
    protected Class<ObjectiveTree> getDomainClass() {
        return ObjectiveTree.class;
    }
}
