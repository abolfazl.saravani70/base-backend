package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivitySubjectiveDao;
import com.rbp.sayban.model.domainmodel.activity.ActivitySubjective;
import org.springframework.stereotype.Repository;

@Repository
public class ActivitySubjectiveDaoImpl extends GenericRepository<ActivitySubjective> implements IActivitySubjectiveDao {


    @Override
    protected Class<ActivitySubjective> getDomainClass() {
        return ActivitySubjective.class;
    }
}
