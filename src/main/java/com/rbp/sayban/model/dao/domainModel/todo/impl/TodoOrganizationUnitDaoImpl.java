/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.todo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoOrganizationUnitDao;
import com.rbp.sayban.model.domainmodel.todo.TodoOrganizationUnit;
import org.springframework.stereotype.Repository;

@Repository
public class TodoOrganizationUnitDaoImpl extends GenericRepository<TodoOrganizationUnit> implements ITodoOrganizationUnitDao {


    @Override
    protected Class<TodoOrganizationUnit> getDomainClass() {
        return TodoOrganizationUnit.class;
    }
}
