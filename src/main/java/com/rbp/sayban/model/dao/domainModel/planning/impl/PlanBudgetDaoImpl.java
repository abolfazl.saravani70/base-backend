package com.rbp.sayban.model.dao.domainModel.planning.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.planning.IPlanBudgetDao;
import com.rbp.sayban.model.domainmodel.planning.PlanBudget;
import org.springframework.stereotype.Repository;

@Repository
public class PlanBudgetDaoImpl extends GenericRepository<PlanBudget> implements IPlanBudgetDao {
    @Override
    protected Class<PlanBudget> getDomainClass() {
        return PlanBudget.class;
    }
}
