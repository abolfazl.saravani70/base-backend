package com.rbp.sayban.model.dao.domainModel.goal.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.goal.IGoalHRDao;
import com.rbp.sayban.model.domainmodel.goal.GoalHR;
import org.springframework.stereotype.Repository;

@Repository
public class GoalHRDaoImpl extends GenericRepository<GoalHR> implements IGoalHRDao {


    @Override
    protected Class<GoalHR> getDomainClass() {
        return GoalHR.class;
    }
}
