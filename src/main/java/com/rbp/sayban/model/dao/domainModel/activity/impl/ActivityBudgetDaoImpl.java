package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityBudgetDao;
import com.rbp.sayban.model.domainmodel.activity.ActivityBudget;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityBudgetDaoImpl extends GenericRepository<ActivityBudget> implements IActivityBudgetDao {
    @Override
    protected Class<ActivityBudget> getDomainClass() {
        return ActivityBudget.class;
    }
}
