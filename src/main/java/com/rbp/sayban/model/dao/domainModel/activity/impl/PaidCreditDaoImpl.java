package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IPaidCreditDao;
import com.rbp.sayban.model.domainmodel.dep.PaidCredit;
import org.springframework.stereotype.Repository;

@Repository
public class PaidCreditDaoImpl extends GenericRepository<PaidCredit> implements IPaidCreditDao {
    @Override
    protected Class<PaidCredit> getDomainClass() {
        return PaidCredit.class;
    }
}
