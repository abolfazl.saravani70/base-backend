package com.rbp.sayban.model.dao.domainModel.task;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.task.TaskBudget;

public interface ITaskBudgetDao extends IGenericRepository<TaskBudget> {
}
