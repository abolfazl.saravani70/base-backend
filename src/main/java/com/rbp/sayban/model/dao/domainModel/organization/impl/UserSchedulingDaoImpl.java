/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.organization.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.organization.IUserSchedulingDao;
import com.rbp.sayban.model.domainmodel.organization.UserScheduling;
import org.springframework.stereotype.Repository;

@Repository
public class UserSchedulingDaoImpl extends GenericRepository<UserScheduling> implements IUserSchedulingDao {
    @Override
    protected Class<UserScheduling> getDomainClass() {
        return UserScheduling.class;
    }
}
