/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.objectives.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveRiskDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveRisk;
import org.springframework.stereotype.Repository;

@Repository
public class ObjectiveRiskDaoImpl extends GenericRepository<ObjectiveRisk> implements IObjectiveRiskDao {

    @Override
    protected Class<ObjectiveRisk> getDomainClass() {
        return ObjectiveRisk.class;
    }
}
