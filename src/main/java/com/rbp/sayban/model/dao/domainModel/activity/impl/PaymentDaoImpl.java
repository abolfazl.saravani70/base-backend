package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IPaymentDao;
import com.rbp.sayban.model.domainmodel.dep.Payment;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentDaoImpl extends GenericRepository<Payment> implements IPaymentDao {
    @Override
    protected Class<Payment> getDomainClass() {
        return Payment.class;
    }
}
