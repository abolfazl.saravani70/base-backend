package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActSharingTypeDao;
import com.rbp.sayban.model.domainmodel.dep.ActSharingType;
import org.springframework.stereotype.Repository;

@Repository
public class ActSharingTypeDaoImpl extends GenericRepository<ActSharingType>implements IActSharingTypeDao {
    @Override
    protected Class<ActSharingType> getDomainClass() {
        return ActSharingType.class;
    }
}
