package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityDao;
import com.rbp.sayban.model.domainmodel.activity.Activity;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityDaoImpl extends GenericRepository<Activity> implements IActivityDao {


    @Override
    protected Class<Activity> getDomainClass() {
        return Activity.class;
    }
}
