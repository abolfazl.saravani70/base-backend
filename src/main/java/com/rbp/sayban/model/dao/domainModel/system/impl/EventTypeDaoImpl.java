/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.system.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.system.IEventTypeDao;
import com.rbp.sayban.model.domainmodel.system.EventType;
import org.springframework.stereotype.Repository;

@Repository
public class EventTypeDaoImpl extends GenericRepository<EventType> implements IEventTypeDao {
    @Override
    protected Class<EventType> getDomainClass() {
        return EventType.class;
    }
}
