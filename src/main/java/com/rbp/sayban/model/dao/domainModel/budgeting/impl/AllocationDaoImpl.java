/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.budgeting.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.budgeting.IAllocationDao;
import com.rbp.sayban.model.domainmodel.budgeting.Allocation;
import org.springframework.stereotype.Repository;

@Repository
public class AllocationDaoImpl extends GenericRepository<Allocation> implements IAllocationDao {

    @Override
    protected Class<Allocation> getDomainClass() {
        return Allocation.class;
    }
}
