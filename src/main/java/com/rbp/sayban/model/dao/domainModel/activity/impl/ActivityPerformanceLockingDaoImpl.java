package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityPerformanceLockingDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityPerformanceLocking;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityPerformanceLockingDaoImpl extends GenericRepository<ActivityPerformanceLocking> implements IActivityPerformanceLockingDao {
    @Override
    protected Class<ActivityPerformanceLocking> getDomainClass() {
        return ActivityPerformanceLocking.class;
    }
}
