package com.rbp.sayban.model.dao.domainModel.subjective.impl;


import com.rbp.core.model.dao.base.impl.GenericTreeRepository;
import com.rbp.sayban.model.dao.domainModel.risk.IRiskTreeDao;
import com.rbp.sayban.model.dao.domainModel.subjective.ISubjectiveDao;
import com.rbp.sayban.model.dao.domainModel.subjective.ISubjectiveTreeDao;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;
import com.rbp.sayban.model.domainmodel.subjective.SubjectiveTree;
import org.springframework.stereotype.Repository;
import sun.reflect.generics.tree.Tree;

@Repository
public class SubjectiveTreeDaoImpl extends GenericTreeRepository<SubjectiveTree> implements ISubjectiveTreeDao {

    @Override
    protected Class<SubjectiveTree> getDomainClass() {
        return SubjectiveTree.class;
    } 
}