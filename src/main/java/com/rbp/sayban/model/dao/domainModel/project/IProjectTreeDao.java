/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.project;

import com.rbp.core.model.dao.base.IGenericRootRepository;
import com.rbp.sayban.model.domainmodel.project.ProjectTree;

public interface IProjectTreeDao extends IGenericRootRepository<ProjectTree> {
}
