package com.rbp.sayban.model.dao.domainModel.goal;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.goal.GoalLocation;

public interface IGoalLocationDao extends IGenericRepository<GoalLocation> {
}
