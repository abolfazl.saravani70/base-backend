package com.rbp.sayban.model.dao.domainModel.goal;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.goal.GoalHR;

public interface IGoalHRDao extends IGenericRepository<GoalHR> {
}
