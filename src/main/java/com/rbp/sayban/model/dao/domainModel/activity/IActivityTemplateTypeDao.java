package com.rbp.sayban.model.dao.domainModel.activity;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateType;

public interface IActivityTemplateTypeDao extends IGenericRepository<ActivityTemplateType> {
}
