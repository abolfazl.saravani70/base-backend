/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.task.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.task.ITaskRiskDao;
import com.rbp.sayban.model.domainmodel.task.TaskRisk;
import org.springframework.stereotype.Repository;

@Repository
public class TaskRiskDaoImpl extends GenericRepository<TaskRisk> implements ITaskRiskDao {


    @Override
    protected Class<TaskRisk> getDomainClass() {
        return TaskRisk.class;
    }

    
}
