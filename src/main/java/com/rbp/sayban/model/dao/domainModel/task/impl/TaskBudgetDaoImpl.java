package com.rbp.sayban.model.dao.domainModel.task.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.task.ITaskBudgetDao;
import com.rbp.sayban.model.domainmodel.task.TaskBudget;
import org.springframework.stereotype.Repository;

@Repository
public class TaskBudgetDaoImpl extends GenericRepository<TaskBudget> implements ITaskBudgetDao {


    @Override
    protected Class<TaskBudget> getDomainClass() {
        return TaskBudget.class;
    }
}
