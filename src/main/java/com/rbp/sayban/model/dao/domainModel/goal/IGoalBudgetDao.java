package com.rbp.sayban.model.dao.domainModel.goal;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.goal.GoalBudget;

public interface IGoalBudgetDao extends IGenericRepository<GoalBudget> {
}
