package com.rbp.sayban.model.dao.domainModel.project;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.project.ProjectHR;

public interface IProjectHRDao extends IGenericRepository<ProjectHR> {
}
