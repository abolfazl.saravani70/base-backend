/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.organization.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.model.consts.organization.C_OrgChart;
import com.rbp.sayban.model.dao.domainModel.organization.IOrganizationChartDao;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrganizationChartDaoImpl extends GenericRepository<OrganizationChart> implements IOrganizationChartDao {
    @Override
    protected Class<OrganizationChart> getDomainClass() {
        return OrganizationChart.class;
    }

    @Override
    public List<OrganizationChart> findAllParent(int pageNumber, int pageSize, Long parentId) {
        List<OrganizationChart> organizationCharts;

        if (pageSize == 0) {
            organizationCharts = getEntityManager().createQuery("FROM " + getDomainClass().getSimpleName() +
                    " WHERE  pvTypeId IN( " + C_OrgChart.CHART + " , " + C_OrgChart.CHART_AND_CHART_POSITION + ") AND parent= " + parentId, getDomainClass())
                    .getResultList();


        } else {

            if (pageNumber <= 0 || pageSize < 0) {
                // TODO: 7/16/2019 throw exception
                throw new ApplicationException(0, "");
            }

            organizationCharts = getEntityManager().createQuery("FROM " + getDomainClass().getSimpleName() +
                    " WHERE  pvTypeId IN( " + C_OrgChart.CHART + " , " + C_OrgChart.CHART_AND_CHART_POSITION + ") AND parent= " + parentId, getDomainClass())
                    .setFirstResult((pageNumber - 1) * pageSize)
                    .setMaxResults(pageSize)
                    .getResultList();
        }
        return organizationCharts;
    }

    @Override
    public List<OrganizationChart> findAllChildren(int pageNumber, int pageSize, Long parentId) {
        List<OrganizationChart> organizationCharts;

        if (pageSize == 0) {
            organizationCharts = getEntityManager().createQuery("FROM " + getDomainClass().getSimpleName() +
                    " WHERE  pvTypeId IN( " + C_OrgChart.CHART_POSITION + " , " + C_OrgChart.CHART_AND_CHART_POSITION + ") AND parent= " + parentId, getDomainClass())
                    .getResultList();


        } else {

            if (pageNumber <= 0 || pageSize < 0) {
                // TODO: 7/16/2019 throw exception
                throw new ApplicationException(0, "");
            }

            organizationCharts = getEntityManager().createQuery("FROM " + getDomainClass().getSimpleName() +
                    " WHERE  pvTypeId IN( "+ C_OrgChart.CHART_POSITION + ") AND parent= " + parentId, getDomainClass())
                    .setFirstResult((pageNumber - 1) * pageSize)
                    .setMaxResults(pageSize)
                    .getResultList();
        }
        return organizationCharts;
    }
}
