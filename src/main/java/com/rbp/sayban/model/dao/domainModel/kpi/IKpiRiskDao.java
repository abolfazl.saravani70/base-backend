/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.kpi;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.kpi.KpiRisk;

public interface IKpiRiskDao extends IGenericRepository<KpiRisk> {


}
