package com.rbp.sayban.model.dao.domainModel.budgeting.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.base.impl.GenericTreeRepository;
import com.rbp.sayban.model.dao.domainModel.budgeting.IBudgetTreeDao;
import com.rbp.sayban.model.domainmodel.budgeting.BudgetTree;
import org.springframework.stereotype.Repository;

@Repository
public class BudgetTreeDaoImpl extends GenericTreeRepository<BudgetTree> implements IBudgetTreeDao {

    @Override
    protected Class<BudgetTree> getDomainClass() {
        return BudgetTree.class;
    } 
}