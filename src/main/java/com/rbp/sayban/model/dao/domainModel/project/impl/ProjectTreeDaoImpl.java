/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.dao.domainModel.project.impl;

import com.rbp.core.model.dao.base.impl.GenericRootRepository;
import com.rbp.sayban.model.dao.domainModel.project.IProjectTreeDao;
import com.rbp.sayban.model.domainmodel.project.ProjectTree;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectTreeDaoImpl extends GenericRootRepository<ProjectTree> implements IProjectTreeDao {
    @Override
    protected Class<ProjectTree> getDomainClass() {
        return ProjectTree.class;
    }
}
