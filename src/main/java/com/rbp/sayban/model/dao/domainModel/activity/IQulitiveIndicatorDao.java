package com.rbp.sayban.model.dao.domainModel.activity;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.dep.QualitativeIndicator;

public interface IQulitiveIndicatorDao extends IGenericRepository<QualitativeIndicator> {
}
