package com.rbp.sayban.model.dao.domainModel.strategies;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.strategies.StrategyLocation;

public interface IStrategyLocationDao extends IGenericRepository<StrategyLocation> {
}
