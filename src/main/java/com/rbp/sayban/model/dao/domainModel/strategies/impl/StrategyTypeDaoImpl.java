/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.strategies.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.strategies.IStrategyTypeDao;
import com.rbp.sayban.model.domainmodel.strategies.StrategyType;
import org.springframework.stereotype.Repository;

@Repository
public class StrategyTypeDaoImpl extends GenericRepository<StrategyType> implements IStrategyTypeDao {
    @Override
    protected Class<StrategyType> getDomainClass() {
        return StrategyType.class;
    }
}
