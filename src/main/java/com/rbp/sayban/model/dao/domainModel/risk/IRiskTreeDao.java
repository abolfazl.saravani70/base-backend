package com.rbp.sayban.model.dao.domainModel.risk;


import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.core.model.dao.base.IGenericTreeRepository;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;

public interface IRiskTreeDao extends IGenericTreeRepository<RiskTree> {
}