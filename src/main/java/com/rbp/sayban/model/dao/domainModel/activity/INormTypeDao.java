package com.rbp.sayban.model.dao.domainModel.activity;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.norm.NormType;

public interface INormTypeDao extends IGenericRepository<NormType> {
}
