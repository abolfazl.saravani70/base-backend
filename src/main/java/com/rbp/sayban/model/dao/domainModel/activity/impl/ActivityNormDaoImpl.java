package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityNormDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityNorm;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityNormDaoImpl extends GenericRepository<ActivityNorm> implements IActivityNormDao {
    @Override
    protected Class<ActivityNorm> getDomainClass() {
        return ActivityNorm.class;
    }
}
