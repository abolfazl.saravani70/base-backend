package com.rbp.sayban.model.dao.domainModel.todo.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.todo.ITodoSubjectiveDao;
import com.rbp.sayban.model.domainmodel.todo.TodoSubjective;
import org.springframework.stereotype.Repository;
@Repository
public class TodoSubjectiveDaoImpl extends GenericRepository<TodoSubjective> implements ITodoSubjectiveDao {


@Override
protected Class<TodoSubjective> getDomainClass() {
        return TodoSubjective.class;
    }

}
