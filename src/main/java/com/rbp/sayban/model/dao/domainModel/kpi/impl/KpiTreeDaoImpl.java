package com.rbp.sayban.model.dao.domainModel.kpi.impl;


import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.core.model.dao.base.impl.GenericTreeRepository;
import com.rbp.sayban.model.dao.domainModel.kpi.IKpiTreeDao;
import com.rbp.sayban.model.domainmodel.kpi.KpiTree;
import org.springframework.stereotype.Repository;

@Repository
public class KpiTreeDaoImpl extends GenericTreeRepository<KpiTree> implements IKpiTreeDao {

    @Override
    protected Class<KpiTree> getDomainClass() {
        return KpiTree.class;
    } 
}