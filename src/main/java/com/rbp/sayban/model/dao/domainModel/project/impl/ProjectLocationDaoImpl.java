package com.rbp.sayban.model.dao.domainModel.project.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.project.IProjectLocationDao;
import com.rbp.sayban.model.domainmodel.project.ProjectLocation;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectLocationDaoImpl extends GenericRepository<ProjectLocation> implements IProjectLocationDao {
    @Override
    protected Class<ProjectLocation> getDomainClass() {
        return ProjectLocation.class;
    }
}
