/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Alireza Souhani , Sayban Project Manager , mobile: 09120295596 , Email: Alireza.Souhani@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.system.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.system.IRequesterDao;
import com.rbp.sayban.model.domainmodel.system.Requester;
import org.springframework.stereotype.Repository;

@Repository
public class RequesterDaoImpl extends GenericRepository<Requester> implements IRequesterDao {
    @Override
    protected Class<Requester> getDomainClass() {
        return Requester.class;
    }
}