package com.rbp.sayban.model.dao.domainModel.project;

import com.rbp.core.model.dao.base.IGenericRepository;
import com.rbp.sayban.model.domainmodel.project.ProjectBudget;

public interface IProjectBudgetDao extends IGenericRepository<ProjectBudget> {
}
