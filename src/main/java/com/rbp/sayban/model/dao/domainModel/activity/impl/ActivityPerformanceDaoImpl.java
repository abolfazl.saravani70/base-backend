package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityPerformanceDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityPerformance;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityPerformanceDaoImpl extends GenericRepository<ActivityPerformance> implements IActivityPerformanceDao {
    @Override
    protected Class<ActivityPerformance> getDomainClass() {
        return ActivityPerformance.class;
    }
}
