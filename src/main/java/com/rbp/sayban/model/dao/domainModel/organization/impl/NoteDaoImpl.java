/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.dao.domainModel.organization.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.organization.INoteDao;
import com.rbp.sayban.model.domainmodel.organization.Note;
import org.springframework.stereotype.Repository;

@Repository
public class NoteDaoImpl extends GenericRepository<Note> implements INoteDao {
    @Override
    protected Class<Note> getDomainClass() {
        return Note.class;
    }
}
