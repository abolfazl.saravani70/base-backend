package com.rbp.sayban.model.dao.domainModel.objectives.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.objectives.IObjectiveHRDao;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveHR;
import org.springframework.stereotype.Repository;

@Repository
public class ObjectiveHRDaoImpl extends GenericRepository<ObjectiveHR> implements IObjectiveHRDao {

    @Override
    protected Class<ObjectiveHR> getDomainClass() {
        return ObjectiveHR.class;
    }
}
