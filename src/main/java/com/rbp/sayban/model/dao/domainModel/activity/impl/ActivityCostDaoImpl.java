package com.rbp.sayban.model.dao.domainModel.activity.impl;

import com.rbp.core.model.dao.base.impl.GenericRepository;
import com.rbp.sayban.model.dao.domainModel.activity.IActivityCostDao;
import com.rbp.sayban.model.domainmodel.dep.ActivityCost;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityCostDaoImpl extends GenericRepository<ActivityCost> implements IActivityCostDao {
    @Override
    protected Class<ActivityCost> getDomainClass() {
        return ActivityCost.class;
    }
}
