/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.todo;

import com.rbp.core.model.dto.base.abstractClass.junction.RiskJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.todo.TodoDTO;

public class TodoRiskViewModel extends RiskJunctionBaseEntityDTO {

    private static final long serialVersionUID = 9190258358940022008L;

    private TodoDTO todo;


    public TodoDTO getTodo() {
        return todo;
    }

    public void setTodo(TodoDTO todo) {
        this.todo = todo;
    }
}
