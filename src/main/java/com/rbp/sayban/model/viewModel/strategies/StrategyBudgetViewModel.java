package com.rbp.sayban.model.viewModel.strategies;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;

public class StrategyBudgetViewModel extends BudgetJunctionBaseEntityDTO {

    private static final long serialVersionUID = -2693279406198355690L;

    private StrategyDTO strategy;

    public StrategyDTO getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyDTO strategy) {
        this.strategy = strategy;
    }
}
