/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.viewModel.strategies;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.OrganizationUnitJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.organization.OrganizationUnitDTO;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;

public class StrategyOrganizationUnitViewModel extends OrganizationUnitJunctionBaseEntityDTO {

    private static final long serialVersionUID = -6393928615620365074L;

    private StrategyDTO strategy;


    public StrategyDTO getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyDTO strategy) {
        this.strategy = strategy;
    }

}
