/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.viewModel.planning;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.KpiJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.kpi.KpiDTO;
import com.rbp.sayban.model.dto.planning.PlanDTO;

public class PlanKpiViewModel extends KpiJunctionBaseEntityDTO {

    private static final long serialVersionUID = -8941769380657883254L;

    private PlanDTO plan;

    public PlanDTO getPlan() {
        return plan;
    }

    public void setPlan(PlanDTO plan) {
        this.plan = plan;
    }

}
