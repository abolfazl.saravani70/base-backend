package com.rbp.sayban.model.viewModel.planning;

import com.rbp.core.model.dto.base.abstractClass.junction.LocationJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.planning.PlanDTO;

public class PlanLocationViewModel extends LocationJunctionBaseEntityDTO {

    private static final long serialVersionUID = -6452782949450587131L;

    private PlanDTO plan;

    public PlanDTO getPlan() {
        return plan;
    }

    public void setPlan(PlanDTO plan) {
        this.plan = plan;
    }
}
