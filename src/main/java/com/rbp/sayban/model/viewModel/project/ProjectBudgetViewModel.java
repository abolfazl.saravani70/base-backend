package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;

public class ProjectBudgetViewModel extends BudgetJunctionBaseEntityDTO {

    private static final long serialVersionUID = -8125191027332889679L;

    private ProjectDTO project;

    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }
}
