/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.todo;

import com.rbp.core.model.dto.base.abstractClass.junction.OrganizationUnitJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.todo.TodoDTO;

public class TodoOrganizationUnitViewModel extends OrganizationUnitJunctionBaseEntityDTO {

    private static final long serialVersionUID = 3398549009213355418L;

    private TodoDTO todo;


    public TodoDTO getTodo() {
        return todo;
    }

    public void setTodo(TodoDTO todo) {
        this.todo = todo;
    }
}
