package com.rbp.sayban.model.viewModel.activity;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.OrganizationUnitJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.organization.OrganizationUnitDTO;

public class ActivityOrganizationUnitViewModel extends OrganizationUnitJunctionBaseEntityDTO {

    private static final long serialVersionUID = 8554728500923924744L;

    private ActivityDTO activity;

    public ActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(ActivityDTO activity) {
        this.activity = activity;
    }

}
