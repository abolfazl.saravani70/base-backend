package com.rbp.sayban.model.viewModel.strategies;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.HRJunctionBaseEntity;
import com.rbp.core.model.dto.base.abstractClass.junction.HRJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;

public class StrategyHRViewModel extends HRJunctionBaseEntityDTO {

    private static final long serialVersionUID = -317626710028438836L;

    private StrategyDTO strategy;


    public StrategyDTO getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyDTO strategy) {
        this.strategy = strategy;
    }
}
