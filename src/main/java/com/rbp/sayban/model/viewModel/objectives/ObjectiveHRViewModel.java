package com.rbp.sayban.model.viewModel.objectives;

import com.rbp.core.model.dto.base.abstractClass.junction.HRJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;

public class ObjectiveHRViewModel extends HRJunctionBaseEntityDTO {

    private static final long serialVersionUID = 3558235115346055958L;

    private ObjectiveDTO objective;


    public ObjectiveDTO getObjective() {
        return objective;
    }

    public void setObjective(ObjectiveDTO objective) {
        this.objective = objective;
    }
}
