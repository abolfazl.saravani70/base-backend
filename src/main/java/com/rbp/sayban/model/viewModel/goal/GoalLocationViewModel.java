package com.rbp.sayban.model.viewModel.goal;

import com.rbp.core.model.dto.base.abstractClass.junction.LocationJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.goal.GoalDTO;

public class GoalLocationViewModel extends LocationJunctionBaseEntityDTO {

    private static final long serialVersionUID = 136856373925780743L;

    private GoalDTO goal;


    public GoalDTO getGoal() {
        return goal;
    }

    public void setGoal(GoalDTO goal) {
        this.goal = goal;
    }
}
