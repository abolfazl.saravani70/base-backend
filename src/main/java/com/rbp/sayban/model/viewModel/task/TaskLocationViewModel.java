package com.rbp.sayban.model.viewModel.task;

import com.rbp.core.model.dto.base.abstractClass.junction.LocationJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.task.TaskDTO;

public class TaskLocationViewModel extends LocationJunctionBaseEntityDTO {


    private static final long serialVersionUID = -5726854268013639391L;

    private TaskDTO task;


    public TaskDTO getTask() {
        return task;
    }

    public void setTask(TaskDTO task) {
        this.task = task;
    }
}
