package com.rbp.sayban.model.viewModel.objectives;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;

public class ObjectiveBudgetViewModel extends BudgetJunctionBaseEntityDTO {


    private static final long serialVersionUID = -7666808635405715414L;

    private ObjectiveDTO objective;

    public ObjectiveDTO getObjective() {
        return objective;
    }

    public void setObjective(ObjectiveDTO objective) {
        this.objective = objective;
    }
}
