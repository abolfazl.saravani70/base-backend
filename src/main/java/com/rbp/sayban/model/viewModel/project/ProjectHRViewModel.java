package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.junction.HRJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;

public class ProjectHRViewModel extends HRJunctionBaseEntityDTO {
    private static final long serialVersionUID = 4041199822039688142L;

    private ProjectDTO project;


    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }
}
