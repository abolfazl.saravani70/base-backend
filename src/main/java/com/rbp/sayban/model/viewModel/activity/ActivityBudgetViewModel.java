package com.rbp.sayban.model.viewModel.activity;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.ActivityDTO;

public class ActivityBudgetViewModel  extends BudgetJunctionBaseEntityDTO {

    private static final long serialVersionUID = 6482586388873303409L;

    private ActivityDTO activity;

    public ActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(ActivityDTO activity) {
        this.activity = activity;
    }
}
