package com.rbp.sayban.model.viewModel.planning;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;
import com.rbp.sayban.model.dto.planning.PlanDTO;

public class PlanBudgetViewModel extends BudgetJunctionBaseEntityDTO {


    private static final long serialVersionUID = 2165727839741863658L;

    private PlanDTO plan;

    public PlanDTO getPlan() {
        return plan;
    }

    public void setPlan(PlanDTO plan) {
        this.plan = plan;
    }
}
