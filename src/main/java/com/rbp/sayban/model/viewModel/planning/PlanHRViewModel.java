package com.rbp.sayban.model.viewModel.planning;

import com.rbp.core.model.dto.base.abstractClass.junction.HRJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.planning.PlanDTO;

public class PlanHRViewModel extends HRJunctionBaseEntityDTO {

    private static final long serialVersionUID = -538975866635402995L;

    private PlanDTO plan;

    public PlanDTO getPlan() {
        return plan;
    }

    public void setPlan(PlanDTO plan) {
        this.plan = plan;
    }
}
