/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.viewModel.goal;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.NormCostJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.NormCostDTO;
import com.rbp.sayban.model.dto.goal.GoalDTO;

public class GoalNormCostViewModel extends NormCostJunctionBaseEntityDTO {

    private static final long serialVersionUID = -8271154287915696307L;

    private GoalDTO goal;

    public GoalDTO getGoal() {
        return goal;
    }

    public void setGoal(GoalDTO goal) {
        this.goal = goal;
    }

}
