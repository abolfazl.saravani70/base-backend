/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;

public class ProjectSubjectiveViewModel extends SubjectiveJunctionBaseEntityDTO {

    private static final long serialVersionUID = 2482885548716302338L;

    private ProjectDTO project;


    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }

}
