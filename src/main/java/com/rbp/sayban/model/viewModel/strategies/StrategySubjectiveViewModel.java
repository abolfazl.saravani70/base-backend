/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.strategies;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;

public class StrategySubjectiveViewModel extends SubjectiveJunctionBaseEntityDTO {

    private static final long serialVersionUID = -6725541775605360854L;

    private StrategyDTO strategy;


    public StrategyDTO getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyDTO strategy) {
        this.strategy = strategy;
    }

}
