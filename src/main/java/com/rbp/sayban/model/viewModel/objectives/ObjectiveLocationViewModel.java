package com.rbp.sayban.model.viewModel.objectives;

import com.rbp.core.model.dto.base.abstractClass.junction.LocationJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;

public class ObjectiveLocationViewModel extends LocationJunctionBaseEntityDTO {

    private static final long serialVersionUID = 3494593452253030187L;

    private ObjectiveDTO objective;


    public ObjectiveDTO getObjective() {
        return objective;
    }

    public void setObjective(ObjectiveDTO objective) {
        this.objective = objective;
    }
}
