package com.rbp.sayban.model.viewModel.goal;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.goal.GoalDTO;

public class GoalBudgetViewModel extends BudgetJunctionBaseEntityDTO {


    private static final long serialVersionUID = -6396968724571818159L;

    private GoalDTO goal;

    public GoalDTO getGoal() {
        return goal;
    }

    public void setGoal(GoalDTO goal) {
        this.goal = goal;
    }
}
