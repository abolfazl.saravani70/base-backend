/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.organization;

import com.rbp.sayban.model.dto.organization.OrganizationChartDTO;

public class OrganizationChartViewModel extends OrganizationChartDTO {

    private static final long serialVersionUID = -5064101909829951660L;

    private String parentName;
    private String pvOrgLevel;
    private String pvOrgDegree;
    private String pvOrganizeType;
    private String pvPositionNumber;
    private String pvPositionTitle;

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getPvOrgLevel() {
        return pvOrgLevel;
    }

    public void setPvOrgLevel(String pvOrgLevel) {
        this.pvOrgLevel = pvOrgLevel;
    }

    public String getPvOrgDegree() {
        return pvOrgDegree;
    }

    public void setPvOrgDegree(String pvOrgDegree) {
        this.pvOrgDegree = pvOrgDegree;
    }

    public String getPvOrganizeType() {
        return pvOrganizeType;
    }

    public void setPvOrganizeType(String pvOrganizeType) {
        this.pvOrganizeType = pvOrganizeType;
    }

    public String getPvPositionNumber() {
        return pvPositionNumber;
    }

    public void setPvPositionNumber(String pvPositionNumber) {
        this.pvPositionNumber = pvPositionNumber;
    }

    public String getPvPositionTitle() {
        return pvPositionTitle;
    }

    public void setPvPositionTitle(String pvPositionTitle) {
        this.pvPositionTitle = pvPositionTitle;
    }
}
