package com.rbp.sayban.model.viewModel.todo;

import com.rbp.core.model.dto.base.abstractClass.junction.HRJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.todo.TodoDTO;

public class TodoHRViewModel extends HRJunctionBaseEntityDTO {


    private static final long serialVersionUID = -7563994113433711198L;

    private TodoDTO todo;


    public TodoDTO getTodo() {
        return todo;
    }

    public void setTodo(TodoDTO todo) {
        this.todo = todo;
    }
}
