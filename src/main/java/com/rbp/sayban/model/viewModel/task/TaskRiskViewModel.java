/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.task;

import com.rbp.core.model.dto.base.abstractClass.junction.RiskJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.task.TaskDTO;

public class TaskRiskViewModel extends RiskJunctionBaseEntityDTO {

    private static final long serialVersionUID = -7385043285017606644L;

    private TaskDTO task;


    public TaskDTO getTask() {
        return task;
    }

    public void setTask(TaskDTO task) {
        this.task = task;
    }
}
