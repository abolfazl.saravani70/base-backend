/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.viewModel.objectives;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.KpiJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.kpi.KpiDTO;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;

public class ObjectiveKpiViewModel extends KpiJunctionBaseEntityDTO {

    private static final long serialVersionUID = -127798460620946459L;

    private ObjectiveDTO objective;


    public ObjectiveDTO getObjective() {
        return objective;
    }

    public void setObjective(ObjectiveDTO objective) {
        this.objective = objective;
    }

}
