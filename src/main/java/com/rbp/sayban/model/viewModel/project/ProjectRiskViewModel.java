package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.RiskJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import com.rbp.sayban.model.dto.risk.RiskDTO;

public class ProjectRiskViewModel extends RiskJunctionBaseEntityDTO {

    private static final long serialVersionUID = -2310276371750334977L;

    private ProjectDTO project;

    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }

}
