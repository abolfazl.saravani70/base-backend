package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.NormCostJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.NormCostDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;

public class ProjectNormCostViewModel extends NormCostJunctionBaseEntityDTO {

    private static final long serialVersionUID = -4118058607946575370L;

    private ProjectDTO project;


    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }

}
