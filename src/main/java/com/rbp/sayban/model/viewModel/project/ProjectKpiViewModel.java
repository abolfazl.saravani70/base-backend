package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.KpiJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.kpi.KpiDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;

public class ProjectKpiViewModel extends KpiJunctionBaseEntityDTO {

    private static final long serialVersionUID = 8491618505108885457L;

    private ProjectDTO project;


    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }

}
