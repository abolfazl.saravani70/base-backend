/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.viewModel.objectives;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;

public class ObjectiveSubjectiveViewModel extends SubjectiveJunctionBaseEntityDTO {

    private static final long serialVersionUID = 8404726792697787838L;

    private ObjectiveDTO objective;


    public ObjectiveDTO getObjective() {
        return objective;
    }

    public void setObjective(ObjectiveDTO objective) {
        this.objective = objective;
    }

}
