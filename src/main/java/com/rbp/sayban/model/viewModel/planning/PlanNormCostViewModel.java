package com.rbp.sayban.model.viewModel.planning;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.NormCostJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.NormCostDTO;
import com.rbp.sayban.model.dto.planning.PlanDTO;

public class PlanNormCostViewModel extends NormCostJunctionBaseEntityDTO {

    private static final long serialVersionUID = 6146142518883289052L;

    private PlanDTO plan;

    public PlanDTO getPlan() {
        return plan;
    }

    public void setPlan(PlanDTO plan) {
        this.plan = plan;
    }

}
