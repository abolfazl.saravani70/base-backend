package com.rbp.sayban.model.viewModel.todo;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.todo.TodoDTO;

public class TodoBudgetViewModel extends BudgetJunctionBaseEntityDTO {

    private static final long serialVersionUID = 7488551177683592118L;

    private TodoDTO todo;

    public TodoDTO getTodo() {
        return todo;
    }

    public void setTodo(TodoDTO todo) {
        this.todo = todo;
    }
}
