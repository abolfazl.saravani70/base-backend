/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.organization;

import com.rbp.sayban.model.dto.organization.OrganizationUnitDTO;

public class OrganizationUnitViewModel extends OrganizationUnitDTO {

    private static final long serialVersionUID = -5774176122097739631L;

    private String orgName;
    private String orgChartName;
    private Long orgChartParentId;
    private String orgChartParentName;
    private Boolean isVirtual;
    private Long pvOrgLevelId;
    private String  pvOrgLevelTitle;
    private Long pvOrgDegreeId;
    private String pvOrgDegreeTitle;
    private Long pvOrgTypeId;
    private String pvOrgTypeTitle;
    private Long pvPositionNumberId;
    private Long pvPositionTitleId;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgChartName() {
        return orgChartName;
    }

    public void setOrgChartName(String orgChartName) {
        this.orgChartName = orgChartName;
    }

    public Long getOrgChartParentId() {
        return orgChartParentId;
    }

    public void setOrgChartParentId(Long orgChartParentId) {
        this.orgChartParentId = orgChartParentId;
    }

    public String getOrgChartParentName() {
        return orgChartParentName;
    }

    public void setOrgChartParentName(String orgChartParentName) {
        this.orgChartParentName = orgChartParentName;
    }

    public Boolean getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Boolean virtual) {
        isVirtual = virtual;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }

    public Long getPvOrgTypeId() {
        return pvOrgTypeId;
    }

    public void setPvOrgTypeId(Long pvOrgTypeId) {
        this.pvOrgTypeId = pvOrgTypeId;
    }

    public Long getPvPositionNumberId() {
        return pvPositionNumberId;
    }

    public void setPvPositionNumberId(Long pvPositionNumberId) {
        this.pvPositionNumberId = pvPositionNumberId;
    }

    public Long getPvPositionTitleId() {
        return pvPositionTitleId;
    }

    public void setPvPositionTitleId(Long pvPositionTitleId) {
        this.pvPositionTitleId = pvPositionTitleId;
    }

    public String getPvOrgLevelTitle() {
        return pvOrgLevelTitle;
    }

    public void setPvOrgLevelTitle(String pvOrgLevelTitle) {
        this.pvOrgLevelTitle = pvOrgLevelTitle;
    }

    public String getPvOrgDegreeTitle() {
        return pvOrgDegreeTitle;
    }

    public void setPvOrgDegreeTitle(String pvOrgDegreeTitle) {
        this.pvOrgDegreeTitle = pvOrgDegreeTitle;
    }

    public String getPvOrgTypeTitle() {
        return pvOrgTypeTitle;
    }

    public void setPvOrgTypeTitle(String pvOrgTypeTitle) {
        this.pvOrgTypeTitle = pvOrgTypeTitle;
    }
}
