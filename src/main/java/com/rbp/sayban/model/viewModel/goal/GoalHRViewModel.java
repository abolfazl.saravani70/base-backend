package com.rbp.sayban.model.viewModel.goal;

import com.rbp.core.model.dto.base.abstractClass.junction.HRJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.goal.GoalDTO;

public class GoalHRViewModel extends HRJunctionBaseEntityDTO {

    private static final long serialVersionUID = 6821588115335943831L;

    private GoalDTO goal;


    public GoalDTO getGoal() {
        return goal;
    }

    public void setGoal(GoalDTO goal) {
        this.goal = goal;
    }
}
