package com.rbp.sayban.model.viewModel.task;

import com.rbp.core.model.dto.base.abstractClass.junction.BudgetJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.task.TaskDTO;

public class TaskBudgetViewModel extends BudgetJunctionBaseEntityDTO {

    private static final long serialVersionUID = -5733842784285858365L;

    private TaskDTO task;

    public TaskDTO getTask() {
        return task;
    }

    public void setTask(TaskDTO task) {
        this.task = task;
    }
}
