/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.model.viewModel.planning;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.planning.PlanDTO;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;

public class PlanSubjectiveViewModel extends SubjectiveJunctionBaseEntityDTO {

    private static final long serialVersionUID = 4982409854892711485L;

    private PlanDTO plan;


    public PlanDTO getPlan() {
        return plan;
    }

    public void setPlan(PlanDTO plan) {
        this.plan = plan;
    }

}
