package com.rbp.sayban.model.viewModel.activity;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.RiskJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.risk.RiskDTO;

public class ActivityRiskViewModel extends RiskJunctionBaseEntityDTO {

    private static final long serialVersionUID = -7344077072082202757L;

    private ActivityDTO activity;


    public ActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(ActivityDTO activity) {
        this.activity = activity;
    }

}
