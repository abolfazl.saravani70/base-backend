/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.viewModel.goal;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.RiskJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.goal.GoalDTO;
import com.rbp.sayban.model.dto.risk.RiskDTO;

public class GoalRiskViewModel extends RiskJunctionBaseEntityDTO {

    private static final long serialVersionUID = -6074150039918364506L;

    private GoalDTO goal;

    public GoalDTO getGoal() {
        return goal;
    }

    public void setGoal(GoalDTO goal) {
        this.goal = goal;
    }

}
