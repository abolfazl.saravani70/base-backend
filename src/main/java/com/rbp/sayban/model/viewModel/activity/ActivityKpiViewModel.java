package com.rbp.sayban.model.viewModel.activity;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.KpiJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.kpi.KpiDTO;

public class ActivityKpiViewModel extends KpiJunctionBaseEntityDTO {

    private static final long serialVersionUID = 6482586388873303409L;

    private ActivityDTO activity;


    public ActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(ActivityDTO activity) {
        this.activity = activity;
    }

}
