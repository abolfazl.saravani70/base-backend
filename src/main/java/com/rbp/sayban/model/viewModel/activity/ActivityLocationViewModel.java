package com.rbp.sayban.model.viewModel.activity;

import com.rbp.core.model.dto.base.abstractClass.junction.LocationJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.ActivityDTO;

public class ActivityLocationViewModel extends LocationJunctionBaseEntityDTO {


    private static final long serialVersionUID = 4042087554940678620L;

    private ActivityDTO activity;


    public ActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(ActivityDTO activity) {
        this.activity = activity;
    }
}
