/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.model.viewModel.organization;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

public class OrganizationViewModel extends BaseEntityDTO {

    private static final long serialVersionUID = 4457645719254708669L;

    private Long parentId;
    private String parentTitle;
    private String name;
    private String title;
    private String orgCode;
    private String zamzamCode;
    private String strategicCode;
    private Boolean isOrganized;
    private String pvOrgLevel;
    private Long pvOrgLevelId;
    private String pvOrgDegree;
    private Long pvOrgDegreeId;
    private String pvOrganizationType;
    private Long pvOrgTypeId;

    private String state;
    private Long stateId;
    private String city;
    private Long cityId;
    private String village;
    private Long villageId;
    private String zone;
    private Long zoneId;

    private String addressText;
    private String postalCode;
    private String addressType;

    private String phone;
    private String preCityCodeNumber;
    private Boolean isMainPhone;
    private String phoneType;

    private String email;
    private Boolean isActiveEmail;
    private String emailType;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getZamzamCode() {
        return zamzamCode;
    }

    public void setZamzamCode(String zamzamCode) {
        this.zamzamCode = zamzamCode;
    }

    public String getStrategicCode() {
        return strategicCode;
    }

    public void setStrategicCode(String strategicCode) {
        this.strategicCode = strategicCode;
    }

    public Boolean getIsOrganized() {
        return isOrganized;
    }

    public void setIsOrganized(Boolean organized) {
        isOrganized = organized;
    }

    public String getPvOrgLevel() {
        return pvOrgLevel;
    }

    public void setPvOrgLevel(String pvOrgLevel) {
        this.pvOrgLevel = pvOrgLevel;
    }

    public String getPvOrgDegree() {
        return pvOrgDegree;
    }

    public void setPvOrgDegree(String pvOrgDegree) {
        this.pvOrgDegree = pvOrgDegree;
    }

    public String getPvOrganizationType() {
        return pvOrganizationType;
    }

    public void setPvOrganizationType(String pvOrganizationType) {
        this.pvOrganizationType = pvOrganizationType;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAddressText() {
        return addressText;
    }

    public void setAddressText(String addressText) {
        this.addressText = addressText;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPreCityCodeNumber() {
        return preCityCodeNumber;
    }

    public void setPreCityCodeNumber(String preCityCodeNumber) {
        this.preCityCodeNumber = preCityCodeNumber;
    }


    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }


    public String getEmailType() {
        return emailType;
    }

    public Boolean getIsMainPhone() {
        return isMainPhone;
    }

    public void setIsMainPhone(Boolean mainPhone) {
        isMainPhone = mainPhone;
    }

    public Boolean getIsActiveEmail() {
        return isActiveEmail;
    }

    public void setIsActiveEmail(Boolean activeEmail) {
        isActiveEmail = activeEmail;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }

    public Long getPvOrgTypeId() {
        return pvOrgTypeId;
    }

    public void setPvOrgTypeId(Long pvOrgTypeId) {
        this.pvOrgTypeId = pvOrgTypeId;
    }

    public Long getStateId() {
        return stateId;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getVillageId() {
        return villageId;
    }

    public void setVillageId(Long villageId) {
        this.villageId = villageId;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }
}
