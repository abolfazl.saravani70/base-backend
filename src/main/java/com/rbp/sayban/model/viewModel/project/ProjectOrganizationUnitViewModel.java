package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.OrganizationUnitJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.organization.OrganizationUnitDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;

public class ProjectOrganizationUnitViewModel extends OrganizationUnitJunctionBaseEntityDTO {

    private static final long serialVersionUID = 2450312861087020440L;

    private ProjectDTO project;


    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }

}
