package com.rbp.sayban.model.viewModel.project;

import com.rbp.core.model.dto.base.abstractClass.junction.LocationJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;

public class ProjectLocationViewModel extends LocationJunctionBaseEntityDTO {

    private static final long serialVersionUID = 1763442729040126989L;

    private ProjectDTO project;


    public ProjectDTO getProject() {
        return project;
    }

    public void setProject(ProjectDTO project) {
        this.project = project;
    }
}
