package com.rbp.sayban.model.viewModel.activity;

import com.rbp.core.model.domainmodel.base.abstractClass.junction.NormCostJunctionBaseEntity;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import com.rbp.core.model.dto.base.abstractClass.junction.NormCostJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.activity.NormCostDTO;

public class ActivityNormCostViewModel extends NormCostJunctionBaseEntityDTO {

    private static final long serialVersionUID = -6642951978448102628L;

    private ActivityDTO activity;


    public ActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(ActivityDTO activity) {
        this.activity = activity;
    }

}
