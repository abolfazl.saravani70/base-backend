package com.rbp.sayban.model.viewModel.strategies;

import com.rbp.core.model.dto.base.abstractClass.junction.LocationJunctionBaseEntityDTO;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;

public class StrategyLocationViewModel extends LocationJunctionBaseEntityDTO {

    private static final long serialVersionUID = 7446162363998465657L;

    private StrategyDTO strategy;


    public StrategyDTO getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyDTO strategy) {
        this.strategy = strategy;
    }
}
