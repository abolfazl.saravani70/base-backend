package com.rbp.sayban.model.dto.goal;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.KpiJunctionBaseEntityDTO;

@ApiModel(value = "goal kpi dto Entity")
public class GoalKpiDTO extends KpiJunctionBaseEntityDTO {

    private Long goalId;


    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }


}