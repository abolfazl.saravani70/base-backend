package com.rbp.sayban.model.dto.budgeting;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "budget dto Entity")
public class BudgetDTO extends EvalStateBaseEntityDTO {

    private Long budgetTypeId;

    public Long getBudgetTypeId() {
        return budgetTypeId;
    }

    public void setBudgetTypeId(Long budgetTypeId) {
        this.budgetTypeId = budgetTypeId;
    }


}