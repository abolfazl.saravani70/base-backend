package com.rbp.sayban.model.dto.budgeting;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "expense dto Entity")
public class ExpenseDTO extends BaseEntityDTO {

    private Long budgetId;


    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }


}