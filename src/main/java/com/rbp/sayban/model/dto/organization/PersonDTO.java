package com.rbp.sayban.model.dto.organization;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.rbp.sayban.model.domainmodel.system.Blob;

@ApiModel(value = "person dto Entity")
public class PersonDTO extends BaseEntityDTO {

    private Long nationalId;
    private Long identityNumber;
    private String fatherName;
    private LocalDate birthDate;
    private String degree;
    private Long uxLevel;
    private Blob image;
    private String title;
    private String name;
    private String lastName;
    private Long pvGenderId;
    private Long stakeholderId;
    private Long organizationUnitId;


    public Long getNationalId() {
        return nationalId;
    }

    public void setNationalId(Long nationalId) {
        this.nationalId = nationalId;
    }

    public Long getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Long identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Long getUxLevel() {
        return uxLevel;
    }

    public void setUxLevel(Long uxLevel) {
        this.uxLevel = uxLevel;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getPvGenderId() {
        return pvGenderId;
    }

    public void setPvGenderId(Long pvGenderId) {
        this.pvGenderId = pvGenderId;
    }

    public Long getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(Long stakeholderId) {
        this.stakeholderId = stakeholderId;
    }

    public Long getOrganizationUnitId() {
        return organizationUnitId;
    }

    public void setOrganizationUnitId(Long organizationUnitId) {
        this.organizationUnitId = organizationUnitId;
    }


}