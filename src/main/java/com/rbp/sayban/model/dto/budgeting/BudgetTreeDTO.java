package com.rbp.sayban.model.dto.budgeting;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.TreeBaseEntityDTO;
import com.rbp.sayban.model.dto.budgeting.BudgetDTO;

@ApiModel(value = "budget tree dto Entity")
public class BudgetTreeDTO extends TreeBaseEntityDTO<BudgetDTO> {

    private BudgetDTO child;
    private BudgetDTO parent;

    @Override
    public BudgetDTO getChild() {
        return child;
    }

    @Override
    public void setChild(BudgetDTO child) {
        this.child = child;
    }

    @Override
    public BudgetDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(BudgetDTO parent) {
        this.parent = parent;
    }
}