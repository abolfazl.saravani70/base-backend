package com.rbp.sayban.model.dto.project;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.planning.PlanDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;

@ApiModel(value = "project tree dto Entity")
public class ProjectTreeDTO extends RootBaseEntityDTO<ProjectDTO,PlanDTO> {

    private PlanDTO root;
    private ProjectDTO child;
    private ProjectDTO parent;

    @Override
    public PlanDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(PlanDTO root) {
        this.root = root;
    }

    @Override
    public ProjectDTO getChild() {
        return child;
    }

    @Override
    public void setChild(ProjectDTO child) {
        this.child = child;
    }

    @Override
    public ProjectDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(ProjectDTO parent) {
        this.parent = parent;
    }
}