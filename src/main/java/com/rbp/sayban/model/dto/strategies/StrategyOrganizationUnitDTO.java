package com.rbp.sayban.model.dto.strategies;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.OrganizationUnitJunctionBaseEntityDTO;

@ApiModel(value = "strategy organization unit dto Entity")
public class StrategyOrganizationUnitDTO extends OrganizationUnitJunctionBaseEntityDTO {

    private Long strategyId;


    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }


}