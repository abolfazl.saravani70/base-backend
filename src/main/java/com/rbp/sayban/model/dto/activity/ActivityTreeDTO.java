package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.ActivityDTO;

@ApiModel(value = "activity tree dto Entity")
public class ActivityTreeDTO extends RootBaseEntityDTO<ActivityDTO,ProjectDTO> {

    private ProjectDTO root;
    private ActivityDTO child;
    private ActivityDTO parent;

    @Override
    public ProjectDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(ProjectDTO root) {
        this.root = root;
    }

    @Override
    public ActivityDTO getChild() {
        return child;
    }

    @Override
    public void setChild(ActivityDTO child) {
        this.child = child;
    }

    @Override
    public ActivityDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(ActivityDTO parent) {
        this.parent = parent;
    }
}