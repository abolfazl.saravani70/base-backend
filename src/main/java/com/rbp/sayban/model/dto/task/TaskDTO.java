package com.rbp.sayban.model.dto.task;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "task dto Entity")
public class TaskDTO extends EvalStateBaseEntityDTO {

    private Long pvTypeId;


    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }


}