package com.rbp.sayban.model.dto.kpi;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.TreeBaseEntityDTO;
import com.rbp.sayban.model.dto.kpi.KpiDTO;

@ApiModel(value = "kpi tree dto Entity")
public class KpiTreeDTO extends TreeBaseEntityDTO<KpiDTO> {

    private KpiDTO child;
    private KpiDTO parent;

    @Override
    public KpiDTO getChild() {
        return child;
    }

    @Override
    public void setChild(KpiDTO child) {
        this.child = child;
    }

    @Override
    public KpiDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(KpiDTO parent) {
        this.parent = parent;
    }
}