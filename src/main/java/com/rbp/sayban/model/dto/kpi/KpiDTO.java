package com.rbp.sayban.model.dto.kpi;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "kpi dto Entity")
public class KpiDTO extends EvalStateBaseEntityDTO {

    private Integer key;
    private String code;
    private Long value;
    private Long bestValue;
    private Integer kpiPriority;
    private Integer cost;
    private Long minimum;
    private Long maximum;
    private Long normal;
    private Long unitId;
    private Long pvKpiTypeId;
    private Long pvMesureUnitId;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Long getBestValue() {
        return bestValue;
    }

    public void setBestValue(Long bestValue) {
        this.bestValue = bestValue;
    }

    public Integer getKpiPriority() {
        return kpiPriority;
    }

    public void setKpiPriority(Integer kpiPriority) {
        this.kpiPriority = kpiPriority;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Long getMinimum() {
        return minimum;
    }

    public void setMinimum(Long minimum) {
        this.minimum = minimum;
    }

    public Long getMaximum() {
        return maximum;
    }

    public void setMaximum(Long maximum) {
        this.maximum = maximum;
    }

    public Long getNormal() {
        return normal;
    }

    public void setNormal(Long normal) {
        this.normal = normal;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public Long getPvKpiTypeId() {
        return pvKpiTypeId;
    }

    public void setPvKpiTypeId(Long pvKpiTypeId) {
        this.pvKpiTypeId = pvKpiTypeId;
    }

    public Long getPvMesureUnitId() {
        return pvMesureUnitId;
    }

    public void setPvMesureUnitId(Long pvMesureUnitId) {
        this.pvMesureUnitId = pvMesureUnitId;
    }


}