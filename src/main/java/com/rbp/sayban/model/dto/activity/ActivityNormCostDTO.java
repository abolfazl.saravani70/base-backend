package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.NormCostJunctionBaseEntityDTO;

@ApiModel(value = "activity norm cost dto Entity")
public class ActivityNormCostDTO extends NormCostJunctionBaseEntityDTO {

    private Long activityId;


    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}