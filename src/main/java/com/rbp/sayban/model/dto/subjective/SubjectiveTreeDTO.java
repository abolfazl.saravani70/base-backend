package com.rbp.sayban.model.dto.subjective;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.TreeBaseEntityDTO;
import com.rbp.sayban.model.dto.subjective.SubjectiveDTO;

@ApiModel(value = "subjective tree dto Entity")
public class SubjectiveTreeDTO extends TreeBaseEntityDTO<SubjectiveDTO> {

    private SubjectiveDTO child;
    private SubjectiveDTO parent;

    @Override
    public SubjectiveDTO getChild() {
        return child;
    }

    @Override
    public void setChild(SubjectiveDTO child) {
        this.child = child;
    }

    @Override
    public SubjectiveDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(SubjectiveDTO parent) {
        this.parent = parent;
    }
}