package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "act sharing detail dto Entity")
public class ActSharingDetailDTO extends BaseEntityDTO {

    private Integer baseAmount;
    private Integer baseKey;
    private Long actSharingId;
    private Long activityId;


    public Integer getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(Integer baseAmount) {
        this.baseAmount = baseAmount;
    }

    public Integer getBaseKey() {
        return baseKey;
    }

    public void setBaseKey(Integer baseKey) {
        this.baseKey = baseKey;
    }

    public Long getActSharingId() {
        return actSharingId;
    }

    public void setActSharingId(Long actSharingId) {
        this.actSharingId = actSharingId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}