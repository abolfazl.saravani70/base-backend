package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "performance dto Entity")
public class PerformanceDTO extends BaseEntityDTO {

    private String name;
    private String title;
    private String keyword;
    private String value;
    private Long pvPerformanceTypeId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getPvPerformanceTypeId() {
        return pvPerformanceTypeId;
    }

    public void setPvPerformanceTypeId(Long pvPerformanceTypeId) {
        this.pvPerformanceTypeId = pvPerformanceTypeId;
    }


}