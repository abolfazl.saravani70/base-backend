package com.rbp.sayban.model.dto.organization;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.TreeBaseEntityDTO;
import com.rbp.sayban.model.dto.organization.OrganizationDTO;

@ApiModel(value = "org tree dto Entity")
public class OrgTreeDTO extends TreeBaseEntityDTO<OrganizationDTO> {

    private OrganizationDTO child;
    private OrganizationDTO parent;

    @Override
    public OrganizationDTO getChild() {
        return child;
    }

    @Override
    public void setChild(OrganizationDTO child) {
        this.child = child;
    }

    @Override
    public OrganizationDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(OrganizationDTO parent) {
        this.parent = parent;
    }
}