package com.rbp.sayban.model.dto.risk;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "alternative dto Entity")
public class AlternativeDTO extends BaseEntityDTO {

    private Long solutionId;


    public Long getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(Long solutionId) {
        this.solutionId = solutionId;
    }


}