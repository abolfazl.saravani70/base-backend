package com.rbp.sayban.model.dto.budgeting;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "fund dto Entity")
public class FundDTO extends BaseEntityDTO {

    private Long budgetResourceId;
    private Long fundId;
    private Long policyId;


    public Long getBudgetResourceId() {
        return budgetResourceId;
    }

    public void setBudgetResourceId(Long budgetResourceId) {
        this.budgetResourceId = budgetResourceId;
    }

    public Long getFundId() {
        return fundId;
    }

    public void setFundId(Long fundId) {
        this.fundId = fundId;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }


}