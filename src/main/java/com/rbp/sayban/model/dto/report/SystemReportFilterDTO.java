package com.rbp.sayban.model.dto.report;

import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.report.GeneralReportFilterDTO;

@ApiModel(value = "system report filter dto Entity")
public class SystemReportFilterDTO extends GeneralReportFilterDTO {

    private Long systemId;


    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }


}