package com.rbp.sayban.model.dto.strategies;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.KpiJunctionBaseEntityDTO;

@ApiModel(value = "strategy kpi dto Entity")
public class StrategyKpiDTO extends KpiJunctionBaseEntityDTO {

    private Long strategyId;


    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }


}