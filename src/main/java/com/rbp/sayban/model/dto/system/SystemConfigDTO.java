package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "system config dto Entity")
public class SystemConfigDTO extends BaseEntityDTO {

    private String key;
    private String value;
    private Long enumValueId;
    private Long systemId;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getEnumValueId() {
        return enumValueId;
    }

    public void setEnumValueId(Long enumValueId) {
        this.enumValueId = enumValueId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }


}