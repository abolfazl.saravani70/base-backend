package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "activity subjective dto Entity")
public class ActivitySubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long activityId;


    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}