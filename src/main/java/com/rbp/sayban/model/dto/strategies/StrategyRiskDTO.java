package com.rbp.sayban.model.dto.strategies;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.RiskJunctionBaseEntityDTO;

@ApiModel(value = "strategy risk dto Entity")
public class StrategyRiskDTO extends RiskJunctionBaseEntityDTO {

    private Long strategyId;


    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }


}