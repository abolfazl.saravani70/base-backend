package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.NormCostJunctionBaseEntityDTO;

@ApiModel(value = "activity template norm cost dto Entity")
public class ActivityTemplateNormCostDTO extends NormCostJunctionBaseEntityDTO {

    private Long activityTemplateId;


    public Long getActivityTemplateId() {
        return activityTemplateId;
    }

    public void setActivityTemplateId(Long activityTemplateId) {
        this.activityTemplateId = activityTemplateId;
    }


}