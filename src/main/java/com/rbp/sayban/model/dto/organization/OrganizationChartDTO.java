package com.rbp.sayban.model.dto.organization;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import java.util.Set;

@ApiModel(value = "organization chart dto Entity")
public class OrganizationChartDTO extends BaseEntityDTO {

    private String name;
    private String code;
    private Boolean isVirtual;
    private Long pvTypeId;
    private Long pvOrgLevelId;
    private Long pvOrgDegreeId;
    private Long pvOrganizeTypeId;
    private Long pvPositionNumberId;
    private Long pvPositionTitleId;
    private Long parentId;
    private Set<Long> childrenIds;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }

    public Long getPvOrganizeTypeId() {
        return pvOrganizeTypeId;
    }

    public void setPvOrganizeTypeId(Long pvOrganizeTypeId) {
        this.pvOrganizeTypeId = pvOrganizeTypeId;
    }

    public Long getPvPositionNumberId() {
        return pvPositionNumberId;
    }

    public void setPvPositionNumberId(Long pvPositionNumberId) {
        this.pvPositionNumberId = pvPositionNumberId;
    }

    public Long getPvPositionTitleId() {
        return pvPositionTitleId;
    }

    public void setPvPositionTitleId(Long pvPositionTitleId) {
        this.pvPositionTitleId = pvPositionTitleId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Set<Long> getChildrenIds() {
        return childrenIds;
    }

    public void setChildren(Set<Long> childrenIds) {
        this.childrenIds = childrenIds;
    }


}