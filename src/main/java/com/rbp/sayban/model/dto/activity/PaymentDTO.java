package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "payment dto Entity")
public class PaymentDTO extends BaseEntityDTO {

    private Integer paymentTemplateId;
    private Integer paidAmount;
    private Integer totalAmount;
    private Integer installmentNumber;
    private Integer installmentList;
    private Long activityId;


    public Integer getPaymentTemplateId() {
        return paymentTemplateId;
    }

    public void setPaymentTemplateId(Integer paymentTemplateId) {
        this.paymentTemplateId = paymentTemplateId;
    }

    public Integer getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Integer paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public Integer getInstallmentList() {
        return installmentList;
    }

    public void setInstallmentList(Integer installmentList) {
        this.installmentList = installmentList;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}