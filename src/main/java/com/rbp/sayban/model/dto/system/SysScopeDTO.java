package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "sys scope dto Entity")
public class SysScopeDTO extends BaseEntityDTO {

    private Long systemId;


    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }


}