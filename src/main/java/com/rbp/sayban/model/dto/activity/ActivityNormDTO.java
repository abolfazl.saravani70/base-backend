package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
@Deprecated
@ApiModel(value = "activity norm dto Entity")
public class ActivityNormDTO extends BaseEntityDTO {

    private Long activityNormId;
    private Long activityTemplateId;
    private Long activityUnitId;
    private Long activityCostId;
    private Long activityId;


    public Long getActivityNormId() {
        return activityNormId;
    }

    public void setActivityNormId(Long activityNormId) {
        this.activityNormId = activityNormId;
    }

    public Long getActivityTemplateId() {
        return activityTemplateId;
    }

    public void setActivityTemplateId(Long activityTemplateId) {
        this.activityTemplateId = activityTemplateId;
    }

    public Long getActivityUnitId() {
        return activityUnitId;
    }

    public void setActivityUnitId(Long activityUnitId) {
        this.activityUnitId = activityUnitId;
    }

    public Long getActivityCostId() {
        return activityCostId;
    }

    public void setActivityCostId(Long activityCostId) {
        this.activityCostId = activityCostId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}