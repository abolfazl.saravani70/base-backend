package com.rbp.sayban.model.dto.goal;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "goal subjective dto Entity")
public class GoalSubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long goalId;


    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }


}