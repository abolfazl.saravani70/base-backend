package com.rbp.sayban.model.dto.activity;

import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.TreeBaseEntityDTO;
import com.rbp.sayban.model.dto.activity.NormCostDTO;

@ApiModel(value = "norm cost norm cost dto Entity")
public class NormCostNormCostDTO extends TreeBaseEntityDTO<NormCostDTO> {

    private NormCostDTO child;
    private NormCostDTO parent;

    @Override
    public NormCostDTO getChild() {
        return child;
    }

    @Override
    public void setChild(NormCostDTO child) {
        this.child = child;
    }

    @Override
    public NormCostDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(NormCostDTO parent) {
        this.parent = parent;
    }
}