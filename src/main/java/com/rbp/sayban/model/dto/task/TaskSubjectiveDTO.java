package com.rbp.sayban.model.dto.task;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "task subjective dto Entity")
public class TaskSubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long taskId;


    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }


}