package com.rbp.sayban.model.dto.report;

import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.report.GeneralReportFilterDTO;

@ApiModel(value = "report filter dto Entity")
public class ReportFilterDTO extends GeneralReportFilterDTO {

    private Long applicationId;


    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }


}