package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "request dto Entity")
public class RequestDTO extends BaseEntityDTO {

    private String subject;
    private String requestType;
    private String number;
    private Long orgUnitIdFrom;
    private Long orgUnitIdTo;
    private Long pvReasonId;
    private Long pvRejectReasonId;
    private Long pvStateId;
    private Long pvRequesterId;


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getOrgUnitIdFrom() {
        return orgUnitIdFrom;
    }

    public void setOrgUnitIdFrom(Long orgUnitIdFrom) {
        this.orgUnitIdFrom = orgUnitIdFrom;
    }

    public Long getOrgUnitIdTo() {
        return orgUnitIdTo;
    }

    public void setOrgUnitIdTo(Long orgUnitIdTo) {
        this.orgUnitIdTo = orgUnitIdTo;
    }

    public Long getPvReasonId() {
        return pvReasonId;
    }

    public void setPvReasonId(Long pvReasonId) {
        this.pvReasonId = pvReasonId;
    }

    public Long getPvRejectReasonId() {
        return pvRejectReasonId;
    }

    public void setPvRejectReasonId(Long pvRejectReasonId) {
        this.pvRejectReasonId = pvRejectReasonId;
    }

    public Long getPvStateId() {
        return pvStateId;
    }

    public void setPvStateId(Long pvStateId) {
        this.pvStateId = pvStateId;
    }

    public Long getPvRequesterId() {
        return pvRequesterId;
    }

    public void setPvRequesterId(Long pvRequesterId) {
        this.pvRequesterId = pvRequesterId;
    }


}