package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "formula parameter dto Entity")
public class FormulaParameterDTO extends BaseEntityDTO {

    private String name;
    private String code;
    private String value;
    private String type;
    private Boolean isNullable;
    private Boolean isOptional;
    private String base;
    private Long sysRuleFormulaId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsNullable() {
        return isNullable;
    }

    public void setIsNullable(Boolean isNullable) {
        this.isNullable = isNullable;
    }

    public Boolean getIsOptional() {
        return isOptional;
    }

    public void setIsOptional(Boolean isOptional) {
        this.isOptional = isOptional;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Long getSysRuleFormulaId() {
        return sysRuleFormulaId;
    }

    public void setSysRuleFormulaId(Long sysRuleFormulaId) {
        this.sysRuleFormulaId = sysRuleFormulaId;
    }


}