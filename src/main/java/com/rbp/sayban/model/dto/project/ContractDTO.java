package com.rbp.sayban.model.dto.project;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "contract dto Entity")
public class ContractDTO extends BaseEntityDTO {

    private String title;
    private String number;
    private String text;
    private String contractor;
    private String contractee;
    private Long numberOfSection;
    private Boolean isAttachment;
    private Boolean isComplement;
    private Long documentId;
    private Long numberCopy;
    private Long versionOfContract;
    private Long baseContractId;
    private Long projectId;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getContractee() {
        return contractee;
    }

    public void setContractee(String contractee) {
        this.contractee = contractee;
    }

    public Long getNumberOfSection() {
        return numberOfSection;
    }

    public void setNumberOfSection(Long numberOfSection) {
        this.numberOfSection = numberOfSection;
    }

    public Boolean getIsAttachment() {
        return isAttachment;
    }

    public void setIsAttachment(Boolean isAttachment) {
        this.isAttachment = isAttachment;
    }

    public Boolean getIsComplement() {
        return isComplement;
    }

    public void setIsComplement(Boolean isComplement) {
        this.isComplement = isComplement;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getNumberCopy() {
        return numberCopy;
    }

    public void setNumberCopy(Long numberCopy) {
        this.numberCopy = numberCopy;
    }

    public Long getVersionOfContract() {
        return versionOfContract;
    }

    public void setVersionOfContract(Long versionOfContract) {
        this.versionOfContract = versionOfContract;
    }

    public Long getBaseContractId() {
        return baseContractId;
    }

    public void setBaseContractId(Long baseContractId) {
        this.baseContractId = baseContractId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }


}