package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "performance indicator dto Entity")
public class PerformanceIndicatorDTO extends BaseEntityDTO {

    private String indicatorName;
    private Integer indexnumber;
    private Integer priority;
    private Integer bestValue;
    private String IndicatorType;
    private String keyword;
    private Integer value;
    private Long activityId;


    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public Integer getIndexnumber() {
        return indexnumber;
    }

    public void setIndexnumber(Integer indexnumber) {
        this.indexnumber = indexnumber;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getBestValue() {
        return bestValue;
    }

    public void setBestValue(Integer bestValue) {
        this.bestValue = bestValue;
    }

    public String getIndicatorType() {
        return IndicatorType;
    }

    public void setIndicatorType(String IndicatorType) {
        this.IndicatorType = IndicatorType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}