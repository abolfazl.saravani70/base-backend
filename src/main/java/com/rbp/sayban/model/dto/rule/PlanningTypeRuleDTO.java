package com.rbp.sayban.model.dto.rule;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "planning type rule dto Entity")
public class PlanningTypeRuleDTO extends BaseEntityDTO {

    private Long pvPlanningTypeId;
    private Long pvPlansTypeIdChild;


    public Long getPvPlanningTypeId() {
        return pvPlanningTypeId;
    }

    public void setPvPlanningTypeId(Long pvPlanningTypeId) {
        this.pvPlanningTypeId = pvPlanningTypeId;
    }

    public Long getPvPlansTypeIdChild() {
        return pvPlansTypeIdChild;
    }

    public void setPvPlansTypeIdChild(Long pvPlansTypeIdChild) {
        this.pvPlansTypeIdChild = pvPlansTypeIdChild;
    }


}