package com.rbp.sayban.model.dto.project;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "project dto Entity")
public class ProjectDTO extends EvalStateBaseEntityDTO {

    private Long pvTypeId;


    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }


}