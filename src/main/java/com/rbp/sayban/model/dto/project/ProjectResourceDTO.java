package com.rbp.sayban.model.dto.project;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "project resource dto Entity")
public class ProjectResourceDTO extends BaseEntityDTO {

    private String name;
    private String title;
    private String code;
    private Long pvProjectResourceTypeId;
    private Long projectResourceTypeId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPvProjectResourceTypeId() {
        return pvProjectResourceTypeId;
    }

    public void setPvProjectResourceTypeId(Long pvProjectResourceTypeId) {
        this.pvProjectResourceTypeId = pvProjectResourceTypeId;
    }

    public Long getProjectResourceTypeId() {
        return projectResourceTypeId;
    }

    public void setProjectResourceTypeId(Long projectResourceTypeId) {
        this.projectResourceTypeId = projectResourceTypeId;
    }


}