package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "duty dto Entity")
public class DutyDTO extends BaseEntityDTO {

    private String name;
    private String title;
    private String code;
    private String processName;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }


}