package com.rbp.sayban.model.dto.report;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "report dto Entity")
public class ReportDTO extends BaseEntityDTO {

    private String name;
    private String number;
    private String query;
    private String reportFile;
    private String reportPath;
    private Boolean isViewFlag;
    private Boolean isTable;
    private Boolean isOnline;
    private Boolean isStoreProcedure;
    private Boolean isDynamic;
    private Long viewId;
    private Long storeProcedureId;
    private Long appEntityId;
    private Long reportFilterId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public Boolean getIsViewFlag() {
        return isViewFlag;
    }

    public void setIsViewFlag(Boolean isViewFlag) {
        this.isViewFlag = isViewFlag;
    }

    public Boolean getIsTable() {
        return isTable;
    }

    public void setIsTable(Boolean isTable) {
        this.isTable = isTable;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

    public Boolean getIsStoreProcedure() {
        return isStoreProcedure;
    }

    public void setIsStoreProcedure(Boolean isStoreProcedure) {
        this.isStoreProcedure = isStoreProcedure;
    }

    public Boolean getIsDynamic() {
        return isDynamic;
    }

    public void setIsDynamic(Boolean isDynamic) {
        this.isDynamic = isDynamic;
    }

    public Long getViewId() {
        return viewId;
    }

    public void setViewId(Long viewId) {
        this.viewId = viewId;
    }

    public Long getStoreProcedureId() {
        return storeProcedureId;
    }

    public void setStoreProcedureId(Long storeProcedureId) {
        this.storeProcedureId = storeProcedureId;
    }

    public Long getAppEntityId() {
        return appEntityId;
    }

    public void setAppEntityId(Long appEntityId) {
        this.appEntityId = appEntityId;
    }

    public Long getReportFilterId() {
        return reportFilterId;
    }

    public void setReportFilterId(Long reportFilterId) {
        this.reportFilterId = reportFilterId;
    }


}