package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalDateTime;

@ApiModel(value = "license dto Entity")
public class LicenseDTO extends BaseEntityDTO {

    private LocalDate expiredDate;
    private LocalDate date;
    private String subject;
    private String number;
    private Long pvLicenseTypeId;
    private Long organizationId;
    private Long signingId;
    private Long documentId;


    public LocalDate getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(LocalDate expiredDate) {
        this.expiredDate = expiredDate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getPvLicenseTypeId() {
        return pvLicenseTypeId;
    }

    public void setPvLicenseTypeId(Long pvLicenseTypeId) {
        this.pvLicenseTypeId = pvLicenseTypeId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getSigningId() {
        return signingId;
    }

    public void setSigningId(Long signingId) {
        this.signingId = signingId;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }


}