package com.rbp.sayban.model.dto.todo;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.task.TaskDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.todo.TodoDTO;

@ApiModel(value = "todo tree dto Entity")
public class TodoTreeDTO extends RootBaseEntityDTO<TodoDTO,TaskDTO> {

    private TaskDTO root;
    private TodoDTO child;
    private TodoDTO parent;

    @Override
    public TaskDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(TaskDTO root) {
        this.root = root;
    }

    @Override
    public TodoDTO getChild() {
        return child;
    }

    @Override
    public void setChild(TodoDTO child) {
        this.child = child;
    }

    @Override
    public TodoDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(TodoDTO parent) {
        this.parent = parent;
    }
}