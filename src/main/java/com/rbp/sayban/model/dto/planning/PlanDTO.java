package com.rbp.sayban.model.dto.planning;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "plan dto Entity")
public class PlanDTO extends EvalStateBaseEntityDTO {

    private Long pvTypeId;


    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }


}