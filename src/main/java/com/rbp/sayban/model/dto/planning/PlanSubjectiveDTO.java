package com.rbp.sayban.model.dto.planning;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "plan subjective dto Entity")
public class PlanSubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long planId;


    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }


}