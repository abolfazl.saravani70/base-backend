package com.rbp.sayban.model.dto.budgeting;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "expenditure dto Entity")
public class ExpenditureDTO extends BaseEntityDTO {

   // private Long activityPerformanceId;
    private Long taskId;
    private Long todoId;


//    public Long getActivityPerformanceId() {
//        return activityPerformanceId;
//    }
//
//    public void setActivityPerformanceId(Long activityPerformanceId) {
//        this.activityPerformanceId = activityPerformanceId;
//    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getTodoId() {
        return todoId;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }


}