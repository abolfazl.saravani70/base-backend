package com.rbp.sayban.model.dto.planning;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.goal.GoalDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.planning.PlanDTO;

@ApiModel(value = "plan tree dto Entity")
public class PlanTreeDTO extends RootBaseEntityDTO<PlanDTO,GoalDTO> {

    private GoalDTO root;
    private PlanDTO child;
    private PlanDTO parent;

    @Override
    public GoalDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(GoalDTO root) {
        this.root = root;
    }

    @Override
    public PlanDTO getChild() {
        return child;
    }

    @Override
    public void setChild(PlanDTO child) {
        this.child = child;
    }

    @Override
    public PlanDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(PlanDTO parent) {
        this.parent = parent;
    }
}