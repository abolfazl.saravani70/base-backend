package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalDateTime;

@ApiModel(value = "sys rule dto Entity")
public class SysRuleDTO extends BaseEntityDTO {

    private String title;
    private String name;
    private String code;
    private LocalDate startDate;
    private LocalDate expirationDate;
    private Long sysRuleFormulaId;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getSysRuleFormulaId() {
        return sysRuleFormulaId;
    }

    public void setSysRuleFormulaId(Long sysRuleFormulaId) {
        this.sysRuleFormulaId = sysRuleFormulaId;
    }


}