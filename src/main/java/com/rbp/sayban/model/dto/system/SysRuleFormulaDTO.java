package com.rbp.sayban.model.dto.system;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "sys rule formula dto Entity")
public class SysRuleFormulaDTO extends BaseEntityDTO {

    private String key;
    private String value;
    private Long sysRuleId;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getSysRuleId() {
        return sysRuleId;
    }

    public void setSysRuleId(Long sysRuleId) {
        this.sysRuleId = sysRuleId;
    }


}