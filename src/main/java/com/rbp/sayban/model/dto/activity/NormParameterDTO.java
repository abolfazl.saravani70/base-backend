package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "norm parameter dto Entity")
public class NormParameterDTO extends BaseEntityDTO {

    private Long normCostId;
    private Long pvMesureUnitId;


    public Long getNormCostId() {
        return normCostId;
    }

    public void setNormCostId(Long normCostId) {
        this.normCostId = normCostId;
    }

    public Long getPvMesureUnitId() {
        return pvMesureUnitId;
    }

    public void setPvMesureUnitId(Long pvMesureUnitId) {
        this.pvMesureUnitId = pvMesureUnitId;
    }


}