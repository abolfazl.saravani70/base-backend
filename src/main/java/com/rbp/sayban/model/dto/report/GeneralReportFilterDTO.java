package com.rbp.sayban.model.dto.report;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "general report filter dto Entity")
public class GeneralReportFilterDTO extends BaseEntityDTO {

    private String name;
    private String query;
    private String number;
    private String defualtValue;
    private Long filterOperatorId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDefualtValue() {
        return defualtValue;
    }

    public void setDefualtValue(String defualtValue) {
        this.defualtValue = defualtValue;
    }

    public Long getFilterOperatorId() {
        return filterOperatorId;
    }

    public void setFilterOperatorId(Long filterOperatorId) {
        this.filterOperatorId = filterOperatorId;
    }


}