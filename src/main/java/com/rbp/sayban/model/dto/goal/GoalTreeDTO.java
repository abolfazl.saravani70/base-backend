package com.rbp.sayban.model.dto.goal;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.goal.GoalDTO;

@ApiModel(value = "goal tree dto Entity")
public class GoalTreeDTO extends RootBaseEntityDTO<GoalDTO,StrategyDTO> {

    private StrategyDTO root;
    private GoalDTO child;
    private GoalDTO parent;

    @Override
    public StrategyDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(StrategyDTO root) {
        this.root = root;
    }

    @Override
    public GoalDTO getChild() {
        return child;
    }

    @Override
    public void setChild(GoalDTO child) {
        this.child = child;
    }

    @Override
    public GoalDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(GoalDTO parent) {
        this.parent = parent;
    }
}