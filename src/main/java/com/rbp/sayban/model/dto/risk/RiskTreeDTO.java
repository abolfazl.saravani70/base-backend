package com.rbp.sayban.model.dto.risk;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.TreeBaseEntityDTO;
import com.rbp.sayban.model.dto.risk.RiskDTO;

@ApiModel(value = "risk tree dto Entity")
public class RiskTreeDTO extends TreeBaseEntityDTO<RiskDTO> {

    private RiskDTO child;
    private RiskDTO parent;

    @Override
    public RiskDTO getChild() {
        return child;
    }

    @Override
    public void setChild(RiskDTO child) {
        this.child = child;
    }

    @Override
    public RiskDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(RiskDTO parent) {
        this.parent = parent;
    }
}