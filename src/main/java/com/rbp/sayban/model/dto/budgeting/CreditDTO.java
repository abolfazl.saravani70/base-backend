package com.rbp.sayban.model.dto.budgeting;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "credit dto Entity")
public class CreditDTO extends BaseEntityDTO {

    private Long fundSharingId;
    private Long allocationId;


    public Long getFundSharingId() {
        return fundSharingId;
    }

    public void setFundSharingId(Long fundSharingId) {
        this.fundSharingId = fundSharingId;
    }

    public Long getAllocationId() {
        return allocationId;
    }

    public void setAllocationId(Long allocationId) {
        this.allocationId = allocationId;
    }


}