package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "activity dto Entity")
public class ActivityDTO extends EvalStateBaseEntityDTO {

    private Long pvTypeId;

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }


}