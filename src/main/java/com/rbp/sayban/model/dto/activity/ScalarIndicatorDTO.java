package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "scalar indicator dto Entity")
public class ScalarIndicatorDTO extends BaseEntityDTO {

    private String indicatorName;
    private Integer indexNumber;
    private Integer priority;
    private String bestValue;
    private String indicatorType;
    private String keyword;
    private String value;
    private Long activityId;


    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public Integer getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(Integer indexNumber) {
        this.indexNumber = indexNumber;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getBestValue() {
        return bestValue;
    }

    public void setBestValue(String bestValue) {
        this.bestValue = bestValue;
    }

    public String getIndicatorType() {
        return indicatorType;
    }

    public void setIndicatorType(String indicatorType) {
        this.indicatorType = indicatorType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}