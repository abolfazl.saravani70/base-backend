package com.rbp.sayban.model.dto.todo;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "todo subjective dto Entity")
public class TodoSubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long todoId;


    public Long getTodoId() {
        return todoId;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }


}