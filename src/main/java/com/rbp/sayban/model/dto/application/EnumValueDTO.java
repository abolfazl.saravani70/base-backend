package com.rbp.sayban.model.dto.application;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "enum value dto Entity")
public class EnumValueDTO extends BaseEntityDTO {

    private Boolean isObsolete;
    private Long appEnumId;


    public Boolean getIsObsolete() {
        return isObsolete;
    }

    public void setIsObsolete(Boolean isObsolete) {
        this.isObsolete = isObsolete;
    }

    public Long getAppEnumId() {
        return appEnumId;
    }

    public void setAppEnumId(Long appEnumId) {
        this.appEnumId = appEnumId;
    }


}