package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "activity quality dto Entity")
public class ActivityQualityDTO extends BaseEntityDTO {

    private Integer qualityIndicator;
    private String value;
    private Long activityId;


    public Integer getQualityIndicator() {
        return qualityIndicator;
    }

    public void setQualityIndicator(Integer qualityIndicator) {
        this.qualityIndicator = qualityIndicator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }


}