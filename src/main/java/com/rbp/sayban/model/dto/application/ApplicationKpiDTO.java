package com.rbp.sayban.model.dto.application;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "application kpi dto Entity")
public class ApplicationKpiDTO extends BaseEntityDTO {

    private String appKpiName;
    private Integer appKpiKey;
    private String appKpiCode;
    private String appKpiValue;
    private String appKpiBestValue;
    private Integer appKpiPriority;
    private Long applicationId;


    public String getAppKpiName() {
        return appKpiName;
    }

    public void setAppKpiName(String appKpiName) {
        this.appKpiName = appKpiName;
    }

    public Integer getAppKpiKey() {
        return appKpiKey;
    }

    public void setAppKpiKey(Integer appKpiKey) {
        this.appKpiKey = appKpiKey;
    }

    public String getAppKpiCode() {
        return appKpiCode;
    }

    public void setAppKpiCode(String appKpiCode) {
        this.appKpiCode = appKpiCode;
    }

    public String getAppKpiValue() {
        return appKpiValue;
    }

    public void setAppKpiValue(String appKpiValue) {
        this.appKpiValue = appKpiValue;
    }

    public String getAppKpiBestValue() {
        return appKpiBestValue;
    }

    public void setAppKpiBestValue(String appKpiBestValue) {
        this.appKpiBestValue = appKpiBestValue;
    }

    public Integer getAppKpiPriority() {
        return appKpiPriority;
    }

    public void setAppKpiPriority(Integer appKpiPriority) {
        this.appKpiPriority = appKpiPriority;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }


}