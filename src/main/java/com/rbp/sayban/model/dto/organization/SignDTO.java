package com.rbp.sayban.model.dto.organization;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.rbp.sayban.model.domainmodel.system.Blob;

@ApiModel(value = "sign dto Entity")
public class SignDTO extends BaseEntityDTO {

    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Long userId;
    private Blob image;


    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }


}