package com.rbp.sayban.model.dto.application;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "test dto Entity")
public class TestDTO extends BaseEntityDTO {

    private String testName;
    private String testCode;
    private String testResult;
    private Integer testCaseID;
    private Long applicationId;


    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTestCode() {
        return testCode;
    }

    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public Integer getTestCaseID() {
        return testCaseID;
    }

    public void setTestCaseID(Integer testCaseID) {
        this.testCaseID = testCaseID;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }


}