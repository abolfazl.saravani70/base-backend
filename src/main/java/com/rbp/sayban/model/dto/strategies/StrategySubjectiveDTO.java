package com.rbp.sayban.model.dto.strategies;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "strategy subjective dto Entity")
public class StrategySubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long strategyId;


    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }


}