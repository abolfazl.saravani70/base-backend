package com.rbp.sayban.model.dto.project;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "project subjective dto Entity")
public class ProjectSubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long projectId;


    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }


}