package com.rbp.sayban.model.dto.organization;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import java.util.Set;
import java.util.List;

@ApiModel(value = "organization dto Entity")
public class OrganizationDTO extends BaseEntityDTO {

    private String name;
    private String title;
    private String orgCode;
    private String zamzamCode;
    private String strategicCode;
    private Boolean isOrganized;
    private Long pvOrgLevelId;
    private Long pvOrgDegreeId;
    private Long pvOrgTypeId;
    private Set<Long> organizationsIds;
    private Long organizationId;
    private Long stakeholderId;
    private List<Long> orgAccountIds;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getZamzamCode() {
        return zamzamCode;
    }

    public void setZamzamCode(String zamzamCode) {
        this.zamzamCode = zamzamCode;
    }

    public String getStrategicCode() {
        return strategicCode;
    }

    public void setStrategicCode(String strategicCode) {
        this.strategicCode = strategicCode;
    }

    public Boolean getIsOrganized() {
        return isOrganized;
    }

    public void setIsOrganized(Boolean isOrganized) {
        this.isOrganized = isOrganized;
    }

    public Long getPvOrgLevelId() {
        return pvOrgLevelId;
    }

    public void setPvOrgLevelId(Long pvOrgLevelId) {
        this.pvOrgLevelId = pvOrgLevelId;
    }

    public Long getPvOrgDegreeId() {
        return pvOrgDegreeId;
    }

    public void setPvOrgDegreeId(Long pvOrgDegreeId) {
        this.pvOrgDegreeId = pvOrgDegreeId;
    }

    public Long getPvOrgTypeId() {
        return pvOrgTypeId;
    }

    public void setPvOrgTypeId(Long pvOrgTypeId) {
        this.pvOrgTypeId = pvOrgTypeId;
    }

    public Set<Long> getOrganizationsIds() {
        return organizationsIds;
    }

    public void setOrganizations(Set<Long> organizationsIds) {
        this.organizationsIds = organizationsIds;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(Long stakeholderId) {
        this.stakeholderId = stakeholderId;
    }

    public List<Long> getOrgAccountIds() {
        return orgAccountIds;
    }

    public void setOrgAccount(List<Long> orgAccountIds) {
        this.orgAccountIds = orgAccountIds;
    }


}