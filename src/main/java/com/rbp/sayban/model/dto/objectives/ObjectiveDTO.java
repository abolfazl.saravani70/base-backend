package com.rbp.sayban.model.dto.objectives;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "objective dto Entity")
public class ObjectiveDTO extends EvalStateBaseEntityDTO {

    private Long pvTypeId;


    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }


}