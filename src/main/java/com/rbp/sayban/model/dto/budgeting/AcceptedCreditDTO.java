package com.rbp.sayban.model.dto.budgeting;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalDateTime;

@ApiModel(value = "accepted credit dto Entity")
public class AcceptedCreditDTO extends BaseEntityDTO {

    private Long limitAmount;
    private Long amount;
    private LocalDate startDate;
    private LocalDate endDate;
    private String title;
    private String letterNumber;
    private Long fundSharingId;


    public Long getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(Long limitAmount) {
        this.limitAmount = limitAmount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLetterNumber() {
        return letterNumber;
    }

    public void setLetterNumber(String letterNumber) {
        this.letterNumber = letterNumber;
    }

    public Long getFundSharingId() {
        return fundSharingId;
    }

    public void setFundSharingId(Long fundSharingId) {
        this.fundSharingId = fundSharingId;
    }


}