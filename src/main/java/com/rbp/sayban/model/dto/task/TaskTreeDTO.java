package com.rbp.sayban.model.dto.task;

import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.task.TaskDTO;

@ApiModel(value = "task tree dto Entity")
public class TaskTreeDTO extends RootBaseEntityDTO<TaskDTO,ActivityDTO> {

    private ActivityDTO root;
    private TaskDTO child;
    private TaskDTO parent;

    @Override
    public ActivityDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(ActivityDTO root) {
        this.root = root;
    }

    @Override
    public TaskDTO getChild() {
        return child;
    }

    @Override
    public void setChild(TaskDTO child) {
        this.child = child;
    }

    @Override
    public TaskDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(TaskDTO parent) {
        this.parent = parent;
    }
}