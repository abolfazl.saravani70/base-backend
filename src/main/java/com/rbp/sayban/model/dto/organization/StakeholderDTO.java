package com.rbp.sayban.model.dto.organization;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;
import java.util.Set;
import java.util.Set;
import java.util.Set;

@ApiModel(value = "stakeholder dto Entity")
public class StakeholderDTO extends BaseEntityDTO {

    private String type;
    private Boolean isPerson;
    private Long userId;
    private Set<Long> emailsIds;
    private Set<Long> addressesIds;
    private Set<Long> phonesIds;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getIsPerson() {
        return isPerson;
    }

    public void setIsPerson(Boolean isPerson) {
        this.isPerson = isPerson;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<Long> getEmailsIds() {
        return emailsIds;
    }

    public void setEmails(Set<Long> emailsIds) {
        this.emailsIds = emailsIds;
    }

    public Set<Long> getAddressesIds() {
        return addressesIds;
    }

    public void setAddresses(Set<Long> addressesIds) {
        this.addressesIds = addressesIds;
    }

    public Set<Long> getPhonesIds() {
        return phonesIds;
    }

    public void setPhones(Set<Long> phonesIds) {
        this.phonesIds = phonesIds;
    }


}