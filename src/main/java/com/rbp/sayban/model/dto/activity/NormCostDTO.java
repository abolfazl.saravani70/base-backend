package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "norm cost dto Entity")
public class NormCostDTO extends BaseEntityDTO {

    private String name;
    private String title;
    private Long cost;
    private Long rate;
    private Integer prioritory;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public Integer getPrioritory() {
        return prioritory;
    }

    public void setPrioritory(Integer prioritory) {
        this.prioritory = prioritory;
    }


}