package com.rbp.sayban.model.dto.risk;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "risk dto Entity")
public class RiskDTO extends BaseEntityDTO {

    private Long alternativeId;


    public Long getAlternativeId() {
        return alternativeId;
    }

    public void setAlternativeId(Long alternativeId) {
        this.alternativeId = alternativeId;
    }


}