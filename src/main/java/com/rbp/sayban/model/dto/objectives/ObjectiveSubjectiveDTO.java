package com.rbp.sayban.model.dto.objectives;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.SubjectiveJunctionBaseEntityDTO;

@ApiModel(value = "objective subjective dto Entity")
public class ObjectiveSubjectiveDTO extends SubjectiveJunctionBaseEntityDTO {

    private Long objectiveId;


    public Long getObjectiveId() {
        return objectiveId;
    }

    public void setObjectiveId(Long objectiveId) {
        this.objectiveId = objectiveId;
    }


}