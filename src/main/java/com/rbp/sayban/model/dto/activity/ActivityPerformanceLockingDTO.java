package com.rbp.sayban.model.dto.activity;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "activity performance locking dto Entity")
public class ActivityPerformanceLockingDTO extends BaseEntityDTO {

    private Boolean isLock;
    private Long activityPerformanceId;


    public Boolean getIsLock() {
        return isLock;
    }

    public void setIsLock(Boolean isLock) {
        this.isLock = isLock;
    }

    public Long getActivityPerformanceId() {
        return activityPerformanceId;
    }

    public void setActivityPerformanceId(Long activityPerformanceId) {
        this.activityPerformanceId = activityPerformanceId;
    }


}