package com.rbp.sayban.model.dto.organization;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.time.LocalDateTime;

@ApiModel(value = "group scheduling dto Entity")
public class GroupSchedulingDTO extends BaseEntityDTO {

    private String name;
    private String action;
    private LocalDate initDate;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer numberOfRepeat;
    private String repeatable;
    private String type;
    private String dbJob;
    private Long groupId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDate getInitDate() {
        return initDate;
    }

    public void setInitDate(LocalDate initDate) {
        this.initDate = initDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getNumberOfRepeat() {
        return numberOfRepeat;
    }

    public void setNumberOfRepeat(Integer numberOfRepeat) {
        this.numberOfRepeat = numberOfRepeat;
    }

    public String getRepeatable() {
        return repeatable;
    }

    public void setRepeatable(String repeatable) {
        this.repeatable = repeatable;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDbJob() {
        return dbJob;
    }

    public void setDbJob(String dbJob) {
        this.dbJob = dbJob;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }


}