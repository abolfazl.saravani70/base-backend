package com.rbp.sayban.model.dto.objectives;

import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;

@ApiModel(value = "objective tree dto Entity")
public class ObjectiveTreeDTO extends RootBaseEntityDTO<ObjectiveDTO,ObjectiveDTO> {

    private ObjectiveDTO root;
    private ObjectiveDTO child;
    private ObjectiveDTO parent;

    @Override
    public ObjectiveDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(ObjectiveDTO root) {
        this.root = root;
    }

    @Override
    public ObjectiveDTO getChild() {
        return child;
    }

    @Override
    public void setChild(ObjectiveDTO child) {
        this.child = child;
    }

    @Override
    public ObjectiveDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(ObjectiveDTO parent) {
        this.parent = parent;
    }
}