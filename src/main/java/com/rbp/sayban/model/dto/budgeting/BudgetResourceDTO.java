package com.rbp.sayban.model.dto.budgeting;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.EvalStateBaseEntityDTO;

@ApiModel(value = "budget resource dto Entity")
public class BudgetResourceDTO extends EvalStateBaseEntityDTO {

    private Long budgetResourceId;
    private Long budgetTypeId;

    public Long getBudgetResourceId() {
        return budgetResourceId;
    }

    public void setBudgetResourceId(Long budgetResourceId) {
        this.budgetResourceId = budgetResourceId;
    }

    public Long getBudgetTypeId() {
        return budgetTypeId;
    }

    public void setBudgetTypeId(Long budgetTypeId) {
        this.budgetTypeId = budgetTypeId;
    }


}