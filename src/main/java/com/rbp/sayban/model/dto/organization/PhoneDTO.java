package com.rbp.sayban.model.dto.organization;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.BaseEntityDTO;

@ApiModel(value = "phone dto Entity")
public class PhoneDTO extends BaseEntityDTO {

    private String number;
    private String cityPrecodeNumber;
    private Boolean isMain;
    private Long pvTypeId;
    private Long stakeholderId;


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCityPrecodeNumber() {
        return cityPrecodeNumber;
    }

    public void setCityPrecodeNumber(String cityPrecodeNumber) {
        this.cityPrecodeNumber = cityPrecodeNumber;
    }

    public Boolean getIsMain() {
        return isMain;
    }

    public void setIsMain(Boolean isMain) {
        this.isMain = isMain;
    }

    public Long getPvTypeId() {
        return pvTypeId;
    }

    public void setPvTypeId(Long pvTypeId) {
        this.pvTypeId = pvTypeId;
    }

    public Long getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(Long stakeholderId) {
        this.stakeholderId = stakeholderId;
    }


}