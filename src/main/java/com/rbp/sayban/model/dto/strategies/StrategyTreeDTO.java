package com.rbp.sayban.model.dto.strategies;

import com.rbp.sayban.model.dto.activity.ActivityDTO;
import com.rbp.sayban.model.dto.project.ProjectDTO;
import io.swagger.annotations.ApiModel;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;
import com.rbp.core.model.dto.base.abstractClass.RootBaseEntityDTO;
import com.rbp.sayban.model.dto.strategies.StrategyDTO;

@ApiModel(value = "strategy tree dto Entity")
public class StrategyTreeDTO extends RootBaseEntityDTO<StrategyDTO,ObjectiveDTO> {

    private ObjectiveDTO root;
    private StrategyDTO child;
    private StrategyDTO parent;

    @Override
    public ObjectiveDTO getRoot() {
        return root;
    }

    @Override
    public void setRoot(ObjectiveDTO root) {
        this.root = root;
    }

    @Override
    public StrategyDTO getChild() {
        return child;
    }

    @Override
    public void setChild(StrategyDTO child) {
        this.child = child;
    }

    @Override
    public StrategyDTO getParent() {
        return parent;
    }

    @Override
    public void setParent(StrategyDTO parent) {
        this.parent = parent;
    }
}