package com.rbp.sayban.model.dto.strategies;

import io.swagger.annotations.ApiModel;
import com.rbp.core.model.dto.base.abstractClass.junction.NormCostJunctionBaseEntityDTO;

@ApiModel(value = "strategy norm cost dto Entity")
public class StrategyNormCostDTO extends NormCostJunctionBaseEntityDTO {

    private Long strategyId;


    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }


}