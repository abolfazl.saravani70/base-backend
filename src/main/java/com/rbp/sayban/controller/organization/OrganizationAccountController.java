package com.rbp.sayban.controller.organization;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.organization.OrganizationAccount;
import com.rbp.sayban.model.dto.organization.OrganizationAccountDTO;
import com.rbp.sayban.service.organization.impl.OrganizationAccountService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.ORGANIZATION_ACCOUNTS )
@Api(description = "Operations pertaining to organization account in Organization Account Management System")
public class OrganizationAccountController extends FrameworkAbstractController<OrganizationAccount, OrganizationAccountDTO, OrganizationAccountService> {
}
