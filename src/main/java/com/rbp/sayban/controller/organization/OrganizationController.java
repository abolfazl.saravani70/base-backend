/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.organization;

import com.rbp.core.controller.base.FrameWorkAbstractTreeController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.domainmodel.basicInformation.geographical.Zone;
import com.rbp.core.service.basicInformation.geographical.IZoneService;
import com.rbp.sayban.model.domainmodel.organization.*;
import com.rbp.sayban.model.dto.organization.OrgTreeDTO;
import com.rbp.sayban.model.viewModel.organization.OrganizationViewModel;
import com.rbp.sayban.service.organization.*;
import com.rbp.sayban.service.organization.impl.OrgTreeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.ORGANIZATIONS)
@Api(description = "Operations pertaining to organization in Organization Management System")
public class OrganizationController extends FrameWorkAbstractTreeController<OrgTree, OrgTreeDTO, OrgTreeService> {

    @Autowired
    IOrganizationService iOrganizationService;

    @Autowired
    IOrganizationAccountService iOrganizationAccountService;

    @Autowired
    IStakeholderService iStakeholderService;

    @Autowired
    IZoneService iZoneService;

    @Autowired
    IAddressService iAddressService;

    @Autowired
    IEmailService iEmailService;

    @Autowired
    IPhoneService iPhoneService;

//    @Override
//    protected ActionResult saveAndUpdate(JsonInput<OrganizationViewModel> jsonInput) {
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().saveAndUpdate", null, null, "Method is starting....");
//        OrganizationViewModel entity = jsonInput.getData();
//        Organization model = getMapper().getMapper(getViewModel(), getModel()).toDomainModel(entity);
//        Stakeholder stakeholder;
//
//        if (entity.getId() == 0) {
//            stakeholder = saveStakeholder();
//            saveAndUpdateForAddress(stakeholder, entity);
//            saveAndUpdateForEmail(stakeholder, entity);
//            saveAndUpdateForPhone(stakeholder, entity);
//        } else {
//            Organization existsOrg = iOrganizationService.loadById(entity.getId());
//            stakeholder = existsOrg.getStakeholder();
//            saveAndUpdateForAddress(stakeholder, entity);
//            saveAndUpdateForEmail(stakeholder, entity);
//            saveAndUpdateForEmail(stakeholder, entity);
//        }
//        model.setStakeholder(stakeholder);
//        //model.setOrganization(iOrganizationService.loadById(entity.getParentId()));
////        model.setOrgAccount(iOrganizationAccountService.);
//        getService().save(model);
//        OrganizationViewModel result = getMapper().getMapper(getViewModel(), getModel()).toView(model);
//        List<Object> objectList = Arrays.asList(result);
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().saveAndUpdate", null, null, "Method finish successfull!!!!");
//        return new ActionResult(objectList, 0, 0, 0, 0, "", "");
//    }
//
//    @Override
//    protected ActionResult loadById(JsonInput<OrganizationViewModel> jsonInput) {
//        if (jsonInput.getFieldId() == null) {
//            // TODO: 7/16/2019 throw exception
//            throw new ApplicationException(0, "لطفاً داده مورد نظر را وارد کنید");
//        }
//
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().loadById", null, null, "Method is starting....");
//        //List<viewModel> objectList=(List<viewModel>) ModelMapper.mapList(getDuty().loadById(id),this.getViewModel());
//        // List<viewModel> objectList = mapper.getMapper(getViewModel(), getModel()).toView(getService().loadById(entityId));
//        Organization org = getService().loadById(jsonInput.getFieldId());
//        OrganizationViewModel object = getMapper().getMapper(getViewModel(), getModel()).toView(org);
//        List<Object> objectList = new ArrayList<>();
//
//        if (object == null) {
//            getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().loadById", null, null, "Such an entity does not exist with this ID");
//            return new ActionResult(objectList, 0, 0, 0, 0, "داده مورد نظر یافت نشد", "");
//        }
//
//        objectList.add(fillOrgVm(object, org.getStakeholder().getId()));
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), getViewModel().getSimpleName(), "getService().loadById", null, null, "Method finish successfull!!!!");
//        return new ActionResult(objectList, 0, 0, 0, 0, "داده مورد نظر یافت شد", "");
//    }
//
//    @Override
//    protected ActionResult delete(JsonInput<OrganizationViewModel> jsonInput) {
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), OrganizationViewModel.class.getSimpleName(), "getService().delete()", null, null, "Method is starting....");
//        iOrganizationService.deleteOrg(jsonInput.getFieldId(), jsonInput.getFilter());
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), OrganizationViewModel.class.getSimpleName(), "getService().delete()", null, null, "Method finish successfull!!!!");
//        return new ActionResult(null, 0, 0, 0, 0, "حذف با موفقیت انجام شد.", "");
//    }



    private Stakeholder saveStakeholder() {
        Stakeholder stakeholder = new Stakeholder();
        stakeholder.setId(0L);
        stakeholder.setIsPerson(false);
        return iStakeholderService.save(stakeholder);
    }

    private Address saveAndUpdateForAddress(Stakeholder stakeholder, OrganizationViewModel entity) {
        Address address = new Address();
        Zone zone = iZoneService.getReference(entity.getZoneId());

        if (entity.getId() == 0) {

            address.setId(0L);
            address.setPvTypeId(25L);// TODO: 8/4/2019
            address.setAddressText(entity.getAddressText());
            address.setStakeholder(stakeholder);
            address.setZone(zone);
            address = iAddressService.save(address);
            return address;
        } else {
            address = stakeholder.getAddresses().stream().findFirst().get();
            address.setZone(zone);
            address.setAddressText(entity.getAddressText());
            address.setPostalCode(entity.getPostalCode());
            return iAddressService.save(address);
        }
    }

    private Email saveAndUpdateForEmail(Stakeholder stakeholder, OrganizationViewModel entity) {
        Email email = new Email();
        if (entity.getId() == 0) {

            email.setId(0L);
            email.setPvTypeId(33L);// TODO: 8/4/2019
            email.setEmailAddress(entity.getEmail());
            email.setStakeholder(stakeholder);
            email = iEmailService.save(email);
            return email;
        } else {
            email = stakeholder.getEmails().stream().findFirst().get();
            email.setEmailAddress(entity.getEmail());
            return iEmailService.save(email);
        }
    }

    private Phone saveAndUpdateForPhone(Stakeholder stakeholder, OrganizationViewModel entity) {
        Phone phone = new Phone();
        if (entity.getId() == 0) {
            phone.setId(0L);
            phone.setPvTypeId(31L);// TODO: 8/4/2019
            phone.setCityPrecodeNumber("021");
            phone.setNumber(entity.getPhone());
            phone.setStakeholder(stakeholder);
            phone = iPhoneService.save(phone);
            return phone;
        } else {
            phone = stakeholder.getPhones().stream().findFirst().get();
            phone.setNumber(entity.getPhone());
            return iPhoneService.save(phone);
        }
    }

    private OrganizationViewModel fillOrgVm(OrganizationViewModel object, Long stakeholderId) {
        Stakeholder stakeholder = iStakeholderService.loadById(stakeholderId);
        Set<Email> emails = stakeholder.getEmails();
        Set<Address> addresses = stakeholder.getAddresses();
        Set<Phone> phones = stakeholder.getPhones();

        Address address = addresses.stream().findFirst().get();
//        object.setAddressType(address.getPvTypeId().toString());
        object.setAddressText(address.getAddressText());
        object.setPostalCode(address.getPostalCode());
        object.setZone(address.getZone().getName());
        object.setZoneId(address.getZone().getId());
        object.setVillage(address.getZone().getVillage().getName());
        object.setVillageId(address.getZone().getVillage().getId());
        object.setCity(address.getZone().getVillage().getCity().getName());
        object.setCityId(address.getZone().getVillage().getCity().getId());
        object.setState(address.getZone().getVillage().getCity().getState().getName());
        object.setStateId(address.getZone().getVillage().getCity().getState().getId());

        Email email = emails.stream().findFirst().get();
        object.setEmail(email.getEmailAddress());
        object.setIsActiveEmail(email.getIsActive());

        Phone phone = phones.stream().findFirst().get();
        object.setPhone(phone.getNumber());
        return object;
    }


}
