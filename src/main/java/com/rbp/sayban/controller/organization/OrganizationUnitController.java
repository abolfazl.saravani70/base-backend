/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.organization;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.sayban.model.domainmodel.organization.OrganizationUnit;
import com.rbp.sayban.model.dto.organization.OrganizationUnitDTO;
import com.rbp.sayban.model.viewModel.organization.OrganizationUnitViewModel;
import com.rbp.sayban.model.viewModel.organization.OrganizationViewModel;
import com.rbp.sayban.service.organization.IOrganizationUnitService;
import com.rbp.sayban.service.organization.impl.OrganizationUnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.ORGANIZATION_UNITS )
@Api(description = "Operations pertaining to organization unit in Organization Unit Management System")
public class OrganizationUnitController extends FrameworkAbstractController<OrganizationUnit, OrganizationUnitDTO, OrganizationUnitService> {

//    @Autowired
//    IOrganizationUnitService iOrganizationUnitService;
//
//    @PostMapping(restConstant.ORGANIZATION_UNITS_GENERATOR)
//    @ApiOperation("Generate Organization unit per organization")
//    public ActionResult generateOUList(@RequestBody JsonInput<OrganizationUnitViewModel> jsonInput){
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), OrganizationViewModel.class.getSimpleName(), "getService().generateOUList()", null, null, "Method is starting....");
//        List<OrganizationUnit> ouPerOrg;
//        List<OrganizationUnitViewModel> result;
//        ouPerOrg = iOrganizationUnitService.findByOrgId(jsonInput.getData().getOrganizationId(),jsonInput.getPageNumber(),jsonInput.getPageSize());
//
//        if(ouPerOrg.size() == 0) {
//            iOrganizationUnitService.generateOuPerOrg(jsonInput.getData().getOrganizationId(),jsonInput.getData().getOrganizationChartId());
//            ouPerOrg = iOrganizationUnitService.findByOrgId(jsonInput.getData().getOrganizationId(),jsonInput.getPageNumber(),jsonInput.getPageSize());
//        }
//
//        result = getMapper().getMapper(OrganizationUnitViewModel.class, OrganizationUnit.class).toView(ouPerOrg);
//        getApplicationLogger().printLog("info", OrganizationController.class, getModel().getSimpleName(), OrganizationViewModel.class.getSimpleName(), "getService().generateOUList()", null, null, "Method finish successfull!!!!");
//        return new ActionResult( (List<Object>)(List<?>) result, 0, 0, 0, 0, "ثبت با موفقیت انجام شد", "");
//    }
}
