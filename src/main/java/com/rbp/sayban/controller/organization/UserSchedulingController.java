/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.organization;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.organization.UserScheduling;
import com.rbp.sayban.model.dto.organization.UserSchedulingDTO;
import com.rbp.sayban.service.organization.impl.UserSchedulingService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.USER_SCHEDULING_LISTS )
@Api(description = "Operations pertaining to user schaduling list in User Schaduling List Management System")
public class UserSchedulingController extends FrameworkAbstractController<UserScheduling, UserSchedulingDTO, UserSchedulingService> {
}
