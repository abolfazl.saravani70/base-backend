/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.organization;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.organization.OrganizationRequirementType;
import com.rbp.sayban.model.dto.organization.OrganizationRequirementTypeDTO;
import com.rbp.sayban.service.organization.impl.OrganizationRequirementTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.ORGANIZATION_REQUIREMENT_TYPES)
@Api(description = "Operations pertaining to organization requirement type in Organization Requirement Type Management System")
public class OrganizationRequirementTypeController extends FrameworkAbstractController<OrganizationRequirementType, OrganizationRequirementTypeDTO, OrganizationRequirementTypeService> {
}
