/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.organization;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.sayban.model.domainmodel.organization.OrganizationChart;
import com.rbp.sayban.model.viewModel.organization.OrganizationChartViewModel;
import com.rbp.sayban.service.organization.IOrganizationChartService;
import com.rbp.sayban.service.organization.impl.OrganizationChartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.ORGANIZATION_CHARTS)
@Api(description = "Operations pertaining to organization chart in Organization chart Management System")
public class OrganizationChartController extends FrameworkAbstractController<OrganizationChart, OrganizationChartViewModel, OrganizationChartService> {
    @Autowired
    IOrganizationChartService iOrganizationChartService;

//    @PostMapping(restConstant.PARENT_ID + restConstant.CHILDREN)
//    @ApiOperation("get chldren who are not parents of an organization chart parent")
//    public ActionResult getOrgChartChildByOrgChartParentId(@RequestBody JsonInput<OrganizationChartViewModel> jsonInput) {
//        getApplicationLogger().printLog("info", OrganizationChartController.class, getModel().getSimpleName(), OrganizationChartViewModel.class.getSimpleName(), "getService().getOrgChartChildByOrgChartParentId()", null, null, "Method is starting....");
//        List<OrganizationChart> ff = iOrganizationChartService.findAllChildren(jsonInput.getPageNumber(), jsonInput.getPageSize(), jsonInput.getFieldId());
//        List<OrganizationChartViewModel> children = getMapper().getMapper(OrganizationChartViewModel.class, OrganizationChart.class).toView(ff);
//        getApplicationLogger().printLog("info", OrganizationChartController.class, getModel().getSimpleName(), OrganizationChartViewModel.class.getSimpleName(), "getService().getOrgChartChildByOrgChartParentId()", null, null, "Method finish successfull!!!!");
//        return new ActionResult((List<Object>) (List<?>) children, 0, 0, 0L, 0, "لیست داده ها یافت شد", "");
//    }

//    @PostMapping(restConstant.PARENT_ID + restConstant.PARENTS)
//    @ApiOperation("get sub parents of an organization chart parent")
//    public ActionResult getOrgChartSubParentByOrgChartParentId(@RequestBody JsonInput<OrganizationChartViewModel> jsonInput) {
//        getApplicationLogger().printLog("info", OrganizationChartController.class, getModel().getSimpleName(), OrganizationChartViewModel.class.getSimpleName(), "getService().getOrgChartSubParentByOrgChartParentId()", null, null, "Method is starting....");
//        List<OrganizationChart> ff=iOrganizationChartService.findAllParent(jsonInput.getPageNumber(), jsonInput.getPageSize(), jsonInput.getFieldId());
//        List<OrganizationChartViewModel> children = getMapper().getMapper(OrganizationChartViewModel.class, OrganizationChart.class).toView(ff);
//        getApplicationLogger().printLog("info", OrganizationChartController.class, getModel().getSimpleName(), OrganizationChartViewModel.class.getSimpleName(), "getService().getOrgChartSubParentByOrgChartParentId()", null, null, "Method finish successfull!!!!");
//        return new ActionResult((List<Object>) (List<?>) children, 0, 0, 0L, 0, "لیست داده ها یافت شد", "");
//    }

//    @Override
//    protected ActionResult delete(JsonInput<OrganizationChartViewModel> jsonInput) {
//        getApplicationLogger().printLog("info", OrganizationChartController.class, getModel().getSimpleName(), OrganizationChartViewModel.class.getSimpleName(), "getService().delete()", null, null, "Method is starting....");
//        iOrganizationChartService.deleteOrgChart(jsonInput.getFieldId(), jsonInput.getFilter());
//        getApplicationLogger().printLog("info", OrganizationChartController.class, getModel().getSimpleName(), OrganizationChartViewModel.class.getSimpleName(), "getService().delete()", null, null, "Method finish successfull!!!!");
//        return new ActionResult(null, 0, 0, 0L, 0, "حذف با موفقیت انجام شد.", "");
//    }

}
