/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.budgeting;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.budgeting.Expenditure;
import com.rbp.sayban.model.dto.budgeting.ExpenditureDTO;
import com.rbp.sayban.service.budgeting.impl.ExpenditureService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.EXPENDITURES)
@Api(description = "Operations pertaining to expenditure in Expenditure Management System")
public class ExpenditureController extends FrameworkAbstractController<Expenditure, ExpenditureDTO,ExpenditureService> {

}
