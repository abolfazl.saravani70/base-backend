package com.rbp.sayban.controller.budgeting;


import com.rbp.core.controller.base.FrameWorkAbstractTreeController;
import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.budgeting.BudgetTree;
import com.rbp.sayban.model.dto.budgeting.BudgetTreeDTO;
import com.rbp.sayban.service.budgeting.impl.BudgetTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.BUDGET_TREES)
@Api(description = "Budget Tree Controller")
public class BudgetTreeController extends FrameWorkAbstractTreeController<BudgetTree, BudgetTreeDTO, BudgetTreeService> {
}