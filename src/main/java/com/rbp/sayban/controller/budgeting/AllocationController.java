/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.budgeting;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.budgeting.Allocation;
import com.rbp.sayban.model.dto.budgeting.AllocationDTO;
import com.rbp.sayban.service.budgeting.impl.AllocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ALLOCATIONS)
@Api(description = "Operations pertaining to allocation in Allocation Management System")
public class AllocationController extends FrameworkAbstractController<Allocation, AllocationDTO,AllocationService> {


}
