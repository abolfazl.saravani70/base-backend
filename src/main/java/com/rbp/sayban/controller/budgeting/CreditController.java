/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.budgeting;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.budgeting.Credit;
import com.rbp.sayban.model.dto.budgeting.CreditDTO;
import com.rbp.sayban.service.budgeting.impl.CreditService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.CREDITS)
@Api(description = "Operations pertaining to credit in Credit Management System")
public class CreditController extends FrameworkAbstractController<Credit, CreditDTO, CreditService> {
}
