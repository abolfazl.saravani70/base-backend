/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.risk;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.risk.Alternative;
import com.rbp.sayban.model.dto.risk.AlternativeDTO;
import com.rbp.sayban.service.risk.impl.AlternativeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.ALTERNATIVES)
@Api(description = "Operations pertaining to alternative in Alternative Management System")
public class AlternativeController extends FrameworkAbstractController<Alternative, AlternativeDTO, AlternativeService> {


}
