/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.risk;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.risk.Risk;
import com.rbp.sayban.model.dto.risk.RiskDTO;
import com.rbp.sayban.service.risk.impl.RiskService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.RISKS)
@Api(description = "Operations pertaining to risk in Risk Management System")
public class RiskController extends FrameworkAbstractController<Risk, RiskDTO, RiskService> {

}
