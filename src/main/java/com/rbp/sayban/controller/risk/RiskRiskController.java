package com.rbp.sayban.controller.risk;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.risk.RiskTree;
import com.rbp.sayban.model.dto.risk.RiskTreeDTO;
import com.rbp.sayban.service.risk.impl.RiskTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.RISK_RISK)
@Api(description = "Risk Risk Controller")
public class RiskRiskController extends FrameworkAbstractController<RiskTree, RiskTreeDTO, RiskTreeService> {
}