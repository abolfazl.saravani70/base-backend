package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectOrganizationUnit;
import com.rbp.sayban.model.viewModel.project.ProjectOrganizationUnitViewModel;
import com.rbp.sayban.service.project.impl.ProjectOrganizationUnitService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_ORGANIZATION_UNITS )
@Api(description = "Operations pertaining to project_organization_unit in Project_Organization_unit Management System")
public class ProjectOrganizationUnitController extends FrameworkAbstractController<ProjectOrganizationUnit, ProjectOrganizationUnitViewModel, ProjectOrganizationUnitService> {
}
