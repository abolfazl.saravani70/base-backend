/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractRootController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectTree;
import com.rbp.sayban.model.dto.project.ProjectTreeDTO;
import com.rbp.sayban.service.project.impl.ProjectTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_TREES)
@Api(description = "Operations pertaining to project tree in Project Tree Management System")
public class ProjectTreeController extends FrameworkAbstractRootController<ProjectTree, ProjectTreeDTO, ProjectTreeService> {
}
