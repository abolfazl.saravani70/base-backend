package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectSubjective;
import com.rbp.sayban.model.viewModel.project.ProjectSubjectiveViewModel;
import com.rbp.sayban.service.project.impl.ProjectSubjectiveService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_SUBJECTIVES )
@Api(description = "Operations pertaining to project_subjective in Project_Subjective Management System")
public class ProjectSubjectiveController extends FrameworkAbstractController<ProjectSubjective, ProjectSubjectiveViewModel, ProjectSubjectiveService> {
}
