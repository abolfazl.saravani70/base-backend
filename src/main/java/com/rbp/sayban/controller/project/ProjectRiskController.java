package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectRisk;
import com.rbp.sayban.model.viewModel.project.ProjectRiskViewModel;
import com.rbp.sayban.service.project.impl.ProjectRiskService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_RISKS )
@Api(description = "Operations pertaining to project_risk in Project_Risk Management System")
public class ProjectRiskController extends FrameworkAbstractController<ProjectRisk, ProjectRiskViewModel, ProjectRiskService> {
}
