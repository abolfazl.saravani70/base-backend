/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ContractSection;
import com.rbp.sayban.model.dto.project.ContractSectionDTO;
import com.rbp.sayban.service.project.impl.ContractSectionService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.CONTRACT_SECTIONS )
@Api(description= "Operations pertaining to contract section in Contract Section Management System")
public class ContractSectionController extends FrameworkAbstractController<ContractSection, ContractSectionDTO, ContractSectionService> {


}
