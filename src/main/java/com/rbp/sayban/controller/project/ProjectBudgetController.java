package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectBudget;
import com.rbp.sayban.model.domainmodel.project.ProjectLocation;
import com.rbp.sayban.model.viewModel.project.ProjectBudgetViewModel;
import com.rbp.sayban.model.viewModel.project.ProjectLocationViewModel;
import com.rbp.sayban.service.project.impl.ProjectBudgetService;
import com.rbp.sayban.service.project.impl.ProjectLocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_BUDGETS )
@Api(description = "Operations pertaining to Project_budget in Project_budget Management System")
public class ProjectBudgetController extends FrameworkAbstractController<ProjectBudget, ProjectBudgetViewModel, ProjectBudgetService> {
}
