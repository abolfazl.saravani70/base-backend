package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectNormCost;
import com.rbp.sayban.model.viewModel.project.ProjectNormCostViewModel;
import com.rbp.sayban.service.project.impl.ProjectNormCostService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_NORM_COSTS )
@Api(description = "Operations pertaining to project_norm_cost in Project_Norm_Cost Management System")
public class ProjectNormCostController extends FrameworkAbstractController<ProjectNormCost, ProjectNormCostViewModel, ProjectNormCostService> {
}
