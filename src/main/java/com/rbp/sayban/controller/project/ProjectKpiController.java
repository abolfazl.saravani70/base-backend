package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectKpi;
import com.rbp.sayban.model.viewModel.project.ProjectKpiViewModel;
import com.rbp.sayban.service.project.impl.ProjectKpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_KPIES )
@Api(description = "Operations pertaining to Project_kpi in Project_Kpi Management System")
public class ProjectKpiController extends FrameworkAbstractController<ProjectKpi, ProjectKpiViewModel, ProjectKpiService> {
}
