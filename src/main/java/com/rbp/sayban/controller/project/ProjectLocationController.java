package com.rbp.sayban.controller.project;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.project.ProjectKpi;
import com.rbp.sayban.model.domainmodel.project.ProjectLocation;
import com.rbp.sayban.model.viewModel.project.ProjectKpiViewModel;
import com.rbp.sayban.model.viewModel.project.ProjectLocationViewModel;
import com.rbp.sayban.service.project.impl.ProjectKpiService;
import com.rbp.sayban.service.project.impl.ProjectLocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROJECT_LOCATIONS )
@Api(description = "Operations pertaining to Project_location in Project_location Management System")
public class ProjectLocationController extends FrameworkAbstractController<ProjectLocation, ProjectLocationViewModel, ProjectLocationService> {
}
