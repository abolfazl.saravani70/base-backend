/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban ReportType Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.report;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.report.ReportType;
import com.rbp.sayban.model.dto.report.ReportTypeDTO;
import com.rbp.sayban.service.report.impl.ReportTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.REPORT_TYPES )
@Api(description= "Operations pertaining toreport type in Report Type Management System")
public class ReportTypeController extends FrameworkAbstractController<ReportType, ReportTypeDTO, ReportTypeService> {

    
}
