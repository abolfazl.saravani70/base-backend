/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban SystemReportFilter Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.report;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.report.SystemReportFilter;
import com.rbp.sayban.model.dto.report.SystemReportFilterDTO;
import com.rbp.sayban.service.report.impl.SystemReportFilterService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.SYSTEM_REPORT_FILTERS)
@Api(description = "Operations pertaining to system report filter in System Report Filter Management System")
public class SystemReportFilterController extends FrameworkAbstractController<SystemReportFilter, SystemReportFilterDTO, SystemReportFilterService> {
}
