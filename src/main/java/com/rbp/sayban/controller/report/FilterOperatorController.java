/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban FilterOperator Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.report;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.report.FilterOperator;
import com.rbp.sayban.model.dto.report.FilterOperatorDTO;
import com.rbp.sayban.service.report.impl.FilterOperatorService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.FILTER_OPERATORS)
@Api(description= "Operations pertaining to filter operator in Filter Operator Management System")
public class FilterOperatorController extends FrameworkAbstractController<FilterOperator, FilterOperatorDTO, FilterOperatorService> {

    
}
