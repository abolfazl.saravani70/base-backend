/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban OperandTypes Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.report;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.report.OperandType;
import com.rbp.sayban.model.dto.report.OperandTypeDTO;
import com.rbp.sayban.service.report.impl.OperandTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.OPERAND_TYPES )
@Api(description = "Operations pertaining to operand type in Operand Type Management System")
public class OperandTypeController extends FrameworkAbstractController<OperandType, OperandTypeDTO, OperandTypeService> {

   
}
