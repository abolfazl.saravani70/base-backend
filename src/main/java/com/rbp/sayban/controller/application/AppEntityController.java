/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.application;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.application.AppEntity;
import com.rbp.sayban.model.dto.application.AppEntityDTO;
import com.rbp.sayban.service.application.impl.AppEntityService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.APP_ENTITIES)
@Api(description = "Operations pertaining to application entity in Application Entity Management System")
public class AppEntityController extends FrameworkAbstractController<AppEntity, AppEntityDTO, AppEntityService> {

}
