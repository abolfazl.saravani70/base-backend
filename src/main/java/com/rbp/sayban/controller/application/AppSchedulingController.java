/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Shobeir Shafiee , Sayban Project Developer , mobile: 09191168087 , Email: shobeir.shafiee@gmail.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.application;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.application.AppScheduling;
import com.rbp.sayban.model.dto.application.AppSchedulingDTO;
import com.rbp.sayban.service.application.impl.AppSchedulingService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.APP_SCHEDULINGS )
@Api(description = "Operations pertaining to application scheduling in Application scheduling Management System")
public class AppSchedulingController extends FrameworkAbstractController<AppScheduling, AppSchedulingDTO, AppSchedulingService> {
}
