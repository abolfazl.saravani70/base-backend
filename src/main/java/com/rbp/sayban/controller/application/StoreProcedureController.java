/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.application;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.application.StoreProcedure;
import com.rbp.sayban.model.dto.application.StoreProcedureDTO;
import com.rbp.sayban.service.application.impl.StoreProcedureService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.STORE_PROCEDURES)
@Api(description = "Operations pertaining to store procedure in Store Procedure Management System")
public class StoreProcedureController extends FrameworkAbstractController<StoreProcedure, StoreProcedureDTO,StoreProcedureService> {


}
