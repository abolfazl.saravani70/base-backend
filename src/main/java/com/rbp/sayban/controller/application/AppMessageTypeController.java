/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.application;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.application.AppMessageType;
import com.rbp.sayban.model.dto.application.AppMessageTypeDTO;
import com.rbp.sayban.service.application.impl.AppMessageTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.APP_MESSAGE_TYPES)
@Api(description = "Operations pertaining to application message type in Application Message Type Management System")
public class AppMessageTypeController extends FrameworkAbstractController<AppMessageType, AppMessageTypeDTO, AppMessageTypeService> {

}
