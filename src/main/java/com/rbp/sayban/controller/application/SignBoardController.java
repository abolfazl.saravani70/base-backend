/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.application;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.application.SignBoard;
import com.rbp.sayban.model.dto.application.SignBoardDTO;
import com.rbp.sayban.service.application.impl.SignBoardService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.SIGN_BOARDS)
@Api(description = "Operations pertaining to sign board in Sign Board Management System")
public class SignBoardController extends FrameworkAbstractController<SignBoard,SignBoardDTO,SignBoardService> {

}
