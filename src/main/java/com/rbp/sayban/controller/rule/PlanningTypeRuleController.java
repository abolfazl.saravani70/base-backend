/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.rule;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.rule.PlanningTypeRule;
import com.rbp.sayban.model.dto.rule.PlanningTypeRuleDTO;
import com.rbp.sayban.service.rule.impl.PlanningTypeRuleService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PLANNING_TYPE_RULE )
@Api(description = "Operations pertaining to plan type rule in PlanningTypeRule Management System")
public class PlanningTypeRuleController extends FrameworkAbstractController<PlanningTypeRule, PlanningTypeRuleDTO, PlanningTypeRuleService> {
}
