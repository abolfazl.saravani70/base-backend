/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.system;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.system.ProcessTemplate;
import com.rbp.sayban.model.dto.system.ProcessTemplateDTO;
import com.rbp.sayban.service.system.impl.ProcessTemplateService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROCESS_TEMPLATES )
@Api(description = "Operations pertaining to process template in Process Template Management System")
public class ProcessTemplateController extends FrameworkAbstractController<ProcessTemplate, ProcessTemplateDTO, ProcessTemplateService> {
}
