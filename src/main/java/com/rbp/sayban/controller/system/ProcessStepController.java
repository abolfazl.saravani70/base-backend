/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.system;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.system.ProcessStep;
import com.rbp.sayban.model.dto.system.ProcessStepDTO;
import com.rbp.sayban.service.system.impl.ProcessStepService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PROCESS_STEPS )
@Api(description = "Operations pertaining to process step in Process Step Management System")
public class ProcessStepController extends FrameworkAbstractController<ProcessStep, ProcessStepDTO, ProcessStepService> {
    
}
