/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.system;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.system.Document;
import com.rbp.sayban.model.dto.system.DocumentDTO;
import com.rbp.sayban.service.system.impl.DocumentService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.DOCUMENTS)
@Api(description = "Operations pertaining to document in Document Management System")
public class DocumentController extends FrameworkAbstractController<Document, DocumentDTO, DocumentService> {
}
