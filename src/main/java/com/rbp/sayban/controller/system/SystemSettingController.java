/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.system;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.system.SystemSetting;
import com.rbp.sayban.model.dto.system.SystemSettingDTO;
import com.rbp.sayban.service.system.impl.SystemSettingService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.SYSTEM_SETTINGS)
@Api(description = "Operations pertaining to system setting in System Setting Management System")
public class SystemSettingController extends FrameworkAbstractController<SystemSetting, SystemSettingDTO, SystemSettingService> {
}
