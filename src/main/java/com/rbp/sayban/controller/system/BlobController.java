/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.system;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.core.model.dto.base.ActionResult;
import com.rbp.core.model.dto.base.JsonInput;
import com.rbp.core.utility.ApplicationException;
import com.rbp.sayban.model.domainmodel.system.Blob;
import com.rbp.sayban.model.dto.system.BlobDTO;
import com.rbp.sayban.service.system.impl.BlobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.engine.jdbc.BlobProxy;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;

/**
 * Do not Use Crud Operations for Blob files!
 * READ THIS:
 * you can send byte[] back and forth to db but you when you require to load this data, it will take a lot of memory.
 * it's clear to see since you will need to load all the bytes inside the memory then send it. like any other entity.
 * if you instead use a generated proxy of SQL.Blob file, the jdbc driver can optimize it.
 */
@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.BLOBS )
@Api(description = "Operations pertaining to blob in Blob Management System")
public class BlobController extends FrameworkAbstractController<Blob, BlobDTO, BlobService> {

    /**
     * The simple way of sending files back and forth is using byte array or string with @Lob annotation of Hibernate.
     * But this method takes a multi part and converts that to a Blob file type inside Database.
     * @param multipartFile
     * @return
     */
    @PostMapping("/upload") //this is different from saving a blob, this post gets a multipart FILE not a Blob
    @ApiOperation("upload a file it returns the file without data bytes, generally you just need the id.")
    public ActionResult upload(@RequestParam MultipartFile multipartFile){
        byte[] file;
        try {
            file=multipartFile.getBytes();
        } catch (IOException e) {
            throw new ApplicationException(1,"گرفتن بایت فایل با مشکل مواجه شد.");
        }
        Blob b=new Blob();
        String name=multipartFile.getName();
        String[] splitted=name.split(".");
        String fileType=splitted[splitted.length-1];
        b.setFileType(fileType);
        b.setFile(BlobProxy.generateProxy(file));
        b.setFileName(multipartFile.getName());
        b=getService().save(b);
        b.setFile(null);//set bytes to null before sending it back. Just Send back the id
        return  new ActionResult(Collections.singletonList(b),0,0,0L,0,"","");
    }

    @PostMapping("/downloadByStream")
    @ApiOperation("Download a BlobFile, set the Id of the blob inside Filed ID of the JsonInput")
    public StreamingResponseBody dowloadByStream(@RequestBody JsonInput<BlobDTO> input){
        Blob blob=getService().loadById(input.getFieldId());
        if(blob==null) return null;
        StreamingResponseBody responseBody=outputStream->{
            java.sql.Blob b=blob.getFile();
            byte[] files=new byte[1000];
            while(true){
                try {
                    if (!((b.getBinaryStream().read(files))>0)) break;
                    outputStream.write(files);
                    outputStream.flush();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        };
        return responseBody;
    }

    @PostMapping("/downloadByByteArray")
    @ApiOperation("downland a file and get a byte[] representing the file")
    public ActionResult<byte[]> downloadByByteArray(@RequestBody JsonInput<BlobDTO> input){
        Blob blob=getService().loadById(input.getFieldId());
        if(blob==null) return new ActionResult(null,0,0,0L,0,"داده ای یافت نشد.","");
        else {
            try {
                return new ActionResult(Collections.singletonList(blob.getFile().getBytes(0,(int)blob.getFile().length())),0,0,0L,0,"","");
            } catch (SQLException e) {
                return new ActionResult(null,0,0,0L,1,"خطا در گرفتن فایل.","");
            }
        }
    }

}
