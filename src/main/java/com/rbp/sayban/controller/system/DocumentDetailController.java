/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.system;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.system.DocumentDetail;
import com.rbp.sayban.model.dto.system.DocumentDetailDTO;
import com.rbp.sayban.service.system.impl.DocumentDetailService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.DOCUMENT_DETAILS)
@Api(description = "Operations pertaining to document detail in Document Detail Management System")
public class DocumentDetailController extends FrameworkAbstractController<DocumentDetail, DocumentDetailDTO, DocumentDetailService> {
}
