/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.goal;

import com.rbp.core.controller.base.FrameworkAbstractRootController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.goal.GoalTree;
import com.rbp.sayban.model.dto.goal.GoalTreeDTO;
import com.rbp.sayban.service.goal.impl.GoalTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.GOAL_TREES)
@Api(description = "Operations pertaining to goal tree in Goal Tree Management System")
public class GoalTreeController extends FrameworkAbstractRootController<GoalTree, GoalTreeDTO, GoalTreeService> {
}
