/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.goal;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.goal.GoalKpi;
import com.rbp.sayban.model.viewModel.goal.GoalKpiViewModel;
import com.rbp.sayban.service.goal.impl.GoalKpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.GOAL_KPIES)
@Api(description = "Operations pertaining to goal_kpi in Goal_Kpi Management System")
public class GoalKpiController extends FrameworkAbstractController<GoalKpi, GoalKpiViewModel, GoalKpiService> {
}
