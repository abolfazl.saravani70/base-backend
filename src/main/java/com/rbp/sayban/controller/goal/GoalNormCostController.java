/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.goal;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.goal.GoalNormCost;
import com.rbp.sayban.model.viewModel.goal.GoalNormCostViewModel;
import com.rbp.sayban.service.goal.impl.GoalNormCostService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.GOAL_NORM_COSTS)
@Api(description = "Operations pertaining to goal_norm_cost in Goal_Norm_Cost Management System")
public class GoalNormCostController extends FrameworkAbstractController<GoalNormCost, GoalNormCostViewModel, GoalNormCostService> {
}
