/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.todo;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.todo.TodoHR;
import com.rbp.sayban.model.domainmodel.todo.TodoKpi;
import com.rbp.sayban.model.viewModel.todo.TodoHRViewModel;
import com.rbp.sayban.model.viewModel.todo.TodoKpiViewModel;
import com.rbp.sayban.service.todo.impl.TodoHRService;
import com.rbp.sayban.service.todo.impl.TodoKpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TODO_HRS)
@Api(description = "Operations pertaining to todo_hr in Todo_hr Management System")
public class TodoHRController extends FrameworkAbstractController<TodoHR, TodoHRViewModel, TodoHRService> {
}