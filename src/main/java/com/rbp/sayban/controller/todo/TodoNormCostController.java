/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.todo;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.todo.TodoNormCost;
import com.rbp.sayban.model.viewModel.todo.TodoNormCostViewModel;
import com.rbp.sayban.service.todo.impl.TodoNormCostService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TODO_NORM_COSTS)
@Api(description = "Operations pertaining to todo_norm_cost in Todo_Norm_Cost Management System")
public class TodoNormCostController extends FrameworkAbstractController<TodoNormCost, TodoNormCostViewModel, TodoNormCostService> {
}