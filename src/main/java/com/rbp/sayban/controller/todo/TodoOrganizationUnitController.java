/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.todo;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.todo.TodoOrganizationUnit;
import com.rbp.sayban.model.viewModel.todo.TodoOrganizationUnitViewModel;
import com.rbp.sayban.service.todo.impl.TodoOrganizationUnitService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TODO_ORGANIZATION_UNITS)
@Api(description = "Operations pertaining to to-do in Todo Management System")
public class TodoOrganizationUnitController extends FrameworkAbstractController<TodoOrganizationUnit, TodoOrganizationUnitViewModel, TodoOrganizationUnitService> {
}