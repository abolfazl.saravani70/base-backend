/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.todo;

import com.rbp.core.controller.base.FrameworkAbstractRootController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.todo.TodoTree;
import com.rbp.sayban.model.dto.todo.TodoTreeDTO;
import com.rbp.sayban.service.todo.impl.TodoTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.TODO_TREES)
@Api(description = "Operations pertaining to todo tree in Todo Tree Management System")
public class TodoTreeController extends FrameworkAbstractRootController<TodoTree, TodoTreeDTO, TodoTreeService> {
}
