/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.todo;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.todo.TodoBudget;
import com.rbp.sayban.model.domainmodel.todo.TodoHR;
import com.rbp.sayban.model.viewModel.todo.TodoBudgetViewModel;
import com.rbp.sayban.model.viewModel.todo.TodoHRViewModel;
import com.rbp.sayban.service.todo.impl.TodoBudgetService;
import com.rbp.sayban.service.todo.impl.TodoHRService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TODO_BUDGETS)
@Api(description = "Operations pertaining to todo_budget in Todo_budget Management System")
public class TodoBudgetController extends FrameworkAbstractController<TodoBudget, TodoBudgetViewModel, TodoBudgetService> {
}