/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.todo;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.todo.TodoBudget;
import com.rbp.sayban.model.domainmodel.todo.TodoLocation;
import com.rbp.sayban.model.viewModel.todo.TodoBudgetViewModel;
import com.rbp.sayban.model.viewModel.todo.TodoLocationViewModel;
import com.rbp.sayban.service.todo.impl.TodoBudgetService;
import com.rbp.sayban.service.todo.impl.TodoLocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TODO_LOCATIONS)
@Api(description = "Operations pertaining to todo_Location in Todo_Location Management System")
public class TodoLocationController extends FrameworkAbstractController<TodoLocation, TodoLocationViewModel, TodoLocationService> {
}