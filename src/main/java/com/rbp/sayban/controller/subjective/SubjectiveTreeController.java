package com.rbp.sayban.controller.subjective;


import com.rbp.core.controller.base.FrameWorkAbstractTreeController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.kpi.KpiTree;
import com.rbp.sayban.model.domainmodel.subjective.SubjectiveTree;
import com.rbp.sayban.model.dto.kpi.KpiTreeDTO;
import com.rbp.sayban.model.dto.subjective.SubjectiveTreeDTO;
import com.rbp.sayban.service.kpi.impl.KpiTreeService;
import com.rbp.sayban.service.subjective.Impl.SubjectiveTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.Subjective_Trees)
@Api(description = "subjective Tree Controller")
public class SubjectiveTreeController extends FrameWorkAbstractTreeController<SubjectiveTree, SubjectiveTreeDTO, SubjectiveTreeService> {
}