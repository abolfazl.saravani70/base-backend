/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.objectives;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveRisk;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveRiskViewModel;
import com.rbp.sayban.service.objectives.impl.ObjectiveRiskService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.OBJECTIVE_RISKS)
@Api(description = "Operations pertaining to objective_risk in Objective_Organization_Risk Management System")
public class ObjectiveRiskController extends FrameworkAbstractController<ObjectiveRisk, ObjectiveRiskViewModel, ObjectiveRiskService> {
}
