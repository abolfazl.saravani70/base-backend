/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.objectives;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveSubjective;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveSubjectiveViewModel;
import com.rbp.sayban.service.objectives.impl.ObjectiveSubjectiveService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.OBJECTIVE_SUBJECTIVES)
@Api(description = "Operations pertaining to objective_subjective in Objective_Subjective Management System")
public class ObjectiveSubjectiveController extends FrameworkAbstractController<ObjectiveSubjective, ObjectiveSubjectiveViewModel, ObjectiveSubjectiveService> {
}
