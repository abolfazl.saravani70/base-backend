/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.objectives;

import com.rbp.core.controller.base.FrameworkAbstractRootController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveTree;
import com.rbp.sayban.model.dto.objectives.ObjectiveTreeDTO;
import com.rbp.sayban.service.objectives.impl.ObjectiveTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.OBJECTIVE_TREES)
@Api(description = "Operations pertaining to objective tree in Objective Tree Management System")
public class ObjectiveTreeController extends FrameworkAbstractRootController<ObjectiveTree, ObjectiveTreeDTO, ObjectiveTreeService> {
}
