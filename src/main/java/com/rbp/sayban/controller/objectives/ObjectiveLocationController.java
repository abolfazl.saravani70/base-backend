/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.objectives;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveBudget;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveLocation;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveBudgetViewModel;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveLocationViewModel;
import com.rbp.sayban.service.objectives.impl.ObjectiveBudgetService;
import com.rbp.sayban.service.objectives.impl.ObjectiveLocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.OBJECTIVE_LOCATIONS)
@Api(description = "Operations pertaining to objective_location in Objective_location Management System")
public class ObjectiveLocationController extends FrameworkAbstractController<ObjectiveLocation, ObjectiveLocationViewModel, ObjectiveLocationService> {
}
