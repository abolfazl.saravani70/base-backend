/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.objectives;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.objectives.Objective;
import com.rbp.sayban.model.dto.objectives.ObjectiveDTO;
import com.rbp.sayban.service.objectives.impl.ObjectiveService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.OBJECTIVES)
@Api(description = "Operations pertaining to objective in Objective Management System")
public class ObjectiveController extends FrameworkAbstractController<Objective, ObjectiveDTO, ObjectiveService> {



}
