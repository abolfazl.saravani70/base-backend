/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.objectives;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.objectives.ObjectiveOrganizationUnit;
import com.rbp.sayban.model.viewModel.objectives.ObjectiveOrganizationUnitViewModel;
import com.rbp.sayban.service.objectives.impl.ObjectiveOrganizationUnitService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.OBJECTIVE_ORGANIZATION_UNITS)
@Api(description = "Operations pertaining to objective_organization-unit in Objective_Organization_Unit Management System")
public class ObjectiveOrganizationUnitController extends FrameworkAbstractController<ObjectiveOrganizationUnit, ObjectiveOrganizationUnitViewModel, ObjectiveOrganizationUnitService> {
}
