package com.rbp.sayban.controller.task;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.task.TaskKpi;
import com.rbp.sayban.model.viewModel.task.TaskKpiViewModel;
import com.rbp.sayban.service.task.impl.TaskKpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TASK_KPIES)
@Api(description = "Operations pertaining to task_kpi in Task_Kpi Management System")
public class TaskKpiController extends FrameworkAbstractController<TaskKpi, TaskKpiViewModel, TaskKpiService> {
}