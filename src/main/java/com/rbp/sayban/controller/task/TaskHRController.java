package com.rbp.sayban.controller.task;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.task.TaskHR;
import com.rbp.sayban.model.domainmodel.task.TaskKpi;
import com.rbp.sayban.model.viewModel.task.TaskHRViewModel;
import com.rbp.sayban.model.viewModel.task.TaskKpiViewModel;
import com.rbp.sayban.service.task.impl.TaskHRService;
import com.rbp.sayban.service.task.impl.TaskKpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TASK_HRS)
@Api(description = "Operations pertaining to task_hr in task_hr Management System")
public class TaskHRController extends FrameworkAbstractController<TaskHR, TaskHRViewModel, TaskHRService> {
}