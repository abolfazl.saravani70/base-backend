package com.rbp.sayban.controller.task;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.task.TaskKpi;
import com.rbp.sayban.model.domainmodel.task.TaskLocation;
import com.rbp.sayban.model.viewModel.task.TaskKpiViewModel;
import com.rbp.sayban.model.viewModel.task.TaskLocationViewModel;
import com.rbp.sayban.service.task.impl.TaskKpiService;
import com.rbp.sayban.service.task.impl.TaskLocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TASK_LOCATIONS)
@Api(description = "Operations pertaining to Task_location in Task_location Management System")
public class TaskLocationController extends FrameworkAbstractController<TaskLocation, TaskLocationViewModel, TaskLocationService> {
}