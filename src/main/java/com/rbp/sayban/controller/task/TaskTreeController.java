/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.task;

import com.rbp.core.controller.base.FrameworkAbstractRootController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.task.TaskTree;
import com.rbp.sayban.model.dto.task.TaskTreeDTO;
import com.rbp.sayban.service.task.impl.TaskTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.TASK_TREES)
@Api(description = "Operations pertaining to task tree in Task Tree Management System")
public class TaskTreeController extends FrameworkAbstractRootController<TaskTree, TaskTreeDTO, TaskTreeService> {
}
