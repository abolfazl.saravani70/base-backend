/*
 *
 *  * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 *  * Author Shobeir shafiee , Sayban Developer , mobile: 09191168087 , Email: Shobeir.shafiee@gmail.com
 *  * Production Year 1398
 *
 *
 *
 */

package com.rbp.sayban.controller.task;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.task.TaskRisk;
import com.rbp.sayban.model.viewModel.task.TaskRiskViewModel;
import com.rbp.sayban.service.task.impl.TaskRiskService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.TASK_RISKS)
@Api(description = "Operations pertaining to task_risk in Task_Risk Management System")
public class TaskRiskController extends FrameworkAbstractController<TaskRisk, TaskRiskViewModel, TaskRiskService> {
}