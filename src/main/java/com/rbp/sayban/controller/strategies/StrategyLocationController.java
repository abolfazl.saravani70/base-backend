/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.strategies;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.strategies.StrategyBudget;
import com.rbp.sayban.model.domainmodel.strategies.StrategyLocation;
import com.rbp.sayban.model.viewModel.strategies.StrategyBudgetViewModel;
import com.rbp.sayban.model.viewModel.strategies.StrategyLocationViewModel;
import com.rbp.sayban.service.strategies.impl.StrategyBudgetService;
import com.rbp.sayban.service.strategies.impl.StrategyLocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.STRATEGY_LOCATIONS)
@Api(description = "Operations pertaining to strategy_location in Strategy_location Management System")
public class StrategyLocationController extends FrameworkAbstractController<StrategyLocation, StrategyLocationViewModel, StrategyLocationService> {
}
