/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.strategies;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.strategies.StrategyType;
import com.rbp.sayban.model.dto.strategies.StrategyTypeDTO;
import com.rbp.sayban.service.strategies.impl.StrategyTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.STRATEGY_TYPES)
@Api(description = "Operations pertaining to strategy type in Strategy Type Management System")
public class StrategyTypeController extends FrameworkAbstractController<StrategyType, StrategyTypeDTO, StrategyTypeService> {
}
