/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.kpi;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.kpi.Kpi;
import com.rbp.sayban.model.dto.kpi.KpiDTO;
import com.rbp.sayban.service.kpi.impl.KpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST + restConstant.KPIES)
@Api(description = "Operations pertaining to kpi in Kpi Management System")
public class KpiController extends FrameworkAbstractController<Kpi, KpiDTO, KpiService> {


}
