package com.rbp.sayban.controller.kpi;


import com.rbp.core.controller.base.FrameWorkAbstractTreeController;
import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.kpi.KpiTree;
import com.rbp.sayban.model.dto.kpi.KpiTreeDTO;
import com.rbp.sayban.service.kpi.impl.KpiTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST + restConstant.KPI_Trees)
@Api(description = "Kpi Tree Controller")
public class KpiTreeController extends FrameWorkAbstractTreeController<KpiTree, KpiTreeDTO, KpiTreeService> {
}