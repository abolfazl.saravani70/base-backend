/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.kpi;


import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.kpi.KpiType;
import com.rbp.sayban.model.dto.kpi.KpiTypeDTO;
import com.rbp.sayban.service.kpi.impl.KpiTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.KPI_TYPES)
@Api(description = "Operations pertaining to kpi type in Kpi Type Management System")
public class KpiTypeController extends FrameworkAbstractController<KpiType, KpiTypeDTO, KpiTypeService> {

}
