/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.planning;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.planning.PlanSubjective;
import com.rbp.sayban.model.viewModel.planning.PlanSubjectiveViewModel;
import com.rbp.sayban.service.planning.impl.PlanSubjectiveService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PLAN_SUBJECTIVES )
@Api(description = "Operations pertaining to Plan_subjective in Plan_Subjective Management System")
public class PlanSubjectiveController extends FrameworkAbstractController<PlanSubjective, PlanSubjectiveViewModel, PlanSubjectiveService> {
}
