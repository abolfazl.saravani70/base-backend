/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.planning;

import com.rbp.core.controller.base.FrameworkAbstractRootController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.planning.PlanTree;
import com.rbp.sayban.model.dto.planning.PlanTreeDTO;
import com.rbp.sayban.service.planning.impl.PlanTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST+restConstant.PLAN_TREES)
@Api(description = "Operations pertaining to plan tree in Plan tree Management System")
public class PlanTreeController extends FrameworkAbstractRootController<PlanTree, PlanTreeDTO, PlanTreeService> {
}
