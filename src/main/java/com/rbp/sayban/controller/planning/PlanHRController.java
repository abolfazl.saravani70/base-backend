/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.planning;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.planning.PlanHR;
import com.rbp.sayban.model.domainmodel.planning.PlanKpi;
import com.rbp.sayban.model.viewModel.planning.PlanHRViewModel;
import com.rbp.sayban.model.viewModel.planning.PlanKpiViewModel;
import com.rbp.sayban.service.planning.impl.PlanHRService;
import com.rbp.sayban.service.planning.impl.PlanKpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.PLAN_HRS )
@Api(description = "Operations pertaining to Plan_hr in Plan_hr Management System")
public class PlanHRController extends FrameworkAbstractController<PlanHR, PlanHRViewModel, PlanHRService> {
}
