package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityKpi;
import com.rbp.sayban.model.viewModel.activity.ActivityKpiViewModel;
import com.rbp.sayban.service.activity.impl.ActivityKpiService;
import com.rbp.sayban.service.activity.impl.ActivityService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_KPIES)
@Api(description = "Operations pertaining to activity_kpi in Activity_Kpi Management System")
public class ActivityKpiController extends FrameworkAbstractController<ActivityKpi, ActivityKpiViewModel, ActivityKpiService> {
}
