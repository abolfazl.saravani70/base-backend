package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActivityQuality;
import com.rbp.sayban.model.dto.activity.ActivityQualityDTO;
import com.rbp.sayban.service.activity.impl.ActivityQualityService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_QUALITIES)
@Api(description = "Operations pertaining to activity quality in Activity  Quality Management System")
public class ActivityQualityController extends FrameworkAbstractController<ActivityQuality, ActivityQualityDTO, ActivityQualityService> {
}
