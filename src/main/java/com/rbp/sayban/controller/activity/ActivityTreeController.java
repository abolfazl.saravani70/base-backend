/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz.
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com
 * Production Year 1398
 */

package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractRootController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityTree;
import com.rbp.sayban.model.dto.activity.ActivityTreeDTO;
import com.rbp.sayban.service.activity.impl.ActivityTreeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = restConstant.BASE_REST +restConstant.ACTIVITY_TREES)
@Api(description = "Operations pertaining to activity tree in Activity Tree Management System")
public class ActivityTreeController extends FrameworkAbstractRootController<ActivityTree, ActivityTreeDTO, ActivityTreeService> {

}
