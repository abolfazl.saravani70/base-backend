package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplateType;
import com.rbp.sayban.model.dto.activity.ActivityTemplateTypeDTO;
import com.rbp.sayban.service.activity.impl.ActivityTemplateTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_TEMPLATE_TYPES)
@Api(description = "Operations pertaining to activity template type in Activity Template Type Management System")
public class ActivityTemplateTypeController extends FrameworkAbstractController<ActivityTemplateType, ActivityTemplateTypeDTO, ActivityTemplateTypeService> {
}
