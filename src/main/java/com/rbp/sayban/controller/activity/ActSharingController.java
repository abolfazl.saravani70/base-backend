package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActSharing;
import com.rbp.sayban.model.dto.activity.ActSharingDTO;
import com.rbp.sayban.service.activity.impl.ActSharingService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACT_SHARINGS)
@Api(description = "Operations pertaining to activity sharing in Activity Sharing Management System")
public class ActSharingController extends FrameworkAbstractController<ActSharing, ActSharingDTO, ActSharingService> {
}
