package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.PerformanceIndicator;
import com.rbp.sayban.model.dto.activity.PerformanceIndicatorDTO;
import com.rbp.sayban.service.activity.impl.PerformanceIndicatorService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.PERFORMANCE_INDICATORS)
@Api(description = "Operations pertaining to performance indicator in Performance Indicator Management System")
public class PerformanceIndicatorController extends FrameworkAbstractController<PerformanceIndicator, PerformanceIndicatorDTO, PerformanceIndicatorService> {
}
