package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityRisk;
import com.rbp.sayban.model.viewModel.activity.ActivityRiskViewModel;
import com.rbp.sayban.service.activity.impl.ActivityRiskService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_RISKS)
@Api(description = "Operations pertaining to activity_risk in Activity_Risk Management System")
public class ActivityRiskController extends FrameworkAbstractController<ActivityRisk, ActivityRiskViewModel, ActivityRiskService> {
}
