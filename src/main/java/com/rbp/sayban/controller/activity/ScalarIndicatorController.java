package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ScalarIndicator;
import com.rbp.sayban.model.dto.activity.ScalarIndicatorDTO;
import com.rbp.sayban.service.activity.impl.ScalarIndicatorService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.SCALAR_INDICATORS)
@Api(description = "Operations pertaining to scalar indicator in Scalar Indicator Management System")
public class ScalarIndicatorController extends FrameworkAbstractController<ScalarIndicator, ScalarIndicatorDTO, ScalarIndicatorService> {
}
