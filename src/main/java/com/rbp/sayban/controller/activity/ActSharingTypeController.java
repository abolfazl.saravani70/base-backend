package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActSharingType;
import com.rbp.sayban.model.dto.activity.ActSharingTypeDTO;
import com.rbp.sayban.service.activity.impl.ActSharingTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACT_SHARING_TYPES)
@Api(description = "Operations pertaining to activity sharing type in Activity Sharing Type Management System")
public class ActSharingTypeController extends FrameworkAbstractController<ActSharingType, ActSharingTypeDTO, ActSharingTypeService> {
}
