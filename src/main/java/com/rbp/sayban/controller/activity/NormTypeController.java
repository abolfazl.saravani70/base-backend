package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.norm.NormType;
import com.rbp.sayban.model.dto.activity.NormTypeDTO;
import com.rbp.sayban.service.activity.impl.NormTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.NORM_TYPES)
@Api(description = "Operations pertaining to norm type in Norm Type Management System")
public class NormTypeController extends FrameworkAbstractController<NormType, NormTypeDTO, NormTypeService> {
}
