package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityBudget;
import com.rbp.sayban.model.domainmodel.activity.ActivityKpi;
import com.rbp.sayban.model.viewModel.activity.ActivityBudgetViewModel;
import com.rbp.sayban.model.viewModel.activity.ActivityKpiViewModel;
import com.rbp.sayban.service.activity.impl.ActivityBudgetService;
import com.rbp.sayban.service.activity.impl.ActivityKpiService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_BUDGETS)
@Api(description = "Operations pertaining to Activity_Budget in Activity_Budget Management System")
public class ActivityBudgetController extends FrameworkAbstractController<ActivityBudget, ActivityBudgetViewModel, ActivityBudgetService> {
}
