package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivitySubjective;
import com.rbp.sayban.model.viewModel.activity.ActivitySubjectiveViewModel;
import com.rbp.sayban.service.activity.impl.ActivitySubjectiveService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_SUBJECTIVES)
@Api(description = "Operations pertaining to activity_subjective in Activity_Subjective Management System")
public class ActivitySubjectiveController extends FrameworkAbstractController<ActivitySubjective, ActivitySubjectiveViewModel, ActivitySubjectiveService> {
}
