package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActivityCost;
import com.rbp.sayban.model.dto.activity.ActivityCostDTO;
import com.rbp.sayban.service.activity.impl.ActivityCostService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_COSTS)
@Api(description = "Operations pertaining to activity cost in Activity Cost Management System")
public class ActivityCostController extends FrameworkAbstractController<ActivityCost, ActivityCostDTO, ActivityCostService> {

}