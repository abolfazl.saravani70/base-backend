package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActivityTemplate;
import com.rbp.sayban.model.dto.activity.ActivityTemplateDTO;
import com.rbp.sayban.service.activity.impl.ActivityTemplateService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_TEMPLATES)
@Api(description = "Operations pertaining to activity  template in Activity Template Management System")
public class ActivityTemplateController extends FrameworkAbstractController<ActivityTemplate, ActivityTemplateDTO, ActivityTemplateService> {
}
