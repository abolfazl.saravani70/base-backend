package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.PaidCredit;
import com.rbp.sayban.model.dto.activity.PaidCreditDTO;
import com.rbp.sayban.service.activity.impl.PaidCreditService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.PAID_CREDITS)
@Api(description = "Operations pertaining to paid credit in Paid Credit Management System")
public class PaidCreditController extends FrameworkAbstractController<PaidCredit, PaidCreditDTO, PaidCreditService> {
}
