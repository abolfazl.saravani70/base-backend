package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityNormCost;
import com.rbp.sayban.model.viewModel.activity.ActivityNormCostViewModel;
import com.rbp.sayban.service.activity.impl.ActivityNormCostService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_NORM_COSTS)
@Api(description = "Operations pertaining to activity norm cost in Activity Norm Cost Management System")
public class ActivityNormCostCotroller extends FrameworkAbstractController<ActivityNormCost, ActivityNormCostViewModel, ActivityNormCostService> {
}
