package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.norm.NormCost;
import com.rbp.sayban.model.dto.activity.NormCostDTO;
import com.rbp.sayban.service.activity.impl.NormCostService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.NORM_COSTS)
@Api(description = "Operations pertaining to norm cost in Norm Cost Management System")
public class NormCostController extends FrameworkAbstractController<NormCost, NormCostDTO, NormCostService> {
}
