package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityOrganizationUnit;
import com.rbp.sayban.model.viewModel.activity.ActivityOrganizationUnitViewModel;
import com.rbp.sayban.service.activity.impl.ActivityOrganizationUnitService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_ORGANIZATION_UNITS)
@Api(description = "Operations pertaining to activity_organization_unit in Activity_Organization_Unit Management System")
public class ActivityOrganizationUnitController extends FrameworkAbstractController<ActivityOrganizationUnit, ActivityOrganizationUnitViewModel, ActivityOrganizationUnitService> {
}
