package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.norm.NormParameter;
import com.rbp.sayban.model.dto.activity.NormParameterDTO;
import com.rbp.sayban.service.activity.impl.NormParameterService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.NORM_PARAMETERS)
@Api(description = "Operations pertaining to norm parameter in Norm Parameter Management System")
public class NormParameterController extends FrameworkAbstractController<NormParameter, NormParameterDTO, NormParameterService> {
}
