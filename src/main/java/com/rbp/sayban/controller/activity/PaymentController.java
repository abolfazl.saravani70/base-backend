package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.Payment;
import com.rbp.sayban.model.dto.activity.PaymentDTO;
import com.rbp.sayban.service.activity.impl.PaymentService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.PAYMENTS)
@Api(description = "Operations pertaining to payment in Payment Management System")
public class PaymentController extends FrameworkAbstractController<Payment, PaymentDTO, PaymentService> {
}
