package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.Performance;
import com.rbp.sayban.model.dto.activity.PerformanceDTO;
import com.rbp.sayban.service.activity.impl.PerformanceService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.PERFORMANCES)
@Api(description = "Operations pertaining to performance in Performance Management System")
public class PerformanceController extends FrameworkAbstractController<Performance, PerformanceDTO, PerformanceService> {
}
