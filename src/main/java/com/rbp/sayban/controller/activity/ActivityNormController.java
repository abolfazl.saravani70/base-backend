package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActivityNorm;
import com.rbp.sayban.model.dto.activity.ActivityNormDTO;
import com.rbp.sayban.service.activity.impl.ActivityNormService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_NORMS)
@Api(description = "Operations pertaining to activity norm in Activity  Norm Management System")
public class ActivityNormController extends FrameworkAbstractController<ActivityNorm, ActivityNormDTO, ActivityNormService> {
}
