package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.QualitativeIndicator;
import com.rbp.sayban.model.dto.activity.QualitativeIndicatorDTO;
import com.rbp.sayban.service.activity.impl.QualitativeIndicatorService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.QUALITATIVE_INDICATORS)
@Api(description = "Operations pertaining to qualitative indicator in Qualitative Indicator Management System")
public class QualitativeIndicatorController extends FrameworkAbstractController<QualitativeIndicator, QualitativeIndicatorDTO, QualitativeIndicatorService> {
}
