/*
 * Copyright (c) 2019. All Right Reserved to Rayan Bahamn Pardaz. 
 * Author Seyed Abolghasem Kalantari , Sayban Project Manager , mobile: 09124866018 , Email: sina3368@yahoo.com  
 * Production Year 1398
 */

package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.dep.ActSharingDetail;
import com.rbp.sayban.model.dto.activity.ActSharingDetailDTO;
import com.rbp.sayban.service.activity.impl.ActSharingDetailService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACT_SHARING_DETAILS)
@Api(description = "Operations pertaining to activity sharing detail in Activity Sharing Detail Management System")
public class ActSharingDetailConteroller extends FrameworkAbstractController<ActSharingDetail, ActSharingDetailDTO, ActSharingDetailService> {
}
