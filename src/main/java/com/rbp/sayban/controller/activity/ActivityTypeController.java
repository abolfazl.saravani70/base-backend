package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityType;
import com.rbp.sayban.model.dto.activity.ActivityTypeDTO;
import com.rbp.sayban.service.activity.impl.ActivityTypeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_TYPE)
@Api(description = "Operations pertaining to activity type in Activity Type Management System")
public class ActivityTypeController extends FrameworkAbstractController<ActivityType, ActivityTypeDTO, ActivityTypeService> {
}
