package com.rbp.sayban.controller.activity;

import com.rbp.core.controller.base.FrameworkAbstractController;
import com.rbp.core.controller.base.restConstant;
import com.rbp.sayban.model.domainmodel.activity.ActivityHR;
import com.rbp.sayban.model.domainmodel.activity.ActivityLocation;
import com.rbp.sayban.model.viewModel.activity.ActivityHRViewModel;
import com.rbp.sayban.model.viewModel.activity.ActivityLocationViewModel;
import com.rbp.sayban.service.activity.impl.ActivityHRService;
import com.rbp.sayban.service.activity.impl.ActivityLocationService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= restConstant.BASE_REST +restConstant.ACTIVITY_HRS)
@Api(description = "Operations pertaining to Activity_HR in Activity_HR Management System")
public class ActivityHRSController extends FrameworkAbstractController<ActivityHR, ActivityHRViewModel, ActivityHRService> {
}
