This is a base one of my colleages and I wrote to be used as a base for all of our projects. 
There were several projects invovled to create our final product. Since all of them used the same architectural design, we wrote this base to be used by other developers.
There are generic service, controller and DAO plus other functionalities to search with JPQL or HQL. There is also an auto mapper with mapStruct. 
This base has no security since this part was done by another team. This snippet is for the review of **PDENG group** at **University of Eindhoven**. 